"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Burger
"""

# Standard packages.
import logging
import pathlib

# 3rd party packages.
from PyQt6 import QtCore
from PyQt6.QtCore import pyqtSlot

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
)

# Local packages.
from data.settings import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


# TODO alle Vorkommnisse von BaseProgram durch neuen "Programm-Name" ersetzen.
class BaseProgram(LECOBaseMainWindowDesigner):
    """TODO This is a base for writing your own GUI program, rename as appropriate.

    :param name: Name of this program.
    :param host: Host name of the Coordinator.
    """

    def __init__(
        self, name: str = "BaseProgram", host: str = "localhost", **kwargs
    ) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(
            name=name,
            host=host,
            ui_file_name="BaseProgram",  # ui file name in the data subfolder
            ui_file_path=pathlib.Path(__file__).parent / "data",
            settings_dialog_class=Settings,  # class of the settings dialog
            **kwargs,
        )

        # Apply settings
        # TODO enable save geometry, if desired, see also closeEvent
        # settings = QtCore.QSettings()
        # geometry = settings.value("geometry")
        # if geometry is not None:
        #     self.restoreGeometry(QtCore.QByteArray(geometry))
        self.setSettings()

        # Connect actions to slots.
        log.info(f"BaseProgram initialized with name '{name}'.")

    def __del__(self):
        try:
            self.stop_listen()
        except AttributeError:
            pass

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.stop_listen()

        # TODO: put in stuff you want to do before closing

        # TODO enable if desired
        # # Store the window geometry
        # settings = QtCore.QSettings()
        # settings.setValue("geometry", self.saveGeometry())

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    def setSettings(self) -> None:
        """Apply new settings, called if the settings dialog is accepted."""
        # TODO apply changes to variables.
        settings = QtCore.QSettings()
        print(settings.value("key", "defaultValue", str))

    def sendData(self) -> None:
        """Example how to handle sending data."""
        # TODO Adjust this method according to your needs.
        data = {"key": "value"}
        try:
            # new style: topic is the sender's name
            self.publisher.send_data(data)
            # old style: topic is the variable name
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)


if __name__ == "__main__":  # pragma: no cover
    """Start the main window if this is the called script file."""
    start_app(BaseProgram)

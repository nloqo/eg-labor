"""
Plot window of the Datalogger.

Created on Fri Jul  9 14:32:56 2021 by Benedikt Burger.
"""

import logging
from typing import Protocol, Optional

import numpy as np
import pyqtgraph as pg
from qtpy import QtCore, QtGui, QtWidgets
from qtpy.QtCore import Slot as pyqtSlot  # type: ignore

from analysis import fit_gaussian_beam
from analysis.math import ellipse

from .image import Image

# Parameters
gradient_map = "bipolar"


class BeamPlotterProtocol(Protocol):
    images: dict[str, Image]


class SpecialView(pg.ImageView):
    """A pyqtgraph imageview initialized with a plot item."""

    def __init__(self, *args, **kwargs):
        """Initialize it."""
        super().__init__(view=pg.PlotItem())


class PlotWidget(QtWidgets.QWidget):
    """Class for the plot widgets."""

    image: Image

    def __init__(
        self,
        main_window: BeamPlotterProtocol,
        log: Optional[logging.Logger] = None,
        **kwargs,
    ):
        super().__init__()
        self._setup_actions()
        self._setup_ui()
        self._layout()
        self.show()
        self.main_window = main_window
        self.parent = main_window  # type: ignore

        if log is None:
            self.log = logging.getLogger(__name__)
        else:
            self.log = log.getChild("Plot")

        # Configure comboboxes and plot.
        self.getNames()

        # self.restore_configuration(configuration=kwargs)

        self.setName()

    def _setup_actions(self) -> None:
        """Set up all the actions."""
        self.action_show_toolbar = QtGui.QAction("Show toolbar")  # type: ignore
        self.action_show_toolbar.setIconText("tb")
        self.action_show_toolbar.setToolTip("Show the toolbar (Ctrl + D).")
        self.action_show_toolbar.setCheckable(True)
        self.action_show_toolbar.setShortcut("Ctrl+D")
        self.action_hide_histogram = QtGui.QAction("Hide histogram")
        self.action_hide_histogram.setIconText("hh")
        self.action_hide_histogram.setCheckable(True)
        self.action_analyze = QtGui.QAction("Analyze")
        self.action_analyze.setIconText("an")
        self.action_analyze.setCheckable(True)
        # self.actionly = QtGui.QAction("Show yellow line")  # type: ignore
        # self.actionly.setIconText("ly")
        # self.actionly.setToolTip("Show a yellow line.")
        # self.actionly.setCheckable(True)
        # self.actionlg = QtGui.QAction("Show green line")  # type: ignore
        # self.actionlg.setIconText("lg")
        # self.actionlg.setToolTip("Show a green line.")
        # self.actionlg.setCheckable(True)
        # self.actionv = QtGui.QAction("Large value font")  # type: ignore
        # self.actionv.setIconText("v")
        # self.actionv.setToolTip("Show the value with a larger fontsize.")
        # self.actionv.setCheckable(True)
        # self.actionvls = QtGui.QAction("Show vertical lines")  # type: ignore
        # self.actionvls.setIconText("||")
        # self.actionvls.setToolTip("Show vertical lines.")
        # self.actionvls.setCheckable(True)
        # self.actionEvaluate = QtGui.QAction("Evaluate data")  # type: ignore
        # self.actionEvaluate.setIconText("ev")
        # self.actionEvaluate.setToolTip("Evaluate the data.")
        # self.actionEvaluate.setCheckable(True)

        # Connect actions to slots
        self.action_hide_histogram.toggled.connect(self.hide_histogram)
        self.action_analyze.toggled.connect(self.toggle_analyze)
        # self.actionly.toggled.connect(self.toggleLineY)
        # self.actionlg.toggled.connect(self.toggleLineG)
        # self.actionv.toggled.connect(self.toggleV)
        # self.actionvls.toggled.connect(self.toggleVerticalLines)
        # self.actionEvaluate.toggled.connect(self.evaluate_data)

    def _setup_plot(self) -> None:
        self.pgImage = SpecialView()
        self.pgImage.setPredefinedGradient(gradient_map)
        self.pgImage.getImageItem().setOpts(axisOrder="row-major")
        plotItem = self.pgImage.getView()
        plotItem.setLabel("bottom", "relative position", "m")
        plotItem.setLabel("left", "relative position", "m")
        self.ellipse = plotItem.plot([])
        self.major = plotItem.plot([])

    def _setup_ui(self) -> None:
        """Generate the UI elements."""
        self._setup_plot()
        self.toolbar = QtWidgets.QToolBar(self)
        self.toolbar.setVisible(False)
        self.menu = QtWidgets.QMenu()
        self.pbOptions = QtWidgets.QToolButton()
        self.pbOptions.setText("...")
        self.pbOptions.setToolTip("Show plot options.")
        self.bbNames = QtWidgets.QComboBox()
        self.bbNames.setMaxVisibleItems(15)
        self.bbNames.setToolTip("File name.")
        self.bbKeys = QtWidgets.QComboBox()
        self.bbKeys.setMaxVisibleItems(15)
        self.bbKeys.setToolTip("data key.")

        for action in (
            self.action_analyze,
            self.action_show_toolbar,
        ):
            self.menu.addAction(action)
            self.toolbar.addAction(action)

        self.pbCopyEvaluation = QtWidgets.QPushButton("Fit")
        self.pbCopyEvaluation.setToolTip("")
        self.pbCopyEvaluation.setVisible(False)

        # # Connect widgets to slots
        self.bbNames.activated.connect(self.setName)
        self.bbKeys.activated.connect(self.setKey)
        self.pbOptions.clicked.connect(self.show_menu)
        self.pbCopyEvaluation.clicked.connect(self.copy_evaluation)
        self.action_show_toolbar.toggled.connect(self.toolbar.setVisible)
        self.action_analyze.toggled.connect(self.pbCopyEvaluation.setVisible)

    def _layout(self) -> None:
        """Organize the elements into a layout."""
        button_box = QtWidgets.QHBoxLayout()
        button_box.setSpacing(2)
        button_box.setContentsMargins(0, 0, 0, 0)
        for widget in (
            self.pbOptions,
            self.bbNames,
            self.bbKeys,
            self.pbCopyEvaluation,
        ):
            button_box.addWidget(widget)
        button_box.setStretchFactor(self.bbNames, 1)
        button_box.setStretchFactor(self.bbKeys, 1)

        vbox = QtWidgets.QVBoxLayout(self)
        vbox.setSpacing(1)
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.addWidget(self.toolbar)
        vbox.addWidget(self.pgImage)
        vbox.addLayout(button_box)
        self.setLayout(vbox)

    def closeEvent(self, event) -> None:
        """Close the plot."""
        # try:
        #     self.main_window.timer.timeout.disconnect(self.update)
        #     self.main_window.signals.closing.disconnect(self.close)  # type: ignore
        # except TypeError:
        #     pass  # Already disconnected.
        self.clear_plot()
        event.accept()

    def _set_scale(self, pixel_length: float) -> None:
        plotItem = self.pgImage.getView()
        plotItem.getAxis("left").setScale(pixel_length)
        plotItem.getAxis("bottom").setScale(pixel_length)

    def displayImage(self, key: str) -> None:
        """Show the resulting image."""
        try:
            self.pgImage.setImage(self.image.get_image(key), autoRange=False)
        except KeyError or TypeError:
            pass
        except Exception:
            self.log.exception("Error displaying image.")
        if self.action_analyze.isChecked():
            self.analyze()

    # def get_configuration(self) -> dict[str, Any]:
    #     """Get the current plot configuration."""
    #     configuration = {
    #         "type": type(self).__name__,
    #         "x_key": self.bbNames.currentText(),
    #         "autoCut": self.autoCut,
    #         "ly": self.lineY.value() if self.actionly.isChecked() else False,
    #         "lg": self.lineG.value() if self.actionlg.isChecked() else False,
    #         "vls": (self.lineV1.value(), self.lineV2.value())
    #         if self.actionvls.isChecked()
    #         else False,  # noqa
    #         "evaluation": self.actionEvaluate.isChecked(),
    #     }
    #     return configuration

    # def restore_configuration(self, configuration: dict[str, Any]) -> None:
    #     for key, value in configuration.items():
    #         if key == "x_key":
    #             self.bbNames.setCurrentText(value)
    #         elif key == "autoCut":
    #             self.sbAutoCut.setValue(value)
    #         elif key == "ly":
    #             if value is not False:
    #                 self.toggleLineY(True, start=value)
    #                 self.actionly.setChecked(True)
    #         elif key == "lg":
    #             if value is not False:
    #                 self.toggleLineG(True, start=value)
    #                 self.actionlg.setChecked(True)
    #         elif key == "vls":
    #             if value is not False:
    #                 self.toggleVerticalLines(True, *value)
    #                 self.actionvls.setChecked(True)
    #         elif key == "evaluation":
    #             self.actionEvaluate.setChecked(value)

    def clear_plot(self) -> None:
        """Clear the plots."""
        return
        raise NotImplementedError

    def show_menu(self) -> None:
        self.menu.popup(self.pbOptions.mapToGlobal(QtCore.QPoint(0, 0)))

    def getNames(self):
        self.bbNames.addItems(self.main_window.images.keys())

    @pyqtSlot(str)
    def add_name(self, name: str) -> None:
        self.bbNames.addItem(name)

    @pyqtSlot()
    def setName(self) -> None:
        """Adjust the current x label."""
        text = self.bbNames.currentText()
        try:
            self.image = self.main_window.images[text]
        except KeyError:
            return
        self._set_scale(self.image.pixel_length)
        current_key = self.bbKeys.currentText()
        self.bbKeys.clear()
        self.bbKeys.addItems(self.image.get_keys())
        if current_key:
            self.bbKeys.setCurrentText(current_key)
        else:
            self.bbKeys.setCurrentIndex(0)
        self.setKey()

    def setKey(self) -> None:
        new_key = self.bbKeys.currentText()
        self.displayImage(key=new_key)

    # Action slots
    @pyqtSlot(bool)
    def hide_histogram(self, checked: bool) -> None:
        self.pgImage.ui.histogram.setVisible(not checked)
        self.pgImage.ui.roiBtn.setVisible(not checked)
        self.pgImage.ui.menuBtn.setVisible(not checked)

    def analyze(self) -> None:
        """Analyze the current data."""
        new_key = self.bbKeys.currentText()
        array = self.image.get_image(new_key)
        if len(array[0]) < 1000:
            config = {"zero_offset": True, "radius": 10}
        else:
            config = {}
        par, b, c, d = fit_gaussian_beam.fit_gaussian_cuts(array, **config)
        r1 = 2 * abs(par["ellipse"][0])
        r2 = 2 * abs(par["ellipse"][1])
        phi = -par["ellipse"][2]
        xcircle, ycircle = ellipse(
            angle=np.linspace(0, 2 * np.pi, 100),
            x0=par[0][1],
            y0=par[90][1],
            r1=r1,
            r2=r2,
            phi=phi,
        )
        self.ellipse.setData(xcircle, ycircle)
        self.major.setData(xcircle[0::50], ycircle[0::50])
        self.pbCopyEvaluation.setToolTip(
            f"Radius {r1 * self.image.pixel_length * 1e6:.6} x "
            f"{r2 * self.image.pixel_length * 1e6 :.6} µm "
            f"with an angle of {phi:.6} to the horizontal."
        )

    @pyqtSlot(bool)
    def toggle_analyze(self, checked: bool) -> None:
        if checked:
            self.analyze()
        else:
            self.ellipse.setData([])
            self.major.setData([])

    @pyqtSlot(bool)
    def toggleLineY(self, checked: bool, start: float = 0) -> None:
        """Toggle to show a horizontal line."""
        try:
            self.lineY.setVisible(checked)
        except AttributeError:
            if checked:
                self.lineY: pg.InfiniteLine = self.pgImage.addLine(
                    y=start, pen="y", movable=True
                )

    @pyqtSlot(bool)
    def toggleLineG(self, checked: bool, start: float = 0) -> None:
        """Toggle to show a horizontal line."""
        try:
            self.lineG.setVisible(checked)
        except AttributeError:
            if checked:
                self.lineG: pg.InfiniteLine = self.pgImage.addLine(
                    y=start, pen="g", movable=True
                )

    @pyqtSlot(bool)
    def toggleVerticalLines(self, checked: bool, l1: float = 0, l2: float = 1) -> None:
        try:
            self.lineV1.setVisible(checked)
            self.lineV2.setVisible(checked)
        except AttributeError:
            if checked:
                self.lineV1: pg.InfiniteLine = self.pgImage.addLine(x=l1, pen="y", movable=True)
                # self.lineV1.sigDragged.connect(self.evaluate_data)
                self.lineV2: pg.InfiniteLine = self.pgImage.addLine(x=l2, pen="y", movable=True)
                # self.lineV2.sigDragged.connect(self.evaluate_data)

    @pyqtSlot()
    def copy_evaluation(self) -> None:
        clipboard = QtWidgets.QApplication.instance().clipboard()  # type: ignore
        clipboard.setText(self.pbCopyEvaluation.toolTip())

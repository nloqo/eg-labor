
from pathlib import Path
from typing import Iterable, Optional

import pandas as pd
import numpy as np

from analysis.beam_profiler import import_image as import_profiler_image
from analysis.beam_camera import import_image as import_camera_image


class Image:
    """Contain the information about an image."""
    pixel_length: float
    _data_dict: dict[str, pd.DataFrame]
    _single_data: np.ndarray

    def __init__(self, path: str | Path) -> None:
        self.load_from_extension(path=path)

    def load_from_extension(self, path: str | Path):
        if not isinstance(path, Path):
            path = Path(path)
        suffix = path.suffix
        if suffix == ".npy":
            self.load_beam_camera(path=path)
        else:
            self.load_beam_profiler(path=path)            

    def load_beam_profiler(self, path: Path) -> None:
        data, size = import_profiler_image(name=path.stem, directory=str(path.parent))
        self.pixel_length = size / 1e6
        self._data_dict = data

    def load_beam_camera(self, path: Path) -> None:
        data, size = import_camera_image(name=path.stem, directory=str(path.parent))
        self._single_data = data
        self.pixel_length = size / 1e6

    def get_image(self, key: Optional[str] = None) -> np.ndarray:
        if key:
            return np.array(self._data_dict[key])
        else:
            return self._single_data

    def get_keys(self) -> Iterable[str]:
        if hasattr(self, "_data_dict"):
            return self._data_dict.keys()
        else:
            return []

"""
Main file of the BeamPlotter.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Burger
"""

# Standard packages.
import logging
import pathlib

# 3rd party packages.
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
)
from pyqtgraph.dockarea import DockArea, Dock


# Local packages.
from data.image import Image
from data.plot_widget import PlotWidget
from data.settings import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class Signals(QtCore.QObject):
    name_loaded = QtCore.pyqtSignal(str)


class BeamPlotter(LECOBaseMainWindowDesigner):
    """Show beam profiles.

    :param name: Name of this program.
    :param host: Host name of the Coordinator.
    """

    images: dict[str, Image]

    actionLoad: QtGui.QAction
    actionNew_Dock: QtGui.QAction

    tabWidget: QtWidgets.QTabWidget

    def __init__(
        self, name: str = "BeamPlotter", host: str = "localhost", **kwargs
    ) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(
            name=name,
            host=host,
            ui_file_name="BeamPlotter",  # ui file name in the data subfolder
            ui_file_path=pathlib.Path(__file__).parent / "data",
            settings_dialog_class=Settings,  # class of the settings dialog
            **kwargs,
        )

        self.signals = Signals()
        self.images = {}
        self.last_path = QtCore.QSettings().value("savePath", type=str)

        self.dockArea = DockArea()
        self.dock_count: int = 0
        self.tabWidget.addTab(self.dockArea, "&Plots")

        self.setup_actions()

        self.spawnPlot()

        # Apply settings
        # TODO enable save geometry, if desired, see also closeEvent
        # settings = QtCore.QSettings()
        # geometry = settings.value("geometry")
        # if geometry is not None:
        #     self.restoreGeometry(QtCore.QByteArray(geometry))
        self.setSettings()

        # Connect actions to slots.
        log.info(f"BeamPlotter initialized with name '{name}'.")

    def setup_actions(self) -> None:
        self.actionNew_Dock.triggered.connect(self.spawnPlot)
        self.actionLoad.triggered.connect(self.load_image)

    def __del__(self) -> None:
        try:
            self.stop_listen()
        except AttributeError:
            pass

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.stop_listen()

        settings = QtCore.QSettings()
        settings.setValue("savePath", self.last_path)

        # TODO: put in stuff you want to do before closing

        # TODO enable if desired
        # # Store the window geometry
        # settings = QtCore.QSettings()
        # settings.setValue("geometry", self.saveGeometry())

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    def setSettings(self) -> None:
        """Apply new settings, called if the settings dialog is accepted."""
        # TODO apply changes to variables.
        settings = QtCore.QSettings()
        print(settings.value("key", "defaultValue", str))

    def sendData(self) -> None:
        """Example how to handle sending data."""
        # TODO Adjust this method according to your needs.
        data = {"key": "value"}
        try:
            # new style: topic is the sender's name
            self.publisher.send_data(data)
            # old style: topic is the variable name
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)

    def load_image(self) -> None:
        """Open a file path dialog."""
        settings = QtCore.QSettings()
        file_names = QtWidgets.QFileDialog.getOpenFileNames(
            self,
            caption="Open file",
            directory=self.last_path or settings.value("savePath", type=str),
            filter=(
                ";;".join(
                    (
                        "Any (*.pkl *.npy)",
                        # f"This year ({strftime('%Y')}_*)",
                        # f"This month ({strftime('%Y_%m')}_*)",
                        # f"Today ({strftime('%Y_%m_%d')}*)",
                        "Pickled Profiler (*.pkl)",
                        "Numpy Camera (*.npy)",
                        "All files (*)",
                    )
                )
            ),
        )[0]
        if not file_names:
            return  # user pressed cancel
        self.last_path = file_names[0]
        for file_name in file_names:
            path = pathlib.Path(file_name)
            try:
                image = Image(path)
            except Exception as exc:
                log.exception(f"Loading a file at '{file_name}' failed.", exc_info=exc)
                self.statusBar().showMessage(  # type: ignore
                    f"Loading a file at '{file_name}' failed with {exc}."
                )
            else:
                self.images[path.stem] = image
                self.signals.name_loaded.emit(path.stem)

    def spawnPlot(self, **kwargs) -> None:
        """Spawn a new plot window.

        :param autoCut: Last values to show. If None, read from settings. 0 means all.
        :param multi: Show the multi plot window.
        """
        plot = PlotWidget(
            self,
            **kwargs,
        )
        dock = Dock(name="name", closable=False, widget=plot)
        self.dockArea.addDock(dock)

        self.signals.name_loaded.connect(plot.add_name)

        # self.signals.closing.connect(dock.close)
        # self.signals.closing.connect(plot.close)  # type: ignore


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(BeamPlotter)

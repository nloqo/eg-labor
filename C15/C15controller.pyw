"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot

from devices.gui_utils import parse_command_line_parameters
from pyleco.utils.data_publisher import DataPublisher
import pyvisa
#from pyleco.utils.qt_listener import QtListener

# Local packages.
from data import Settings
from devices import C15

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class C15Controller(QtWidgets.QMainWindow):

    def __init__(self, name: str = "C15Controller", host: str = "localhost", *args, **kwargs) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/C15Gui.ui", self)
        self.setWindowTitle(name)
        self.show()

        #self.listener = QtListener(name=name, logger=log, host=host)
        #self.listener.start_listen()
        self.publisher = DataPublisher(full_name=name, host=host)

        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readout)

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName(name)

        # Apply settings
        # TODO enable save geometry, if desired, see also closeEvent
        # settings = QtCore.QSettings()
        # geometry = settings.value("geometry")
        # if geometry is not None:
        #     self.restoreGeometry(QtCore.QByteArray(geometry))
        self.setSettings()
        self.autostart()

        # Connect actions to slots.
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)
        self.actionConnect.triggered.connect(self.connect)

        self.pbKeyswitch.clicked.connect(self.keyswitch)
        self.pbSetPower.clicked.connect(self.setPower)

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        del self.C15
        event.accept()

    @pyqtSlot()
    def openSettings(self) -> None:
        """Open the settings dialogue and apply changed settings."""
        settingsDialog = Settings.Settings()
        if settingsDialog.exec():
            self.setSettings()

    def setSettings(self) -> None:
        """Apply new settings."""
        # TODO apply changes to variables.
        settings = QtCore.QSettings()
        # self.publisher.port = settings.value("publisherPort", type=int)

    def sendData(self) -> None:
        """Example how to handle sending data."""
        # TODO Adjust this method according to your needs.
        data = {"key": "value"}
        try:
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)

    def autostart(self):
        try:
            self.actionConnect.setChecked(True)
            self.connect()
        except RuntimeError:
            log.error("Couldn't connect to laser. Try manually.")

    def connect(self):
        settings = QtCore.QSettings()
        Port = settings.value("LaserPort")
        try:
            self.C15 = C15.C15(f"COM{Port}", visa_library="@py")
            self.faultList = C15.faultList()
            self.readoutTimer.start(500)
        except (ConnectionError, RuntimeError, pyvisa.errors.VisaIOError):
            self.actionConnect.setChecked(False)
            log.warning("Couldn't connect to C15 laser.")

    def readout(self):
        '''Readout of all relevant data.'''
        # Status of keyswitch, shutter and laser.
        try:
            try:
                self.lbShutterStatus.setText(self.C15.shutter_state())
                if self.lbShutterStatus.text() == "Shutter closed":
                    self.lbShutterStatus.setStyleSheet("color: red")
                else:
                    self.lbShutterStatus.setStyleSheet("color: green")
            except ValueError:
                pass
            self.lbLaserStatus.setText(self.C15.state())
            if self.lbLaserStatus.text() == "Fault" or self.lbLaserStatus.text() == "Standby":
                self.lbLaserStatus.setStyleSheet("color: red")
            elif self.lbLaserStatus.text() == "Warmup" or self.lbLaserStatus.text() == "Ramping Up All Diodes" or self.lbLaserStatus.text() == "CDRH Delay":
                self.lbLaserStatus.setStyleSheet("color: orange")
            else:
                self.lbLaserStatus.setStyleSheet("color: green")
            self.lbKeyStatus.setText(self.C15.key_state())
            if self.lbKeyStatus.text() == "Off":
                self.lbKeyStatus.setStyleSheet("color: red")
            else:
                self.lbKeyStatus.setStyleSheet("color: green")
            self.lbVirtualKeyStatus.setText(self.C15.virtKey_state())
            if self.lbVirtualKeyStatus.text() == "Off":
                self.lbVirtualKeyStatus.setStyleSheet("color: red")
            else:
                self.lbVirtualKeyStatus.setStyleSheet("color: green")
            self.lbBRFstate.setText(self.C15.get_BRFstate())
            if self.lbBRFstate.text == "off" or self.lbBRFstate.text() == "open" or self.lbBRFstate.text() == "faulted":
                self.lbBRFstate.setStyleSheet("color: red")
            elif self.lbBRFstate.text() == "seeking" or self.lbBRFstate.text() == "optimizing":
                self.lbBRFstate.setStyleSheet("color: yellow")
            else:
                self.lbBRFstate.setStyleSheet("color: green")
            self.lbSHGstate.setText(self.C15.get_SHGstate())
            if self.lbSHGstate.text() == "off" or self.lbSHGstate.text() == "open" or self.lbSHGstate.text == "faulted":
                self.lbSHGstate.setStyleSheet("color: red")
            elif self.lbSHGstate.text() == "seeking" or self.lbSHGstate.text() == "optimizing":
                self.lbSHGstate.setStyleSheet("color: orange")
            else:
                self.lbSHGstate.setStyleSheet("color: green")
            #Read faults
            try:
                faults = self.C15.faults().split("&")
                warnings = self.C15.warnings().split("&")
                if faults[0] == "SYSTEM OK" and warnings[0] == "SYSTEM OK":
                    self.lbFaults.setText("System OK")
                    self.lbFaults.setStyleSheet("color: green")
                else:
                    listedFaults = ""
                    if warnings[0] != "SYSTEM OK":
                        for x in warnings:
                            for element in self.faultList:
                                if element[0] == x:
                                    listedFaults += element[1] + "\t" + element[2] + "\n"
                        self.lbFaults.setStyleSheet("color: orange")
                    elif faults[0] != "SYSTEM OK":
                        for x in faults:
                            for element in self.faultList:
                                if element[0] == x:
                                    listedFaults += element[1] + "\t" + element[2] + "\n"
                        self.lbFaults.setStyleSheet("color: red")
                    self.lbFaults.setText(listedFaults)
            except (AttributeError, ValueError):
                pass
            # Read Power
            self.lbPower.setText(str(self.C15.get_power()))
            # Read temperatures
            data = {}
            data["T_BRF"] = self.C15.get_BRFtemp()
            data["T_BRFset"] = self.C15.get_BRFset()
            data["BRFdrive"] = self.C15.get_BRFdrive()
            data["T_SHG"] = self.C15.get_SHGtemp()
            data["T_SHGset"] = self.C15.getSHGset()
            data["SHGdrive"] = self.C15.get_SHGdrive()
            data["battery"] = self.C15.get_bat()
            data["D1current"] = self.C15.get_currentD1()
            data["D2current"] = self.C15.get_diode2Current()
            data["currentSet"] = self.C15.get_currentset()
            data["T_module"] = self.C15.get_modulTemp()
            data["T_CPU"] = self.C15.get_CPUtemp()
            data["power"] = self.C15.get_power()
            data["T_main"] = self.C15.get_maintemp()
            data["T_mainset"] = self.C15.get_mainset()
            data["T_OPS"] = self.C15.get_OPStemp()
            data["T_OPSset"] = self.C15.get_OPSset()
            data["T_resonator"] = self.C15.get_resonatortemp()
            data["T_resonatorset"] = self.C15.get_resonatorset()
            data["T_FAP1"] = self.C15.get_FAP1temp()
            data["T_FAP1set"] = self.C15.get_FAP1set()
            data["T_FAP2"] = self.C15.get_FAP2temp()
            data["T_FAP2set"] = self.C15.get_FAP2set()
            self.publisher.send_legacy(data)
        except pyvisa.errors.VisaIOError:
            log.exception("Timeout error.")
            self.C15.read_bytes(-1)

    @pyqtSlot()
    def keyswitch(self):
        if self.pbKeyswitch.isChecked():
            self.C15.virtKey_on(1)
        else:
            if float(self.lbPower.text()) == 0:
                self.C15.virtKey_on(0)
            else:
                self.C15.setPower(0)
                self.keyswitch()

    @pyqtSlot()
    def setPower(self):
        print(f"New power setpoint: {self.sbSetPower.value()} W")
        self.C15.setPower(self.sbSetPower.value())


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    doc = C15Controller.__doc__
    kwargs = parse_command_line_parameters(
        description=doc.split(":param", maxsplit=1)[0] if doc else None,
    )
    app = QtWidgets.QApplication([])  # create an application
    mainwindow = C15Controller(**kwargs)  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

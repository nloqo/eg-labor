"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import pyqtSlot

from devices.gui_utils import parse_command_line_parameters
from pyleco_extras.gui_utils.base_main_window import LECOBaseMainWindowDesigner, start_app
import pyvisa

# Local packages.
from data import Settings
from devices import C15_enhancedProtocolVersion as C15

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class C15Controller(LECOBaseMainWindowDesigner):

    def __init__(self, name: str = "C15Controller",
                 host: str = "localhost", **kwargs) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(name=name, ui_file_name="C15Gui", **kwargs)

        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readout)

        self.index = 0
        self.setSettings()
        self.autostart()

        # Connect actions to slots.
        self.actionConnect.triggered.connect(self.connect)

        self.pbKeyswitch.clicked.connect(self.keyswitch)
        self.pbSetPower.clicked.connect(self.setPower)

        # self.setup_laser_status()

    def setup_laser_status(self) -> None:
        """Configure the listener to return the laser status."""
        self.listener.register_rpc_method(self.get_parameters)
        self.listener.local_methods.append("get_parameters")

    def get_parameters(self, parameters: list[str]) -> dict:
        """Get device properties from the list `properties`."""
        data = {}
        for key in parameters:
            if key == "emission_enabled":
                data[key] = self.lbKeyStatus.text() != "Off"
            elif key == "power":
                data[key] = float(self.lbPower.text())
        return data

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        try:
            self.readoutTimer.stop()
            del self.C15
        except AttributeError:
            pass
        self.stop_listen()
        event.accept()

    @pyqtSlot()
    def openSettings(self) -> None:
        """Open the settings dialogue and apply changed settings."""
        settingsDialog = Settings.Settings()
        if settingsDialog.exec():
            self.setSettings()

    def setSettings(self) -> None:
        """Apply new settings."""
        settings = QtCore.QSettings()
        # self.publisher.port = settings.value("publisherPort", type=int)

    def sendData(self) -> None:
        """Example how to handle sending data."""
        data = {"key": "value"}
        try:
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)

    def autostart(self):
        try:
            self.actionConnect.setChecked(True)
            self.connect(True)
        except RuntimeError:
            log.error("Couldn't connect to laser. Try manually.")
            self.actionConnect.setChecked(False)

    @pyqtSlot(bool)
    def connect(self, status):
        settings = QtCore.QSettings()
        Port = settings.value("LaserPort")
        self.averagingIndex = 0
        if status:
            try:
                self.C15 = C15.C15(f"COM{Port}")
                self.C15.write("EP=1")
                self.C15.read_bytes(-1)
                virtualKeyState = self.C15.virtKey_state(self.index)
                if virtualKeyState[1] == self.index:
                    if virtualKeyState[0] == 1:
                        self.pbKeyswitch.setChecked(True)
                    else:
                        self.pbKeyswitch.setChecked(False)
                else:
                    log.info("Index error for virtual key state readout.")
                self.set_index()
                self.faultList = C15.faultList()
                self.readoutTimer.start(400)
            except (ConnectionError, RuntimeError, pyvisa.errors.VisaIOError) as exc:
                self.actionConnect.setChecked(False)
                log.warning(f"Couldn't connect to C15 laser.: {exc}")
        else:
            try:
                self.readoutTimer.stop()
                del self.C15
            except AttributeError:
                pass

    def set_index(self):
        if self.index < 9:
            self.index += 1
        else:
            self.index = 0

    def readout(self):
        '''Readout of all relevant data.'''
        try:
            # Status of keyswitch, shutter and laser.
            # Shutter state readout
            shutterState = self.C15.shutter_state(self.index)
            if shutterState[1] == self.index:
                self.lbShutterStatus.setText(shutterState[0])
            else:
                log.info("Index error for shutter state readout.")
            self.set_index()
            # Coloring the label
            if self.lbShutterStatus.text() == "Shutter closed":
                self.lbShutterStatus.setStyleSheet("color: red")
            elif self.lbShutterStatus.text() == "Shutter open":
                self.lbShutterStatus.setStyleSheet("color: green")
            else:
                self.lbShutterStatus.setStyleSheet("color: black")
            # Laser status readout
            laserStatus = self.C15.state(self.index)
            if laserStatus[1] == self.index:
                self.lbLaserStatus.setText(laserStatus[0])
            else:
                log.info("Index error for laser status readout.")
            self.set_index()
            # Coloring the label
            if self.lbLaserStatus.text() == "Fault" or self.lbLaserStatus.text() == "Standby":
                self.lbLaserStatus.setStyleSheet("color: red")
            elif (self.lbLaserStatus.text() == "Warmup"
                  or self.lbLaserStatus.text() == "Ramping Up All Diodes"
                  or self.lbLaserStatus.text() == "CDRH Delay"):
                self.lbLaserStatus.setStyleSheet("color: orange")
            else:
                self.lbLaserStatus.setStyleSheet("color: green")
            # Key state readout
            keyState = self.C15.key_state(self.index)
            if keyState[1] == self.index:
                self.lbKeyStatus.setText(keyState[0])
            else:
                log.info("Index error for key state readout.")
            self.set_index()
            # Coloring the label
            if self.lbKeyStatus.text() == "Off":
                self.lbKeyStatus.setStyleSheet("color: red")
            else:
                self.lbKeyStatus.setStyleSheet("color: green")
            # Virtual key state readout
            virtualKeyState = self.C15.virtKey_state(self.index)
            if virtualKeyState[1] == self.index:
                self.lbVirtualKeyStatus.setText(virtualKeyState[0])
            else:
                log.info("Index error for virtual key state readout.")
            self.set_index()
            # Coloring the label
            if self.lbVirtualKeyStatus.text() == "Off":
                self.lbVirtualKeyStatus.setStyleSheet("color: red")
            else:
                self.lbVirtualKeyStatus.setStyleSheet("color: green")
            # BRF temperature state readout
            BRF_state = self.C15.get_BRFstate(self.index)
            if BRF_state[1] == self.index:
                self.lbBRFstate.setText(BRF_state[0])
            else:
                log.info("Index error for BRF temperature state readout.")
            self.set_index()
            # Coloring the label
            if (self.lbBRFstate.text == "off"
                or self.lbBRFstate.text() == "open"
                or self.lbBRFstate.text() == "faulted"):
                self.lbBRFstate.setStyleSheet("color: red")
            elif self.lbBRFstate.text() == "seeking" or self.lbBRFstate.text() == "optimizing":
                self.lbBRFstate.setStyleSheet("color: orange")
            else:
                self.lbBRFstate.setStyleSheet("color: green")
            # SHG temperature state readout
            SHG_state = self.C15.get_SHGstate(self.index)
            if SHG_state[1] == self.index:
                self.lbSHGstate.setText(SHG_state[0])
            else:
                log.info("Index error for SHG temperature state readout.")
            self.set_index()
            # Coloring the label
            if (self.lbSHGstate.text() == "off"
                or self.lbSHGstate.text() == "open"
                or self.lbSHGstate.text == "faulted"):
                self.lbSHGstate.setStyleSheet("color: red")
            elif self.lbSHGstate.text() == "seeking" or self.lbSHGstate.text() == "optimizing":
                self.lbSHGstate.setStyleSheet("color: orange")
            else:
                self.lbSHGstate.setStyleSheet("color: green")
            # Read faults
            try:
                faults_raw = self.C15.faults(self.index)
                faults = faults_raw[0].split("&")
                self.set_index()
                warnings_raw = self.C15.warnings(self.index)
                warnings = warnings_raw[0].split("&")
                if (warnings_raw[1] == self.index
                    and (faults_raw[1] == self.index - 1 or faults_raw[1] == 0)):
                    if faults[0] == "SYSTEM OK" and warnings[0] == "SYSTEM OK":
                        self.lbFaults.setText("System OK")
                        self.lbFaults.setStyleSheet("color: green")
                    else:
                        listedFaults = ""
                        if warnings[0] != "SYSTEM OK":
                            for x in warnings:
                                for element in self.faultList:
                                    if element[0] == x:
                                        listedFaults += element[1] + "\t" + element[2] + "\n"
                            self.lbFaults.setStyleSheet("color: orange")
                        elif faults[0] != "SYSTEM OK":
                            for x in faults:
                                for element in self.faultList:
                                    if element[0] == x:
                                        listedFaults += element[1] + "\t" + element[2] + "\n"
                            self.lbFaults.setStyleSheet("color: red")
                        self.lbFaults.setText(listedFaults)
                else:
                    log.info("Index error for fault or warning readout.")
            except (AttributeError, ValueError, IndexError):
                log.info(f"Couldn't print the following fault/warning response: {faults, warnings}")
                pass
            self.set_index()
            # Read Power
            data = {}
            power = self.C15.get_power(self.index)
            if power[1] == self.index:
                self.lbPower.setText(str(power[0]))
                data["power"] = power[0]
            else:
                log.info("Index error for power readout.")
            self.set_index()
            # Read temperatures
            T_BRF = self.C15.get_BRFtemp(self.index)
            if T_BRF[1] == self.index:
                data["T_BRF"] = T_BRF[0]
            else:
                log.info("Index error for T_BRF readout.")
            self.set_index()
            T_BRFset = self.C15.get_BRFset(self.index)
            if T_BRFset[1] == self.index:
                data["T_BRFset"] = T_BRFset[0]
            else:
                log.info("Index error for T_BRFset readout.")
            self.set_index()
            BRFdrive = self.C15.get_BRFdrive(self.index)
            if BRFdrive[1] == self.index:
                data["BRFdrive"] = BRFdrive[0]
            else:
                log.info("Index error for BRFdrive readout.")
            self.set_index()
            T_SHG = self.C15.get_SHGtemp(self.index)
            if T_SHG[1] == self.index:
                data["T_SHG"] = T_SHG[0]
            else:
                log.info("Index error for T_SHG readout.")
            self.set_index()
            T_SHGset = self.C15.getSHGset(self.index)
            if T_SHGset[1] == self.index:
                data["T_SHGset"] = T_SHGset[0]
            else:
                log.info("Index error for T_SHGset readout.")
            self.set_index()
            SHGdrive = self.C15.get_SHGdrive(self.index)
            if SHGdrive[1] == self.index:
                data["SHGdrive"] = SHGdrive[0]
            else:
                log.info("Index error for SHGdrive readout.")
            self.set_index()
            D1current = self.C15.get_currentD1(self.index)
            if D1current[1] == self.index:
                data["D1current"] = D1current[0]
            else:
                log.info("Index error for D1current readout.")
            self.set_index()
            D2current = self.C15.get_diode2Current(self.index)
            if D2current[1] == self.index:
                data["D2current"] = D2current[0]
            else:
                log.info("Index error for D2current readout.")
            self.set_index()
            currentSet = self.C15.get_currentset(self.index)
            if currentSet[1] == self.index:
                data["currentSet"] = currentSet[0]
            else:
                log.info("Index error for currentSet readout.")
            self.set_index()
            T_module = self.C15.get_modulTemp(self.index)
            if T_module[1] == self.index:
                data["T_module"] = T_module[0]
            else:
                log.info("Index error for T_module readout.")
            self.set_index()
            T_CPU = self.C15.get_CPUtemp(self.index)
            if T_CPU[1] == self.index:
                data["T_CPU"] = T_CPU[0]
            else:
                log.info("Index error for T_CPU readout.")
            self.set_index()
            T_CPU = self.C15.get_CPUtemp(self.index)
            if T_CPU[1] == self.index:
                data["T_CPU"] = T_CPU[0]
            else:
                log.info("Index error for T_CPU readout.")
            self.set_index()
            T_main = self.C15.get_maintemp(self.index)
            if T_main[1] == self.index:
                data["T_main"] = T_main[0]
            else:
                log.info("Index error for T_main readout.")
            self.set_index()
            T_mainset = self.C15.get_mainset(self.index)
            if T_mainset[1] == self.index:
                data["T_mainset"] = T_mainset[0]
            else:
                log.info("Index error for T_mainset readout.")
            self.set_index()
            T_OPS = self.C15.get_OPStemp(self.index)
            if T_OPS[1] == self.index:
                data["T_OPS"] = T_OPS[0]
            else:
                log.info("Index error for T_OPS readout.")
            self.set_index()
            T_OPSset = self.C15.get_OPSset(self.index)
            if T_OPSset[1] == self.index:
                data["T_OPSset"] = T_OPSset[0]
            else:
                log.info("Index error for T_OPSset readout.")
            self.set_index()
            T_resonator = self.C15.get_resonatortemp(self.index)
            if T_resonator[1] == self.index:
                data["T_resonator"] = T_resonator[0]
            else:
                log.info("Index error for T_resonator readout.")
            self.set_index()
            T_resonatorset = self.C15.get_resonatorset(self.index)
            if T_resonatorset[1] == self.index:
                data["T_resonatorset"] = T_resonatorset[0]
            else:
                log.info("Index error for T_resonatorset readout.")
            self.set_index()
            T_FAP1 = self.C15.get_FAP1temp(self.index)
            if T_FAP1[1] == self.index:
                data["T_FAP1"] = T_FAP1[0]
            else:
                log.info("Index error for T_FAP1 readout.")
            self.set_index()
            T_FAP2 = self.C15.get_FAP2temp(self.index)
            if T_FAP2[1] == self.index:
                data["T_FAP2"] = T_FAP2[0]
            else:
                log.info("Index error for T_FAP2 readout.")
            self.set_index()
            T_FAP2set = self.C15.get_FAP2set(self.index)
            if T_FAP2set[1] == self.index:
                data["T_FAP2set"] = T_FAP2set[0]
            else:
                log.info("Index error for T_FAP2set readout.")
            self.set_index()
            self.averagingIndex += 1
            if self.averagingIndex == 20:
                self.publisher.send_legacy(data)
                self.averagingIndex = 0
        except (pyvisa.errors.VisaIOError, ConnectionError, TimeoutError, TypeError) as exc:
            log.warning(f"Readout failed with the following error code (restart application):\n {exc}")
            self.C15.read_bytes(-1)
            self.connect(False)
            self.connect(True)

    @pyqtSlot(bool)
    def keyswitch(self, status):
        if status:
            self.C15.virtKey_on(1, self.index)
            self.set_index()
        else:
            if float(self.lbPower.text()) == 0:
                self.C15.virtKey_on(0, self.index)
                self.set_index()
            else:
                self.C15.setPower(0, self.index)
                self.set_index()
                self.keyswitch()

    @pyqtSlot()
    def setPower(self):
        print(f"New power setpoint: {self.sbSetPower.value()} W")
        self.C15.setPower(self.sbSetPower.value(), self.index)
        self.set_index()


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(C15Controller)

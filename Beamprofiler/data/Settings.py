"""
Settings dialog for the Beamprofiler.

classes
-------
GeneralSettings
    The dialog for all the settings.


Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from qtpy import uic, QtCore, QtWidgets

from devices import motors


class GeneralSettings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self, *args, **kwargs):
        """Initialize the settings dialog."""
        super().__init__(*args, **kwargs)

        uic.load_ui.loadUi("data/Settings.ui", self)
        self.show()
        self.signals = self.SettingsSignals()

        self.settings = QtCore.QSettings()

        self.sets = (
            # Motoren
            # (self.leName, 'cardName', "", str),
            (self.sb_com, "COM", 0, int),
            (self.sbMotorMin, "motorMin", 0, float),
            (self.sbMotorMax, "motorMax", 100, float),
            # General
            (self.sbCalibrationX1, "calibrationX1", 1, float),
            (self.sbCalibrationX2, "calibrationX2", 0, float),
            (self.sbCalibrationX2, "calibrationX2", 0, float),
            (self.sbPlotLength, "plotLength", 100, int),
            (self.sbCalibrationOffset, "calibrationOffset", 0, float),
            (self.sbDeadband, "deadband", 10, int),
            # (self.leSavePath, 'savePath', "", str)  # different methods
        )

        self.readValues()

        # CONNECT BUTTONS
        # define RestoreDefaults button and connect it
        self.pbRestoreDefaults = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        )
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)
        self.pbSavePath.clicked.connect(self.openFileDialog)
        # Motor buttons
        self.pbMotor1.clicked.connect(self.openMotor1)
        self.pbMotor2.clicked.connect(self.openMotor2)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings."""

        motorConfigured = QtCore.Signal(str)

    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        self.leName.setText(self.settings.value("cardName", type=str))
        self.leSavePath.setText(self.settings.value("savePath", type=str))

    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)
        self.leName.setText("")

    def accept(self):
        """Save the values from the user interface in the settings."""
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        self.settings.setValue("cardName", self.leName.text())
        self.settings.setValue("savePath", self.leSavePath.text())
        super().accept()  # make the normal accept things

    def openFileDialog(self):
        """Open a file dialog to enter the path to the log file folder."""
        path = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
        self.leSavePath.setText(path)

    def openMotor1(self):
        """Open motor settings for Motor 1"""
        motorSettings = motors.MotorSettings(key="motor1", motorName="Motor 1")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("motor1")

    def openMotor2(self):
        """Open motor settings for Motor 2"""
        motorSettings = motors.MotorSettings(key="motor2", motorName="Motor 2")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("motor2")


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    app = QtWidgets.QApplication([])  # start an application
    app.setOrganizationName("NLOQO")
    app.setApplicationName("Beamprofiler")
    mainwindow = GeneralSettings()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

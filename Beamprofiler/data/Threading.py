# -*- coding: utf-8 -*-
"""
Extra Threads for the Beamprofiler

classes
-------
WorkerThread
    Moves the motors and does the measurement.

Created on Thu Jan 20 14:51:23 2022

@author: moneke
"""

import logging
import time

from PyQt6 import QtCore

from pyleco_extras.directors.tmc_motor_director import TMCMotorDirector
from devices import motors


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


class Worker(QtCore.QObject):
    """A thread that handles the motor control."""

    measurement_started = QtCore.pyqtSignal(int)
    measurement_finished = QtCore.pyqtSignal()
    signalStartMP = QtCore.pyqtSignal()
    signalStopMP = QtCore.pyqtSignal(int, int, list)
    alignmentEnd = QtCore.pyqtSignal(str)

    # init checkboxes, will be controlled via setHor and setVer.
    measureHor = False
    measureVer = False

    def setConfigurations(self, configurations):
        """Store the measurement configuration"""
        log.debug("Set config")
        # Center points.
        self.start1 = configurations["Start1"]
        self.start2 = configurations["Start2"]
        # Ranges.
        self.range1 = configurations["Range1"]
        self.range2 = configurations["Range2"]
        self.stepsize = configurations["Stepsize"]
        self.pixels1: int = configurations["Number1"]
        self.pixels2: int = configurations["Number2"]
        self.motors: TMCMotorDirector = configurations["motors"]
        self.settings = QtCore.QSettings()
        self.config1 = self.settings.value("motor1", type=dict)
        self.config2 = self.settings.value("motor2", type=dict)
        if self.config2 == {} or self.config1 == {}:
            return
        self.motor1: int = self.config1["motorNumber"]
        self.motor2: int = self.config2["motorNumber"]
        self.measurementTime = configurations["Time"]
        self.stepwidth1 = motors.unitsToSteps(self.stepsize, self.config1)
        self.stepwidth2 = motors.unitsToSteps(self.stepsize, self.config2)

    def moveToCenter(self):
        """Move the motors to the set center positions."""
        log.info("Move to center.")
        startPositionMotor1 = motors.unitsToSteps(self.start1, self.config1)
        startPositionMotor2 = motors.unitsToSteps(self.start2, self.config2)

        self.motors.move_to(self.motor1, startPositionMotor1)
        self.motors.move_to(self.motor2, startPositionMotor2)

    def measurePoint(self, measurementTime, i, j):
        """Take a measurement point during `measurementTime` at indices i,j."""
        assert not self.stop, "Motors have been stopped."
        log.debug(f"Start MP with i {i}, j {j}.")
        position1 = motors.stepsToUnits(
            self.motors.motors[self.motor1].actual_position, self.config1
        )
        position2 = motors.stepsToUnits(
            self.motors.motors[self.motor2].actual_position, self.config2
        )

        self.signalStartMP.emit()  # To empty temporary list.
        time.sleep(measurementTime)  # Collecting data.
        self.signalStopMP.emit(i, j, [position1, position2])
        log.debug("stop MP")
        assert not self.stop, "Motors have been stopped."

    def waiting(self, motors: list[int] | tuple[int, ...]) -> None:
        """Wait until motors have reached their position."""
        result = self.stop
        while not result:
            result = True
            for motor in motors:
                result *= self.motors.motors[motor].get_position_reached()
            result = result or self.stop

    def setHor(self, state):
        """gets value from checkbox for horizontal alignment measurement"""
        self.measureHor = state
        log.debug("hor state = " + str(self.measureHor))

    def setVer(self, state):
        """gets value from checkbox for vertical alignment measurement"""
        self.measureVer = state
        log.debug("ver state = " + str(self.measureVer))

    # Different algorithms.
    def measure(self):
        """Measure in a grid-like data point arrangement."""
        log.info("start measure")
        self.stop = False
        self.measurement_started.emit(self.pixels1 * self.pixels2)

        # Move to start.
        startPositionMotor1 = motors.unitsToSteps(
            self.start1 - self.stepsize * (self.pixels1 - 1) / 2, self.config1
        )
        startPositionMotor2 = motors.unitsToSteps(
            self.start2 - self.stepsize * (self.pixels2 - 1) / 2, self.config2
        )
        self.motors.move_to(self.motor1, startPositionMotor1)
        self.motors.move_to(self.motor2, startPositionMotor2)
        self.waiting((self.motor1, self.motor2))
        if self.stop:
            return

        sign = 1
        for j in range(self.pixels1):
            log.debug(f"column j {j}.")
            ranging = range(self.pixels2 - 1) if sign == 1 else range(self.pixels2 - 1, 0, -1)
            i = 0
            for i in ranging:
                try:
                    self.measurePoint(self.measurementTime, i, j)
                except AssertionError:
                    log.error("Motor stopped")
                    return
                self.motors.move_by(self.motor2, sign * self.stepwidth2)
                log.debug(f"move down to i {i}")
                self.waiting((self.motor2,))
                if self.stop:
                    return
            i += sign
            try:
                self.measurePoint(self.measurementTime, i, j)
            except AssertionError:
                log.error("Motor stopped")
                return
            if j == self.pixels1 - 1:
                break  # skip moving on last pixel
            if self.stop:
                return
            self.motors.move_by(self.motor1, self.stepwidth1)
            log.debug(f"move to j {j + 1}")
            self.waiting((self.motor1,))
            sign *= -1

        self.moveToCenter()
        self.measurement_finished.emit()

    def measureOneway(self):
        """
        Measure in a grid-like data point arrangement.

        Moving the second motor always in the same direction.
        """
        log.info("start measure one way")
        self.stop = False
        self.measurement_started.emit(self.pixels1 * self.pixels2)
        log.debug(("1", motors.unitsToSteps(self.start1, self.config1)))
        log.debug(("2", motors.unitsToSteps(self.start2, self.config2)))
        log.debug(("1/2", motors.unitsToSteps(self.range1 / 2, self.config1)))
        log.debug(("2/2", motors.unitsToSteps(self.range2 / 2, self.config2)))

        # Move to start.
        startPositionMotor1 = motors.unitsToSteps(
            self.start1 - self.stepsize * (self.pixels1 - 1) / 2, self.config1
        )
        startPositionMotor2 = motors.unitsToSteps(
            self.start2 - self.stepsize * (self.pixels2 - 1) / 2, self.config2
        )
        self.motors.move_to(self.motor1, startPositionMotor1)
        self.motors.move_to(self.motor2, startPositionMotor2)
        self.waiting((self.motor1, self.motor2))
        if self.stop:
            return

        for j in range(self.pixels1):
            log.debug(f"column j {j}.")
            for i in range(self.pixels2):
                log.debug(f"i mp {i}.")
                try:
                    self.measurePoint(self.measurementTime, i, j)
                except AssertionError:
                    log.error("Motor stopped")
                    return
                if i == self.pixels2 - 1:
                    break  # skip moving on last pixel
                self.motors.move_by(self.motor2, self.stepwidth2)
                log.debug("move to i")
                self.waiting((self.motor2,))
                if self.stop:
                    return
            if j == self.pixels1 - 1:
                break  # skip moving on last pixel
            if self.stop:
                return
            self.motors.move_to(self.motor2, startPositionMotor2)
            self.motors.move_by(self.motor1, self.stepwidth1)
            self.waiting((self.motor1, self.motor2))

        self.moveToCenter()
        self.measurement_finished.emit()

    def cuts(self):
        """
        Measure along a horizontal and a vertical cut.

        Intersecting in the set center positions.
        """
        self.stop = False
        self.measurement_started.emit(self.pixels1 + self.pixels2)

        log.info("Start cuts measurement.")
        # Move to start for horizontal cut.
        startPositionMotor1 = motors.unitsToSteps(
            self.start1 - self.stepsize * (self.pixels1 - 1) / 2, self.config1
        )
        startPositionMotor2 = motors.unitsToSteps(self.start2, self.config2)
        self.motors.move_to(self.motor1, startPositionMotor1)
        self.motors.move_to(self.motor2, startPositionMotor2)
        self.waiting((self.motor1, self.motor2))
        if self.stop:
            return
        i = (self.pixels2 - 1) // 2
        log.debug(f"i {i}.")
        for j in range(self.pixels1):
            try:
                self.measurePoint(self.measurementTime, i, j)
                log.debug(f"i {i}, j {j}.")
            except AssertionError:
                return
            if j == self.pixels1 - 1:
                break  # skip moving on last pixel
            self.motors.move_by(self.motor1, self.stepwidth1)
            self.waiting((self.motor1,))
        self.alignmentEnd.emit("x")

        # Move to start for vertical cut.
        startPositionMotor1 = motors.unitsToSteps(self.start1, self.config1)
        startPositionMotor2 = motors.unitsToSteps(
            self.start2 - self.stepsize * (self.pixels2 - 1) / 2, self.config2
        )
        self.motors.move_to(self.motor1, startPositionMotor1)
        self.motors.move_to(self.motor2, startPositionMotor2)
        self.waiting((self.motor1, self.motor2))
        if self.stop:
            return
        j = (self.pixels1 - 1) // 2
        for i in range(self.pixels2):
            try:
                self.measurePoint(self.measurementTime, i, j)
            except AssertionError:
                return
            if i == self.pixels2 - 1:
                break  # skip moving on last pixel
            self.motors.move_by(self.motor2, self.stepwidth2)
            self.waiting((self.motor2,))

        self.moveToCenter()
        self.measurement_finished.emit()

    def alignment(self):
        """
        Measure along a horizontal and a vertical cut.

        Intersecting in the set center positions.
        """
        self.stop = False
        self.measurement_started.emit(0)

        sign1 = 1
        sign2 = 1

        # Move to start for horizontal cut.
        if self.measureHor:
            startPositionMotor1 = motors.unitsToSteps(
                self.start1 - self.stepsize * (self.pixels1 - 1) / 2, self.config1
            )
            startPositionMotor2 = motors.unitsToSteps(self.start2, self.config2)
            self.motors.move_to(self.motor1, startPositionMotor1)
            self.motors.move_to(self.motor2, startPositionMotor2)
            self.waiting((self.motor1, self.motor2))
        elif self.measureVer:
            startPositionMotor1 = motors.unitsToSteps(self.start1, self.config1)
            startPositionMotor2 = motors.unitsToSteps(
                self.start2 - self.stepsize * (self.pixels2 - 1) / 2, self.config2
            )
            self.motors.move_to(self.motor1, startPositionMotor1)
            self.motors.move_to(self.motor2, startPositionMotor2)
            self.waiting((self.motor1, self.motor2))

        while not self.stop:
            # get current info from gui.
            QtCore.QCoreApplication.processEvents()

            if self.measureHor:
                # move vertical motor back if necessary:
                startPositionMotor2 = motors.unitsToSteps(self.start2, self.config2)

                # Move to start of scan.
                startPositionMotor1 = motors.unitsToSteps(
                    self.start1 - sign1 * self.stepsize * (self.pixels1 - 1) / 2, self.config1
                )

                self.motors.move_to(self.motor1, startPositionMotor1)
                self.motors.move_to(self.motor2, startPositionMotor2)
                self.waiting((self.motor1, self.motor2))

                if self.stop:
                    return

                i = int(round(self.range2 / (self.stepsize)) / 2)

                ranging = range(self.pixels1) if sign1 == 1 else range(self.pixels1 - 1, 1, -1)
                for j in ranging:
                    try:
                        self.measurePoint(self.measurementTime, i, j)
                    except AssertionError:
                        return
                    self.motors.move_by(self.motor1, sign1 * self.stepwidth1)
                    self.waiting((self.motor1,))
                sign1 *= -1
                self.alignmentEnd.emit("x")

            QtCore.QCoreApplication.processEvents()

            # Move to start for vertical cut.
            if self.measureVer:
                # move horizontal motor back if necessary:
                startPositionMotor1 = motors.unitsToSteps(self.start1, self.config1)

                # Move to start.
                startPositionMotor2 = motors.unitsToSteps(
                    self.start2 - sign2 * self.stepsize * (self.pixels2 - 1) / 2, self.config2
                )
                self.motors.move_to(self.motor1, startPositionMotor1)
                self.motors.move_to(self.motor2, startPositionMotor2)
                self.waiting((self.motor1, self.motor2))

                if self.stop:
                    return

                j = int(round(self.range1 / (self.stepsize)) / 2)
                ranging2 = range(self.pixels2) if sign2 == 1 else range(self.pixels2 - 1, 1, -1)

                for i in ranging2:
                    try:
                        self.measurePoint(self.measurementTime, i, j)
                    except AssertionError:
                        return
                    self.motors.move_by(self.motor2, sign2 * self.stepwidth2)
                    self.waiting((self.motor2,))
                sign2 *= -1
                self.alignmentEnd.emit("y")

            if not self.measureHor and not self.measureVer:
                # sleep if there is no mode selected
                time.sleep(0.1)
        self.measurement_finished.emit()

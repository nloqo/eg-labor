"""
Main file of the Beamprofiler.

created on 23.11.2020 by Benedikt Moneke

test with command: !py Beamprofiler.pyw -t
"""

# Standard packages.
import datetime
import logging
import time
from typing import Any

# 3rd party.
import pint
from PyQt6 import QtCore, QtGui, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from PyQt6.QtWidgets import QMessageBox
import pyqtgraph as pg
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit

from analysis.math import gaussian
from analysis.beam_profiler import save_image, save_alignment_data
from devices import hid, motors
from pyleco_extras.directors.tmc_motor_director import TMCMotorDirector
from pyleco_extras.gui_utils.base_main_window import start_app, LECOBaseMainWindowDesigner, Path

# Local packages.
from data import Settings, Threading

log = logging.getLogger(__name__)
handler = logging.StreamHandler()
log.addHandler(handler)
logging.getLogger("data.Threading").addHandler(handler)
logging.getLogger("devices").addHandler(handler)

u = pint.UnitRegistry()

# Parameters
gradient_map = "bipolar"


class Beamprofiler(LECOBaseMainWindowDesigner):
    """Analyse a beam profile by moving two motors.

    :param bool remote: Whether to connect to a remote card.
    """

    pgImage: pg.ImageView
    pgPlot: pg.PlotWidget

    actionConnectGamepad: QtGui.QAction
    actionDefaultSettings: QtGui.QAction
    actionConnectMotors: QtGui.QAction
    actionCurrentPosition: QtGui.QAction
    actionSet_fit_as_center: QtGui.QAction

    leVariables: QtWidgets.QLineEdit
    comboImages: QtWidgets.QComboBox

    gbStart: QtWidgets.QGroupBox
    pbStartCuts: QtWidgets.QPushButton
    pbStartGrid: QtWidgets.QPushButton
    pbStartOne: QtWidgets.QPushButton
    pbStartAlignment: QtWidgets.QPushButton
    pbCenterToFit: QtWidgets.QPushButton

    sbStart1: QtWidgets.QDoubleSpinBox
    sbStart2: QtWidgets.QDoubleSpinBox
    sbRange1: QtWidgets.QDoubleSpinBox
    sbRange2: QtWidgets.QDoubleSpinBox
    sbStepsize: QtWidgets.QDoubleSpinBox
    sbTime: QtWidgets.QDoubleSpinBox
    sbNumberMeasurements1: QtWidgets.QDoubleSpinBox
    sbNumberMeasurements2: QtWidgets.QDoubleSpinBox
    cbBigsteps: QtWidgets.QCheckBox
    cbMoveToStart: QtWidgets.QCheckBox

    pbStop: QtWidgets.QPushButton

    lbStart1: QtWidgets.QLabel
    lbStart2: QtWidgets.QLabel
    lbRange1: QtWidgets.QLabel
    lbRange2: QtWidgets.QLabel
    lbStepsize: QtWidgets.QLabel
    lbNumberMeasurements1: QtWidgets.QLabel
    lbNumberMeasurements2: QtWidgets.QLabel
    lbTime: QtWidgets.QLabel
    lbStepsMotor1: QtWidgets.QLabel
    lbStepsMotor2: QtWidgets.QLabel
    lbPositionMotor1: QtWidgets.QLabel
    lbPositionMotor2: QtWidgets.QLabel
    lbError: QtWidgets.QLabel

    cbAutosave: QtWidgets.QCheckBox
    leSavePath: QtWidgets.QLineEdit

    sbController: QtWidgets.QSpinBox
    sbSteps: QtWidgets.QSpinBox

    pbSaveData: QtWidgets.QPushButton
    pbGetPosition: QtWidgets.QPushButton
    pbAlignment: QtWidgets.QPushButton

    alignment_enabled: bool = False

    def __init__(self, name="Beamprofiler", host="localhost", **kwargs):
        super().__init__(
            name=name,
            ui_file_name="Beamprofiler",
            ui_file_path=Path(__file__).parent / "data",
            host=host,
            **kwargs,
        )
        self.progressBar = QtWidgets.QProgressBar(parent=self)
        self.statusBar().addWidget(self.progressBar)  # type: ignore

        self.today = datetime.datetime.now(datetime.timezone.utc).date()
        self.signals = self.Signals()
        # Thread for motor actions
        self.wthread = QtCore.QThread()
        self.wthread.start()
        self.worker = Threading.Worker()
        self.worker.moveToThread(self.wthread)

        # Get settings.
        self.setSettings()

        # Settings for the measurement
        self.sets = (
            # widget, name, value, typ, label
            # Center point.
            (self.sbStart1, "Start1", 2, float, self.lbStart1),
            (self.sbStart2, "Start2", 0, float, self.lbStart2),
            # Ranges.
            (self.sbRange1, "Range1", 1, float, self.lbRange1),
            (self.sbRange2, "Range2", 1, float, self.lbRange2),
            (self.sbStepsize, "Stepsize", 1, float, self.lbStepsize),
            (self.sbTime, "Time", 2, float, self.lbTime),
            (self.sbNumberMeasurements1, "Number1", 2, int, self.lbNumberMeasurements1),
            (self.sbNumberMeasurements2, "Number2", 2, int, self.lbNumberMeasurements2),
        )

        self.keyList = []
        self.restoreConfiguration()
        self.setCurrentSettings()

        self.configureMotor("motor1")
        self.configureMotor("motor2")

        self.setupPlots()

        # Connect actions to slots.

        for setting in self.sets:
            widget, name, value, typ, label = setting
            widget.valueChanged.connect(self.setCurrentSettings)
        self.leVariables.editingFinished.connect(self.updateVariables)

        self.pbStop.clicked.connect(self.stopMotors)

        # Slots activated by Qt.Action
        self.actionConnectGamepad.triggered.connect(self.connectGamepad)
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)
        self.actionDefaultSettings.triggered.connect(self.restoreDefaults)
        self.actionConnectMotors.toggled.connect(self.connectMotors)

        self.actionCurrentPosition.triggered.connect(self.setCurrentPosition)
        self.actionSet_fit_as_center.triggered.connect(self.set_fit_as_center)

        # Slots for adjusting the values on the GUI
        self.sbStart1.valueChanged.connect(self.adjustRange1)
        self.sbStart2.valueChanged.connect(self.adjustRange2)
        self.sbRange1.valueChanged.connect(self.adjustNumberMeasurements1)
        self.sbRange2.valueChanged.connect(self.adjustNumberMeasurements2)
        self.sbStepsize.valueChanged.connect(self.adjustNumberMeasurements)

        # Initialize
        self.adjustNumberMeasurements1()
        self.adjustNumberMeasurements2()
        self.adjustNumberMeasurements()
        self.adjustRange1()
        self.adjustRange2()

        self.signals.setConfigurations.connect(self.worker.setConfigurations)
        # Grid measurement

        self.pbStartCuts.clicked.connect(self.startCuts)
        self.pbStartGrid.clicked.connect(self.startGrid)
        self.pbStartOne.clicked.connect(self.startOneway)
        self.pbStartAlignment.clicked.connect(self.startAlignment)

        self.signals.startMeasurement.connect(self.worker.measure)
        self.signals.startMeasurementCuts.connect(self.worker.cuts)
        self.signals.startMeasurementOneway.connect(self.worker.measureOneway)
        self.signals.startMeasurementAlignment.connect(self.worker.alignment)

        # Move to centerposition
        self.sbStart1.valueChanged.connect(self.moveToCenter)
        self.sbStart2.valueChanged.connect(self.moveToCenter)
        self.signals.moveToCenter.connect(self.worker.moveToCenter)

        # Signals from worker thread
        self.worker.measurement_started.connect(self.set_measurement_gui)
        self.worker.measurement_finished.connect(self.set_measurement_finished_gui)
        self.pbSaveData.clicked.connect(self.saveData)
        self.worker.signalStartMP.connect(self.startMP)
        self.worker.signalStopMP.connect(self.stopMP)

        self.comboImages.currentTextChanged.connect(self.displayImage)
        self.comboImages.currentTextChanged.connect(self.setCurrentImageKey)
        self.setCurrentImageKey(self.comboImages.currentText())

        # Activate big steps
        self.cbBigsteps.stateChanged.connect(self.bigsteps)
        self.pbGetPosition.clicked.connect(self.showPosition)

        self.setSettings()
        self.listener.signals.dataReady.connect(self.setData)
        self.blockGUI(False)
        self.newPicture()

        # new for alignment mode: continuous measurements
        self.pbAlignment.clicked.connect(self.alignmentButtonPressed)

        # Gamepad
        self.gamepad = hid.Gamepad()
        self.gamepad.disconnected.connect(self.gamepadConnectionLost)

        self.updateVariables()
        log.info("BeamProfiler initialized.")

    "# For Setup"

    class Signals(QtCore.QObject):
        """Signals for the beamprofiler.
        helps communicating between different classes"""

        # General signals.
        close = QtCore.pyqtSignal()
        # signals for the worker thread
        starter = QtCore.pyqtSignal()
        startMeasurement = QtCore.pyqtSignal()
        startMeasurementCuts = QtCore.pyqtSignal()
        startMeasurementOneway = QtCore.pyqtSignal()
        startMeasurementAlignment = QtCore.pyqtSignal()
        moveToCenter = QtCore.pyqtSignal()
        setConfigurations = QtCore.pyqtSignal(dict)
        measurementType = QtCore.pyqtSignal(str)
        # variables to transfer to the alignment window.
        params = QtCore.pyqtSignal(dict)
        mpDone = QtCore.pyqtSignal()  # that a measurement point is taken.
        imageKey = QtCore.pyqtSignal(str)

    def setupPlots(self):
        """Configure the plots."""
        self.reference = self.pgPlot.plot([], [])
        self.pgImage.setPredefinedGradient(gradient_map)
        self.pgImage.getImageItem().setOpts(axisOrder="row-major")
        plotItem = self.pgImage.getView()
        plotItem.setLabel("bottom", "relative position", "m")
        plotItem.setLabel("left", "relative position", "m")

    def restoreConfiguration(self):
        """Restore the UI configuration."""
        settings = QtCore.QSettings()
        self.cbAutosave.setChecked(settings.value("autosave", type=bool))
        for widget, name, value, typ, label in self.sets:
            widget.setValue(settings.value(name, value, type=typ))
        self.leVariables.setText(settings.value("variables"))

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing")
        self.stop_listen()
        settings = QtCore.QSettings()
        settings.setValue("autosave", self.cbAutosave.isChecked())
        self.setCurrentSettings()
        self.gamepad.disconnect()
        if self.actionConnectMotors.isChecked():
            self.stopMotors()
            self.wthread.quit()
            if not self.wthread.wait(1000):
                log.debug("Waited too long for thread to quit.")
            self.connectMotors(False)
        # accept the close event (reject it, if you want to do something else)
        event.accept()

    "# Slots for Settings"

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        # open the settings dialogue
        self.settingsDialog = Settings.GeneralSettings()
        self.settingsDialog.accepted.connect(self.setSettings)
        self.settingsDialog.signals.motorConfigured.connect(self.configureMotor)
        self.settingsDialog.open()  # to enable concurrent motor settings

    def setSettings(self):
        """Set variables to values from stored settings."""
        settings = QtCore.QSettings()
        self.sbStart1.setMinimum(settings.value("motorMin", type=float))
        self.sbStart1.setMaximum(settings.value("motorMax", type=float))
        self.sbStart2.setMinimum(settings.value("motorMin", type=float))
        self.sbStart2.setMaximum(settings.value("motorMax", type=float))
        self.deadband = settings.value("deadband", defaultValue=10, type=int)
        self.config1 = settings.value("motor1", type=dict)
        self.config2 = settings.value("motor2", type=dict)
        if self.config1 and self.config2:
            self.motor1 = self.config1["motorNumber"]
            self.motor2 = self.config2["motorNumber"]

    def restoreDefaults(self):
        """Restore the UI to default values of the measurement settings."""
        for widget, name, value, typ, label in self.sets:
            widget.setValue(value)

    def setCurrentSettings(self):
        """Save the values of the measurement settings from the UI."""
        # Save values to settings
        settings = QtCore.QSettings()
        for widget, name, value, typ, label in self.sets:
            settings.setValue(name, float(widget.value()))
        settings.setValue("variables", self.leVariables.text())
        # Read settings into variables
        self.stepsize = float(settings.value("Stepsize"))
        self.range1 = float(settings.value("Range1"))
        self.range2 = float(settings.value("Range2"))
        self.numberMeasurements1 = int(settings.value("Number1"))
        self.numberMeasurements2 = int(settings.value("Number2"))

    "# Motor control"

    def connectMotors(self, checked):
        """Connect the stepmotor card."""
        if checked:
            settings = QtCore.QSettings()
            card_name = settings.value("cardName", type=str)
            if "." in card_name:
                self.motors = TMCMotorDirector(actor=card_name, communicator=self.communicator)
            else:
                if card_name:
                    com = motors.getPort(card_name)
                else:
                    com = settings.value("COM", defaultValue=0, type=int)
                try:
                    self.motorsConnectionManager = ConnectionManager(f"--port COM{com}")
                    self.motors = TMCM6110(self.motorsConnectionManager.connect())
                except Exception as exc:
                    log.exception("Connection to motors failed.", exc_info=exc)
                    self.actionConnectMotors.setChecked(False)
                    self.blockGUI(False)
                    return
            # Connect the gamepad
            self.gamepad.axisChanged.connect(self.onAxisChanged)
            self.gamepad.buttonPressed.connect(self.onButtonPressed)
        else:
            try:
                self.motorsConnectionManager.disconnect()
            except AttributeError:
                pass
            try:
                self.gamepad.axisChanged.disconnect()
                self.gamepad.buttonPressed.disconnect()
            except TypeError:
                pass
        self.blockGUI(checked)

    def configureMotor(self, motorName):
        """Configure the motor `motorname`."""
        log.info("Configure motor.")
        if not self.actionConnectMotors.isChecked():
            return
        settings = QtCore.QSettings()
        config = settings.value(motorName, defaultValue={}, type=dict)
        try:
            # test it with the motor director
            self.motors.configure_motor(config)  # type: ignore
        except AttributeError:
            # the real motor card does not have that method
            try:
                motors.configureMotor(self.motors, config)
            except KeyError:
                pass  # no value
            except Exception:
                log.exception(f"Configure motor {motorName} failed.")

    def moveToCenter(self):
        """Move the motors to the startposition"""
        if self.cbMoveToStart.isChecked():
            configurations = {}
            for setting in self.sets:
                widget, name, value, typ, label = setting
                configurations[name] = widget.value()
            configurations["motors"] = self.motors
            self.signals.setConfigurations.emit(configurations)
            self.signals.moveToCenter.emit()

    def stopMotors(self):
        """Stops the motor movement and exits all methods of the motors."""
        self.worker.stop = True
        time.sleep(0.1)
        try:
            self.motors.connection.reference_search(1, self.motor1)  # type: ignore
            self.motors.connection.reference_search(1, self.motor2)  # type: ignore
        except AttributeError:
            pass
        self.motors.stop(self.motor1)
        self.motors.stop(self.motor2)
        self.updateGUI(False)
        self.progressBar.reset()
        self.progressBar.setMaximum(1)

    def showPosition(self):
        """Show the current motor position."""
        steps1 = self.motors.motors[self.motor1].actual_position
        steps2 = self.motors.motors[self.motor2].actual_position
        self.lbStepsMotor1.setText(str(steps1))
        self.lbStepsMotor2.setText(str(steps2))

        pos1 = motors.stepsToUnits(steps1, self.config1)
        pos2 = motors.stepsToUnits(steps2, self.config2)
        self.lbPositionMotor1.setText(f"{pos1:.6g} mm")
        self.lbPositionMotor2.setText(f"{pos2:.6g} mm")

    "# Measurement configuration."

    def updateVariables(self, text=None):
        """Update the current variables list."""
        self.units = {}
        self.communicator.unsubscribe_all()
        self.keyList = self.leVariables.text().split(" ")
        log.debug(f"New variables list {self.keyList}.")
        self.communicator.subscribe(self.keyList)
        self.comboImages.clear()
        self.tmp = {}
        self.tmpPlot = {}
        for key in self.keyList:
            self.tmpPlot[key] = []
            self.tmp[key] = []
            self.comboImages.addItem(key)
            # self.picture[key] = np.zeros((self.numberMeasurements2,
            #                               self.numberMeasurements1))
            self.picture[key] = np.full(
                [self.numberMeasurements2, self.numberMeasurements1], np.nan
            )

    @pyqtSlot()
    def setCurrentPosition(self):
        """Set the measurement center to the motor positions."""
        pos1 = motors.stepsToUnits(self.motors.motors[self.motor1].actual_position, self.config1)
        self.sbStart1.setValue(pos1)
        pos2 = motors.stepsToUnits(self.motors.motors[self.motor2].actual_position, self.config2)
        self.sbStart2.setValue(pos2)
        self.signals.params.emit({"startHor": pos1, "startVer": pos2})

    @pyqtSlot()
    def set_fit_as_center(self):
        """Set the fit data as measurement center."""
        try:
            pars = self.alignment_window.fit_parameters
        except AttributeError:
            print("not found")
            return  # TODO show error
        phor = pars.get("x")
        if phor is not None:
            self.sbStart1.setValue(phor[1])
        pver = pars.get("y")
        if pver is not None:
            self.sbStart2.setValue(pver[1])
        print(phor, pver)

    "# Measurement"

    def prepare_measurement(self) -> None:
        configurations = {}
        settings = QtCore.QSettings()
        for widget, name, value, typ, label in self.sets:
            label.setText(str(round(settings.value(name, value, type=typ), 3)))
            configurations[name] = widget.value()
        configurations["motors"] = self.motors
        self.signals.setConfigurations.emit(configurations)
        # Prepare the plot
        self.newPicture()
        plotItem = self.pgImage.getView()
        plotItem.getAxis("left").setScale(self.sbStepsize.value() / 1000)
        plotItem.getAxis("bottom").setScale(self.sbStepsize.value() / 1000)
        self.alignment_enabled = False

    @pyqtSlot()
    def startCuts(self) -> None:
        self.prepare_measurement()
        self.signals.startMeasurementCuts.emit()

    @pyqtSlot()
    def startGrid(self) -> None:
        self.prepare_measurement()
        self.signals.startMeasurement.emit()

    @pyqtSlot()
    def startOneway(self) -> None:
        self.prepare_measurement()
        self.signals.startMeasurementOneway.emit()

    @pyqtSlot()
    def startAlignment(self) -> None:
        self.prepare_measurement()
        self.alignment_enabled = True
        self.signals.startMeasurementAlignment.emit()
        self.newAlignmentData()

    def newPicture(self):
        """Define empty pictures for a new measurement"""
        self.picture: dict[str, np.ndarray] = {}
        self.columns = np.zeros(self.numberMeasurements1)
        self.index = np.zeros(self.numberMeasurements2)
        for key in self.keyList:
            self.picture[key] = np.full(
                (self.numberMeasurements2, self.numberMeasurements1), np.nan
            )
        self.picture["Motor1"] = np.full(
            (self.numberMeasurements2, self.numberMeasurements1), np.nan
        )
        self.picture["Motor2"] = np.full(
            (self.numberMeasurements2, self.numberMeasurements1), np.nan
        )

    def newAlignmentData(self):
        """Create new alignment data"""
        self.alignmentData = {"Motor1": [], "Motor2": []}
        for key in self.keyList:
            self.alignmentData[key] = []

    def startMP(self):
        """Empty the temporary data lists for a new measurement point."""
        self.start = time.time()
        for key in self.keyList:
            self.tmp[key] = []
        if "time" in self.keyList:
            self.tmp["time"] = time.time()

    @pyqtSlot(dict)
    def setData(self, data: dict[str, Any]):
        """Store `data` dict in `tmp`"""
        for key, value in data.items():
            # Turn a (magnitude, units) tuple into a Quantity.
            if isinstance(value, tuple):
                try:
                    value = u.Quantity(value[0], u(value[1]))  # type: ignore
                except Exception:
                    log.exception("Error unpacking received data")
            # Get the right value off of the Quantity.
            if isinstance(value, pint.Quantity):
                try:
                    unit = self.units[key]
                except KeyError:
                    self.units[key] = value.units  # type: ignore
                    value = value.magnitude  # type: ignore
                else:
                    try:
                        value = value.m_as(unit)  # type: ignore
                    except pint.DimensionalityError as exc:
                        self.lbError.setText(f"Data error for {key}: {exc}")
                        continue
            try:
                self.tmp[key].append(value)
                self.tmpPlot[key].append(value)
                if len(self.tmpPlot[key]) > 10000:
                    self.tmpPlot[key] = self.tmpPlot[key][-200:]
            except KeyError:
                log.error(f"Got value for {key}, but no list present.")
        self.updatePlot()

    def updatePlot(self):
        """Update the plot."""
        plotLength = QtCore.QSettings().value("plotLength", 100, int)
        try:
            self.reference.setData(self.tmpPlot[self.imageKey][-plotLength:])
        except (KeyError, IndexError):
            pass
        except Exception:
            log.exception("Update plot failed.")

    @pyqtSlot(int, int, list)
    def stopMP(self, i: int, j: int, actualPositionList: list[float]):
        """
        Take the mean value of the temporary list (self.tmp) for each
        variable and safe the value to the picture.
        """
        stop = time.time()
        log.debug(
            f"StopMP {i}, {j}: {actualPositionList}. "
            f"Keylist:{self.keyList}, Time:{stop - self.start}."
        )
        actualPositionMotor1 = actualPositionList[0]
        actualPositionMotor2 = actualPositionList[1]
        self.picture["Motor1"][i, j] = actualPositionMotor1
        self.picture["Motor2"][i, j] = actualPositionMotor2
        meanValues = {}
        self.index[i] = actualPositionMotor2
        self.columns[j] = actualPositionMotor1
        for key in self.keyList:
            meanValues[key] = np.nanmean(self.tmp[key])
            self.picture[key][i, j] = meanValues[key]
        self.displayImage(self.comboImages.currentText())
        self.lbPositionMotor1.setText(f"{actualPositionMotor1:.6g} mm")
        self.lbPositionMotor2.setText(f"{actualPositionMotor2:.6g} mm")

        self.signals.params.emit({"posHor": actualPositionMotor1, "posVer": actualPositionMotor2})
        self.signals.mpDone.emit()
        if self.alignment_enabled:
            self.alignmentData["Motor1"].append(actualPositionMotor1)
            self.alignmentData["Motor2"].append(actualPositionMotor2)
            for key in self.keyList:
                self.alignmentData[key].append(meanValues[key])
        self._step_count += 1
        self.progressBar.setValue(self._step_count)

    def displayImage(self, key: str) -> None:
        """Show the resulting image."""
        try:
            self.pgImage.setImage(self.picture[key], autoRange=False)
        except KeyError or TypeError:
            pass
        except Exception:
            log.exception("Error displaying image.")

    def saveData(self) -> None:
        settings = QtCore.QSettings()
        self.progressBar.hide()
        self.statusBar().showMessage("Saving...")  # type: ignore
        self.leSavePath.setText("Saving...")
        self.pbSaveData.setEnabled(False)

        folder = settings.value("savePath", type=str)

        settingsOfMeasurement = {}
        for setting in self.sets:
            widget, name, value, typ, label = setting
            settingsOfMeasurement[name] = widget.value()
        settingsOfMeasurement["units"] = {key: f"{value:~}" for key, value in self.units.items()}
        # Save the data.
        if self.alignment_enabled:
            filename = save_alignment_data(
                directory=folder,
                configuration=settingsOfMeasurement,
                alignment_data=self.alignmentData,
            )
        else:
            dataToSave: dict[str, pd.DataFrame] = {}
            for key in self.keyList + ["Motor1", "Motor2"]:
                dataToSave[key] = pd.DataFrame(
                    self.picture[key], columns=self.columns, index=self.index
                )
            filename = save_image(
                directory=folder, configuration=settingsOfMeasurement, data=dataToSave
            )
        self.leSavePath.setText(filename)
        clipboard = QtWidgets.QApplication.instance().clipboard()  # type: ignore
        clipboard.setText(filename)
        self.statusBar().showMessage(f"Saved data to '{folder}/{filename}'.", 5000)  # type: ignore

    "# Slots for Gamepad"

    @pyqtSlot(bool)
    def connectGamepad(self, checked):
        """Establish a connection to the gamepad."""
        if checked:
            self.gamepad.connect()
        else:
            self.gamepad.disconnect()

    @pyqtSlot()
    def gamepadConnectionLost(self):
        """Uncheck the connected gamepad button."""
        self.actionConnectGamepad.setChecked(False)

    @pyqtSlot(str, float)
    def onAxisChanged(self, axis, value):
        """Handle the new `value` of `axis`."""
        # Adjust the value according to the rotation speed and rotate the motor
        if axis == "ABS_Y":
            velocity = round(self.getDeadband(value) * self.sbController.value())
            self.motors.rotate(self.motor1, velocity)
        elif axis == "ABS_RY":
            velocity = round(self.getDeadband(value) * self.sbController.value())
            self.motors.rotate(self.motor2, velocity)
        elif axis == "ABS_HAT0Y":
            if value < 0:
                if self.sbSteps.value() == 0:
                    self.sbSteps.setValue(1)
                else:
                    self.sbSteps.setValue(self.sbSteps.value() * 4)
            elif value > 0:
                self.sbSteps.setValue(self.sbSteps.value() // 4)
        elif axis == "ABS_HAT0X":
            if value < 0:
                self.sbController.setValue(self.sbController.value() // 2)
            elif value > 0:
                self.sbController.setValue(self.sbController.value() * 2)

    def getDeadband(self, value):
        """Set the central region to zero, adjust the rest accordingly."""
        value = value / 2**15  # Get a proportional value.
        # Calculate a deadband around the center of the axis.
        db = self.deadband / 100
        if value > db:
            value = (value - db) / (1 - db)
        elif value < -db:
            value = (value + db) / (1 - db)
        else:
            value = 0
        return value

    @pyqtSlot(str)
    def onButtonPressed(self, button):
        """Handle what happens if `button` is pressed."""
        if button == "BTN_EAST":
            self.pbStop.clicked.emit()
        elif button == "BTN_SOUTH":
            pass  # TODO
        elif button == "BTN_NORTH":
            self.setCurrentPosition()

    "# Slots adjusting the UI"

    @pyqtSlot(bool)
    def blockGUI(self, checked):
        """Block the GUI parts, if motors are not connected."""
        self.gbStart.setEnabled(checked)
        if not checked:
            self.cbMoveToStart.setChecked(False)
        self.cbMoveToStart.setEnabled(checked)
        self.pbGetPosition.setEnabled(checked)
        self.actionCurrentPosition.setEnabled(checked)

    def set_measurement_gui(self, step_count: int):
        self.progressBar.show()
        self.progressBar.setValue(0)
        self.progressBar.setMaximum(step_count)
        self._step_count = 0
        self.updateGUI(True)

    def set_measurement_finished_gui(self):
        self.progressBar.setValue(self.progressBar.maximum())
        self.pbSaveData.setEnabled(True)
        self.updateGUI(False)

    def updateGUI(self, running):
        """
        Disable parts of the GUI that must not be clicked when the
        measurement is running.
        """
        enable = not running
        if enable:
            self.gamepad.axisChanged.connect(self.onAxisChanged)
        else:
            try:
                self.gamepad.axisChanged.disconnect()
            except TypeError:
                pass
        self.gbStart.setEnabled(enable)
        self.cbMoveToStart.setEnabled(enable)
        self.actionDefaultSettings.setEnabled(enable)
        self.leVariables.setEnabled(enable)

        if not running and self.cbAutosave.isChecked():
            self.saveData()

    def setCurrentImageKey(self, key):
        """Set the currently shown data to `key`."""
        log.info(f"Current image key {key}.")
        self.signals.imageKey.emit(key)
        self.imageKey = key

    def adjustRange1(self):
        """Adjust the maximum range of measurement for motor 1."""
        distToBoundary = 2 * np.amin(
            [
                self.sbStart1.maximum() - self.sbStart1.value(),
                self.sbStart1.value() - self.sbStart1.minimum(),
            ]
        )
        self.sbRange1.setMaximum(distToBoundary)

    def adjustRange2(self):
        """Adjust the maximum range of measurement for motor 2."""
        distToBoundary = 2 * np.amin(
            [
                self.sbStart2.maximum() - self.sbStart2.value(),
                self.sbStart2.value() - self.sbStart2.minimum(),
            ]
        )
        self.sbRange2.setMaximum(distToBoundary)

    def _rangeToPoints(self, range):
        """Get the number of points for a given range."""
        # Calculate the number of data points on each side of the center.
        # Add both sides plus the center itself.
        return round(range / self.stepsize / 2) * 2 + 1

    def adjustNumberMeasurements1(self):
        """
        Adjust the number of measurement points on change of motor 1 range.
        """
        self.sbNumberMeasurements1.setValue(self._rangeToPoints(self.range1))

    def adjustNumberMeasurements2(self):
        """
        Adjust the number of measurement points on change of motor 2 range.
        """
        self.sbNumberMeasurements2.setValue(self._rangeToPoints(self.range2))

    def adjustNumberMeasurements(self):
        """Adjust the pixelnumbers when the stepsize is changed."""
        self.sbNumberMeasurements1.setValue(self._rangeToPoints(self.range1))
        self.sbNumberMeasurements2.setValue(self._rangeToPoints(self.range2))

    def bigsteps(self):
        """Multiply the singlestep size of the measurement settings
        (except for pixelnumbers(int)) by factor 10"""
        for setting in self.sets:
            widget, name, value, typ, label = setting
            if name == "Time":
                continue
            if typ == int:
                continue
            if self.cbBigsteps.isChecked():
                widget.setSingleStep(widget.singleStep() * 10)
            elif not self.cbBigsteps.isChecked():
                widget.setSingleStep(widget.singleStep() / 10)

    "# Alignment mode widget"

    # ToDo: start continuous measurement along the slices (radio buttons for dimensions)
    # ToDo: print data as it comes, perform Gaussian fit afterwards (steal from BeamCamera),
    #   persistent data points (overwrite data)
    # ToDo: Gui and buttons for different parameters for alignment mode
    #   (lower stepsize, acquisition time)
    # ToDo: (optional) buttons to set new center positions depending on the current peaks
    # ToDo: (optional) move to maximum, e.g., for power alignment

    @pyqtSlot()
    def alignmentButtonPressed(self):
        """opens the alignment window, if the corresponding checkbox is checked, close otherwise"""
        # set measurement mode to alignment:
        self.alignment_enabled = True
        self.actionSet_fit_as_center.setEnabled(True)
        self.pbCenterToFit.setEnabled(True)
        try:
            self.alignment_window.show()
        except AttributeError:
            self.alignment_window = Alignment(parent=self)
            # move alignment window relative to the main window.
            rect = self.alignment_window.geometry()
            rect.moveCenter(self.geometry().center())
            rect.translate(QtCore.QPoint(600, 0))
            self.alignment_window.setGeometry(rect)

            self.alignment_window.imageKey = self.imageKey

            # connect slots
            self.signals.mpDone.connect(self.alignment_window.updatePlot)
            # Todo: check dependencies here...

            self.alignment_window.cbHor.stateChanged.connect(self.worker.setHor)
            self.alignment_window.cbVer.stateChanged.connect(self.worker.setVer)
            self.worker.measurement_finished.connect(self.alignment_window.updateFits)
            self.worker.alignmentEnd.connect(self.alignment_window.updateFit)

            # enable only horizontal alignment mode
            # HACK dirty workaround
            self.alignment_window.cbHor.setChecked(True)
            self.alignment_window.cbVer.setChecked(False)

            self.signals.imageKey.connect(self.alignment_window.setImageKey)
            self.signals.setConfigurations.connect(self.alignment_window.setConfigurations)
            self.alignment_window.show()
            # zeigt den Dialog und führt ihn mit einer eigenen Eventschleife aus.
        self.newAlignmentData()

    "# Auxiliary methods."

    def showInformation(self, title, text, detailedText=None, icon=QMessageBox.Icon.Information):
        """Show some information to the user."""
        self.info = QtWidgets.QMessageBox()
        info = self.info
        info.setIcon(icon)
        info.setWindowTitle(title)
        info.setText(text)
        if detailedText is not None:
            info.setDetailedText(detailedText)
        info.exec()


class Alignment(QtWidgets.QMainWindow):
    """Secondary window to display alignment info and tools."""

    cbHor: QtWidgets.QCheckBox
    cbVer: QtWidgets.QCheckBox
    pgHor: pg.PlotWidget
    pgVer: pg.PlotWidget
    leHor: QtWidgets.QLineEdit
    leVer: QtWidgets.QLineEdit

    fit_parameters: dict[str, np.ndarray] = {}

    def __init__(self, parent: Beamprofiler, **kwargs):
        super().__init__(parent=parent, **kwargs)
        uic.load_ui.loadUi("data/Alignment.ui", self)
        self.profiler = parent
        self.plots = {}
        self.setupPlots()

    @pyqtSlot(dict)
    def setConfigurations(self, configurations):
        """Store the measurement configuration"""
        log.debug("Set config")
        # Center points.
        self.start1 = configurations["Start1"]
        self.start2 = configurations["Start2"]
        # Ranges.
        self.range1 = configurations["Range1"]
        self.range2 = configurations["Range2"]
        self.stepsize = configurations["Stepsize"]
        # Line indices. Middle round is necessary for floating point unprecision.
        self.i = (configurations["Number2"] - 1) // 2
        self.j = (configurations["Number1"] - 1) // 2

    def setupPlots(self):
        """Configure the plots."""
        canvas = [self.pgHor, self.pgVer]
        # Get line references with initial plots
        self.plots["horizontal"] = canvas[0].plot([], [], name="horizontal", pen=None, symbol="o")
        self.plots["horizontalFit"] = canvas[0].plot([], [], name="fit", pen="red")
        canvas[0].setLabel("bottom", "hor. position (mm)")
        canvas[0].setLabel("left", "signal")
        self.plots["vertical"] = canvas[1].plot([], [], name="vertical", pen=None, symbol="o")
        self.plots["verticalFit"] = canvas[1].plot([], [], name="fit", pen="red")
        canvas[1].setLabel("bottom", "ver. position (mm)")
        canvas[1].setLabel("left", "signal")

    @pyqtSlot(str)
    def setImageKey(self, imageKey):
        """Set the key of the currently shown data."""
        self.imageKey = imageKey
        self.updatePlot()
        self.updateFits()

    @pyqtSlot()
    def updatePlot(self):
        """Show the data in the plots."""
        values = self.profiler.picture
        self.plots["horizontal"].setData(values["Motor1"][self.i], values[self.imageKey][self.i])
        self.plots["vertical"].setData(values["Motor2"].T[self.j], values[self.imageKey].T[self.j])

    @pyqtSlot()
    def updateFits(self) -> None:
        """Update both fits if measurement is done."""
        self.updateFit("x")
        self.updateFit("y")

    @pyqtSlot(str)
    def updateFit(self, axis: str) -> None:
        """Calculate and show fits."""
        values = self.profiler.picture
        if axis == "x":
            axisName = "horizontal"
            center = self.start1
            width = self.range1
            le = self.leHor
            x = values["Motor1"][self.i]
            y = values[self.imageKey][self.i]
        else:
            axisName = "vertical"
            center = self.start2
            width = self.range2
            le = self.leVer
            x = values["Motor2"].T[self.j]
            y = values[self.imageKey].T[self.j]
        # gaussian(x, amplitude=1, x0=0, sigma=1, offset=0)
        try:
            par, _ = curve_fit(gaussian, x, y, p0=(max(y), center, width / 4))
        except ValueError:
            pass  # Measurement abort
        except RuntimeError:
            self.plots[f"{axisName}Fit"].setData([], [])
            le.setText("")
            del self.fit_parameters[axis]
        else:
            self.fit_parameters[axis] = par
            raw = np.linspace(center - width / 2, center + width / 2, 100)
            self.plots[f"{axisName}Fit"].setData(raw, gaussian(raw, *par))
            le.setText(f"A= {par[0]:.3f}, c= {par[1]:.3f} mm, w= {2 * abs(par[2]):.3f} mm")


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(Beamprofiler, logger=logging.getLogger())

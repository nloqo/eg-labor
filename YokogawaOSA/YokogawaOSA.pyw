"""
Main file for the Yokogawa OSA program.

created on 28.04.2022 by Benedikt Burger
"""

# Standard packages.
import logging
import time

# 3rd party packages.
import numpy as np
from pymeasure.instruments.yokogawa.aq6370series import AQ6370D
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
    Path,
)

# Local packages.
from data import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class YokogawaOSA(LECOBaseMainWindowDesigner):
    """Define the main window and essential methods of the program."""

    actionAutoAnalysis: QtGui.QAction
    actionConnect: QtGui.QAction
    actionClearCom: QtGui.QAction

    pbScript: QtWidgets.QPushButton
    pbSweep: QtWidgets.QPushButton
    pbSweeping: QtWidgets.QPushButton
    pbStop: QtWidgets.QPushButton
    pbSave: QtWidgets.QPushButton
    pbGetAnalysis: QtWidgets.QPushButton
    gbCommunication: QtWidgets.QGroupBox

    leSend: QtWidgets.QLineEdit
    leQuery: QtWidgets.QLineEdit
    leResponse: QtWidgets.QLineEdit
    teNames: QtWidgets.QTextEdit
    teValues: QtWidgets.QTextEdit

    def __init__(self, name="YokogawaOSA", **kwargs) -> None:
        super().__init__(
            name=name,
            ui_file_name="YokogawaOSA",
            ui_file_path=Path(__file__).parent / "data",
            settings_dialog_class=Settings.Settings,
            **kwargs,
        )

        self.pollingTimer = QtCore.QTimer()
        self.pollingTimer.timeout.connect(self.poll)

        # Get settings.
        self.restoreConfiguration()
        self.setSettings()

        # added by Viktor for automatic switching
        self.pbScript.clicked.connect(self.startScript)
        self.stopVar = False
        self.script = True
        self.values = ""
        self.scriptTimer = QtCore.QTimer()
        self.scriptTimer.timeout.connect(self.script_timeout)
        self.scriptTimer.setSingleShot(True)
        self.timings: list[float] = [100, 100]
        log.info("Yokogawa OSA initialized.")

    def _make_connections(self):
        super()._make_connections()
        # Connect actions to slots.
        self.actionConnect.triggered.connect(self.connect)
        self.actionClearCom.triggered.connect(self.clearCom)
        self.actionAutoAnalysis.triggered.connect(self.autoAnalysis)

        # Communication.
        self.leSend.returnPressed.connect(self.send)
        self.leQuery.returnPressed.connect(self.query)

        # Buttons.
        self.pbSweep.clicked.connect(self.sweep)
        self.pbSweeping.clicked.connect(self.sweeping)
        self.pbStop.clicked.connect(self.stop)
        self.pbGetAnalysis.clicked.connect(self.getAnalysis)
        self.pbSave.clicked.connect(self.save)

    @pyqtSlot()
    def closeEvent(self, event: QtCore.QEvent) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.connect(False)
        # Store UI configuration
        settings = QtCore.QSettings()
        settings.setValue("names", self.teNames.toPlainText())

        super().closeEvent(event)

    def restoreConfiguration(self):
        """Restore the UI configuration from the settings."""
        settings = QtCore.QSettings()
        self.teNames.setPlainText(settings.value("names", type=str))

    def setSettings(self):
        """Set the settings."""
        settings = QtCore.QSettings()
        self.autoClear = settings.value("autoClear", True, type=bool)
        self.pollingTimer.setInterval(settings.value("pollingInterval", 1000, int))

    @pyqtSlot(bool)
    def connect(self, checked):
        """Connect to the OSA."""
        if checked:
            settings = QtCore.QSettings()
            try:
                self.osa = AQ6370D(f"ASRL{settings.value('COM', type=int)}")
            except Exception as exc:
                self.leResponse.setText(str(exc))
                log.exception("Connecting to OSA failed.", exc_info=exc)
                self.actionConnect.setChecked(False)
                checked = False
        else:
            self.pollingTimer.stop()
            try:
                self.osa.shutdown()
                del self.osa
            except AttributeError:
                pass
        self.gbCommunication.setEnabled(checked)

    @pyqtSlot()
    def clearCom(self):
        """Clear the communication."""
        try:
            self.osa.clearCom()
        except AttributeError:
            pass

    @pyqtSlot()
    def sweep(self):
        """Start a sweep."""
        self.osa.initiate_sweep()

    @pyqtSlot()
    def sweeping(self):
        """Start a continuous sweeping measurement."""
        try:
            self.osa.write(":TRACe:attribute:ravg 2")  # rolling average
            self.osa.write(":init:smode repeat;:init")
        except AttributeError:
            pass
        else:
            self.autoAnalysis(True)
            self.actionAutoAnalysis.setChecked(True)

    @pyqtSlot()
    def stop(self):
        """Stop an OSA operation."""
        self.osa.abort()
        self.actionAutoAnalysis.setChecked(False)
        self.scriptTimer.stop()
        self.stopVar = True

    @pyqtSlot(bool)
    def autoAnalysis(self, checked):
        """Activate automatic analysis."""
        if checked:
            self.pollingTimer.start()
        else:
            self.pollingTimer.stop()

    @pyqtSlot()
    def poll(self):
        """Check whether a new sweep is done."""
        try:
            status = self.osa.ask(":STATUS:operation:Event?")
        except AttributeError:
            self.actionAutoAnalysis.setChecked(False)
            self.pollingTimer.stop()
        except Exception as exc:
            self.leResponse.setText(str(exc))
        else:
            if int(status) & 1:
                self.getAnalysis()

    @pyqtSlot()
    def getAnalysis(self):
        """Get the analysis data."""
        try:
            values = self.osa.ask(":calc:Data?")
        except Exception as exc:
            self.leResponse.setText(str(exc))
        else:
            self.teValues.setPlainText(values.replace(",", "\n"))
            names = self.teNames.toPlainText().split("\n")
            values = values.split(",")
            data = {}
            for i in range(min(len(names), len(values))):
                if names[i]:
                    data[names[i]] = float(values[i])
            try:
                self.publisher.send_data(data)
            except Exception as exc:
                log.exception("Publisher error.", exc)

    "Communication"

    @pyqtSlot()
    def send(self):
        """Send a command."""
        try:
            self.osa.write(self.leSend.text())
        except Exception as exc:
            self.leResponse.setText(str(exc))
        else:
            if self.autoClear:
                self.leSend.clear()

    @pyqtSlot()
    def query(self):
        """Send a command."""
        try:
            response = self.osa.ask(self.leQuery.text())
        except Exception as exc:
            self.leResponse.setText(str(exc))
        else:
            self.leResponse.setText(response)
            if self.autoClear:
                self.leQuery.clear()

    @pyqtSlot()
    def save(self):
        """Save the trace A as numpy file."""
        data = {
            "x": self.osa.TRA.get_data("X"),
            "y": self.osa.TRA.get_data("Y"),
        }
        name = round(time.time())
        with open(f"trace_{name}.txt", "w") as file:
            np.savetxt(
                file, np.stack((data["x"], data["y"])).T, header="wavelength (m), peak"
            )

    # added by Viktor
    @pyqtSlot()
    def script_timeout(self):
        """Performs the script of OSA to measure pump wavelength at 1064, search for the signal
        wavelength around an area of 50 nm around 1422 nm and measure it. And then write the
        measured data in the textbox and publish it to the intercom.
        """
        if self.script:
            # Mach den ersten Teil.
            start = time.perf_counter()
            self.osa.write(":SENS:wav:cent 1064nm;:SENS:wav:span 2nm")
            self.osa.sweep()
            # check if sweep is done
            try:
                status = self.osa.ask(":STATUS:operation:Event?")
            except Exception as exc:
                self.leResponse.setText(str(exc))
            else:
                if int(status) & 1:
                    try:
                        self.values = self.osa.ask(":calc:Data?")
                    except Exception as exc:
                        self.leResponse.setText(str(exc))
            # performance analyse
            self.timings[1] = (time.perf_counter() - start) * 1000 - 10
            self.script = False
            self.scriptTimer.start(int(self.timings[0]))
        else:
            # Mach den zweiten Teil.
            start = time.perf_counter()
            self.osa.write(
                ":SENS:wav:cent 1422nm;:SENS:wav:span 50nm;:sens:band:res 0.5nm"
            )  # look for signal peak with low resolution of 0.5nm"
            self.osa.sweep()

            self.osa.write(
                ":SENS:wav:span 1nm;:calc:mark:max:scen;:sens:band:res 0.05nm"
            )  # set center wavelength to peak and resolution back to 0.05nm
            self.osa.sweep()
            # check if sweep is done
            try:
                status = self.osa.ask(":STATUS:operation:Event?")
            except Exception as exc:
                self.leResponse.setText(str(exc))
            else:
                if int(status) & 1:
                    try:
                        self.values = self.values + "," + self.osa.ask(":calc:Data?")
                    except Exception as exc:
                        self.leResponse.setText(str(exc))
                    # try:
                    #    self.values = self.values + "," + self.osa.ask(":calc:Data?")
                    # except Exception as exc:
                    #    self.leResponse.setText(str(exc))
                    else:
                        to_write = self.values.replace(",", "\n")
                        self.teValues.setPlainText(to_write)
                    names = self.teNames.toPlainText().split("\n")
                    values2 = self.values.split(",")
                    data = {}
                    for i in range(min(len(names), len(values2))):
                        if names[i]:
                            data[names[i]] = float(values2[i])
                    try:
                        self.publisher.send_data(data)
                    except Exception as exc:
                        log.exception("Publisher error.", exc)
            # performance analyse
            self.timings[0] = (time.perf_counter() - start) * 1000 - 10
            log.debug(self.timings[0] + self.timings[1])
            self.script = True
            self.scriptTimer.start(int(self.timings[1]))

    @pyqtSlot()
    def startScript(self):
        """ "Start custom Script to measure wavelength of pump and signal beam."""
        # Messung vorbereiten
        self.scriptTimer.start(200)

        # self.scriptThread = QtCore.QThread()
        # self.scriptor = self.Script()
        # self.scriptor.moveToThread(self.scriptThread)
        # self.scriptThread.started.connect(self.scriptor.start)
        # self.scriptor.values_ready.connect(self.setValues)
        # self.scriptor.error.connect(self.showError)
        # self.scriptThread.start()

    # class Script(QtCore.QObject):
    #    values_ready = QtCore.pyqtSignal(str)
    #    error = QtCore.pyqtSignal(Exception)
    #    @pyqtSlot()
    #    def start(self):
    #        try:
    #            while(not self.stopVar):
    #                #print("whilestart")
    #                self.osa.write(":SENS:wav:cent 1064nm;:SENS:wav:span 2nm")
    #                self.osa.sweep()
    #                #time.sleep(1)
    #                #print(self.osa.ask(":calc:Data?"))
    #                print('sweeped!')
    #                try:
    #                    values = self.osa.ask(":calc:Data?")
    #                except Exception as exc:
    #                    self.leResponse.setText(str(exc))
    #                print("not crashed!")
    #                #self.getAnalysis()
    #                #print(values)
    #                self.osa.write(":SENS:wav:cent 1422nm;:SENS:wav:span 50nm;:sens:band:res 0.5nm")
    # look for signal peak with low resolution of 0.5nm"
    #                self.osa.sweep()
    #                self.osa.write(":SENS:wav:span 1nm;:calc:mark:max:scen;:sens:band:res 0.05nm")
    # set center wavelength to peak and resolution back to 0.05nm
    #                self.osa.sweep()
    #                #time.sleep(4)
    #                #self.getAnalysis()
    #                #print(self.osa.ask(":calc:Data?"))
    #
    #                try:
    #                    values1 = values + "," + self.osa.ask(":calc:Data?")
    #                except Exception as exc:
    #                    self.leResponse.setText(str(exc))
    #                else:
    #                    to_write = values1.replace(',', '\n')
    #                    self.teValues.setPlainText(to_write)
    #                    if self.intercomConnected:
    #                        names = self.teNames.toPlainText().split("\n")
    #                        values2 = values1.split(',')
    #                        data = {}
    #                        for i in range(min(len(names), len(values2))):
    #                            if names[i]:
    #                                data[names[i]] = float(values2[i])
    #                        try:
    #                            self.intercom.sendObject('SET', data)
    #                        except (ConnectionRefusedError, intercom.timeout):
    #                            self.intercomConnected = False
    #                            self.intercomReconnectTimer.start()
    #            self.stopVar = False
    #
    #        except Exception as exc:
    #            self.error.emit(exc)
    #            #self.leResponse.setText(str(exc))


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(main_window_class=YokogawaOSA)

"""
Main file for reading an Arduino. It shows the raw data read.

Make a subclass to interpret the data.

created on 24.06.2022 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from pymeasure.instruments.resources import find_serial_port

from pyleco.utils.data_publisher import DataPublisher

from devices import arduino
from devices.gui_utils import start_app

# Local packages.
from data import Settings


log = logging.getLogger(__name__)  # get the logger for this file.
log.addHandler(logging.NullHandler())  # Damit kein Fehler geworfen


class ArduinoReader(QtWidgets.QMainWindow):
    """
    Define the main window and essential methods of the program.

    Parameters
    ----------
    name
        Name of the program.
    ui : str, optional
        User interface file name if different from the base one.
    """

    def __init__(self, name="ArduinoReader", ui=None, host="localhost", *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi(ui or "data/ArduinoReader.ui", self)
        self.setWindowTitle(name)
        self.show()

        # For regular readout.
        self.pollingTimer = QtCore.QTimer()
        self.pollingTimer.timeout.connect(self.readout)

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName(name)
        # settings = QtCore.QSettings()
        self.restoreConfiguration()
        self.publisher = DataPublisher(full_name=name, host=host)
        self.setSettings()

        # Communication.
        self.leSend.returnPressed.connect(self.send)
        self.leQuery.returnPressed.connect(self.query)

        # Connect actions to slots.
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)
        self.actionConnect.triggered.connect(self.connect)
        log.info(f"{name} Initialized.")

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing")
        self.connect(False)
        # Store UI configuration
        settings = QtCore.QSettings()
        settings.setValue('names', self.teNames.toPlainText())

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    def restoreConfiguration(self):
        """Restore the UI configuration from the settings."""
        settings = QtCore.QSettings()
        self.teNames.setPlainText(settings.value('names', type=str))

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        settingsDialog = Settings.Settings()
        if settingsDialog.exec():
            self.setSettings()

    def setSettings(self):
        """Set the settings."""
        settings = QtCore.QSettings()
        self.pollingTimer.setInterval(settings.value('pollingInterval',
                                                     1000, int))
        try:
            self.arduino.averaging = settings.value('averaging', 50, int)
        except AttributeError:
            pass

    def get_com_port(self) -> str:
        """Get the com port of the device to connect to. Override in subclass."""
        settings = QtCore.QSettings()
        com = settings.value('COM', type=int)
        if com == 0:
            port = find_serial_port(
                settings.value('vendorID', type=int),
                settings.value('productID', type=int),
                settings.value('serialNumber', type=str),
            )
            self.statusBar().showMessage(f"Found port {port}", 10000)
            return port
        else:
            return f"ASRL{com}"

    @pyqtSlot(bool)
    def connect(self, checked):
        """Connect to the Arduino."""
        if checked:
            log.info("Connecting")
            settings = QtCore.QSettings()
            try:
                self.arduino = arduino.Arduino(self.get_com_port(),
                                               visa_library=settings.value('visa', "@py"))
            except Exception as exc:
                log.exception("Connection failed.", exc_info=exc)
                self.teValues.setPlainText("Connection failed\n" + str(exc))
                self.actionConnect.setChecked(False)
                return
            try:
                self.arduino.ping()
            except Exception as exc:
                log.exception("Ping failed.", exc_info=exc)
                self.teValues.setPlainText("Ping failed\n" + str(exc))
                self.actionConnect.setChecked(False)
                return
            try:
                self.arduino.averaging = settings.value('averaging', 50, int)
            except Exception as exc:
                log.exception("Averaging failed.", exc_info=exc)
                self.teValues.setPlainText("Configuration failed\n" + str(exc))
                self.actionConnect.setChecked(False)
                return
            else:
                self.pollingTimer.start()
        else:
            log.info("Disconnecting")
            self.pollingTimer.stop()
            try:
                self.arduino.close()
                del self.arduino
            except AttributeError:
                pass
        self.gbCommunication.setEnabled(checked)

    def handle_volts(self, volts):
        """
        Transform and show the voltages

        Change it in subclasses.
        """
        values = volts
        self.teValues.setPlainText("\n".join(str(v) for v in values))
        return values

    @pyqtSlot()
    def readout(self):
        """Read the Arduino values."""
        try:
            # Get relative volts.
            volts = self.arduino.data
        except Exception as exc:
            log.exception("Readout failed.", exc_info=exc)
            self.teValues.setPlainText(str(exc))
            return
        try:
            values = self.handle_volts(volts)
        except Exception as exc:
            log.exception("Handle volts failed.", exc_info=exc)
            self.teValues.setPlainText(str(exc))
        else:
            names = self.teNames.toPlainText().split("\n")
            data = {}
            for i in range(min(len(names), len(values))):
                if names[i]:
                    data[names[i]] = values[i]
            log.debug(data)
            try:
                self.publisher.send_legacy(data)
            except Exception as exc:
                log.exception("Publisher error.", exc_info=exc)

    "Communication with Arduino"
    @pyqtSlot()
    def send(self):
        """Send a command."""
        try:
            self.arduino.write(self.leSend.text())
        except Exception as exc:
            self.leResponse.setText(str(exc))
        # else:
        #     if self.autoClear:
        #         self.leSend.clear()

    @pyqtSlot()
    def query(self):
        """Send a command."""
        try:
            response = self.arduino.ask(self.leQuery.text())
        except Exception as exc:
            self.leResponse.setText(str(exc))
        else:
            self.leResponse.setText(response)
            # if self.autoClear:
            #     self.leQuery.clear()


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    log.addHandler(logging.StreamHandler())
    start_app(ArduinoReader)

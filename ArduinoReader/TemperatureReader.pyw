"""
Main file for the Temperature Reader program.

created on 28.04.2022 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtGui

from devices.tdk_ntc import ntc_sh
from devices.gui_utils import start_app

# Local packages.
from ArduinoReader import ArduinoReader

logging.getLogger("PyQt6").setLevel(logging.INFO)
log = logging.getLogger(__name__)
gLog = logging.getLogger()
gLog.addHandler(logging.StreamHandler())


class TemperatureReader(ArduinoReader):
    """Read temperatures with an arduino."""

    def __init__(self, name="TemperatureReader", *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(name=name, *args, **kwargs)
        self.setWindowIcon(QtGui.QIcon("data/thermometer.ico"))

        self.converter = ntc_sh.NTC_SH(((10, 19900), (30, 8057), (60, 2488)))

    def handle_volts(self, volts):
        """Turn the values into temperatures in °C."""
        try:
            values = [self.converter.temperature(1e4 * v / (1 - v)) for v in volts]
        except ValueError as exc:
            log.exception("Value error on temperature calculation.", exc)
            self.teValues.setPlainText(f"Value Error {exc}")
            return []
        else:
            text = "°C\n".join(f"{value:7.3f}" for value in values) + "°C"
            self.teValues.setPlainText(text)
            return values


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(main_window_class=TemperatureReader, logger=gLog)

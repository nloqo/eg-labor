# -*- coding: utf-8 -*-
"""
Start another window of the Temperature Reader

Created on Mon Jun 27 12:58:10 2022

@author: THG-User
"""

import logging
import sys

from PyQt6 import QtWidgets

from TemperatureReader import TemperatureReader, gLog

if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:
        gLog.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = TemperatureReader(name="TemperatureReader2")
    app.exec()  # start the application with its Event loop

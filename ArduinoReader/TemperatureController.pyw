"""
Main file for the TemperatureController Reader program.

created on 24.06.2022 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtGui

from devices.gui_utils import start_app
from devices.tdk_ntc import ntc_sh

# Local packages.
from ArduinoReader import ArduinoReader


logging.getLogger("PyQt6").setLevel(logging.INFO)
log = logging.getLogger(__name__)
gLog = logging.getLogger()
gLog.addHandler(logging.StreamHandler())


class TemperatureController(ArduinoReader):
    """Control the OPO block temperature and read some temperatures."""

    def __init__(self, *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(name="TemperatureControllerA", *args, **kwargs)
        self.setWindowIcon(QtGui.QIcon("data/thermometer.ico"))
        tt = self.leQuery.toolTip()
        # ToolTip corresponds to TemperatureController 1.0.1
        self.leQuery.setToolTip("<br>".join((
            tt,
            "Commands:",
            "'k' sets a parameter: 'p', 'i', 'x' integral value, 's' setpoint in internal values.",
            "'d' requests current PID state.",
            "'e' stores the setpoint, P, I, and current integral in EEPROM.",
        )))
        self.converter = ntc_sh.NTC_SH(((10, 19900), (30, 8057), (60, 2488)))

    def handle_volts(self, volts):
        """Turn the values into temperatures in °C."""
        try:
            values = [self.converter.temperature(1e4 * v / (1 - v)) for v in volts[:7]]
        except ValueError as exc:
            log.exception("Value error on temperature calculation.", exc)
            self.teValues.setPlainText(f"Value Error {exc}")
            return []
        try:
            values.append(1064 * volts[7])
        except IndexError:
            pass  # not enough data received
        text = "°C\n".join(f"{value:7.3f}" for value in values[:7]) + "°C"
        try:
            text += f"\n{values[7]:.4}%"
        except IndexError:
            pass
        self.teValues.setPlainText(text)
        return values

    def readout_pid(self):
        """Read the PID parameters."""
        try:
            response = self.arduino.ask("d")
            integral = float(response.split(",")[0])
        except Exception as exc:
            self.leResponse.setText(str(exc))
        else:
            self.publisher.send_legacy({'OPOBlockIntegral': integral})
            self.teValues.setPlainText(self.teValues.toPlainText() + f"\n{integral}‰")

    def readout(self):
        """Read the sensor values and PID parameters."""
        super().readout()
        self.readout_pid()


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(TemperatureController, logger=gLog)

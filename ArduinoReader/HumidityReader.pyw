"""
Main file for the Humidty Reader program.

created on 24.06.2022 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot

from devices.gui_utils import parse_command_line_parameters
from devices.tdk_ntc import ntc_sh

# Local packages.
from ArduinoReader import ArduinoReader


logging.getLogger("PyQt6").setLevel(logging.INFO)
log = logging.getLogger(__name__)
gLog = logging.getLogger()
gLog.addHandler(logging.StreamHandler())


hwinfo = (0x2341, 0x003D, "753303039343517022B0")


class HumidityReader(ArduinoReader):
    """Read the humidity."""

    def __init__(self, name="HumidityReader", *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(name=name, *args, **kwargs)
        self.setWindowIcon(QtGui.QIcon("data/thermometer.ico"))

        self.converter = ntc_sh.NTC_SH(((10, 19900), (30, 8057), (60, 2488)))

    @pyqtSlot()
    def readout(self):
        """Read the Arduino values."""
        try:
            # Get relative volts.
            text = self.arduino.ask('y').split('\t')
        except Exception as exc:
            log.exception("Readout failed.", exc_info=exc)
            self.teValues.setPlainText("VISA\n" + str(exc))
        else:
            self.teValues.setPlainText(f"{text[0]:.4} °C\n{text[1]:.4}%")
            # Convert input voltage fraction to resistance and to temperature.
            names = self.teNames.toPlainText().split("\n")
            data = {}
            for i in range(min(len(names), len(text))):
                if names[i]:
                    data[names[i]] = float(text[i])
            try:
                self.publisher.send_legacy(data)
            except Exception as exc:
                log.exception("Publisher error.", exc)


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    kwargs = parse_command_line_parameters(
        logger=gLog,
        description=HumidityReader.__doc__.split(":param", maxsplit=1)[0],
    )
    app = QtWidgets.QApplication([])  # create an application
    mainwindow = HumidityReader(kwargs)  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

# -*- coding: utf-8 -*-

"""
Main file for the Temperature Reader program.

created on 28.04.2022 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtWidgets

from devices.gui_utils import parse_command_line_parameters

# Local packages.
from ArduinoReader import ArduinoReader


logging.getLogger("PyQt6").setLevel(logging.INFO)
log = logging.getLogger(__name__)
gLog = logging.getLogger()
gLog.addHandler(logging.StreamHandler())


class TemperatureReader(ArduinoReader):
    """Define the main window and essential methods of the program."""

    def __init__(self, name="TemperatureReaderChiller", *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(name=name, *args, **kwargs)

    def handle_volts(self, volts):
        """Turn the values into temperatures in °C."""
        volts = self.arduino.data_raw

        text = ""
        for value in volts:
            if (volts.index(value) == 1):
                text += f"{value:7.3f} °C\n"
            else:
                text += f"{value:7.3f} l/min\n"
        self.teValues.setPlainText(text)
        return volts


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    kwargs = parse_command_line_parameters(logger=gLog)
    app = QtWidgets.QApplication([])  # create an application
    mainwindow = TemperatureReader()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

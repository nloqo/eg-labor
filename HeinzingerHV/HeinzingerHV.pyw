"""
Main file of the Heinzinger Control.

created on 23.11.2020 by Benedikt Burger
"""

# Standard packages.
import logging
from typing import cast

# 3rd party
import numpy as np
from pyleco.directors.transparent_director import TransparentDirector
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot

import nidaqmx

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    Path,
    start_app,
)

# Local packages.
from data import Settings

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


ACTOR = "MYRES.nicard"


def signal_to_HV(signal: float) -> float:
    """Calculate the HV output in Volts depending on the measured signal in Volts."""
    return (signal + 0.035127) / 0.0016706


def HV_to_control(high_voltage: float) -> float:
    """Calculate the necessary control voltage in Volts to generate HV output in Volts."""
    return high_voltage * 0.0016706 - 0.035127


class Heinzinger_voltage(LECOBaseMainWindowDesigner):
    """Define the main window and essential methods of the program."""

    lbMeasurement: QtWidgets.QLabel
    lbVoltage: QtWidgets.QLabel
    lbSet_voltage: QtWidgets.QLabel
    sbSetVoltage: QtWidgets.QDoubleSpinBox
    pbSet: QtWidgets.QPushButton
    lbFactor: QtWidgets.QLabel
    lbStartingVoltage: QtWidgets.QLabel
    bbFactor: QtWidgets.QComboBox
    sbStartingVoltage: QtWidgets.QDoubleSpinBox
    actionHeinzingerConnect: QtGui.QAction
    actionAnpassen: QtGui.QAction
    actionSetLow: QtGui.QAction
    actionSetHigh: QtGui.QAction
    actionPublish: QtGui.QAction
    actionClose: QtGui.QAction
    actionSettings: QtGui.QAction

    local = False

    def __init__(self, name="HeinzingerHV", **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(
            name=name,
            settings_dialog_class=Settings.Settings,
            ui_file_name="HeinzingerHV",
            ui_file_path=Path(__file__).parent / "data",
            **kwargs,
        )

        # Timers etc.
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.readout)

        self.setSettings()
        self.director = TransparentDirector(actor=ACTOR, communicator=self.communicator)

        # Try to connect at startup.
        if True:
            self.actionHeinzingerConnect.setChecked(True)
            self.connectHeinzinger(True)

        log.info("HeinzingerHV initialized.")

    def _make_connections(self) -> None:
        super()._make_connections()
        self.actionHeinzingerConnect.triggered.connect(self.connectHeinzinger)
        # Value in Spinbox changed
        self.pbSet.clicked.connect(self.setVoltagePressed)

        # Factor Combobox
        self.bbFactor.addItems(["1", "0.5", "0.3", "0.2", "0.1", "0.05", "0.02"])
        self.sbStartingVoltage.valueChanged.connect(self.startChanged)
        self.bbFactor.currentIndexChanged.connect(self.factorChanged)
        self.actionAnpassen.triggered.connect(self.anpassenGedrueckt)

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        self.connectHeinzinger(False)
        # accept the close event (reject it, if you want to do something else)
        super().closeEvent(event)

    def setSettings(self) -> None:
        """Apply some settings."""
        settings = QtCore.QSettings()
        self.averaging = settings.value("averaging", defaultValue=1, type=int)
        self.timer.setInterval(settings.value("interval", 250, type=int))

    @pyqtSlot(bool)
    def connectHeinzinger(self, checked: bool) -> None:
        self.connectHeinzinger_remotely(checked=checked)

    @pyqtSlot(bool)
    def connectHeinzinger_locally(self, checked: bool) -> None:
        """Connect Heinzinger at the NI card."""
        if checked:
            try:
                self.niTask = niTask = nidaqmx.Task()
                niTask.ai_channels.add_ai_voltage_chan("Dev1/ai7")
            except Exception as exc:
                log.exception("Connection to device failed.", exc)
                self.actionHeinzingerConnect.setChecked(False)
            else:
                self.local = True
                self.timer.start()
        else:
            self.timer.stop()
            try:
                self.niTask.close()
            except Exception as exc:
                log.exception("Closing connection failed.", exc)

    @pyqtSlot(bool)
    def connectHeinzinger_remotely(self, checked: bool) -> None:
        """Connect Heinzinger at the NI card via LECO."""
        if checked:
            self.local = False
            self.timer.start()
        else:
            self.timer.stop()

    def _read_locally(self) -> float:
        try:
            voltage = self.niTask.read(number_of_samples_per_channel=self.averaging)
        except nidaqmx.DaqError:
            raise ValueError
        else:
            return signal_to_HV(np.mean(voltage))

    def _read_remotely(self) -> float:
        return cast(float, self.director.device.heinzinger)  # type: ignore

    @pyqtSlot()
    def readout(self):
        """Read the voltage and show it. Publish it, if checked."""
        data = {}
        if self.actionHeinzingerConnect.isChecked():
            try:
                if self.local:
                    voltage = self._read_locally()
                else:
                    voltage = self._read_remotely()
            except ValueError:
                pass
            else:
                self.lbVoltage.setText(f"{voltage:g} V")
                data["highVoltage"] = voltage
                self.publisher.send_data(data)

    @pyqtSlot()
    def setVoltagePressed(self):
        self.setVoltage(self.sbSetVoltage.value())

    def setVoltage(self, voltage: float) -> None:
        """Set the output such, that the HV outputs `voltage` in V."""
        if self.actionHeinzingerConnect.isChecked():
            if self.local:
                self._set_voltage_locally(voltage=voltage)
            else:
                self._set_voltage_remotely(voltage=voltage)

    def _set_voltage_locally(self, voltage: float) -> None:
        """Set the voltage locally."""
        with nidaqmx.Task() as niTask_w:
            niTask_w.ao_channels.add_ao_voltage_chan("Dev1/ao0")
            try:
                niTask_w.write(HV_to_control(voltage))
            except nidaqmx.DaqError as exc:
                messagebox = QtWidgets.QMessageBox(
                    QtWidgets.QMessageBox.Icon.Warning,
                    "ERROR",
                    f"Writing failed:  {exc}",
                )
                messagebox.exec()

    def _set_voltage_remotely(self, voltage: float) -> None:
        self.director.device.heinzinger = voltage  # type: ignore

    @pyqtSlot(bool)
    def anpassenGedrueckt(self, checked: bool) -> None:
        """Sets the default value of the starting value."""
        if checked:
            self.sbStartingVoltage.setValue(self.sbSetVoltage.value())

    def factorChanged(self, index):
        self.changeVoltage()

    def startChanged(self, value):
        self.changeVoltage()

    def changeVoltage(self) -> None:
        """Calculates the new voltage for the chosen factor and sends it to the HV supplier."""
        f0 = (
            0.3 / (1 + np.exp(-3.0957e-2 * 0.3 * 68.36) * (0.3 / 0.11213 - 1)) + 7.09e-4
        )
        new = (
            -np.log(
                (0.3 * (float(self.bbFactor.currentText()) * f0 - 7.09e-4) ** (-1) - 1)
                / (0.3 / 0.11213 - 1)
            )
            / (0.3 * 3.0957e-2)
            - 68.36
            + self.sbStartingVoltage.value()
        )
        self.setVoltage(new)
        self.sbSetVoltage.setValue(new)


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(Heinzinger_voltage)

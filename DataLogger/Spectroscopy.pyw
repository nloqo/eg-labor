# -*- coding: utf-8 -*-
"""
Adaptation of the DataLogger to spectroscopy.

Created on Mon Aug 16 11:44:44 2021 by Benedikt Burger
"""

import logging


from pyleco_extras.gui.data_logger.data_logger import DataLoggerGUI, start_app

from devices.pyleco_addons.spectroscopy_mixin import SpectroscopyMixin

log = logging.Logger(__name__)
log.addHandler(logging.StreamHandler())


class Spectroscopy(SpectroscopyMixin, DataLoggerGUI):
    """Adaptation of the DataLogger to spectroscopy."""

    def __init__(self, name="Spectroscopy", **kwargs):
        super().__init__(name=name, **kwargs)
        self.listener.signals.started.connect(self.setup_additional_lists)


if __name__ == '__main__':
    logging.getLogger("pyleco").addHandler(logging.StreamHandler())
    start_app(main_window_class=Spectroscopy, logger=logging.getLogger())

# -*- coding: utf-8 -*-
"""
Adaptation of the DataLogger to spectroscopy.

Created on Mon Aug 16 11:44:44 2021 by Benedikt Burger
"""

import logging
import numpy as np

from pyleco_extras.gui.data_logger.data_logger_viewer import DataLoggerViewer, start_app

from devices.pyleco_addons.spectroscopy_mixin import SpectroscopyMixin, wlc_offset

log = logging.Logger(__name__)
log.addHandler(logging.StreamHandler())


class SpectroscopyViewer(SpectroscopyMixin, DataLoggerViewer):
    """Adaptation of the DataLoggerViewer to spectroscopy."""

    _additional_lists: dict[str, list[float] | np.ndarray]

    def __init__(self, name="SpectroscopyViewer", **kwargs):
        super().__init__(name=name, **kwargs)

    def start(self) -> None:
        super().start()
        self.setup_additional_lists()
        self.signals.started.emit()
        self.signals.update_plots.emit()

    def setup_additional_lists(self) -> None:
        self._additional_lists = {}

        try:
            signal_wl = np.array(self.get_data("signal_wl"))
        except (KeyError, RuntimeError):
            return
        try:
            pump_wl = np.array(self.get_data("pump_wl"))
        except KeyError:
            pass
        else:
            self._additional_lists["idler_wl"] = 1 / (1 / pump_wl - 1 / signal_wl)
        try:
            pump_wlc = np.array(self.get_data("seedWavelengthCalculated"))
        except KeyError:
            pass
        else:
            self._additional_lists["idler_wlc"] = 1 / (
                1 / (pump_wlc + wlc_offset) - 1 / signal_wl
            )


if __name__ == '__main__':
    start_app(main_window_class=SpectroscopyViewer, logger=logging.getLogger())

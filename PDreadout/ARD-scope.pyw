"""
Main file of the ARD-scope program.

created on 12.08.2024 by Leif Pfannekuch
"""

# Standard packages.
import logging
import time
from typing import Any, Optional

# 3rd party
import numpy as np
# from picoscope import picobase, ps4000a, ps5000a, ps6000a
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot
from PyQt6.QtWidgets import QMessageBox
from devices import arduino

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
    Path,
)

# Local packages.
from data import Settings, calibration
from data.Arduino_Settings import Arduino_Settings

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())

Resolutions = {
    "8bit": 8,
    "10bit": 10,
    "12bit": 12,
    "14bit": 14,
    "16bit": 16,   
}

channelcount = 3


class ARD_scope(LECOBaseMainWindowDesigner):
    """Control ARD-scope and reads data"""
    actionADCset_AC: QtGui.QAction

    actionConnect_AC: QtGui.QAction
    actionReconnect_AC: QtGui.QAction
    actionInfo: QtGui.QAction
    actionMeasure: QtGui.QAction
    actionChA: QtGui.QAction
    actionChB: QtGui.QAction
    actionChC: QtGui.QAction

    cbRestart: QtWidgets.QCheckBox

    actionSettings: QtGui.QAction

    lbDisplay: QtWidgets.QLabel
    lbVariable: QtWidgets.QLabel
    lbOverrange: QtWidgets.QLabel

    def __init__(self, name="ARD_scope", **kwargs):
        super().__init__(
            name=name,
            ui_file_name="ARD_scope",
            ui_file_path=Path(__file__).parent / "data",
            settings_dialog_class=Settings.Arduino_gen_Settings,
            **kwargs,
        )

        self.signals = self.ArduinoSignals()
        self.Ard_AC_signal = arduino.Arduino("COM73", timeout=5000, baud_rate=921600)
        self.arduinoSettings: dict[str, Arduino_Settings] = {}
        self.name = name

        # Get settings.
        settings = QtCore.QSettings()

        # Set settings.
        self.setSettings()
        self.starttime = time.time()
        self.timeTake = 0
        self.dataTimer = QtCore.QTimer()
        self.dataTimer.timeout.connect(self.runBlock)
        self.kleiner9 = 0
        # Connect actions to slots.
        # Connection
        self.actionConnect_AC.triggered.connect(self.connect_AC)
        # self.actionReconnect_AC.triggered.connect(self.reconnect_AC)
        # self.actionInfo.triggered.connect(self.showInfo)

        self.actionMeasure.triggered.connect(self.startMeasurement)

        self.actionChA.triggered.connect(self.readoutA)
        self.actionChB.triggered.connect(self.readoutB)
        self.actionChC.triggered.connect(self.readoutC)

        self.actionADCset_AC.triggered.connect(self.openArduinoSettings_AC)   # !!!!!!!!!!!!
        # Signals
        # self.signals.blockReady.connect(self.transferData)
        self.signals.runBlock.connect(self.runBlock)

        self.cbRestart.stateChanged.connect(self.restart) # !!!!!!!!!!

        # Select the last active channels at startup
        self.channelsToConfigure = {}
        self.listOfChannelActions = [
            self.actionChA,
            self.actionChB,
            self.actionChC,
        ]
        self.activeChannels: dict[str, bool] = {}
        self.data: dict[str, int] = {}
        self.dataArray: dict[str,  list | np.ndarray] = {}
        for i in range(3):
            self.data[chr(i + 65)] = 0
            self.dataArray[chr(i + 65)] = []
            if settings.value(f"ch{chr(i + 65)}/enabled", type=bool):
                self.listOfChannelActions[i].setChecked(True)
                self.activeChannels[chr(i + 65)] = True

        # For counting the measurement interval
        self.last = time.perf_counter()
        log.info(f"{name} initialized.")

    class ArduinoSignals(QtCore.QObject):
        """Signals from the Arduino."""

        close = QtCore.pyqtSignal()
        blockReady = QtCore.pyqtSignal(int)
        runBlock = QtCore.pyqtSignal()
        channelUpdatedAC = QtCore.pyqtSignal(bool)
        # channelUpdatedB = QtCore.pyqtSignal(bool)
        # channelUpdatedC = QtCore.pyqtSignal(bool)
        samplesChanged = QtCore.pyqtSignal(int)

        def __init__(self):
                """Aggregate signals into a dictionary."""
                super().__init__()
                self.channelUpdated = {
                    "AC": self.channelUpdatedAC,
                    # "B": self.channelUpdatedB,
                    # "C": self.channelUpdatedC,
                }

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.actionConnect_AC(False)
        self.stop_listen()
        # accept the close event (reject it, if you want to do something else)
        event.accept()
        self.stop_listen()  # ?
        # accept the close event (reject it, if you want to do something else)
        event.accept()

    @pyqtSlot()
    def openSettings(self) -> None:
        """Open the settings dialogue and apply changed settings."""
        settingsWindow = Settings.Arduino_gen_Settings(Resolutions.keys(), main=self)
        if settingsWindow.exec():
            self.setSettingsExt()

    def setSettings(self) -> None:
        """Set the general settings."""
        settings = QtCore.QSettings()
        ClockSpeed = settings.value("ClockSpeed", type=int)
        self.setWindowTitle(f"{self.name}")
        Res = settings.value("Resolution", "16bit", type=str)
        StringSt = "c"
        StringSt = StringSt + str(Res[0:2]) + ";"
        if len(str(ClockSpeed)) < 6:
            for i in range(len(str(ClockSpeed)), 6):
                StringSt = StringSt + "0"
            StringSt = StringSt + str(ClockSpeed/1000)
        # printStr = self.device_AC.ask(StringSt)
        # print(printStr, StringSt)

        # fehlt vielleicht noch was#
    def setSettingsExt(self) -> None:
        settings = QtCore.QSettings()
        ClockSpeed = settings.value("ClockSpeed", type=int)
        self.setWindowTitle(f"{self.name}")
        Res = settings.value("Resolution", "16bit", type=str)
        StringSt = "c"
        StringSt = StringSt + str(Res[0:2]) + ";"
        if len(str(ClockSpeed)) < 6:
            for i in range(len(str(ClockSpeed)), 6):
                StringSt = StringSt + "0"
            StringSt = StringSt + str(ClockSpeed/1000)
        printStr = self.device_AC.ask(StringSt)
        print(printStr, StringSt)

    def configureEverything(self) -> None:
        """Set the flags in order to configure the scope and all channels."""
        self.channelsToConfigure = {}
        for i in range(channelcount):
            self.channelsToConfigure[chr(65 + i)] = True
            self.listOfChannelActions[i].setVisible(True)

    def setChannel(self, channel) -> None:  # pico scoep ish
        """Configure a channel."""
        settings = QtCore.QSettings()
        settings.beginGroup(f"ch{channel}")
        enabled = settings.value("enabled", type=bool)
        data = {
            "ADCs": settings.value("ADCs", "1 & 2 & 3", type=str),
            "Resolution": self.ResStrToInt(settings.value("Resolution", "16bit", type=str)),
            "enabled": enabled,
        }
        data

    def ResStrToInt(self, string:str) -> int:
        return int(string.replace("bit", ""))

    @pyqtSlot(str)
    def channelConfigured(self, channel: str) -> None:
        """Set `channel` to be configured."""
        self.channelsToConfigure[channel] = True

    @pyqtSlot(bool)
    def connect_AC(self, checked: bool):
        if checked:
            self.device_AC = arduino.Arduino("COM73", timeout=5000, baud_rate=921600)
            self.actionMeasure.setChecked(True)
            self.startMeasurement(True)
        else:
            self.device_AC.close()
            self.device_AC = 0
            self.startMeasurement(False)
            self.actionMeasure.setChecked(False)

    # @pyqtSlot()
    # def reconnect(self) -> None:
    #     self.connectPico(False)
    #     self.actionMeasure.setChecked(True)
    #     self.connectPico(True)

    @pyqtSlot(bool)
    def startMeasurement(self, checked: bool) -> None:
        """Start/stop measureing."""
        if checked:
            self.dataTimer.start(5)
        else:
            self.dataTimer.stop()

    def readout(self, checked: bool, channel: str)-> None:
        settings = QtCore.QSettings()
        if checked:
            settings.setValue(f"ch{channel}/enabled", checked)
            self.channelsToConfigure[channel] = True

    @pyqtSlot(bool)
    def readoutA(self, checked: bool):
        """Configure Channel A."""
        self.readout(checked, "A")

    @pyqtSlot(bool)
    def readoutB(self, checked: bool):
        """Configure Channel B."""
        self.readout(checked, "B")

    @pyqtSlot(bool)
    def readoutC(self, checked: bool):
        """Configure Channel C."""
        self.readout(checked, "C")

    def runBlock(self) -> None:
        # Gets Data from Arduino and converts the string so it can we added to the dict
        self.dataTimer.stop()
        settings = QtCore.QSettings()
        func = settings.value("ardAC/function", type=str)

        if func == "sum":
            # self.dat = {}
            # data_AC = self.device_AC.ask("a6")
            # data_AC = data_AC.split("\t")
            # self.dat["Arduino_A"] = int(data_AC[0])
            # self.dat["Arduino_B"] = int(data_AC[1])
            # self.dat["Arduino_C"] = int(data_AC[2])

            self.read_Data()

            Ard_list = ["Arduino_A", "Arduino_B", "Arduino_C", "Time per Shot", "Frequenz in Hz"]
            self.sendData(self.data)

            VarStr = ""
            for ard in Ard_list:
                VarStr = VarStr + ard + "\n"

            DisStr = ""
            for i in range(3):
                DisStr = DisStr + str(self.data[chr(i + 65)]) + "\n"

            self.stoptime = time.time()
            t1 = self.stoptime - self.starttime
            DisStr = DisStr + f"{t1}" + "\n"
            self.starttime = time.time()
            if (t1) > 0:
                DisStr = DisStr + f"{1/(t1)}" + "\n"
            # DisStr = DisStr + f"{self.timeTake}" + "\n"

            self.lbVariable.setText(VarStr)
            self.lbDisplay.setText(DisStr)
        elif func == "array_lim":
            self.read_Data()
            self.sendData(self.data)
            self.stoptime = time.time()
            t1 = self.stoptime - self.starttime
            self.starttime = time.time()
            DisStr2 = f"{t1}" + "\n"
            if t1 > 0:
                DisStr2 = DisStr2 + f"{1/t1}" + "\n"
                if 1/t1 < 10:
                    self.kleiner9 = self.kleiner9 + 1
            DisStr2 = DisStr2 + f"{self.kleiner9}" + "\n"

            for i in range(3):
                DisStr2 = DisStr2 + str(self.data[chr(i + 65)]) + "\n"

            self.lbVariable.setText("Lim in s \n Freq in Hz \n kleiner 9Hz \n ADC A \n ADC B \n ADC C \n")
            self.lbDisplay.setText(DisStr2)
            # self.sendData(self.data)
            # self.read_Data()

        elif func == "array_full":
            self.read_Data()
        elif func == "array_once":
            self.read_Data()

        self.startMeasurement(self.actionMeasure.isChecked())

    def sendData(self, values):
        try:
            self.publisher.send_legacy(values)
        except Exception as exc:
            log.exception("Sending data failed!", exc)

    def openArduinoSettings(self, ard: str):
        """Open the Channel Settings."""
        print(self.arduinoSettings.keys())
        if ard not in self.arduinoSettings.keys():
            arduinoSettings = Arduino_Settings(ard, main=self)
            arduinoSettings.signals.channelChanged.connect(self.channelConfigured)
            self.signals.channelUpdated[ard].connect(arduinoSettings.update)
            self.signals.samplesChanged.connect(arduinoSettings.setLimits)
            self.arduinoSettings[ard] = arduinoSettings
        self.arduinoSettings[ard].open()
        print(self.arduinoSettings.keys())
        # print(self.arduinoSettings[ard])

    def getFunction(self, ard: str):
        arduinoSettings = Arduino_Settings(ard, main=self)
        return arduinoSettings.getFunction()

    def restart(self, checked):
        if checked:
            try:
                self.device_AC.ask("c16;055.0")
            except Exception as exc:
                log.error(f"Error at restating is {exc}")

    def openArduinoSettings_AC(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openArduinoSettings("AC")

    def blockReady(self, handle, status: int, pointer=None) -> None:
        """Emit a data ready signal."""
        self.signals.blockReady.emit(status)

    def read_Data(self,):
        # func = self.getFunction("AC")
        settings = QtCore.QSettings()
        func = settings.value("ardAC/function")
        sendArray = settings.value("ardAC/sendArray")
        SaveStartStop = settings.value("ardAC/SaveStartStop")
        getSumStartStop = settings.value("ardAC/getSumStartStop")
        doFFT = settings.value("ardAC/doFFT")
        # print(settings.allKeys())
        try:
            if func == "sum":
                # first_char = "a"

                data_AC = self.device_AC.ask("a6")
                print(data_AC)

                # print(data_AC)

                data_AC = data_AC.split("\t")

                # print(data_AC)

                data_AC = [int(i) for i in data_AC]

                # print(data_AC)
                # print(data_AC[0])

                for i in range(3):
                    self.data[chr(i + 65)] = data_AC[i]
                    print(chr(i + 65), self.data[chr(i + 65)])
                    # print(np.array(int(data_AC[i])))
                    # print(np.ndarray(int(data_AC[i])).shape)
                # self.data["A"] = np.ndarray(int(data_AC[0]))
                # self.data["B"] = np.ndarray(int(data_AC[1]))
                # self.data["C"] = np.ndarray(int(data_AC[2]))
            elif SaveStartStop == 'True':
                settings.setValue('ardAC/SaveStartStop', 'False')
                print(SaveStartStop)
                ADCs = settings.value("ardAC/ADCs")
                SigStart = str(settings.value("ardAC/signalStart"))
                SigStop = str(settings.value("ardAC/signalStop"))
                if int(SigStart) >= int(SigStop):
                    print("SignalStart is not allowed to be bigger the SignalStop")
                elif ADCs == "1":
                    StringStartStop = "s1;"
                    for i in range(len(SigStart), 5):
                        StringStartStop = StringStartStop + "0"
                    StringStartStop = StringStartStop + SigStart + ";"

                    for i in range(len(SigStop), 5):
                        StringStartStop = StringStartStop + "0"
                    StringStartStop = StringStartStop + SigStop

                    printer = self.device_AC.ask(StringStartStop)

                    print(StringStartStop, printer)
                    # self.device_AC.ask("s1")
                elif ADCs == "2":
                    StringStartStop = "s2;"
                    for i in range(len(SigStart), 5):
                        StringStartStop = StringStartStop + "0"
                    StringStartStop = StringStartStop + SigStart + ";"

                    for i in range(len(SigStop), 5):
                        StringStartStop = StringStartStop + "0"
                    StringStartStop = StringStartStop + SigStop

                    printer = self.device_AC.ask(StringStartStop)

                    print(StringStartStop, printer)
                    # self.device_AC.ask("s1")
                elif ADCs == "3":
                    StringStartStop = "s3;"
                    for i in range(len(SigStart), 5):
                        StringStartStop = StringStartStop + "0"
                    StringStartStop = StringStartStop + SigStart + ";"

                    for i in range(len(SigStop), 5):
                        StringStartStop = StringStartStop + "0"
                    StringStartStop = StringStartStop + SigStop

                    printer = self.device_AC.ask(StringStartStop)

                    print(StringStartStop, printer)
                    # self.device_AC.ask("s1")

            # elif getSumStartStop == 'True':
            #     settings.setValue('ardAC/getSumStartStop', 'False')
            #     ArdACset = self.device_AC.ask("p")
            #     ADCs = settings.value("ardAC/ADCs")
            #     ArdACset = ArdACset.split("\t")
            #     sumStart = 0
            #     sumStop = 25000
            #     if ADCs == "1":
            #         sumStart = int(ArdACset[2])
            #         sumStop = int(ArdACset[3])
            #         settings.setValue('ardAC/signalStart1', sumStart)
            #         settings.setValue('ardAC/signalStop1', sumStop)
            #     elif ADCs == "2":
            #         sumStart = int(ArdACset[4])
            #         sumStop = int(ArdACset[5])
            #         settings.setValue('ardAC/signalStart2', sumStart)
            #         settings.setValue('ardAC/signalStop2', sumStop)
            #     elif ADCs == "3":
            #         sumStart = int(ArdACset[6])
            #         sumStop = int(ArdACset[7])
            #         settings.setValue('ardAC/signalStart3', sumStart)
            #         settings.setValue('ardAC/signalStop3', sumStop)

            #     arduinoSettings = Arduino_Settings("AC", main=self)
            #     arduinoSettings.setSignalStartnow(int(sumStart))
            #     arduinoSettings.setSignalStopnow(int(sumStop))
            #     print(ArdACset)
            #     print(sumStart, sumStop)
            elif func == "array_lim":  # only use if u have all 3 set to an length of 2048, so 12288 elents in total
                self.device_AC.write("l6")
                data_AC_byte = self.device_AC.read_bytes(12288)  # Since 3 ADCs with 25000 16 bit int so 3 * 25000 * 2
                DataByteTestin16bitInt = self.split_byteString(data_AC_byte, 6)
                if doFFT == "True":
                    y_fft_A = np.fft.fft(DataByteTestin16bitInt[0])
                    y_fft_B = np.fft.fft(DataByteTestin16bitInt[1])
                    y_fft_C = np.fft.fft(DataByteTestin16bitInt[2])
                    nA = len(DataByteTestin16bitInt[0])
                    print(nA)
                    freq = np.fft.fftfreq(nA)
                    threshold = 0.05
                    y_fft_A_filtered = np.where(np.abs(freq) > threshold, 0, y_fft_A)
                    y_fft_B_filtered = np.where(np.abs(freq) > threshold, 0, y_fft_B)
                    y_fft_C_filtered = np.where(np.abs(freq) > threshold, 0, y_fft_C)

                    y_A_filtered = np.fft.ifft(y_fft_A_filtered)
                    y_B_filtered = np.fft.ifft(y_fft_B_filtered)
                    y_C_filtered = np.fft.ifft(y_fft_C_filtered)

                    self.dataArray['A'] = np.real(y_A_filtered)
                    self.dataArray['B'] = np.real(y_B_filtered)
                    self.dataArray['C'] = np.real(y_C_filtered)

                    self.signals.channelUpdated["AC"].emit(True)
                    sumList = [0, 0, 0]
                    for j in range(60, 1988):  # set boundaries for th 2048
                        sumList[0] += self.dataArray['A'][j]
                        sumList[1] += self.dataArray['B'][j]
                        sumList[2] += self.dataArray['C'][j]

                    for i in range(3):
                        self.data[chr(i + 65)] = round(sumList[i])
                        print(chr(i + 65), self.data[chr(i + 65)])

                else:
                    for i in range(3):
                        self.dataArray[chr(i + 65)] = np.array(DataByteTestin16bitInt[i])
                    sumList = [0, 0, 0]
                    for j in range(60, 1988):  # set boundaries for th 2048
                        sumList[0] += self.dataArray['A'][j]
                        sumList[1] += self.dataArray['B'][j]
                        sumList[2] += self.dataArray['C'][j]
                    for k in range(3):
                        self.data[chr(k + 65)] = round(sumList[k])
                        print(chr(k + 65), self.data[chr(k + 65)])

                self.signals.channelUpdated["AC"].emit(True)
            elif func == "array_full":
                self.device_AC.write("v6")
                data_AC_byte = self.device_AC.read_bytes(150000) # Since 3 ADCs with 25000 16 bit int so 3 * 25000 * 2

                DataByteTestin16bitInt = self.split_byteString(data_AC_byte, 6) 
                if doFFT == "True":
                    print("DO FFT")
                    y_fft_A = np.fft.fft(DataByteTestin16bitInt[0])
                    y_fft_B = np.fft.fft(DataByteTestin16bitInt[1])
                    y_fft_C = np.fft.fft(DataByteTestin16bitInt[2])
                    nA = len(DataByteTestin16bitInt[0])
                    print(nA)
                    freq = np.fft.fftfreq(nA)
                    threshold = 0.05
                    y_fft_A_filtered = np.where(np.abs(freq) > threshold, 0, y_fft_A)
                    y_fft_B_filtered = np.where(np.abs(freq) > threshold, 0, y_fft_B)
                    y_fft_C_filtered = np.where(np.abs(freq) > threshold, 0, y_fft_C)

                    y_A_filtered = np.fft.ifft(y_fft_A_filtered)
                    y_B_filtered = np.fft.ifft(y_fft_B_filtered)
                    y_C_filtered = np.fft.ifft(y_fft_C_filtered)

                    self.dataArray['A'] = np.real(y_A_filtered)
                    self.dataArray['B'] = np.real(y_B_filtered)
                    self.dataArray['C'] = np.real(y_C_filtered)

                    self.signals.channelUpdated["AC"].emit(True)

                else:
                    for i in range(3):
                        self.dataArray[chr(i + 65)] = np.array(DataByteTestin16bitInt[i])
                self.signals.channelUpdated["AC"].emit(True)
            elif func == "array_once":
                # print("got into func == array")
                # print("_----------")
                # print(sendArray)
                # print("_----------")
                if sendArray == 'True':
                    settings.setValue('ardAC/sendArray', 'False')
                    print(SaveStartStop)
                    # first_char = "r"
                    data_AC = self.device_AC.ask("r6")
                    # print(data_AC)
                    data_AC = data_AC.split("####")
                    print(len(data_AC))
                    for i in range(len(data_AC)):
                        local_save = data_AC[i].split('\t')
                        local_save = local_save[:-1]
                        print(local_save[0:10])
                        print(local_save[-10:])
                        print(len(local_save))
                        print("--------------------------------------------------------------------")
                        local_save = [int(i) for i in local_save]
                        self.dataArray[chr(i + 65)] = np.array(local_save)

                    # data_AC_1 = data_AC[0].split('\t')
                    # data_AC_1 = [int(i) for i in data_AC_1]
                    # data_AC_2 = data_AC[1].split('\t')
                    # data_AC_2 = [int(i) for i in data_AC_2]
                    # data_AC_3 = data_AC[2].split('\t')
                    # data_AC_3 = [int(i) for i in data_AC_3]
                    self.signals.channelUpdated["AC"].emit(True)
                # else:
                    # sett = self.device_AC.ask("p")
                    # print(sett)


        except Exception as exc:
            log.error(f"Readout failed with {exc}!!!!!!!!!!!!!!!!!!")
            # time.sleep(1)
    
    def combine_bytes_from_single_bytes(self, high_bytes, low_bytes):
        if len(high_bytes) != len(low_bytes):
            raise ValueError("High bytes and low bytes must have the same length")
        
        combined_integers = []
        
        for high_byte, low_byte in zip(high_bytes, low_bytes):
            # Convert bytes to integers
            high_int = high_byte
            low_int = low_byte
            # Combine the bytes into a 16-bit integer
            combined_int = (high_int << 8) | low_int
            combined_integers.append(combined_int)
        
        return combined_integers
    
    
    
    def split_byteString(self, byte_string, part_number):
        """
        Splits a given pyte string into part_number substrings that are then combined into part_number/2 16bit arrays
        """
        if len(byte_string) % part_number == 0 and part_number % 2 == 0:
            part_size = len(byte_string) // part_number
            byte_parts = [byte_string[i * part_size:(i + 1) * part_size] for i in range(6)]
            List_16bitArrays = []
            for i in range(0, part_number, 2):
                List_16bitArrays.append(self.combine_bytes_from_single_bytes(byte_parts[i], byte_parts[i+1]))
            
            return List_16bitArrays
        else:
            raise ValueError("High bytes array and low bytes array must have the same length")




if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(ARD_scope)
"""
Module for the settings dialog class.

Created on 12.08.2024 by Leif Pfannekuch
"""

from typing import Any, Iterable

# from picoscope import picobase
from qtpy import QtCore, QtWidgets, uic
from qtpy.QtCore import Slot as pyqtSlot  # type: ignore


class Arduino_gen_Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    buttonBox = QtWidgets.QDialogButtonBox

    bbResolution: QtWidgets.QComboBox
    sbClockSpeed: QtWidgets.QSpinBox

    sbSamples: QtWidgets.QSpinBox

    def __init__(self, Resolutions: Iterable[str], main, **kwargs):
        # Initialize the dialog.

        # Use initialization of parent class QDialog.
        super().__init__(**kwargs)
        self.main = main
        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/Settings.ui", self)
        self.show()

        self.bbResolution.addItems(Resolutions)
        # Configure settings.
        self.settings = QtCore.QSettings()  # ?

        # Convenience list for widgets with value(), SetValue() methods.
        self.bb_sets: tuple[tuple[QtWidgets.QComboBox, str, Any, Any], ...] = (
            (self.bbResolution, "Resolution", "16bit", str),
        )

        self.sets: tuple[tuple[QtWidgets.QComboBox, str, Any, Any], ...] = (
            (self.sbClockSpeed, "ClockSpeed", 60000, int),
            (self.sbSamples, "Samples", 25000, int),
        )
        self.readValues()

        # CONNECT BUTTONS.
        self.pbRestoreDefaults = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        )
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            widget.setCurrentText(self.settings.value(name, defaultValue=value,
                                                      type=typ))
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value,
                                                type=typ))

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            widget.setCurrentText(value)
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.currentText())
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        super().accept()  # make the normal accept things

# -*- coding: utf-8 -*-
"""
Created on 12.08.2024

@author: Leif Pfannekuch
"""


def linear(x, a, b):
    return a * x + b


def quadratic(x, a, b, c):
    return a * x**2 + b * x + c


def Calibration(device, data):
    if device == "ADC1":
        parameters = [20, 0]
        return linear(data, *parameters)

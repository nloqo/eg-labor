
"""
Created on 12.08.2024

@author: Leif Pfannekuch
"""

import math

import pyqtgraph as pg
from qtpy import QtCore, QtGui, QtWidgets, uic
from qtpy.QtCore import Slot as pyqtSlot, Signal as pyqtSignal  # type: ignore
import numpy as np

import json
import datetime
# import pyperclip


class Arduino_Settings(QtWidgets.QDialog):
    """Window for the settings of a single Arduino, including a plot."""
    plot: pg.PlotItem

    leVariableName: QtWidgets.QLineEdit

    lbResolution: QtWidgets.QLabel
    bbResolution: QtWidgets.QComboBox

    lbADC: QtWidgets.QLabel
    bbADCs: QtWidgets.QComboBox

    pbSaveStartStop: QtWidgets.QPushButton
    # cbDataRequest: QtWidgets.QCheckBox
    cbdoFFT: QtWidgets.QCheckBox
    pbSaveArray: QtWidgets.QPushButton
    pbsendArray: QtWidgets.QPushButton
    leJsonName: QtWidgets.QLineEdit
    # pbTestSettings: QtWidgets.QPushButton

    pbScale: QtWidgets.QPushButton
    bbFunction: QtWidgets.QComboBox

    lbSignalStart: QtWidgets.QLabel
    sbSignalStart: QtWidgets.QSpinBox

    lbSignalLength: QtWidgets.QLabel
    sbSignalStop: QtWidgets.QSpinBox

    gbZeroing: QtWidgets.QGroupBox
    sbZeroStart: QtWidgets.QSpinBox
    sbZeroStop: QtWidgets.QSpinBox
    leVariableNameZero: QtWidgets.QLineEdit

    lbOverrange: QtWidgets.QLabel

    def __init__(self, Arduino: str, main, **kwargs):
        # Arduino = "AC" (also ADC A - C)

        super().__init__(**kwargs)
        self.Arduino = Arduino
        self.main = main
        self.signals = self.SettingsSignals()

        # UI
        uic.load_ui.loadUi("data/Arduino_Settings.ui", self)
        self.setWindowTitle(f"Arduino with ADC {Arduino} settings")

        # Space for Red circle for overrange (more the 3.3V)
        # Red circle as overrange indicator.
        size = min(self.lbOverrange.height(), self.lbOverrange.width())
        self.lbOverrange.setFixedHeight(20)
        self.lbOverrange.setFixedWidth(self.lbOverrange.height())
        picture = QtGui.QPixmap("data/RedCircle.png")
        picture2 = picture.scaled(size, size,
                                  QtCore.Qt.AspectRatioMode.KeepAspectRatio,
                                  QtCore.Qt.TransformationMode.SmoothTransformation)
        self.lbOverrange.setPixmap(picture2)

        # The settings object.
        self.settings = QtCore.QSettings()
        self.samples = self.settings.value('samples', type=int)  # ?
        self.settings.beginGroup(f"ard{self.Arduino}")
        self.bbResolution.addItems(['8bit', '10bit', '12bit', '14bit',
                                    '16bit'])
        self.bbADCs.addItems(['1', '2', '3', '1 & 2', '1 & 3',
                              '2 & 3', '1 & 2 & 3'])
        self.bbFunction.addItems(["sum", "array_lim", "array_full", "array_once", "focus/center",
                                  "first", "maximum", "minimum"])

        self.bbSets = (
            (self.bbResolution, 'Resolution', "16bit", str),
            (self.bbADCs, 'ADCs', "1 & 2 & 3", str),
            (self.bbFunction, 'function', "sum", str)
        )
        self.sets = (
            # (self.cbDataRequest, 'DataReq', False, bool),
            # (self.cbdoFFT, 'doFFT', False, bool),
            (self.sbSignalStart, 'signalStart', 1000, int),
            (self.sbSignalStop, 'signalStop', 2000, int),
            # (self.gbZeroing, "zeroing", False, bool),
            (self.sbZeroStart, 'zeroStart', 0, int),
            (self.sbZeroStop, 'ZeroStop', 1000, int),
            # (self.leVariableNameZero, "variableNameZero", "", str),
        )

        # Initialize
        self.setupPlot()
        self.setLimits(self.samples)
        self.readValues()
        if self.gbZeroing.isChecked():
            self.createZeroLines()

        # Connect to slots
        self.leVariableName.editingFinished.connect(self.setVariableName)
        self.bbADCs.currentTextChanged.connect(self.setADCs)
        self.bbResolution.currentTextChanged.connect(self.setResolution)
        # self.cbDataRequest.stateChanged.connect(self.sendData)
        self.cbdoFFT.stateChanged.connect(self.checkFFT)
        self.pbsendArray.clicked.connect(self.requestArray)
        self.pbSaveArray.clicked.connect(self.saveArray)

        self.pbSaveStartStop.clicked.connect(self.saveStartStop)
        # Evaluation
        self.bbFunction.currentTextChanged.connect(self.setFunction)
        self.sbSignalStart.valueChanged.connect(self.setSignalStart)
        self.sbSignalStop.valueChanged.connect(self.setSignalStop)
        self.pbScale.clicked.connect(self.scale)

        # Zeroing
        self.gbZeroing.clicked.connect(self.setZeroing)
        self.sbZeroStart.valueChanged.connect(self.setZeroStart)
        self.sbZeroStop.valueChanged.connect(self.setZeroStop)
        self.leVariableNameZero.editingFinished.connect(self.setVariableNameZero)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings"""
        channelChanged = pyqtSignal(str)

    def setLimits(self, samples):
        """Set the limits of the spinboxes to the number of `samples`."""
        for widget in (self.sbSignalStart, self.sbSignalStop,
                       self.sbZeroStart, self.sbZeroStop):
            widget.setMaximum(samples)
        try:
            for line in (self.signalStart, self.signalStop,
                         self.zeroStart, self.zeroStop):
                line.setBounds((0, samples))
        except AttributeError:
            pass

    def setupPlot(self):
        """Configure the plot."""
        self.figure = self.plot.plot([])
        self.plot.setLabel('bottom', "sample")
        self.plot.setLabel('left', "voltage")
        # Lines for the signal.
        start = self.settings.value('signalStart', type=int)
        stop = self.settings.value('signalStop', type=int)
        self.signalStart = self.plot.addLine(x=start, pen='y', movable=True)
        self.signalStop = self.plot.addLine(x=stop, pen='y', movable=True)
        self.signalStart.sigDragged.connect(self.signalStartDragged)
        self.signalStop.sigDragged.connect(self.signalStopDragged)

    def createZeroLines(self):
        """Create lines for zeroing."""
        start = self.settings.value('zeroStart', type=int)
        stop = self.settings.value('ZeroStop', type=int)
        self.zeroStart = self.plot.addLine(x=start, pen='b', movable=True,
                                           bounds=(0, self.samples))
        self.zeroStop = self.plot.addLine(x=stop, pen='b', movable=True,
                                          bounds=(0, self.samples))
        self.zeroStart.sigDragged.connect(self.zeroStartDragged)
        self.zeroStop.sigDragged.connect(self.zeroStopDragged)

    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        for setting in self.bbSets:
            widget, name, value, typ = setting
            widget.setCurrentText(self.settings.value(name, defaultValue=value, type=typ))
        self.leVariableName.setText(self.settings.value("variableName", "name", str))
        # self.cbDataRequest.setChecked(self.settings.value('DataReq', type=bool))
        self.cbdoFFT.setChecked(self.settings.value('doFFT', type=bool))
        
        self.gbZeroing.setChecked(self.settings.value('zeroing', type=bool))
        self.leVariableNameZero.setText(self.settings.value("variableNameZero", type=str))

    @pyqtSlot()
    def signalStartDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbSignalStart.setValue(math.ceil(self.signalStart.value()))

    @pyqtSlot()
    def signalStopDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbSignalStop.setValue(math.floor(self.signalStop.value()))

    @pyqtSlot()
    def zeroStartDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbZeroStart.setValue(math.ceil(self.zeroStart.value()))

    @pyqtSlot()
    def zeroStopDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbZeroStop.setValue(math.floor(self.zeroStop.value()))

    # Store the changed values.
    #   General
    def setVariableName(self):
        """Set the variable name."""
        self.settings.setValue('variableName', self.leVariableName.text())

    @pyqtSlot(str)
    def setADCs(self, text):
        self.settings.setValue('ADCs', text)
        self.signals.channelChanged.emit(self.Arduino)
        self.getSumStartStop()
        self.update(1)
        # print("----------------")
        # print(self.settings.value("signalStart1"))
        # print("---------------------")
        # self.sbSignalStart.setValue(self.settings.value())

    def getSumStartStop(self):
        # self.settings.setValue('getSumStartStop', 'True')
        ADCs = self.settings.value("ADCs")
        # print(f"----------{ADCs}-------------------")
        # print(self.settings.allKeys())
        ArdACset = self.main.device_AC.ask("p")
        ArdACset = ArdACset.split("\t")
        sumStart = 0
        sumStop = 25000
        if ADCs == "1":
            sumStart = int(ArdACset[2])
            sumStop = int(ArdACset[3])
            self.settings.setValue('signalStart1', sumStart)
            self.settings.setValue('signalStop1', sumStop)
        elif ADCs == "2":
            sumStart = int(ArdACset[4])
            sumStop = int(ArdACset[5])
            self.settings.setValue('signalStart2', sumStart)
            self.settings.setValue('signalStop2', sumStop)
        elif ADCs == "3":
            sumStart = int(ArdACset[6])
            sumStop = int(ArdACset[7])
            self.settings.setValue('signalStart3', sumStart)
            self.settings.setValue('signalStop3', sumStop)
        self.setSignalStartnow(int(sumStart))
        self.setSignalStopnow(int(sumStop))
        print(sumStart, sumStop)
        print(ArdACset)

    @pyqtSlot(str)
    def setResolution(self, text):
        """Set the Resolution in bit."""
        self.settings.setValue('Resolution', text)
        self.signals.channelChanged.emit(self.Arduino)

    @pyqtSlot(int)
    def sendData(self, checked):
        self.sbSignalStart.setValue(45)
        # self.settings.setValue('DataReq', bool(checked))
        self.signals.channelChanged.emit(self.Arduino)

    @pyqtSlot(int)
    def checkFFT(self, checked):
        if checked:
            self.settings.setValue('doFFT', 'True')
        else:
            self.settings.setValue('doFFT', 'False')

    # Evaluation
    @pyqtSlot(str)
    def setFunction(self, text):
        """Sets the function that was chosen."""
        print(text)
        self.settings.setValue('function', text)
        func = self.settings.value("function")
        print(func)
        print(self.settings.allKeys())

    def getFunction(self):
        """Returns the string value of the Function"""
        return str(self.settings.value('function'))

    @pyqtSlot(int)
    def setSignalStart(self, value):
        self.settings.setValue("signalStart", value)
        self.signalStart.setValue(value)

    def setSigStart1(self, value):
        self.settings.setValue("signalStart1", value)

    def setSigStart2(self, value):
        self.settings.setValue("signalStart2", value)

    def setSigStart3(self, value):
        self.settings.setValue("signalStart3", value)

    @pyqtSlot(int)
    def setSignalStop(self, value):
        self.settings.setValue("signalStop", value)
        self.signalStop.setValue(value)

    def setSigStop1(self, value):
        self.settings.setValue("signalStop1", value)

    def setSigStop2(self, value):
        self.settings.setValue("signalStop2", value)

    def setSigStop3(self, value):
        self.settings.setValue("signalStop3", value)

    def setSignalStartnow(self, value: int):
        self.sbSignalStart.setValue(value)
        self.setSignalStart(value)

    def setSignalStopnow(self, value: int):
        self.sbSignalStop.setValue(value)
        self.setSignalStop(value)

    def scale(self, checked):
        """Zooms into the function interval of the signal."""
        if checked:
            self.plot.setXRange(self.sbSignalStart.value() - 10,
                                self.sbSignalStop.value() + 10)
        else:
            self.plot.enableAutoRange(x=True)

    @pyqtSlot(bool)
    def requestArray(self, checked):
        self.settings.setValue('sendArray', 'True')
        print(checked)
        self.leJsonName.setText("")

    @pyqtSlot(bool)
    def saveArray(self, checked):
        data = self.main.dataArray
        ArdSet = self.main.device_AC.ask("p")
        ArdSet = ArdSet.split("\t")
        path = "D:\\Measurement Data\\PDReadout\\ARD_Scope_Save\\"
        print(ArdSet)
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        dataA = data["A"].tolist()
        dataB = data["B"].tolist()
        dataC = data["C"].tolist()

        print(dataA[0:10])
        saveDict = {
            "Resolution": int(ArdSet[0]),
            "Clockspeed": float(ArdSet[1]),
            "ADC_A": dataA,
            "ADC_B": dataB,
            "ADC_C": dataC
        }
        # print(saveDict)
        with open(f"{path}{file_name}.json", "w") as file:
            json.dump(saveDict, file)
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(file_name)
        # pyperclip.copy(file_name)
        self.leJsonName.setText(file_name)

    @pyqtSlot(bool)
    def saveStartStop(self, checked):
        self.settings.setValue('saveStartStop', 'True')
        print("StSp")
        # settings = QtCore.QSettings()
        # ADCs = settings.value("ardAC/ADCs")
        # print("a"+ADCs+"a")
        # if ADCs == "1":
        #     aaaaa = 111111111111111111111111
        #     print(aaaaa)

    # Zeroing
    @pyqtSlot(bool)
    def setZeroing(self, checked):
        self.settings.setValue('zeroing', checked)
        if checked:
            self.createZeroLines()
        else:
            try:
                self.plot.removeItem(self.zeroStart)
                self.plot.removeItem(self.zeroStop)
            except Exception as exc:
                print(f"Error removing items {exc}")

    @pyqtSlot(int)
    def setZeroStart(self, value):
        self.settings.setValue('zeroStart', value)
        self.zeroStart.setValue(value)

    @pyqtSlot(int)
    def setZeroStop(self, value):
        self.settings.setValue("zeroStop", value)
        self.zeroStop.setValue(value)

    @pyqtSlot()
    def setVariableNameZero(self):
        """Set the variable name."""
        self.settings.setValue('variableNameZero', self.leVariableNameZero.text())

    # Show data.
    @pyqtSlot(bool)
    def update(self, overrange):
        """Update the plot with the new data."""
        # settings = QtCore.QSettings()
        func = self.settings.value("function")
        ADC = self.settings.value("ADCs")
        # print(settings.allKeys())
        # print("it is in updates" + func)
        print(func)
        if func == "array_lim":
            if int(ADC) == 1:
                data = self.main.dataArray["A"]  # How do i get the data here !!! main is
            elif int(ADC) == 2:
                data = self.main.dataArray["B"]
            elif int(ADC) == 3:
                data = self.main.dataArray["C"]
            # print(data)
            # Arduino_scope Object
            self.figure.setData(data)  # What is the format needet
            self.lbOverrange.setVisible(overrange)
        elif func == "array_full":
            if int(ADC) == 1:
                data = self.main.dataArray["A"]  # How do i get the data here !!! main is
            elif int(ADC) == 2:
                data = self.main.dataArray["B"]
            elif int(ADC) == 3:
                data = self.main.dataArray["C"]
            # print(data)
            # Arduino_scope Object
            self.figure.setData(data)  # What is the format needet
            self.lbOverrange.setVisible(overrange)
            # print(data)
        elif func == "array_once":
            if int(ADC) == 1:
                data = self.main.dataArray["A"]  # How do i get the data here !!! main is
            elif int(ADC) == 2:
                data = self.main.dataArray["B"]
            elif int(ADC) == 3:
                data = self.main.dataArray["C"]
            # print(data)
            # Arduino_scope Object
            self.figure.setData(data)  # What is the format needet
            self.lbOverrange.setVisible(overrange)

"""
Module for the Settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from typing import Any

from qtpy import QtCore, QtWidgets, uic
from qtpy.QtCore import Slot as pyqtSlot  # type: ignore
from devices import motors





# For definition via designer
class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    buttonBox = QtWidgets.QDialogButtonBox
    sbITR100port = QtWidgets.QSpinBox
    sbITR200port = QtWidgets.QSpinBox
    sbReaderInterval = QtWidgets.QSpinBox
    pbGratingMotorSettings = QtWidgets.QAbstractButton
    pbHWPMotorSettings = QtWidgets.QAbstractButton
    sbGrating = QtWidgets.QDoubleSpinBox
    sbK = QtWidgets.QDoubleSpinBox
    sbSlope = QtWidgets.QDoubleSpinBox
    sbYintersect = QtWidgets.QDoubleSpinBox

    def __init__(self, **kwargs) -> None:
        """Initialize the dialog."""
        super().__init__(**kwargs)

        uic.load_ui.loadUi("data/settings.ui", self)
        self.show()

        self.signals = self.SettingsSignals()
        self.settings = QtCore.QSettings()
        self.sets: tuple[tuple[QtWidgets.QSpinBox | QtWidgets.QDoubleSpinBox,
                               str, Any, type], ...] = (
            (self.sbITR100port, "ITR100port", 53, int),
            (self.sbITR200port, "ITR200port", 54, int),
            (self.sbReaderInterval, "ReaderInterval", 50, int),
            (self.sbGrating, "G", 1118.0, float),
            (self.sbK, "K", 32.0, float),
            (self.sbSlope, "Slope", -2e-5, float),
            (self.sbYintersect, "Yintersect", -112.0, float),
            
        )
        self.readValues()

        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbRestoreDefaults = self.buttonBox.button(restore)
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)
        self.pbGratingMotorSettings.clicked.connect(self.openGratingMotorSettings)
        self.pbHWPMotorSettings.clicked.connect(self.openHWPMotorSettings)
        # self.SettingsSignals.motorConfigured.connect(self.configureMotor)

    @pyqtSlot()
    def openGratingMotorSettings(self) -> None:
        """Open the settings for the grating motor."""
        motorSettings = motors.MotorSettings(key="Grating", motorName="Grating")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("Grating", "motorCardGrating")

    @pyqtSlot()
    def openHWPMotorSettings(self):
        """Open the settings for the grating motor."""
        motorSettings = motors.MotorSettings(key="HWP", motorName="HWP")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("HWP", "motorCardHWP")

    def readValues(self) -> None:
        """Read the stored values and show them on the user interface."""
        for widget, name, value, typ in self.sets:
            widget.setValue(self.settings.value(name, defaultValue=value,
                                                type=typ))

    @pyqtSlot()
    def restoreDefaults(self) -> None:
        """Restore the user interface to default values."""
        for widget, name, value, typ in self.sets:
            widget.setValue(value)

    @pyqtSlot()
    def accept(self) -> None:
        """Save the values from the user interface in the settings."""
        for widget, name, value, typ in self.sets:
            self.settings.setValue(name, widget.value())
        super().accept()  # make the normal accept things

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings."""

        motorConfigured = QtCore.Signal(str, str)

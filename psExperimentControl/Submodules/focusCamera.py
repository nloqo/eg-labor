import imagingcontrol4 as ic4
<<<<<<< HEAD
from imagingcontrol4.ic4exception import IC4Exception
from PyQt6 import QtWidgets, QtCore, uic, QtGui
from PyQt6.QtCore import pyqtSlot
import logging
import numpy as np
import pyqtgraph as pg

import Submodules.psExperimentControl_main as ps_main
log = logging.getLogger("psExperimentControl")

class FocusCamera(QtWidgets.QDialog):

    pbScanForDevices: QtWidgets.QPushButton
    pbActivate: QtWidgets.QPushButton
    pbSetROI: QtWidgets.QPushButton

    sbExposure: QtWidgets.QSpinBox
    sbTriggerDelay: QtWidgets.QSpinBox
    sbYLow: QtWidgets.QSpinBox
    sbYUp: QtWidgets.QSpinBox
    sbXLow: QtWidgets.QSpinBox
    sbXUp: QtWidgets.QSpinBox

    lbRadius1: QtWidgets.QLabel
    lbRadius2: QtWidgets.QLabel
    lbArea: QtWidgets.QLabel
    lbRepetitionRate: QtWidgets.QLabel

    cbDevices: QtWidgets.QComboBox
    cbTriggerMode: QtWidgets.QComboBox

    gbROI: QtWidgets.QGroupBox

    pgImage: pg.ImageView
=======
from PyQt6 import QtWidgets, QtCore, uic
from PyQt6.QtCore import pyqtSlot
import logging
import numpy as np

import Submodules.psExperimentControl_main as ps_main
log = loggig.getLogger("psExperimentControl")

class FocusCamera(QtWidgets.QDialog):

>>>>>>> 6d7ab397390d8ff01311e299784bd62004dd46bb

    def __init__(self, parent, *args, **kwargs)
        """Initialize the focus camera control window."""
        super().__init__(parent=parent, *args, **kwargs)
        self.main_window: ps_main.psExperimentControl = parent

        uic.load_ui.loadui("data/beamCamera.ui", self)
        self.show()

<<<<<<< HEAD
        ic4.Library.init()
        self.grabber = ic4.Grabber()
        self.sink = ic4.SnapSink()

        self.checkDataTimer = QtCore.QTimer()
        self.checkDataTimer.timeout.connect(self.handleData)

        self.pgImage.setPredefinedGradient("spectrum")

        self.pbActivate.clicked.connect(self.activateCamera)
        self.pbScanForDevices.clicked.connect(self.scanForDevices)
        self.cbDevices.currentIndexChanged.connect(self.changeCamera)
        self.gbROI.toggled.connect(self.ROIactivated)
        self.pbSetROI.clicked.connect(self.changeROI)

    @pyqtSlot(QtGui.QCloseEvent)
    def closeEvent(self, event: QtGui.QCloseEvent):
        """Actions to perform before closing the window."""
        event.accept()

    def setExposureTime(self, time: int):
        """Set the exposure time given in �s."""


    def activateCamera(self, activate: bool):
        """Connect or diconnect the camera."""
        if activate:
            if self.cbDevices.itemText != "None":
                try:
                    self.grabber.device_open(self.cbDevices.itemText)
                except TypeError:
                    log.error("Variable is neither DeviceInfo nor str.")
                except Exception:
                    code, message = IC4Exception(Exception)
                    log.error(f"Failed to connect camera: [{code}] {message}")
                else:
                    self.configureCamera()
                    self.checkDataTimer.start(20)
        else:
            try:
                self.checkDataTimer.stop()
                self.grabber.stream_stop()
                self.grabber.device_close()
                self.cbDevices.setCurrentText("None")
            except Exception as exc:
                log.info(f"Device is already disconnected: {exc}")

    def configureCamera(self):
        if self.cbTriggerMode == "ON":
            self.grabber.device_property_map.set_value(ic4.PropId.TRIGGER_MODE, True)
        else:
            self.grabber.device_property_map.set_value(ic4.PropId.TRIGGER_MODE, False)
        self.grabber.device_property_map.set_value(ic4.PropId.TRIGGER_DELAY, self.sbTriggerDelay.value())
        self.grabber.stream_setup(self.sink, setup_option=ic4.StreamSetupOption.ACQUISITION_START)
        self.grabber.device_property_map.set_value(ic4.PropId.EXPOSURE_TIME, self.sbExposure.value())

    @pyqtSlot(int)
    def changeCamera(self, index: int):
        """Disconnect old camera, and conncet new camera."""
        if index == 0:
            self.grabber.device_close()
        else:
            self.grabber.device_close()
            self.grabber.device_open(self.cbDevices.itemText(index))
            self.configureCamera()

    @pyqtSlot()
    def scanForDevices(self):
        """Search for available devices."""
        self.cbDevices.clear()
        self.cbDevices.addItem("None")
        deviceList = ic4.DeviceEnum.devices()
        for element in deviceList:
            self.cbDevices.addItem(element)
        try:
            self.cbDevices.setCurrentIndex(1)
            self.activateCamera(True)
        except IndexError:
            pass

    def Gaussian2D(xy, a, x0, y0, sigma_x, sigma_y, theta, offset):
        """2D-Gaussian function to fit to the data."""
        x, y = xy
        a = np.cos(theta)**2 / (2 * sigma_x**2) + np.sin(theta)**2 / (2 * sigma_y**2)
        b = -np.sin(2 * theta) / (4 * sigma_x**2) + np.sin(2 * theta) / (4 * sigma_y**2)
        c = np.sin(theta)**2 / (2 * sigma_x**2) + np.cos(theta)**2 / (2 * sigma_y**2)
        g = offset + a * np.exp(-(a * (x - x0)**2 + 2 * b * (x - x0) * (y - y0) 
                            + c*((y-yo)**2)))

    def handleData(self):
        """Check for new frames and process the camera data, if available."""
        try:
            data_buffer: ic4.ImageBuffer = self.sink.snap_single(50)
            data = data_buffer.numpy_copy()
            self.pgImage.setImage(data)
        except Exception as exc:
            log.error(f"Failed to read frame from the camera: {exc}")


    @pyqtSlot(bool)
    def ROIactivated(self, status: bool):
        """Activate region of interest (ROI) to reduce number of pixels to readout."""
        try:
            if status:
                self.grabber.device_property_map.set_value(ic4.PropId.AUTO_FOCUS_ROI_ENABLE, True)
            else:
                self.grabber.device_property_map.set_value(ic4.PropId.AUTO_FOCUS_ROI_ENABLE, False)
        except Exception as exc:
            log.exception(f"Could not activate ROI: {exc}")

    @pyqtSlot()
    def changeROI(self):
        """Set the region of interest (ROI)."""
        self.grabber.device_property_map.set_value(ic4.PropId.AUTO_FOCUS_ROI_TOP, self.sbYLow.value())
        self.grabber.device_property_map.set_value(ic4.PropId.AUTO_FOCUS_ROI_HEIGHT, self.sbYUp.value()-self.sbYLow.value())
        self.grabber.device_property_map.set_value(ic4.PropId.AUTO_FOCUS_ROI_LEFT, self.sbXLow.value())
        self.grabber.device_property_map.set_value(ic4.PropId.AUTO_FOCUS_ROI_WIDTH, self.sbXUp.value()-self.sbXLow.value())


ic4.PropertyMap.get_value_str("TriggerMode")
ic4.Property
=======
    @pyqtSlot(QtCore)
    def closeEvent(self, event):
        """Actions to perform before closing the window."""
        event.accept()


ic4.Library.init()

grabber = ic4.Grabber()

deviceList = ic4.DeviceEnum.devices()
grabber.stream_setup()
grabber.device_property_map.set_value(ic4.PropId.)
>>>>>>> 6d7ab397390d8ff01311e299784bd62004dd46bb

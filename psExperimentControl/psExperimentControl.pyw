"""
Control of parameters for the vacuum chamber.
"""

# Standard packages.
import logging
import numpy as np
from contextlib import suppress

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from pytrinamic.modules import TMCM6110
from pytrinamic.modules import TMCM1140
from pytrinamic.connections import ConnectionManager
import nidaqmx as ni

from devices.gui_utils import start_app
from devices.base_main_window import LECOBaseMainWindow
from pyleco_extras.directors.tmc_motor_director import TMCMotorDirector
from pyleco.directors.starter_director import StarterDirector

# Local packages.
from data import settings
from devices import motors, ITR

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class psExperimentControl(LECOBaseMainWindow):
    """
    Control parameters for the experiment and readout sensors. The program includes control over
    pressure, pulse energy in the vacuum chamber and VUV grating.
    """

    # %% GUI objects

    actionSettings: QtWidgets.QWidgetAction
    actionClose: QtWidgets.QWidgetAction
    actionITR_200: QtWidgets.QWidgetAction
    actionITR_100: QtWidgets.QWidgetAction
    actionMotors: QtWidgets.QWidgetAction
    actionAll: QtWidgets.QWidgetAction
    gbGratingControl: QtWidgets.QGroupBox
    gbPressureSensor: QtWidgets.QGroupBox
    gbVoltageReaders: QtWidgets.QGroupBox
    gbLaserPower: QtWidgets.QGroupBox
    lbActualWavelength: QtWidgets.QLabel
    lbActualPumpWavelength: QtWidgets.QLabel
    lbPressureITR100: QtWidgets.QLabel
    lbPressureITR200: QtWidgets.QLabel
    lbIonizationCurrent: QtWidgets.QLabel
    lbHWPposition: QtWidgets.QLabel
    lbEMTvoltage: QtWidgets.QLabel
    sbSetWavelength: QtWidgets.QDoubleSpinBox
    sbHarmonics: QtWidgets.QSpinBox
    sbSetPumpWavelength: QtWidgets.QSpinBox
    sbSetHWP: QtWidgets.QSpinBox
    pbConfigureITR100: QtWidgets.QAbstractButton
    pbConfigureITR200: QtWidgets.QAbstractButton
    pbSetWavelength: QtWidgets.QAbstractButton
    pbSetPumpWavelength: QtWidgets.QAbstractButton
    pbSetHWP: QtWidgets.QAbstractButton
    pbZeroPosition: QtWidgets.QAbstractButton
    pbCalibration: QtWidgets.QAbstractButton
    pbSetEMTvoltage: QtWidgets.QPushButton
    sbOrdnung: QtWidgets.QSpinBox
    sbEMTvoltage: QtWidgets.QSpinBox

    # %% general functions

    def __init__(self, name: str = "psExperimentControl",
                 host: str = "localhost", **kwargs) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(name=name, host=host,
                         ui_file_name="psExperimentControl",  # ui file name in the data subfolder
                         **kwargs)
        self.setSettings()
        settings = QtCore.QSettings()

        # Initialize connection to Ni-card
        try:
            self.task_daq = ni.Task()
            self.task_daq.ai_channels.add_ai_voltage_chan("myDAQ1/ai0")
        except ni.DaqError:
            log.exception("Could not connect myDAQ1.")
        try:
            self.task_r = ni.Task()
            self.task_r.ai_channels.add_ai_voltage_chan("Dev1/ai0")
            self.task_w = ni.Task()
            self.task_w.ao_channels.add_ao_voltage_chan("Dev1/ao0", min_val=0)
            self.task_w.write(0)
        except ni.DaqError:
            log.exception("Could not connect Dev1.")

        # Connect actions to slots.
        self.actionITR_200.triggered.connect(self.connectITR200)
        self.actionITR_100.triggered.connect(self.connectITR100)
        self.actionMotors.triggered.connect(self.connectMotors)
        self.actionAll.triggered.connect(self.connectAll)
        self.pbSetWavelength.clicked.connect(self.setWavelength)
        self.pbSetPumpWavelength.clicked.connect(self.setPumpWavelength)
        self.pbConfigureITR100.clicked.connect(self.configureITR100)
        self.pbSetHWP.clicked.connect(self.setPowerHWP)
        self.pbConfigureITR200.clicked.connect(self.configureITR200)
        self.pbZeroPosition.clicked.connect(self.zeroPosition)
        self.pbCalibration.clicked.connect(self.openCalibration)
        self.pbSetEMTvoltage.clicked.connect(self.setEMTvoltage)

        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readout)
        self.waitMotorTimer = QtCore.QTimer()
        self.waitMotorTimer.timeout.connect(self.waitMotorGrating)
        self.waitMotorZeroTimer = QtCore.QTimer()
        self.waitMotorZeroTimer.timeout.connect(self.waitMotorGratingZero)
        self.connectMotors(True)
        self.connectITR200(True)
        self.actionMotors.setChecked(True)
        print(settings.value("ReaderInterval"))
        self.readoutTimer.start(settings.value("ReaderInterval"))

        log.info(f"Experiment Control initialized with name '{name}'.")

    def __del__(self) -> None:
        pass

    @pyqtSlot()
    def openSettings(self) -> None:
        self.settingsDialog = settings.Settings()
        self.settingsDialog.open()

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.starter_director.close()
        try:
            self.readoutTimer.stop()
        except AttributeError:
            pass
        try:
            self.ITR100.setEmission(False)
            del self.ITR100
        except AttributeError:
            pass
        try:
            del self.ITR200
        except AttributeError:
            pass
        try:
            self.motorCard1.disconnect()
            del self.motorCard1
        except AttributeError:
            pass
        try:
            self.motorCard2.disconnect()
            del self.motorCard2
        except AttributeError:
            pass
        event.accept()

    @pyqtSlot()
    def readout(self) -> None:
        data = {}
        # Pressure readout
        try:
            pressure200: float = self.ITR200.pressure
            data["ITR200pressure"] = pressure200
            self.lbPressureITR200.setText(f"{'%.2e' % pressure200} mbar")
        except AttributeError:
            self.lbPressureITR200.setText("nan")
        except IndexError:
            log.info("Readout too fast for pressure sensor.")

            data["ITR200pressure"] = np.nan
        try:
            pressure100: float = self.ITR100.pressure
            if self.unit100 == "mbar":
                pressure100_cal = pressure100
            elif self.unit100 == "Pa":
                pressure100_cal = pressure100 / 100
            elif self.unit100 == "Torr":
                pressure100_cal = pressure100 * 1.33322
            else:
                pressure100_cal = np.nan
                log.info("Unknown unit to calculate ITR 100 pressure in mbar.")
            self.lbPressureITR100.setText(f"{'%.2e' % pressure100} {self.unit100}")
            data["ITR100pressure"] = pressure100_cal
        except Exception:
            self.lbPressureITR100.setText("nan")
        
        # Grating motor readout
        try:
            gratingSteps = self.motorCard1.get_actual_position("VUVspec_m")
            data["GratingMotorPosition"] = gratingSteps
            self.actualWavelength = self.stepsToWavelength(gratingSteps)
            data["wavelengthPositionVUV"] = self.actualWavelength / 1e9
            data["stepPositionVUV"] = gratingSteps
            self.lbActualWavelength.setText(f"{round(self.actualWavelength, 2)} nm")
            self.actualPumpWavelength = self.actualWavelength * self.sbHarmonics.value()
            self.lbActualPumpWavelength.setText(f"{round(self.actualPumpWavelength, 2)} nm")
        except AttributeError:
            self.lbActualWavelength.setText("nan")
        except Exception as exc:
            log.error(f"Could not readout VUV grating motor with following exception: {exc}")
        # HWP motor readout
        try:
            self.HWPpos = self.motorCard2.get_actual_units("HWPexp_m")
            self.lbHWPposition.setText(f"{round(self.HWPpos, 2)} °")
            data["HWPExperiment"] = self.HWPpos
        except Exception as exc:
            log.error(f"Could not connect to experiment motor card: {exc}")

        # NI card readout
        try:
            voltage = self.task_r.read() * 600
            self.lbEMTvoltage.setText(f"{round(voltage, 0)} V")
            data["EMTvoltage"] = voltage
        except Exception as exc:
            log.exception(f"Readout of EMT-voltage failed with the following exception: {exc}")
        # Send data
        try:
            # TODO: Change to new version send_data. Should be done in all programs parallel.
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)

    # %% Connect devices

    @pyqtSlot(bool)
    def connectITR200(self, connect: bool) -> None:
        settings = QtCore.QSettings()
        if connect:
            try:
                self.ITR200 = ITR.ITR(f"COM{settings.value('ITR200port')}")
                self.actionITR_200.setChecked(connect)
                log.info("Successfully connected ITR200.")
            except (AttributeError, ConnectionError) as exc:
                self.actionITR_200.setChecked(False)
                log.error(f"Connection to ITR200 failed with the following exception: {exc}")
            try:
                self.connectITR100(True)
            except Exception:
                pass
        else:
            try:
                del self.ITR200
                log.info("Disconnected ITR200.")
            except AttributeError:
                pass

    @pyqtSlot(bool)
    def connectITR100(self, connect: bool) -> None:
        settings = QtCore.QSettings()
        if connect:
            try:
                if float(self.ITR200.pressure) < 0.1:
                    try:
                        self.ITR100 = ITR.ITR100(f"COM{settings.value('ITR100port')}")
                        self.actionITR_100.setChecked(True)
                        log.info("Successfully connected ITR100.")
                    except Exception as exc:
                        self.actionITR_100.setChecked(False)
                        log.error("Connection to ITR100 failed"
                                    f"with the following exception: {exc}")
                    try:
                        self.unit100 = self.ITR100.unit
                        with suppress(TypeError):
                            self.ITR100.emission = True
                    except AttributeError:
                        self.actionITR_100.setChecked(False)
                        log.error("Could not activate ITR100.")
                else:
                    self.actionITR_100.setChecked(False)
                    log.warning("Pressure too high for ITR 100."
                                " Activation just possible for pressures < 0.1 mbar!")
            except (ValueError, AttributeError):
                log.error("Connect ITR200 before connecting ITR100.")
                self.actionITR_100.setChecked(False)
            except Exception as exc:
                log.error(f"Could not connect to ITR100: {exc}")
                self.actionITR_100.setChecked(False)
        else:
            try:
                with suppress(TypeError):
                    self.ITR100.emission = False
                del self.ITR100
                log.info("Disconnected ITR100.")
            except AttributeError:
                pass

    @pyqtSlot(bool)
    def connectMotors(self, connect: bool) -> None:
        try:
            self.starter_director = StarterDirector("PICO2.starter", communicator=self.communicator)
            self.starter_director.start_tasks(["experiment_t", "VUVspec_t"])
        except Exception:
            log.exception("One or both of the tasks are already running.")
        if connect:
            try:
                self.motorCard1 = TMCMotorDirector("PICO2.VUVspec_a",
                                                   communicator=self.communicator)
                self.motorCard2 = TMCMotorDirector("PICO2.Experiment_a",
                                                   communicator=self.communicator)
            except Exception as exc:
                log.error(f"Could not start motor task with the following error: {exc}")

        else:
            try:
                self.motorCard1.close()
                self.motorCard2.close()
            except AttributeError:
                pass
            except Exception as exc:
                log.error(f"Motor connection failed with the following error error code: {exc}")
            self.actionMotors.setChecked(False)

    @pyqtSlot(bool)
    def connectAll(self, connect: bool) -> None:
        """Connect all devices."""
        if connect:
            self.connectITR100(True)
            self.connectITR200(True)
            self.connectMotors(True)

    # %% Grating functions

    @pyqtSlot()
    def setWavelength(self) -> None:
        order = self.sbOrdnung.value()
        velocity = self.motorCard1.get_configuration("VUVspec_m")["positioningSpeed"]
        print(velocity)
        if order < 0:
            try:
                if self.sbSetWavelength.value() <= self.stepsToWavelength(
                    self.motorCard1.get_actual_position("VUVspec_m")):
                    self.motorCard1.vol_move_to("VUVspec_m", self.wavelengthToSteps(
                        self.sbSetWavelength.value()),
                        velocity=velocity)
                else:
                    self.motorCard1.vol_move_to("VUVspec_m", self.wavelengthToSteps(
                        self.sbSetWavelength.value() + 100),
                        velocity=velocity)
                    self.waitMotorTimer.start(200)
            except Exception as exc:
                log.error(f"Could not move grating motor: {exc}")
        else:
            try:
                if self.sbSetWavelength.value() >= self.stepsToWavelength(
                    self.motorCard1.get_actual_position("VUVspec_m")):
                    self.motorCard1.vol_move_to("VUVspec_m", self.wavelengthToSteps(
                        self.sbSetWavelength.value()),
                        velocity=velocity)
                else:
                    self.motorCard1.vol_move_to("VUVspec_m", self.wavelengthToSteps(
                        self.sbSetWavelength.value() - 100),
                        velocity=velocity)
                    self.waitMotorTimer.start(200)
            except Exception as exc:
                log.error(f"Could not move grating motor: {exc}")


    @pyqtSlot()
    def setPumpWavelength(self) -> None:
        wavelength = self.sbSetPumpWavelength.value() / self.sbHarmonics.value()
        self.sbSetWavelength.setValue(wavelength)
        self.setWavelength()

    @pyqtSlot()
    def waitMotorGrating(self) -> None:
        try:
            if self.motorCard1.get_position_reached("VUVspec_m"):
                self.waitMotorTimer.stop()
                self.motorCard1.vol_move_to("VUVspec_m", self.wavelengthToSteps(
                    self.sbSetWavelength.value()),
                    velocity=self.motorCard1.get_configuration("VUVspec_m")["positioningSpeed"])
            else:
                pass
        except Exception:
            pass

    @pyqtSlot()
    def zeroPosition(self) -> None:
        try:
            if 0 > self.motorCard1.get_actual_position("VUVspec_m"):
                self.motorCard1.vol_move_to("VUVspec_m", self.wavelengthToSteps(0),
                                            velocity=self.motorCard1.get_configuration("VUVspec_m")["positioningSpeed"])
            else:
                self.motorCard1.vol_move_to("VUVspec_m", -5000,
                                            velocity=self.motorCard1.get_configuration("VUVspec_m")["positioningSpeed"])
                self.waitMotorZeroTimer.start(200)
        except Exception as exc:
            log.error(f"Could not move grating motor: {exc}")

    @pyqtSlot()
    def waitMotorGratingZero(self):
        settings = QtCore.QSettings()
        config = settings.value("Grating")
        motor = config["motorNumber"]
        try:
            if self.motorCard1.get_position_reached("VUVspec_m"):
                self.waitMotorZeroTimer.stop()
                self.motorCard1.vol_move_to("VUVspec_m", 0,
                                        velocity=self.motorCard1.get_configuration("VUVspec_m")["positioningSpeed"])
            else:
                pass
        except Exception:
            pass

    def stepstoAngel(self,steps:int) -> float:
        """
        Calculates the Angel Theta of the Grating based on the Motorposition.
        
        :param steps: Motoposition in samllest form so 256
        :return: float of the resulting angel Theta from Neumann masster Thesis in Gradient
        """
        settings = QtCore.QSettings()

        slope = settings.value("Slope", type=float)
        b = settings.value("Yintersect", type=float)
    
        result = np.radians(slope * steps + b)
        return result
    
    def stepsToWavelength(self, steps: int) -> float:
        """
        Calculate the wavelength in nm for the given step position.
        
        :param steps: Motoposition 
        :return: float of the Resuling Wavelength in nm
        """
        settings = QtCore.QSettings()
        config = settings.value('Grating')
        
        resolution = self.motorCard1.get_configuration("VUVspec_m")["stepResolution"]
        steps = round(steps * 2**(8-resolution), 0)
        # print(resolution, steps)
        angels = self.stepstoAngel(steps)

        order = self.sbOrdnung.value()
        
        G = settings.value("G", type=float) * 1e3
        K = np.radians(settings.value("K", type=float))
        # print(f"K ={K}")
        # print(f"G ={G}")
        # print(f"steps ={steps}")
        # print(f"angels ={angels}")
        if np.pi/2 - K < abs(angels):
            wl = 0
        else:
            if order != 0:
                wl = 2 / G / order * np.sin(angels) * np.cos(K) * 1e9  # Calc WL in nm
            else:
                wl = 0
        return wl

    def wavelengthToSteps(self, wavelength: float) -> int:
        """
        :param wavelength: Wavelength in nm
        :return: int Motorposition in 256 settings of the -1 order
        """
        settings = QtCore.QSettings()

        resolution = self.motorCard1.get_configuration("VUVspec_m")["stepResolution"]
        
        order = self.sbOrdnung.value()
        
        G = settings.value("G", type=float) * 1e3
        K = np.radians(settings.value("K", type=float))
        slope = settings.value("Slope", type=float)
        b = settings.value("Yintersect", type=float)

        print(f"K = {K}")
        print(f"G = {G}")
        print(f"order = {order}")
        print(f"wavelength = {wavelength}")

        Theta = np.degrees(np.arcsin(G * order * wavelength * 1e-9 / 2 / np.cos(K)))   # Theta in degree
        ress = (Theta - b) / slope 
        print(round(ress * 2**(resolution-8)))
        return round(ress * 2**(resolution-8))
    
    @pyqtSlot()
    def setEMTvoltage(self):
        voltage = self.sbEMTvoltage.value() / 600
        self.task_w.write(voltage)
        log.info(f"New EMT voltage: {voltage} V")

    # %% HWP

    @pyqtSlot()
    def setPowerHWP(self):
        try:
            self.motorCard2.move_to_units("HWPexp_m", self.sbSetHWP.value())
        except Exception as exc:
            log.error(f"Could not set HWP: {exc}")

    # %% Pressure sensor functions

    @pyqtSlot()
    def configureITR100(self):
        self.config100 = Configuration100(self)
        self.config100.open()

    @pyqtSlot()
    def configureITR200(self):
        self.config200 = Configuration200(self)
        self.config200.open()

    @pyqtSlot()
    def openCalibration(self):
        self.calibrate = Calibrate(self)
        self.calibrate.open()


class Configuration100(QtWidgets.QDialog):

    lbTTRstatus: QtWidgets.QLabel
    lbDegasStatus: QtWidgets.QLabel
    lbUnit: QtWidgets.QLabel
    lbGasCorrection: QtWidgets.QLabel
    lbVariableGasCorrection: QtWidgets.QLabel
    lbSensorStatus: QtWidgets.QLabel
    cbTTRstatusOptions: QtWidgets.QComboBox
    cbUnit: QtWidgets.QComboBox
    cbGas: QtWidgets.QComboBox
    sbVariableGasCorrection: QtWidgets.QSpinBox
    pbSetTTRstatus: QtWidgets.QAbstractButton
    pbStartDegas: QtWidgets.QAbstractButton
    pbStopDegas: QtWidgets.QAbstractButton
    pbSetUnit: QtWidgets.QAbstractButton
    pbSetGasCorrection: QtWidgets.QAbstractButton
    pbSetVariableGasCorrection: QtWidgets.QAbstractButton

    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent

        uic.loadUi("data/ConfigITR100.ui", self)
        self.show()

        self.parent.readoutTimer.stop()

        self.readValues()
        self.pbSetTTRstatus.clicked.connect(self.setTTRemission)
        self.pbStartDegas.clicked.connect(self.startDegas)
        self.pbStopDegas.clicked.connect(self.stopDegas)
        self.pbSetUnit.clicked.connect(self.setUnit)
        self.pbSetGasCorrection.clicked.connect(self.setGasCorrection)
        # self.pbSetVariableGasCorrection.clicked.connect(self.setVariableGas)

    def close(self):
        settings = QtCore.QSettings()
        self.parent.readoutTimer.start(settings.value("ReaderInterval"))

    def readValues(self):
        self.lbTTRstatus.setText(str(self.parent.ITR100.TTR))
        self.lbDegasStatus.setText(str(self.parent.ITR100.degas))
        self.lbUnit.setText(self.parent.ITR100.unit)
        self.lbGasCorrection.setText(self.parent.ITR100.gasCorrection)
        # self.lbVariableGasCorrection.setText(self.parent.ITR100.variableGasCorrection)
        self.lbSensorStatus.setText(self.parent.ITR100.status)

    @pyqtSlot()
    def setTTRemission(self):
        with suppress(TypeError):
            self.parent.ITR100.TTR = bool(self.cbTTRstatusOptions.currentText())
        self.lbTTRstatus.setText(self.parent.ITR100.TTR)

    @pyqtSlot()
    def startDegas(self):
        if self.parent.ITR100.pressure < 2e-5:
            with suppress(TypeError):
                self.parent.ITR100.degas = True
            self.lbDegasStatus.setText(self.parent.ITR100.degas)
        else:
            log.info("Pressure too high to degas.")

    @pyqtSlot()
    def stopDegas(self):
        with suppress(TypeError):
            self.parent.ITR100.degas = False
        self.lbDegasStatus.setText(self.parent.ITR100.degas)

    @pyqtSlot()
    def setUnit(self):
        with suppress(TypeError):
            self.parent.ITR100.unit = self.cbUnit.currentText()
        self.lbUnit.setText(self.parent.ITR100.unit)
        self.parent.unit100 = self.lbUnit.text()

    @pyqtSlot()
    def setGasCorrection(self):
        try:
            with suppress(TypeError):
                self.parent.ITR100.gasCorrection = self.cbGas.currentText()
            self.lbGasCorrection.setText(self.parent.ITR100.gasCorrection)
        except Exception:
            self.lbGasCorrection.setText(self.parent.ITR100.gasCorrection)

    @pyqtSlot()
    def setVariableGas(self):
        with suppress(TypeError):
            self.parent.ITR100.variableGasCorrection = self.sbVariableGasCorrection.value()
        self.lbVariableGasCorrection.setText(self.parent.ITR100.variableGasCorrection)


class Configuration200(QtWidgets.QDialog):

    lbError: QtWidgets.QLabel
    lbDegasStatus: QtWidgets.QLabel
    lbFilament: QtWidgets.QLabel
    lbSensorStatus: QtWidgets.QLabel
    cbFilament: QtWidgets.QComboBox
    pbStartDegas: QtWidgets.QAbstractButton
    pbStopDegas: QtWidgets.QAbstractButton
    pbSetFilament: QtWidgets.QAbstractButton

    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent

        uic.loadUi("data/ConfigITR200.ui", self)
        self.show()

        self.degasTimer = QtCore.QTimer()
        self.degasTimer.timeout.connect(self.readValues)

        self.pbStartDegas.clicked.connect(self.startDegas)
        self.pbStopDegas.clicked.connect(self.stopDegas)
        self.pbSetFilament.clicked.connect(self.filament)

        self.readValues()

    def readValues(self):
        status = self.parent.ITR200.status()
        if status["filament1"] == "active":
            self.lbFilament.setText("Filament 1")
        else:
            self.lbFilament.setText("Filament 2")
        self.lbSensorStatus.setText(status["emission"])
        if status["emission"] == "degas":
            self.lbDegasStatus.setText("ON")
        else:
            self.lbDegasStatus.setText("OFF")
            try:
                self.degasTimer.stop()
            except Exception:
                pass
        error = self.parent.ITR200.error()
        self.lbError.setText(error)

    @pyqtSlot()
    def startDegas(self):
        if self.parent.ITR200.pressure < 7.2e-6:
            with suppress(TypeError):
                self.parent.ITR200.degas(True)
            log.info("Start degas process.")
            self.degasTimer.start(1000)
        else:
            log.warning("Pressure too high to degas.")

    @pyqtSlot()
    def stopDegas(self):
        log.info("Degas process interrupted.")
        self.parent.ITR200.degas(False)

    @pyqtSlot()
    def filament(self):
        """Select the filament, that should be used with the ITR200."""
        filament = self.cbFilament.currentText()
        if filament == "Filament 2":
            self.parent.ITR200.select_filament(2)
        else:
            self.parent.ITR200.select_filament(1)
        log.info("ITR200 filament selected.")


class Calibrate(QtWidgets.QDialog):

    lbPosition: QtWidgets.QLabel
    sbPosition: QtWidgets.QSpinBox
    pbSet: QtWidgets.QAbstractButton
    pbStop: QtWidgets.QAbstractButton

    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent

        uic.loadUi("data/Calibration.ui", self)
        self.show()

        self.pbSet.clicked.connect(self.setPosition)
        self.pbStop.clicked.connect(self.stopMotor)

    @pyqtSlot()
    def setPosition(self):
        position = self.sbPosition.value()
        self.parent.motorCard1.vol_move_to("VUVspec_m", position,
                                       velocity=self.parent.motorCard1.get_configuration("VUVspec_m")["positioningSpeed"])

    @pyqtSlot()
    def stopMotor(self):
        self.parent.motorCard1.stop("VUVspec_m")


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(psExperimentControl)

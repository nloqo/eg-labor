# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 11:45:19 2022

@author: THG-User
"""

from pymeasure.test import expected_protocol

from devices.np_photonics import RockFiberLaser, ControllerFlags


def test_pcb_type():
    with expected_protocol(RockFiberLaser,
                           [(":SYSTem:HARDware:TYPE?", "141")]
                           ) as inst:
        assert inst.pcb_type == "FLM-0141"


def test_temperature_monitor2_sensor_fault():
    with expected_protocol(RockFiberLaser,
                           [(":SYST:TMON2:FLAG?", "1")]
                           ) as inst:
        assert inst.heater1.sensor_fault is True


def test_temperature_monitor2_temperature():
    with expected_protocol(RockFiberLaser,
                           [(":SYST:TMON2:TEMP?", "12.3")]
                           ) as inst:
        assert inst.heater1.temperature == 12.3


def test_controller_state_monitor1():
    with expected_protocol(RockFiberLaser,
                           [(":SYST:TCON1:FLAG?", "1")],
                           ) as inst:
        assert inst.tec2.controller_state == ControllerFlags.TEMPERATURE_STABLE


import pytest

from pymeasure.test import expected_protocol

from devices.sensirion import SCD30


def test_init():
    with expected_protocol(SCD30,
                           []):
        pass  # test init


class Test_manual_examples:
    """These examples are in the manual"""

    def test_trigger_continuous_measurement(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x06\x00\x36\x00\x00\x60\x64", b"\x61\x06\x00\x36\x00\x00\x60\x64")],
        ) as inst:
            # ambient pressure is 0 for no special compensation
            inst.start_measurement()

    def test_stop_continuous_measurement(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x06\x00\x37\x00\x01\xF0\x64", b"\x61\x06\x00\x37\x00\x01\xF0\x64")],
        ) as inst:
            inst.stop_measurement()

    @pytest.mark.xfail(reason="Not implemented")
    def test_set_measurement_interval(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x06\x00\x25\x00\x02\x10\x60", b"\x61\06\x00\x26\00\x02\x10\x60")],
        ) as inst:
            inst.measurement_interval = 2  # s

    @pytest.mark.xfail(reason="Not implemented")
    def test_get_measurement_interval(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x03\x00\x25\x00\x01\x9c\x61", b"\x61\x03\x02\x00\x02\xB9\x8d")],
        ) as inst:
            assert inst.measurement_interval == 2  # s

    def test_get_data_ready(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x03\x00\x27\x00\x01\x3d\xa1", b"\x61\x03\x02\x00\x01\xf9\x8c")],
        ) as inst:
            assert inst.data_ready is True

    def test_read_measurement(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x03\x00\x28\x00\x06\x4c\x60",
              b"\x61\03\x0c\x43\xdb\x8c\x2e\x41\xd9\xe7\xff\x42\x43\x3a\x1b\50\x07")],
        ) as inst:
            assert inst.get_values() == pytest.approx((439, 27.2, 48.8), rel=1e3)

    @pytest.mark.xfail(reason="Not implemented")
    def test_deactivate_asc(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x06\x00\x3a\x00\00\xa0\x67", b"\x61\x06\x00\x3a\00\x00\xa0\x67")],
        ) as inst:
            inst.asc_enabled = False

    @pytest.mark.xfail(reason="Not implemented")
    def test_get_asc(self):
        with expected_protocol(
            SCD30,
            [(b"\x61\x03\x00\x3a\x00\01\xad\xa7", b"\x61\x03\x02\x00\x00\x38\x4c")],
        ) as inst:
            assert inst.asc_enabled is False

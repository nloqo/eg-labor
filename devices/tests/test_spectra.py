import pytest

from pymeasure.test import expected_protocol

from devices.spectra import QuantaRay


def test_id():
    with expected_protocol(
        QuantaRay,
        [("*IDN?", "Spectra Physics,QUANTA-RAY-PRO190-10,24041,0452-0045A/456-6600A")],
    ) as inst:
        assert inst.id == "Spectra Physics,QUANTA-RAY-PRO190-10,24041,0452-0045A/456-6600A"


@pytest.mark.parametrize("response, value", (("ON", True), ("OFF", False)))
def test_emission_enabled_getter(response, value):
    # TODO verify that OFF is the correct answer
    with expected_protocol(QuantaRay, [("ON?", response)]) as inst:
        assert inst.emission_enabled is value


@pytest.mark.parametrize("command, value", (("ON", True), ("OFF", False)))
def test_emission_enabled_setter(command, value):
    with expected_protocol(QuantaRay, [(command, None)]) as inst:
        inst.emission_enabled = value


def test_amplifier_power_setpoint_getter():
    with expected_protocol(QuantaRay, [("APFN?", "100.0 %")]) as inst:
        assert inst.amplifier_power_setpoint == 100.0


def test_amplifier_power_setpoint_setter():
    with expected_protocol(QuantaRay, [("APFN 100.000000", None)]) as inst:
        inst.amplifier_power_setpoint = 100


def test_oscillator_power_setpoint_getter():
    with expected_protocol(QuantaRay, [("OPFN?", "100.0 %")]) as inst:
        assert inst.oscillator_power_setpoint == 100.0


def test_oscillator_power_setpoint_setter():
    with expected_protocol(QuantaRay, [("OPFN 100.000000", None)]) as inst:
        inst.oscillator_power_setpoint = 100


def test_shot_count_getter():
    with expected_protocol(QuantaRay, [("SHOTs?", "1134")]) as inst:
        assert inst.shot_count == 1134


def test_dlok_enabled_getter():
    with expected_protocol(QuantaRay, [("DLOK?", "2 1234567 31.2")]) as inst:
        assert inst.dlok_enabled is True


@pytest.mark.parametrize("command, value", (("ON", True), ("OFF", False)))
def test_dlok_enabled_setter(command, value):
    with expected_protocol(QuantaRay, [("DLOK " + command, None)]) as inst:
        inst.dlok_enabled = value


def test_dlok_power():
    with expected_protocol(QuantaRay, [("DLOK?", "2 1234567 31.2")]) as inst:
        assert inst.dlok_power == 31.2


def test_watchdog_timeout_setter():
    with expected_protocol(QuantaRay, [("WATChdog 5.100000", None)]) as inst:
        inst.watchdog_timeout = 5.1


@pytest.mark.parametrize(
    "messages, content",
    (
        (("1 827", "301 810", "0 0"), [(301, 17)]),  # from manual
        (("0 827", "0 0"), []),  # interpolated from manual
        (),
    ),
)
def read_history(messages, content):
    with expected_protocol(
        QuantaRay,
        [("READ:HIST?", messages[0])] + [(None, messages[i]) for i in range(1, len(messages))],
    ) as inst:
        assert inst.history == content


def test_temperature_getter():
    with expected_protocol(QuantaRay, [("READ:TEMP?", "22.01 C")]) as inst:
        assert inst.temperature == 22.01


def test_status_getter():
    with expected_protocol(QuantaRay, [("*STB?", "137")]) as inst:
        assert (
            inst.status
            == QuantaRay.Status.EMISSION
            | QuantaRay.Status.QUESTIONABLE
            | QuantaRay.Status.OPERATION
        )


def test_status_questionable_getter():
    with expected_protocol(QuantaRay, [("STAT:QUES?", "512")]) as inst:
        assert inst.status_question == QuantaRay.Questionable.OSCILLATOR_HV_FAILURE


"""raw code for copy paste
def test_emission_enabled_getter():
    with expected_protocol(QuantaRay,
                           [()]
                           ) as inst:
        assert inst.


def test_emission_enabled_setter():
    with expected_protocol(QuantaRay,
                           [(, None)]
                           ) as inst:
        inst.
"""

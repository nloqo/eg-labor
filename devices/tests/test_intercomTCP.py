import pytest

# File to test
from devices import intercomTCP


@pytest.fixture(autouse=True, scope='module')
def validCommands():
    return intercomTCP.validCommands


class Mock_connection:
    """An empty connection."""

    def __init__(self, received=b''):
        self.received = received

    def sendall(self, content=b''):
        self.sent = content

    def recv(self, count):
        return self.received[:count]


class Mock_long_connection(Mock_connection):

    def __init__(self, receiving):
        super().__init__(receiving.pop())
        self.receiving = receiving

    def sendall(self, content=b''):
        self.sent = content
        if content == b'ACK00000':
            try:
                self.received = self.receiving.pop()
            except AttributeError:
                pass  # no second received message


@pytest.fixture()
def connection():
    return Mock_connection()


class Test_readMessage:
    @pytest.fixture()
    def connection(self):
        return Mock_connection(b'SEA00006ABCDEF')

    @pytest.fixture()
    def decoded(self, connection):
        return intercomTCP.readMessage(connection)

    def test_typ(self, decoded):
        assert decoded[0] == 'SEA'

    def test_content(self, decoded):
        assert decoded[1] == b"ABCDEF"


class Test_readMessageLong:
    @pytest.fixture()
    def connection(self):
        return Mock_long_connection([b'SEA00007GHIJKLM', b'LON00006ABCDEF'])

    @pytest.fixture()
    def data(self, connection):
        return intercomTCP.readMessageLong(connection)

    def test_typ(self, data):
        assert data[0] == 'SEA'

    def test_content(self, data):
        assert data[1] == b'ABCDEFGHIJKLM'


class Test_sendMessage:

    def test_wrong_type(self):
        with pytest.raises(AssertionError):
            intercomTCP.sendMessage(None, '000')

    def test_too_long(self):
        with pytest.raises(AssertionError):
            intercomTCP.sendMessage(None, 'SET', 100000 * b'0123456789')

    def test_SEA(self, connection):
        intercomTCP.sendMessage(connection, 'SEA', b"ABCDEF")
        assert connection.sent == b'SEA00006ABCDEF'


def test_sendMessageLong(connection):
    a = b'0123456789'
    connection.received = b'ACK00000'
    b = intercomTCP.sendMessageLong(connection, 'SET', 100000 * a)
    assert b == 2

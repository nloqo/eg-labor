# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 16:47:11 2023

@author: moneke
"""

from enum import IntFlag
import pytest


class S(IntFlag):
    WL_MODULATION_RANGE = 0x2
    WL_MODULATION_DC = 0x8
    WL_MODULATION_INTERNAL = 0x10
    WL_MODULATION_EXTERNAL = 0x20
    SIGNAL_OUTPUT = 0x40
    CONSTANT_CURRENT = 0x200
    AUTOSTART = 0x800


class FakeNKTP:
    def __init__(self):
        self.setup = 0

    def update_setup(self, set=0, unset=0):
        """Update the setup, setting those defined in set and unsetting thos in unset."""
        setup = self.setup
        self.setup = (setup | set) & ~unset


@pytest.fixture()
def laser():
    return FakeNKTP()


@pytest.mark.parametrize(
    "s0, set, unset, s1",
    (
        # adding something, removing not yet set
        (0, S.WL_MODULATION_INTERNAL | S.WL_MODULATION_EXTERNAL, S.SIGNAL_OUTPUT, 0x30),
        # removing a set value, adding another
        (S.SIGNAL_OUTPUT, S.WL_MODULATION_DC, S.SIGNAL_OUTPUT, S.WL_MODULATION_DC),
        # adding an already present value and more
        (
            S.SIGNAL_OUTPUT | S.CONSTANT_CURRENT,
            S.SIGNAL_OUTPUT | S.AUTOSTART,
            0,
            S.SIGNAL_OUTPUT | S.CONSTANT_CURRENT | S.AUTOSTART,
        ),
    ),
)
def test_update_setup(laser, s0, set, unset, s1):
    laser.setup = s0
    laser.update_setup(set, unset)
    assert laser.setup == s1

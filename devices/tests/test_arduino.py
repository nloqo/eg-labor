"""
Test the Arduino

it is written according to TemperatureController.ino 1.3.0
"""

from pymeasure.test import expected_protocol

from devices.arduino import Arduino, ControllerArduino


# Arduino
def test_ping():
    with expected_protocol(
        Arduino,
        [("p", "pong")],
        startup_sleep=0,
    ) as inst:
        assert inst.ping() == "pong"


def test_data():
    with expected_protocol(
        Arduino,
        [("l", "0.000\t102.4\t256\t512\t768\t1024")],
        startup_sleep=0,
    ) as inst:
        assert inst.data == [0, 0.1, 0.25, 0.5, 0.75, 1]


# ControllerArduino
def test_factor_p():
    with expected_protocol(
        ControllerArduino,
        [("kp-4.500000", "ACK-Kp")],
        startup_sleep=0,
    ) as inst:
        inst.factor_p = -4.5


def test_factor_i():
    with expected_protocol(
        ControllerArduino,
        [("ki-4.500000", "ACK-Ki")],
        startup_sleep=0,
    ) as inst:
        inst.factor_i = -4.5


def test_setpoint():
    with expected_protocol(
        ControllerArduino,
        [("ks123.456000", "ACK-setpoint")],
        startup_sleep=0,
    ) as inst:
        inst.setpoint = 123.456


def test_integral():
    with expected_protocol(
        ControllerArduino,
        [("kx500.000000", "ACK-integral")],
        startup_sleep=0,
    ) as inst:
        inst.integral_value = 500


def test_get_pid():
    with expected_protocol(
        ControllerArduino,
        [("d", "902.82, p:-250.00, i:-2.50, setpoint: 390.00")],
        startup_sleep=0,
    ) as inst:
        assert inst.get_pid_values() == [902.82, -250, -2.5, 390, None]


def test_write_to_EEPROM():
    with expected_protocol(
        ControllerArduino,
        [("e", "ACK EEPROM saved.")],
        startup_sleep=0,
    ) as inst:
        inst.write_to_EEPROM()


def test_enable_control():
    with expected_protocol(
        ControllerArduino,
        [("o1", "ACK enable control")],
        startup_sleep=0,
    ) as inst:
        inst.control_enabled = True


def test_disable_control():
    with expected_protocol(
        ControllerArduino,
        [("o0", "ACK disable control")],
        startup_sleep=0,
    ) as inst:
        inst.control_enabled = False

# -*- coding: utf-8 -*-
"""
Created on Wed May 25 11:03:47 2022

@author: moneke
"""

import pytest
from pytest import raises

from pymeasure.test import expected_protocol

from devices.ophir import OphirSerial, Nova, Modes, LegacyModes, ScreenModes, Capabilities


def test_read_processes_response():
    with expected_protocol(
        OphirSerial,
        [("$II", "* USBD 113217 SH2USB"), (None, "* Afas 23 45e-3")],
    ) as inst:
        assert inst.read() == " Afas 23 45e-3"


def test_read_raises_error():
    with expected_protocol(
        OphirSerial,
        [("$II", "* USBD 113217 SH2USB"), (None, "?COM ERROR")],
    ) as inst:
        with raises(ConnectionError) as exc:
            inst.read()
        assert str(exc.value) == "COM ERROR"


def test_init():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB")]) as instr:
        assert instr.device_name == "USBD"


# INFORMATION ABOUT DEVICE AND HEAD
def test_id():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$II", "* USBD 113217 SH2USB")]
    ) as instr:
        assert instr.id == ["USBD", "113217", "SH2USB"]


def test_head1():
    # From manual
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$HI", "* TH 12345 03AP 00000183")]
    ) as instr:
        assert instr.head_information == {
            "sensortype": "TH",
            "serialnumber": "12345",
            "name": "03AP",
            "capabilities": Capabilities.POWER | Capabilities.ENERGY | Capabilities(384),
        }


def test_head2():
    # From manual
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$HI", "* PY 22323 PE10-C 80000003")]
    ) as instr:
        assert instr.head_information == {
            "sensortype": "PY",
            "serialnumber": "22323",
            "name": "PE10-C",
            "capabilities": Capabilities.POWER | Capabilities.ENERGY | Capabilities.FREQUENCY,
        }


# CONFIG
# In general


def test_mode_getter():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$MM0", "*2")]) as instr:
        assert instr.mode == Modes.POWER


def test_mode_setter():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$MM3", "*")]) as instr:
        instr.mode = Modes.ENERGY


def test_units():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$SI", "* W")]) as instr:
        assert instr.units == "W"


# Range
def test_range_getter():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$RN", "*4")]) as instr:
        assert instr.range == 4


def test_range_setter():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$WN1", "*")]) as instr:
        instr.range = 1


def test_range_values():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$WN-1", "*")]) as instr:
        instr.range_map_values = True
        instr.range_values = {
            "AUTO": -1,
            "30.0mW": 0,
            "3.00mW": 1,
            "300uW": 2,
            "30.0uW": 3,
            "3.00uW": 4,
            "300nW": 5,
            "30.0nW": 6,
        }
        instr.range = "AUTO"


def test_getAllRanges():
    with expected_protocol(
        OphirSerial,
        [
            ("$II", "* USBD 113217 SH2USB"),
            ("$AR", "* 3 AUTO 30.0mW 3.00mW 300uW 30.0uW 3.00uW 300nW 30.0nW"),
        ],
    ) as instr:
        assert instr.getAllRanges() == {
            "AUTO": -1,
            "30.0mW": 0,
            "3.00mW": 1,
            "300uW": 2,
            "30.0uW": 3,
            "3.00uW": 4,
            "300nW": 5,
            "30.0nW": 6,
        }


# Wavelength


# Special measurement


def test_diffuser_getter():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$DQ", "*1 OUT IN")]
    ) as instr:
        assert instr.diffuser == "OUT"


def test_diffuser_setter():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$DQ2", "*2 OUT IN")]
    ) as instr:
        instr.diffuser = "IN"


def test_filter_getter():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$FQ", "*1 OUT IN")]
    ) as instr:
        assert instr.filter == "OUT"


def test_filter_setter():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$FQ2", "*2 OUT IN")]
    ) as instr:
        instr.filter = "IN"


def test_mains_getter():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$MA", "* 1 50Hz 60Hz")]
    ) as instr:
        assert instr.mains == "50Hz"


def test_mains_setter():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$MA1", "*1 50Hz 60Hz")]
    ) as instr:
        instr.mains = "50Hz"


# Other settings


def test_screen_mode_setter():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$FS1", "*")]) as instr:
        instr.screen_mode = ScreenModes.ENERGY


# Measurements


def test_energy():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$SE", "*1.300E-5")]
    ) as instr:
        assert instr.energy == 1.3e-5


def test_energy_flag():
    with expected_protocol(OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$EF", "*1")]) as instr:
        assert instr.energy_flag is True


def test_frequency():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$SF", "*1.000E3")]
    ) as instr:
        assert instr.frequency == 1000


def test_power():
    with expected_protocol(
        OphirSerial, [("$II", "* USBD 113217 SH2USB"), ("$SP", "*1.300E-5")]
    ) as instr:
        assert instr.power == 1.3e-5


def test_position():
    with expected_protocol(
        OphirSerial,
        [("$II", "* USBD 113217 SH2USB"), ("$BT", "* F 00000000 X -1.50 Y -0.9 S 6.50")],
    ) as instr:
        assert instr.position == [-1.5, -0.9, 6.5]


# Nova (version I) tests


def test_Nova_power():
    with expected_protocol(
        Nova, [("$II", "* USBD 113217 SH2USB"), ("$SP10", "*1.300E-5")]
    ) as instr:
        instr.power_timeout = 1000
        assert instr.power == 1.3e-5


@pytest.mark.parametrize(
    "mode, string",
    (
        (LegacyModes.POSITION, "$FB"),
        (LegacyModes.ENERGY, "$FE"),
        (LegacyModes.POWER, "$FP"),
        (LegacyModes.EXPOSURE, "$FX"),
    ),
)
def test_Nova_mode(mode, string):
    with expected_protocol(
        Nova,
        [("$II", "* USBD 113217 SH2USB"), (string, "*")],
    ) as instr:
        instr.mode = mode

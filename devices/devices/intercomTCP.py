"""
Library for communication between programs and with labview.

classes
------
Intercom : address="127.0.0.1", port=12345, timeout=10
    Convenience class for connecting, reading and sending tcp socket messages.
timeout
    For convenience: Just the socket.timeout error raised at a timeout.
Publisher : host="localhost", port=11100
    Publish measurement data via zmq.
Listener
    Listening on incoming data or control messages via zmq.
Communicator
    Communicating with a zmq server.

functions
-----------
connect : address="127.0.0.1", port=12345, timeout=None
    Establish and return a connected socket connection.
readMessage : connection
    Receive and decode a message. Return `typ` and `content`.
readMessageLong : connection
    Receive a possibly long message,  Return `typ` and `content`.
sendMessage: connection, typ, content=b''
    Encode and send a message with `typ` and `content`.
sendMessageLong : connection, typ, content=b''
    Send a message longer than 1e6 bytes with `typ` and `content`.

Created on Thu Mar  4 13:04:32 2021 by Benedikt Burger
"""

import pickle
import socket
from socket import timeout as timeout  # noqa: F401  for dependencies
from typing import Any, Optional

"""
Protocol definition for pure TCP sockets:
    - A three letter uppercase command out of the list 'validCommands'.
    - 5 digits (zero padded on the left) indicating the length of the following
    content in bytes. The first two parts are utf-8 encoded.
    - The content with a number of bytes indicated by the previous length.

ASCII version of a list is newline (\n) separated.
ASCII version of a dictionary has its elements separated by a newline (\n).
An element consists in a {key}:{type}:{value} triple. Type is:
    - f for float
    - i for int
    - s for string
    - n for None/Null
"""

# list of valid commands
validCommands = (
    # General communication
    "ACK",  # acknowledge any command without a specified response type
    "ERR",  # an error message (pickled)/ascii
    "ECO",  # echo back the message
    "SAV",  # save storage to disk
    "CMD",  # execute some command
    "RDY",  # query or indicate readiness
    "OFF",  # tell the other side to switch off
    "LON",  # A long message, more messages will follow: response is ACK
    # The last message contains the real command.
    # Data communication with pickled objects
    "GET",  # get variables (pickled iterable): response is SET
    "SET",  # set variables (pickled dictionary)
    "DEL",  # delete variables (pickled iterable)
    "PUB",  # set and publish variables (pickled dictionary)
    "DMP",  # get all variables: response is SET
    # Data communication via ASCII encoding
    "GEA",  # get variables (Ascii list): response is SEA
    "SEA",  # set variables (Ascii dictionary)
    "DEA",  # del variables (Ascii list)
    "PUA",  # set and publish variables (Ascii dictionary)
)


def connect(
    address: str = "127.0.0.1", port: int = 12345, timeout: Optional[int] = None  # noqa: F811
) -> socket.socket:
    """Create and return a TCP connection.

    Parameters
    ----------
    address : str, default "127.0.0.1"
        Host address
    port : int
        Host port
    timeout : int, default None
        Timeout in s. If None, no timeout.

    Returns
    -------
    socket.socket
        TCP connection.
    """
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.settimeout(timeout)
    connection.connect((address, port))
    return connection


def readMessage(connection: socket.socket) -> tuple[str, bytes]:
    """Receive and decode a message. Return `typ` and `content`."""
    headerLength = 3 + 5  # 3 letters for type, 5 for length
    status = 0
    readBuffer = b""
    while status < 2:
        read = connection.recv(1064)
        if not read:
            break
        readBuffer += read
        if status == 0 and len(readBuffer) >= headerLength:  # Reading header.
            typ = readBuffer[0:3].decode("utf-8")
            length = int(readBuffer[3:headerLength].decode("utf-8"))
            readBuffer = readBuffer[headerLength:]
            status = 1
        if status == 1 and len(readBuffer) >= length:  # Reading message.
            content = readBuffer[:length]
            return typ, content
    raise ValueError


def readMessageLong(connection: socket.socket) -> tuple[str, bytes]:
    """Receive a possibly long message,  Return `typ` and `content`."""
    typ, content = readMessage(connection)
    while typ == "LON":
        sendMessage(connection, "ACK")
        typ, newContent = readMessage(connection)
        content += newContent
    return typ, content


def sendMessage(connection: socket.socket, typ: str, content: bytes = b"") -> None:
    """Encode and send a message with `typ` and `content`."""
    assert typ in validCommands, "Unknown type."
    assert len(content) < 1e6, "Content too long."
    header = f"{typ}{len(content):05}".encode("utf-8")
    connection.sendall(header + content)


def sendMessageLong(connection: socket.socket, typ: str, content: bytes = b"") -> int:
    """Send a message longer than 1e6 bytes with `typ` and `content`."""
    assert typ in validCommands, "Unknown type."
    start = 0
    count = 1
    while len(content[start:]) >= 1e6:
        sendMessage(connection, "LON", content[start : start + 99999])
        start += 99999
        count += 1
        typ, content = readMessage(connection)
        if typ == "ERR":
            raise ConnectionError(f"Long communication interrupted {content}")
    sendMessage(connection, typ, content[start:])
    return count


class Intercom:
    """An intercom channel using one-time connections.

    :param str address: IP address to connect to. Defaults to localhost.
    :param int port: Port to connect to.
    :param int timeout: Timeout in seconds. None means indefinitely.
    """

    def __init__(
        self, address: str = "127.0.0.1", port: int = 12345, timeout: int = 10  # noqa: F811
    ) -> None:
        self.address: tuple[str, int, int] = (address, port, timeout)

    def send(
        self, typ: str, content: bytes = b"", timeout: int = -1  # noqa: F811
    ) -> tuple[str, bytes]:
        """
        Send a message and return the answer.

        Parameters
        ----------
        typ : str
            Command type of the message.
        content : bytes, optional
            The data to transmit, if any.
        timeout : positive int, optional
            The timeout in s for this send operation. If no value is given,
            the timeout of this instance is used. None waits indefinitely.

        Returns
        -------
        str
            Command type of response.
        bytes
            Content of response.
        """
        if timeout < 0:
            connection = connect(*self.address)
        else:
            connection = connect(*self.address[:2], timeout)
        sendMessage(connection, typ, content)
        read = readMessage(connection)
        connection.close()
        return read

    def sendAscii(
        self, typ: str, content: str, timeout: int = -1  # noqa: F811
    ) -> tuple[str, bytes]:
        """Send a message with ASCII content."""
        return self.send(typ, content.encode("ascii"), timeout)

    def sendObject(
        self, typ: str, content: Any, timeout: int = -1  # noqa: F811
    ) -> tuple[str, Any]:
        """Send python objects and return python objects if answer is 'SET'."""
        typ, content = self.send(typ, pickle.dumps(content), timeout)
        if typ in ("SET", "DMP"):
            return typ, pickle.loads(content)
        else:
            return typ, content


"Example of Code listening to incoming connections and handles them."
# class Listener(QtCore.QObject):
#     """Listening to new connections."""

#     def __init__(self, host, port):
#         """Initialize the Thread."""
#         super().__init__()
#         print(f"Listener init at {host}:{port}.")
#         listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#         # Die Adresse sofort wieder benutzen,
#         # nicht den 2 Minuten Timer nach Stop des vorherigen Servers warten.
#         listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#         listener.bind((host, port))
#         listener.listen(3)
#         self.listener = listener
#         self.stop = False
#         self.threadpool = QtCore.QThreadPool()
#         self.signals = self.ListenerSignals()
#         self.address = (host, port)

#     class ListenerSignals(QtCore.QObject):
#         """Signals for the Listener."""
#         dataReady = QtCore.pyqtSignal(dict)
#         command = QtCore.pyqtSignal(dict)
#         save = QtCore.pyqtSignal()

#     def __del__(self):
#         """On deletion close connection."""
#         try:
#             self.listener.close()
#         except Exception as exc:
#             print(f"Closure failed with {exc}.")

#     def listen(self):
#         """Listen for connections and emit it via signals."""
#         print(f"Start listen on {self.address}.")
#         self.parent.signals.starter.disconnect(self.listen)
#         while not self.stop:
#             connection, addr = self.listener.accept()
#             # print(addr)
#             handler = ConnectionHandler(connection, self.signals)
#             self.threadpool.start(handler)
#         self.listener.close()
#         print(f"Listen on {self.address} stopped.")

#     def close(self):
#         """Close the listener."""
#         try:
#             self.listener.close()
#         except Exception as exc:
#             print(f"Closure failed with {exc}.")


# class ConnectionHandler(QtCore.QRunnable):
#     """Handling a single connection emitting a signal with the read data."""

#     def __init__(self, parent, connection, signals):
#         """Initialize the handler."""
#         super().__init__()
#         self.parent = parent
#         self.connection = connection
#         self.signals = signals

#     def run(self):
#         """Handle the connection."""
#         typ, content = readMessage(self.connection)
#         # Set data.
#         if typ == 'SET':
#             data = pickle.loads(content)
#             try:
#                 assert type(data) is dict
#             except AssertionError:
#                 sendMessage(self.connection, 'ERR',
#                             "Send a dictionary, please!".encode('ASCII'))
#             else:
#                 self.signals.dataReady.emit(data)
#                 sendMessage(self.connection, 'ACK')
#         self.connection.close()

from warnings import warn

from pyleco_extras.gui_utils.base_settings import BaseSettings  # noqa

warn(
    "Deprecated to use `BaseSettings`, import it from pyleco_extras instead.",
    FutureWarning,
)

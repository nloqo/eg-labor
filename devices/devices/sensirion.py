import struct
from time import sleep

from pymeasure.instruments import Instrument


class SCD30(Instrument):
    """Sensirion SCD30 air quality sensor: CO2, temperature, humidity.

    This is in modbus mode (setting "SEL" pin to some voltage between 2 V and 4 V).
    The supported baud rate is 19200 Baud with 8 Data bits, 1 Start bit and 1 Stop bit, no Parity
    bit.
    """

    def __init__(self, adapter, name="SCD30"):
        super().__init__(
            adapter=adapter,
            name=name,
            includeSCPI=False,
            asrl={"baud_rate": 19200},
        )

    def write_command(
        self, function_code: int, address: int, data: tuple[int, ...] = ()
    ):
        self.write_bytes(bytes((0x61, function_code, *address.to_bytes(2), *data)))

    @property
    def data_ready(self) -> bool:
        """Get whether data is ready to be read (bool)."""
        self.write_command(
            function_code=0x03, address=0x27, data=(0x00, 0x01, 0x3D, 0xA1)
        )
        # self.write_bytes(bytes([0x61, 0x03, 0x00, 0x27, 0x00, 0x01, 0x3D, 0xA1]))
        response = self.read_bytes(7)
        assert response[:4] == b"\x61\x03\x02\x00", f"response {response!r} differs."
        return bool(response[4])

    def get_values(self) -> tuple[float, float, float]:
        """Get co2 (in ppm), temperature (in °C), humidity (in percent)."""
        # self.write_bytes(b"\x61\x03\x00\x28\x00\x06\x4c\x60")
        self.write_command(function_code=0x03, address=0x28, data=(0, 6, 0x4C, 0x60))
        sleep(0.01)  # > 3 ms
        response = self.read_bytes(17)
        co2 = struct.unpack(">f", response[3:3+4])[0]
        temp = struct.unpack(">f", response[3+4:3+4+4])[0]
        hum = struct.unpack(">f", response[3+8:3+8+4])[0]
        return co2, temp, hum

    def start_measurement(self, ambient_pressure=None):
        """Start a measurement."""
        if ambient_pressure is None:
            # self.write_bytes(b"\x61\x06\x00\x36\x00\x00\x60\x64")
            self.write_command(
                function_code=0x06, address=0x36, data=(0, 0, 0x60, 0x64)
            )
        else:
            raise NotImplementedError("not yet due missing CRC calculation")
        self.read_bytes(8)

    def stop_measurement(self):
        """Stop a measurement."""
        self.write_command(function_code=6, address=0x37, data=(0, 1, 0xF0, 0x64))
        self.read_bytes(8)

# -*- coding: utf-8 -*-
"""
For testing devices publish random values.

Created on Sat Jul  2 06:25:50 2022 by Benedikt Moneke
"""

from random import random
import time

from pyleco.utils.data_publisher import DataPublisher


def publishing(interval=0.1, standalone=False):
    """Publish random values every `interval` seconds."""
    if standalone:
        raise ValueError("Not allowed anymore")
    publisher = DataPublisher(full_name="Test.randomPublisher")
    while True:
        try:
            publisher.send_legacy({"random": random()})
            publisher.send_data({"random2": random(), "random3": random()})
            time.sleep(interval)
        except KeyboardInterrupt:
            break


if __name__ == "__main__":
    print("Start publishing random values. Stop with 'Ctrl+C'.")
    publishing()

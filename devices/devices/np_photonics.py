# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 11:43:07 2022 by Benedikt Moneke
"""

from enum import IntFlag

from pymeasure.instruments import Instrument, Channel


class ControllerFlags(IntFlag):
    """Flags for the temperature controller."""

    TEMPERATURE_STABLE = 1
    ALARM1 = 2
    ALARM2 = 4
    OVER_TEMPERATURE = 8
    UNDER_TEMPERATURE = 16
    SHUTDOWN = 32


class TemperatureChannel(Channel):
    """Temperature monitor and control channel."""

    ControllerFlags = ControllerFlags

    # monitor properties
    sensor_fault = Instrument.measurement(
        ":SYST:TMON{ch}:FLAG?",
        """Measure a sensor fault (bool).""",
        cast=bool,
    )

    temperature = Instrument.measurement(
        ":SYST:TMON{ch}:TEMP?",
        """Measure the temperature in °C (float).""",
    )

    # controller properties
    temperature_setpoint = Instrument.control(
        "SYST:TCON{ch}:SPO?",
        "SYST:TCON{ch}:SPO %f",
        """Control the temperature setpoint in °C (float).""",
    )

    controller_state = Instrument.measurement(
        ":SYST:TCON{ch}:FLAG?",
        """Get the controller state.""",
        get_process=lambda s: ControllerFlags(int(s)),
    )


class RockFiberLaser(Instrument):
    """Control the Rock Fiber Laser Seeder by NP Photonics.

    FLM-005
    Commands are SCPI style

    Channelnames
    FLM-0107 (twin-pump): TEC 1, TEC 2, Heater 1, Heater 2, PCB
    """

    tec1 = Instrument.ChannelCreator(TemperatureChannel, 0)
    tec2 = Instrument.ChannelCreator(TemperatureChannel, 1)
    heater1 = Instrument.ChannelCreator(TemperatureChannel, 2)
    heater2 = Instrument.ChannelCreator(TemperatureChannel, 3)
    pcb = Instrument.ChannelCreator(TemperatureChannel, 4)

    def __init__(self, adapter, name="Rock Fiber Laser", **kwargs):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=True,
            write_termination="\r\n",
            read_termination="\r\n",
            asrl={
                "baud_rate": 57600,
                # 'data_bits': 8,
                # 'stop_bits': 1,
                # 'parity': 0,
            },
            **kwargs,
        )

    pcb_type = Instrument.measurement(
        ":SYSTem:HARDware:TYPE?",
        """Measure the pcb type""",
        cast=str,  # type: ignore
        get_process=lambda v: f"FLM-0{v}",
    )

    def save(self):
        """Save configuration to non-volatile memory."""
        self.write("*SAV")

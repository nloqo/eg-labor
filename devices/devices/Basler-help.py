# flake8: noqa
"""
Hints for usage of Basler Dart Cameras

Created on Sun Feb  7 09:31:14 2021 by Benedikt Moneke

Examples on Github: https://github.com/basler/pypylon
"""

# import third party tools
from pypylon import pylon  # offers many enumerations for settings
from pypylon import genicam


class ImageEventHandler(pylon.ImageEventHandler):
    """Handle image events of the camera."""

    def OnImageGrabbed(self, camera, grabResult):
        """What to do if an image is grabbed (saved to memory)."""
        return grabResult.Array

    def OnImageSkipped(self, camera, countOfSkippedImages):
        """What to do if a grabbed image is not retrieved."""
        return countOfSkippedImages


class ConfigurationHandler(pylon.ConfigurationEventHandler):
    """Handle configuration events."""

    def OnOpen(self, camera):
        """Do things before the camera is opened."""
        print(f"Camera {camera} has been opened.")

    def OnOpened(self, camer):
        """Handle an camera after it has been opened."""

    def OnCameraDeviceRemoved(self, camera):
        """Handle a removed camera device (which has to be opened)."""
        del camera  # destroy the camera upon detected removal


class CPixelFormatAndAoiConfiguration(pylon.ConfigurationEventHandler):
    """A configuration event handler, example by Basler."""

    def OnOpened(self, camera):
        """On opening the camera."""
        try:
            # Maximize the Image AOI.
            if genicam.IsWritable(camera.OffsetX):
                camera.OffsetX = camera.OffsetX.Min
            if genicam.IsWritable(camera.OffsetY):
                camera.OffsetY = camera.OffsetY.Min
            camera.Width = camera.Width.Max
            camera.Height = camera.Height.Max

            # Set the pixel data format.
            camera.PixelFormat = "Mono8"
        except genicam.GenericException as e:
            raise genicam.RuntimeException(
                "Could not apply configuration. GenICam::GenericException \
                                            caught in OnOpened method msg=%s"
                % e.what()
            )


def examples():
    """Examples."""
    pylon.AcquireContinuousConfiguration()  # the default configuration handler: deactivates triggers and compression, sets AcquisitionMode to Continuous

    # Get the transport layer factory.
    tlFactory: pylon.TlFactory = pylon.TlFactory.GetInstance()

    # Get all attached devices and exit application if no device is found.
    devices = tlFactory.EnumerateDevices()
    if len(devices) == 0:
        raise pylon.RuntimeException("No camera present.")

    # Create an instant camera and attach the device to it.
    cam = pylon.InstantCamera()
    cam.Attach(tlFactory.CreateDevice(devices[0]))
    # Configure the device with the default config and above config replacing earlier configs and deleting the configuration objects afterwards.
    cam.RegisterConfiguration(
        pylon.AcquireContinuousConfiguration(),
        pylon.RegistrationMode_ReplaceAll,
        pylon.Cleanup_Delete,
    )
    cam.RegisterConfiguration(
        ConfigurationHandler(), pylon.RegistrationMode_Append, pylon.Cleanup_Delete
    )
    # Register an image event handler additionally to existing ones, keeping it afterwards.
    imHandler = ImageEventHandler()
    cam.RegisterImageEventHandler(
        imHandler, pylon.RegistrationMode_Append, pylon.Cleanup_None
    )

    """
    Image structure:

    At least for 'daA1280-54um' cameras, the USB port is on the left (point of view of the camera).
    The image data is row-major (first index is the row). The origin is in the top left (point of view of the camera).
    """

    # Take a single picture
    timeoutMS = 5000
    result: pylon.GrabResult = cam.GrabOne(timeoutMS)
    if result.GrabSucceeded():
        array = result.GetArray()

    info = cam.GetDeviceInfo()

    # Informationen, for example:
    info.GetModelName()
    info.GetSerialNumber()
    info.GetFriendlyName()  # User given name

    # The same can be queried for existence or set:
    info.IsFriendlyNameAvailable()
    info.SetFriendlyName()

    """
    Setup of the camera via one of the following methods
    - register configuration objects
    - code after Open() call
    - preconfiguration via pylon Viewer
    - loading from disk
    The last three may require a Null configuration object to be loaded.
    """

    # In order to read or write values, the device has to be opened!

    # Config get settings for ints
    cam.Parameter.Min
    cam.Parameter.Max
    cam.Parameter.Inc  # step size
    # or as methods
    cam.Parameter.GetMin()
    cam.Parameter.GetMax()
    cam.Parameter.GetInc()  # step size
    # get current value, two possibilities
    value = cam.Parameter()  # it has to be called, not like a property!
    value = cam.Parameter.GetValue()

    # write the current value, two equivalent possibilities
    cam.Parameter = value
    cam.Parameter.SetValue(value)

    # Parameter can be
    # int
    cam.SensorWidth  # of the sensor
    cam.SensorHeight
    cam.OffsetX  # of area of interest
    cam.OffsetY
    cam.Width  # of area of interest
    cam.Height
    # Enumeration
    cam.Pixelformat  # e.g. "Mono8"
    # Float
    cam.ExposureTime


# Output of dir(cam):
"""
AcquisitionControl
AcquisitionFrameRate  # float, limits the framerate in Hz
AcquisitionMode
AcquisitionStart
AcquisitionStartStopExecutionEnable
AcquisitionStop
AnalogControl
Attach
AutoBacklightCompensation
AutoBacklightCompensationRaw
AutoExposureTimeLowerLimit
AutoExposureTimeLowerLimitRaw
AutoExposureTimeUpperLimit
AutoExposureTimeUpperLimitRaw
AutoFunctionControl
AutoFunctionProfile
AutoGainLowerLimit
AutoGainLowerLimitRaw
AutoGainLowerLimitRaw_All
AutoGainLowerLimit_All
AutoGainUpperLimit
AutoGainUpperLimitRaw
AutoGainUpperLimitRaw_All
AutoGainUpperLimit_All
AutoTargetBrightness
AutoTargetBrightnessRaw
BinningHorizontal
BinningHorizontalMode
BinningVertical
BinningVerticalMode
BlackLevel
BlackLevelRaw
BlackLevelRaw_All
BlackLevelSelector
BlackLevel_All
BslUSBSpeedMode
CalibLockChallenge
CalibLockResponse
CalibStore
CalibrationControl
CameraContext
ChunkNodeMapsEnable
ClearBufferModeEnable
Close
ContrastEnhancement
ContrastEnhancementRaw
DefectPixelCorrectionMode
DeregisterCameraEventHandler
DeregisterConfiguration
DeregisterImageEventHandler
DestroyDevice
DetachDevice
DeviceControl
DeviceFirmwareVersion
DeviceIndicatorMode
DeviceInfo
DeviceLinkSelector
DeviceLinkSpeed
DeviceLinkSpeed_0
DeviceLinkThroughputLimit
DeviceLinkThroughputLimitMode
DeviceLinkThroughputLimitMode_0
DeviceLinkThroughputLimit_0
DeviceManufacturerInfo
DeviceModelName
DeviceReset
DeviceSFNCVersionMajor
DeviceSFNCVersionMinor
DeviceSFNCVersionSubMinor
DeviceScanType
DeviceSerialNumber
DeviceUserID
DeviceVendorName
DeviceVersion
DigitalIOControl
EpRecoveries
EpRetries
EpUnderruns
EventGrabber
EventGrabberNodeMap
ExecuteSoftwareTrigger
ExposureAuto  # enum: 0 'Off', 1 'Once', 2 'Continuous'
ExposureTime  # float
ExposureTimeRaw
Gain
GainAuto
GainRaw
GainRaw_All
GainSelector
Gain_All
Gamma
GammaRaw
GetCameraContext
GetCameraEventWaitObject
GetDeviceInfo
GetEventGrabberNodeMap
GetGrabResultWaitObject
GetGrabStopWaitObject
GetInstantCameraNodeMap
GetNodeMap
GetQueuedBufferCount
GetSfncVersion
GetStreamGrabberNodeMap
GetTLNodeMap
GrabCameraEvents
GrabLoopThreadPriority
GrabLoopThreadPriorityOverride
GrabLoopThreadTimeout
GrabLoopThreadUseTimeout
GrabOne
HasOwnership
Height
HeightMax
ImageFormatControl
ImageQualityControl
InstantCameraNodeMap
InternalGrabEngineThreadPriority
InternalGrabEngineThreadPriorityOverride
Is1394
IsBcon
IsCameraDeviceRemoved
IsCameraLink
IsCxp
IsGigE
IsGrabbing
IsOpen
IsPylonDeviceAttached
IsUsb
LineDebouncerTime
LineFormat
LineInverter
LineMode
LineSelector
LineSource
LineStatus
LineStatusAll
LnkErrors
LnkRecoveries
MaxNumBuffer
MaxNumGrabResults
MaxNumQueuedBuffer
MigrationModeActive
MonitorModeActive
NodeMap
NumEmptyBuffers
NumQueuedBuffers
NumReadyBuffers
OffsetX
OffsetY
Open
OutputQueueSize
OverlapMode
PayloadFinalTransfer1Size
PayloadFinalTransfer2Size
PayloadSize
PayloadTransferCount
PayloadTransferSize
PhyErrors
PixelDynamicRangeMax
PixelDynamicRangeMin
PixelFormat
PixelSize
QueuedBufferCount
RegisterCameraEventHandler
RegisterConfiguration
RegisterImageEventHandler
ResultingFramePeriod
ResultingFrameRate
RetrieveResult
ReverseX
ReverseY
Root
SensorHeight
SensorShutterMode
SensorWidth
SequenceErrors
SetBufferFactory
SetCameraContext
StartGrabbing
StartGrabbingMax
StaticChunkNodeMapPoolSize
StopGrabbing
StreamGrabber
StreamGrabberNodeMap
TLNodeMap
TLParamsLocked
TestControl
TestPattern
TestPendingAck
TransportLayer
TransportLayerControl
TriggerActivation
TriggerMode  # On
TriggerSelector  # FrameStart
TriggerSoftware  # Software, Line0
TriggerSource
UsbResets
UserOutputSelector
UserOutputValue
UserSetControl
UserSetDefault
UserSetLoad
UserSetSave
UserSetSelector
WaitForFrameTriggerReady
Width
WidthMax
_GetBaseType_ChunkNodeMapsEnable
_GetBaseType_GrabCameraEvents
_GetBaseType_GrabLoopThreadPriority
_GetBaseType_GrabLoopThreadPriorityOverride
_GetBaseType_GrabLoopThreadTimeout
_GetBaseType_GrabLoopThreadUseTimeout
_GetBaseType_InternalGrabEngineThreadPriority
_GetBaseType_InternalGrabEngineThreadPriorityOverride
_GetBaseType_MaxNumBuffer
_GetBaseType_MaxNumGrabResults
_GetBaseType_MaxNumQueuedBuffer
_GetBaseType_MonitorModeActive
_GetBaseType_NumEmptyBuffers
_GetBaseType_NumQueuedBuffers
_GetBaseType_NumReadyBuffers
_GetBaseType_OutputQueueSize
_GetBaseType_StaticChunkNodeMapPoolSize
_Get_ChunkNodeMapsEnable
_Get_GrabCameraEvents
_Get_GrabLoopThreadPriority
_Get_GrabLoopThreadPriorityOverride
_Get_GrabLoopThreadTimeout
_Get_GrabLoopThreadUseTimeout
_Get_InternalGrabEngineThreadPriority
_Get_InternalGrabEngineThreadPriorityOverride
_Get_MaxNumBuffer
_Get_MaxNumGrabResults
_Get_MaxNumQueuedBuffer
_Get_MonitorModeActive
_Get_NumEmptyBuffers
_Get_NumQueuedBuffers
_Get_NumReadyBuffers
_Get_OutputQueueSize
_Get_StaticChunkNodeMapPoolSize
_Set_ChunkNodeMapsEnable
_Set_GrabCameraEvents
_Set_GrabLoopThreadPriority
_Set_GrabLoopThreadPriorityOverride
_Set_GrabLoopThreadTimeout
_Set_GrabLoopThreadUseTimeout
_Set_InternalGrabEngineThreadPriority
_Set_InternalGrabEngineThreadPriorityOverride
_Set_MaxNumBuffer
_Set_MaxNumGrabResults
_Set_MaxNumQueuedBuffer
_Set_MonitorModeActive
_Set_NumEmptyBuffers
_Set_NumQueuedBuffers
_Set_NumReadyBuffers
_Set_OutputQueueSize
_Set_StaticChunkNodeMapPoolSize
__class__
__delattr__
__dict__
__dir__
__doc__
__eq__
__format__
__ge__
__getattr__
__getattribute__
__gt__
__hash__
__init__
__init_subclass__
__le__
__lt__
__module__
__ne__
__new__
__reduce__
__reduce_ex__
__repr__
__setattr__
__sizeof__
__str__
__subclasshook__
__swig_destroy__
__weakref__
this
thisown
"""

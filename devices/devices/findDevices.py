# -*- coding: utf-8 -*-
"""
Find connected devices.

Find all connected devices, list of properties and find the TMC cards among
them. Store the TMCs Port information in the appropriate Settings object.

vid and pid (vendor/product ID) are ints
serial_number is a string


Functions
---------
findPort : vid, pid, serial_number
    Return the port name as a string for a given device hardware information.


Created on Mon May  9 08:55:19 2022 by Benedikt Moneke
"""

import logging
from serial.tools import list_ports
from warnings import warn

from pymeasure.instruments.resources import find_serial_port as _find_serial_port
from qtpy import QtCore


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


def findPort(vid: int, pid: int, sn: str) -> str:
    """Find the port name ('COM5') of the first device with the given USB information.

    It is better to use `pymeasure.instruments.resources.find_serial_port`.

    :param int vid: Vendor ID.
    :param int pid: Product ID.
    :param str sn: Serial number.
    :return: Port as a string as `device` attribute returned from `list_ports.comports`.
    """
    warn("Deprecated, use `pymeasure.instruments.resources.find_serial_port` instead.",
         FutureWarning)
    for port in sorted(list_ports.comports()):
        if port.vid == vid and port.pid == pid and port.serial_number == str(sn):
            return port.device
    raise AttributeError("No device found for the given data.")


def find_serial_port(vendor_id=None, product_id=None, serial_number=None) -> str:
    """Find the VISA port name of the first serial device with the given USB information.

    Deprecated, use `pymeasure.instruments.resources.find_serial_port` instead!

    Use `None` as a value if you do not want to check for that parameter.

    .. code-block:: python

        resource_name = find_serial_port(vendor_id=1256, serial_number="SN12345")
        dmm = Agilent34410(resource_name)

    :param int vid: Vendor ID.
    :param int pid: Product ID.
    :param str sn: Serial number.
    :return str: Port as a VISA string for a serial device (e.g. "ASRL5" or "ASRL/dev/ttyACM5").
    """
    warn(
        "Deprecated, use `pymeasure.instruments.resources.find_serial_port` instead.",
        FutureWarning,
    )
    return _find_serial_port(
        vendor_id=vendor_id, product_id=product_id, serial_number=serial_number
    )


def get_tmc_address(port: str | int) -> int:
    """Get the TMC address of that port.

    :param port: port name ("COM4") or port number (5).
    """
    from pytrinamic.connections import ConnectionManager
    from pytrinamic.modules import TMCM6110
    if isinstance(port, int):
        port = f"COM{port}"
    try:
        connectionManager = ConnectionManager(f"--port {port}")
        motorCard = TMCM6110(connectionManager.connect(), True)
    except Exception as exc:
        log.exception(f"Reading motor card at {port} failed.", exc_info=exc)
        return -1
    else:
        address = motorCard.get_global_parameter(66, 0)
        connectionManager.disconnect()
        return address


def search_ports() -> tuple[list[str], list[str]]:
    """Search all ports and report the devices.

    :return: List of device information, List of TMC devices
    """
    devices = ["Port: description, hwid, vid, pid, serial number\n",
               "================================================\n"]
    TMCs: list[str] = []
    settings = QtCore.QSettings("NLOQO")
    for port in sorted(list_ports.comports()):
        devices.append((f"{port.device}: {port.description}, {port.hwid}, "
                        f"{port.vid}, {port.pid}, {port.serial_number}\n"))
        match port.vid, port.pid:  # noqa
            # different vid/pid combinations known for TMC motor cards
            case (0x16d0, 0x059e) | (0x16d0, 0x0650) | (0x16d0, 0x0653) | (0x2a3c, 0x0100):  # noqa
                address = get_tmc_address(port.device)
                settings.setValue(f"TMCport/{address}", port.device[3:])
                TMCs.append(f"Port: {port.device},\tTMC Address: {address:3}\n")
    return devices, TMCs


getAddress = get_tmc_address
searchPorts = search_ports


if __name__ == "__main__":
    devices, TMCs = search_ports()
    with open("COMs.txt", "w") as file:
        file.writelines(devices)
        file.writelines(["\n",
                         "TMC cards\n",
                         "=========\n"])
        file.writelines(TMCs)

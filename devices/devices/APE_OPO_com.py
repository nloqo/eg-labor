"""This class offers the possibility for remote control of various parameters of the APE OPO."""


import pyvisa
from time import perf_counter


class APE_OPO:
    # %% Initialization
    def __init__(self, COM, query_delay=0.01):
        self._last_value: dict[str, tuple[float, float]] = {}
        rm = pyvisa.ResourceManager()  # create a resource manager
        if isinstance(COM, int):
            COM = f"ASRL{COM}"
        try:
            self._connection = rm.open_resource(
                COM,
                baud_rate=115200,
                data_bits=8,
                parity=pyvisa.constants.Parity.none,
                stop_bits=pyvisa.constants.StopBits.one,
                write_termination=",",
                timeout=1000,
            )
            self._connection.clear()
        except Exception:
            # re raise the exception and hand it over to caller
            raise
        # read starts with space and ends with \n\rOPO>

    def __del__(self):
        """Close the connection on garbage collection."""
        try:
            self._connection.close()
        except AttributeError:
            pass  # no connection exists

    def query(self, command, bytes=2):
        """Send a command and receive the answer."""
        try:
            self._connection.write(command)
            got = self._connection.read_bytes(bytes)
        except Exception:
            raise
        return got

    # %% OPO Commands

    def setWavelength(self, wavelength):
        """Set the wavelength of the OPO and return whether the command has been received."""
        got = self.query(f"SLQ{wavelength}", 1)
        if got[0] == 1:
            return False
        else:
            self._connection.read_bytes(2)
            return True

    @property
    def humidity(self):
        """Returns the humidity value as integer in %."""
        time, value = self._last_value.get("humidity", (0, 0))
        now = perf_counter()
        if now > time + 0.5:
            got = self.query("GH", 1)
            value = got[0]
            self._last_value["humidity"] = (now, value)
        return value

    @property
    def getPowerIR(self):
        """Returns the internally measured IR power in arbitrary units and returns integer."""
        time, value = self._last_value.get("IR", (0, 0))
        now = perf_counter()
        if now > time + 0.5:
            got = self.query("GPI")
            value = got[0] * 256 + got[1]
            self._last_value["IR"] = (now, value)
        return value

    @property
    def getPowerVIS(self):
        """Returns the internally measured VIS power in arbitrary units and returns integer."""
        time, value = self._last_value.get("VIS", (0, 0))
        now = perf_counter()
        if now > time + 0.5:
            got = self.query("GPV")
            value = got[0] * 256 + got[1]
            self._last_value["VIS"] = (now, value)
        return value

    @property
    def TemperatureSHG(self):
        """Read out the actual SHG temperature and returns it in °C with one decimal after comma.
        The first byte represents the 10^2 digit, while the second byte represents the other 3
        digits.
        """
        time, value = self._last_value.get("temperature", (0, 0))
        now = perf_counter()
        if now > time + 0.5:
            got = self.query("GTS")
            value = (got[0] * 256 + got[1]) / 10
            self._last_value["temperature"] = (now, value)
        return value

    def setTemperatureSHG(self, T):
        """Set the temperature analogous to the TemperatureSHG function.
        No response returned after sending value."""
        T = T * 10
        self._connection.write(f"STS{T:04}")

    @property
    def piezoVoltage(self):
        got = self.query("GZ")
        return got[0]

    @piezoVoltage.setter
    def setPiezoVoltage(self, voltage):
        return

    def setBaudRate(self):
        return

    # %% Spectrometer Commands

    @property
    def spectrometerGain(self):
        """Read out the actual gain of the spectrometer."""
        got = self.query("GG", 1)
        if got[0] == 1:
            return (1,)
        elif got[0] == 2:
            return (3,)
        elif got[0] == 3:
            return (10,)
        elif got[0] == 4:
            return (30,)
        elif got[0] == 5:
            return (100,)
        elif got[0] == 6:
            return 300

    @spectrometerGain.setter
    def setSpectrometerGain(self, gain):
        """Set the spectrometer gain with an integer from 1-6. No response expected."""
        self._connection.write(f"GN{gain}")

    def setZoomWindow(self, middleWavelength):
        if middleWavelength < 1000:
            self._connection.write(f"LZ0{middleWavelength * 10}")
        else:
            self.connection.write(f"LZ{middleWavelength * 10}")

    def zoomIn(self):
        """Zoom into the spectrum."""
        self._connection.write("SZI")

    def zoomOut(self):
        """Zoom out of the spectrum."""
        self._connection.write("SZO")

    def specWindowWidth(self, width):
        """Set the window width for the spectrum with an integer from 1-7. No response expected.
        '1' for 5 nm; '2' for 10 nm; '3' for 20 nm; '4' for 50 nm; '5' for 100 nm; '6' for 200 nm;
        '7' for 500 nm."""
        self._connection.write(f"F{width}")

    def setOffset(self, offset):
        """Set offset with integer values between 0 and 255."""
        self._connection.write(f"SO{offset:03}")

    def spectrumValues(self, length):
        """Set the data set length of the spectrum to 256, 512 or 1024."""
        self._connection.write(f"W{length:04}")

    def getCalibration(self):
        return

    def getSpectralPeakValue(self):
        return

    def getSpectrumAll(self, spectrumValues):
        """Get all spectrometer values."""
        self._connection.write("GSH")
        if spectrumValues == 256:
            got = self._connection.read_bytes(524)
            data = [got[i] * 256 + got[i + 1] for i in range(0, 512, 2)]
            left = got[512] * 256 + got[513]
            right = got[514] * 256 + got[515]
            peak = got[516] * 256 + got[517]
            FWHM = got[518] * 256 + got[519]
            centre = got[520] * 256 + got[521]
            lockStatus = got[523]
            return {
                "data": data,
                "boundaries": [left, right],
                "peak": peak,
                "FWHM": FWHM,
                "centre": centre,
                "status": lockStatus,
            }
        elif spectrumValues == 512:
            got = self._connection.read_bytes(1036)
            data = [got[i] * 256 + got[i + 1] for i in range(0, 1024, 2)]
            left = got[1025] * 256 + got[1026]
            right = got[1027] * 256 + got[1028]
            peak = got[1029] * 256 + got[1030]
            FWHM = got[1031] * 256 + got[1032]
            centre = got[1033] * 256 + got[1034]
            lockStatus = got[1035]
            return {
                "data": data,
                "boundaries": [left, right],
                "peak": peak,
                "FWHM": FWHM,
                "centre": centre,
                "status": lockStatus,
            }
        elif spectrumValues == 1024:
            got = self._connection.read_bytes(2060)
            data = [got[i] * 256 + got[i + 1] for i in range(0, 2048, 2)]
            left = got[2049] * 256 + got[2050]
            right = got[2051] * 256 + got[2052]
            peak = got[2053] * 256 + got[2054]
            FWHM = got[2055] * 256 + got[2056]
            centre = got[2057] * 256 + got[2058]
            lockStatus = got[2059]
            return {
                "data": data,
                "boundaries": [left, right],
                "peak": peak,
                "FWHM": FWHM,
                "centre": centre,
                "status": lockStatus,
            }

    def getSpectrumFast(self):
        return

    # %% Correlation Commands

    def correlationAutoGain(self):
        self._connection.write("CAG")

    @property
    def correlatorAverage(self):
        """Get the correlator average as value between 1 and 64."""
        self._connection.write("CA")
        got = self._connection.read()
        return got

    @correlatorAverage.setter
    def setCorrelatorAverage(self, average):
        """Send correlation average value as integer between 1 and 64."""
        self._connection.write(f"CAV{average}")

    @property
    def correlatorFilter(self):
        """Get the setting for the filter: '0' for off, '1' for on."""
        self._connection.write("GF")
        got = self._connection.read()
        return got

    @correlatorFilter.setter
    def setCorrelatorFilter(self, switch):
        """Set the filter on or off."""
        self._connection.write(f"CF{switch}")

    def setCorrelatorGain(self, gain):
        """Set the correlator gain from 0 to 255."""
        self._connection.write("CG{gain:03}")

    def setCorrelatorPreamp(self, preamp):
        return

    def setCorrelatorRange(self, rgn):
        """Set the correlator range with: '1' for 150 fs; '2' for 500 fs;
        '3' for 1.5 ps; '4' for 5.0 ps and '5' for 15 ps."""
        self._connection.write(f"CR{rgn}")

    def setCorrelatorSmooth(self, switch):
        """Set smooth on with '1' and off with '0'."""
        self._connection.write(f"CS{switch}")

    def getCorrelatorFWHM(self):
        """Receive information about FWHM as 0/256 ... 256/256 from scan range."""
        self._connection.write("GAT")
        return self._connection.read()

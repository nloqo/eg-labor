# -*- coding: utf-8 -*-
"""
Created on Wed Aug 23 13:40:47 2023

@author: ernst
"""

from pymeasure.instruments import Instrument
from pyvisa.constants import Parity, StopBits


def faultList():
    return [['1', 'FAULT', 'FAULT: Emission lamp interlock is open'],
            ['2', 'FAULT', 'FAULT: External interlock is open'],
            ['3', 'FAULT', 'FAULT: Cover interlock is open'],
            ['4', 'FAULT', 'FAULT: SHG temperature is out of range'],
            ['7', 'FAULT', 'FAULT: THG temperature is out of range'],
            ['10', 'SERVO', 'FAULT: Baseplate temperature is out of range (Check chiller operation/settings)'],
            ['11', 'FAULT', 'FAULT: Baseplate temperature too low after diodes on (Check chiller operation/settings)'],
            ['14', 'FAULT', 'FAULT: Power supply voltage is out of range'],
            ['16', 'CRITICAL FAULT: Diode 1 over expected current'],
            ['17', 'CRITICAL FAULT: Diode 2 over expected current'],
            ['19', 'CRITICAL FAULT: Diode 1 under expected voltage'],
            ['20', 'CRITICAL FAULT: Diode 2 under expected voltage'],
            ['21', 'CRITICAL FAULT: Diode 1 over expected voltage'],
            ['22', 'CRITICAL FAULT: Diode 2 over expected voltage'],
            ['23', 'CRITICAL FAULT: Diode 1 under expected current'],
            ['24', 'CRITICAL FAULT: Diode 2 under expected current'],
            ['25', 'FAULT', 'FAULT: Diode 1 1-wire error'],
            ['26', 'FAULT', 'FAULT: Diode 2 1-wire error'],
            ['27', 'FAULT', 'FAULT: Laser head 1-wire error'],
            ['28', 'FAULT', 'FAULT: I2C 1-wire Bridge error'],
            ['29', 'FAULT', 'FAULT: Shutter connection invalid'],
            ['30', 'FAULT', 'FAULT: Startup fault cycle keyswitch to enable laser'],
            ['31', 'FAULT', "FAULT: Shutter isn't in the expected state"],
            ['32', 'FAULT', "FAULT: OPS servo can't control temperature"],
            ['33', 'FAULT', "FAULT: RES servo can't control temperature"],
            ['34', 'FAULT', "FAULT: BRF servo can't control temperature"],
            ['35', 'FAULT', "FAULT: ETA servo can't control temperature"],
            ['36', 'FAULT', 'FAULT: Power lock light loop error'],
            ['37', 'FAULT', "FAULT: SHG servo can't control temperature"],
            ['38', 'FAULT', "FAULT: THG servo can't control temperature"],
            ['39', 'FAULT', 'FAULT: Shutter interlock is open'],
            ['40', 'CRITICAL FAULT: Chiller water level is low'],
            ['41', 'CRITICAL FAULT: Chiller temperature is out of range'],
            ['42', 'CRITICAL FAULT: Chiller communication is lost'],
            ['43', 'CRITICAL FAULT: Chiller flow fault detected'],
            ['44', 'CRITICAL FAULT: Diode interlock is open'],
            ['45', 'FAULT', 'FAULT: Chiller temperature is too far from its set temperature'],
            ['46', 'FAULT', "FAULT: FAP1 servo can't control temperature"],
            ['47', 'FAULT', "FAULT: FAP2 servo can't control temperature"],
            ['48', 'FAULT', 'FAULT: FAP1 temperature is out of range'],
            ['49', 'FAULT', 'FAULT: FAP2 temperature is out of range'],
            ['52', 'FAULT', 'FAULT: OPS temperature is out of range'],
            ['53', 'FAULT', 'FAULT: Resonator temperature is out of range'],
            ['57', 'CRITICAL FAULT: An interlock is open'],
            ['58', 'CRITICAL FAULT: Interlock sense conflict (Current OK)'],
            ['59', 'CRITICAL FAULT: Interlock sense conflict (Voltage OK)'],
            ['60', 'CRITICAL FAULT: Emission interlock is faulted'],
            ['63', 'FAULT', 'FAULT: FAP1 drive is too low'],
            ['64', 'CRITICAL FAULT: FAP1 oven overheating - FAP1T exceeded FAP1MTD setpoint'],
            ['65', 'FAULT', 'FAULT: FAP2 drive is too low'],
            ['66', 'CRITICAL FAULT: FAP2 oven overheating - FAP2T exceeded FAP2MTD setpoint'],
            ['67', 'FAULT', 'Crystal Map Fault', ' boundaries exceeded'],
            ['69', 'FAULT', 'FAULT: OPS drive is too low'],
            ['70', 'CRITICAL FAULT: OPS oven overheating - OPST exceeded OPSMTD setpoint'],
            ['71', 'FAULT', 'FAULT: RES drive is too low'],
            ['72', 'CRITICAL FAULT: RES oven overheating - REST exceeded RESMTD setpoint'],
            ['73', 'FAULT', 'FAULT: Emission lamp voltage is out of range'],
            ['74', 'FAULT', 'FAULT: Crystal motor controller error'],
            ['75', 'FAULT', 'FAULT: ThermaTrack motor controller error'],
            ['76', 'CRITICAL FAULT: Horizontal crystal motor error'],
            ['77', 'CRITICAL FAULT: Vertical crystal motor error'],
            ['78', 'FAULT', 'FAULT: ThermaTrack motor error'],
            ['79', 'FAULT', 'FAULT: Software thread exception occurred'],
            ['82', 'CRITICAL FAULT: The HSN on this head board is not recognized by this laser'],
            ['83', 'FAULT', 'FAULT: Crystal spot hours exceeded limit - Move to next spot'],
            ['84', 'FAULT', "FAULT: Shutter doesn't match the SSI setting"],
            ['85', 'FAULT', "FAULT: Power servo can't maintain the set power"],
            ['86', 'FAULT', 'FAULT: THG drive is too low - Change the spot location'],
            ['87', 'FAULT', 'FAULT: SHG drive is too low'],
            ['88', 'CRITICAL FAULT: THG oven overheating - THGT exceeded THGMTD setpoint'],
            ['89', 'CRITICAL FAULT: SHG oven overheating - SHGT exceeded SHGMTD setpoint'],
            ['90', 'FAULT', 'FAULT: Hardware watchdog reset occurred'],
            ['91', 'CRITICAL FAULT: Possible chiller temperature issue or laser diode failure. Refer to User Manual for resolution.'],
            ['92', 'FAULT', 'FAULT: ETA temperature is out of range'],
            ['93', 'FAULT', 'FAULT: BRF temperature is out of range'],
            ['94', 'FAULT', 'FAULT: ETA drive is too low'],
            ['95', 'FAULT', 'FAULT: BRF drive is too low'],
            ['96', 'CRITICAL FAULT: ETA TEC overheating - ETAT exceeded ETAMTD setpoint'],
            ['97', 'CRITICAL FAULT: BRF TEC overheating - BRFT exceeded BRFMTD setpoint'],
            ['98', 'FAULT', 'FAULT: M1T temperature is out of range'],
            ['99', 'FAULT', "FAULT: M1T servo can't control temperature"],
            ['100', 'FAULT', 'FAULT: M1T drive is too low'],
            ['101', 'CRITICAL FAULT: M1T oven overheating - M1TR exceeded M1TRMTD setpoint'],
            ['102', 'FAULT', "FAULT: Wavelength locking servo can't control output"],
            ['103', 'FAULT', 'FAULT: PZT voltage is out of range'],
            ['104', 'FAULT', "FAULT: PZT servo can't control voltage"],
            ['300', 'FAULT', 'FAULT: Software thread stalled'],
            ['301', 'FAULT', 'FAULT: Communication to control computer is lost'],
            ['302', 'FAULT', 'FAULT: Unable to read from FPGA'],
            ['303', 'FAULT', 'FAULT: Wrong FPGA firmware installed'],
            ['304', 'FAULT', 'FAULT: Power On Self-Test (POST) failed (Dynamic)'],
            ['305', 'FAULT', 'FAULT: Unable to write to FPGA'],
            ['306', 'SERVO', 'FAULT: DC GOOD signal is bad'],
            ['307', 'FAULT', 'FAULT: GUI is out of date'],
            ['308', 'FAULT', 'FAULT: Unapproved chiller'],
            ['309', 'FAULT', 'FAULT: I2C failure'],
            ['310', 'FAULT', 'FAULT: Illegal storage value (Dynamic)'],
            ['500', 'WARNING', 'WARNING: Relative Humidity is too high - Change desiccant'],
            ['501', 'WARNING', 'WARNING: Shutter interlock is open'],
            ['502', 'WARNING', 'WARNING: Crystal spot hours have reached the preset limit'],
            ['503', 'WARNING', 'WARNING: Power lock didnt occur within 60 seconds'],
            ['504', 'WARNING', 'WARNING: ThermaTrack optimizer failure'],
            ['505', 'WARNING', 'WARNING: SHG optimizer failure'],
            ['506', 'WARNING', 'WARNING: THG optimizer failure'],
            ['507', 'WARNING', 'WARNING: Chiller water level is getting low'],
            ['508', 'WARNING', 'WARNING: Chiller flow is out of range'],
            ['509', 'WARNING', 'WARNING: Baseplate-Chiller temperature difference is high - Check the chiller flow'],
            ['510', 'STATUS', 'WARNING: Crystal spot hours approaching the fault limit - Move to new spot to avoid laser shutdown'],
            ['511', 'WARNING', "WARNING: Power servo has rolled over and can't maintain set power"],
            ['512', 'WARNING', 'WARNING: THG drive is getting low - Change spot location'],
            ['513', 'WARNING', 'WARNING: SHG drive is getting low'],
            ['514', 'STATUS', 'WARNING: Pulse energy is below minimum threshold'],
            ['515', 'STATUS', 'WARNING: Pulse energy is above maximum threshold'],
            ['516', 'WARNING', 'WARNING: Crystal motor controller installed in 532nm system'],
            ['517', 'WARNING', 'WARNING: Motor controller firmware needs upgrading'],
            ['518', 'WARNING', 'WARNING: Chiller is not near its set temperature'],
            ['519', 'WARNING', 'WARNING: There are no xtal map protections in this build mode'],
            ['520', 'WARNING', 'WARNING: Diode degradation optimizer failure'],
            ['521', 'WARNING', 'WARNING: Pulsing threshold limit exceeded'],
            ['522', 'WARNING', 'WARNING: ETA drive is getting low'],
            ['523', 'WARNING', 'WARNING: BRF drive is getting low'],
            ['524', 'WARNING', 'WARNING: RES drive is getting low'],
            ['525', 'WARNING', 'WARNING: OPS drive is getting low'],
            ['526', 'WARNING', 'WARNING: FAP1 drive is getting low'],
            ['527', 'WARNING', 'WARNING: FAP2 drive is getting low'],
            ['528', 'WARNING', 'WARNING: M1T optimizer failure'],
            ['529', 'WARNING', 'WARNING: M1T drive is getting low'],
            ['530', 'WARNING', 'WARNING: Shutter is closed', ' laser cannot be enabled'],
            ['531', 'WARNING', 'WARNING: CCMD or PCMD set to 0 since shutter closed'],
            ['801', 'WARNING', 'WARNING: Operating system image is below revision v1.3'],
            ['802', 'WARNING', 'WARNING: Operating system is not 7.0'],
            ['803', 'WARNING', 'WARNING: Software thread is too slow'],
            ['804', 'WARNING', 'WARNING: CPU temperature is too high (Dynamic)'],
            ['805', 'WARNING', 'WARNING: Battery is low'],
            ['806', 'WARNING', 'WARNING: Module temperature is too high (Dynamic)'],
            ['807', 'WARNING', 'WARNING: BasePlate temperature too low. Condensation may form.'],
            ['808', 'WARNING', 'WARNING: (Dynamic)'],
            ['809', 'WARNING', 'WARNING: Spot movement failed. No more good spots'],
            ['810', 'WARNING', 'WARNING: Laser on last good spot'],
            ['811', 'WARNING', 'WARNING: Non standard shutdown'],
            ['812', 'WARNING', 'WARNING: Diode calibration failed'],
            ['813', 'WARNING', 'WARNING: Chiller filter and water service required'],
            ['814', 'WARNING', 'WARNING: Installed module is not a T20 512MB']]

table = [0x00, 0x4D, 0x9A, 0xD7, 0x79, 0x34, 0xE3, 0xAE, 0xF2, 0xBF, 0x68, 0x25, 0x8B, 0xC6, 0x11, 0x5C,
         0xA9, 0xE4, 0x33, 0x7E, 0xD0, 0x9D, 0x4A, 0x07, 0x5B, 0x16, 0xC1, 0x8C, 0x22, 0x6F, 0xB8, 0xF5,
         0x1F, 0x52, 0x85, 0xC8, 0x66, 0x2B, 0xFC, 0xB1, 0xED, 0xA0, 0x77, 0x3A, 0x94, 0xD9, 0x0E, 0x43,
         0xB6, 0xFB, 0x2C, 0x61, 0xCF, 0x82, 0x55, 0x18, 0x44, 0x09, 0xDE, 0x93, 0x3D, 0x70, 0xA7, 0xEA,
         0x3E, 0x73, 0xA4, 0xE9, 0x47, 0x0A, 0xDD, 0x90, 0xCC, 0x81, 0x56, 0x1B, 0xB5, 0xF8, 0x2F, 0x62,
         0x97, 0xDA, 0x0D, 0x40, 0xEE, 0xA3, 0x74, 0x39, 0x65, 0x28, 0xFF, 0xB2, 0x1C, 0x51, 0x86, 0xCB,
         0x21, 0x6C, 0xBB, 0xF6, 0x58, 0x15, 0xC2, 0x8F, 0xD3, 0x9E, 0x49, 0x04, 0xAA, 0xE7, 0x30, 0x7D,
         0x88, 0xC5, 0x12, 0x5F, 0xF1, 0xBC, 0x6B, 0x26, 0x7A, 0x37, 0xE0, 0xAD, 0x03, 0x4E, 0x99, 0xD4,
         0x7C, 0x31, 0xE6, 0xAB, 0x05, 0x48, 0x9F, 0xD2, 0x8E, 0xC3, 0x14, 0x59, 0xF7, 0xBA, 0x6D, 0x20,
         0xD5, 0x98, 0x4F, 0x02, 0xAC, 0xE1, 0x36, 0x7B, 0x27, 0x6A, 0xBD, 0xF0, 0x5E, 0x13, 0xC4, 0x89,
         0x63, 0x2E, 0xF9, 0xB4, 0x1A, 0x57, 0x80, 0xCD, 0x91, 0xDC, 0x0B, 0x46, 0xE8, 0xA5, 0x72, 0x3F,
         0xCA, 0x87, 0x50, 0x1D, 0xB3, 0xFE, 0x29, 0x64, 0x38, 0x75, 0xA2, 0xEF, 0x41, 0x0C, 0xDB, 0x96,
         0x42, 0x0F, 0xD8, 0x95, 0x3B, 0x76, 0xA1, 0xEC, 0xB0, 0xFD, 0x2A, 0x67, 0xC9, 0x84, 0x53, 0x1E,
         0xEB, 0xA6, 0x71, 0x3C, 0x92, 0xDF, 0x08, 0x45, 0x19, 0x54, 0x83, 0xCE, 0x60, 0x2D, 0xFA, 0xB7,
         0x5D, 0x10, 0xC7, 0x8A, 0x24, 0x69, 0xBE, 0xF3, 0xAF, 0xE2, 0x35, 0x78, 0xD6, 0x9B, 0x4C, 0x01,
         0xF4, 0xB9, 0x6E, 0x23, 0x8D, 0xC0, 0x17, 0x5A, 0x06, 0x4B, 0x9C, 0xD1, 0x7F, 0x32, 0xE5, 0xA8]

string_table = ["00", "4D", "9A", "D7", "79", "34", "E3", "AE", "F2", "BF", "68", "25", "8B", "C6", "11", "5C",
                "A9", "E4", "33", "7E", "D0", "9D", "4A", "07", "5B", "16", "C1", "8C", "22", "6F", "B8", "F5",
                "1F", "52", "85", "C8", "66", "2B", "FC", "B1", "ED", "A0", "77", "3A", "94", "D9", "0E", "43",
                "B6", "FB", "2C", "61", "CF", "82", "55", "18", "44", "09", "DE", "93", "3D", "70", "A7", "EA",
                "3E", "73", "A4", "E9", "47", "0A", "DD", "90", "CC", "81", "56", "1B", "B5", "F8", "2F", "62",
                "97", "DA", "0D", "40", "EE", "A3", "74", "39", "65", "28", "FF", "B2", "1C", "51", "86", "CB",
                "21", "6C", "BB", "F6", "58", "15", "C2", "8F", "D3", "9E", "49", "04", "AA", "E7", "30", "7D",
                "88", "C5", "12", "5F", "F1", "BC", "6B", "26", "7A", "37", "E0", "AD", "03", "4E", "99", "D4",
                "7C", "31", "E6", "AB", "05", "48", "9F", "D2", "8E", "C3", "14", "59", "F7", "BA", "6D", "20",
                "D5", "98", "4F", "02", "AC", "E1", "36", "7B", "27", "6A", "BD", "F0", "5E", "13", "C4", "89",
                "63", "2E", "F9", "B4", "1A", "57", "80", "CD", "91", "DC", "0B", "46", "E8", "A5", "72", "3F",
                "CA", "87", "50", "1D", "B3", "FE", "29", "64", "38", "75", "A2", "EF", "41", "0C", "DB", "96",
                "42", "0F", "D8", "95", "3B", "76", "A1", "EC", "B0", "FD", "2A", "67", "C9", "84", "53", "1E",
                "EB", "A6", "71", "3C", "92", "DF", "08", "45", "19", "54", "83", "CE", "60", "2D", "FA", "B7",
                "5D", "10", "C7", "8A", "24", "69", "BE", "F3", "AF", "E2", "35", "78", "D6", "9B", "4C", "01",
                "F4", "B9", "6E", "23", "8D", "C0", "17", "5A", "06", "4B", "9C", "D1", "7F", "32", "E5", "A8"]


def checksum(data):
    """Calculate the serial communication checksum (CRC-8)."""
    init = 0x00
    for element in data:
        position = init ^ element
        init = table[position]
    return position


class C15(Instrument):
    """
    Initialize the transmitter.

    Parameters
    ----------
    COM : int
        The number of the COM port.
    baud_rate
        The communication baud rate, one of 9600, 14400, 19200, 28800,
        38400, 57600, 115200.
    address
        The device address in the range 1-16.
    """

    def __init__(self, adapter, name="C15", baud_rate=115200, address=1):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=False,
            write_termination="\r",
            read_termination="\r\n",
            baud_rate=baud_rate,
            timeout=250,
            parity=Parity.none,
            stop_bits=StopBits.one,
        )
        self.address = address  # 1-16

    def ask(self, command, index):
        try:
            raw = command + ":" + str(index)
            bit = bytes(raw.encode("utf-8"))
            sending = raw + string_table[checksum(bit)]
            self.write(sending)
            answer = self.read().split(":")
            data = answer[0]
            error = answer[1]
            reply_index = int(answer[2][0])
            if error != "!K":
                return f"Serious communication error occurred!\n Error code: {error}"
            else:
                return data, reply_index
        except IndexError:
            print(answer)

    def get_bat(self, index):
        """Returns battery voltage."""
        response = self.ask("?BAT", index)
        return float(response[0]), response[1]

    def get_BRFdrive(self, index):
        """Returns BRF temperature servo drive."""
        response = self.ask("?BRFD", index)
        return int(response[0]), response[1]

    def get_BRFstate(self, index):
        """Returns BRF temperature servo state."""
        data = self.ask("?BRFSS", index)
        if data[0] == "0":
            return "open", data[1]
        elif data[0] == "1":
            return "locked", data[1]
        elif data[0] == "2":
            return "seeking", data[1]
        elif data[0] == "3":
            return "faulted", data[1]
        elif data[0] == "4":
            return "optimizing", data[1]
        elif data[0] == "5":
            return "off", data[1]

    def get_SHGstate(self, index):
        """Returns SHG temperature servo state."""
        data = self.ask("?SHGSS", index)
        if data[0] == "0":
            return "open", data[1]
        elif data[0] == "1":
            return "locked", data[1]
        elif data[0] == "2":
            return "seeking", data[1]
        elif data[0] == "3":
            return "faulted", data[1]
        elif data[0] == "4":
            return "optimizing", data[1]
        elif data[0] == "5":
            return "off", data[1]

    def get_currentavg(self, index):
        """Returns average current value (in Ampere)."""
        response = self.ask("?C", index)
        return float(response[0]), response[1]

    def get_currentD1(self, index):
        """Returns diode1 current value."""
        response = self.ask("?C1", index)
        return float(response[0]), response[1]

    def get_currentD2(self, index):
        """Returns diode2 current value."""
        response = self.ask("?C2", index)
        return float(response[0]), response[1]

    def get_currentset(self, index):
        """Returns current setpoint."""
        response = self.ask("?CCMD", index)
        return float(response[0]), response[1]

    def get_modulTemp(self, index):
        """Returns module temperature."""
        response = self.ask("?CPUMT", index)
        return float(response[0]), response[1]

    def get_CPUtemp(self, index):
        """Returns the CPU temperature."""
        response = self.ask("?CPUT", index)
        return float(response[0]), response[1]

    def get_timeD1(self, index):
        """Returns D1 hours"""
        response = self.ask("?D1H", index)
        return float(response[0]), response[1]

    def get_timeD2(self, index):
        """Returns D2 hours"""
        response = self.ask("?D2H", index)
        return float(response[0]), response[1]

    def get_data(self, index):
        """Return data from the datalogger"""
        response = self.ask("?DATA", index)
        return float(response[0]), response[1]

    def get_diode1Curret(self, index):
        """Returns the diode 1 actual current"""
        response = self.ask("?LDD1ACT", index)
        return float(response[0]), response[1]

    def get_diode2Current(self, index):
        """Returns the diode 2 actual current"""
        response = self.ask("?LDD2ACT", index)
        return float(response[0]), response[1]

    def get_new(self, index):
        """Returns every parameter that has changed"""
        response = self.ask("?NEW", index, -1)
        return response[0], response[1]

    def get_power(self, index):
        """Returns the GRN power output"""
        response = self.ask("?P", index)
        return float(response[0]), response[1]

    def get_SHGdrive(self, index):
        """Returns SHG temperature servo drive"""
        response = self.ask("?SHGD", index)
        return float(response[0]), response[1]

    def get_BRFtemp(self, index):
        """Returns BRF temperature"""
        response = self.ask("?BRFT", index)
        return float(response[0]), response[1]

    def get_BRFset(self, index):
        """Returns BRF set temperature"""
        response = self.ask("?TBRFCMD", index)
        return float(response[0]), response[1]

    def get_FAP1temp(self, index):
        """Returns FAP1 temperature"""
        response = self.ask("?FAP1T", index)
        return float(response[0]), response[1]

    def get_FAP1set(self, index):
        """Returns FAP1 set temperature"""
        response = self.ask("?TFAP1CMD", index)
        return float(response[0]), response[1]

    def get_FAP2temp(self, index):
        """Returns FAP2 temperature"""
        response = self.ask("?FAP2T", index)
        return float(response[0]), response[1]

    def get_FAP2set(self, index):
        """Returns FAP2 set temperature"""
        response = self.ask("?TFAP2CMD", index)
        return float(response[0]), response[1]

    def get_maintemp(self, index):
        """Returns source Main temperature"""
        response = self.ask("?TMAIN", index)
        return float(response[0]), response[1]

    def get_mainset(self, index):
        """Returns source Main set temperature"""
        response = self.ask("?TMAINCMD", index)
        return float(response[0]), response[1]

    def get_OPStemp(self, index):
        """Returns OPS temperature"""
        response = self.ask("?OPST", index)
        return float(response[0]), response[1]

    def get_OPSset(self, index):
        """Returns OPS set temperature"""
        response = self.ask("?TOPSCMD", index)
        return float(response[0]), response[1]

    def get_resonatortemp(self, index):
        """Returns resonator temperature"""
        response = self.ask("?REST", index)
        return float(response[0]), response[1]

    def get_resonatorset(self, index):
        """Returns resonator set temperature"""
        response = self.ask("?TRESCMD", index)
        return float(response[0]), response[1]

    def get_SHGtemp(self, index):
        """Returns SHG temperature in °C"""
        response = self.ask("?SHGT", index)
        return float(response[0]), response[1]

    def getSHGset(self, index):
        """Returns SHG set temperature in °C"""
        response = self.ask("?TSHGCMD", index)
        return float(response[0]), response[1]

    def warnings(self, index):
        """Returns latched/active warnings"""
        response = self.ask("?W", index)
        return response[0], response[1]

    def state(self, index):
        """Returns a number connected to the actual state of the laser."""
        response = self.ask("?STATE", index)
        state = int(response[0])
        if state == 0:
            return "Warmup", response[1]
        elif state == 1:
            return "Fault", response[1]
        elif state == 2:
            return "Standby", response[1]
        elif state == 3:
            return "CDRH Delay", response[1]
        elif state == 4:
            return "On", response[1]
        elif state == 5:
            return "Ramping Up All Diodes", response[1]
        elif state == 6:
            return "Power Lock Mode", response[1]
        else:
            return "Undefined State", response[1]

    def state_name(self, index):
        """Returns the name of the actual laser state."""
        response = self.ask("?ST", index)
        return response[0], response[1]

    def shutter_state(self, index):
        """Returns the shutter state."""
        response = self.ask("?SH", index)
        state = int(response[0])
        if state == 0:
            return "Shutter closed", response[1]
        elif state == 1:
            return "Shutter open", response[1]

    def interlock_state(self, index):
        """Returns the interlock state."""
        response = self.ask("?INT", index)
        return response[0], response[1]

    def key_state(self, index):
        """Returns the physical keyswitch state."""
        response = self.ask("?KSW", index)
        state = int(response[0])
        if state == 0:
            return "Off", response[1]
        elif state == 1:
            return "On", response[1]

    def virtKey_state(self, index):
        """Returns the virtual keyswitch state."""
        response = self.ask("?KSWCMD", index)
        state = int(response[0])
        if state == 0:
            return "Off", response[1]
        elif state == 1:
            return "On", response[1]

    def virtKey_on(self, state, index):
        """Switch the virtual keyswitch on."""
        raw = f"KSWCMD={state}:" + str(index)
        bit = bytes(raw.encode("utf-8"))
        command = raw + string_table[checksum(bit)]
        self.write(command)
        self.read()

    def laser_state(self, index):
        "Returns the laser on state."
        response = self.ask("?L", index)
        state = int(response[0])
        if state == 0:
            return "Off", response[1]
        elif state == 1:
            return "On", response[1]

    def newValues(self, index):
        """Returns every value, that has changed."""
        self.write("?NEW", index)
        data = self.read_bytes(-1).decode("utf-8").split[":"]
        dat = data[0].split("\r\n")[:-3]
        values = {}
        for element in dat:
            values[element.split("=")[0]] = float(element.split("=")[1])
        return values, data[1]

    def setPower(self, power, index):
        raw = f"PCMD={power}:" + str(index)
        bit = bytes(raw.encode("utf-8"))
        command = raw + string_table[checksum(bit)]
        self.write(command)
        self.read()

    def faults(self, index):
        """Return active faults."""
        response = self.ask("?F", index)
        return response[0], response[1]

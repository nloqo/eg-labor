# -*- coding: utf-8 -*-
"""
Created on Wed Aug 23 13:40:47 2023

@author: ernst
"""

from pymeasure.instruments import Instrument

from pyvisa.constants import Parity, StopBits


def faultList():
    return [['1', 'FAULT', 'FAULT: Emission lamp interlock is open'],
            ['2', 'FAULT', 'FAULT: External interlock is open'],
            ['3', 'FAULT', 'FAULT: Cover interlock is open'],
            ['4', 'FAULT', 'FAULT: SHG temperature is out of range'],
            ['7', 'FAULT', 'FAULT: THG temperature is out of range'],
            ['10', 'SERVO', 'FAULT: Baseplate temperature is out of range (Check chiller operation/settings)'],
            ['11', 'FAULT', 'FAULT: Baseplate temperature too low after diodes on (Check chiller operation/settings)'],
            ['14', 'FAULT', 'FAULT: Power supply voltage is out of range'],
            ['16', 'CRITICAL FAULT: Diode 1 over expected current'],
            ['17', 'CRITICAL FAULT: Diode 2 over expected current'],
            ['19', 'CRITICAL FAULT: Diode 1 under expected voltage'],
            ['20', 'CRITICAL FAULT: Diode 2 under expected voltage'],
            ['21', 'CRITICAL FAULT: Diode 1 over expected voltage'],
            ['22', 'CRITICAL FAULT: Diode 2 over expected voltage'],
            ['23', 'CRITICAL FAULT: Diode 1 under expected current'],
            ['24', 'CRITICAL FAULT: Diode 2 under expected current'],
            ['25', 'FAULT', 'FAULT: Diode 1 1-wire error'],
            ['26', 'FAULT', 'FAULT: Diode 2 1-wire error'],
            ['27', 'FAULT', 'FAULT: Laser head 1-wire error'],
            ['28', 'FAULT', 'FAULT: I2C 1-wire Bridge error'],
            ['29', 'FAULT', 'FAULT: Shutter connection invalid'],
            ['30', 'FAULT', 'FAULT: Startup fault cycle keyswitch to enable laser'],
            ['31', 'FAULT', "FAULT: Shutter isn't in the expected state"],
            ['32', 'FAULT', "FAULT: OPS servo can't control temperature"],
            ['33', 'FAULT', "FAULT: RES servo can't control temperature"],
            ['34', 'FAULT', "FAULT: BRF servo can't control temperature"],
            ['35', 'FAULT', "FAULT: ETA servo can't control temperature"],
            ['36', 'FAULT', 'FAULT: Power lock light loop error'],
            ['37', 'FAULT', "FAULT: SHG servo can't control temperature"],
            ['38', 'FAULT', "FAULT: THG servo can't control temperature"],
            ['39', 'FAULT', 'FAULT: Shutter interlock is open'],
            ['40', 'CRITICAL FAULT: Chiller water level is low'],
            ['41', 'CRITICAL FAULT: Chiller temperature is out of range'],
            ['42', 'CRITICAL FAULT: Chiller communication is lost'],
            ['43', 'CRITICAL FAULT: Chiller flow fault detected'],
            ['44', 'CRITICAL FAULT: Diode interlock is open'],
            ['45', 'FAULT', 'FAULT: Chiller temperature is too far from its set temperature'],
            ['46', 'FAULT', "FAULT: FAP1 servo can't control temperature"],
            ['47', 'FAULT', "FAULT: FAP2 servo can't control temperature"],
            ['48', 'FAULT', 'FAULT: FAP1 temperature is out of range'],
            ['49', 'FAULT', 'FAULT: FAP2 temperature is out of range'],
            ['52', 'FAULT', 'FAULT: OPS temperature is out of range'],
            ['53', 'FAULT', 'FAULT: Resonator temperature is out of range'],
            ['57', 'CRITICAL FAULT: An interlock is open'],
            ['58', 'CRITICAL FAULT: Interlock sense conflict (Current OK)'],
            ['59', 'CRITICAL FAULT: Interlock sense conflict (Voltage OK)'],
            ['60', 'CRITICAL FAULT: Emission interlock is faulted'],
            ['63', 'FAULT', 'FAULT: FAP1 drive is too low'],
            ['64', 'CRITICAL FAULT: FAP1 oven overheating - FAP1T exceeded FAP1MTD setpoint'],
            ['65', 'FAULT', 'FAULT: FAP2 drive is too low'],
            ['66', 'CRITICAL FAULT: FAP2 oven overheating - FAP2T exceeded FAP2MTD setpoint'],
            ['67', 'FAULT', 'Crystal Map Fault', ' boundaries exceeded'],
            ['69', 'FAULT', 'FAULT: OPS drive is too low'],
            ['70', 'CRITICAL FAULT: OPS oven overheating - OPST exceeded OPSMTD setpoint'],
            ['71', 'FAULT', 'FAULT: RES drive is too low'],
            ['72', 'CRITICAL FAULT: RES oven overheating - REST exceeded RESMTD setpoint'],
            ['73', 'FAULT', 'FAULT: Emission lamp voltage is out of range'],
            ['74', 'FAULT', 'FAULT: Crystal motor controller error'],
            ['75', 'FAULT', 'FAULT: ThermaTrack motor controller error'],
            ['76', 'CRITICAL FAULT: Horizontal crystal motor error'],
            ['77', 'CRITICAL FAULT: Vertical crystal motor error'],
            ['78', 'FAULT', 'FAULT: ThermaTrack motor error'],
            ['79', 'FAULT', 'FAULT: Software thread exception occurred'],
            ['82', 'CRITICAL FAULT: The HSN on this head board is not recognized by this laser'],
            ['83', 'FAULT', 'FAULT: Crystal spot hours exceeded limit - Move to next spot'],
            ['84', 'FAULT', "FAULT: Shutter doesn't match the SSI setting"],
            ['85', 'FAULT', "FAULT: Power servo can't maintain the set power"],
            ['86', 'FAULT', 'FAULT: THG drive is too low - Change the spot location'],
            ['87', 'FAULT', 'FAULT: SHG drive is too low'],
            ['88', 'CRITICAL FAULT: THG oven overheating - THGT exceeded THGMTD setpoint'],
            ['89', 'CRITICAL FAULT: SHG oven overheating - SHGT exceeded SHGMTD setpoint'],
            ['90', 'FAULT', 'FAULT: Hardware watchdog reset occurred'],
            ['91', 'CRITICAL FAULT: Possible chiller temperature issue or laser diode failure. Refer to User Manual for resolution.'],
            ['92', 'FAULT', 'FAULT: ETA temperature is out of range'],
            ['93', 'FAULT', 'FAULT: BRF temperature is out of range'],
            ['94', 'FAULT', 'FAULT: ETA drive is too low'],
            ['95', 'FAULT', 'FAULT: BRF drive is too low'],
            ['96', 'CRITICAL FAULT: ETA TEC overheating - ETAT exceeded ETAMTD setpoint'],
            ['97', 'CRITICAL FAULT: BRF TEC overheating - BRFT exceeded BRFMTD setpoint'],
            ['98', 'FAULT', 'FAULT: M1T temperature is out of range'],
            ['99', 'FAULT', "FAULT: M1T servo can't control temperature"],
            ['100', 'FAULT', 'FAULT: M1T drive is too low'],
            ['101', 'CRITICAL FAULT: M1T oven overheating - M1TR exceeded M1TRMTD setpoint'],
            ['102', 'FAULT', "FAULT: Wavelength locking servo can't control output"],
            ['103', 'FAULT', 'FAULT: PZT voltage is out of range'],
            ['104', 'FAULT', "FAULT: PZT servo can't control voltage"],
            ['300', 'FAULT', 'FAULT: Software thread stalled'],
            ['301', 'FAULT', 'FAULT: Communication to control computer is lost'],
            ['302', 'FAULT', 'FAULT: Unable to read from FPGA'],
            ['303', 'FAULT', 'FAULT: Wrong FPGA firmware installed'],
            ['304', 'FAULT', 'FAULT: Power On Self-Test (POST) failed (Dynamic)'],
            ['305', 'FAULT', 'FAULT: Unable to write to FPGA'],
            ['306', 'SERVO', 'FAULT: DC GOOD signal is bad'],
            ['307', 'FAULT', 'FAULT: GUI is out of date'],
            ['308', 'FAULT', 'FAULT: Unapproved chiller'],
            ['309', 'FAULT', 'FAULT: I2C failure'],
            ['310', 'FAULT', 'FAULT: Illegal storage value (Dynamic)'],
            ['500', 'WARNING', 'WARNING: Relative Humidity is too high - Change desiccant'],
            ['501', 'WARNING', 'WARNING: Shutter interlock is open'],
            ['502', 'WARNING', 'WARNING: Crystal spot hours have reached the preset limit'],
            ['503', 'WARNING', 'WARNING: Power lock didnt occur within 60 seconds'],
            ['504', 'WARNING', 'WARNING: ThermaTrack optimizer failure'],
            ['505', 'WARNING', 'WARNING: SHG optimizer failure'],
            ['506', 'WARNING', 'WARNING: THG optimizer failure'],
            ['507', 'WARNING', 'WARNING: Chiller water level is getting low'],
            ['508', 'WARNING', 'WARNING: Chiller flow is out of range'],
            ['509', 'WARNING', 'WARNING: Baseplate-Chiller temperature difference is high - Check the chiller flow'],
            ['510', 'STATUS', 'WARNING: Crystal spot hours approaching the fault limit - Move to new spot to avoid laser shutdown'],
            ['511', 'WARNING', "WARNING: Power servo has rolled over and can't maintain set power"],
            ['512', 'WARNING', 'WARNING: THG drive is getting low - Change spot location'],
            ['513', 'WARNING', 'WARNING: SHG drive is getting low'],
            ['514', 'STATUS', 'WARNING: Pulse energy is below minimum threshold'],
            ['515', 'STATUS', 'WARNING: Pulse energy is above maximum threshold'],
            ['516', 'WARNING', 'WARNING: Crystal motor controller installed in 532nm system'],
            ['517', 'WARNING', 'WARNING: Motor controller firmware needs upgrading'],
            ['518', 'WARNING', 'WARNING: Chiller is not near its set temperature'],
            ['519', 'WARNING', 'WARNING: There are no xtal map protections in this build mode'],
            ['520', 'WARNING', 'WARNING: Diode degradation optimizer failure'],
            ['521', 'WARNING', 'WARNING: Pulsing threshold limit exceeded'],
            ['522', 'WARNING', 'WARNING: ETA drive is getting low'],
            ['523', 'WARNING', 'WARNING: BRF drive is getting low'],
            ['524', 'WARNING', 'WARNING: RES drive is getting low'],
            ['525', 'WARNING', 'WARNING: OPS drive is getting low'],
            ['526', 'WARNING', 'WARNING: FAP1 drive is getting low'],
            ['527', 'WARNING', 'WARNING: FAP2 drive is getting low'],
            ['528', 'WARNING', 'WARNING: M1T optimizer failure'],
            ['529', 'WARNING', 'WARNING: M1T drive is getting low'],
            ['530', 'WARNING', 'WARNING: Shutter is closed', ' laser cannot be enabled'],
            ['531', 'WARNING', 'WARNING: CCMD or PCMD set to 0 since shutter closed'],
            ['801', 'WARNING', 'WARNING: Operating system image is below revision v1.3'],
            ['802', 'WARNING', 'WARNING: Operating system is not 7.0'],
            ['803', 'WARNING', 'WARNING: Software thread is too slow'],
            ['804', 'WARNING', 'WARNING: CPU temperature is too high (Dynamic)'],
            ['805', 'WARNING', 'WARNING: Battery is low'],
            ['806', 'WARNING', 'WARNING: Module temperature is too high (Dynamic)'],
            ['807', 'WARNING', 'WARNING: BasePlate temperature too low. Condensation may form.'],
            ['808', 'WARNING', 'WARNING: (Dynamic)'],
            ['809', 'WARNING', 'WARNING: Spot movement failed. No more good spots'],
            ['810', 'WARNING', 'WARNING: Laser on last good spot'],
            ['811', 'WARNING', 'WARNING: Non standard shutdown'],
            ['812', 'WARNING', 'WARNING: Diode calibration failed'],
            ['813', 'WARNING', 'WARNING: Chiller filter and water service required'],
            ['814', 'WARNING', 'WARNING: Installed module is not a T20 512MB']]
    
class C15(Instrument):
    """
    Initialize the transmitter.

    Parameters
    ----------
    COM : int
        The number of the COM port.
    baud_rate
        The communication baud rate, one of 9600, 14400, 19200, 28800,
        38400, 57600, 115200.
    address
        The device address in the range 1-16.
    """

    def __init__(self, adapter, name="C15", baud_rate=115200, address=1):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=False,
            write_termination="\r",
            read_termination="\r\n",
            baud_rate=baud_rate,
            timeout=500,
            parity=Parity.none,
            stop_bits=StopBits.one,
        )
        self.address = address  # 1-16
    '''
    def read(self):
        response = super().read()
        ok = super().read()
        if ok == "OK":
            return response
        else:
            raise ConnectionError(f"Received stuff: {response} {ok}")
    '''
    battery_voltage = Instrument.measurement("?BAT", "Get battery voltage.")

    def get_bat(self):
        '''Returns battery voltage.'''
        return float(self.ask("?BAT"))

    def get_BRFdrive(self):
        '''Returns BRF temperature servo drive.'''
        return int(self.ask("?BRFD"))

    def get_BRFstate(self):
        '''Returns BRF temperature servo state.'''
        data = self.ask("?BRFSS")
        if data == "0":
            return "open"
        elif data == "1":
            return "locked"
        elif data == "2":
            return "seeking"
        elif data == "3":
            return "faulted"
        elif data == "4":
            return "optimizing"
        elif data == "5":
            return "off"

    def get_SHGstate(self):
        '''Returns SHG temperature servo state.'''
        data = self.ask("?SHGSS")
        if data == "0":
            return "open"
        elif data == "1":
            return "locked"
        elif data == "2":
            return "seeking"
        elif data == "3":
            return "faulted"
        elif data == "4":
            return "optimizing"
        elif data == "5":
            return "off"

    def get_currentavg(self):
        '''Returns average current value (in Ampere).'''
        return float(self.ask("?C"))

    def get_currentD1(self):
        '''Returns diode1 current value.'''
        return float(self.ask("?C1"))

    def get_currentD2(self):
        '''Returns diode2 current value.'''
        return float(self.ask("?C2"))

    def get_currentset(self):
        '''Returns current setpoint.'''
        return float(self.ask("?CCMD"))

    def get_modulTemp(self):
        '''Returns module temperature.'''
        return float(self.ask("?CPUMT"))

    def get_CPUtemp(self):
        '''Returns the CPU temperature.'''
        return float(self.ask("?CPUT"))

    def get_timeD1(self):
        '''Returns D1 hours'''
        return float(self.ask("?D1H"))

    def get_timeD2(self):
        '''Returns D2 hours'''
        return float(self.ask("?D2H"))

    def get_data(self):
        '''Return data from the datalogger'''
        return float(self.ask("?DATA"))

    def get_diode1Curret(self):
        '''Returns the diode 1 actual current'''
        return float(self.ask("?LDD1ACT"))

    def get_diode2Current(self):
        '''Returns the diode 2 actual current'''
        return float(self.ask("?LDD2ACT"))

    def get_new(self):
        '''Returns every parameter that has changed'''
        return self.ask("?NEW")

    def get_power(self):
        '''Returns the GRN power output'''
        return float(self.ask("?P"))

    def get_SHGdrive(self):
        '''Returns SHG temperature servo drive'''
        return float(self.ask("?SHGD"))

    def get_BRFtemp(self):
        '''Returns BRF temperature'''
        return float(self.ask("?BRFT"))

    def get_BRFset(self):
        '''Returns BRF set temperature'''
        return float(self.ask("?TBRFCMD"))

    def get_FAP1temp(self):
        '''Returns FAP1 temperature'''
        return float(self.ask("?FAP1T"))

    def get_FAP1set(self):
        '''Returns FAP1 set temperature'''
        return float(self.ask("?TFAP1CMD"))

    def get_FAP2temp(self):
        '''Returns FAP2 temperature'''
        return float(self.ask("?FAP2T"))

    def get_FAP2set(self):
        '''Returns FAP2 set temperature'''
        return float(self.ask("?TFAP2CMD"))

    def get_maintemp(self):
        '''Returns source Main temperature'''
        return float(self.ask("?TMAIN"))

    def get_mainset(self):
        '''Returns source Main set temperature'''
        return float(self.ask("?TMAINCMD"))

    def get_OPStemp(self):
        '''Returns OPS temperature'''
        return float(self.ask("?OPST"))

    def get_OPSset(self):
        '''Returns OPS set temperature'''
        return float(self.ask("?TOPSCMD"))

    def get_resonatortemp(self):
        '''Returns resonator temperature'''
        return float(self.ask("?REST"))

    def get_resonatorset(self):
        '''Returns resonator set temperature'''
        return float(self.ask("?TRESCMD"))

    def get_SHGtemp(self):
        '''Returns SHG temperature in °C'''
        return float(self.ask("?SHGT"))

    def getSHGset(self):
        '''Returns SHG set temperature in °C'''
        return float(self.ask("?TSHGCMD"))

    def warnings(self):
        '''Returns latched/active warnings'''
        return self.ask("?W")

    def state(self):
        '''Returns a number connected to the actual state of the laser.'''
        state = int(self.ask("?STATE"))
        if state == 0:
            return "Warmup"
        elif state == 1:
            return "Fault"
        elif state == 2:
            return "Standby"
        elif state == 3:
            return "CDRH Delay"
        elif state == 4:
            return "On"
        elif state == 5:
            return "Ramping Up All Diodes"
        elif state == 6:
            return "Power Lock Mode"
        else:
            return "Undefined State"

    def state_name(self):
        '''Returns the name of the actual laser state.'''
        return self.ask("?ST")

    def shutter_state(self):
        '''Returns the shutter state.'''
        state = int(self.ask("?SH"))
        if state == 0:
            return "Shutter closed"
        elif state == 1:
            return "Shutter open"

    def interlock_state(self):
        '''Returns the interlock state.'''
        return self.ask("?INT")

    def key_state(self):
        '''Returns the physical keyswitch state.'''
        state = int(self.ask("?KSW"))
        if state == 0:
            return "Off"
        elif state == 1:
            return "On"

    def virtKey_state(self):
        '''Returns the virtual keyswitch state.'''
        state = int(self.ask("?KSWCMD"))
        if state == 0:
            return "Off"
        elif state == 1:
            return "On"

    def virtKey_on(self, state):
        '''Switch the virtual keyswitch on.'''
        self.ask(f"KSWCMD={state}")

    def laser_state(self):
        "Returns the laser on state."
        state = int(self.ask("?L"))
        if state == 0:
            return "Off"
        elif state == 1:
            return "On"

    def newValues(self):
        '''Returns every value, that has changed.'''
        self.write("?NEW")
        data = self.read_bytes(-1).decode("utf-8")
        dat = data.split("\r\n")[:-3]
        values = {}
        for element in dat:
            values[element.split("=")[0]] = float(element.split("=")[1])
        return values

    def GRNpower(self):
        return float(self.ask("?P"))

    def setPower(self, power):
        self.write(f"PCMD={power}")
        self.read_bytes(-1)
        

    def faults(self):
        '''Return active faults.'''
        return self.ask("?F")

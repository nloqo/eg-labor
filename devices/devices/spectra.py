from enum import IntFlag

from pymeasure.instruments import Instrument
from pymeasure.instruments.validators import strict_range


BOOL = {True: "ON", False: "OFF"}


class QuantaRay(Instrument):
    """Control a Spectra Physics Quanta-Ray Nd:YAG laser."""

    def __init__(self, adapter, name="QuantaRay", **kwargs):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=True,
            asrl={
                "write_termination": "\n",
                "read_termination": "\n",
                "baud_rate": 9600,
            },
            **kwargs,
        )

    class Status(IntFlag):
        """Status bits."""

        EMISSION = 1 << 0
        # 1 Reserved
        ERROR_LOG = 1 << 2
        QUESTIONABLE = 1 << 3  # check questionable bits
        # 4 Reserved
        ESR = 1 << 5  # check ESR bits
        # 6 Reserved
        OPERATION = 1 << 7  # check operation bits
        CONTACTOR_ENERGIZED = 1 << 8
        OSCILLATOR_SIMMER = 1 << 9
        AMPLIFIER_SIMMER = 1 << 10
        OSCILLATOR_POWER_REACHED = 1 << 11
        RECENTLY_FIRED = 1 << 12
        POWER_15Vdc_FAILURE = 1 << 13
        LASER_COVER_INTERLOCK_OPEN = 1 << 14
        INTERLOCK_OPEN = 1 << 15  # CDRH plug, power supply cover, laser head cover,
        # laser head temperature, water pressure, water flow
        ANALOG_CONTROLLER_INTERLOCK_OPEN = 1 << 16
        POWER_208Vac_FAILURE = 1 << 17
        CDRH_FAILURE = 1 << 18
        LASER_ID_FAULT = 1 << 19
        LOW_WATER_FAULT = 1 << 20
        INTERLOCK_FAULT = 1 << 21
        ANALOG_CONTROLLER_CONNECTED = 1 << 22
        COMPUTER_CONTROL = 1 << 23  # The remote panel indicates that the computer is in control.
        MAIN_CONTACTOR_ON = 1 << 24
        LAMPS_INHIBITED = 1 << 25
        AC_POWER_ERROR = 1 << 26
        BEAMLOK_ERROR = 1 << 27  # signal too low
        BEAMLOK_LIMIT_REACHED = 1 << 28  # <10% or >90%
        DLOG_EXCESSIVE_OSCILLATOR_POWER = 1 << 29  # lamp changed without reset counter?
        DLOK_DETECTED_LOW_OSCILLATOR = 1 << 30
        # 31 Reserved

    class Questionable(IntFlag):
        """Questionable bits."""

        SHOTS_OVER_40_M = 4
        OSCILLATOR_HV_FAILURE = 1 << 9
        AMPLIFIER_HV_FAILURE = 1 << 10
        EXTERNAL_TRIGGER_RATE_OUT_OF_RANGE = 1 << 11
        DE_IONIZED_WATER_LOW = 1 << 12

    status = Instrument.measurement(
        "*STB?",
        "Get the status.",
        cast=int,
        get_process=Status,
    )

    status_question = Instrument.measurement(
        "STAT:QUES?",
        "Get the questionable condition register.",
        cast=int,
        get_process=Questionable,
    )

    emission_enabled = Instrument.control(
        "ON?",
        "%s",
        """Control the laser emission. If set to True, start the laser and set
        it to the last commanded power settings. (bool)""",
        values=BOOL,
        map_values=True,
        # TODO funktioniert irgendwie nicht (im getter modus), aber über Status geht
    )

    amplifier_power_setpoint = Instrument.control(
        "APFN?",
        "APFN %f",
        """Control the relative amplifier power setpoint in percent. (float)""",
        # TODO setter also float?
        preprocess_reply=lambda v: v.replace("%", ""),
    )

    oscillator_power_setpoint = Instrument.control(
        "OPFN?",
        "OPFN %f",
        """Control the relative oscillator power setpoint in percent. (float)""",
        # TODO setter also float?
        preprocess_reply=lambda v: v.replace("%", ""),
    )

    amplifier_power_internal_setpoint = Instrument.measurement(
        "READ:APFN?",
        """Measure the relative amplifier power setpoint in percent, as it is currently used by the
        driver. (float)""",
        preprocess_reply=lambda v: v.replace("%", ""),
    )

    oscillator_power_internal_setpoint = Instrument.measurement(
        "READ:OPFN?",
        """Measure the relative oscillator power setpoint in percent, as it is currently used by the
        driver. (float)""",
        preprocess_reply=lambda v: v.replace("%", ""),
    )

    amplifier_power = Instrument.measurement(
        "READ:AMON?",
        """Measure the current relative amplifier power in percent. (float)""",
        preprocess_reply=lambda v: v.replace("%", ""),
    )

    oscillator_power = Instrument.measurement(
        "READ:OMON?",
        """Measure the current relative oscillator power in percent. (float)""",
        preprocess_reply=lambda v: v.replace("%", ""),
    )

    shot_count = Instrument.measurement(
        "SHOTs?",
        "Measure the number of shots on the lamps. (int)",
        cast=int,
    )

    def reset_shots(self):
        """Reset the number of shots on the lamps."""
        self.write("SHOT 0")

    lamp_trigger = Instrument.control(
        "LAMP?",
        "LAMP %s",
        """Control the lamp trigger: 'FIX', 'EXT' 'VAR %f'. (str)""",
    )

    qswitch_settings = Instrument.control(
        "QSW?",
        "QSW %s",
        """Control the Q-switch settings: 'EXT', 'LONG', 'NORM'; 'FIRe', 'REPetitive', 'SINGleshot',
        'DIVisor'...""",
    )

    temperature = Instrument.measurement(
        "READ:TEMP?",
        "Measure the temperature of the control board in °C. (float)",
        preprocess_reply=lambda v: v.replace("C", ""),
    )

    dlok_enabled = Instrument.control(
        "DLOK?",
        "DLOK %s",
        """Control whether DLOK is enabled. DLOK ensures constant output power over
        the lamp live time. (bool)""",
        separator=" ",
        get_process=lambda values: values[0] == 2,
        set_process=lambda value: BOOL.get(value),
    )

    dlok_power = Instrument.measurement(
        "DLOK?",
        "Measure the current relative output power according to DLOK.",
        separator=" ",
        get_process=lambda values: values[2],
    )

    watchdog_timeout = Instrument.setting(
        "WATChdog %f",
        """Set the watchdog timeout in seconds, that is the time without communication
        after which the laser turns off itself. 0 means unlimited. (float)""",
        values=[0, 110],
        validator=strict_range,
    )

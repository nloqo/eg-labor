# -*- coding: utf-8 -*-
"""
Auxiliary classes useful for testing.

standard classes
-------
Empty
    Just an empty class.
function
    A function adding the arguments to its parent class.

Qt classes
----------
pyqtSignal
    A signal.
QSettings
    From QtCore.
QWidget
    A mock Qt-Widget with values.


Other classes
-------------
TMC
    A mock TMC motor card.



Created on Thu Feb  3 11:44:32 2022 by Benedikt Moneke
"""

"# Standard classes"


class Empty:
    pass


def empty(*args, **kwargs):
    pass


"# Qt"


class pyqtSignal:
    def emit(self, *args):
        self.emitted = args


class QButton:
    def __init__(self, checked=False):
        self.checked = checked

    def isChecked(self):
        return self.checked

    def setChecked(self, checked):
        self.checked = checked


class QSettings:
    """
    You have to define QSettings in your own module copying the init in order
    to access the global variables.
    For using have in your fixture:
    global settings
    settings = {your funny dictionary}
    monkeypatch.setattr("PyQt6.QtCore.QSettings", QSettings)
    """

    def __init__(self):
        global settings
        self.settings = settings

    def value(self, key, defaultValue=None, type=None):
        value = self.settings.get(key, defaultValue)
        if type is not None:
            value = type(value)
        return value

    def setValue(self, key, value):
        self.settings[key] = value


class QWidget:
    def __init__(self, value=None):
        self.content = value

    def value(self):
        return self.content

    def setValue(self, value):
        self.content = value


"# TMC Motors"


class TMC:
    def __init__(self, positions=[0] * 6):
        self.positions = positions

    def getActualPosition(self, motor):
        return self.positions[motor]

    def moveTo(self, motor, steps):
        self.positions[motor] = steps

    def moveBy(self, motor, steps):
        self.positions[motor] += steps

from warnings import warn

from pyleco.utils.parser import parser, parse_command_line_parameters  # noqa

from pyleco_extras.gui_utils.base_main_window import start_app

warn("Deprecated to use the `devices.gui_utils`, use it from pyleco-extras instead.")

# -*- coding: utf-8 -*-
"""
Model of the TMC in order to test correct workings.

classes
-------
TMCM_3110
    A model of the real class, but with simulated motors.

Created on Thu Feb 17 17:37:02 2022 by Benedikt Moneke
"""

import logging
import threading
import time

from pytrinamic.modules import TMCM6110 as _TMCM6110

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class ConnectionManager:
    """A fake connection manager simulating a TMC motor card."""

    def __init__(self, *args):
        # Axis parameters
        self._steps: list[int] = [0] * 6
        self._targets: list[int] = [0] * 6
        self._velocities: list[int] = [0] * 6
        self._max_velocity: list[int] = [1064] * 6
        self._position_reached_flag: list[bool] = [False] * 6
        # Variables
        self._aps = _TMCM6110._MotorTypeA.AP

    def simulator(self):
        """Simulate a motor card."""
        while self.connected:
            for i in range(6):
                if abs(self._steps[i] - self._targets[i]) < abs(self._velocities[i]):
                    # Close to position -> position reached.
                    self._steps[i] = self._targets[i]
                    self._velocities[i] = 0
                    self._position_reached_flag[i] = True
                    continue
                self._steps[i] += self._velocities[i]
            time.sleep(0.2)

    def connect(self):
        """Start a thread calculating steps."""
        self.connected = True
        self.thread = threading.Thread(target=self.simulator)
        self.thread.daemon = True
        self.thread.start()
        return self

    def disconnect(self):
        self.connected = False

    # Axis parameter access functions
    def get_axis_parameter(self, command_type, axis, module_id=None, signed=False):
        match command_type:
            case self._aps.ActualPosition:
                return self._steps[axis]
            case self._aps.ActualVelocity:
                return self._velocities[axis]
            case self._aps.ActualAcceleration:
                return 5
            case self._aps.TargetPosition:
                return self._targets[axis]
            case self._aps.MaxVelocity:
                return self._max_velocity[axis]
            case self._aps.PositionReachedFlag:
                return self._position_reached_flag[axis]
            case _:
                log.debug(("get not specified axis parameter", command_type))
                return 5

    def set_axis_parameter(self, command_type, axis, value, module_id=None):
        log.debug(("setAxisParameter", command_type, axis, value))
        match command_type:
            case self._aps.MaxVelocity:
                self._max_velocity[axis] = value

    # Global parameter access functions
    def get_global_parameter(self, command_type, bank, module_id=None, signed=False):
        log.debug(("get_global_parameter", command_type, bank))
        return 5

    def set_global_parameter(self, command_type, bank, value, module_id=None):
        log.debug(("set_global_parameter", command_type, bank, value))

    # Motion control functions
    def rotate(self, motor, velocity, module_id=None):
        self._targets[motor] = float("inf")  # type: ignore
        self._velocities[motor] = velocity

    def stop(self, motor, module_id=None):
        self._velocities[motor] = 0

    def move(self, move_type, motor, position, module_id=None):
        log.debug(("move", move_type, motor, position))
        self._position_reached_flag[motor] = False
        self._targets[motor] = position + move_type * self._steps[motor]
        sign = -1 if self._targets[motor] < self._steps[motor] else 1
        self._velocities[motor] = sign * self._max_velocity[motor]

    def move_to(self, motor, position, module_id=None):
        return self.move(0, motor, position, module_id)

    def move_by(self, motor, distance, module_id=None):
        return self.move(1, motor, distance, module_id)

    def reference_search(self, command_type, motor, module_id=None):
        log.debug(("reference search", command_type, motor, 0, module_id))

"""
Communication protocol for Julabo chillers MV-4/MV-6/MV-12/MV-26 & MW-4/MW-6/MW-12/MW-26/MW-Z.
"""

from pymeasure.instruments import Instrument
from pyvisa.constants import Parity, StopBits

def errorEncoder(code: str) -> tuple[str, int]:
    """Return an error message for a given error code."""
    if code == '':
        return ('No error present', 0)
    elif code == '-01 TEMP / LEVEL ALARM':
        return ('Error 01: Security-temperature alarm or under-threshold alarm', 1)
    elif code == '-03 EXCESS TEMPERATURE WARNING':
        return ('Error 03: Over-temperature warning', 3)
    elif code == '-04 LOW TEMPERATURE WARNING':
        return ('Error 04: Low-temperature warning', 4)
    elif code == '-05 TEMPERATURE MEASUREMENT ALARM':
        return ('Error 05: Error in measurement system', 5)
    elif code == '-06 SENSOR DIFFERENCE ALARM':
        return ('Error 06: Sensor difference alarm: Control sensor and security sensor have a difference of more than 25 K', 6)
    elif (code == '07 I2C-BUS WRITE ERROR' or code == '07 I2C-BUS READ ERROR' or code == '07 I2C-BUS READ/WRITE ERROR'):
        return ('Error 07: Internal error', 7)
    elif code == '-08 INVALID COMMAND':
        return('Error 08: Command not recognized', 8)
    elif code == '-10 VALUE TOO SMALL':
        return ('Error 10: Value too small', 10)
    elif code == '-11 VALUE TOO LARGE':
        return ('Error 11: Value too large', 11)
    elif code == '-12 WARNING : VALUE EXCEEDS TEMPERATURE LIMITS':
        return ('Error 12: Value is not between set values for over-temperature and under-temperature, but will be stored', 12)
    elif code == '-13 COMMAND NOT ALLOWED IN CURRENT OPERATING MODE':
        return ('Error 13: Command not valid in the current device mode', 13)
    else:
        return ('Unknown error return', -1)

def statusEncoder(status) -> tuple[str, int]:
    """Return an status message for a given status code."""
    if status == '00 MANUAL STOP':
        return ('Status 00: Thermostate in off mode', 0)
    elif status == '01 MANUAL START':
        return ('Status 01: Thermostate in manual mode', 1)
    elif status == '02 REMOTE STOP':
        return('Status 02: Thermostate in remote-off mode', 2)
    elif status == '03 REMOTE START':
        return ('Status 03: Thermostate in remote mode', 3)

class Julabu(Instrument):

    def __init__(self, adapter, name="Julabu", baud_rate=4800, address=1):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=False,
            write_termination="\r",
            read_termination="\r",
            baud_rate=baud_rate,
            timeout=500,
            parity=Parity.none,
            stop_bits=StopBits.one,
        )
        self.address = address  # 1-16

    def message(self, command: str, parameter: str | int | float | None = None, answer: bool = False):
        if not parameter == None:
            self.write(command + hex(0x0d))
        else:
            if answer:
                return self.ask(command + hex(20) + str(parameter) + hex(13))
            else:
                return self.write(command + hex(20) + str(parameter) + hex(13))

    def version(self) -> str:
        """Version number of the software. Returns it in the from 'V X.xx'."""
        return self.message("version", answer=True)

    def status(self) -> dict:
        """Return the status and error code."""
        # TODO: Find out the what the overall message looks like to split status and error.
        pass

    def useT1(self) -> None:
        """Choose T1 as the chiller temperature."""
        self.message("out_mode_01", 0)

    def useT2(self) -> None:
        """Choose T2 as the chiller temperature."""
        self.message("out_mode_01", 1)

    def stop(self) -> None:
        """Set the chiller to 'r OFF' mode."""
        self.message("out_mode_05", 0)

    def start(self) -> None:
        """Start the chiller."""
        self.message("out_mode_05", 1)

    def setT1(self, temperature: float) -> None:
        """Set the T1 temperature."""
        if temperature < 10:
            T1 = "00" + str(round(temperature, 1))
        elif temperature >= 10 and temperature < 100:
            T1 = "0" + str(round(temperature, 1))
        elif temperature >= 100:
            T1 = str(round(temperature, 1))
        self.message("out_sp_00", T1)

    def setT2(self, temperature: float) -> None:
        """Set the T2 temperature."""
        if temperature < 10:
            T1 = "00" + str(round(temperature, 1))
        elif temperature >= 10 and temperature < 100:
            T1 = "0" + str(round(temperature, 1))
        elif temperature >= 100:
            T1 = str(round(temperature, 1))
        self.message("out_sp_01", T1)


# -*- coding: utf-8 -*-
"""
Hints for usage of a national instruments (NI) data acquisition (DAQ) card.

Official documentation:

* https://github.com/ni/nidaqmx-python
* https://nidaqmx-python.readthedocs.io/en/latest/
* https://zone.ni.com/reference/en-XX/help/370471AA-01/

Created on Wed Jul 28 16:13:59 2021 by Benedikt Burger
"""

from typing import Optional

import nidaqmx
import nidaqmx.stream_readers


# Examples
def readAnalogChannel(
    channels: int | list[int], numberOfSamples: Optional[int] = 0
) -> float | list[float] | list[list[float]]:
    """Read an analog channel"""
    with nidaqmx.Task() as task:
        if hasattr(channels, "__iter__"):
            for channel in channels:
                task.ai_channels.add_ai_voltage_chan(f"Dev1/ai{channel}")
        else:
            task.ai_channels.add_ai_voltage_chan(f"Dev1/ai{channels}")
        if numberOfSamples:
            return task.read(number_of_samples_per_channel=numberOfSamples)
        else:
            return task.read()


def readAnalogStream(channel: int = 1) -> "np.ndarray":
    """Read an analog stream to a prepared numpy array."""
    import numpy as np

    array = np.zeros(50)
    task = nidaqmx.Task()
    task.ai_channels.add_ai_voltage_chan(f"Dev1/ai{channel}")
    stream = nidaqmx.stream_readers.AnalogSingleChannelReader(task.in_stream)
    for i in range(5):
        stream.read_many_sample(array, number_of_samples_per_channel=50)
        print(np.mean(array))
    task.close()
    return array


def writeChannel(
    voltage: float | list[float] | list[list[float]],
    channel: str | list[str] = "Dev1/ao0",
) -> None:
    """Set `channel` to a `voltage` in V."""
    with nidaqmx.Task() as niTask_w:
        niTask_w.ao_channels.add_ao_voltage_chan(channel)
        niTask_w.write(voltage)

# -*- coding: utf-8 -*-
"""Control motors


Created on Tue Nov 29 17:34:36 2022

@author: moneke
"""

from warnings import warn

from pyleco_extras.actors.tmc_motor_actor import TMCMotorActor as MotorController  # noqa

warn("Deprecated to use the MotorController, use pyleco_extras.actors.tmc_motor_actor.TMCMotorActor instead.",
     FutureWarning)

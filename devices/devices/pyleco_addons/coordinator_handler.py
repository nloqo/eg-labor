"""
Idea: A message handler which seems to be a Coordinator, such that you can create a standalone
application (without an external Coordinator)
"""

import logging
from typing import Optional

import zmq
from pyleco.core.message import Message, MessageTypes
from pyleco.utils.message_handler import MessageHandler


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


class CoordinatorHandler(MessageHandler):

    def __init__(self, name: str, namespace: Optional[str] = None, **kwargs):
        if namespace is None:
            namespace = name
        self.namespace = namespace
        self.coordinator_name = ".".join((namespace, name)).encode()
        super().__init__(name=name, **kwargs)

        self.components: dict[bytes, bytes] = {}  # name, identity

    def setup_socket(self, host: str, port: int, protocol: str, context) -> None:
        self.socket: zmq.Socket = context.socket(zmq.ROUTER)
        self.log.info(f"MessageHandler binding to {host}:{port}")
        self.socket.bind(f"{protocol}://{host}:{port}")

    def _send_message(self, message: Message) -> None:
        if not message.sender:
            message.sender = self.full_name.encode()
        try:
            identity = self.components[message.receiver_elements.name]
        except KeyError:
            pass  # TODO error logging and handling
        else:
            self.socket.send_multipart([identity] + message.to_frames())
        return super()._send_message(message)

    def read_message(self) -> Message:
        identity, *frames = self.socket.recv_multipart()
        msg = Message.from_frames(*frames)
        self.components[msg.sender_elements.name] = identity
        return msg

    def handle_commands(self, msg: Message) -> None:
        if msg.receiver_elements.name == b"COORDINATOR":
            self.handle_coordinator_commands(msg=msg)
        else:
            return super().handle_commands(msg)

    def handle_coordinator_commands(self, msg: Message) -> None:
        if msg.header_elements.message_type == MessageTypes.JSON:
            if b'"method":' in msg.payload[0]:
                self.log.info(f"Handling COORDINATOR commands of {msg}.")
                reply = self.rpc.process_request(msg.payload[0])
                response = Message(msg.sender, sender=self.coordinator_name,
                                   conversation_id=msg.conversation_id,
                                   message_type=MessageTypes.JSON, data=reply)
                self.send_message(response)
            else:
                self.log.error(f"Unknown message from {msg.sender!r} received: {msg.payload[0]!r}")
        else:
            self.log.warning(f"Unknown message from {msg.sender!r} received: '{msg.data}', {msg.payload!r}.")  # noqa: E501

    # Coordinator methods
    def register_rpc_methods(self) -> None:
        super().register_rpc_methods()
        for method in (self.sign_in, self.sign_out,
                       self.coordinator_sign_in,
                       self.coordinator_sign_out,
                       self.add_nodes,
                       self.send_nodes,
                       self.record_components,
                       self.send_local_components,
                       self.send_global_components,
                       self.remove_expired_addresses,
                       ):
            self.register_rpc_method(method=method)

    def sign_in(self) -> None:
        return  # done in message receiving, no checks done

    def sign_out(self) -> None:
        return  # no checks done, stay in list until end of life.

    def coordinator_sign_in(self) -> None:
        raise NotImplementedError

    def coordinator_sign_out(self) -> None:
        raise NotImplementedError

    def add_nodes(self, nodes: dict[str, str]) -> None:
        raise NotImplementedError

    def send_nodes(self) -> dict[str, str]:
        raise NotImplementedError

    def record_components(self, components: list[str]) -> None:
        raise NotImplementedError

    def send_local_components(self) -> list[str]:
        raise NotImplementedError

    def send_global_components(self) -> dict[str, list[str]]:
        raise NotImplementedError

    def remove_expired_addresses(self, expiration_time: float) -> None:
        self.components.clear()

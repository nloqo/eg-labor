# -*- coding: utf-8 -*-
"""
Adaptation of the DataLogger to spectroscopy.

Created on Mon Aug 16 11:44:44 2021 by Benedikt Burger
"""

import logging
import pickle
from typing import Any, Iterable, Optional

from PyQt6 import QtCore, QtWidgets

from pyleco_extras.gui.data_logger.data.settings import Settings


log = logging.Logger(__name__)
log.addHandler(logging.StreamHandler())


# Parameters
wl_offset = 0.0295  # nm, WS6-600 measured wavelength is that much larger than the calculated one.
wlc_offset = 0.0285  # nm empirically 0.0285 nm in order to be on resonance


class ModSettings(Settings):

    def setup_form(self, layout: QtWidgets.QFormLayout) -> None:
        super().setup_form(layout)
        self.create_any_file_dialog(
            key="simulation_file",
            tooltip="Path to the simulation data file.",
            caption="Open file",
            dialog=QtWidgets.QFileDialog.getOpenFileName,
        )


class SpectroscopyMixin():
    """Adaptation of the DataLogger to spectroscopy."""

    _additional_lists: dict[str, list[float]]

    def __init__(self, **kwargs) -> None:
        self.setup_additional_lists()
        self._simulation = {}
        super().__init__(**kwargs)
        self.settings_dialog_class = ModSettings

    def setup_additional_lists(self) -> None:
        self._additional_lists = {
            "idler_wl": [],
            "idler_wlc": [],
        }

    def setSettings(self):
        super().setSettings()  # type: ignore
        settings = QtCore.QSettings()
        simulation_file_name = settings.value("simulation_file", type=str)
        self.load_simulation_data(simulation_file_name)

    def show_data_point(self, datapoint: dict[str, Any]) -> None:
        datapoint = self.calculate_data(datapoint)
        super().show_data_point(datapoint)  # type: ignore

    def calculate_data(self, datapoint: dict[str, Any]) -> dict[str, Any]:
        """Calculate the data for a data point including calibration.

        It replaces the last value (nan) with the calibration.
        """
        try:
            value = (1 / (1 / datapoint['pump_wl'] - 1 / datapoint['signal_wl']))
        except Exception:
            pass
        else:
            self._additional_lists['idler_wl'].append(value)
            datapoint['idler_wl'] = value

        try:
            value = 1 / (
                1 / (datapoint["seedWavelengthCalculated"] + wlc_offset)
                - 1 / datapoint["signal_wl"]
            )
        except Exception:
            pass
        else:
            self._additional_lists['idler_wlc'].append(value)
            datapoint['idler_wlc'] = value
        return datapoint

    def get_data(self, key: str, start: int | None = None, stop: int | None = None) -> list[float]:
        if key in self._additional_lists:
            return self._additional_lists[key][start:stop]
        else:
            return super().get_data(key, start, stop)  # type: ignore

    def get_xy_data(
        self,
        y_key: str,
        x_key: str | None = None,
        start: int | None = None,
        stop: int | None = None,
    ) -> tuple[list[float]] | tuple[list[float], list[float]]:
        if y_key == "simulation":
            return self.get_simulation_data(x_key=x_key)
        else:
            return super().get_xy_data(y_key, x_key, start, stop)  # type: ignore

    def get_data_keys(self) -> Iterable[str]:
        keys = list(super().get_data_keys())  # type: ignore
        keys.extend(self._additional_lists.keys())
        if self._simulation:
            keys.append("simulation")
        return keys

    def get_simulation_data(
        self, x_key: Optional[str]
    ) -> tuple[list[float]] | tuple[list[float], list[float]]:
        if x_key is None:
            return ([],)
        elif x_key == "idler_wl" or x_key == "idler_wlc":
            return self._simulation.get("wl", []), self._simulation.get("P", [])
        else:
            return [], []

    def load_simulation_data(self, simulation_file_name: str) -> None:
        try:
            with open(simulation_file_name, "rb") as file:
                self._simulation: dict[str, list[float]] = pickle.load(file)
        except FileNotFoundError:
            self.show_status_bar_message("Simulation file not found!", 3000)  # type: ignore
            self._simulation = {}
        else:
            self._simulation["wl"] = 1e7 / self._simulation["nu"]  # type: ignore

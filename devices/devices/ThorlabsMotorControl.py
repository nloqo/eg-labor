# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 13:28:11 2023

@author: ernst
"""

from pymeasure.instruments import Instrument

from pyvisa.constants import Parity, StopBits

device_code: dict[str, int] = {
    "HOST": 0x01,
    "RACK": 0x11,
    "BAY0": 0x21,
    "BAY1": 0x22,
    "BAY2": 0x23,
    "BAY3": 0x24,
    "BAY4": 0x25,
    "BAY5": 0x26,
    "BAY6": 0x27,
    "BAY7": 0x28,
    "BAY8": 0x29,
    "BAY9": 0x2A,
    "USB": 0x50
}

error_codes: dict[str, int] = {
    "FT_OK: Success": 0,
    "FT_InvalidHandle: The FTDI functions have not been initialized.": 1,
    "FT_DeviceNotFound: The device could not be found. This can be generated if the function"
    " buildDeviceList() has not been called.": 2,
    "FT_DeviceNotOpened: The device must be opened before it can be accessed. See the appropriate"
    " open function for your device.": 3,
    "FT_IOError: An I/O error has occurred in the FTDI chip.": 4,
    "FTInsufficientResources: There are insufficient resources to run this application.": 5,
    "FT_InvalidParameter: An Invalid parameter has been supplied to the device.": 6,
    "FT_DeviceNotPresent: The device is no longer present. The device may have been disconnected"
    " since the last BuildDeviceList() call.": 7,
    "FT_IncorrectDevice: The device detected does not match that expected.": 8,
    "FT_NoDLLLoaded: The library for this device  could not be found.": 16,
    "FT_NoFunctionsAvailable: No functions available for this device.": 17
}


class ITR(Instrument):

    def __init__(self, adapter, name="ThorlabsMotor", baud_rate=115200,
                 source=0x01, destination=0x50):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=False,
            baud_rate=baud_rate,
            timeout=4,
            parity=Parity.none,
            stop_bits=StopBits.one,
        )
        self.destination = destination
        self.source = source

    def identify(self, channel: int = 0) -> None:
        """
        Instruct hardware unit to identify itself (by flashing its front panel LEDs).
        channel:    Used to select the channel to be identified. In single-channel controllers the
        Channel Ident byte is ignored as the destination of the command is uniquely identified by
        the USB serial number of the controller.
        """
        self.write(bytes([23, 2, channel, 0, self.destination, self.source]))

    def enableChannel(self, _channel: int, _enable: bool):
        """
        Enable or disable the specified drive channel.
        channel:    Channel to be enabled
        enable: Choose to enable or disable channel
        """
        if _enable:
            enable = 0x01
        else:
            enable = 0x02
        if _channel == 2:
            channel = 0x02
        elif _channel == 3:
            channel = 0x04
        elif _channel == 4:
            channel = 0x08
        else:
            channel = 0x01

        self.write(bytes([10, 2, channel, enable, self.destination, self.source]))

    def disconnect(self) -> None:
        """
        Disconnect from the Ethernet/USB bus.
        """
        self.write(bytes([2, 0, 0, 0, self.destination, self.source]))

    def checkErrors(self) -> str | tuple[str, str, str]:
        """
        Check for error.
        Return:
            MsgIdent:   If the message is sent in response to an Thorlabs Software message, these
                        bytes show the message number that evoked the message. Most often though
                        the message is transmitted due to some unexpected fault condition, in which
                        case these bytes are 0x00, 0x00
            Code:   This is an internal Thorlabs specific code that specifies the condition that
                    has caused the message (see Return Codes).
            Notes:  This is a zero-terminated printable (ascii) text string that contains the
                    textual information about the condition that has occurred. For example:
                    “Hardware Time Out Error”.
        """
        self.write(bytes([80, 0, 0, 0, self.destination, self.source]))
        raw_answer = self.read()
        if raw_answer[0] != 81:
            return "Read message is not a valid error message."
        else:
            # If the message is sent in response to an Thorlabs Software message, these bytes show
            # the message number that evoked the message. Most often though the message is
            # transmitted due to some unexpected fault condition, in which case these bytes are
            # 0x00, 0x00
            MsgIdent = raw_answer[6:7]

            Code = raw_answer[8:9]
            data: str = ''.join(chr(i) for i in raw_answer[10:71])

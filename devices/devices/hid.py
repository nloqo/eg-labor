# -*- coding: utf-8 -*-
"""
Using human interface devices for controlling programs via qt.

classes
-------
Gamepad
    A gamepad module.

Created on Tue Dec 21 19:38:54 2021 by Benedikt Burger
"""

from qtpy import QtCore
from qtpy.QtCore import Slot as pyqtSlot, Signal as pyqtSignal  # type: ignore

import inputs


class Gamepad(QtCore.QObject):
    """A gamepad class exposing communication to programs via signals."""

    start = pyqtSignal()
    disconnected = pyqtSignal()
    axisChanged = pyqtSignal(str, float)
    buttonPressed = pyqtSignal(str)
    buttonReleased = pyqtSignal(str)
    error = pyqtSignal(Exception)

    def __init__(self, connect: bool = True) -> None:
        """Initialize the gamepad."""
        super().__init__()
        thread = QtCore.QThread()
        thread.start()
        self._listener = self.GamepadListener(self)
        self._listener.moveToThread(thread)
        self.start.connect(self._listener.listen)
        if connect:
            self.connect()
        self._thread = thread

    class GamepadListener(QtCore.QObject):
        """Listening for gamepad events and emitting signals."""

        def __init__(self, parent: "Gamepad") -> None:
            """Initialize the listener for the `parent` instance."""
            super().__init__(parent=parent)
            self.gamepad = parent
            self.listening = False

        def listen(self) -> None:
            """Wait for events and emit signals."""
            try:
                # Search for new connected devices and get the first gamepad.
                inputs.devices._post_init()
                gamepad = inputs.devices.gamepads[0]
            except IndexError:
                self.gamepad.disconnected.emit()
                return  # No gamepad present
            self.listening = True
            events = ()
            while self.listening:
                for event in events:
                    if event.code == "SYN_REPORT":
                        continue
                    if event.ev_type == "Absolute":
                        self.gamepad.axisChanged.emit(event.code, event.state)
                    elif event.ev_type == "Key" and event.state:
                        self.gamepad.buttonPressed.emit(event.code)
                    elif event.ev_type == "Key" and not event.state:
                        self.gamepad.buttonReleased.emit(event.code)
                    else:
                        print(event.ev_type, event.code, event.state)
                try:
                    events = gamepad.read()
                except inputs.UnpluggedError:
                    self.gamepad.disconnected.emit()
                    self.listening = False
                    break  # No gamepad found
                except inputs.UnknownEventType:
                    print("unknown event type")
                except Exception as exc:
                    self.gamepad.disconnected.emit()
                    print(f"read Error {type(exc).__name__}: {exc}")
                    self.listening = False
                    break

    def __del__(self) -> None:
        """Close the connection."""
        self.close()

    @pyqtSlot()
    def close(self) -> None:
        """Close the connection."""
        self._listener.listening = False
        self._thread.quit()
        self._thread.wait(100)

    @pyqtSlot()
    def connect(self) -> None:
        """Connect to a gamepad and start to listen for events."""
        if not self._listener.listening:
            self.start.emit()

    @pyqtSlot()
    def disconnect(self) -> None:
        """Stop to listen for a gamepad."""
        self._listener.listening = False

    @pyqtSlot(float, float, int)
    def vibrate(self, left: float = 0, right: float = 0, duration: int = 100) -> None:
        """
        Vibrate the controller.

        Parameters
        ----------
        left : float between 0 and 1
            Intensity of the left vibration motor.
        right : float between 0 and 1
            Intensity of the right vibration motor.
        duration : int
            Duration of the vibration in ms.
        """
        try:
            inputs.devices.gamepads[0].set_vibration(left, right, duration)
        except (inputs.UnpluggedError, IndexError):
            print("unplugged")
        except Exception as exc:
            print(f"Vibration Exception {exc}")

    """
    Xbox 360 Controller
    Axes in ("ABS_X", "ABS_Y", "ABS_RX", "ABS_RY", "ABS_RZ", "ABS_HAT0X",
             "ABS_HAT0Y")
    Keys in ("BTN_NORTH", "BTN_EAST", "BTN_SOUTH", "BTN_WEST", "BTN_TL",
             "BTN_TR"). Start / Select buttons are not yet known.
    """

# flake8: noqa
"""
Explain the communication with TRINAMIC motor cards.
TMCL - TINAMIC Motion Control Language

Execute for default imports and creation of a connectionManager `cm`.


Created on Thu Dec 10 16:21:33 2020 by Benedikt Moneke
"""

# General import
import sys
import time

# import PyTrinamic
import pytrinamic
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110

"""
Official package by TRINAMIC
https://pypi.org/project/PyTrinamic/
https://github.com/trinamic/PyTrinamic

the github repository contains examples
"""


if __name__ == "__main__":
    """Prepare a module if executed."""
    pytrinamic.show_info()
    if len(sys.argv) > 1:
        args = f"--port {sys.argv[1]}"
        # "--port COM5" for example to connect to serial port 5
        cm = ConnectionManager(args)
        interface = cm.connect()
        mod = TMCM6110(interface)
    else:
        print("no port was given")


# From here on examples of usage or lists of names.
def examples(port="COM1", motor=0, velocity=40000, duration=5):
    """Different example usages."""
    connectionManager = ConnectionManager(f"--port {port}")
    # Serial port on Linux: /dev/ttyACM0
    interface = connectionManager.connect()
    Module_6110 = TMCM6110(interface)
    mot = Module_6110.motors[motor]

    print("Preparing parameters")
    Module_6110.motors[motor].linear_ramp.max_acceleration = 9000

    print("Rotating")
    mot.rotate(velocity)
    # or: Module_3110.rotate(motor, velocity)

    time.sleep(duration)

    print("Stopping")
    mot.stop()

    print("ActualPosition")
    print(mot.actual_position)
    time.sleep(duration)

    print("Doubling moved distance")
    mot.move_by(mot.actual_position, velocity)
    while not (mot.get_position_reached()):
        pass

    print("Furthest point reached")
    print(mot.actual_position)

    time.sleep(duration)

    print("Moving back to 0")
    mot.move_to(0)

    # Wait until position 0 is reached
    while not (mot.get_position_reached()):
        pass

    print(f"Reached Position {mot.actual_position}")

    print("Close connection")
    interface.close()


def input_output_example(port="COM1"):
    """Example on the usage of the input/output ports."""
    connectionManager = ConnectionManager(f"--port {port}")
    # Serial port on Linux: /dev/ttyACM0
    interface = connectionManager.connect()
    mod = TMCM6110(interface)

    # digital out
    mod.set_digital_output(mod.IO.OUT6)  # setze den Output auf an
    assert mod.get_digital_output(mod.IO.OUT6) is True  # lese den Output status aus
    mod.clear_digital_output(mod.IO.OUT6)  # setze den Output auf aus

    # input as int between 0 and 4095
    print(mod.get_analog_input(mod.IO.AIN0))
    print(mod.get_digital_input(mod.IO.IN3))
    #   read the input voltage in V
    print(mod.get_analog_input(8) / 10)  # at least for TMCM_6110 it is value 8


class _APs:
    """Collection of all axis parameters."""
    TargetPosition                 = 0
    ActualPosition                 = 1
    TargetVelocity                 = 2
    ActualVelocity                 = 3
    MaxVelocity                    = 4
    MaxAcceleration                = 5
    MaxCurrent                     = 6
    StandbyCurrent                 = 7
    PositionReachedFlag            = 8
    ReferenceSwitchStatus          = 9
    RightEndstop                   = 10  # The logical state of the right limit switch input
    LeftEndstop                    = 11
    RightLimitSwitchDisable        = 12  # Disable limit switch
    LeftLimitSwitchDisable         = 13
    MinimumSpeed                   = 130
    ActualAcceleration             = 135
    RampMode                       = 138  # How to generate steps
    MicrostepResolution            = 140  # Exponent of 2 of number of microsteps
    ReferenceSwitchTolerance       = 141
    SoftStopFlag                   = 149  # 0 hard stop, 1 deceleration ramp at stop switch
    EndSwitchPowerDown             = 150  # 0 run current, 1 standby current at stop.
    RampDivisor                    = 153
    PulseDivisor                   = 154
    Intpol                         = 160
    DoubleEdgeSteps                = 161
    ChopperBlankTime               = 162
    ConstantTOffMode               = 163
    DisableFastDecayComparator     = 164
    ChopperHysteresisEnd           = 165
    ChopperHysteresisStart         = 166
    TOff                           = 167
    SEIMIN                         = 168
    SECDS                          = 169
    smartEnergyHysteresis          = 170
    SECUS                          = 171
    smartEnergyHysteresisStart     = 172
    SG2FilterEnable                = 173
    SG2Threshold                   = 174
    SlopeControlHighSide           = 175
    SlopeControlLowSide            = 176
    ShortToGroundProtection        = 177
    ShortDetectionTime             = 178
    VSense                         = 179
    smartEnergyActualCurrent       = 180
    smartEnergyStallVelocity       = 181
    smartEnergyThresholdSpeed      = 182
    smartEnergySlowRunCurrent      = 183
    RandomTOffMode                 = 184
    ReferenceSearchMode            = 193  # How to search for the reference via limit switches
    ReferenceSearchSpeed           = 194  # How fast to search for the limit.
    ReferenceSwitchSpeed           = 195  # How fast to search for the exact position.
    ReferenceSwitchDistance        = 196  # Distance between the two limit switches.
    LastReferenceSwitchPosition    = 197  # Last position before counter has been reset to 0 during reference search.
    BoostCurrent                   = 200  # current for acceleration. If == 0, value of Parameter 6 is used.
    EncoderMode                    = 201
    FreewheelingDelay              = 204
    LoadValue                      = 206  # actual load value for stall detection
    ExtendedErrorFlags             = 207  # 0 no error. 1 stallGuardError. Flag is cleared on reading it
    DrvStatusFlags                 = 208  # status flags
    EncoderPosition                = 209
    EncoderPrescaler               = 210
    MaxEncoderDeviation            = 212
    GroupIndex                     = 213
    PowerDownDelay                 = 214
    StepDirectionMode              = 254

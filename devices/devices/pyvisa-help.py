"""Information about pyvisa and its defaults

Connection defaults
-------------------
- ASRL (serial communication)

"""

from pyvisa import constants, ResourceManager  # noqa: F401
from pyvisa.resources import Resource, MessageBasedResource, SerialInstrument  # type: ignore  # noqa


class MessageBasedResource(Resource):  # noqa: F811
    # see pyvisa.resources.MessageBasedResource
    read_termination = None  # uses write_termination
    write_termination = "\r\n"


class ASRL(MessageBasedResource):
    """ASRL - asynchronous serial devices"""

    baud_rate = 9600
    stop_bits = constants.StopBits.one  # ONE = 10, ONE5 = 15, TWO = 20
    parity = constants.Parity.none  # NONE = 0, ODD = 1, EVEN = 2, MARK = 3
    data_bits = 8  # 5-8
    flow_control = constants.ControlFlow.none  # NONE = 0


class SomeDevice:
    """Example on how to use pyvisa.

    :param address: Address string for pyvisa, e.g. "ASRL7".
    """

    def __init__(self, address, **kwargs):
        rm = ResourceManager()
        self._connection = rm.open_resource(
            address,
            baud_rate=115200,
            data_bits=8,
            parity=constants.Parity.none,
            stop_bits=constants.StopBits.one,
            write_termination=",",
            timeout=1000,
        )

    def write(self, command):
        """Write `command` to the device."""
        self._connection.write(command)

    def read(self):
        """Read the device's response."""
        return self._connection.read()

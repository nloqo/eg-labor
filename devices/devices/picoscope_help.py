# flake8: noqa
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 09:52:19 2021 by Oskar Ernst

Documentation on the following GitHub-Link:
https://github.com/colinoflynn/pico-python
"""

from picoscope import ps4000a


# Pico_Base functions


def methods():
    """List all the methods of Picoscope_Base program."""
    return
    changePowerSource(self, powerstate)
    checkResult(self, ec)
    close(self)
    __del__(self)
    enumerateUnits(self)
    errorNumToDesc(self, num)
    errorNumToName(self, num)
    flashLed(self, times=5, start=False, stop=False)
    getAllUnitInfo(self)
    getAWGDeltaPhase(self, timeIncrement)
    getAWGTimeIncrement(self, deltaPhase)
    getDataRaw(
        self,
        channel="A",
        numSamples=0,
        startIndex=0,
        downSampleRatio=1,
        downSampleMode=0,
        segmentIndex=0,
        data=None,
    )
    getDataRawBulk(
        self,
        channel="A",
        numSamples=0,
        fromSegment=0,
        toSegment=None,
        downSampleRatio=1,
        downSampleMode=0,
        data=None,
    )
    getDataV(
        self,
        channel,
        numSamples=0,
        startIndex=0,
        downSampleRatio=1,
        downSampleMode=0,
        segmentIndex=0,
        returnOverflow=False,
        exceptOverflow=False,
        dataV=None,
        dataRaw=None,
        dtype=np.float64,
    )
    getMaxMemorySegments(self)
    getMaxValue(self)
    getMinValue(self)
    getScaleAndOffset(self, channel)
    getTriggerTimeOffset(self, segmentIndex=0)
    getUnitInfo(self, info)
    """
        -> UNIT_INFO_TYPES = {"DriverVersion": 0x0,
                              "USBVersion": 0x1,
                              "HardwareVersion": 0x2,
                              "VariantInfo": 0x3,
                              "BatchAndSerial": 0x4,
                              "CalDate": 0x5,
                              "KernelVersion": 0x6,
                              "DigitalHardwareVersion": 0x7,
                              "AnalogueHardwareVersion": 0x8,
                              "PicoFirmwareVersion1": 0x9,
                              "PicoFirmwareVersion2": 0xA}
    """
    __init__(self, serialNumber=None, connect=True)
    isReady(self)
    memorySegments(self, noSegments)
    open(self, serialNumber=None)
    openUnitAsync(self, serialNumber=None)
    openUnitProgress(self)
    ping(self)
    rawToV(self, channel, dataRaw, dataV=None, dtype=np.float64)
    runBlogetDatack(self, pretrig=0.0, segmentIndex=0)
    setAWGSimple(
        self,
        waveform,
        duration,
        offsetVoltage=None,
        pkToPk=None,
        indexMode="Single",
        shots=1,
        triggerType="Rising",
        triggerSource="ScopeTrig",
    )
    setAWGSimpleDeltaPhase(
        self,
        waveform,
        deltaPhase,
        offsetVoltage=None,
        pkToPk=None,
        indexMode="Single",
        shots=1,
        triggerType="Rising",
        triggerSource="ScopeTrig",
    )
    setChannel(
        self,
        channel,
        coupling,
        VRange,
        VOffset,
        enabled=True,
        BWLimited=0,
        probeAttenuation=1.0,
    )
    """
        ->  channel:            "A"-"H"
            coupling:           "AC" / "DC"
            VRange:             20e-3 - 50
            VOffset:             0.0 - ...
            BWLimited:          Bandwidth Limiter (fixed for specific Picoscope version; for the ps4824 it's 0')
            probeAttenuation:   If using a probe (or a sense resistor), the probeAttenuation value is 
                                used to find the appropriate channel range on the scope to use.
    """
    setExtTriggerRange(self, VRange=0.5)
    setNoOfCaptures(self, noCaptures)
    setResolution(self, resolution)
    setSamplingFrequency(self, sampleFreq, noSamples, oversample=0, segmentIndex=0)
    setSamplingInterval(self, sampleInterval, duration, oversample=0, segmentIndex=0)
    setSigGenBuiltInSimple(
        self,
        offsetVoltage=0,
        pkToPk=2,
        waveType="Sine",
        frequency=1e6,
        shots=1,
        triggerType="Rising",
        triggerSource="None",
        stopFreq=None,
        increment=10.0,
        dwellTime=1e-3,
        sweepType="Up",
        numSweeps=0,
    )
    setSimpleTrigger(
        self,
        trigSrc,
        threshold_V=0,
        direction="Rising",
        delay=0,
        timeout_ms=100,
        enabled=True,
    )
    """
        ->  THRESHOLD_TYPE = {"Above": 0,
                              "Below": 1,
                              "Rising": 2,
                              "Falling": 3,
                              "RiseOrFall": 4}
    """
    sigGenSoftwareControl(self, state=True)
    stop(self)
    waitReady(self, spin_delay=0.01)


# ps4000a functions


def methods_ps4000a():
    return

    def __init__(self, serialNumber=None, connect=True):
        getTimeBaseNum(self, sampleTimeS)
        getTimestepFromTimebase(self, timebase)
        getTimestepFromTimebase(self, timebase)

"""
Communication with High Finesse Wavemeters and optical spectrometers.

See the manual which is available via the HighFinesse Software.
The Software has to be running in order to use the data.

classes
---------
Wavemeter
    Communication with High Finesse wavemeters

Execute for getting a wavemeter called `wlm`.

Ideas from https://github.com/stepansnigirev/py-ws7/

The package pylablib offers a more exhaustive wavemeter implementation.
https://github.com/AlexShkarin/pyLabLib/tree/main/pylablib/devices/HighFinesse

Here is yet another one, in the style of instrumentkit
https://github.com/RIMS-Code/high_finesse_ws6

Created on Mon May  3 12:09:29 2021 by Benedikt Moneke
"""

from ctypes import CFUNCTYPE, c_void_p, c_int32, c_double
from enum import IntEnum
from typing import Callable, Optional

from .wlmData import load_wlm_data_dll
from . import wlmConst


def callback_generator(
    function: Optional[Callable[[int, int, int, float, int], None]],
) -> Optional[Callable[[int, int, int, float, int], None]]:
    """
    void CallbackProc(long Ver, long Mode, long IntVal, double DblVal, long Res1)

    Parameters
    ----------
    VER
        Device version number which called.
    Mode
        Indicates meaning of IntVal and DblVal (cmi constants)
    IntVal
        Exact measurement time stamp rounded to ms. The timestamp reflects the
        point in time at which the Wavelength Meter has received the
        interferometer pattern, before calculation of the wavelength and
        exporting.
    DblVal
        Corresponding value.
    Res1
        With multichannel switch: switching timestamp
    )
    """
    if function is None:
        return None

    callback = CFUNCTYPE(c_void_p, c_int32, c_int32, c_int32, c_double, c_int32)
    return callback(function)


class Wavemeter:
    """High Finesse wavemeter class.

    :param dllpath: Path to the dll file.
    """

    def __init__(self, dllpath=r"C:\Windows\System32\wlmData.dll") -> None:
        """Initialize the wavemeter."""
        self.channels = []
        self.dllpath = dllpath
        self.dll = load_wlm_data_dll(dllpath)
        self.initialize(self.INIT_REASONS.RESET_CALC)

    class INIT_REASONS(IntEnum):
        CHECK_FOR_WLM = wlmConst.cInstCheckForWLM
        RESET_CALC = wlmConst.cInstResetCalc

    class INIT_MODES(IntEnum):
        LATEST = 0  # return the latest value
        ZERO_AFTER = 1  # zero after returning the value

    def initialize(self, reason: INIT_REASONS, mode: INIT_MODES = INIT_MODES.LATEST):
        """Initialize a wavemeter and return True on success."""
        got = self.dll.Instantiate(c_int32(reason), c_int32(mode), 0, 0)
        self.range = wlmConst.cRangeModelByWavelength
        return got

    def setup_callback(
        self, callback: Optional[Callable[[int, int, int, float, int], None]] = None
    ) -> None:
        """Install a callback method or remove it if 'None'."""
        if callback is None:
            return self.dll.Instantiate(
                wlmConst.cInstNotification,
                wlmConst.cNotifyRemoveCallback,
                0,
                0,  # thread priority, standard priority = 0
            )
        else:
            self._c_callback = callback_generator(callback)
            return self.dll.Instantiate(
                c_int32(wlmConst.cInstNotification),
                c_int32(wlmConst.cNotifyInstallCallbackEx),
                self._c_callback,
                0,  # thread priority. standard = 0
            )

    def connection_enabled(self) -> bool:
        """Get whether a connection exists."""
        return bool(self.dll.Instantiate(wlmConst.cInstCheckForWLM, 0, 0, 0))

    def checkConnection(self) -> bool:
        """Return whether a connection exists."""
        # TODO deprecated
        return self.connection_enabled()

    @property
    def type(self) -> int:
        """
        The device type.

        Wavelength Meter (5 to 9) or Laser Spectrum Analyser (4 or 5).
        """
        return self.dll.GetWLMVersion(0)

    @property
    def version(self) -> int:
        """Version number of the device."""
        return self.dll.GetWLMVersion(1)

    @property
    def software_version(self):
        """Revision number of the software."""
        return self.dll.GetWLMVersion(2)

    def get_index(self, version) -> int:
        """Get the dll handling index for the device with `version` number."""
        return self.dll.GetWLMIndex(version)

    def set_index(self, version: int) -> None:
        """
        Set the current handling index.

        `version` can be either the index or a device version number.
        """
        self.dll.PresetWLMIndex(version)

    @property
    def wlm_count(self) -> int:
        """Number of different active wavelength servers (programs open)."""
        return self.dll.GetWLMCount(0)

    @property
    def auto_exposure_enabled(self) -> bool:
        """Control whether automatic exposure is active or not."""
        return self.dll.GetExposureMode(0) == 1

    @auto_exposure_enabled.setter
    def auto_exposure_enabled(self, value: bool) -> None:
        return self.dll.SetExposureMode(value)

    @property
    def exposureMode(self) -> bool:
        """Return whether automatic exposure is active or not."""
        # TODO deprecated
        return self.auto_exposure_enabled

    @exposureMode.setter
    def exposureMode(self, b: bool):
        """(De)Activate automatic exposure."""
        # TODO deprecated
        self.auto_exposure_enabled = b

    @property
    def exposure(self, channel: int = 1) -> int:
        """Exposure time in ms."""
        return self.dll.GetExposureNum(channel, 1, 0)

    @exposure.setter
    def exposure(self, value: int, channel: int = 1):
        self.dll.SetExposureNum(channel, 1, value)

    @property
    def range(self) -> int:
        """Wavelength index 0 based. Set desired wavelength in nm."""
        # WS6-200: Range 6 is below 1000 nm and Range 7 is above 1000 nm
        return self.dll.GetRange(0)

    @range.setter
    def range(self, value: int) -> None:
        self.dll.SetRange(value)

    @property
    def wavelength(self, channel: int = 1) -> float:
        """Return the wavelength for `channel` in nm."""
        # The second parameter should be left zero.
        got = self.dll.GetWavelengthNum(channel, 0)
        if got < 0:
            # Errors are negative numbers, see wlmConst.Err...
            return float("nan")
        else:
            return got

    @property
    def frequency(self, channel: int = 1) -> float:
        """Return the frequency for `channel` in THz."""
        got = self.dll.GetFrequencyNum(channel, 0)
        if got < 0:
            return float("nan")
        else:
            return got

    @property
    def power(self, channel=1) -> float:
        """Return the power of the current measurement shot in µW or µJ."""
        got = self.dll.GetPowerNum(channel, 0)
        if got > 0:
            return got
        elif got == wlmConst.ErrNoSignal:
            return 0
        elif got in (wlmConst.ErrBadSignal, wlmConst.ErrLowSignal, wlmConst.ErrNoPulse):
            return float("nan")
        elif got == wlmConst.ErrBigSignal:
            return float("inf")
        elif got in (wlmConst.ErrWlmMissing, wlmConst.ErrNotAvailable):
            raise ConnectionError("Wavemeter or channel not available.")
        else:
            raise ConnectionError("Something happened during power readout.")

    def getAll(self) -> dict[str, float]:
        # TODO deprecated
        return self.get_all()

    def get_all(self) -> dict[str, float]:
        return {
            "wavelength": self.wavelength,
            "frequency": self.frequency,
            "exposureMode": self.exposureMode,
        }

    @property
    def temperature(self) -> float:
        """Get temperature inside the optical unit."""
        # argument is reserved for future use, but is obligatory, therefore 0 as just anything.
        return self.dll.GetTemperature(0)

    @property
    def pressure(self) -> float:
        """Get pressure inside the optical unit."""
        # argument is reserved for future use, but is obligatory, therefore 0 as just anything.
        return self.dll.GetPressure(0)


if __name__ == "__main__":
    wlm = Wavemeter()
    print(f"Wavelength {wlm.wavelength}")

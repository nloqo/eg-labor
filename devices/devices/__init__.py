"""
Library for communication with devices in the EG lab.

Either classes to model the devices or help-files for manufacturer provided
classes.

If several devices of the same manufacturere have several files, they are in a
subpackage. Otherwise a single file for the manufacturer.

It is recommended to use `pymeasure.Instrument` as a base and write tests with
`pymeasure.test.expected_protocol`. See https://pymeasure.readthedocs.io


Sources of more devices (if not told otherwise: pypip package)
-----------------------
pymeasure
    Connection to several devices, fitting routines etc.
pylablib
    Contains some devices, especially several cameras. Ugly source code.
"""

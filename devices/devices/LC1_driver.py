"""https://www.ivifoundation.org/downloads/IVI%20GSG%202019/
    Getting%20Started%20with%20IVI%20and%20Python.pdf"""

import ctypes as ct

# Character types
ViChar = ct.c_char
ViInt8 = ct.c_int8
ViInt16 = ct.c_int16
ViUInt16 = ct.c_uint16
ViInt32 = ct.c_int32
ViUInt32 = ct.c_uint32
ViInt64 = ct.c_int64
ViString = ct.c_char_p
ViReal32 = ct.c_float
ViReal64 = ct.c_double

ViBoolean = ViUInt16
VI_TRUE = ViBoolean(True)
VI_FALSE = ViBoolean(False)
ViStatus = ViInt32
ViSession = ViUInt32
ViAttr = ViUInt32
ViConstString = ViString
ViRsrc = ViString

# Load library and get an initial session
LC1dll = ct.cdll.LoadLibrary("C:\\MeasurementSoftwarePython\\devices\\devices\\TLLC_64.dll")


# %% Initialize

def init(resourceNameString: str, IDQuery: ViBoolean = VI_TRUE,
         resetDevice: ViBoolean = VI_TRUE) -> tuple[ViSession, str]:
    """
    Initialize the connection to LC100.
    :param resourceNameString:  specifies the device that is to be initialized (Resource Name). The
                                syntax for this parameter is shown below.
                                "USB0::0x1313::0x80A0::DEVICE-SERIAL-NUMBER::RAW"
    :param IDQuery: specifies whether an identification query is performed during the
                    initialization process
                        VI_OFF (0): Skip query
                        VI_ON  (1): Do query (default)
    :param: resetDevice:    specifies whether the instrument is reset during the initialization
                            process.
                                VI_OFF (0) - no reset
                                VI_ON  (1) - instrument is reset (default)
    :return: tuple with session and error message
    """
    session = ViSession()
    instrumentHandle = ct.pointer(session)
    resourceName = ct.create_string_buffer(resourceNameString.encode('windows-1251'))
    answer = LC1dll.tllc100_init(resourceName, IDQuery, resetDevice, instrumentHandle)
    return (instrumentHandle.contents, errorMessage(answer, instrumentHandle.contents))

'''
# %% Configuration Functions

def setIntTime(integrationTimes: float, instrumentHandle: ViSession) -> str:
    """
    Set the integration time in seconds.
    :param integrationTime: specifies the optical integration time in seconds
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_setIntegrationTime(instrumentHandle, ViReal64(integrationTimes))
    return errorMessage(answer, instrumentHandle)


def getIntTime(attribute: str, instrumentHandle: ViSession) -> tuple[float, str]:
    """Get the integration time in seconds.
       :param attribute:  The desired integration time. Valid values:
                            - 'act':    get the actual value
                            - 'min':    get the minimum value
                            - 'max':    get the maximum value
                            - 'def':    get the default value
        :param instrumentHandle:    session given by init()
        :return:    tuple with integration time and error message
    """
    integrationTimes = ct.pointer(ViReal64(0))
    if attribute == 'act':
        attribute = ViInt16(0)
    elif attribute == 'min':
        attribute = ViInt16(1)
    elif attribute == 'max':
        attribute = ViInt16(2)
    elif attribute == 'def':
        attribute = ViInt16(3)
    else:
        return (0.0, 'No valid attribute given. Choose between act, min, max and def.')
    answer = LC100dll.tllc100_getIntegrationTime(instrumentHandle, attribute, integrationTimes)
    return (integrationTimes.contents.value, errorMessage(answer, instrumentHandle))


def setOperatingMode(operatingMode: str, instrumentHandle: ViSession) -> str:
    """
    Set the operating mode.
    :param operatingMode:   The desired operation mode.
                                - 'idle:        idle mode, camera does nothing
                                - 'SW-single':  waits for a software trigger
                                - 'SW-loop':    internal triggermode (continuous)
                                - 'HW-single':  waits for a hardware trigger
                                - 'HW-loop':    externally triggermode
    :param instrumentHandle:    session given by init()
    :return: error message
    """
    if operatingMode == 'idle':
        operatingMode = ViInt32(0)
    elif operatingMode == 'SW-single':
        operatingMode = ViInt32(1)
    elif operatingMode == 'SW-loop':
        operatingMode = ViInt32(2)
    elif operatingMode == 'HW-single':
        operatingMode = ViInt32(3)
    elif operatingMode == 'HW-loop':
        operatingMode = ViInt32(4)
    else:
        return ('operating mode is not defined. Choose between idle, SW-single, SW-loop, HW-single'
                'and HW-loop.')
    answer = LC100dll.tllc100_setOperatingMode(instrumentHandle, operatingMode)
    return errorMessage(answer, instrumentHandle)


def getOperatingMode(instrumentHandle: ViSession) -> tuple[str, str]:
    """
    Get the operating mode.
    :param instrumentHandle:    session given by init()
    : return: tuple with operating mode and error message
    """
    operatingMode = ct.pointer(ViUInt16(0))
    answer = LC100dll.tllc100_getOperatingMode(instrumentHandle, operatingMode)
    if operatingMode.contents.value == 0:
        operatingMode = 'idle'
    elif operatingMode.contents.value == 1:
        operatingMode = 'SW-single'
    elif operatingMode.contents.value == 2:
        operatingMode = 'SW-loop'
    elif operatingMode.contents.value == 3:
        operatingMode = 'HW-single'
    elif operatingMode.contents.value:
        operatingMode = 'HW-loop'
    else:
        operatingMode = 'unknown mode'
    return (operatingMode, errorMessage(answer, instrumentHandle))


# Not working yet: 'Invalid USB control transfer parameter index' (LabView VI is also not working)
def setHWAveragingMode(averagingMode: int, instrumentHandle: ViSession) -> str:
    """
    Set the hardware averaging mode.
    :param averagingMode:   Averaging can only be done in powers of 2.
                                - 0 -> no averaging, one single scan at a time
                                - 1 -> 2 scans will be averaged
                                ...
                                - 9 -> 512 scans will be averaged
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    averagingMode = ViInt32(averagingMode)
    answer = LC100dll.tllc100_setHWAveragingMode(instrumentHandle, averagingMode)
    return errorMessage(answer, instrumentHandle)


# Not working yet: 'Invalid USB control transfer parameter index' (LabView VI is also not working)
def getHWAveragingMode(attribute: str, instrumentHandle: ViSession) -> tuple[int, str]:
    """
    Get the averaging mode as the number of measurements averaged.
    :param attribute:   the desired Hardware Averaging Mode
                            - 'act':    get the actual value
                            - 'min':    get the minimum value
                            - 'max':    get the maximum value
                            - 'def':    get the default value
    :param instrumentHandle:    session given by init()
    :return:    tuple with number of averaging shots and the error message
    """
    hardwareAveragingMode = ct.pointer(ViInt32(0))
    if attribute == 'act':
        attribute = ViInt16(0)
    elif attribute == 'min':
        attribute = ViInt16(1)
    elif attribute == 'max':
        attribute = ViInt16(2)
    elif attribute == 'def':
        attribute = ViInt16(3)
    else:
        return (-1, 'No valid attribute given. Choose between act, min, max and def.')
    answer = LC100dll.tllc100_getHWAveragingMode(instrumentHandle, attribute, hardwareAveragingMode)
    return (hardwareAveragingMode.contents.value, errorMessage(answer, instrumentHandle))


# %%% Intensity Correction
# TODO: implement function setIntensityCorr
def setIntensityCorr(intensityCorrectionValues: list[float], instrumentHandle: ViSession):
    """
    Set the intensity correction with an array intensityCorrectionValues with the length as the
    number of pixels, where each element represents the multiplier for the pixel.
    :param intensityCorrelationValues:  The intensity correction values array (must contain at least
                                        TLLC100_NUM_PIXELS elements)
    :param instrumentHandle:    session given by init()
    :return: error message
    """
    pass


def getIntensityCorr(instrumentHandle: ViSession) -> tuple[str, str]:
    """
    Get the intensity correction as an array intensityCorrectionValues with the length of the
    number of pixels, where each element represents the multiplier for the pixel.
    :param instrumentHandle:    session given by init()
    :return: tuple with the array containing the IntensityCorrelation values and the error message
    """
    intensityCorrectionValues = ct.create_string_buffer('\000'.encode('windows-1251'), 2048)
    answer = LC100dll.tllc100_getIntensityCorr(instrumentHandle, intensityCorrectionValues)
    return (intensityCorrectionValues.value.decode('windows-1251'),
            errorMessage(answer, instrumentHandle))


def resetIntensityCorr(instrumentHandle: ViSession) -> str:
    """
    Reset the intensity correction values.
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_resetIntensityCorr(instrumentHandle)
    return errorMessage(answer, instrumentHandle)


# %%% PatternRecognition

def setEvalBox(x1: int, x2: int, y1: float, y2: float, instrumentHandle: ViSession,
               evalBox: int = 0,) -> str:
    """
    Set a range / interval of a specified evaluation box evalBox with pixels between x1 and x2
    as well as a level between y1 and y2.
    :param x1:  the start pixel
    :param x2:  the end pixel
    :param y1:  the lower level
    :param y2:  the upper level
    :param instrumentHandle:    session given by init()
    :param evalBox: the desired evaluation box (0 to TLLC100_NUM_EVALBOX)
    :return:    error message
    """
    answer = LC100dll.tllc100_setEvalBox(instrumentHandle, ViInt16(evalBox), ViInt32(x1),
                                         ViInt32(x2), ViReal64(y1), ViReal64(y2))
    return errorMessage(answer, instrumentHandle)


def getEvalBox(evalBox: int, instrumentHandle: ViSession) -> tuple[tuple[int, int],
                                                                   tuple[float, float], str]:
    """
    Get the range / interval of the specified evaluation box evalBox.
    :param evalBox: the desired evaluation box (0 to TLLC100_NUM_EVALBOX)
    :param instrumentHandle:    session given by init()
    :return:    tuple with a tuple of the x1/x2 value, a tuple of the y1/y2 values
                and the error message
    """
    x1 = ct.pointer(ViInt32(0))
    x2 = ct.pointer(ViInt32(0))
    y1 = ct.pointer(ViReal64(0.0))
    y2 = ct.pointer(ViReal64(0.0))
    answer = LC100dll.tllc100_getEvalBox(instrumentHandle, ViInt16(evalBox), x1, x2, y1, y2)
    return ((x1.contents.value, x2.contents.value), (y1.contents.value, y2.contents.value),
            errorMessage(answer, instrumentHandle))


def setGPIOFeed(GPIO: int, feed: int, mode: int, instrumentHandle: ViSession) -> str:
    """
    Set the GPIO mapping.
    :param GPIO:    the GPIO number (0 to TLLC100_NUM_GPIO - 1)
    :param feed:    the desired feed. Range 0 to TLLC100_NUM_EVALBOX - 1 (0 - 15) correlates to
                    eval boxes (e.g. 0 means eval box 1, 4 means eval box 5 and so on). Range
                    TLLC100_NUM_EVALBOX to TLLC100_NUM_EVALBOX + TLLC100_NUM_GPIO - 1 (16 - 20)
                    correlates to additional GPIO inputs (e.g. 16 means GPIO1 and so on).
    :param mode:    the desired feed. Range 0 to TLLC100_NUM_EVALBOX - 1 (0 - 15) correlates to
                    eval boxes (e.g. 0 means eval box 1, 4 means eval box 5 and so on). Range
                    TLLC100_NUM_EVALBOX to TLLC100_NUM_EVALBOX + TLLC100_NUM_GPIO - 1 (16 - 20)
                    correlates to additional GPIO inputs (e.g. 16 means GPIO1 Sand so on).
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_setGPIOFeed(instrumentHandle, ViInt16(GPIO), ViInt16(feed),
                                          ViInt16(mode))
    return errorMessage(answer, instrumentHandle)


def getGPIOFeed(GPIO: int, feed: int, instrumentHandle: ViSession) -> tuple[int, str]:
    """
    Get the GPIO mapping.
    :param GPIO:    the GPIO number (0 to TLLC100_NUM_GPIO - 1)
    :param feed:    the desired feed. See "tllc100_setGPIOFeed" function for more information about
                    valid values
    :param instrumentHandle:    session given by init()
    :return:    tuple of mode and error message
    """
    mode = ct.pointer(ViInt16(0))
    answer = LC100dll.tllc100_getGPIOFeed(instrumentHandle, ViInt16(GPIO), ViInt16(feed), mode)
    return (mode.contents.value, errorMessage(answer, instrumentHandle))


def setGPIOMode(GPIO: int, mode: str, instrumentHandle: ViSession) -> str:
    """
    Set the GPIO mode.
    :param GPIO:    the GPIO number (0 to TLLC100_NUM_GPIO - 1)
    :param mode:    the GPIO mode
                        - input     (0) GPIO is used as input
                        - pos       (1) always high
                        - neg       (2) always low
                        - evalPos   (3) high when evaluation pattern is found
                        - evalNeg   (4) low when evaluation pattern is found
                        - expPos    (5) high during exposure
                        - expNeg    (6) low during exposure
                        - flashPos  (7) high for specified strobe/flash parameters
                        - flashNeg  (8) low for specified strobe/flash parameters
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    if mode == 'input':
        mode = ViInt16(0)
    elif mode == 'pos':
        mode = ViInt16(1)
    elif mode == 'neg':
        mode == ViInt16(2)
    elif mode == 'evalPos':
        mode = ViInt16(3)
    elif mode == 'evalNeg':
        mode = ViInt16(4)
    elif mode == 'expPos':
        mode = ViInt16(5)
    elif mode == 'expNeg':
        mode = ViInt16(6)
    elif mode == 'flashPos':
        mode = ViInt16(7)
    elif mode == 'flashNeg':
        mode = ViInt16(8)
    else:
        return ('mode is not defined. Choose between input, pos, neg, evalPos, evalNeg, expPos,'
                'expNeg, flashPos and flashNeg.')
    answer = LC100dll.tllc100_setGPIOMode(instrumentHandle, ViInt16(GPIO), mode)
    return errorMessage(answer, instrumentHandle)


def getGPIOMode(GPIO: int, instrumentHandle: ViSession) -> tuple[str, str]:
    """
    Get the GPIO mode.
    :param GPIO:    the GPIO number (0 to TLLC100_NUM_GPIO - 1)
    :param instrumentHandle:    session given by init()
    :return:    tuple of the mode and the error message
    """
    mode = ct.pointer(ViInt16(0))
    answer = LC100dll.tllc100_getGPIOMode(instrumentHandle, ViInt16(GPIO), mode)
    if mode.contents.value == 0:
        mode = 'input'
    elif mode.contents.value == 1:
        mode = 'pos'
    elif mode.contents.value == 2:
        mode == 'neg'
    elif mode.contents.value == 3:
        mode = 'evalPos'
    elif mode.contents.value == 4:
        mode = 'evalNeg'
    elif mode.contents.value == 5:
        mode = 'expPos'
    elif mode.contents.value == 6:
        mode = 'expNeg'
    elif mode.contents.value == 7:
        mode = 'flashPos'
    elif mode.contents.value == 8:
        mode = 'flashNeg'
    return (mode, errorMessage(answer, instrumentHandle))


def getGPIOstate(GPIO: int, instrumentHandle: ViSession) -> tuple[int, str]:
    """
    Get the GPIO state.
    :param GPIO:    the GPIO number (0 to TLLC100_NUM_GPIO - 1)
    :param instrumentHandle:    session given by init()
    :return:    tuple of the state and the error message
    """
    state = ct.pointer(ViInt32(0))
    answer = LC100dll.tllc100_getGPIOState(instrumentHandle, ViInt16(GPIO), state)
    return (state.contents.value, errorMessage(answer, instrumentHandle))


# %%% Analog Output

def setAnalogOutputMode(mode: str, instrumentHandle: ViSession) -> str:
    """Set the analog output mode.
    :param mode:    the analog output mode. It can be either set to a fixed value
                    (TLLC100_AOUT_MODE_FIXED) or it can be configured as a variable
                    output (TLLC100_AOUT_MODE_VARIABLE)
                        - fixed       (0) fixed voltage output between 0V and 4V
                        - variable    (1) variable voltage output at a specified pixel
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    if mode == 'fixed':
        mode = ViUInt32(0)
    elif mode == 'variable':
        mode = ViUInt32(1)
    else:
        return 'mode is not defined. Choose between fixed and variable.'
    answer = LC100dll.tllc100_setAnalogOutputMode(instrumentHandle, mode)
    return errorMessage(answer, instrumentHandle)


def getAnalogOutputMode(instrumentHandle: ViSession) -> tuple[str, str]:
    """
    Returns the mode of the analog output.
    :param: instrumentHandle:   session given by init()
    :return:    tuple of the mode and the error message
    """
    mode = ct.pointer(ViUInt32(0))
    answer = LC100dll.tllc100_getAnalogOutputMode(instrumentHandle, mode)
    if mode.contents.value == 0:
        return ('fixed', errorMessage(answer, instrumentHandle))
    elif mode.contents.value == 1:
        return ('variable', errorMessage(answer, instrumentHandle))
    else:
        return (str(mode.contents.value), errorMessage(answer, instrumentHandle))


# TODO: not working: 'Parameter 2 out of range'
def setAnalogOutputVoltage(voltage: float, select: str, instrumentHandle: ViSession) -> str:
    """
    Set the analog output voltage or set the lower / upper limit for the analog output voltage
    when a pixel intensity is monitored.
    :param voltage: the voltage to set
    :param select:  specifies the voltage used as:
                        - fixed (0) fixed output voltage
                        - min   (3) minimal voltage which is present when the monitored pixel has
                                    an intensity below (or equal) the configured level
                        - max   (4) maximal voltage which is present when the monitored pixel has an
                                    intensity above (or equal) the configured level
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    if select == 'fixed':
        select == ViInt16(0)
    elif select == 'min':
        select == ViInt16(3)
    elif select == 'max':
        select = ViInt16(4)
    else:
        return 'select is not defined. Choose between fixed, min and max.'
    answer = LC100dll.tllc100_setAnalogOutputVoltage(instrumentHandle, select, ViReal64(voltage))
    return errorMessage(answer, instrumentHandle)


# TODO: not working: 'Parameter 2 out of range'
def getAnalogOutputVoltage(select: str, instrumentHandle: ViSession) -> tuple[float, str]:
    """
    Get a specified analog output voltage.
    :param select:  specifies the voltage used as:
                        - fixed (0) fixed output voltage
                        - min   (1) minimal analog output voltage
                        - max   (2) maximal analog output voltage
                        - imin  (3) minimal voltage which is given when the monitored pixel has an
                                    intensity below (or equal) the configured level
                        - imax  (4) maximal voltage which is given when the monitored pixel has an
                                    intensity above (or equal) the configured level
    :param instrumentHandle:    session given by init()
    :return:    tuple with the voltage and the error message
    """
    voltage = ct.pointer(ViReal64(0.0))
    if select == 'fixed':
        select == ViInt16(0)
    elif select == 'min':
        select == ViInt16(1)
    elif select == 'max':
        select = ViInt16(2)
    elif select == 'imin':
        select = ViInt16(3)
    elif select == 'imax':
        select = ViInt16(4)
    else:
        return (voltage.contents.value, 'select is not defined. Choose between fixed, min, max,'
                'imin and imax.')
    answer = LC100dll.tllc100_getAnalogOutputVoltage(instrumentHandle, select, voltage)
    return (voltage.contents.value, errorMessage(answer, instrumentHandle))


# TODO: not working: 'Invalid USB control transfer parameter value'
def setAnalogOutputPixel(pixel: int, imax: float, imin: float, instrumentHandle: ViSession) -> str:
    """
    Set the pixel to be monitored and the lower and upper intensity limit.
    :param pixel:   the pixel to be monitored (0 to TLLC100_NUM_PIXELS -1)
    :param imax:    the maximum intensity (all values above will result to output
                    TLLC100_AOUT_VOLT_IMAX at analog output)
    :param imin:    the minimum intensity (all values below will result to output
                    TLLC100_AOUT_VOLT_IMIN at analog output)
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_setAnalogOutputPixel(instrumentHandle, ViInt32(pixel), ViReal64(imax),
                                                   ViReal64(imin))
    return errorMessage(answer, instrumentHandle)


def getAnalogOutputPixel(instrumentHandle: ViSession) -> tuple[int, tuple[float, float], str]:
    """
    Get the pixel which is monitored and the lower and upper intensity limit.
    :param instrumentHandle:    session given by init()
    :return:    tuple with the actual monitored pixel, a tuple with the max/min intensity and the
                error message
    """
    pixel = ct.pointer(ViInt16(0))
    imax = ct.pointer(ViReal64(0.0))
    imin = ct.pointer(ViReal64(0.0))
    answer = LC100dll.tllc100_getAnalogOutputPixel(instrumentHandle, pixel, imax, imin)
    return (pixel.contents.value, (imin.contents.value, imax.contents.value),
            errorMessage(answer, instrumentHandle))


# %%% Timing

def setTriggerDelay(triggerDelays: float, instrumentHandle: ViSession) -> str:
    """
    Set the trigger delay in seconds.
    :param triggerDelays:   specifies the trigger delay in seconds. The allowed range for this
                            parameter can be queried by "tllc100_getTriggerDelay"
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_setTriggerDelay(instrumentHandle, ViReal64(triggerDelays))
    return errorMessage(answer, instrumentHandle)


def getTriggerDelay(select: str, instrumentHandle: ViSession) -> tuple[float, str]:
    """
    Get the trigger delay.
    :param select:  the trigger delay to retrieve
                        - set   (0) the actual set trigger delay
                        - min   (1) the minimum trigger delay
                        - max   (2) the maximum trigger delay
    :param instrumentHandle:    session given by init()
    :return:    tuple with the trigger delay and the error message
    """
    triggerDelays = ct.pointer(ViReal64(0.0))
    if select == 'set':
        select = ViInt16(0)
    elif select == 'min':
        select = ViInt16(1)
    elif select == 'max':
        select = ViInt16(2)
    else:
        return (triggerDelays.contents.value, 'select is not defined.'
                'Choose between set, min and max.')
    answer = LC100dll.tllc100_getTriggerDelay(instrumentHandle, select, triggerDelays)
    return (triggerDelays.contents.value, errorMessage(answer, instrumentHandle))


def setFlashParams(select: str, values: float, instrumentHandle: ViSession) -> str:
    """
    Set the flash duration or delay in seconds.
    :param select:  Either TLLC100_FLASH_DURATION_SET to set the flash duration or
                    TLLC100_FLASH_DELAY_SET to set the flash delay.
                        - delay     (3) the actual flash delay
                        - duration  (6) the actual flash duration
    :param values:  The flash duration or delay in seconds. The allowed range for these
                    parameters can be queried by "tllc100_getFlashParams".
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    if select == 'delay':
        select = ViInt16(3)
    elif select == 'duration':
        select = ViInt16(6)
    else:
        return ('select is not defined. Choose between delay and duration.')
    answer = LC100dll.tllc100_setFlashParams(instrumentHandle, select, ViReal64(values))
    return errorMessage(answer, instrumentHandle)


def getFlashParams(select: str, instrumentHandle: ViSession) -> tuple[float, str]:
    """
    Get the flash parameters.
    :param select:  Either TLLC100_FLASH_DURATION_SET to set the flash duration or
                    TLLC100_FLASH_DELAY_SET to set the flash delay.
                        - delay_set     (3) the actual flash delay
                        - delay_min     (4) the minimum flash delay (read-only)
                        - delay_max     (5) the maximum flash delay (read-only)
                        - duration_set  (6) the actual flash duration
                        - duration_min  (7) the minimum flash duration (read-only)
                        - duration_max  (8) the maximum flash duration (read-only)
    :param instrumentHandle:    session given by init()
    :return:    tuple with the values for the flash parameter and the error message
    """
    values = ct.pointer(ViReal64(0.0))
    if select == 'delay_set':
        select = ViInt16(3)
    elif select == 'delay_min':
        select = ViInt16(4)
    elif select == 'delay_max':
        select = ViInt16(5)
    elif select == 'duration_set':
        select = ViInt16(6)
    elif select == 'duration_min':
        select = ViInt16(7)
    elif select == 'duration_max':
        select = ViInt16(8)
    else:
        return (values.contents.value, 'select is not defined. Choose between delay_set,'
                'delay_min, delay_max, duration_set, duration_min and duration_max.')
    answer = LC100dll.tllc100_getFlashParams(instrumentHandle, select, values)
    return (values.contents.value, errorMessage(answer, instrumentHandle))


# %% Action / Status functions

def startScan(instrumentHandle: ViSession) -> str:
    """
    Trigger the LC100 to take a single shot.
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_startScan(instrumentHandle)
    return errorMessage(answer, instrumentHandle)


def startScanCont(instrumentHandle: ViSession) -> str:
    """
    Start the free running mode (continuous scanning).
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_startScanCont(instrumentHandle)
    return errorMessage(answer, instrumentHandle)


def startScanExtTrg(instrumentHandle: ViSession) -> str:
    """
    Arms the external trigger of the camera. A following low to high transition at the trigger
    input of the camera then starts a scan.
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_startScanExtTrg(instrumentHandle)
    return errorMessage(answer, instrumentHandle)


def startScanContExtTrg(instrumentHandle: ViSession) -> str:
    """
    Arms the camera for scanning after an external trigger. A following low to high transition
    at the trigger input starts a scan. The camera will rearm immediately after the scan data is
    readout.
    :param instrumentHandle:    session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_startScanContExtTrg(instrumentHandle)
    return errorMessage(answer, instrumentHandle)


def getDeviceStatus(instrumentHandle: ViSession) -> tuple[str, str]:
    """
    Queries the status register of the LC100. You can use this function to detect if an external
    trigger has already occurred.
    :param instrumentHandle:    session given by init()
    :return:    tuple with the status and the error message
    """
    deviceStatus = ct.pointer(ViInt32(-1))
    answer = LC100dll.tllc100_getDeviceStatus(instrumentHandle, deviceStatus)
    if deviceStatus.contents.value == 0:
        status = 'camera is idle'
    elif deviceStatus.contents.value == 1:
        status = 'scan is done, waiting for data transfer to PC'
    elif deviceStatus.contents.value == 2:
        status = 'scan is in progress'
    elif deviceStatus.contents.value == 3:
        status = 'external trigger is armed and camera waits for trigger'
    else:
        status = 'unknown status'
    return (status, errorMessage(answer, instrumentHandle))


# %% Data Functions

def setup(instrumentHandle: ViSession, setupCommand: str, IOSetup: bool = False,
          evalBox: bool = False, trigger: bool = False, daCount: bool = False,
          CCD: bool = True) -> str:
    """
    Settings for the LC100 camera.
    :param instrumentHandle:    session given by init()
    :param setupCommand:    This parameter accepts one of the following values
                                - ram       (0) set the LC100 to its factory settings, current
                                                session only, do not alter non volatile memory,
                                                next power up will run with old settings
                                - storeRam  (1) store the current session setting to NVMEM, next
                                                power up will run with these settings
                                - resetNV   (2) set the LC100 to its factory settings in RAM as
                                                well as in non volatile memory, on next power up
                                                LC100 will run with factory default settings
                                - reload    (3) set the LC100 to the settings of the last power up,
                                                i.e. the NVMEM settings, like a reboot
    :param IOSetup: affects settings set with setGPIOFeed() and setGPIOMode()
    :param evalbox: affects settings set with setEvalBox()
    :param trigger: affects settings set with setTriggerDelay() and setFlashParams()
    :param daCount: affects settings set with setAnalogOutputMode(), setAnalogOutputVoltage() and
                    setAnalogOutputPixel()
    :param CCD: affects settings set with setIntegrationTime() and setOperatingMode()
    :return: error message
    """
    if setupCommand == 'ram':
        setupCommand = ViUInt16(0)
    elif setupCommand == 'storeRam':
        setupCommand = ViUInt16(1)
    elif setupCommand == 'resetNV':
        setupCommand = ViUInt16(2)
    elif setupCommand == 'reload':
        setupCommand = ViUInt16(3)
    else:
        return 'setup is not defined. Choose between ram, storeRam, resetNV, reload.'
    filter = "0b"
    filter += str(int(IOSetup))
    filter += str(int(evalBox))
    filter += str(int(trigger))
    filter += str(int(daCount))
    filter += str(int(CCD))
    filter = ViUInt16(int(filter, base=2))
    answer = LC100dll.tllc100_Setup(instrumentHandle, setupCommand, filter)
    return errorMessage(answer, instrumentHandle)


def getScanData(instrumentHandle: ViSession) -> tuple[list[int], str]:
    """
    Read out the processed scan data.
    :param instrumentHandle:    session given by init()
    :return:    tuple with the scan data and the error message
    """
    data = (ViUInt16*2048)()
    answer = LC100dll.tllc100_getScanData(instrumentHandle, data)
    return (data[:], errorMessage(answer, instrumentHandle))


# %%% Wavelength Correlation
# TODO: Implement setWavelengthData
def setWavelengthData(instrumentHandle: ViSession, dataSet: str, pixelDataArray: list[int],
                      wavelengthDataArray: list[float], bufferLength: int) -> str:
    """
    Stores data for user-defined pixel-wavelength correlation to the instruments nonvolatile memory.
    The given data pair arrays are used to interpolate the pixel-wavelength correlation array
    returned by the tllc100_getWavelengthData function.
    :param instrumentHandle:    session given by init()
    :param dataSet: Specifies which pixel-wavelength points are set by this function.
                        - factory   (0) set factory pixel-wavelength points, will be used by the
                                        driver but not sent to the non volatile memory (NVMEM) of
                                        the LC100
                        - user      (1) set user pixel-wavelength points, will be used by the
                                        driver but not sent to the non volatile memory (NVMEM) of
                                        the LC100
                        - factNVMEM (2) set factory pixel-wavelength points, will be used by the
                                        driver and additionally sent to nonvolatile memory of the
                                        LC100
                        - userNVMEM (3) set user pixel-wavelength points, will be used by the
                                        driver an additionally sent to nonvolatile memory of the
                                        LC100
    :param pixelDataArray:  Accepts the pixel values of the pixel/wavelength data pairs. The valid
                            range for pixel values is from 0 up to TLLC100_NUM_PIXEL (2048) - 1.
    :param wavelengthDataArray: Accepts the wavelength values of the pixel/wavelength data pairs in
                                nm. The wavelength value has to be positive.
    :param bufferLength:    Accepts the number of elements in the Pixel Data Array and the
                            Wavelength Data Array.
    :return:    error message
    """
    pass


def getWavelengthData(instrumentHandle: ViSession, dataSet: str) -> tuple[list[float],
                                                                          tuple[float, float], str]:
    """
    Returns data for the pixel-wavelength correlation. The maximum and the minimum wavelength are
    additionally provided in two separate return values.
    :param instrumentHandle:    session given by init()
    :param dataSet: Specifies which pixel-wavelength points are set by this function.
                        - factory   (0) set factory pixel-wavelength points, will be used by the
                                        driver but not sent to the non volatile memory (NVMEM) of
                                        the LC100
                        - user      (1) set user pixel-wavelength points, will be used by the
                                        driver but not sent to the non volatile memory (NVMEM) of
                                        the LC100
                        - factNVMEM (2) set factory pixel-wavelength points, will be used by the
                                        driver and additionally sent to nonvolatile memory of the
                                        LC100
                        - userNVMEM (3) set user pixel-wavelength points, will be used by the
                                        driver an additionally sent to nonvolatile memory of the
                                        LC100
    :return:    tuple with the wavelengthDataArray, a tuple with the min/max wavelength and the
                error message
    """
    WavelengthDataArray = (ViReal64*2048)()
    minimumWavelength = ct.pointer(ViReal64(0.0))
    maximumWavelength = ct.pointer(ViReal64(0.0))
    if dataSet == 'factory':
        dataSet = ViInt16(0)
    elif dataSet == 'user':
        dataSet = ViInt16(1)
    elif dataSet == 'factNVMEM':
        dataSet = ViInt16(2)
    elif dataSet == 'userNVMEM':
        dataSet = ViInt16(3)
    else:
        return (WavelengthDataArray[:],
                (minimumWavelength.contents.value, maximumWavelength.contents.value),
                'dataSet is not specified. Choose between factory, user, factNVMEM and userNVMEM.')
    answer = LC100dll.tllc100_getWavelengthData(instrumentHandle, dataSet, WavelengthDataArray,
                                                minimumWavelength, maximumWavelength)
    return (WavelengthDataArray[:],
            (minimumWavelength.contents.value, maximumWavelength.contents.value),
            errorMessage(answer, instrumentHandle))


# TODO: Check and rework implementation of getCalibrationPoints
def getCalibrationPoints(instrumentHandle: ViSession,
                         dataSet: str) -> tuple[list[int], list[float], int, str]:
    """
    Returns the user-defined pixel-wavelength correlation supporting points. These given data pair
    arrays are used by the driver to interpolate the pixel-wavelength correlation array returned by
    the tllc100_getWavelengthData function.
    :param instrumentHandle:    session given by init()
    :param dataSet: Specifies which pixel-wavelength points are provided by this function.
                        - factory   (0) get factory pixel-wavelength points as read out from the
                                        device at power up
                        - user      (1) get user pixel-wavelength points  as read out from the
                                        device at power up
                        - factNVMEM (2) get factory pixel-wavelength points from nonvolatile memory
                                        of the LC100
                        - userNVMEM (3) get user pixel-wavelength points from nonvolatile memory of
                                        the LC100
    :return:    tuple with the pixel data array, the wavelength data array and the buffer length
    """
    pixelDataArray = (ViInt32*2048)()
    wavelengthDataArray = (ViReal64*2048)()
    bufferLength = ct.pointer(ViInt32)
    if dataSet == 'factory':
        dataSet = ViInt16(0)
    elif dataSet == 'user':
        dataSet = ViInt16(1)
    elif dataSet == 'factNVMEM':
        dataSet = ViInt16(2)
    elif dataSet == 'userNVMEM':
        dataSet = ViInt16(3)
    else:
        return (pixelDataArray[:], wavelengthDataArray[:], bufferLength,
                'dataSet is not specified. Choose between factory, user, factNVMEM and userNVMEM.')
    answer = LC100dll.tllc100_getCalibrationPoints(instrumentHandle, dataSet, pixelDataArray,
                                                   wavelengthDataArray, bufferLength)
    return (pixelDataArray[:], wavelengthDataArray[:], bufferLength,
            errorMessage(answer, instrumentHandle))


# %%% Amplitude Correction

def setAmplitudeData(instrumentHandle: ViSession, amplitudeCorrectionFactors: list[float],
                     bufferStart: int, bufferLength: int, mode: str) -> str:
    """
    Stores data for user-defined pixel-wavelength correlation to the instruments nonvolatile memory.
    :param instrumentHandle:    session given by init()
    :param amplitudeCorrectionFactors:  Accepts the amplitude correction factor array.
    :param bufferStart: The start index for the amplitude correction factor array. From this point
                        on the data given in the parameter Amplitude_Correction_Factors[] will
                        replace the data stored in the user amplitude correction factor array.
    :param bufferLength:    Indicates the length of the amplitude correction factor array. Buffer
                            Length values from given parameter Amplitude_Correction_Factors[] will
                            replace the data stored in the user amplitude correction factor array.
    :param mode:    Accepts one of the three values.
                        - meas      (1) the values given will be applied to the current
                                        measurements only, the data will not be copied to the non
                                        volatile memory
                        - measNVMEM (2) the values given will be applied to the current
                                        measurements and will be copied to the non volatile memory
                                        too, i.e. the data will be used on next power up
                        - NVMEMonly (3) the values given will be copied to the non volatile memory
                                        only but not used for the current measurements, but the
                                        data will be used on next power up
    :return: error message
    """
    pass


def getAmplitudeData(instrumentHandle: ViSession, bufferStart: int, bufferLength: int,
                     mode: str) -> tuple[list[float], str]:
    """
    Returns the user-defined pixel-wavelength correlation supporting points. These given data pair
    arrays are used by the driver to interpolate the pixel-wavelength correlation array returned by
    the tllc100_getWavelengthData function.
    :param instrumentHandle:    session given by init()
    :param bufferStart: This parameter is the start index for the amplitude correction factor array.
                        From this point on the data given in the parameter
                        Amplitude_Correction_Factors[] will replace the data stored in the user
                        amplitude correction factor array.
    :param bufferLength:    This parameter indicates the length of the amplitude correction factor
                            array. Buffer Length values from given parameter
                            Amplitude_Correction_Factors[] will replace the data stored in the user
                            amplitude correction factor array.
    :param mode:    Accepts one of the two values.
                        - current   (1) the values read out from LC100 are the currently used
                                        amplitude correction factors used by the LC100
                        - NVMEM     (2) the values read out from LC100 are the amplitude correction
                                        factors as stored in the non volatile memory of the LC100.
                                        These data is used on power up of the LC100.
    :return: tuple with the amplitude correction array and the error message
    """
    amplitudeCorrectionFactors = (ViReal64*2048)()
    if mode == 'current':
        mode = ViInt32(1)
    elif mode == 'NVMEM':
        mode = ViInt32(2)
    else:
        return (amplitudeCorrectionFactors[:],
                'mode is not defined. Choose between current and NVMEM.')
    answer = LC100dll.tllc100_getAmplitudeData(instrumentHandle, amplitudeCorrectionFactors,
                                               ViInt32(bufferStart), ViInt32(bufferLength), mode)
    return (amplitudeCorrectionFactors[:], errorMessage(answer, instrumentHandle))


# %% Utility Functions

def identificationQuery(instrumentHandle: ViSession) -> tuple[dict[str, str, str, str], str]:
    """
    Returns the device identification information.
    :param instrumentHandle:    session given by init()
    :return:    tuple with dictionary with the device information and the error message
    """
    manufacturerName = ct.create_string_buffer('\000'.encode('windows-1251'), 256)
    deviceName = ct.create_string_buffer('\000'.encode('windows-1251'), 256)
    serialNumber = ct.create_string_buffer('\000'.encode('windows-1251'), 256)
    firmwareRevision = ct.create_string_buffer('\000'.encode('windows-1251'), 256)
    answer = LC100dll.tllc100_identificationQuery(instrumentHandle, manufacturerName, deviceName,
                                                  serialNumber, firmwareRevision)
    return ({'manufacturerName': manufacturerName.value.decode('windows-1251'),
             'deviceName': deviceName.value.decode('windows-1251'),
             'serialNumber': serialNumber.value.decode('windows-1251'),
             'firmwareRevision': firmwareRevision.value.decode('windows-1251')},
            errorMessage(answer, instrumentHandle))


def sensorTypeQuery(instrumentHandle: ViSession) -> tuple[dict[int, float, float, str], str]:
    """
    Queries sensor specific data.
    :param instrumentHandle:    session given by init()
    :return:    tuple with dictionary with the sensor information and the error message
    """
    numberOfPixels = ct.pointer(ViInt16(0))
    minimumIntegrationTime = ct.pointer(ViReal64(0.0))
    maximumIntegrationTime = ct.pointer(ViReal64(0.0))
    description = ct.create_string_buffer('\000'.encode('windows-1251'), 512)
    answer = LC100dll.tllc100_sensorTypeQuery(instrumentHandle, numberOfPixels,
                                              minimumIntegrationTime, maximumIntegrationTime,
                                              description)
    return ({'numberOfPixels': numberOfPixels.contents.value,
             'minimumIntegrationTime': minimumIntegrationTime.contents.value,
             'maximumIntegrationTime': maximumIntegrationTime.contents.value,
             'description': description.value.decode('windows-1251')},
            errorMessage(answer, instrumentHandle))


def revisionQuery(instrumentHandle: ViSession):
    """
    Returns the revision numbers of the instrument driver and the device firmware.
    :param instrumentHandle:    session given by init()#
    :return:    tuple with the instrument driver revision, the firmware revision and the error
                message
    """
    instrumentDriverRevision = ct.create_string_buffer('\000'.encode('windows-1251'), 256)
    firmwareRevision = ct.create_string_buffer('\000'.encode('windows-1251'), 256)
    answer = LC100dll.tllc100_revisionQuery(instrumentHandle, instrumentDriverRevision,
                                            firmwareRevision)
    return (instrumentDriverRevision.value.decode('windows-1251'),
            firmwareRevision.value.decode('windows-1251'),
            errorMessage(answer, instrumentHandle))


def reset(instrumentHandle: ViSession):
    """
    Resets the device.
    :param: instrumentHandle:    session given by init()
    :return: error message
    """
    answer = LC100dll.tllc100_reset(instrumentHandle)
    return errorMessage(answer, instrumentHandle)


# attributeError: function not found
def self_test(instrumentHandle: ViSession):
    """
    Runs the device self test routine and returns the test result.
    :param instrumentHandle:    session given by init()
    :return:    tuple with test result number (0 if sucessfull), the self test message and the
                error message
    """
    selfTestResult = ct.pointer(ViInt16(0))
    selfTestMessage = ct.create_string_buffer('\000'.encode('windows-1251'), 256)
    answer = LC100dll.tllc100_selfTest(instrumentHandle, selfTestResult, selfTestMessage)
    return (selfTestResult.contents.value, selfTestMessage.value.decode('windows-1251'),
            errorMessage(answer, instrumentHandle))
'''

def errorMessage(statusCode: ViStatus, instrumentHandle: ViSession) -> str:
    """
    Interpret the error code returned by the instrument driver functions interprets it and returns
    it as an user readable string.
    :param: statusCode:  the error code returned from the instrument driver functions
    :param: instrumentHandle:    session with the LC100 camera returned by the init() function
    :return:    description of the error
    """
    description = ct.create_string_buffer('\000'.encode('windows-1251'), 512)
    message = LC1dll.tllc100_error_message(instrumentHandle, statusCode, description)
    if message == 0:
        return description.value.decode('windows-1251')
    else:
        return 'Error while transforming the error code: ' + errorMessage(message)

'''
def setUserText(instrumentHandle: ViSession, userText: str):
    """
    Transfers a user definable text and saves it in the nonvolatile memory of the LC100.
    :param instrumentHandle:    session with the LC100 camera returned by the init() function
    :param userText:    accepts the Instrument Handle returned by the Initialize function to select
                        the desired instrument driver session.
    :return:    error message
    """
    text = ct.create_string_buffer(userText.encode('windows-1251'))
    answer = LC100dll.tllc100_setUserText(instrumentHandle, text)
    return errorMessage(answer, instrumentHandle)


def getUserText(instrumentHandle: ViSession):
    """
    Receives a user defined text stored in the nonvolatile memory of the device.
    :param instrumentHandle:    session with the LC100 camera returned by the init() function
    :return:    tuple with user text and error message
    """
    text = ct.create_string_buffer('\000'.encode('windows-1251'), 512)
    answer = LC100dll.tllc100_getUserText(instrumentHandle, text)
    return (text.value.decode('windows-1251'), errorMessage(answer, instrumentHandle))


def setAttribute(instrumentHandle: ViSession, attribute: ViAttr, value):
    pass


def getAttribute():
    pass


# %% Close

def close(instrumentHandle: ViSession) -> str:
    """
    Closes the instrument driver session.
    :param: instrumentHandle:   session given by init()
    :return:    error message
    """
    answer = LC100dll.tllc100_close(instrumentHandle)
    return errorMessage(answer, instrumentHandle)
'''
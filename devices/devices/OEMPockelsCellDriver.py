# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 11:33:40 2023

@author: ernst
"""
from pymeasure.instruments import Instrument

from pyvisa.constants import Parity, StopBits

import pyvisa as pv

# from pyvisa import VisaIOError  # republish


table = [0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15,
         0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d,
         0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65,
         0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d,
         0xe0, 0xe7, 0xee, 0xe9, 0xfc, 0xfb, 0xf2, 0xf5,
         0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd,
         0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85,
         0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd,
         0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
         0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea,
         0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2,
         0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a,
         0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32,
         0x1f, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0d, 0x0a,
         0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42,
         0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a,
         0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c,
         0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
         0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec,
         0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4,
         0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c,
         0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44,
         0x19, 0x1e, 0x17, 0x10, 0x05, 0x02, 0x0b, 0x0c,
         0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34,
         0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b,
         0x76, 0x71, 0x78, 0x7f, 0x6a, 0x6d, 0x64, 0x63,
         0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
         0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13,
         0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb,
         0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83,
         0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb,
         0xe6, 0xe1, 0xe8, 0xef, 0xfa, 0xfd, 0xf4, 0xf3]


def checksum(data):
    """Calculate the serial communication checksum (CRC-8)."""
    init = 0x00
    for element in data:
        position = init ^ element
        init = table[position]
    return init


class Driver(Instrument):
    """
    Initialize the transmitter.

    Parameters
    ----------
    COM : int
        The number of the COM port.
    baud_rate
        The communication baud rate, one of 9600, 14400, 19200, 28800,
        38400, 57600, 115200.
    address
        The device address in the range 1-16.
    """

    def __init__(self, adapter, name="OEM", baud_rate=57600, address=1, ID=1):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=False,
            write_termination="\r",
            read_termination="\r",
            baud_rate=baud_rate,
            timeout=250,
            parity=Parity.none,
            stop_bits=StopBits.one,
        )
        self.address = address  # 1-16
        self.ID = ID

    def write(self, message, data="", length=0):
        # Build command
        command = []
        if data == "":
            command.append(0b10100001)  # Synchronization/flags
        else:
            command.append(0b10100101)  # Synchronization/flags
        if data == "":
            command.append(0)  # length of data (here zero since data is requested, not sent.)
        else:
            command.append(length)
        command.append(self.ID)
        command.append(message)
        if data != "":
            if length > 1:
                for i in range(length):
                    command.append(data[i])
            else:
                command.append(data)
        command.append(checksum(command))
        # Send command
        super().write_bytes(bytes(command))

    def read(self):
        """Read the data and return the requested data, error and checksum calculation result."""
        try:
            got = self.adapter.read_bytes(1)
            message = got
        except pv.VisaIOError as exc:
            pass
            if exc.abbreviation == "VI_ERROR_TMO":
                raise ConnectionResetError("Timeout.")
        got = self.adapter.read_bytes(1)
        message += got
        length = int.from_bytes(got)
        got = self.adapter.read_bytes(1)
        message += got
        if got == b'\x00':
            error = ""
        elif got == b"\x01":
            error = "Parameter/Funktion not available."
        elif got == b"\x02":
            error = "Parameter is read only."
        elif got == b"\x03":
            error = "Request packet has an invalid amount of data."
        elif got == b"\x04":
            error = "Value transmitted in the request packet is out of range."
        elif got == b"\x05":
            error = "Current request cannot be processed at the moment."
        else:
            error = "Device specific error, that can not be specified further."
        if length > 0:
            got = self.read_bytes(length)
            message += got
            data = int.from_bytes(got, "little")
        else:
            data = ""
            print(message)
        got = self.adapter.read_bytes(1)
        check_meas = int.from_bytes(got)
        check_cal = checksum(message)
        if check_meas == check_cal:
            check = True
        else:
            check = False
        return {"data": data, "error": error, "check": check}

    def read_raw(self):
        """Read the data and return the requested data as bytes."""
        try:
            got = self.adapter.read_bytes(1)
            message = got
        except pv.VisaIOError as exc:
            pass
            if exc.abbreviation == "VI_ERROR_TMO":
                raise ConnectionResetError("Timeout.")
        got = self.adapter.read_bytes(1)
        message += got
        length = int.from_bytes(got)
        got = self.adapter.read_bytes(1)
        message += got
        if got == "\x00":
            error = ""
        elif got == "\x01":
            error = "Parameter/Funktion not available."
        elif got == "\x02":
            error = "Parameter is read only."
        elif got == "\x03":
            error = "Request packet has an invalid amount of data."
        elif got == "\x04":
            error = "Value transmitted in the request packet is out of range."
        elif got == "\x05":
            error = "Current request cannot be processed at the moment."
        else:
            error = "Device specific error, that can not be specified further."  # noqa: F841
            # TODO was soll diese Fehlermeldung? Sie wird nicht verwendet.
        if length > 0:
            got = self.read_bytes(length)
            message += got
            data = got
        got = self.adapter.read_bytes(1)
        try:
            check_meas = int.from_bytes(got)
            check_cal = checksum(message)
            if check_meas == check_cal:
                check = True
            else:
                check = False
            if not check:
                print("Checksum not correct!")
        except IndexError:
            pass
        return data

    # Common Parameters / functions

    def ping(self):
        """Get response from the device to see, if it works properly."""
        self.write(0x00)
        response = self.read()
        if response["error"] != "" and not response["check"]:
            return (False, response["error"])
        elif response["error"] != "" and response["check"]:
            return (True, response["error"])
        elif response["error"] == "" and response["check"]:
            return True
        else:
            return False

    def deviceID(self, ID):
        """Set the device ID between 1 and 254."""
        self.write(0x01, data=ID, length=1)

    def protocolVersion(self):
        """Get the actual protocol version (value between 1 and 254)."""
        self.write(0x02)
        response = self.read()
        version = response["data"]
        error = response["error"]
        if error == "":
            return version
        else:
            return error

    def devicePartNumber(self, number=-1):
        if number == -1:
            self.write(0x03)
        else:
            data = [number % 256, number // 256]
            self.write(0x03, data=data, length=2)
        value = self.read()
        return value

    def deviceSerialNumber(self, number=-1):
        if number == -1:
            self.write(0x04)
        else:
            data = [number % 256, number // 256]
            self.write(0x04, data=data, length=2)
        value = self.read()
        return value

    def hardwareVersion(self, number=-1):
        if number == -1:
            self.write(0x05)
        else:
            data = [number % 256, number // 256]
            self.write(0x05, data=data, length=2)
        value = self.read()
        return value

    def softwareVersion(self):
        self.write(0x06)
        version = self.read()
        return version

    def deviceString(self):
        self.write(0x07)
        name = self.read()
        return name

    def bootloaderControlAndStatus(self, activeBootloader=-1, clearMemory=-1, bootApplication=-1):
        if activeBootloader == -1 and clearMemory == -1 and bootApplication == -1:
            self.write(0x08)
        else:
            self.write(0x08, data=int(f"{activeBootloader}{clearMemory}{bootApplication}", 2))
        bits = bin(self.read()["data"])[2:]

        if int(bits[0]) == 1:
            ab = True
        else:
            ab = False
        if int(bits[1]) == 1:
            cm = True
        else:
            cm = False
        if int(bits[2]) == 1:
            ba = True
        else:
            ba = False
        return {"Active Bootloader": ab, "Clear Memory": cm, "Boot Application": ba}

    def bootloaderStream(self, packets):
        pass

    def deviceStatus(self):
        self.write(0x0A)
        bits = bin(self.read()["data"])[2:]
        status = []
        if int(bits[0]) == 1:
            status.append("At least one warning is present.")
        else:
            status.append("No warning is present.")
        if int(bits[1]) == 1:
            status.append("At least one error is present.")
        else:
            status.append("No error is present.")
        if int(bits[3]) == 1:
            status.append("Bootloader is active.")
        else:
            status.append("Bootloader is not active.")
        if int(bits[4]) == 1:
            status.append("Device is ready.")
        else:
            status.append("Device is not ready.")
        if int(bits[7]) == 1:
            status.append("Device is On.")
        else:
            status.append("Device is Off.")
        return status

    def availableBusSpeed(self):
        pass

    def setBusSpeed(self, speed=57600):
        pass

    def busMute(self, communication=1):
        pass

    # Device specific parameters / functions

    def highVoltage(self, voltage=-1):
        """Read or set the high voltage between 0 - 2000 V."""
        if voltage == -1:
            self.write(0x40)
            value = self.read()["data"]
            return value
        else:
            value = [voltage % 256, voltage // 256]
            self.write(0x40, data=value, length=2)

    def gateLimit(self, limit=-1):
        """Read or set the gate limit between 200 - 2200 ns."""
        if limit == -1:
            self.write(0x41)
            value = self.read()["data"]
            return value
        else:
            value = [limit % 256, limit // 256]
            self.write(0x41, data=value, length=2)

    def transistorsTemperatureThreshold(self, threshold=-1):
        """Read or set the transistors temperature threshold between 0 - 160°C."""
        if threshold == -1:
            self.write(0x42)
            value = self.read()["data"] / 10
            return value
        else:
            value = [threshold * 10 % 256, threshold * 10 // 256]
            self.write(0x42, data=value, length=2)

    def caseTemperatureThreshold(self, threshold):
        """Read or set the case temperature threshold between 0 - 160°C."""
        if threshold == -1:
            self.write(0x43)
            value = self.read()["data"] / 10
            return value
        else:
            value = [threshold % 256 * 10, threshold * 10 // 256]
            self.write(0x43, data=value, length=2)

    def HVenable(self, status=-1):
        """Enables (1) or disables (0) HV."""
        if status == -1:
            self.write(0x44)
            value = self.read()["data"]
            return value
        else:
            self.write(0x44, data=status, length=1)

    def pulseWidthMode(self, mode=-1):
        """Get or set the pulse width mode; 1: fixed, 0: variable"""
        if mode == -1:
            self.write(0x45)
            return self.read()["data"]
        else:
            self.write(0x45, data=mode, length=1)

    def triggeringMode(self, mode=-1):
        """Get or set the triggering mode; 1: soft, 0: harsh."""
        if mode == -1:
            self.write(0x46)
            return self.read()["data"]
        else:
            self.write(0x46, data=mode, length=1)

    def HVprogramMode(self, mode=-1):
        """Get or set the HV program mode; 1: smart, 0: default"""
        if mode == -1:
            self.write(0x47)
            return self.read()["data"]
        else:
            self.write(0x47, data=mode, length=1)

    def getSensors(self):
        """Get errors from sensors."""
        self.write(0x60)
        bits = bin(self.read()["data"])[2:]
        bitList = []
        for i in range(8 - len(bits)):
            bitList.append(0)
        for i in range(len(bits)):
            bitList.append(bits[i])
        status = []
        if bitList[0] == 1:
            status.append("Overtemperature error")
        elif bitList[1] == 1:
            status.append("Gate limit error")
        elif bitList[6] == 1:
            status.append("HV fault error")
        return status

    def getTransistorsTemperature(self):
        """Get Transistor temperature in °C."""
        self.write(0x61)
        T = self.read()["data"] / 10
        return T

    def getCaseTemperature(self):
        """Get case temperature in °C."""
        self.write(0x62)
        T = self.read()["data"] / 10
        return T

    def getAllMonitors(self):
        """Returns all monitor values in the following order:
        sensors (1 byte), transistor temperature (2 bytes), case temperature (2 bytes)"""
        self.write(0xF2)
        data = self.read_raw()
        bits = bin(data[0])[2:]
        bitList = []
        for i in range(8 - len(bits)):
            bitList.append(0)
        for i in range(len(bits)):
            bitList.append(int(bits[i]))
        oe = bitList[0]
        gle = bitList[1]
        HVfe = bitList[6]
        transistorTemperature = int.from_bytes(data[1:3], "little") / 10
        caseTemperature = int.from_bytes(data[3:5], "little") / 10
        return {
            "overtemperatureError": oe,
            "gateLimitError": gle,
            "HVfaultError": HVfe,
            "transistorTemperature": transistorTemperature,
            "caseTemperature": caseTemperature}

    def getAllParameters(self):
        self.write(0xF1)
        data = self.read_raw()
        voltage = int.from_bytes(data[0:2], "little")
        gateLimit = int.from_bytes(data[2:4], "little")
        transistorTemperatureThreshold = int.from_bytes(data[4:6], "little") / 10
        caseTemperatureThreshold = int.from_bytes(data[6:8], "little") / 10
        bits = bin(data[8])[2:]
        bitList = []
        for i in range(8 - len(bits)):
            bitList.append(0)
        for i in range(len(bits)):
            bitList.append(int(bits[i]))
        oe = bitList[0]
        gle = bitList[1]
        hve = bitList[5]
        pwm = bitList[3]
        tm = bitList[2]
        hvf = bitList[6]
        transistorTemperature = int.from_bytes(data[9:11], "little") / 10
        caseTemperature = int.from_bytes(data[11:13], "little") / 10
        return {
            "voltage": voltage,
            "gateLimit": gateLimit,
            "transistorTemperatureThreshold": transistorTemperatureThreshold,
            "caseTemperatureThreshold": caseTemperatureThreshold,
            "overtemperatureError": oe,
            "gateLimitError": gle,
            "highVoltageEnableState": hve,
            "pulseWidthModeState": pwm,
            "triggeringModeState": tm,
            "HVfaultError": hvf,
            "transistorTemperature": transistorTemperature,
            "caseTemperature": caseTemperature,
        }

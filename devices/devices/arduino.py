# -*- coding: utf-8 -*-
"""
Communication with Arduino.

ReadSensors.ino is the corresponding script.

Created on Wed May  4 17:11:43 2022 by Benedikt Burger
"""

import time

from pymeasure.instruments import Instrument
from pymeasure.adapters import VISAAdapter


class Arduino(Instrument):
    """
    Communication with an Arduino.

    .. note::

        Connecting to an Arduino resets it. Afterwards it takes a few seconds
        to restart, before it is ready to communicate. Therefore, the init
        waits some time.

    :param adapter: A pyVISA resource_string
    :param str visa: Visa library to use. @py for pyvisa-py.
    """

    def __init__(self, adapter, timeout=1000, startup_sleep=2, **kwargs):
        """Initialize the communication."""
        super().__init__(
            adapter,
            "Arduino Communicator",
            includeSCPI=False,
            write_termination="\n",
            read_termination="\r\n",
            timeout=timeout,
            **kwargs,
        )
        self.resource = adapter
        self.kwargs = kwargs
        """
        Arduino uses the same defaults as visa:
            baudrate 9600
            8 data bits
            no parity
            one stop bit
        """
        time.sleep(startup_sleep)

    def open(self):
        """Open a connection to the Arduino."""
        self.adapter = VISAAdapter(
            self.resource,
            write_termination="\n",
            read_termination="\r\n",
            timeout=1000,
            **self.kwargs,
        )
        time.sleep(2)

    def close(self):
        """Close the connection."""
        self.adapter.close()

    def ping(self):
        """Ping the Arduino."""
        return self.ask("p")

    data = Instrument.measurement(
        "l",
        "List of the latest available data as relative voltage.",
        separator="\t",
        get_process=lambda v: [f / 1024 for f in v],
    )

    data_averaged = Instrument.measurement(
        "r",
        "List of the data as rolling average, as relative voltage.",
        separator="\t",
        get_process=lambda v: [f / 1024 for f in v],
    )

    data_raw = Instrument.measurement(
        "q", "List of data with raw values from Arduino.", separator="\t"
    )

    version = Instrument.measurement("v", "Get current firmware version.")

    precision = Instrument.setting(
        "f%i", "Set the precision of the data to be returned."
    )

    averaging = Instrument.setting("m%i", "Set cycles to average over.")


class ControllerArduino(Arduino):
    """An Arduino with a temperature controller.

    According to TemperatureController.ino 1.3.0
    """

    def check_set_errors(self):
        got = self.read()
        if got.startswith("ACK"):
            return []
        else:
            raise ConnectionError(f"Received non Acknowledgement: '{got}'.")

    factor_p = Instrument.setting(
        "kp%f",
        """Set the p factor of the PID controller.""",
        check_set_errors=True,
    )

    factor_i = Instrument.setting(
        "ki%f",
        """Set the p factor of the PID controller.""",
        check_set_errors=True,
    )

    # factor_d = Instrument.setting(
    #      "kd%f",
    #      """Set the p factor of the PID controller.""",
    #      check_set_errors=True,
    # )

    setpoint = Instrument.setting(
        "ks%f",
        """Set the setpoint.""",
        check_set_errors=True,
    )

    integral_value = Instrument.setting(
        "kx%f",
        """Set the current integral value of the PID controller.""",
        check_set_errors=True,
    )

    control_enabled = Instrument.setting(
        "o%i",
        """Set whether the control is enabled or not.""",
        values=[False, True],
        map_values=True,
        check_set_errors=True,
    )

    def get_pid_values(self):
        """Get the PID values.

        :return list: integral_value, Kp, Ki, setpoint, duty_cycle
        """
        values = self.values("d", separator=",", cast=str)  # type: ignore
        for i, value in enumerate(values):
            values[i] = float(value.split(":")[-1])
        if len(values) < 5:
            values.append(None)
        return values

    def write_to_EEPROM(self):
        """Write current values to EEPROM."""
        self.write("e")
        self.check_set_errors()

    def get_compilation_parameters(self):
        """Get the compilation parameters (for version>=1.3.0)"""
        values = self.values("x", separator=",", cast=str)  # type: ignore
        d = {}
        for value in values:
            k, v = value.split(":")
            d[k] = v
        return d

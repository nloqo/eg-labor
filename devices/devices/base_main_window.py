from warnings import warn

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    LECOBaseMainWindowNoDesigner,  # noqa
    FullCommunicator,  # noqa
)


warn(
    "Deprecated to use `LECOBaseMainWindowDesigner`, import it from pyleco_extras instead.",
    FutureWarning,
)

LECOBaseMainWindow = LECOBaseMainWindowDesigner

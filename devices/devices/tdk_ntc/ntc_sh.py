##############################################################################
# Filename    : NTC_SH.py
# Description : Class definition to convert NTC R/T values based on the
#               Steinhart-Hart equation and to do "soft trimming"
#
# AUTHOR: B.Ostrick, TDK electronics AG
#
# SOURCE:
# https://www.tdk-electronics.tdk.com/en/2988200/design-support/design-tools/ntc-thermistors/ntc-class-definition?wt_mc=ref.ScriptNTCPython3
#
# Copyright (c) 2021 TDK electronics AG
#
# License: LICENSE.TXT
#
# Important Notes: README.TXT, IMPORTANT_NOTES.TXT
#
# Modification:     2021/03/15: Initial version
#                   2021/04/01: Classes separated in different files and
#                               optimized a little to work with CircuitPython
#                               No numpy required.
#                   2021/07/19: Version 1.1 bugfix of interval assignment
#                               in NTC_LT. Adapted to Python naming convention
#                               for better reading
##############################################################################
import math


class NTC_SH:
##############################################################################
# NTC class using three datapoints [T[°C],R] to calculate the Steinhart Hart
# coefficients.
# data_tr  - Array of 3 data points [T[°C],R] - T shall be in Celsius
# Calculation of resistance is done by Steinhart Hart formula
# temperature() - Returns a temperature in Celsius as function of resistance
# resistance() - Converts a temperature T in Celsius to a resistance
# calibrate() - Trimms the values to a specific temperature & resistance
# calc_sh() - Calculates the Steinhart-Hart coefficients w/o numpy
##############################################################################
    def calc_sh(self):
        # Calculate order 1 coefficient C1=sh[1]
        # (useing sh[0] and sh[2] as storage variables)
        self.sh[0] = self.ln_r[0] ** 3.0 - self.ln_r[1] ** 3.0
        self.sh[2] = self.ln_r[1] ** 3.0 - self.ln_r[2] ** 3.0
        self.sh[1] = (self.inv_t[0] - self.inv_t[1]) / self.sh[0]
        self.sh[1] = self.sh[1] - (self.inv_t[1] - self.inv_t[2]) / self.sh[2]
        self.sh[1] = self.sh[1] / (
            (self.ln_r[0] - self.ln_r[1]) / self.sh[0]
            - (self.ln_r[1] - self.ln_r[2]) / self.sh[2]
        )
        # Calculate order 3 coefficient C3 = sh[2]
        # (use sh[0] as storage variable)
        self.sh[0] = self.ln_r[0] ** 3.0 - self.ln_r[2] ** 3.0
        self.sh[2] = (self.inv_t[0] - self.inv_t[2]) / self.sh[0] - self.sh[1] * (
            self.ln_r[0] - self.ln_r[2]
        ) / self.sh[0]
        # Calcualte order 0 coefficient C0 = sh[0]
        self.sh[0] = (
            self.inv_t[1]
            - self.sh[1] * self.ln_r[1]
            - self.sh[2] * (self.ln_r[1] ** 3.0)
        )

    def __init__(self, data_tr):
        # self.inv_t stores 1/T of the three input temperatures.
        # This is needed for calibration.
        self.inv_t = [
            1 / (data_tr[0][0] + 273.15),
            1 / (data_tr[1][0] + 273.15),
            1 / (data_tr[2][0] + 273.15),
        ]
        # self.ln_r stores the logarithm of the 3 input resistances.
        # This is needed for calibration.
        self.ln_r = [
            math.log(data_tr[0][1]),
            math.log(data_tr[1][1]),
            math.log(data_tr[2][1]),
        ]
        # self.sh stores the Steinhart Hart coefficients for calculation
        self.sh = [0, 0, 0]
        self.calc_sh()

    def temperature(self, res):
        out = self.sh[0] + self.sh[1] * math.log(res) + self.sh[2] * math.log(res) ** 3
        out = 1 / out - 273.15
        return out

    def resistance(self, T):
        # Inversion of the cubic function in self.temperature() by
        # Cardano's Formula:
        # coefficient of x^3+px+q=0
        p = self.sh[1] / self.sh[2]
        q = (self.sh[0] - 1 / (T + 273.15)) / self.sh[2]
        # cubic discriminant must be > 0
        D = math.sqrt((q / 2) ** 2.0 + (p / 3) ** 3.0)
        out = (-q / 2 + D) ** (1 / 3.0) - (q / 2 + D) ** (1 / 3.0)
        return math.exp(out)

    def calibrate(self, data_tr):
        # point_tr is the [T,R]-value used for calibration
        factor = data_tr[1] / self.resistance(data_tr[0])
        for i in [0, 1, 2]:
            self.ln_r[i] = self.ln_r[i] + math.log(factor)
        self.calc_sh()
#########################################################################
# end of class NTC_SH
#########################################################################

"""
TDK NTC Thermistor classes

You can get Resistance - Temperature Data here:
    https://www.tdk-electronics.tdk.com/web/designtool/ntc/
A manual is called: "ntc-thermistors-an.pdf"

Also read the readme.
"""

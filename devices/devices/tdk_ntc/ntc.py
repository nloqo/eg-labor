##############################################################################
# Filename    : NTC.py
# Description : Two class definitions to convert NTC R/T values based on
#               B-value (ntc_b) or 2-Point calibration (ntc_2p)
#               Both classes offer "soft trimming" using one measurement
# AUTHOR: B.Ostrick, TDK electronics AG
#
# SOURCE:
# https://www.tdk-electronics.tdk.com/en/2988200/design-support/design-tools/ntc-thermistors/ntc-class-definition?wt_mc=ref.ScriptNTCPython3
#
# Copyright (c) 2021 TDK electronics AG
#
# License: LICENSE.TXT
#
# Important Notes: README.TXT, IMPORTANT_NOTES.TXT
#
# Modification:     2021/03/15: Initial version
#                   2021/04/01: Classes separated in different files and
#                               optimized a little to work with CircuitPython
#                               NTC_2P is now a subclass of NTC_B
#                   2021/07/19: Version 1.1 bugfix of interval assignment
#                               in ntc_lt. Adapted to Python naming convention
#                               for better reading.
##############################################################################

import math


class NTC_B:
##############################################################################
# The Basic NTC class using rated temperature, resistance and B-Value
# t_r,t_val - Rated Temperature in Celsius
# r_r, r_val - Rated resistance in Ohm or kOhms
# b,b_bal - B-Value of Thermistor in K
# temperature() - Converts a resistance R to a temperature in Celsius
# resistance() - Converts a temperature T[°C] to a resistance
# calibrate() - Trimms the values to a specific resistance & temperature
##############################################################################
    def __init__(self, t_val, r_val, b_val):
        self.b = b_val  # B-value
        self.r_r = r_val  # rated resistance
        self.t_r = t_val  # rated temperature

    def temperature(self, res):
        out = math.log(res / self.r_r) / self.b + 1 / (273.15 + self.t_r)
        out = 1 / out - 273.15
        return out

    def resistance(self, tem):
        out = self.b * (1 / (tem + 273.15) - 1 / (self.t_r + 273.15))
        return self.r_r * math.exp(out)

    def calibrate(self, point_tr):
        # point_tr is the [T[°C],R]-value used for calibration
        factor = point_tr[1] / self.resistance(point_tr[0])
        self.r_r = self.r_r * factor
##############################################################################
# end of Class ntc_b
##############################################################################


class NTC_2P(NTC_B):
##############################################################################
# NTC class using two datapoints [T[°C],R] to calculate a B-Value
# First datapoint is used to define the Rated Resistance and Temperature
# data_tr  - Array of 2 datapoints [T[°C],R]
# temperature() - Converts a resistance R to a temperature in Celsius
# resistance() - Converts a temperature T[°C] to a resistance
# calibrate() - Trimms the values to a specific resistance & temperature
##############################################################################
    def __init__(self, data_tr):
        self.b = (data_tr[0][0] + 273.15) * (data_tr[1][0] + 273.15)
        self.b = self.b / ((data_tr[0][0] + 273.15) - (data_tr[1][0] + 273.15))
        self.b = self.b * math.log(data_tr[1][1] / data_tr[0][1])
        self.t_r = data_tr[0][0]
        self.r_r = data_tr[0][1]
##############################################################################
# end of class ntc_2p
##############################################################################

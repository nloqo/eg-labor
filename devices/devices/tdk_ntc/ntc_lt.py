##############################################################################
# Filename    : NTC_LT.py
# Description : Class definition to convert NTC R/T values based on a look-up
#               table with multiple [T[°C],R] data points. Interpolation
#               between two datapoints is done by the B-Value formula and not
#               by a linear function.
#
# AUTHOR: B.Ostrick, TDK electronics AG
#
# SOURCE:
# https://www.tdk-electronics.tdk.com/en/2988200/design-support/design-tools/ntc-thermistors/ntc-class-definition?wt_mc=ref.ScriptNTCPython3
#
# Copyright (c) 2021 TDK electronics AG
#
# License: LICENSE.TXT
#
# Important Notes: README.TXT, IMPORTANT_NOTES.TXT
#
# Modification:     2021/03/15: Initial version
#                   2021/04/01: Classes were separated in different files and
#                               optimized to work with CircuitPython.
#                               NTC_LT is not a subclass of NTC_2P anymore.
#                   2021/07/19: Version 1.1 bugfix of interval assignment
#                               in NTC_LT. Adapted to Python naming convention
#                               for better reading.
##############################################################################
import math


class NTC_LT:
##############################################################################
# NTC class using a look-up table of multiple [T[°C],R] points: Extrapolation
# between 2 data points is done with the NTC B-Value formula.
# data_tr  - array of n data points [Temperature [°C],Resistancce]
# temperature() - Converts a resistance R to a temperature in Celsius
# resistance() - Converts a temperature T in Celsius to a resistance
# calibrate() - Trimms the values to a specific temperature & resistance
##############################################################################
    def __init__(self, data_tr):
        self.tr = data_tr
        self.length = len(data_tr)

    def find_ppr(self, res):
        # Finds the index of the first point of the interval that includes the
        # resistance "res". Outside of the lookup table the first or last interval
        # is used respectively.
        p = 1
        if self.tr[1][1] < res:
            p = 0
        elif self.tr[self.length - 1][1] > res:
            p = self.length - 2
        else:
            while (self.tr[p + 1][1] >= res) and (p < self.length - 2):
                p = p + 1
        return p

    def find_ppt(self, tem):
        # finds the index of the first point of the interval that includes the
        # temperature "tem". outside of the lookup table the first or last interval
        # is used respectively.
        p = 1
        if self.tr[1][0] > tem:
            p = 0
        elif self.tr[self.length - 1][0] < tem:
            p = self.length - 2
        else:
            while (self.tr[p + 1][0] <= tem) and (p < self.length - 1):
                p = p + 1
        return p

    def temperature(self, res):
        p = self.find_ppr(res)
        b = (self.tr[p][0] + 273.15) * (self.tr[p + 1][0] + 273.15)
        b = b / (self.tr[p][0] - self.tr[p + 1][0])
        b = b * math.log(self.tr[p + 1][1] / self.tr[p][1])
        out = math.log(res / self.tr[p][1]) / b + 1 / (273.15 + self.tr[p][0])
        out = 1 / out - 273.15
        return out

    def resistance(self, tem):
        p = self.find_ppt(tem)
        b = (self.tr[p][0] + 273.15) * (self.tr[p + 1][0] + 273.15)
        b = b / (self.tr[p][0] - self.tr[p + 1][0])
        b = b * math.log(self.tr[p + 1][1] / self.tr[p][1])
        out = b * (1 / (tem + 273.15) - 1 / (self.tr[p][0] + 273.15))
        return self.tr[p][1] * math.exp(out)

    def calibrate(self, point_tr):
        # point_tr is the [T,R]-value used for calibration
        factor = point_tr[1] / self.resistance(point_tr[0])
        for i in range(0, self.length):
            self.tr[i][1] = self.tr[i][1] * factor
##############################################################################
# end of class NTC_LT
##############################################################################

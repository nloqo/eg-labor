# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 13:28:11 2023

@author: ernst
"""

from pymeasure.instruments import Instrument

from pyvisa.constants import Parity, StopBits


class ITR(Instrument):

    def __init__(self, adapter, name="ITR", baud_rate=9600, address=1):
        super().__init__(
            adapter,
            name=name,
            includeSCPI=False,
            baud_rate=baud_rate,
            timeout=4,
            parity=Parity.none,
            stop_bits=StopBits.one,
        )

    def read_last(self):
        return self.read_bytes(-1)[-9:]
        self.adapter.flush_read_buffer()

    @property
    def status(self) -> dict:
        """Return the sensor status."""
        response = self.read_last()
        status = {}
        status_raw = bin(response[2])[2:]
        while len(status_raw) < 8:
            status_raw = '0' + status_raw
        if status_raw[0] == '0' and status_raw[1] == '0':
            status['emission'] = "off"
        elif status_raw[0] == '1' and status_raw[1] == '0':
            status['emission'] = "25 µA"
        elif status_raw[0] == '0' and status_raw[1] == '1':
            status['emission'] = "5 mA"
        elif status_raw[0] == '1' and status_raw[1] == '1':
            status['emission'] = "degas"
        if status_raw[4] == '0' and status_raw[5] == '0':
            status['unit'] = 'mbar'
        elif status_raw[4] == '1' and status_raw[5] == '0':
            status['unit'] == 'torr'
        elif status_raw[4] == '0' and status_raw[5] == '1':
            status['unit'] == 'Pa'
        if status_raw[6] == '0':
            status['filament1'] = 'active'
            status['filament2'] = 'not active'
        elif status_raw[6] == '1':
            status['filament1'] = 'not active'
            status['filament2'] = 'active'
        return status

    @property
    def pressure(self) -> float:
        """Get pressure in mbar."""
        last_message = self.read_last()
        return 10**((int(last_message[4]) * 256 + int(last_message[5])) / 4000 - 12.5)

    @property
    def error(self) -> list:
        """Return the active errors."""
        response = self.read_last()
        error = []
        error_raw = bin(response[3])[2:]
        while len(error_raw) < 8:
            error_raw = '0' + error_raw
        if error_raw[2] == '1':
            error.append('Pirani error')
        if error_raw[4] == '1':
            error.append('Hot cathode error: Both filaments broken.')
        if error_raw[5] == '1':
            error.append('Hot cathode warning: One filament broken.')
        if error_raw[6] == '1':
            error.append('electronics error / EEPROM error')
        return error

    def select_filament(self, filament: int) -> str:
        '''Select the filament that should be active. You can choose between filament 1 and 2.'''
        status = self.status
        if status['filament1'] == 'active' and filament == 1:
            return 'Filament 1 already active.'
        elif status['filament2'] == 'active' and filament == 2:
            return 'Filament 2 already active.'
        elif filament != 1 or filament != 2:
            return "Filament not available."
        else:
            self.emission(False)
            if filament == 1:
                self.write(bytes([3, 16, 210, 0, 226]))
            elif filament == 2:
                self.write(bytes([3, 16, 210, 1, 227]))
            if not status['emission'] == 'off':
                self.emission(True)
            return "Switching of filament successful."

    def emission(self, status: bool):
        if status:
            self.write(bytes([3, 64, 16, 1, 81]))
        else:
            self.write(bytes([3, 64, 16, 0, 80]))

    def degas(self, status: bool):
        """Degas the filament. !!!Only activate if the pressure is below 7.2e-6 mbar!!!
           The degasing automatically stops after 3 minutes if not interrupted before."""
        if status:
            self.write(bytes([3, 16, 196, 1, 213]))
        else:
            self.write(bytes([3, 16, 196, 0, 212]))


class ITR100(Instrument):

    def __init__(self, adapter, name="ITR100", baud_rate=9600, address=1):
        """Initialize the connection."""
        super().__init__(
            adapter,
            name=name,
            includeSCPI=False,
            baud_rate=baud_rate,
            read_termination="\r",
            write_termination="\r",
            timeout=200,
            parity=Parity.none,
            stop_bits=StopBits.one,
        )

    def check_set_errors(self):
        self.read()

    pressure = Instrument.measurement("MES<CR>",
                                      """Get pressure in mbar.""",
                                      preprocess_reply=lambda v: v.split(":")[1].replace("OFF",
                                                                                         "nan"))

    emission = Instrument.control("EMI<CR>", "EMI W %s<CR>", """Control filament emission.""",
                                  values={True: "ON", False: "OFF"},
                                  map_values=True,
                                  check_set_errors=True,
                                  cast=str,
                                  preprocess_reply=lambda v: v.split()[-1]
                                  )
    TTR = Instrument.control("TTR<CR>", "TTR W %s<CR>", """Control TTR emissionArithmeticError""",
                             values={True: "ON", False: "OFF"},
                             map_values=True,
                             check_set_errors=True,
                             cast=str,
                             preprocess_reply=lambda v: v.split()[-1])

    TTRvoltage = Instrument.measurement("MEST<CR>",
                                        """Read the voltage at the emission start/stop
                                        - port of the ITR100. Can be used to read the pressure of a
                                        connected TTR 211 S.""",
                                        preprocess_reply=lambda v: v.split(":")[1].replace(" V",
                                                                                           ""))

    degas = Instrument.control("DEG<CR>", "DEG W %s<CR>", """Control degas. Degasing should just be
                               started at pressures below 2E-5 mbar""",
                               values={True: "ON", False: "OFF"},
                               map_values=True,
                               check_set_errors=True,
                               cast=str,
                               preprocess_reply=lambda v: v.split()[-1])

    unit = Instrument.control("UNI<CR>", "UNI W %s<CR>", """Control the unit.""",
                              values=("mbar", "Pa", "Torr"),
                              check_set_errors=True,
                              cast=str,
                              preprocess_reply=lambda v: v.split()[-1])
    gasCorrection = Instrument.control("GAS<CR>", "GAS W %s<CR>", """Control the gas correction
                                       factor for: N2, Ar or H2.""",
                                       values=("N2", "Ar", "H2"),
                                       check_set_errors=True,
                                       cast=str,
                                       preprocess_reply=lambda v: v.split()[-1])

    variableGasCorrection = Instrument.control("GFC<CR>", "GCL %s<CR>",
                                               """Control variable gas correction factor between
                                               0.1 and 9.999 in the form n.nnn.""",
                                               check_get_errors=True,
                                               preprocess_reply=lambda v: v.split()[-1])

    status = Instrument.measurement("ERS<CR>", """Read the status of the sensor.""",
                                    preprocess_reply=lambda v: v.split()[-1])

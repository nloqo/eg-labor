
import pytest

from TiSa_nirSpectrometer import NIRSpectrum


@pytest.mark.parametrize("px, result", ((1, 3), (3, 9)))
def test_stuff_to_wavelength(px, result):
    assert NIRSpectrum.px_to_wavelength(px, 2, 3, 4, 6, 7, 9) == result


import logging

import pyqtgraph as pg
from PyQt6 import QtWidgets, QtCore, uic
from devices.basler import ImageEventHandler, ConfigurationHandler
import numpy as np
from PyQt6.QtCore import pyqtSlot
from pypylon import pylon
from scipy.optimize import curve_fit, OptimizeWarning
import datetime
import json

gradient_map = "bipolar"  # "spectrum"

log = logging.getLogger(__name__)


class NIRSpectrum(QtWidgets.QDialog):
    # %%% Widgets in the GUI
    pltSpectrum: pg.PlotWidget
    cbPixelScale: QtWidgets.QCheckBox
    pgBeamProfile: pg.imageview.ImageView
    pbReadout: QtWidgets.QPushButton
    lbBandwidth: QtWidgets.QLabel
    lbCentralWavelength: QtWidgets.QLabel

    lbActualWavelength: QtWidgets.QLabel
    sbTargetWavelength: QtWidgets.QDoubleSpinBox
    pbSetWavelength: QtWidgets.QPushButton
    pbHome: QtWidgets.QPushButton
    pbStop: QtWidgets.QPushButton

    sbExposure: QtWidgets.QSpinBox
    slExposure: QtWidgets.QSlider
    sbGain: QtWidgets.QDoubleSpinBox
    lbMaxPxVal: QtWidgets.QLabel
    lbOverexposed: QtWidgets.QLabel

    sbAverages: QtWidgets.QSpinBox
    pbSaveBeamProfile: QtWidgets.QPushButton
    pbSaveSpectrum: QtWidgets.QPushButton
    leSavedNameBP: QtWidgets.QLineEdit
    leSavedNameSpec: QtWidgets.QLineEdit

    # %%% Initialisierung

    def __init__(self, parent, *args, **kwargs):
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)
        self.main_window = parent

        # Load the user interface file and show it.
        uic.loadUi("data/NIRSpectrum.ui", self)
        self.show()

        # Initialize window
        settings = QtCore.QSettings()

        # Check connections & start readout
        try:
            self.configNIRSpec: dict = settings.value("NIRSpec", type=dict)
            self.NIRSpecMotor = self.main_window.motorCard2.motors[
                self.configNIRSpec["motorNumber"]
            ]
            print(self.configNIRSpec)
            self.wavelengthReadoutTimer = QtCore.QTimer()
            self.wavelengthReadoutTimer.timeout.connect(self.stepsToWavelength)
            self.wavelengthReadoutTimer.start(
                settings.value("interval", type=int)
            )  # in welchem Zeitintervall soll die Anzeige der Gitterposition aktualisiert werden?
            self.NIRSpecMotorReady = True
        except (ConnectionError, AttributeError):
            log.error("Could not connect motor for NIR spectrometer.")
            self.NIRSpecMotorReady = False

        # Define motor timers and motor signals
        self.motorTimerNeg = QtCore.QTimer()
        self.motorTimerNeg.timeout.connect(self.waitMotorNeg)

        self.imageEventHandler = ImageEventHandler()
        self.imageEventHandler.signals.imageGrabbed.connect(self.cameraReadout)

        self.signals = self.MotorSignals()
        self.publisher = self.main_window.publisher
        # self.signals.UpdatePlot.connect(self.updatePlot)

        # Connect buttons with slots
        self.pbSetWavelength.clicked.connect(self.setWavelength)
        self.pbHome.clicked.connect(self.home)
        self.pbStop.clicked.connect(self.stopMotor)
        self.sbExposure.valueChanged.connect(self.setExposure)
        self.slExposure.valueChanged.connect(self.slExposureChanged)
        self.sbGain.valueChanged.connect(self.setGain)
        self.pbSaveBeamProfile.clicked.connect(self.saveBeamProfile)
        self.pbSaveSpectrum.clicked.connect(self.saveSpectrum)
        self.pbReadout.clicked.connect(self.cameraReadout)

        self.setupPlot()
        self.connectCamera(True)

    class MotorSignals(QtCore.QObject):
        """Signals for motor"""

        GratingStart = QtCore.pyqtSignal(bool)
        UpdatePlot = QtCore.pyqtSignal(list)

    def setupPlot(self):
        self.pgBeamProfile.getImageItem().setOpts(axisOrder="row-major")  # for array
        self.pgBeamProfile.setPredefinedGradient(name=gradient_map)

        self.spectrum = self.pltSpectrum.plot([])
        self.spectrumFit = self.pltSpectrum.plot([], pen="r")
        self.pltSpectrum.setLabel("bottom", "Wavelength in m")
        self.pltSpectrum.setLabel("left", "Signal in arb. units")

    # %%% Define actions

    # %%%% Motoransteuerung

    # Einstellen des Gitters auf die gewünschte Wellenlänge

    def home(self):
        """Move grating to home position."""
        self.moveTo(0)

    def setWavelength(self):
        lamba = self.sbTargetWavelength.value()
        steps = self.wavelengthToSteps(lamba)
        self.moveTo(steps)

    def wavelengthToSteps(self, lamba: float):
        settings = QtCore.QSettings()
        m = -1  # diffraction order
        grating_const = settings.value("NIRGratingConst", type=float)
        delta = np.radians(settings.value("NIRAngleDelta", type=float))  # in rad
        res = self.NIRSpecMotor.drive_settings.microstep_resolution

        return self.wavelength_to_steps_function(lamba, m, grating_const, delta, res)

    @staticmethod
    def wavelength_to_steps_function(
        lamba: float, m: int, grating_const: int, delta: float, res: int
    ):
        gratingConst = grating_const * 1e3  # in lines/m
        return np.arcsin(
            m * lamba * 1e-9 * gratingConst / (2 * np.cos(delta))
            ) * 360 / (2 * np.pi) * 100 * 2**res  # in microsteps
    
    def px_to_wavelength(self, px):
        settings = QtCore.QSettings()
        m = -1  # diffraciton order
        grating_const = settings.value("NIRGratingConst", type=float)
        delta = settings.value("NIRAngleDelta", type=float)
        res = self.NIRSpecMotor.drive_settings.microstep_resolution
        motor_steps = self.NIRSpecMotor.actual_position
        focal_length = settings.value("FocalLength", type=int)

        return self.px_to_wavelength_formula(
            px, grating_const, m, delta, res, motor_steps, focal_length
        )
        # return 1 / (m * grating_const) * (
        #         np.sin((theta + delta))
        #         + np.sin((theta - delta) + np.arctan(px / focal_length))
        #         )

    @staticmethod
    def px_to_wavelength_formula(
        px: int,
        grating_const: float,
        m: int,
        delta: float,
        res: int,
        motor_steps: int,
        focal_length: float,
    ):
        gratingConst = grating_const * 1e3  # in lines/m
        deltaRad = delta * 2 * np.pi / 360
        theta = motor_steps / (100 * 2**res) * 2 * np.pi / 360  # in rad
        focalLength = focal_length * 1e-3  # in m
        return (
            1
            / (m * gratingConst)
            * (
                np.sin((theta + deltaRad))
                + np.sin((theta - deltaRad) + np.arctan(px / focalLength))
            )
        )

    def stepsToWavelength(self):
        """
        Take the current position of the grating motor
        and show it converted to central wavelengths.
        """
        lamba = self.px_to_wavelength(0) * 1e9
        self.lbActualWavelength.setText(f"{lamba:7.3f} nm")

    def moveTo(self, steps):
        settings = QtCore.QSettings()
        res = self.NIRSpecMotor.drive_settings.microstep_resolution
        self.config = settings.value("NIRSpec")
        self.motor = self.config["motorNumber"]
        self.steps = steps
        try:
            if steps >= self.NIRSpecMotor.actual_position:
                self.NIRSpecMotor.move_to(int(steps))
            else:
                self.NIRSpecMotor.move_to(int(steps - 50 * 2**res))  
                # startet regelmäßige Abfrage von waitMotorNeg(), bis position_reached=True
                self.motorTimerNeg.start(100)  
        except Exception as exc:
            print(exc)

    def waitMotorNeg(self):
        if self.NIRSpecMotor.get_position_reached():
            self.motorTimerNeg.stop()
            self.NIRSpecMotor.move_to(int(self.steps))  # fahre zurück auf pos
        else:
            pass

    @pyqtSlot()
    def stopMotor(self):
        settings = QtCore.QSettings()
        self.main_window.motorCard2.stop(settings.value("NIRSpec")["motorNumber"])

    @pyqtSlot(bool)
    def connectCamera(self, activated) -> None:
        """Connect with the camera."""
        settings = QtCore.QSettings()

        if activated:
            try:
                self.tlFactory = pylon.TlFactory.GetInstance()
                self.camera = pylon.InstantCamera()

                # Configure configuration events.
                self.configurationEventHandler = ConfigurationHandler()
                self.configurationEventHandler.signals.cameraRemoved.connect(self.cameraRemoved)
                self.camera.RegisterConfiguration(
                    self.configurationEventHandler,
                   pylon.RegistrationMode_ReplaceAll,
                   pylon.Cleanup_None,
                )

                self.camera.RegisterImageEventHandler(
                    self.imageEventHandler,
                    pylon.RegistrationMode_Append,
                    pylon.Cleanup_None,
                )

                for deviceInfo in self.tlFactory.EnumerateDevices():
                    name = deviceInfo.GetFriendlyName()
                    fullName = deviceInfo.GetFullName()
                    print(name, fullName)
                    if name == "Spektrometer (21782227)":
                        cameraName = fullName
                device = self.tlFactory.CreateDevice(cameraName)
                self.camera.Attach(device)
                self.camera.Open()
                frm = settings.value("frameRateMax", 10, type=int)
                self.camera.AcquisitionFrameRate.SetValue(frm)
                pxVertical = self.camera.SensorWidth.GetValue()
                pxHorizontal = self.camera.SensorHeight.GetValue()
                # lL = self.camera.AutoGainLowerLimit.GetValue()
                # print(f"lower limit {lL}")
                # lLr = self.camera.AutoGainLowerLimitRaw.GetValue()
                # print(f"lower limit raw {lLr}")
                # lLra = self.camera.AutoGainLowerLimitRaw_All.GetValue()
                # print(f"lower limit raw all {lLra}")
                # uL = self.camera.AutoGainUpperLimit.GetValue()
                # print(f"upper limit {uL}")
                # uLr = self.camera.AutoGainUpperLimitRaw.GetValue()
                # print(f"upper limit {uLr}")
                # uLra = self.camera.AutoGainUpperLimitRaw_All.GetValue()
                # print(f"upper limit {uLra}")
                # self.camera.AutoGainUpperLimit.SetValue(0)
                # self.camera.AutoGainUpperLimitRaw.SetValue(64)
                # self.camera.AutoGainUpperLimitRaw_All.SetValue(32)
                self.avArray = np.zeros((pxHorizontal, pxVertical))
                self.avNumber = 1
                self.camera
                self.camera.StartGrabbing(
                    pylon.GrabStrategy_LatestImageOnly,
                    pylon.GrabLoop_ProvidedByInstantCamera,
                )
            except Exception as exc:
                log.error(f"Could not connect camera: {exc}")
        else:
            print("camera not activated")
            self.camera.Close()
            self.camera.DetachDevice()

    # @pyqtSlot(list[int])
    def cameraReadout(self, array) -> None:
        """
        Process the camera data in the plot and evaluate it.
        param array: data array from camera
        """
        # autoRange adjusts the dimensions of the imageview to show everything
        self.pgBeamProfile.setImage(array, autoRange=True)
        self.singleArray = array
        self.lbMaxPxVal.setText(str(np.max(array)))
        # exposure = self.camera.ExposureTime.GetValue()
        # gain = self.camera.Gain.GetValue()
        # print(f"exposure = {exposure}")
        # print(f"gain = {gain}")

        if np.max(array) > 4095:
            self.lbOverexposed.setText(str("OVEREXPOSED"))
            # set color red, set font bold
        else:
            self.lbOverexposed.setText(str("Exposure OK"))
            # set color green
        self.cameraEvaluation(array)

    def cameraEvaluation(self, array):
        # Spectrum
        if self.avNumber < self.sbAverages.value():
            self.avArray = self.avArray + array
            self.avNumber = self.avNumber + 1
        else:
            self.avArray = (self.avArray + array) / self.sbAverages.value()
            settings = QtCore.QSettings()

            self.pxValues = self.avArray.sum(axis=0) / self.sbAverages.value()
            pxSize = settings.value("PxSize", type=int) * 1e-9  # in m

            # TODO: Rotation/Spiegelung des Sensors

            mPxPositions = np.arange(-len(self.pxValues)/2 + 1/2, len(self.pxValues)/2, 1) * pxSize

            def Gaussian(x, a, sigma, x0, c):
                return a * np.exp(-((x - x0) ** 2) / sigma**2 / 2) + c
            
            if self.cbPixelScale.isChecked():
                self.wavelengths = mPxPositions / pxSize
                expStdDev = 100  # 100 px width
            
            else:
                self.wavelengths = self.px_to_wavelength(mPxPositions)
                expStdDev = 1e-9  # 1 nm bandwidth
    
            pxMean = np.where(self.pxValues == max(self.pxValues))

            try:
                self.fitParameter, fitCovariance = curve_fit(
                    f=Gaussian,
                    xdata=self.wavelengths,
                    ydata=self.pxValues,
                    # a = Max. Pixelwert; sigma = 1nm; x0 = lambda(max. Pixelwert); c = min. Pixelwert
                    p0=[max(self.pxValues), expStdDev, self.wavelengths[pxMean][0], min(self.pxValues)]
                )
                            
            except Exception:
                pass

            bandwidth = abs(2 * np.sqrt(2 * np.log(2)) * self.fitParameter[1]) * 1e9  # FWHM in nm
            pxCentral = self.fitParameter[2] * 1e9  # fitted value for x0 in nm
            
            fitValues = Gaussian(self.wavelengths, *self.fitParameter)

            self.spectrum.setData(self.wavelengths, self.pxValues)
            self.spectrumFit.setData(self.wavelengths, fitValues)

            self.lbBandwidth.setText(f"{bandwidth:7.3f} nm")
            self.lbCentralWavelength.setText(f"{pxCentral:7.3f} nm")
            self.sendData({"NIRwavelength": pxCentral, "NIRbandwidth": bandwidth})

            pxVertical = self.camera.SensorWidth.GetValue()
            pxHorizontal = self.camera.SensorHeight.GetValue()
            self.avArray = np.zeros((pxHorizontal, pxVertical))
            self.avNumber = 1

        #       - test motor movement

        #       - send data to common port (11100) ("send data" in TiSa-program),
        #       - optional saving of the raw plot (option for averaging)
        #       - maybe: show color plot with full camera data to see if the camera gets hit
        #       vertically in the centre

    @pyqtSlot(int)
    def setExposure(self, value: int) -> None:
        """Set the new exposure time of the camera in µs."""
        try:
            self.camera.ExposureTime.SetValue(value)
            self.slExposure.setSliderPosition(value)
        except Exception as exc:
            log.exception(exc)
    
    @pyqtSlot(int)
    def slExposureChanged(self, value: int) -> None:
        self.sbExposure.setValue(value)

    @pyqtSlot(float)
    def setGain(self, value: float) -> None:
        """Set the gain of the camera"""
        try:
            self.camera.Gain.SetValue(value)
        except Exception as exc:
            log.exception(exc)

    def saveBeamProfile(self):
        """Saves one single camera image"""
        settings = QtCore.QSettings()
        self.leSavedNameBP.setText("Saving...")

        path = settings.value("pathNIRSpectrum", type=str)
        filename = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S_img")
        data_dict = {
            "motor position": self.NIRSpecMotor.actual_position,
            "data": self.singleArray.tolist(),
            "wavelengths": self.wavelengths.tolist(),
            }
        with open(f"{path}{filename}.pkl", "wb") as file:
            pickle.dump(data_dict, file)
        
        self.leSavedNameBP.setText(filename)
        log.info(f"Saved image {filename}.")
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(filename)

    def saveSpectrum(self):
        """Saves the averaged and summed-up pixel values & fit parameters"""
        self.leSavedNameSpec.setText("Saving...")

        path = "D:\\Measurement Data\\NIRSpectrometer_py\\"
        filename = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S_spec")
        data_dict = {
            "amplitude": self.fitParameter[0],
            "std. dev.": self.fitParameter[1],
            "mean": self.fitParameter[2],
            "offset": self.fitParameter[3],
            "motor position": self.NIRSpecMotor.actual_position,
            "pixel values": self.pxValues.tolist(),
            "wavelengths": self.wavelengths.tolist(),
            }
        with open(f"{path}{filename}.json", "wb") as file:
            json.dump(data_dict, file)
        
        self.leSavedNameSpec.setText(filename)
        log.info(f"Saved spectrum {filename}.")
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(filename)
        
    def sendData(self, values):
        try:
            self.publisher.send_legacy(values)
        except Exception as exc:
            log.exception("Sending data failed!", exc)

    def cameraRemoved(self, camera: pylon.InstantCamera) -> None:
        """Handle a camera removal."""
        camera.DestroyDevice()

    ### Handhabung des Programms
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.wavelengthReadoutTimer.stop()  # Stoppe Aktualisierung der Gitterposition
        self.connectCamera(False)
        event.accept()


if __name__ == "__main__":
    from pytrinamic.modules.TMCM6110 import TMCM6110
    from devices.pyTrinamicTest import ConnectionManager
    app = QtWidgets.QApplication([])  # create an application
    
    class Main:
        def __init__(self):
            conn = ConnectionManager()
            self.motorCard2 = TMCM6110(conn.connect())

    mainwindow = NIRSpectrum(parent=Main())  # start the first widget, the main window
    app.exec()  # start the application with its Event loop


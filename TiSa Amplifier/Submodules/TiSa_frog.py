from PyQt6 import QtWidgets, QtCore, uic
import pyqtgraph as pg
from PyQt6.QtCore import pyqtSlot
import numpy as np
from devices import motors
from pyleco.core.data_message import DataMessage
import datetime
import pickle
import json
from scipy.optimize import curve_fit, OptimizeWarning
import matplotlib.pyplot as plt
import logging

import Submodules.TiSa_main as TiSa_main
log = logging.getLogger("TiSaAmp")


class FROG(QtWidgets.QDialog):

    # %%% Widgets in the GUI
    CameraDataPlot: pg.PlotWidget
    Cut_delay: pg.PlotWidget
    Cut_spectrum: pg.PlotWidget
    pgImage: pg.ImageView
    gbAlignment: QtWidgets.QGroupBox
    gbEvaluation: QtWidgets.QGroupBox
    gbMeasurement: QtWidgets.QGroupBox
    lbStagePosition: QtWidgets.QLabel
    lbGratingPosition: QtWidgets.QLabel
    pbDelayAlignment: QtWidgets.QPushButton
    pbGratingAlignment: QtWidgets.QPushButton
    pbLoadDefault: QtWidgets.QPushButton
    pbSaveSpec: QtWidgets.QPushButton
    pbEval: QtWidgets.QPushButton
    pbLoadEval: QtWidgets.QPushButton
    pbStart: QtWidgets.QPushButton
    pbStop: QtWidgets.QPushButton
    pbSaveDefault: QtWidgets.QPushButton
    sbStagePosition: QtWidgets.QDoubleSpinBox
    sbGratingPosition: QtWidgets.QDoubleSpinBox
    sbCenterWavelength: QtWidgets.QDoubleSpinBox
    sbDelayCenter: QtWidgets.QDoubleSpinBox
    sbTimeWidth: QtWidgets.QDoubleSpinBox
    sbShots: QtWidgets.QSpinBox
    sbSteps: QtWidgets.QSpinBox
    leFileName: QtWidgets.QLineEdit
    cbSaveData: QtWidgets.QCheckBox
    progressBar: QtWidgets.QProgressBar
    lbProgress: QtWidgets.QLabel
    lbTimeLeft: QtWidgets.QLabel
    comboFunction: QtWidgets.QComboBox
    cbShowWl: QtWidgets.QCheckBox

    # %%% Setup
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent=parent, *args, **kwargs)
        self.main_window: TiSa_main.TiSaAmp = parent

        uic.loadUi("data/Frog.ui", self)
        self.show()

        # Initialize window
        settings = QtCore.QSettings()

        # Setup Plots
        self.setupPlotCamera()
        self.pgImage.setPredefinedGradient("spectrum")
        self.imagePlotItem: pg.PlotItem = self.pgImage.getView()

        self.imagePlotItem.setLabel("bottom", "Delay", "m")
        self.imagePlotItem.setLabel("left", "Spectrum", "nm")

        # Start readout & check connections
        try:
            self.configGrating: dict = settings.value("FROGgrating", type=dict)
            self.configStage: dict = settings.value("FROGstage", type=dict)
            self.gratingMotor = self.main_window.motorCard2.motors[self.configGrating["motorNumber"]]
            self.stageMotor = self.main_window.motorCard3.motors[self.configStage["motorNumber"]]
            self.readoutTimer = QtCore.QTimer()
            self.readoutTimer.timeout.connect(self.readout)
            self.readoutTimer.start(settings.value("Interval"))
            self.gratingMotorReady = True
            self.stageMotorReady = True
            # TODO: Find a nice way to check, if the motor positions from the last software usage
            # are identical.
        except (ConnectionError, AttributeError):
            log.error("Could not connect motors for FROG-measurement.")
            self.gratingMotorReady = False
            self.stageMotorReady = False

        # Define motor timers for measurement, as well as motor signals
        self.motorTimerGrating = QtCore.QTimer()
        self.motorTimerGrating.timeout.connect(self.waitMotorGrating)
        self.motorTimerGratingStart = QtCore.QTimer()
        self.motorTimerGratingStart.timeout.connect(self.waitMotorGratingStart)
        self.motorTimerStage = QtCore.QTimer()
        self.motorTimerStage.timeout.connect(self.waitMotorStage)
        self.motorTimerStageStart = QtCore.QTimer()
        self.motorTimerStageStart.timeout.connect(self.waitMotorStageStart)
        self.motorTimerStageMeasurement = QtCore.QTimer()
        self.motorTimerStageMeasurement.timeout.connect(self.waitMotorStageMeasurement)
        self.readoutTimerMeasurement = QtCore.QTimer()
        self.readoutTimerMeasurement.timeout.connect(self.readoutCamera)
        self.motorTimerNegative = QtCore.QTimer()
        self.motorTimerNegative.timeout.connect(self.negativeGrating)

        self.signals = self.MotorSignals()

        self.signals.GratingStart.connect(self.stageStart)
        self.signals.StageContinue.connect(self.moveStageMeasurement)
        self.signals.UpdatePlot.connect(self.UpdatePlot)
        self.cbShowWl.stateChanged.connect(self.showWl)

        # Setup of Listener.
        self.data_pipe = []
        self.listener = self.main_window.listener
        self.listener.signals.dataReady.connect(self.handle_data)
        self.listener.signals.data_message.connect(self.handle_data_message)
        self.communicator = self.main_window.communicator
        self.communicator.subscribe('FROG')

        # Setup progress bar.
        self.progressBar.setTextVisible(False)
        self.progressBar.reset()

        # Setup Measurement standard values
        measurementValues: list = settings.value("lastFROGMeasurementValues")
        self.sbCenterWavelength.setValue(float(measurementValues[0]))
        self.sbDelayCenter.setValue(float(measurementValues[1]))
        self.sbGratingPosition.setValue(float(measurementValues[0]))
        self.sbStagePosition.setValue(float(measurementValues[1]))
        self.sbTimeWidth.setValue(float(measurementValues[2]))
        self.sbSteps.setValue(int(measurementValues[3]))
        self.sbShots.setValue(int(measurementValues[4]))

        # Connect buttons to slots
        self.pbDelayAlignment.clicked.connect(self.alignDelay)
        self.pbGratingAlignment.clicked.connect(self.alignGrating)
        self.pbStart.clicked.connect(self.startMeasurement)
        self.pbStop.clicked.connect(self.stopMeasurement)
        self.pbSaveDefault.clicked.connect(self.saveDefault)
        self.pbLoadDefault.clicked.connect(self.loadDefault)
        self.pbSaveSpec.clicked.connect(self.saveSpectrum)
        self.pbEval.clicked.connect(self.evaluation)
        self.pbLoadEval.clicked.connect(self.loadEval)

        self.pbDelayAlignment.click()
        self.pbGratingAlignment.click()

    def reset(self):
        """Move motors to home position."""
        settings = QtCore.QSettings()
        settings.setValue("lastMotorPositionFROGgrating", self.gratingMotor.actual_position)
        settings.setValue("lastMotorPositionFROGstage", self.stageMotor.actual_position)
        settings.setValue("lastFROGMeasurementValues", [self.sbCenterWavelength.value(),
                                                        self.sbDelayCenter.value(),
                                                        self.sbTimeWidth.value(),
                                                        self.sbSteps.value(),
                                                        self.sbShots.value()])

    @pyqtSlot()
    def acceptEvent(self, event):
        """Remove variables from the communicator, that are not needed in the main program."""
        log.info("Closing.")
        self.communicator.unsubscribe("FROGcamData")
        self.reset()
        event.accept()

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.communicator.unsubscribe("FROGcamData")
        self.reset()
        event.accept()

    @pyqtSlot()
    def setupPlotCamera(self):
        """Configure the plot."""
        self.CamPlot = self.CameraDataPlot.plot([])
        self.CameraDataPlot.setLabel("bottom", "Wavelength in nm")
        self.CameraDataPlot.setLabel("left", "Signal in arb. units")
        self.plot_spectrum = self.Cut_spectrum.plot([], [], pen="white")
        self.spectrum_Gaussianfit = self.Cut_spectrum.plot([], name="Gaussianfit", pen="red")
        self.spectrum_Sechfit = self.Cut_spectrum.plot([], name="Sechfit", pen="orange")
        self.Cut_spectrum.setLabel("bottom", "Wavelength in nm")
        self.Cut_spectrum.setLabel("left", "Intensity in a.u.")
        self.plot_delay = self.Cut_delay.plot([], [], pen="white")
        self.delay_Gaussianfit = self.Cut_delay.plot([], name="fit", pen="red")
        self.delay_Sechfit = self.Cut_delay.plot([], name="Sechfit", pen="orange")
        self.Cut_delay.setLabel("bottom", "Delay in ps")
        self.Cut_delay.setLabel("left", "Intensity in a.u.")

    # Changes the Units of the Spinbox if the Checkbox is Checked or not
    @pyqtSlot(int)
    def showWl(self, state: int) -> None:
        if state == 0:
            self.sbGratingPosition.setSuffix("°")
            self.sbGratingPosition.setDecimals(6)
        else:
            self.sbGratingPosition.setSuffix(" nm")
            self.sbGratingPosition.setDecimals(2)

    # %%% Handle data

    @pyqtSlot()
    def readout(self):
        """Calculate the wavelength position of the grating and the transformation of the x-axis of
        the plot. Explicit values are extracted from the former LabView program new
        (frog v0.5.1_new Spectro vi.vi)."""

        try:
            pos_steps: int = self.gratingMotor.actual_position  # position of the grating in steps
            revolutions: float = pos_steps / 2**(self.configGrating['stepResolution'] - 1) / 200
            ang: float = revolutions / 25.2 / 220 * 360 / 2    # transformation to angle in deg
            if not self.cbShowWl.isChecked():
                self.lbGratingPosition.setText(f"{round(ang, 3)} °")
            else:
                lines: float = 1798.5   # lines per mm
                delta: float = 12.946   # half opening angle
                N: int = 3000   # number of pixels on the line camera
                d: int = 7000   # size of one pixel in nm
                G: float = 1e6 / lines
                f: float = 500 * 1e6 * np.cos(delta / 180 * np.pi)  # focus length
                m: int = -1     # order of diffraction
                a1: float = np.sin((ang + delta) / 180 * np.pi)
                lamba_central: float = G * (a1 + np.sin((ang - delta) / 180 * np.pi)) / m
                lamba_vector: list = []
                for i in range(N):
                    x3 = (N / 2 - i) * d
                    a2: float = np.sin((ang - delta) / 180 * np.pi + np.arctan(x3 / f))
                    lamba_vector.append(G * (a1 + a2) / m)
                self.lamba_vector = lamba_vector
                self.lbGratingPosition.setText(f"{round(lamba_central, 3)} nm")
            stageSteps: int = self.stageMotor.actual_position
            stagePos: float = motors.stepsToUnits(stageSteps, self.configStage)
            self.lbStagePosition.setText(f"{round(stagePos, 3)} mm")
        except (ConnectionError, TimeoutError):
            log.error("Readout not possible.")
            self.readoutTimer.stop()

    @pyqtSlot(DataMessage)
    def handle_data_message(self, message: DataMessage):
        self.handle_data({message.topic.decode(): message.data})

    @pyqtSlot(dict)
    def handle_data(self, data: dict):
        try:
            self.datapoint: list = data['FROGcamData']
            if len(self.data_pipe) < 10000:
                self.data_pipe.append(self.datapoint)
            else:
                self.data_pipe.append(self.datapoint)
                self.data_pipe.pop(0)
            self.signals.UpdatePlot.emit(self.datapoint)
        except KeyError:
            pass

    @pyqtSlot(list)
    def UpdatePlot(self, data: list):
        if self.cbShowWl.isChecked():
            try:
                self.CamPlot.setData(self.lamba_vector, data)
            except AttributeError:
                self.CamPlot.setData(data)
        else:
            self.CamPlot.setData(np.linspace(1, 3001, 3000), data)

    # %%% Movement of grating and stage

    @pyqtSlot()
    def alignDelay(self):
        try:
            steps: int = motors.unitsToSteps(self.sbStagePosition.value(), self.configStage)
            self.motorTimerStage.start(200)
            self.stageMotor.move_to(steps, velocity=self.configStage["positioningSpeed"])
        except (ConnectionError, TimeoutError):
            log.error("Motorcard of stage not connected.")

    def waitMotorStage(self):
        posReached: bool = self.stageMotor.get_position_reached()
        if posReached:
            self.pbDelayAlignment.setEnabled(True)
            self.pbStart.setEnabled(True)
            self.motorTimerStage.stop()

    @pyqtSlot()
    def alignGrating(self):
        lines: float = 1798.5
        G: float = 1e6 / lines
        delta: float = 12.946
        m: int = -1
        
        wavelength: float = self.sbGratingPosition.value()
        ang: float = np.arcsin(m * wavelength / (2 * G * np.cos(delta / 180 * np.pi))) * 180 / np.pi
        if not self.cbShowWl.isChecked(): 
            ang: float = self.sbGratingPosition.value()
        stepResolution: int = self.configGrating['stepResolution']
        steps: int = int(ang / 360 * 220 * 25.2 * 2**(stepResolution - 0) * 200)
        try:
            self.pbStart.setEnabled(False)
            if self.gratingMotor.actual_position < steps - 4000:
                self.gratingMotor.move_to(steps, velocity=self.configGrating["positioningSpeed"])
                self.motorTimerGrating.start(200)
            elif self.gratingMotor.actual_position == steps:
                self.pbGratingAlignment.setEnabled(True)
                self.pbStart.setEnabled(True)
            else:
                self.gratingMotor.move_to(steps - 5000, velocity=self.configGrating["positioningSpeed"])
                self.motorTimerNegative.start(200)
        except (ConnectionError, TimeoutError):
            log.error("Motorcard of grating not connected.")

    @pyqtSlot()
    def waitMotorGrating(self):
        posReached: bool = self.gratingMotor.get_position_reached()
        if posReached:
            self.pbGratingAlignment.setEnabled(True)
            self.pbStart.setEnabled(True)
            self.motorTimerGrating.stop()

    @pyqtSlot()
    def negativeGrating(self):
        if self.gratingMotor.get_position_reached():
            self.motorTimerNegative.stop()
            self.alignGrating()
        else:
            pass

    def saveDefault(self):
        settings = QtCore.QSettings()
        settings.setValue("FROGStagePosDefault", float(self.lbStagePosition.text()[:-3]))
        settings.setValue("FROGGratingPosDefault", float(self.lbGratingPosition.text()[:-3]))

    def loadDefault(self):
        settings = QtCore.QSettings()
        try:
            self.sbStagePosition.setValue(settings.value("FROGStagePosDefault"))
            self.sbGratingPosition.setValue(settings.value("FROGGratingPosDefault"))
        except KeyError:
            log.info("No default values have been stored yet.")

    # %%% Measurement

    @pyqtSlot()
    def saveSpectrum(self):
        """Save the spectrum that can be seen in the plot right now."""
        path = "D:\\Measurement Data\\FROG_py\\Spectrum\\"
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        data_dict = {"spectrum": self.datapoint, "wavelength": self.lamba_vector}
        with open(f"{path}{file_name}.pkl", "wb") as file:
            pickle.dump(data_dict, file)
        with open(f"{path}{file_name}.json", "w") as file:
            json.dump(data_dict, file)
        print("Data saved.")

    @pyqtSlot()
    def startMeasurement(self):
        """Move grating to starting position"""
        try:
            log.info("Move grating to start.")
            self.pbStart.setEnabled(False)
            self.pbGratingAlignment.setEnabled(False)
            self.pbDelayAlignment.setEnabled(False)
            self.cameraData = np.full(shape=[self.sbSteps.value(), 3000], fill_value=np.nan)
            stepsize_mm = self.sbTimeWidth.value() * 1e-9 * 299792458 * 2 / self.sbSteps.value()
            self.stepsize = motors.unitsToSteps(stepsize_mm, self.configStage)
            self.position = []
            self.shotNumber = 0
            self.stepNumber = 1
            self.sbGratingPosition.setValue(self.sbCenterWavelength.value())
            self.alignGrating()
            self.progressBar.reset()
            self.progressBar.setRange(0, 100)
            self.lbProgress.setText("progress: 0.00 %")
            self.zeroTime = round(self.sbShots.value() * 0.05 * self.sbSteps.value(), 1)
            self.lbTimeLeft.setText(f"Time left: {self.zeroTime} s")
            self.progress = 0
            self.motorTimerGratingStart.start(50)
        except (ConnectionError, TimeoutError):
            log.error("Couldn't move grating to start position.")

    @pyqtSlot(bool)
    def stageStart(self, status: bool):
        """Move the stage to the start position."""
        if status:
            try:
                log.info("Move stage to start.")
                stepsStage = motors.unitsToSteps(self.sbDelayCenter.value()
                                                 - self.sbTimeWidth.value()
                                                 * 1e-9 * 299792458, self.configStage)
                self.stageMotor.move_to(stepsStage, velocity=self.configStage["positioningSpeed"])
                self.data_raw = np.array([0.0 for element in range(3000)])
                delay_vector = np.linspace(self.sbDelayCenter.value()
                                           - self.sbTimeWidth.value() * 1e-9 * 299792458,
                                           self.sbDelayCenter.value()
                                           + self.sbTimeWidth.value() * 1e-9 * 299792458,
                                           self.sbSteps.value(), dtype=float)
                self.imagePlotItem.getAxis("left").setScale(self.lamba_vector[1]
                                                            - self.lamba_vector[0])
                self.imagePlotItem.getAxis("bottom").setScale((self.sbTimeWidth.value()
                                                               * 1e-9 * 299792458)
                                                              / self.sbSteps.value() * 2 / 1000)
                self.imagePlotItem.getAxis("bottom").setRange(delay_vector[0] / 1000,
                                                              delay_vector[-1] / 1000)
                self.motorTimerStageStart.start(50)
            except (ConnectionError, TimeoutError):
                log.error("Couldn't move stage to start position.")

    @pyqtSlot(bool)
    def moveStageMeasurement(self, status: bool):
        """Move the stage forward the number of specified steps,
           while waiting for the measurement in between."""
        if self.stepNumber < self.sbSteps.value():
            self.main_window.motorCard3.move_by(self.configStage["motorNumber"], self.stepsize)
            self.stepNumber += 1
            self.data_raw = np.array([0.0 for element in range(3000)])
            self.motorTimerStageMeasurement.start(10)
        else:
            if self.cbSaveData.isChecked():
                self.saveData()
            self.pbStart.setEnabled(True)
            self.pbGratingAlignment.setEnabled(True)
            self.pbDelayAlignment.setEnabled(True)

    def readoutCamera(self):
        """Readout of camera data for the measurement."""
        if self.shotNumber < self.sbShots.value():
            self.data_raw += np.array(self.data_pipe[-1])
            self.shotNumber += 1
            self.progress += 1
            prog_value = self.progress / self.sbShots.value() / self.sbSteps.value() * 100
            self.lbProgress.setText(f"progress: {round(prog_value, 2)} %")
            self.lbTimeLeft.setText(f"Time left: {round(self.zeroTime * (1 - prog_value / 100), 1)}")
            self.progressBar.setValue(int(round(prog_value, 0)))
        else:
            print(f'Steps: {self.stepNumber}')
            self.readoutTimerMeasurement.stop()
            self.cameraData[self.stepNumber - 1] = self.data_raw
            self.shotNumber = 0
            actualPos = motors.stepsToUnits(self.stageMotor.actual_position, self.configStage)
            self.position.append(actualPos)
            self.pgImage.setImage(self.cameraData, autoRange=True)
            self.signals.StageContinue.emit(True)

    def saveData(self):
        """Save the data of the measurement, in case the checkbox is checked."""
        print("Save data.")
        path = "D:\\Measurement Data\\FROG_py\\"
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        data_dict = {"position": self.position,
                     "spectrum": self.lamba_vector,
                     "data": self.cameraData.tolist()}
        with open(f"{path}{file_name}.pkl", "wb") as file:
            pickle.dump(data_dict, file)
        with open(f"{path}{file_name}.json", "w") as file:
            json.dump(data_dict, file)
        self.leFileName.setText(file_name)
        self.data = np.array(data_dict['data']) / np.amax(data_dict['data'])
        self.spectrum = self.lamba_vector
        position_middle = (max(self.position) + min(self.position)) / 2
        self.position_array = position_middle - np.array(self.position)
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(file_name)
        self.loadEval()
        print("Data saved.")

    @pyqtSlot()
    def stopMeasurement(self):
        """Stop the measurement, including stopping all motor movements and release the buttons."""
        self.pbStart.setEnabled(True)
        self.pbGratingAlignment.setEnabled(True)
        self.pbDelayAlignment.setEnabled(True)
        self.readoutTimerMeasurement.stop()
        self.motorTimerGratingStart.stop()
        self.motorTimerStageStart.stop()
        self.motorTimerStageMeasurement.stop()
        try:
            self.gratingMotor.stop()
        except ConnectionError:
            log.error("Connection to Grating motor lost.")
        try:
            self.stageMotor.stop()
        except ConnectionError:
            log.error("Connection to Stage motor lost.")

    def waitMotorGratingStart(self):
        """Check regular, if the grating has reached its position for the measurement."""
        if self.pbGratingAlignment.isEnabled():
            self.pbStart.setEnabled(False)
            self.pbGratingAlignment.setEnabled(False)
            self.pbDelayAlignment.setEnabled(False)
            self.motorTimerGratingStart.stop()
            self.signals.GratingStart.emit(True)
            log.info("Grating reached position for measurement.")

    def waitMotorStageStart(self):
        '''Check regular, if the stage has reached the start position.'''
        posReached = self.stageMotor.get_position_reached()
        if posReached:
            self.motorTimerStageStart.stop()
            log.info("Stage reached start position.")
            self.readoutTimerMeasurement.start(50)

    def waitMotorStageMeasurement(self):
        '''Check regular, if the stage has reached its new position.'''
        posReached = self.stageMotor.get_position_reached()
        if posReached:
            self.motorTimerStageMeasurement.stop()
            log.info("Stage reached start position.")
            self.readoutTimerMeasurement.start(50)

    class MotorSignals(QtCore.QObject):
        """Signals for motor."""
        GratingStart = QtCore.pyqtSignal(bool)
        StageContinue = QtCore.pyqtSignal(bool)
        UpdatePlot = QtCore.pyqtSignal(list)

    # %%% Evaluation

    @pyqtSlot()
    def evaluation(self) -> None:

        def Gaussian(x, a, sigma, x0, c):
            return a * np.exp(-(x - x0)**2 / sigma**2 / 2) + c

        def sech37(x, a, sigma, x0, c):
            return a / np.cosh((x - x0) / sigma)**3.794 + c

        delay_sum = np.sum(self.data.T, axis=0)
        self.plot_delay.setData(self.delay, delay_sum / np.max(delay_sum))
        try:
            if self.comboFunction.currentText() == "Gaussian":
                fitParameter, fitCovariance = curve_fit(f=Gaussian,
                                                        xdata=self.delay,
                                                        ydata=delay_sum / np.max(delay_sum),
                                                        p0=[1, 2, 0, 0.1])
                timewidth = abs(2 * np.sqrt(2 * np.log(2)) * fitParameter[1] / np.sqrt(3 / 2))
                self.delay_Gaussianfit.setData(self.delay, Gaussian(self.delay, *fitParameter))
            elif self.comboFunction.currentText() == "sech²":
                fitParameter, fitCovariance = curve_fit(sech37, self.delay, delay_sum
                                                        / np.max(delay_sum),
                                                        p0=[1, 2, 0, 0.1])
                timewidth = abs(fitParameter[1]) / 1.286
                self.delay_Sechfit.setData(self.delay, sech37(self.delay, *fitParameter))
        except OptimizeWarning:
            self.lbPulseLength.setText("Fit error.")

        spec_sum = np.zeros(len(self.spectrum))
        spec_center = (self.spectrum[-1] + self.spectrum[0]) / 2
        for i in range(len(self.data)):
            spec_sum += self.data[i]

        self.plot_spectrum.setData(self.spectrum, spec_sum / np.max(spec_sum))
        try:
            fitParameter, fitCovariance = curve_fit(Gaussian, self.spectrum, spec_sum
                                                    / np.max(spec_sum),
                                                    p0=[1, 0.5, spec_center, 0.1])
            centerwavelength = fitParameter[2]
            bandwidth = np.sqrt((2 * np.sqrt(2 * np.log(2)) * fitParameter[1] * 1e-9)**2
                                - 2.3548**4 * (centerwavelength * 1e-9)**4
                                / (8 * np.pi**2 * 299792458**2 * (timewidth * 1e-12)**2)) * 1e9
            self.spectrum_Gaussianfit.setData(self.spectrum, Gaussian(self.spectrum, *fitParameter))
            self.lbBandwidth.setText(str(round(bandwidth, 2)) + " nm")
            self.lbWavelengthCenter.setText(str(round(centerwavelength, 2)) + " nm")
            self.lbPulseLength.setText(str(round(timewidth, 2)) + " ps")
            freq = 299792458 * bandwidth * 1e9 / centerwavelength**2
            TBP = freq * timewidth * 1e-12
            self.lbTBP.setText(str(round(TBP, 3)))
        except OptimizeWarning:
            self.lbBandwidth.setText("Fit error.")

    @pyqtSlot()
    def loadEval(self):
        # Loading Data
        folder = 'D:\\Measurement Data\\FROG_py\\'
        with open(folder + self.leFileName.text() + '.pkl', 'rb') as file:
            data_dict = pickle.load(file)
        print("Loading data successful.")
        # Colorplot of Data
        self.data = np.array(data_dict['data']) / np.amax(data_dict['data'])
        self.spectrum = data_dict['spectrum']
        position = np.array(data_dict['position'])
        position_middle = (max(position) + min(position)) / 2
        self.position_array = position_middle - position
        self.delay = self.position_array / 299792458 * 2 * 1e9
        print(len(self.spectrum), len(self.position_array), np.size(self.data))
        # plt.figure()
        # plt.pcolormesh(self.spectrum, self.position_array, self.data, cmap="gist_ncar")
        # plt.colorbar()
        # plt.show()
        self.evaluation()

from PyQt6 import QtWidgets, QtCore, uic
from PyQt6.QtCore import pyqtSlot
from devices import motors
import numpy as np
import pandas as pd
import warnings
from scipy.optimize import OptimizeWarning
from scipy.optimize import curve_fit
import datetime
import json
import pickle
import logging

log = logging.getLogger("TiSaAmp")


class Autocorrelator(QtWidgets.QDialog):
    def __init__(self, parent, *args, **kwargs):
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)
        self.parent = parent
        self.main_window = parent

        # Load the user interface file and show it.
        uic.loadUi("data/Autocorrelator.ui", self)
        self.show()

        # Initialize window
        self.setupPlot()
        self.pbStart.setEnabled(True)
        self.motorTimer = QtCore.QTimer()
        self.motorTimer.timeout.connect(self.waitMotor)
        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.adjustment)
        self.dataAdjust = []
        self.motorTimerSweep = QtCore.QTimer()
        self.motorTimerSweep.timeout.connect(self.waitMotorSweep)

        self.signals = self.MotorSignals()

        self.signals.startPos.connect(self.startMeasurement)
        self.signals.endPos.connect(self.reset)

        # Setup of Listener.
        self.data_pipe = {"Autocorrelator": [], "Regen": [],
                          "PD532Regen": [], "cwMira": []}
        self.listener = self.main_window.listener
        self.listener.signals.dataReady.connect(self.handle_data)
        # self.listener.signals.data_message.connect(self.handle_data_message)
        self.keys = ['Autocorrelator', "Regen", "cwMira", "PD532Regen"]
        self.communicator = self.parent.communicator
        for element in self.keys:
            self.communicator.subscribe(element)
        # self.communicator.subscribe('FROG')
        # self.listener = OtListener(name='Autocorrelator', host='localhost', logger=log)
        # self.listener.signals.dataReady.connect(self.handle_data)
        # self.listener.start_listen()
        #
        # for element in self.keys:
        #    self.listener.subscribe(element)

        # Connect buttons to slots
        self.pbStart.clicked.connect(self.start)
        self.pbStop.clicked.connect(self.stop)
        self.pbSave.clicked.connect(self.saveData)
        self.pbAdjustment.clicked.connect(self.startAdjustment)
        self.pbSweep.clicked.connect(self.sweep)

    def closeEvent(self, event):
        log.info("Close Autocorrelator.")
        self.listener.stop_listen()
        event.accept()

    def handle_data(self, data):
        for element in self.keys:
            if element in data:
                if len(self.data_pipe[element]) < 1000:
                    self.data_pipe[element].append(data[element])
                else:
                    self.data_pipe[element].append(data[element])
                    self.data_pipe[element].pop(0)

    def setupPlot(self):
        """Configure the plot."""
        self.figure = self.plot.plot([])
        self.figure_fit = self.plot.plot([], pen="r")
        self.plot.setLabel("left", "PD signal in V")

    @pyqtSlot()
    def start(self):
        self.pbStart.setEnabled(False)
        settings = QtCore.QSettings()
        self.config = settings.value("Autocorrelator", type=dict)
        self.motor = self.config["motorNumber"]
        # initialize motor
        self.startPos = self.sbStartPos.value()
        self.endPos = self.startPos + self.sbDistance.value()
        steps = motors.unitsToSteps(self.startPos, self.config)
        self.position = self.startPos
        self.parent.motorCard2.move_to(self.motor, steps, velocity=80)
        self.motorTimer.start(50)

    def waitMotor(self):
        actPosSteps = self.parent.motorCard2.motors[self.config["motorNumber"]].actual_position
        actPosUnits = motors.stepsToUnits(actPosSteps, self.config)
        if self.position == self.startPos:
            print(f"Move to Start: {round(actPosUnits, 2)} -> {self.startPos}")
            if actPosUnits == self.position:
                print("StartPos reached.")
                self.motorTimer.stop()
                self.signals.startPos.emit(True)
        elif self.position == self.endPos:
            print(f"Proceed measurement: {round(actPosUnits, 2)} -> {self.endPos}")
            self.data["Position"].append(actPosUnits)
            self.data["PD"].append(self.data_pipe["Autocorrelator"][-1])
            self.data["cw"].append(self.data_pipe["cwMira"][-1])
            self.data["Pump"].append(self.data_pipe["PD532Regen"][-1])
            self.data["Regen"].append(self.data_pipe["Regen"][-1])
            if actPosUnits == self.position:
                self.signals.endPos.emit(True)
                self.motorTimer.stop()

    @pyqtSlot(bool)
    def startMeasurement(self, status):
        print("Start measurement.")
        steps = motors.unitsToSteps(self.endPos, self.config)
        self.position = self.endPos
        self.parent.motorCard2.move_to(self.motor, steps, velocity=self.sbMotorSpeed.value())
        self.data = {"Position": [], "PD": [], "cw": [], "Regen": [], "Pump": []}
        self.motorTimer.start(50)

    @pyqtSlot(bool)
    def reset(self, status):
        # Fitfunction for Autocorrelation
        def f_auto(x, a, b, c, d):
            return a + b * (np.cosh((x - d) / c)) ** (-3.7)

        steps = motors.unitsToSteps(self.startPos, self.config)
        self.parent.motorCard2.move_to(self.motor, steps, 80)
        dat = pd.DataFrame(
            {"Pos": np.array(self.data["Position"]) * 1e-3 / 299792458 * 2, "PD": self.data["PD"]}
        )
        dat = dat.dropna()
        self.figure.setData(np.array(self.data["Position"]) * 1e-3 / 299792458 * 2, self.data["PD"])
        with warnings.catch_warnings():
            warnings.simplefilter("error", OptimizeWarning)
            try:
                x = np.linspace(min(dat["Pos"]), max(dat["Pos"]), 10000)
                fitParameter, fitCovariance = curve_fit(
                    f_auto,
                    dat["Pos"],
                    dat["PD"],
                    p0=[dat["PD"][0], max(dat["PD"]), 2,
                        dat["Pos"][list(dat["PD"]).index(max(dat["PD"]))]]
                )
                self.pulselength = 2 * fitParameter[2] * np.arccosh(np.sqrt(2) ** 3.7) / 1.54
                self.lbPulseLength.setText(str(round(self.pulselength, 3)))
                self.plot.setLabel("bottom", "Time ps")
                self.figure_fit.setData(x * 1e12, f_auto(x, *fitParameter))
            except OptimizeWarning:
                print("Could not fit data.")
                self.pulselength = np.nan
        self.pbStart.setEnabled(True)

    @pyqtSlot()
    def saveData(self):
        print(self.data)
        data = self.data
        header = ["Position", "PD", "cw", "Pump", "Regen"]
        config = {"units": ["mm", "arb. units"]}
        name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        path = "D:\\Measurement Data\\FastAutocorrelator\\"
        with open(f"{path}{name}.pkl", "wb") as file:
            file.write(pickle.dumps((header, data, config)))
        with open(f"{path}{name}.json", "w") as file:
            json.dump((header, data, config), file)

    @pyqtSlot()
    def stop(self):
        self.pbStart.setEnabled(True)
        self.motorTimer.stop()
        self.parent.motorCard2.stop(self.motor)

    @pyqtSlot()
    def startAdjustment(self):
        self.plot.setLabel("bottom", "")
        if self.pbAdjustment.isChecked():
            settings = QtCore.QSettings()
            self.readoutTimer.start(settings.value("interval"))
        else:
            self.readoutTimer.stop()
            self.dataAdjust = []
            self.figure.setData([])

    @pyqtSlot()
    def adjustment(self):
        if len(self.dataAdjust) < self.sbSamplenumber.value():
            self.dataAdjust.append(self.data_pipe["Autocorrelator"][-1])
        elif len(self.dataAdjust) == self.sbSamplenumber.value():
            self.dataAdjust.pop(0)
            self.dataAdjust.append(self.data_pipe["Autocorrelator"][-1])
        else:
            self.dataAdjust.pop()
        self.figure.setData(self.dataAdjust)

    @pyqtSlot()
    def sweep(self):
        settings = QtCore.QSettings()
        self.startPos = self.sbStartPos.value()
        self.endPos = self.sbStartPos.value() + self.sbDistance.value()
        self.config = settings.value("Autokorrelator", type=dict)
        self.motor = self.config["motorNumber"]
        actPosSteps = self.parent.motorCard2.motors[self.config["motorNumber"]].actual_position
        actPosUnits = motors.stepsToUnits(actPosSteps, self.config)
        if self.pbSweep.isChecked():
            if actPosUnits > self.endPos or (
                actPosUnits < self.endPos and actPosUnits >= self.startPos
            ):
                self.position = self.endPos
            elif actPosUnits == self.endPos:
                self.position = self.startPos
            elif actPosUnits < self.startPos:
                self.position = self.startPos
            steps = motors.unitsToSteps(self.position, self.config)
            self.parent.motorCard2.move_to(self.motor, steps)
            self.motorTimerSweep.start(settings.value("interval"))
        else:
            try:
                self.motorTimerSweep.stop()
                self.parent.motorCard2.motors[self.config["motorNumber"]].stop()
            except Exception as exc:
                print(exc)

    def waitMotorSweep(self):
        actPosSteps = self.parent.motorCard2.motors[self.config["motorNumber"]].actual_position
        actPosUnits = motors.stepsToUnits(actPosSteps, self.config)
        if actPosUnits == self.position:
            self.motorTimerSweep.stop()
            self.sweep()

    class MotorSignals(QtCore.QObject):
        """Signals for motor."""

        # General signals.
        startPos = QtCore.pyqtSignal(bool)
        endPos = QtCore.pyqtSignal(bool)

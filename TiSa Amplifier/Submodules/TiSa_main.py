from devices.base_main_window import LECOBaseMainWindow
from PyQt6 import QtWidgets, QtCore
from pyleco.utils.qt_listener import QtListener
from pyleco.utils import data_publisher
from pyleco.directors.director import Director
from pyleco.core.message import Message
from PyQt6.QtCore import pyqtSlot
from devices import ITR, motors
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110
from simple_pid import PID
import numpy as np
import pickle
import logging
from data import Settings

log = logging.getLogger("TiSaAmp")

try:
    from pyleco.json_utils.errors import JSONRPCError, ServerError
except ModuleNotFoundError:
    from jsonrpcobjects.errors import JSONRPCError, ServerError

try:
    from Submodules.TiSa_autocorrelator import Autocorrelator
except Exception as exc:
    log.error("Autocorrelator submodule has errors and can not be imported.")
    log.info(exc)
try:
    from Submodules.TiSa_nirSpectrometer import NIRSpectrum
except Exception as exc:
    log.error("NIR-spectrometer submodule has errors and can not be imported.")
    log.info(exc)
try:
    from Submodules.TiSa_frog import FROG
except Exception as exc:
    log.error("FROG submodule has errors and can not be imported.")
    log.info(exc)
try:
    from Submodules.TiSa_opa1 import OPA1
except Exception as exc:
    log.error(f"OPA1 submodule has errors and can not be imported: {exc}")
    log.info(exc)


class TiSaAmp(LECOBaseMainWindow):
    """Define the main window and essential methods of the program."""

    # %%% Objects in GUI
    gbRegen: QtWidgets.QGroupBox
    lbRegenActAng: QtWidgets.QLabel
    sbRegenSetAng: QtWidgets.QDoubleSpinBox
    pbRegenSetAng: QtWidgets.QPushButton
    pbCalibrationRegenHWP: QtWidgets.QPushButton
    pbStabilizeRegen: QtWidgets.QPushButton
    lbMaxValRegen: QtWidgets.QLabel
    sbStabilizeRegen: QtWidgets.QDoubleSpinBox

    gbPowerAmp: QtWidgets.QGroupBox
    lbPowerAmpActAng: QtWidgets.QLabel
    sbPowerAmpSetAng: QtWidgets.QDoubleSpinBox
    pbPowerAmpSetAng: QtWidgets.QPushButton

    gbStrecher: QtWidgets.QGroupBox
    lbCBGActAng: QtWidgets.QLabel
    sbCBGSetAngle: QtWidgets.QDoubleSpinBox
    pbCBGSetAng: QtWidgets.QPushButton

    gbPulseAnalysis: QtWidgets.QGroupBox
    pbNIRSpectrum: QtWidgets.QPushButton
    pbAutocorrelator: QtWidgets.QPushButton
    pbFROG: QtWidgets.QPushButton
    pbOPA1Adjustment: QtWidgets.QPushButton

    gbCompressor: QtWidgets.QGroupBox
    cbGrating1: QtWidgets.QCheckBox
    cbGrating2: QtWidgets.QCheckBox
    gbFirstGrating: QtWidgets.QGroupBox
    gbSecondGrating: QtWidgets.QGroupBox
    lbGrating1ActAng: QtWidgets.QLabel
    sbGrating1SetAng: QtWidgets.QSpinBox
    pbSetAngleGrating1: QtWidgets.QPushButton
    lbGrating2ActAng: QtWidgets.QLabel
    sbGrating2SetAng: QtWidgets.QSpinBox
    pbSetAngleGrating2: QtWidgets.QPushButton

    gbShutter: QtWidgets.QGroupBox
    slFundShutter: QtWidgets.QSlider
    slSHGShutter: QtWidgets.QSlider
    lbFundShutter: QtWidgets.QLabel
    lbSHGShutter: QtWidgets.QLabel

    gbExperimentWavelength: QtWidgets.QGroupBox
    cbOPO_SHG: QtWidgets.QCheckBox
    cbOPO_800: QtWidgets.QCheckBox
    cbOPO_400: QtWidgets.QCheckBox
    cbIdler_SHG: QtWidgets.QCheckBox
    cbIdler_800: QtWidgets.QCheckBox
    cbIdler_400: QtWidgets.QCheckBox

    gbSeedShutter: QtWidgets.QGroupBox
    lbSeedShutter: QtWidgets.QLabel
    pbSeedShutterClose: QtWidgets.QPushButton
    pbSeedShutterGet: QtWidgets.QPushButton
    pbSeedShutterOpen: QtWidgets.QPushButton

    gbPressure: QtWidgets.QGroupBox
    cbReadPressure: QtWidgets.QCheckBox
    lbPressure: QtWidgets.QLabel

    gbShutterMira: QtWidgets.QGroupBox
    pbOpen: QtWidgets.QPushButton
    pbClose: QtWidgets.QPushButton

    gbHWPMira: QtWidgets.QGroupBox
    lbMiraActAng: QtWidgets.QLabel
    sbHWPMira: QtWidgets.QDoubleSpinBox
    pbSetPowerMira: QtWidgets.QPushButton

    gbPDMira: QtWidgets.QGroupBox
    lbMira: QtWidgets.QLabel
    lbOPO: QtWidgets.QLabel

    gbStabilizeMira: QtWidgets.QGroupBox
    pbCalibrationMiraHWP: QtWidgets.QPushButton
    pbStabilizeMira: QtWidgets.QPushButton
    lbMaxValMira: QtWidgets.QLabel
    sbStabilizeMira: QtWidgets.QDoubleSpinBox

    actionConnect: QtWidgets.QWidgetAction
    actionSettings: QtWidgets.QWidgetAction
    actionClose: QtWidgets.QWidgetAction

    listener: QtListener

    def __init__(self, name="TiSaAmp", *args, **kwargs):
        """Initialize the main window."""
        super().__init__(name=name, ui_file_name="TiSaAmp",
                         **kwargs)

        self.setSettings()

        # Data Acquisition
        self.publisher = data_publisher.DataPublisher(
            full_name="TiSaPublisher",
            port=QtCore.QSettings().value("publisherPort", type=int)
        )
        self.listener.signals.dataReady.connect(self.handle_data)
        self.communicator.subscribe(['PD532Regen_cal', 'PD532Amp_cal'])

        self.seedShutter = "pico3.seedShutter"
        self.YAGseed = "pico3.innolas"
        self.YAGshutter = "pico3.yagShutter"

        self.closed_mark = '\u2716'  # heavy multiplication x
        self.open_mark = "\u2b58"  # heavy circle
        self.enforced_close_mark = "\u25b2"  # black triangle

        # Standard readout timer
        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readData)
        # Timer for moving the gratings of compressor negative
        self.grating1Timer = QtCore.QTimer()
        self.grating1Timer.timeout.connect(self.moveGrating1Neg)
        self.grating2Timer = QtCore.QTimer()
        self.grating2Timer.timeout.connect(self.moveGrating2Neg)
        # Timer to control the motor movement in the calibration process for power stabilization
        self.calibrationTimer = QtCore.QTimer()
        self.calibrationTimer.timeout.connect(self.HWPCalibrationCalc)
        self.calibrationTimerZero = QtCore.QTimer()
        self.calibrationTimerZero.timeout.connect(self.HWPCalibrationZero)

        # Initialize program
        self.autostartConnect()

        # Initialize Shutter communication
        self.director = Director(communicator=self.communicator)

        # Connect actions to slots.
        self.actionConnect.triggered.connect(self.connectMotors)

        # Connect buttons to slots
        self.pbRegenSetAng.clicked.connect(self.SetRegenAng)
        self.pbPowerAmpSetAng.clicked.connect(self.SetPowerAmpAng)
        self.pbSetPowerMira.clicked.connect(self.setPowerMira)
        self.pbOpen.clicked.connect(self.openShutter)
        self.pbClose.clicked.connect(self.closeShutter)
        self.pbFROG.clicked.connect(self.openFROG)
        self.pbNIRSpectrum.clicked.connect(self.openNIRSpectrum)
        self.pbAutocorrelator.clicked.connect(self.openAutocorrelator)
        self.pbStabilizeMira.clicked.connect(self.setupPIDMira)
        self.pbStabilizeRegen.clicked.connect(self.setupPIDRegen)
        self.pbSetAngleGrating1.clicked.connect(self.adjustGrating1)
        self.pbSetAngleGrating2.clicked.connect(self.adjustGrating2)
        self.pbCalibrationRegenHWP.clicked.connect(self.calibrationRegenHWP)
        self.pbCalibrationMiraHWP.clicked.connect(self.calibrationMiraHWP)
        self.pbSeedShutterClose.clicked.connect(self.closeSeedShutter)
        self.pbSeedShutterOpen.clicked.connect(self.openSeedShutter)
        self.pbSeedShutterGet.clicked.connect(self.getSeedShutter)
        self.slFundShutter.valueChanged.connect(self.setFundShutter)
        self.slSHGShutter.valueChanged.connect(self.setSHGShutter)
        self.pbOPA1Adjustment.clicked.connect(self.openOPA1)

        # Signals
        self.sbStabilizeMira.valueChanged.connect(self.stabilizationValueChanged)
        self.sbStabilizeRegen.valueChanged.connect(self.stabilizationValueChanged)
        self.cbReadPressure.stateChanged.connect(self.pressureReader)
        self.cbGrating1.stateChanged.connect(self.enableGrating1)
        self.cbGrating2.stateChanged.connect(self.enableGrating2)

        self.setup_laser_status()

    def setup_laser_status(self) -> None:
        """Configure the listener to return the laser status."""
        self.listener.register_rpc_method(self.get_parameters)
        self.listener.signals.message.connect(self.handle_message)

    def handle_message(self, message: Message):
        response = self.listener.message_handler.process_json_message(message)
        self.communicator.send_message(response)

    def get_parameters(self, parameters: list[str]) -> dict:
        """Get device properties from the list `properties`."""
        data = {}
        for key in parameters:
            if key == "mira":
                data[key] = self.dat.get("Mira_cal", 0) > 0.1
            elif key == "mira_power":
                if self.dat.get("Mira_cal", 0) > 0.1:
                    data[key] = self.dat.get("Mira_cal", 0)
                else:
                    data[key] = None
            elif key == "ApeOPO":
                data[key] = self.dat.get("OPO_cal", 0) > 0.1
            elif key == "regen":
                data["key"] = self.pumpRegen > 20
            elif key == "amp":
                data["key"] = self.pumpAmp > 20
            elif key == "OPA":
                data["key"] = self.pumpAmp > 20
            elif key == "wavelengthRange":
                if self.OPO_SHG:
                    data["key"] = "252-350 nm, up to 1 mJ, 2 ps, 20 Hz"
                elif self.OPO_800:
                    data["key"] = "309-376 nm, up to 2 mJ, 2 ps, 20 Hz"
                elif self.OPO_400:
                    data["key"] = "223-255 nm, up to 3 mJ, 2 ps, 20 Hz"
                elif self.Idler_SHG:
                    data["key"] = "467-961 nm, up to 1 mJ, 2 ps, 20 Hz"
                elif self.Idler_800:
                    data["key"] = "431-564 nm, up to 2 mJ, 2 ps, 20 Hz"
                elif self.Idler_400:
                    data["key"] = "280-331 nm, up to 2 mJ, 2 ps, 20 Hz"
                else:
                    data["key"] = "no experiment running"
        return data

    # %%% General functions

    @pyqtSlot(int)
    def pressureReader(self, state: int) -> None:
        """Read the ITR200 pressure. (now actually implemented in psExperiment)"""
        settings = QtCore.QSettings()
        if state == 2:
            try:
                self.ITRsensor = ITR.ITR(f"COM{settings.value('pressurePort')}")
                print("Connected ITR pressure reader.")
            except (IndexError, ConnectionError) as exc:
                self.cbReadPressure.setChecked(False)
                log.info("Connection to ITR failed.")
                print(exc)
        else:
            try:
                del self.ITRsensor
                log.info("Disconnected ITR pressure reader.")
            except Exception:
                pass

    def handle_data(self, data: dict) -> None:
        """Import the data of picoscope for calibration of pump PD."""
        try:
            self.pumpRegen = data['PD532Regen_cal']
            self.pumpAmp = data['PD532Amp_cal']
        except AttributeError:
            pass
        except KeyError:
            self.communicator.subscribe(['PD532Regen_cal', 'PD532Amp_cal'])

    def message_received(self, message):
        return
        # log.info(message)

    def autostartConnect(self) -> None:
        """Autoconnect to devices when starting the program."""
        settings = QtCore.QSettings()

        # Connect Motors
        try:
            self.actionConnect.setChecked(True)
            self.connectMotors(True)
            try:
                self.lbMaxValMira.setText(str(round(settings.value("CalibrationMira")[1][1], 4)))
            except (ValueError, TypeError):
                print("No valid value for MaxValue for Mira.")
            try:
                self.lbMaxValRegen.setText(str(round(settings.value("CalibrationRegen")[1][1], 4)))
            except (ValueError, TypeError):
                print("No valid value for MaxValue for Regen.")
            self.stabilizeMira = False
            self.stabilizeRegen = False
            # Initialize Shutter
            value_open = 0
            value_close = 10
            config = settings.value("ShutterMira", type=dict)
            motor = config["motorNumber"]
            if round(self.motorCard1.motors[motor].actual_position, 2) == round(
                motors.unitsToSteps(value_open, config), 2
            ):
                self.pbOpen.setChecked(True)
                self.pbOpen.setStyleSheet("background-color: green")
            elif round(self.motorCard1.motors[motor].actual_position, 2) == round(
                motors.unitsToSteps(value_close, config), 2
            ):
                self.pbClose.setChecked(True)
                self.pbClose.setStyleSheet("background-color: red")
            else:
                print(round(self.motorCard1.motors[motor].actual_position, 2))
        except (ConnectionError, AttributeError) as exc:
            log.exception("Motor Connection failed!", exc)
            self.actionConnect.setChecked(False)

    @pyqtSlot()
    def closeEvent(self, event: QtCore.QEvent) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        try:
            self.readoutTimer.stop()
            self.connectionManager1.disconnect()
            self.connectionManager2.disconnect()
            self.connectionManager3.disconnect()
        except Exception:
            pass
        self.DAQcheck = False
        # Store angle of stretcher / compressor gratings in settings for the case of a blackout
        try:
            settings = QtCore.QSettings()
            settings.setValue("CBGangle", float(self.lbCBGActAng.text()))
            settings.setValue("Grating1angle", float(self.lbGrating1ActAng.text()))
            settings.setValue("Grating2angle", float(self.lbGrating2ActAng.text()))
        except ValueError:
            pass
        self.stop_listen()
        event.accept()

    @pyqtSlot()
    def openSettings(self) -> None:
        """Open the settings dialogue and apply changed settings."""
        self.settingsDialog = Settings.Settings()
        self.settingsDialog.accepted.connect(self.setSettings)
        self.settingsDialog.signals.motorConfigured.connect(self.configureMotor)
        self.settingsDialog.open()

    @pyqtSlot(bool)
    def connectMotors(self, checked: bool) -> None:
        """(Dis)connect the motor card."""
        settings = QtCore.QSettings()
        if checked:
            # self.arduino = arduino.Arduino("ASRL" + settings.value("arduinoPort"))
            # connectionTest = self.arduino.ping()
            # if connectionTest != "pong":
            #     log.error("Connection to Arduino failed!")
            COM1 = motors.getPort("Mira")
            COM2 = motors.getPort("Regen")
            # COM3 = motors.getPort("FROG")
            try:
                self.connectionManager1 = ConnectionManager(f"--port COM{COM1}")
                self.motorCard1 = TMCM6110(self.connectionManager1.connect())
                self.connectionManager2 = ConnectionManager(f"--port COM{COM2}")
                self.motorCard2 = TMCM6110(self.connectionManager2.connect())
                # self.connectionManager3 = ConnectionManager(f"--port COM{COM3}")
                # self.motorCard3 = TMCM6110(self.connectionManager3.connect())
                log.info("Motorcards successfully connected.")
            except Exception as exc:
                log.exception("Connection failed.", exc)
            else:
                self.configureMotor("ShutterMira", 1)
                self.configureMotor("HWPMira", 2)
                self.configureMotor("CBGRegen", 2)
                self.configureMotor("HWPRegen", 2)
                self.configureMotor("HWPPowerAmp", 1)
                self.configureMotor("Grating1PowerAmp", 1)
                self.configureMotor("Grating2PowerAmp", 1)
                self.configureMotor("Autokorrelator", 2)
                self.configureMotor("NIRSpec", 2)
                self.readoutTimer.start(settings.value("interval"))

        else:
            try:
                self.connectionManager1.disconnect()
                self.connectionManager2.disconnect()
                self.readoutTimer.stop()
            except AttributeError:
                pass  # Not existent
            except Exception as exc:
                print(exc)
            try:
                del self.motorCard1
            except AttributeError:
                pass
            try:
                del self.motorCard2
            except AttributeError:
                pass
            # try:
            #     del self.motorCard3
            # except AttributeError:
            #    pass
            self.actionConnect.setChecked(False)

    @pyqtSlot(str, int)
    def configureMotor(self, motorName: str, COM: int) -> None:
        """Configure the motor `motorname`."""
        # Load the config for the motor and configure the motor.
        settings = QtCore.QSettings()
        config = settings.value(motorName, type=dict)
        try:
            if COM == 1:
                motors.configureMotor(self.motorCard1, config)
            elif COM == 2:
                motors.configureMotor(self.motorCard2, config)
            else:
                log.exception("COM-Port not available!")
        except KeyError:
            log.exception("Motor not defined.")
        except Exception as exc:
            log.exception("Error configuring motor.", exc)

    @pyqtSlot()
    def readData(self) -> None:
        settings = QtCore.QSettings()
        try:
            self.dat = {}
            # Regen Data
            config = settings.value("HWPRegen", type=dict)
            step = self.motorCard2.motors[config["motorNumber"]].actual_position
            pos = motors.stepsToUnits(step, config)
            self.lbRegenActAng.setText(str(round(pos, 4)))
            self.dat["HWPRegen"] = round(pos, 4)

            # PowerAmp Data
            config = settings.value("HWPPowerAmp", type=dict)
            step = self.motorCard1.motors[config["motorNumber"]].actual_position
            pos = motors.stepsToUnits(step, config)
            self.lbPowerAmpActAng.setText(str(round(pos, 4)))

            # Mira Data
            config = settings.value("HWPMira", type=dict)
            step = self.motorCard2.motors[config["motorNumber"]].actual_position
            pos = motors.stepsToUnits(step, config)
            self.lbMiraActAng.setText(str(round(pos, 4)))
            self.dat["HWPMira"] = round(pos, 4)
            self.dat["Mira"] = self.motorCard1.get_analog_input(0)
            self.dat["OPO"] = self.motorCard1.get_analog_input(4)
            self.dat["Mira_cal"] = round(
                float(settings.value("calMiram")) * self.dat["Mira"]
                + float(settings.value("calMirab")),
                4,
            )
            self.dat["OPO_cal"] = round(
                float(settings.value("calOPOm")) * self.dat["OPO"]
                + float(settings.value("calOPOb")),
                4,
            )
            self.lbMira.setText(str(self.dat["Mira_cal"]))
            self.lbOPO.setText(str(self.dat["OPO_cal"]))

            # Stretcher Data
            config = settings.value("CBGRegen", type=dict)
            step = self.motorCard2.motors[config["motorNumber"]].actual_position
            pos = motors.stepsToUnits(step, config)
            self.lbCBGActAng.setText(str(round(pos, 4)))

            # Compressor Data
            config = settings.value("Grating1PowerAmp", type=dict)
            step = self.motorCard1.motors[config["motorNumber"]].actual_position
            self.lbGrating1ActAng.setText(str(step))
            config = settings.value("Grating2PowerAmp", type=dict)
            step = self.motorCard1.motors[config["motorNumber"]].actual_position
            self.lbGrating2ActAng.setText(str(step))

            # Read pressure
            if self.cbReadPressure.isChecked():
                try:
                    pressure = self.ITRsensor.pressure
                    self.dat["pressure"] = pressure
                    self.lbPressure.setText(f"{'%.2e' % pressure} mbar")
                    log.info("Pressure reading successful.")
                except Exception as exc:
                    self.lbPressure.setText("nan")
                    log.info("No values from pressure reader.")
                    log.exception(exc)

            # Shutter state
            '''
            try:
                self.getYAGShutterState()
            except TimeoutError:
                pass
            '''

            # Experiment wavelengths
            self.OPO_SHG = self.cbOPO_SHG.isChecked()
            self.OPO_800 = self.cbOPO_800.isChecked()
            self.OPO_400 = self.cbOPO_400.isChecked()
            self.Idler_SHG = self.cbIdler_SHG.isChecked()
            self.Idler_800 = self.cbIdler_800.isChecked()
            self.Idler_400 = self.cbIdler_400.isChecked()

            self.sendData(self.dat)
            # Stabilization
            if self.pbStabilizeMira.isChecked():
                config = settings.value("HWPMira", type=dict)
                output = self.pidMira(self.dat["OPO_cal"])
                motor = config["motorNumber"]
                self.motorCard2.move_to(motor, int(output))
                if self.dat["OPO_cal"] >= 1.9:
                    self.closeShutter()
                    log.warning("Pulse energy threshold of 1.84 W exceeded for OPO pump "
                                "stabilization. Shutter has been closed and the energy"
                                " for the OPO is reduced.")
                    self.motorCard2.move_to(motor, motors.unitsToSteps(85, config))
                    self.pbStabilizeMira.setChecked(False)
                elif motors.stepsToUnits(self.motorCard2.motors[motor].actual_position,
                                         config) == 15:
                    self.pbStabilizeMira.setChecked(False)
                    log.info("OPO stabilization stopped due to closed shutter.")
            if self.pbStabilizeRegen.isChecked():
                config = settings.value("HWPRegen", type=dict)
                output = self.pidRegen(self.pumpRegen)
                motor = config["motorNumber"]
                self.motorCard2.move_to(motor, int(output))
                if self.pumpRegen >= 54:
                    self.setSHGShutter(-1)
                    log.warning("Pulse energy threshold of 54 mJ exceeded for Regen pump"
                                " stabilization. Shutter has been closed and the energy for the"
                                " Regen pump is minimized.")
                    self.motorCard2.move_to(motor,
                                            motors.unitsToSteps(
                                                settings.value("CalibrationRegen")[0][0], config))
                    self.pbStabilizeRegen.setChecked(False)
                elif self.YAGstatus < 1:
                    self.pbStabilizeRegen.setChecked(False)

        except Exception as exc:
            log.exception("Readout failed!", exc)

    def sendData(self, values):
        try:
            self.publisher.send_legacy(values)
        except Exception as exc:
            log.exception("Sending data failed!", exc)

    # %%% Shutter control

    @pyqtSlot()
    def openSeedShutter(self):
        self._open_seed_YAG_seed_shutter(True)

    @pyqtSlot()
    def closeSeedShutter(self):
        self._open_seed_YAG_seed_shutter(False)

    def _open_seed_YAG_seed_shutter(self, state: bool) -> None:
        """Open or close the YAG seed shutter."""
        try:
            self.director.set_parameters({'shutter_open': bool(state)}, actor=self.seedShutter)
        except Exception as exc:
            self.statusBar().showMessage(f"Setting seed shutter failed: {exc}")
        else:
            self.getSeedShutter()

    @pyqtSlot()
    def getSeedShutter(self) -> None:
        """Get the state of the YAG seed shutter."""
        try:
            result = self.director.get_parameters(actor=self.seedShutter,
                                                  parameters=["shutter_open"])
        except TimeoutError:
            # self.showInformation("Seed shutter timeout",
            #                      "Timeout while getting Quanta-Ray seed shutter state.")
            self.lbSeedShutter.setText("-")
        except JSONRPCError as exc:
            # self.showInformation("Seed shutter error",
            #                      f"Error while getting Quanta-Ray seed shutter state: {exc}.")
            self.lbSeedShutter.setText("-")
            self.statusBar().showMessage(f"Gettin seed shutter failed: {exc}")
        else:
            try:
                state = result["shutter_open"]
            except Exception as exc:
                log.exception(f"Shutter state data exception: {result}", exc_info=exc)
            else:
                mapping = {True: self.open_mark, False: self.closed_mark, "Unknown": "?"}
                color_mapping = {True: "orange", False: "green"}
                self.lbSeedShutter.setStyleSheet(
                    f"color: {color_mapping.get(state, 'red')}; font-weight: bold")
                self.lbSeedShutter.setText(mapping.get(state, "-"))

    @pyqtSlot()
    def getYAGShutterState(self) -> None:
        """Get the state of the YAG beamdump shutters."""
        mapping = {-1: self.enforced_close_mark, 0: self.closed_mark, 1: self.open_mark}
        colors = {self.enforced_close_mark: "red", self.closed_mark: "green",
                  self.open_mark: "orange", "?": "black"}
        try:
            result = self.director.get_parameters(actor=self.YAGshutter,
                                                  parameters=["shg_open", "fundamental_open"])
            self.YAGstatus = result
        except Exception as exc:
            log.exception("Reading YAG shutter state failed.", exc_info=exc)
            for label in (self.lbFundShutter, self.lbSHGShutter):
                label.setText("-")
                label.setStyleSheet("color: black")
        else:
            shg = mapping.get(result.get("shg_open", -1000), "?")
            fund = mapping.get(result.get("fundamental_open", -1000), "?")
            self.lbFundShutter.setText(fund)
            self.lbSHGShutter.setText(shg)
            for value, label in ((fund, self.lbFundShutter), (shg, self.lbSHGShutter)):
                label.setStyleSheet(f"color: {colors.get(value, 'black')}; font-weight: bold")

    @pyqtSlot(int)
    def setSHGShutter(self, value: int) -> None:
        self.director.set_parameters({"shg_switch": value}, actor=self.YAGshutter)

    def setFundShutter(self, value: int) -> None:
        """Set the YAG fundamental shutter switch to a state: 1 open, 0 neutral, -1 emergency."""
        self.director.set_parameters({"fundamental_switch": value}, actor=self.YAGshutter)

    # %%% PID setup

    @pyqtSlot()
    def calibrationRegenHWP(self):
        """Make a new calibration for the Regen & PowerAmp pump stabilization."""
        settings = QtCore.QSettings()
        self.config = settings.value("HWPRegen", type=dict)
        confirmation = QtWidgets.QMessageBox()
        confirmation.setIcon(QtWidgets.QMessageBox.Icon.Question)
        confirmation.setWindowTitle("Start calibration for Regen HWP?")
        confirmation.setStandardButtons(
            QtWidgets.QMessageBox.StandardButton.Yes | QtWidgets.QMessageBox.StandardButton.Cancel)
        confirmation.setText("Do you want to start the calibration for the Regen HWP?")
        if confirmation.exec() != QtWidgets.QMessageBox.StandardButton.Yes:
            return
        try:
            self.dataLoggerConfig = self.communicator.ask_rpc("DataLogger",
                                                              method="getConfiguration")
            self.dataLoggerConfig["variables"] += " HWPRegen"
            self.communicator.ask_rpc("DataLogger", method="setConfiguration",
                                      configuration=self.dataLoggerConfig)
            self.startVal = motors.stepsToUnits(
                self.motorCard2.motors[self.config["motorNumber"]].actual_position,
                )
            self.motorCard2.move_to(self.config['motorNumber'], 0)
            self.lbCalibrationRegenStatus.setText("Move to start position.")
            self.calibrationName = "Regen"
            self.calibrationTimerZero.start(settings.value("interval"))
        except ServerError:
            log.error("Remote DataLogger can not be accessed.")

    @pyqtSlot()
    def calibrationMiraHWP(self):
        '''Make a new calibration for the Mira power stabilization.'''
        settings = QtCore.QSettings()
        self.config = settings.value("HWPMira", type=dict)
        confirmation = QtWidgets.QMessageBox()
        confirmation.setIcon(QtWidgets.QMessageBox.Icon.Question)
        confirmation.setWindowTitle("Start calibration for Mira HWP?")
        confirmation.setStandardButtons(
            QtWidgets.QMessageBox.StandardButton.Yes | QtWidgets.QMessageBox.StandardButton.Cancel)
        confirmation.setText("Do you want to start the calibration for the Mira HWP?")
        if confirmation.exec() != QtWidgets.QMessageBox.StandardButton.Yes:
            return
        self.dataLoggerConfig = self.communicator.ask_rpc("DataLogger", method="getConfiguration")
        self.dataLoggerConfig["variables"] += " HWPMira"
        self.communicator.ask_rpc("DataLogger", method="setConfiguration",
                                  configuration=self.dataLoggerConfig)
        pos = self.motorCard2.motors[self.config["motorNumber"]].actual_position
        self.startVal = motors.stepsToUnits(pos, self.config)
        self.motorCard2.move_to(self.config['motorNumber'], 0)
        print("Move to start position.")
        self.calibrationName = "Mira"
        self.calibrationTimerZero.start(settings.value("interval"))

    def HWPCalibrationZero(self):
        '''Move the motor to zero position to start the calibration'''
        settings = QtCore.QSettings()
        if self.calibrationName == "Regen":
            if self.motorCard2.motors[self.config["motorNumber"]].get_position_reached():
                self.lbCalibrationRegenStatus.setText("Start position reached.")
                print("Start position reached.")
                self.calibrationTimerZero.stop()
                self.communicator.ask_rpc("DataLogger", method="start_collecting", trigger="Mira")
                motor = self.config['motorNumber']
                self.motorCard2.move_to(motor, motors.unitsToSteps(90, self.config))
                self.calibrationTimer.start(settings.value("interval"))
            else:
                pass
        elif self.calibrationName == "Mira":
            if self.motorCard2.motors[self.config["motorNumber"]].get_position_reached():
                self.lbCalibrationMiraStatus.setText("Start position reached.")
                print("Start position reached.")
                self.calibrationTimerZero.stop()
                self.communicator.ask_rpc("DataLogger", method="start_collecting", trigger="Mira")
                motor = self.config['motorNumber']
                self.motorCard2.move_to(motor, motors.unitsToSteps(90, self.config))
                self.calibrationTimer.start(settings.value("interval"))
            else:
                pass
        else:
            self.calibrationTimerZero.stop()
            print("Unknown HWP to calibrate.")

    def HWPCalibrationCalc(self):
        '''Move the motor to the final position while collecting data for the motor position and
           the measured power. Afterwards fitting the data to get PID stabilization range.'''

        if self.calibrationName == "Regen":
            if self.motorCard2.motors[self.config["motorNumber"]].get_position_reached():
                self.lbCalibrationMiraStatus.setText("End position readched.")
                self.calibrationTimer.stop()
                dat = self.communicator.ask_rpc("DataLogger", method="save_data")
                with open("D:\\Measurement Data\\DataLogger\\" + dat + ".pkl", "rb") as file:
                    header, data, config = pickle.load(file)
                minimum = data['HWPRegen'][data['PD532Regen_cal'].index(
                    np.nanmin(data['PD532Regen_cal']))]
                maximum = data['HWPRegen'][data['PD532Regen_cal'].index(
                    np.nanmax(data['PD532Regen_cal']))]
                if maximum < minimum:
                    maximum += 90
                settings = QtCore.QSettings()
                settings.setValue("CalibrationRegen",
                                  [[minimum, np.nanmin(data['PD532Regen_cal'])],
                                   [maximum, np.nanmax(data['PD532Regen_cal'])]])
                self.lbMaxValRegen.setText(str(round(settings.value("CalibrationRegen")[1][1], 4)))
                self.dataLoggerConfig["variables"].removesuffix(" HWPRegen")
                self.communicator.ask_rpc("DataLogger", method="setConfiguration",
                                          configuration=self.dataLoggerConfig)
            else:
                pass
        elif self.calibrationName == "Mira":
            if self.motorCard2.motors[self.config["motorNumber"]].get_position_reached():
                self.lbCalibrationMiraStatus.setText("End position readched.")
                self.calibrationTimer.stop()
                dat = self.communicator.ask_rpc("DataLogger", method="save_data")
                with open("D:\\Measurement Data\\DataLogger\\" + dat + ".pkl", "rb") as file:
                    header, data, config = pickle.load(file)
                minimum = data['HWPMira'][data['OPO_cal'].index(np.nanmin(data['OPO_cal']))]
                maximum = data['HWPMira'][data['OPO_cal'].index(np.nanmax(data['OPO_cal']))]
                if maximum < minimum:
                    maximum += 90
                settings = QtCore.QSettings()
                settings.setValue("CalibrationMira", [[minimum, np.nanmin(data['OPO_cal'])],
                                                      [maximum, np.nanmax(data['OPO_cal'])]])
                self.lbMaxValMira.setText(str(round(settings.value("CalibrationMira")[1][1], 4)))
                self.dataLoggerConfig["variables"].removesuffix(" HWPMira")
                self.communicator.ask_rpc("DataLogger", method="setConfiguration",
                                          configuration=self.dataLoggerConfig)
            else:
                pass

    @pyqtSlot()
    def setupPIDMira(self):
        settings = QtCore.QSettings()
        config = settings.value("HWPMira", type=dict)
        if self.pbStabilizeMira.isChecked():
            self.pidMira = PID(auto_mode=False)
            self.pidMira.output_limits = (
                motors.unitsToSteps(settings.value("CalibrationMira")[0][0], config),
                motors.unitsToSteps(settings.value("CalibrationMira")[1][0], config),
            )
            self.pidMira.Kd = settings.value("D", defaultValue=0, type=float)
            self.pidMira.Kp = settings.value("P", defaultValue=0, type=float)
            self.pidMira.Ki = settings.value("I", defaultValue=0, type=float)
            self.pidMira.setpoint = self.sbStabilizeMira.value()
            self.pidMira.set_auto_mode(enabled=True, last_output=self.dat["OPO_cal"])
        else:
            self.pidMira.set_auto_mode(enabled=False)

    @pyqtSlot()
    def setupPIDRegen(self):
        settings = QtCore.QSettings()
        config = settings.value("HWPRegen", type=dict)
        if self.pbStabilizeRegen.isChecked():
            self.pidRegen = PID(auto_mode=False)
            self.pidRegen.output_limits = (
                motors.unitsToSteps(settings.value("CalibrationRegen")[0][0], config),
                motors.unitsToSteps(settings.value("CalibrationRegen")[1][0], config),
            )
            self.pidRegen.Kd = settings.value("D", defaultValue=0, type=float)
            self.pidRegen.Kp = settings.value("P", defaultValue=0, type=float)
            self.pidRegen.Ki = settings.value("I", defaultValue=0, type=float)
            self.pidRegen.setpoint = self.sbStabilizeRegen.value()
            self.pidRegen.set_auto_mode(enabled=True, last_output=self.pumpRegen)
        else:
            self.pidRegen.set_auto_mode(enabled=False)

    def stabilizationValueChanged(self):
        if self.pbStabilizeMira.isChecked():
            self.pidMira.setpoint = self.sbStabilizeMira.value()
        if self.pbStabilizeRegen.isChecked():
            self.pidRegen.setpoint = self.sbStabilizeRegen.value()

    # %%% Pump power amplifier

    @pyqtSlot()
    def SetRegenAng(self):
        settings = QtCore.QSettings()
        config = settings.value("HWPRegen", type=dict)
        steps = motors.unitsToSteps(self.sbRegenSetAng.value(), config)
        motor = config["motorNumber"]
        self.motorCard2.move_to(motor, steps)

    @pyqtSlot()
    def SetPowerAmpAng(self):
        settings = QtCore.QSettings()
        config = settings.value("HWPPowerAmp", type=dict)
        steps = motors.unitsToSteps(self.sbPowerAmpSetAng.value(), config)
        motor = config["motorNumber"]
        self.motorCard1.move_to(motor, steps)

    # %%% Mira

    @pyqtSlot()
    def openShutter(self):
        """Open the Shutter behind MIRA"""
        settings = QtCore.QSettings()
        value_open = 0
        config = settings.value("ShutterMira", type=dict)
        steps = motors.unitsToSteps(value_open, config)
        motor = config["motorNumber"]
        try:
            self.motorCard1.move_to(motor, steps)
            self.pbOpen.setStyleSheet("background-color: green")
            self.pbClose.setStyleSheet("background-color: grey")
            self.pbClose.setChecked(False)
        except ConnectionError:
            print("Motors not connected!")

    @pyqtSlot()
    def closeShutter(self):
        """Close Shutter behind MIRA"""
        self.pbStabilizeMira.setChecked(False)
        settings = QtCore.QSettings()
        value_close = 15
        config = settings.value("ShutterMira", type=dict)
        steps = motors.unitsToSteps(value_close, config)
        motor = config["motorNumber"]
        try:
            self.motorCard1.move_to(motor, steps)
            self.pbOpen.setStyleSheet("background-color: grey")
            self.pbClose.setStyleSheet("background-color: red")
            self.pbOpen.setChecked(False)
        except ConnectionError:
            print("Motors not connected!")

    @pyqtSlot()
    def setPowerMira(self):
        """Set the HWP angle"""
        settings = QtCore.QSettings()
        config = settings.value("HWPMira", type=dict)
        steps = motors.unitsToSteps(self.sbHWPMira.value(), config)
        motor = config["motorNumber"]
        self.motorCard2.move_to(motor, steps)

    # %%% Strecher Control
    # TODO: Implement function to set the CBG angle and the mirror angle.

    # %%% Compressor Control

    @pyqtSlot(int)
    def enableGrating1(self, state: int) -> None:
        if not state:
            self.gbFirstGrating.setEnabled(False)
        else:
            self.gbFirstGrating.setEnabled(True)

    @pyqtSlot(int)
    def enableGrating2(self, state: int) -> None:
        if not state:
            self.gbSeondGrating.setEnabled(False)
        else:
            self.gbSeondGrating.setEnabled(True)

    @pyqtSlot()
    def adjustGrating1(self):
        log.info("Adjust grating 1")
        settings = QtCore.QSettings()
        config = settings.value("Grating1PowerAmp", type=dict)
        motor = config["motorNumber"]
        if self.motorCard1.motors[motor].actual_position <= self.sbGrating1SetAng.value():
            try:
                log.info("Move grating 1 positive.")
                self.motorCard1.move_to(motor, self.sbGrating1SetAng.value())
            except ConnectionError:
                log.error("Motors not connected!")
        else:
            log.info(f'Move grating negative to position {self.sbGrating1SetAng.value() - 5000}.')
            self.motorCard1.move_to(motor, self.sbGrating1SetAng.value() - 5000)
            self.grating1Timer.start(100)

    def moveGrating1Neg(self):
        settings = QtCore.QSettings()
        config = settings.value("Grating1PowerAmp", type=dict)
        motor = config["motorNumber"]
        if self.motorCard1.motors[motor].get_position_reached():
            log.info("Grating 1 overshot position reached.")
            self.grating1Timer.stop()
            self.motorCard1.move_to(motor, self.sbGrating1SetAng.value())

    def adjustGrating2(self):
        settings = QtCore.QSettings()
        config = settings.value("Grating2PowerAmp", type=dict)
        motor = config["motorNumber"]
        if self.motorCard1.motors[motor].actual_position <= self.sbGrating2SetAng.value():
            try:
                self.motorCard1.move_to(motor, self.sbGrating2SetAng.value())
            except ConnectionError:
                log.error("Motors not connected!")
        else:
            self.motorCard1.move_to(motor, self.sbGrating2SetAng.value() - 5000)
            self.grating2Timer.start(100)

    def moveGrating2Neg(self):
        settings = QtCore.QSettings()
        config = settings.value("Grating2PowerAmp", type=dict)
        motor = config["motorNumber"]
        if self.motorCard1.motors[motor].get_position_reached():
            self.grating2Timer.stop()
            self.motorCard1.move_to(motor, self.sbGrating2SetAng.value())

    # %%% general spectrometer readout
    def spectrometerReadout(self, config: dict, motorcard: int, camType: str,
                            camData: float, calibration: dict):
        """Calculate the x- and y-data from the given camera data and the motor position of the
        grating with a calibration."""
        # TODO: Implement function.
        return

    def spectrometerCalibration(self, name: str):
        """Return a dictionary with all relevant calibration parameters to directly calculate the
        pixel values of the camera into a wavelength. The values for the calculation can be
        subtracted from the settings. The key of the settings has to be given with the name."""

        # TODO: Implement calculation that is valid for all spectrometers dependent on the name.

    # %%% Open sub-programs

    @pyqtSlot()
    def openAutocorrelator(self) -> None:
        """Open the autocorrelator program."""
        try:
            self.autocorrelator = Autocorrelator(self)
            self.autocorrelator.open()
        except AttributeError:
            log.error("Autocorrelator class not imported. Check for errors in code.")

    @pyqtSlot()
    def openNIRSpectrum(self) -> None:
        """Open the NIR spectrometer program."""
        try:
            self.NIRSpectrum = NIRSpectrum(self)
            self.NIRSpectrum.open()
        except AttributeError:
            log.error("NIR-spectrometer class not imported. Check for errors in code.")

    @pyqtSlot()
    def openFROG(self) -> None:
        """Open the FROG program."""
        try:
            self.frog = FROG(self)
            self.frog.open()
        except AttributeError:
            log.error("FROG class not imported. Check for errors in code.")

    @pyqtSlot()
    def openOPA1(self) -> None:
        """Open the OPA1 optimization program."""
        try:
            self.opa1 = OPA1(self)
            self.opa1.open()
        except AttributeError:
            log.error("OPA1 class not imported. Check for errors in code.")

import logging

import numpy as np
from pyleco.directors.starter_director import StarterDirector
from pyleco_extras.directors.tmc_motor_director import TMCMotorDirector
from PyQt6 import QtWidgets, QtCore, uic
from PyQt6.QtCore import pyqtSlot
import pyqtgraph as pg
from pylablib.devices.Thorlabs import KinesisPiezoMotor

import Submodules.TiSa_main as TiSa_main
log = logging.getLogger("TiSaAmp")


class OPA1(QtWidgets.QDialog):

    sbDelayStart1: QtWidgets.QDoubleSpinBox
    sbDelayStart2: QtWidgets.QDoubleSpinBox
    sbDelayStart3: QtWidgets.QDoubleSpinBox
    sbDelayEnd1: QtWidgets.QDoubleSpinBox
    sbDelayEnd2: QtWidgets.QDoubleSpinBox
    sbDelayEnd3: QtWidgets.QDoubleSpinBox
    sbPhaseStart1: QtWidgets.QDoubleSpinBox
    sbPhaseStart2: QtWidgets.QDoubleSpinBox
    sbPhaseStart3: QtWidgets.QDoubleSpinBox
    sbPhaseEnd1: QtWidgets.QDoubleSpinBox
    sbPhaseEnd2: QtWidgets.QDoubleSpinBox
    sbPhaseEnd3: QtWidgets.QDoubleSpinBox
    sbStepsizeDelay1: QtWidgets.QDoubleSpinBox
    sbStepsizeDelay2: QtWidgets.QDoubleSpinBox
    sbStepsizeDelay3: QtWidgets.QDoubleSpinBox
    sbStepsizePhase1: QtWidgets.QDoubleSpinBox
    sbStepsizePhase2: QtWidgets.QDoubleSpinBox
    sbStepsizePhase3: QtWidgets.QDoubleSpinBox
    sbSetDelay1: QtWidgets.QDoubleSpinBox
    sbSetDelay2: QtWidgets.QDoubleSpinBox
    sbSetDelay3: QtWidgets.QDoubleSpinBox
    sbSetPhase1: QtWidgets.QDoubleSpinBox
    sbSetPhase2: QtWidgets.QDoubleSpinBox
    sbSetPhase3: QtWidgets.QDoubleSpinBox
    sbMoveByPhase1: QtWidgets.QDoubleSpinBox
    sbMoveByPhase2: QtWidgets.QDoubleSpinBox
    sbMoveByPhase3: QtWidgets.QDoubleSpinBox
    pbMoveByPhase1: QtWidgets.QPushButton
    pbMoveByPhase2: QtWidgets.QPushButton
    pbMoveByPhase3: QtWidgets.QPushButton
    sbMoveByDelay1: QtWidgets.QDoubleSpinBox
    sbMoveByDelay2: QtWidgets.QDoubleSpinBox
    sbMoveByDelay3: QtWidgets.QDoubleSpinBox
    pbMoveByDelay1: QtWidgets.QPushButton
    pbMoveByDelay2: QtWidgets.QPushButton
    pbMoveByDelay3: QtWidgets.QPushButton
    pbStartSearching1: QtWidgets.QPushButton
    pbStopSearching1: QtWidgets.QPushButton
    pbStartSearching2: QtWidgets.QPushButton
    pbStopSearching2: QtWidgets.QPushButton
    pbStartSearching3: QtWidgets.QPushButton
    pbStopSearching3: QtWidgets.QPushButton
    pbSetDelay1: QtWidgets.QPushButton
    pbSetPhase1: QtWidgets.QPushButton
    pbSetDelay2: QtWidgets.QPushButton
    pbSetPhase2: QtWidgets.QPushButton
    pbSetDelay3: QtWidgets.QPushButton
    pbSetPhase3: QtWidgets.QPushButton
    pbMaximizeSignal1: QtWidgets.QPushButton
    pbMaximizeSignal2: QtWidgets.QPushButton
    pbMaximizeSignal3: QtWidgets.QPushButton
    lbActualDelay1: QtWidgets.QLabel
    lbActualPhase1: QtWidgets.QLabel
    lbActualDelay2: QtWidgets.QLabel
    lbActualPhase2: QtWidgets.QLabel
    lbActualDelay3: QtWidgets.QLabel
    lbActualPhase3: QtWidgets.QLabel

    sbDataPoints: QtWidgets.QSpinBox
    lbSignal: QtWidgets.QLabel
    cbSearchDelay: QtWidgets.QCheckBox
    cbSearchPhase: QtWidgets.QCheckBox
    plot: pg.PlotWidget
    cbMeasurementKey: QtWidgets.QComboBox

    def __init__(self, parent, *args, **kwargs):
        """Initialize the OPA1 control window."""
        super().__init__(parent=parent, *args, **kwargs)
        self.main_window: TiSa_main.TiSaAmp = parent

        uic.load_ui.loadUi("data/OPA1.ui", self)
        self.show()

        # Initialize window

        self.communicator = self.main_window.communicator
        self.configurePlot()
        self.communicator.subscribe([self.cbMeasurementKey.currentText()])
        self.listener = self.main_window.listener
        self.listener.signals.dataReady.connect(self.handle_data)
        self.publisher = self.main_window.publisher

        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readout)

        self.connectMotors()
        self.delayMax = False
        self.phaseMax = False

        self.pbStartSearching1.clicked.connect(lambda: self.startSearching(1))
        self.pbStopSearching1.clicked.connect(lambda: self.stopSearching(1))
        self.pbSetDelay1.clicked.connect(lambda: self.moveToDelay(1))
        self.pbSetPhase1.clicked.connect(lambda: self.moveToPhase(1))
        self.pbMoveByDelay1.clicked.connect(lambda: self.moveByDelay(1))
        self.pbMoveByPhase1.clicked.connect(lambda: self.moveByPhase(1))
        self.pbMaximizeSignal1.clicked.connect(lambda: self.maximizeSignal(1))

        self.pbStartSearching2.clicked.connect(lambda: self.startSearching(2))
        self.pbStopSearching2.clicked.connect(lambda: self.stopSearching(2))
        self.pbSetDelay2.clicked.connect(lambda: self.moveToDelay(2))
        self.pbSetPhase2.clicked.connect(lambda: self.moveToPhase(2))
        self.pbMoveByDelay2.clicked.connect(lambda: self.moveByDelay(2))
        self.pbMoveByPhase2.clicked.connect(lambda: self.moveByPhase(2))
        self.pbMaximizeSignal2.clicked.connect(lambda: self.maximizeSignal(2))

        self.pbStartSearching3.clicked.connect(lambda: self.startSearching(3))
        self.pbStopSearching3.clicked.connect(lambda: self.stopSearching(3))
        self.pbSetDelay3.clicked.connect(lambda: self.moveToDelay(3))
        self.pbSetPhase3.clicked.connect(lambda: self.moveToPhase(3))
        self.pbMoveByDelay3.clicked.connect(lambda: self.moveByDelay(3))
        self.pbMoveByPhase3.clicked.connect(lambda: self.moveByPhase(3))
        self.pbMaximizeSignal3.clicked.connect(lambda: self.maximizeSignal(3))

        self.cbSearchDelay.checkStateChanged.connect(self.delayCheckChanged)
        self.cbSearchPhase.checkStateChanged.connect(self.phaseCheckChanged)
        self.cbMeasurementKey.currentTextChanged.connect(self.changeMeasurementKey)

    def phaseUnitsToSteps(self, value: float) -> int:
        """Calculate phase angle value from units (°) to steps."""
        return int(np.radians(value) / 0.5e-6)

    def phaseStepsToUnits(self, value: int) -> float:
        """Calculate phase steps from angle value in °."""
        return np.degrees(value * 0.5e-6)

    @pyqtSlot(str)
    def changeMeasurementKey(self, key: str):
        """Change the key of the pulse energy measurement."""
        self.communicator.subscribe([key])
        for i in range(self.cbMeasurementKey.count()):
            if i != self.cbMeasurementKey.currentIndex():
                try:
                    self.communicator.unsubscribe([self.cbMeasurementKey.itemText(i)])
                except Exception:
                    pass

    @pyqtSlot()
    def closeEvent(self, event):
        self.readoutTimer.stop()
        try:
            self.continueTimer.stop()
        except AttributeError:
            pass
        self.phaseController.close()
        self.communicator.unsubscribe([self.cbMeasurementKey.currentText()])
        event.accept()

    @pyqtSlot(bool)
    def delayCheckChanged(self, checked: bool):
        if checked:
            self.cbSearchPhase.setChecked(False)
        else:
            self.cbSearchPhase.setChecked(True)

    @pyqtSlot(bool)
    def phaseCheckChanged(self, checked: bool):
        if checked:
            self.cbSearchDelay.setChecked(False)
        else:
            self.cbSearchDelay.setChecked(True)

    def connectMotors(self):
        """Connect the motors for all 3 delay stages."""
        settings = QtCore.QSettings()
        try:
            starter_director = StarterDirector("PICO2.starter", communicator=self.communicator)
            starter_director.start_tasks(["OPA1Delay_t", "Experiment_t"])

            self.motorCard1 = TMCMotorDirector("PICO2.OPA1Delay_a", 1,
                                               communicator=self.communicator)
            self.motorCard2 = TMCMotorDirector("PICO2.Experiment_a", communicator=self.communicator)
        except Exception as exc:
            log.error(f"Could not connect motorcards with the following error code: {exc}")
        try:
            self.phaseController = KinesisPiezoMotor("97250767")
        except Exception as exc:
            log.error(f"Could not connect Kinesis Driver with the following error code: {exc}")
        try:
            for chan in range(1, 4):
                self.phaseController.setup_drive(max_voltage=settings.value("KinesisMaxVoltage"),
                                                 velocity=settings.value("KinesisVelocity"),
                                                 acceleration=settings.value("KinesisAcceleration"),
                                                 channel=chan)
        except Exception as exc:
            log.error(f"Could not set Kinesis motor settings with the following error code: {exc}")

        self.readoutTimer.start(200)

    def configurePlot(self):
        """Prepare the plot widget for showing the data."""
        self.output = self.plot.plot([])
        self.plot.setLabel("bottom", "index")
        self.plot.setLabel("left", "signal in arb. units")
        self.data_pipe: list = []

    @pyqtSlot()
    def readout(self):
        """Readout of the actual motor positions."""
        try:
            delay1Units = self.motorCard1.get_actual_units("OPA1Delay_m")
            delay2Units = self.motorCard2.get_actual_units("OPA2Delay_m")
            delay3Units = self.motorCard2.get_actual_units("OPA3Delay_m")
            self.lbActualDelay1.setText(str(round(delay1Units, 2)))
            self.lbActualDelay2.setText(str(round(delay2Units, 2)))
            self.lbActualDelay3.setText(str(round(delay3Units, 2)))

            phase1Units = self.phaseStepsToUnits(self.phaseController.get_position(1))
            phase2Units = self.phaseStepsToUnits(self.phaseController.get_position(2))
            phase3Units = self.phaseStepsToUnits(self.phaseController.get_position(3))
            self.lbActualPhase1.setText(str(round(phase1Units, 2)))
            self.lbActualPhase2.setText(str(round(phase2Units, 2)))
            self.lbActualPhase3.setText(str(round(phase3Units, 2)))
            self.publisher.send_legacy({
                    "OPA1Delay": delay1Units, "OPA2Delay": delay2Units, "OPA3Delay": delay3Units,
                    "OPA1Phase": phase1Units, "OPA2Phase": phase2Units, "OPA3Phase": phase3Units
                 })
        except AttributeError:
            pass

    @pyqtSlot(dict)
    def handle_data(self, data: dict):
        try:
            key = self.cbMeasurementKey.currentText()
            self.datapoint: float = data[key]
            if (key == "Nova2" and self.datapoint < 3e-3) or (key != "Nova2"):
                if len(self.data_pipe) < 50000:
                    self.data_pipe.append(self.datapoint)
                else:
                    self.data_pipe.append(self.datapoint)
                    self.data_pipe.pop(0)
                self.lbSignal.setText(str(round(self.datapoint, 2)))
            else:
                pass
        except KeyError:
            pass
            # log.info(f"Wrong key in data: {data.keys()}")
        except AttributeError as exc:
            log.error(exc)
        else:
            length: int = len(self.data_pipe) - self.sbDataPoints.value()
            if length < 0:
                self.output.setData(self.data_pipe)
            else:
                self.output.setData(self.data_pipe[length:])

    def enableButtons(self, checked: bool):
        self.pbMaximizeSignal1.setEnabled(checked)
        self.pbSetDelay1.setEnabled(checked)
        self.pbSetPhase1.setEnabled(checked)
        self.pbStartSearching1.setEnabled(checked)
        self.pbMaximizeSignal2.setEnabled(checked)
        self.pbSetDelay2.setEnabled(checked)
        self.pbSetPhase2.setEnabled(checked)
        self.pbStartSearching2.setEnabled(checked)
        self.pbMaximizeSignal3.setEnabled(checked)
        self.pbSetDelay3.setEnabled(checked)
        self.pbSetPhase3.setEnabled(checked)
        self.pbStartSearching3.setEnabled(checked)

    @pyqtSlot(int)
    def startSearching(self, opaNumber: int):
        self.opaNumber = opaNumber
        try:
            settings = QtCore.QSettings()
            config1: dict = settings.value(f'OPA{opaNumber}Delay', type=dict)
            print(config1)
            # config2: dict = settings.value('OPA1Phase', type=dict)
            self.startingTimer = QtCore.QTimer()
            self.startingTimer.timeout.connect(self.searching)
            self.continueTimer = QtCore.QTimer()
            self.continueTimer.timeout.connect(self.continueSearching)
            self.stepTimer = QtCore.QTimer()
            self.stepTimer.timeout.connect(self.waitForStep)

            # disable buttons for OPA1, 2 and 3
            self.enableButtons(False)

            # create corresponding variables of currently used OPA stage
            sbDelayStart: QtWidgets.QDoubleSpinBox = getattr(self, f"sbDelayStart{opaNumber}")
            sbDelayEnd: QtWidgets.QDoubleSpinBox = getattr(self, f"sbDelayEnd{opaNumber}")
            sbStepsizeDelay: QtWidgets.QDoubleSpinBox = getattr(self, f"sbStepsizeDelay{opaNumber}")
            sbPhaseStart: QtWidgets.QDoubleSpinBox = getattr(self, f"sbPhaseStart{opaNumber}")
            sbPhaseEnd: QtWidgets.QDoubleSpinBox = getattr(self, f"sbPhaseEnd{opaNumber}")
            sbPhaseStep: QtWidgets.QDoubleSpinBox = getattr(self, f"sbStepsizePhase{opaNumber}")

            self.startPositionDelay = sbDelayStart.value()
            self.endPositionDelay = sbDelayEnd.value()
            self.delaySteps = sbStepsizeDelay.value()
            self.startPositionPhase = self.phaseUnitsToSteps(sbPhaseStart.value())
            self.endPositionPhase = self.phaseUnitsToSteps(sbPhaseEnd.value())
            self.phaseSteps = self.phaseUnitsToSteps(sbPhaseStep.value())
            print(sbPhaseStart.value(), sbPhaseEnd.value())
            log.info(f"Phase: {(self.startPositionPhase, self.endPositionPhase, self.phaseSteps)}\n"
                     f"Delay: {(self.startPositionDelay, self.endPositionDelay, self.delaySteps)}")

            if opaNumber == 1:
                speed = self.motorCard1.get_configuration("OPA1Delay_m")["positioningSpeed"]
                self.motorCard1.move_to_units("OPA1Delay_m", self.startPositionDelay, speed)
                self.phaseController.move_to(self.startPositionPhase, channel=1)
            else:
                speed = self.motorCard2.get_configuration(f"OPA{opaNumber}Delay_m")["positioningSpeed"]
                self.motorCard2.move_to_units(f"OPA{opaNumber}Delay_m", self.startPositionDelay,
                                              speed)
                self.phaseController.move_to(self.startPositionPhase, channel=opaNumber)
            self.startingTimer.start(100)
        except Exception as exc:
            log.error(f'Could not start searching due to the following error: {exc}')

    def searching(self):
        opaNumber = self.opaNumber
        if opaNumber == 1:
            speed = self.motorCard1.get_configuration("OPA1Delay_m")["positioningSpeed"]
            if (self.motorCard1.get_position_reached("OPA1Delay_m") and not
                self.phaseController.is_moving(1)):
                self.startingTimer.stop()
                if self.cbSearchPhase.isChecked():
                    self.position = self.endPositionPhase
                    self.phaseController.move_to(self.position, channel=1)
                else:
                    self.position = self.endPositionDelay
                    self.motorCard1.move_to_units("OPA1Delay_m", self.position, 700)
                self.signal = np.nanmean(self.data_pipe[-100:])
                self.signal_fluc = np.nanstd(self.data_pipe[-100:])
                self.continueTimer.start(150)
        else:
            speed = self.motorCard2.get_configuration(f"OPA{opaNumber}Delay_m")["positioningSpeed"]
            if (self.motorCard2.get_position_reached(f"OPA{opaNumber}Delay_m")
                and not self.phaseController.is_moving(opaNumber)):
                self.startingTimer.stop()
                if self.cbSearchPhase.isChecked():
                    self.position = self.endPositionPhase
                    self.phaseController.move_to(self.position, channel=opaNumber)
                else:
                    self.position = self.endPositionDelay
                    self.motorCard2.move_to_units(f"OPA{opaNumber}Delay_m", self.position, speed)
                self.signal = np.nanmean(self.data_pipe[-100:])
                self.signal_fluc = np.nanstd(self.data_pipe[-100:])
                self.continueTimer.start(250)

    def continueSearching(self):
        opaNumber = self.opaNumber
        lbActualDelay: QtWidgets.QDoubleSpinBox = getattr(self, f"lbActualDelay{opaNumber}")
        lbActualPhase: QtWidgets.QDoubleSpinBox = getattr(self, f"lbActualPhase{opaNumber}")
        if np.nanmean(self.data_pipe[-10:]) > self.signal + 5 * self.signal_fluc:
            self.phaseController.stop(opaNumber)
            if opaNumber == 1:
                self.motorCard1.stop("OPA1Delay_m")
            else:
                self.motorCard2.stop(f"OPA{opaNumber}Delay_m")
            self.continueTimer.stop()
            log.info('Signal found! You can now adjust to maximum signal.')
        else:
            if self.cbSearchPhase.isChecked():
                if not self.phaseController.is_moving(opaNumber):
                    self.continueTimer.stop()
                    if float(lbActualDelay.text()) < self.endPositionDelay:
                        if opaNumber == 1:
                            self.motorCard1.move_by_units("OPA1Delay_m", self.delaySteps, 500)
                        else:
                            self.motorCard2.move_by_units(f"OPA{opaNumber}Delay_m", self.delaySteps,
                                                          500)
                        self.stepTimer.start(250)
                    else:
                        log.info('Could not find any signal.')
                        self.enableButtons(True)
            else:
                if (self.motorCard1.get_position_reached("OPA1Delay_m")
                   and self.motorCard2.get_position_reached(f"OPA{opaNumber}Delay_m")):
                    self.continueTimer.stop()
                    if self.phaseUnitsToSteps(float(lbActualPhase.text())) < self.endPositionPhase:
                        self.phaseController.move_by(self.phaseSteps, channel=opaNumber)
                        self.stepTimer.start(250)
                    else:
                        log.info('Could not find any signal.')
                        self.enableButtons(True)

    @pyqtSlot()
    def waitForStep(self):
        opaNumber = self.opaNumber
        if self.cbSearchPhase.isChecked():
            if (self.motorCard1.get_position_reached("OPA1Delay")
               and self.motorCard2.get_position_reached(f"OPA{opaNumber}Delay")):
                self.stepTimer.stop()
                if self.position == self.endPositionPhase:
                    self.position = self.startPositionPhase
                    self.phaseController.move_to(self.startPositionPhase, channel=opaNumber)
                else:
                    self.position = self.endPositionPhase
                    self.phaseController.move_to(self.endPositionPhase, channel=opaNumber)
                self.continueTimer.start(250)
        else:
            if not self.phaseController.is_moving(opaNumber):
                self.stepTimer.stop()
                if opaNumber == 1:
                    if self.position == self.endPositionDelay:
                        self.position = self.startPositionDelay
                    else:
                        self.position = self.endPositionDelay
                    self.motorCard1.move_to_units("OPA1Delay_m", self.position, 500)
                else:
                    if self.position == self.endPositionDelay:
                        self.position = self.startPositionDelay
                    else:
                        self.position = self.endPositionDelay
                    self.motorCard2.move_to_units(f"OPA{opaNumber}Delay_m", self.position, 500)
                self.continueTimer.start(250)

    @pyqtSlot(int)
    def stopSearching(self, opaNumber: int):
        """Stops all motors as well as the timer and enables all buttons."""
        self.motorCard1.stop("OPA1Delay_m")
        self.motorCard2.stop("OPA2Delay_m")
        self.motorCard2.stop("OPA3Delay_m")
        self.phaseController.stop(1)
        self.phaseController.stop(2)
        self.phaseController.stop(3)
        self.continueTimer.stop()
        # enable all buttons for OPA1, 2 and 3
        self.enableButtons(True)

    @pyqtSlot(int)
    def maximizeSignal(self, opaNumber: int):
        if not self.delayMax:
            self.enableButtons(False)
            self.opaNumber = opaNumber
            sbStepsizeDelay: QtWidgets.QDoubleSpinBox = getattr(self, f"sbStepsizeDelay{opaNumber}")
            self.delaySteps = sbStepsizeDelay.value()
            sbStepsizePhase: QtWidgets.QDoubleSpinBox = getattr(self, f"sbStepsizePhase{opaNumber}")
            self.phaseSteps = self.phaseUnitsToSteps(sbStepsizePhase.value())
            self.maxDelayTimer = QtCore.QTimer()
            self.maxDelayTimer.timeout.connect(self.maxDelay)
            self.signal = np.nanmean(self.data_pipe[-20:])
            if opaNumber == 1:
                self.motorCard1.move_by_units("OPA1Delay_m", self.delaySteps, 500)
            else:
                self.motorCard2.move_by_units(f"OPA{opaNumber}Delay_m", self.delaySteps, 500)
            self.maxDelayTimer.start(500)
        else:
            self.maxPhaseTimer = QtCore.QTimer()
            self.maxPhaseTimer.timeout.connect(self.maxPhase)
            self.maxPhaseTimer.start(500)

    def maxDelay(self):
        if self.opaNumber == 1:
            if self.motorCard1.get_position_reached("OPA1Delay_m"):
                if np.mean(self.data_pipe[-20:]) > self.signal:
                    self.signal = np.mean(self.data_pipe[-20:])
                    self.motorCard1.move_by_units("OPA1Delay_m", self.delaySteps, 500)
                else:
                    self.motorCard1.move_by_units("OPA1Delay_m", -self.delaySteps, 500)
                    log.info('Best delay position found.'
                             'Start searching for optimal phase matching.')
                    self.maxDelayTimer.stop()
                    self.signal = np.mean(self.data_pipe[-20:])
                    self.phaseController.move_by(self.phaseUnitsToSteps(self.phaseSteps), channel=1)
                    self.delayMax = True
                    self.maximizeSignal(self.opaNumber)
        else:
            if self.motorCard2.get_position_reached(f"OPA{self.opaNumber}Delay_m"):
                if np.mean(self.data_pipe[-20:]) > self.signal:
                    self.signal = np.mean(self.data_pipe[-20:])
                    self.motorCard2.move_by_units(f"OPA{self.opaNumber}Delay_m", self.delaySteps,
                                                  500)
                else:
                    self.motorCard2.move_by_units(f"OPA{self.opaNumber}Delay_m", -self.delaySteps,
                                                  500)
                    log.info('Best delay position found.'
                             'Start searching for optimal phase matching.')
                    self.maxDelayTimer.stop()
                    self.signal = np.mean(self.data_pipe[-20:])
                    self.phaseController.move_by(self.phaseUnitsToSteps(self.phaseSteps),
                                                 channel=self.opaNumber)
                    self.delayMax = True
                    self.maximizeSignal(self.opaNumber)

    def maxPhase(self):
        if not self.phaseController.is_moving(self.opaNumber):
            if np.mean(self.data_pipe[-20:]) > self.signal:
                self.signal = np.mean(self.data_pipe[-20:])
                self.phaseController.move_by(self.phaseUnitsToSteps(self.phaseSteps),
                                             channel=self.opaNumber)
            else:
                self.phaseController.move_by(-self.phaseUnitsToSteps(self.phaseSteps),
                                             channel=self.opaNumber)
                log.info('Best phase matching reached. Maximizing signal completed.')
                self.maxPhaseTimer.stop()
                self.phaseMax = False
                self.delayMax = False
                self.enableButtons(True)

    @pyqtSlot(int)
    def moveToDelay(self, opaNumber: int):
        sbSetDelay: QtWidgets.QDoubleSpinBox = getattr(self, f"sbSetDelay{opaNumber}")
        if opaNumber == 1:
            self.motorCard1.move_to_units("OPA1Delay_m", sbSetDelay.value(), 400)
        else:
            self.motorCard2.move_to_units(f"OPA{opaNumber}Delay_m", sbSetDelay.value(), 400)

    @pyqtSlot(int)
    def moveToPhase(self, opaNumber: int):
        sbSetPhase: QtWidgets.QDoubleSpinBox = getattr(self, f"sbSetPhase{opaNumber}")
        try:
            self.phaseController.move_to(self.phaseUnitsToSteps(sbSetPhase.value()), channel=opaNumber)
        except Exception as exc:
            log.error(f"Could not move phase motor with the following exception: {exc}")

    @pyqtSlot(int)
    def moveByDelay(self, opaNumber: int):
        sbMoveByDelay: QtWidgets.QDoubleSpinBox = getattr(self, f"sbMoveByDelay{opaNumber}")
        try:
            if opaNumber == 1:
                self.motorCard1.move_by_units("OPA1Delay_m", sbMoveByDelay.value(), 400)
            else:
                self.motorCard2.move_by_units(f"OPA{opaNumber}Delay_m", sbMoveByDelay.value(), 400)
        except Exception as exc:
            log.error(f"Could not move by delay motor with the following exception: {exc}")

    @pyqtSlot(int)
    def moveByPhase(self, opaNumber: int):
        sbMoveByPhase: QtWidgets.QDoubleSpinBox = getattr(self, f"sbMoveByPhase{opaNumber}")
        try:
            self.phaseController.move_by(self.phaseUnitsToSteps(sbMoveByPhase.value()), channel=opaNumber)
        except Exception as exc:
            log.error(f"Could not move phase motor with the following exception: {exc}")

"""
Defines own widgets with special arguments.

classes
--------
SpecialView
    A pyqtgraph ImageView with a PlotItem as view.


Created on Fri Feb 19 18:24:22 2021 by Benedikt Moneke
"""


import pyqtgraph


class SpecialView(pyqtgraph.ImageView):
    """A pyqtgraph imageview initialized with a plot item."""

    def __init__(self, *args, **kwargs):
        """Initialize it."""
        super().__init__(view=pyqtgraph.PlotItem())

# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 16:25:27 2023

@author: ps-admin
"""

import nidaqmx as ni
import numpy as np
import matplotlib.pyplot as plt
from devices import intercom
import pickle


def f(x, a, b, c):
    return a / (x - c) + b


fitParameter = [9.68158462e-01, -1.25842999e-02, -5.96124527e02]
publisher = intercom.Publisher(port=11100)
a = 0
x = True
rate = 100000
pulseForm = False
frame = []
dat = []
try:
    with ni.Task() as task:
        task.ai_channels.add_ai_voltage_chan(
            "myDAQ1/ai1",
            name_to_assign_to_channel="Regen",
        )
        task.timing.cfg_samp_clk_timing(
            rate,
            source="",
            active_edge=ni.constants.Edge.RISING,
            sample_mode=ni.constants.AcquisitionType.CONTINUOUS,
        )
        while x:
            a += 1
            data = task.read(number_of_samples_per_channel=int(rate * 0.05))
            # publisher.send({"Regen":sum(np.array(data)),
            #                "PD532Regen_cal": f(sum(np.array(data)), *fitParameter)})
            start = data.index(max(data)) - 10
            end = data.index(max(data)) + 120
            plt.plot(np.array(data[start:end]) / len(data[start:end]), ".")
            plt.show()
            dat.append(sum(np.array(data[start:end]) / len(data[start:end])))
            if pulseForm:
                frame.append(data)
                if a == 3000:
                    x = False
                    with open("D:\\Measurement Data\\DataLogger\\myDaqtest.pkl", "wb") as file:
                        file.write(pickle.dumps(frame))
except Exception as exc:
    print(exc)
    plt.plot(dat)

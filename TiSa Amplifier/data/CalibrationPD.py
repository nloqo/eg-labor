# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 09:51:57 2023

@author: ps-admin
"""

import pickle
import matplotlib.pyplot as plt
import pandas as pd
from scipy.optimize import curve_fit
import numpy as np


def lin(x, a, b, c):
    return a / (x - c) + b


with open("D:\\Measurement Data\\DataLogger\\2023_01_23T09_49_55.pkl", "rb") as file:
    header, data, config = pickle.load(file)

plt.plot(data["PD532Regen"], data["Nova2"], ".")
dat = pd.DataFrame({"PD532Regen": data["PD532Regen"], "Nova2": data["Nova2"]})
dat = dat.dropna()

x = np.linspace(-580, -520, 10000)
fitParameter, fitCovariance = curve_fit(lin, dat["PD532Regen"], dat["Nova2"], p0=[0.04, 0, -580])
print(fitParameter)
plt.plot(x, lin(x, *fitParameter))

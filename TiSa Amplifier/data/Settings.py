"""
Module for the settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from devices import motors


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    sbInterval = QtWidgets.QSpinBox
    sbPublisherPort = QtWidgets.QSpinBox

    sbArduinoPort = QtWidgets.QSpinBox
    sbAIRegen = QtWidgets.QSpinBox
    sbAIPowerAmp = QtWidgets.QSpinBox
    sbAIPumpPowerAmp = QtWidgets.QSpinBox
    sbAIPumpRegen = QtWidgets.QSpinBox
    sbAIAutocorrelator = QtWidgets.QSpinBox
    sbAICwMira = QtWidgets.QSpinBox

    sbMMira = QtWidgets.QDoubleSpinBox
    sbBMira = QtWidgets.QDoubleSpinBox
    sbMOPO = QtWidgets.QDoubleSpinBox
    sbBOPO = QtWidgets.QDoubleSpinBox
    sbM532Regen = QtWidgets.QDoubleSpinBox
    sbB532Regen = QtWidgets.QDoubleSpinBox
    sbM532PowerAmp = QtWidgets.QDoubleSpinBox
    sbB532PowerAmp = QtWidgets.QDoubleSpinBox
    sbMRegen = QtWidgets.QDoubleSpinBox
    sbBRegen = QtWidgets.QDoubleSpinBox
    sbMPowerAmp = QtWidgets.QDoubleSpinBox
    sbBPowerAmp = QtWidgets.QDoubleSpinBox

    sbKinesisMaxVoltage: QtWidgets.QSpinBox
    sbKinesisVelocity: QtWidgets.QSpinBox
    sbKinesisAcceleration: QtWidgets.QSpinBox

    sbI = QtWidgets.QSpinBox
    sbP = QtWidgets.QSpinBox
    sbD = QtWidgets.QSpinBox

    sbNIRAngleDelta = QtWidgets.QDoubleSpinBox
    sbNIRGratingConst = QtWidgets.QDoubleSpinBox
    sbPxSize = QtWidgets.QSpinBox
    sbPxHorizontal = QtWidgets.QSpinBox
    sbPxVertical = QtWidgets.QSpinBox
    sbFocalLength = QtWidgets.QSpinBox

    sbITR100port = QtWidgets.QSpinBox
    sbITR200port = QtWidgets.QSpinBox
    sbGratingVUV = QtWidgets.QDoubleSpinBox
    sbKVUV = QtWidgets.QDoubleSpinBox
    sbSlopeVUV = QtWidgets.QDoubleSpinBox
    sbYintersectVUV = QtWidgets.QDoubleSpinBox

    pbVUVGratingMotorSettings = QtWidgets.QPushButton
    pbShutterMira = QtWidgets.QPushButton
    pbHWPMira = QtWidgets.QPushButton
    pbCBGRegen = QtWidgets.QPushButton
    pbHWPRegen = QtWidgets.QPushButton
    pbHWPPowerAmp = QtWidgets.QPushButton
    pbGrating1PowerAmp = QtWidgets.QPushButton
    pbGrating2PowerAmp = QtWidgets.QPushButton
    pbAutokorrelator = QtWidgets.QPushButton
    pbNIRSpec = QtWidgets.QPushButton
    pbFROGstage = QtWidgets.QPushButton
    pbFROGgrating = QtWidgets.QPushButton

    def __init__(self):
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/Settings.ui", self)
        self.show()

        # Configure settings.
        self.signals = self.SettingsSignals()
        self.settings = QtCore.QSettings()

        # Convenience list for widgets with value(), SetValue() methods.
        self.sets = (
            (self.sbInterval, "interval", 0, int),
            (self.sbPublisherPort, "publisherPort", 11100, int),
            (self.sbAIRegen, "AIRegen", 0, int),
            (self.sbAIPowerAmp, "AIPowerAmp", 4, int),
            (self.sbAIPumpPowerAmp, "AIPumpPowerAmp", 3, int),
            (self.sbAIPumpRegen, "AIPumpRegen", 0, int),
            (self.sbAIAutocorrelator, "AIAutocorrelator", 1, int),
            (self.sbAICwMira, "AICwMira", 2, int),
            (self.sbMMira, "calMIRAm", 0, float),
            (self.sbBMira, "calMIRAb", 0, float),
            (self.sbMOPO, "calOPOm", 0, float),
            (self.sbBOPO, "calOPOb", 0, float),
            (self.sbM532Regen, "cal532Regenm", 0, float),
            (self.sbB532Regen, "cal532Regenb", 0, float),
            (self.sbM532PowerAmp, "cal532PowerAmpm", 0, float),
            (self.sbB532PowerAmp, "cal532PowerAmpb", 0, float),
            (self.sbMRegen, "calRegenm", 0, float),
            (self.sbBRegen, "calRegenb", 0, float),
            (self.sbMPowerAmp, "calPowerAmpm", 0, float),
            (self.sbBPowerAmp, "calPowerAmpb", 0, float),
            (self.sbI, "I", 0, float),
            (self.sbP, "P", 0, float),
            (self.sbD, "D", 0, float),
            (self.sbArduinoPort, "arduinoPort", 33, int),
            (self.sbNIRAngleDelta, "NIRAngleDelta", 11.20, float),
            (self.sbNIRGratingConst, "NIRGratingConst", 1800, float),
            (self.sbPxSize, "PxSize", 3750, int),
            (self.sbPxHorizontal, "PxHorizontal", 1280, int),
            (self.sbPxVertical, "PxVertical", 960, int),
            (self.sbFocalLength, "FocalLength", 500, int),
            (self.sbITR100port, "ITR100port", 53, int),
            (self.sbITR200port, "ITR200port", 54, int),
            (self.sbGratingVUV, "G", 1118.0, float),
            (self.sbKVUV, "K", 32.0, float),
            (self.sbSlopeVUV, "Slope", -2e-5, float),
            (self.sbYintersectVUV, "Yintersect", -112.0, float),
            (self.sbKinesisMaxVoltage, "KinesisMaxVoltage", 120, int),
            (self.sbKinesisVelocity, "KinesisVelocity", 2000, int),
            (self.sbKinesisAcceleration, "KinesisAcceleration", 1000, int)
        )
        self.textsets = (
            (
                self.lePathAutocorrelator,
                "pathAutocorrelator",
                "D:\\Measurement Data\\FastAutocorrelator\\",
                str,
            ),
            (self.lePathNIRSpectrum, "pathNIRSpectrum", "D:\\Measurement Data\\NIRSpectrometer_py\\", str),
            (self.lePathFROG, "pathFROG", "D:\\Measurement Data\\FROG\\", str),
        )
        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbShutterMira.clicked.connect(self.openShutterMiraSettings)
        self.pbHWPMira.clicked.connect(self.openHWPMiraSettings)
        self.pbCBGRegen.clicked.connect(self.openCBGRegenSettings)
        self.pbHWPRegen.clicked.connect(self.openHWPRegenSettings)
        self.pbHWPPowerAmp.clicked.connect(self.openHWPPowerAmpSettings)
        self.pbGrating1PowerAmp.clicked.connect(self.openGrating1PowerAmpSettings)
        self.pbGrating2PowerAmp.clicked.connect(self.openGrating2PowerAmpSettings)
        self.pbAutokorrelator.clicked.connect(self.openAutokorrelatorSettings)
        self.pbFROGgrating.clicked.connect(self.openFROGgratingSettings)
        self.pbFROGstage.clicked.connect(self.openFROGstageSettings)
        self.pbNIRSpec.clicked.connect(self.openNIRSpecSettings)
        self.pbVUVGratingMotorSettings.clicked.connect(self.openVUVGratingSettings)
        
        # self.pbOPA1Phase.clicked.connect(self.openOPA1PhaseSettings)
        self.pbRestoreDefaults = self.buttonBox.button(restore)
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings."""

        motorConfigured = QtCore.pyqtSignal(str, int)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        for setting in self.textsets:
            widget, name, value, typ = setting
            widget.setText(self.settings.value(name, defaultValue=value, type=typ))
        # TODO: read settings and write them to the field
        # self.Interval.setValue(self.settings.value("interval", defaultValue=5000, type=int))

    @pyqtSlot()
    def openShutterMiraSettings(self):
        motorSettings = motors.MotorSettings(key="ShutterMira", motorName="ShutterMira")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("ShutterMira", 1)

    @pyqtSlot()
    def openHWPMiraSettings(self):
        motorSettings = motors.MotorSettings(key="HWPMira", motorName="HWPMira")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("HWPMira", 2)

    @pyqtSlot()
    def openHWPRegenSettings(self):
        motorSettings = motors.MotorSettings(key="HWPRegen", motorName="HWPRegen")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("HWPRegen", 2)

    @pyqtSlot()
    def openCBGRegenSettings(self):
        motorSettings = motors.MotorSettings(key="CBGRegen", motorName="CBGRegen")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("CBGRegen", 2)

    @pyqtSlot()
    def openHWPPowerAmpSettings(self):
        motorSettings = motors.MotorSettings(key="HWPPowerAmp", motorName="HWPPowerAmp")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("HWPPowerAmp", 1)

    @pyqtSlot()
    def openGrating1PowerAmpSettings(self):
        motorSettings = motors.MotorSettings(key="Grating1PowerAmp", motorName="Grating1PowerAmp")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("Grating1PowerAmp", 1)

    @pyqtSlot()
    def openGrating2PowerAmpSettings(self):
        motorSettings = motors.MotorSettings(key="Grating2PowerAmp", motorName="Grating2PowerAmp")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("Grating2PowerAmp", 1)

    @pyqtSlot()
    def openAutokorrelatorSettings(self):
        motorSettings = motors.MotorSettings(key="Autocorrelator", motorName="Autocorrelator")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("Autocorrelator", 2)

    @pyqtSlot()
    def openFROGgratingSettings(self):
        motorSettings = motors.MotorSettings(key="FROGgrating", motorName="FROGgrating")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("FROGgrating", 3)

    @pyqtSlot()
    def openFROGstageSettings(self):
        motorSettings = motors.MotorSettings(key="FROGstage", motorName="FROGstage")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("FROGstage", 2)

    @pyqtSlot()
    def openNIRSpecSettings(self):
        motorSettings = motors.MotorSettings(key="NIRSpec", motorName="NIRSpec")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("NIRSpec", 2)

    @pyqtSlot()
    def openVUVGratingSettings(self):
        motorSettings = motors.MotorSettings(key="VUVGrating", motorName="VUVGrating")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("VUVGrating", 2)


    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)
        for setting in self.textsets:
            widget, name, value, typ = setting
            widget.setText(value)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        for setting in self.textsets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.text())
        # TODO: save the values from the fields into settings
        # self.settings.setValue('savePath', self.leSavePath.text())
        super().accept()  # make the normal accept things

    '''
    TEMPLATE: a possible file dialog
    def openFileDialog(self):
        """Open a file path dialog."""
        self.savePath = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
        self.leSavePath.setText(self.savePath)
    '''

# -*- coding: utf-8 -*-
"""
@author: Oskar Ernst

Main program for the amplifier stages, especially the Regen and PowerAmp. Control and stabilize the
Splitting of the Mira output between Regen and OPO, set and control the Regen and PowerAmp pump
energy, get access to the NIR spectometer, the fast autocorrelator and the FROG as well as the
stretcher and compressor motors.
"""

# Standard packages.
import logging
import sys
from PyQt6 import QtWidgets

from Submodules.TiSa_main import TiSaAmp

log = logging.getLogger("TiSaAmp")
log.addHandler(logging.StreamHandler())

if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:  # Verbose log.
        log.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = TiSaAmp(name="TiSaAmp")  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

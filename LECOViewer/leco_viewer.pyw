
from pyleco_extras.gui.leco_viewer.leco_viewer import LECOViewer, start_app


if __name__ == "__main__":  # pragma: no cover
    start_app(LECOViewer)

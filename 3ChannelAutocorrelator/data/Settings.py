"""
Module for the settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from devices import motors


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self):
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/Settings.ui", self)
        self.show()

        # Configure settings.
        self.signals = self.SettingsSignals()
        self.settings = QtCore.QSettings()

        # Convenience list for widgets with value(), SetValue() methods.
        self.sets = (
            # name of widget, key of setting, defaultValue, type of data
            # (self.widget, 'name', 0, int),
            (self.sbInterval, "interval", 0, int),
            (self.sbMiraPD, "MiraPD", 6, int),
            (self.sbIRPD, "IRPD", 4, int),
            (self.sbVISPD, "VISPD", 2, int),
            (self.sbMiraAuto, "MiraAuto", 7, int),
            (self.sbIRAuto, "IRAuto", 5, int),
            (self.sbVISAuto, "VISAuto", 3, int),
            (self.sbStartPos, "startPos", 0, float),
            (self.sbDistance, "distance", 5, float),
        )
        self.textsets = (
            (self.leDAQName, "DAQName", "Dev1", str),
            (self.lePath, "path", "D:\\Measurement Data\\3ChanAutocorrelator\\", str),
        )
        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        self.pbStageMotor.clicked.connect(self.openStageMotorSettings)
        self.pbBBOIR.clicked.connect(self.openBBOIRSettings)
        self.pbBBOVIS.clicked.connect(self.openBBOVISSettings)

        # TODO: example of a fileDialog
        # self.pbSavePath.clicked.connect(self.openFileDialog)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings."""

        motorConfigured = QtCore.pyqtSignal(str, int)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        for setting in self.textsets:
            widget, name, value, typ = setting
            widget.setText(self.settings.value(name, defaultValue=value, type=typ))
        # TODO: read settings and write them to the field
        # self.Interval.setValue(self.settings.value("interval", defaultValue=5000, type=int))

    @pyqtSlot()
    def openStageMotorSettings(self):
        motorSettings = motors.MotorSettings(key="Stage", motorName="Stage")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("Stage", 1)

    @pyqtSlot()
    def openBBOIRSettings(self):
        motorSettings = motors.MotorSettings(key="BBOIR", motorName="BBOIR")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("BBOIR", 2)

    @pyqtSlot()
    def openBBOVISSettings(self):
        motorSettings = motors.MotorSettings(key="BBOVIS", motorName="BBOVIS")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("BBOVIS", 2)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        for setting in self.textsets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.text())
        # TODO: save the values from the fields into settings
        # self.settings.setValue('savePath', self.leSavePath.text())
        super().accept()  # make the normal accept things

    '''
    TEMPLATE: a possible file dialog
    def openFileDialog(self):
        """Open a file path dialog."""
        self.savePath = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
        self.leSavePath.setText(self.savePath)
    '''

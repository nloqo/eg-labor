# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 13:32:17 2023

@author: ps-admin
"""
import nidaqmx as ni
import matplotlib.pyplot as plt
import numpy as np

x = 0
data = {"MiraPD": [], "IRPD": [], "VISPD": [], "MiraAuto": [], "IRAuto": [], "VISAuto": []}
while x < 100:
    with ni.Task() as task:
        task.ai_channels.add_ai_voltage_chan("Dev1/ai6", name_to_assign_to_channel="MiraPD")
        task.ai_channels.add_ai_voltage_chan("Dev1/ai4", name_to_assign_to_channel="IRPD")
        task.ai_channels.add_ai_voltage_chan("Dev1/ai2", name_to_assign_to_channel="VISPD")
        task.ai_channels.add_ai_voltage_chan("Dev1/ai7", name_to_assign_to_channel="MiraAuto")
        task.ai_channels.add_ai_voltage_chan("Dev1/ai5", name_to_assign_to_channel="IRAuto")
        task.ai_channels.add_ai_voltage_chan("Dev1/ai3", name_to_assign_to_channel="VISAuto")
        task.timing.cfg_samp_clk_timing(40000, source="", active_edge=ni.constants.Edge.RISING,
                                        sample_mode=ni.constants.AcquisitionType.CONTINUOUS)
        DAQ_data = task.read(number_of_samples_per_channel=800)
    # plt.plot(DAQ_data[0], color="green")
    # plt.plot(DAQ_data[1], color="red")
    # plt.plot(DAQ_data[2], color="blue")
    # plt.plot(DAQ_data[3], color="lightgreen")
    # plt.plot(DAQ_data[4], color="orange")
    # plt.plot(DAQ_data[5], color="purple")
    # plt.show()
    data["MiraPD"].append(-np.mean(DAQ_data[0]))
    data["IRPD"].append(-np.mean(DAQ_data[1]))
    data["VISPD"].append(-np.mean(DAQ_data[2]))
    data["MiraAuto"].append(-np.mean(DAQ_data[3]))
    data["IRAuto"].append(-np.mean(DAQ_data[4]))
    data["VISAuto"].append(-np.mean(DAQ_data[5]))
    x += 1

# plt.plot(data["MiraPD"], color="g")
# plt.plot(data["IRPD"], color="r")
# plt.plot(data["VISPD"], color="b")
plt.plot(data["MiraAuto"], color="lightgreen")
plt.plot(data["IRAuto"], color="orange")
plt.plot(data["VISAuto"], color="purple")

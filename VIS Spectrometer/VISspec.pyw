"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Burger
"""

# Standard packages.
import logging
import pathlib

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
import datetime
import json
import numpy as np
from scipy.optimize import curve_fit
from simple_pid import PID

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
)

from pyleco.utils import data_publisher
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110
import nidaqmx as ni
from devices import Thorlabs_LC100 as LC100
from devices import motors

# Local packages.
from data import settings
from data.LC100settings import LC100Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class VISspec(LECOBaseMainWindowDesigner):
    """
    Control program for the VIS spectrometer with stabilization and automatization of the OPO
    output wavelength.

    :param name: Name of this program.
    :param host: Host name of the Coordinator.
    """

    actionClose: QtWidgets.QWidgetAction
    actionSettings: QtWidgets.QWidgetAction
    actionConnect_Motor: QtWidgets.QWidgetAction
    actionConnect_LC100_2: QtWidgets.QWidgetAction
    actionLC100_Settings_2: QtWidgets.QWidgetAction
    actionReset: QtWidgets.QWidgetAction
    actionStart_scan: QtWidgets.QWidgetAction
    actionCalibration: QtWidgets.QWidgetAction
    settings_dialog_class: QtWidgets.QDialog
    pbMoveTo: QtWidgets.QPushButton
    pbStopMotor: QtWidgets.QPushButton
    pbSave: QtWidgets.QPushButton
    pbHome: QtWidgets.QPushButton
    pbStabilize: QtWidgets.QPushButton
    pbSetPiezoVoltage: QtWidgets.QPushButton
    lbBandwidth: QtWidgets.QLabel
    lbWavelength: QtWidgets.QLabel
    lbMotorPosition: QtWidgets.QLabel
    lbPiezoVoltage: QtWidgets.QLabel
    sbMotorPosition: QtWidgets.QDoubleSpinBox
    sbPiezoVoltage: QtWidgets.QDoubleSpinBox
    LC100SettingsDialog: QtWidgets.QDialog

    def __init__(self, name: str = "VISspec", host: str = "localhost", **kwargs) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(
            name=name,
            host=host,
            ui_file_name="VISspec",  # ui file name in the data subfolder
            ui_file_path=pathlib.Path(__file__).parent / "data",
            settings_dialog_class=settings.Settings,  # class of the settings dialog
            **kwargs,
        )

        self.configurePlot()

        self.publisher = data_publisher.DataPublisher(full_name='VisSpec')

        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.checkForData)

        self.actionConnect_LC100_2.triggered.connect(self.connectLC100)
        self.actionLC100_Settings_2.triggered.connect(self.openLC100Settings)
        self.actionReset.triggered.connect(self.resetLC100)
        self.actionStart_scan.triggered.connect(self.startScan)
        self.actionConnect_Motor.triggered.connect(self.connectMotor)
        self.actionCalibration.triggered.connect(self.openCalibration)

        self.pbMoveTo.clicked.connect(self.checkMotorStart)
        self.pbStopMotor.clicked.connect(self.stopMotor)
        self.pbSave.clicked.connect(self.saveSpectrum)
        self.pbHome.clicked.connect(self.home)
        self.pbStabilize.clicked.connect(self.stabilize)
        self.pbSetPiezoVoltage.clicked.connect(self.setPiezoVoltage)

        self.actionConnect_Motor.setChecked(True)
        self.connectMotor(True)

        self.actionConnect_LC100_2.setChecked(True)
        self.connectLC100(True)

        log.info(f"VISspec initialized with name '{name}'.")

    def __del__(self):
        try:
            self.stop_listen()
        except AttributeError:
            pass

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.stop_listen()
        self.startScan(False)
        try:
            error = LC100.close(self.device)
            del self.device
            log.info(f"LC100-close: {error}")
        except Exception:
            log.warning('Could not close connection to LC100.')
        event.accept()

    @pyqtSlot()
    def home(self):
        log.info("Move to home position.")
        Calibration.sbMotorPosition.setValue(0)
        Calibration.moveMotor()

    def configurePlot(self):
        self.spectrum = self.plot.plot([])
        self.spectrumFit = self.plot.plot([], pen="r")
        self.plot.setLabel("bottom", "Wavelength in nm")
        self.plot.setLabel("left", "Signal in arb. units")

    @pyqtSlot(bool)
    def connectLC100(self, checked: bool) -> None:
        settings = QtCore.QSettings()
        if checked:
            deviceName = settings.value("deviceName", type=str)
            log.info(f'Try to connect to {deviceName}')
            self.device, error = LC100.init(deviceName)
            log.info(f"LC100-init: {error}")
            if LC100.getDeviceStatus(self.device) == "unknown status":
                self.actionConnect_LC100_2.setChecked(False)
                log.error("Could not connect to LC100. Check device name and USB connection.")
        else:
            try:
                error = LC100.close(self.device)
                del self.device
            except AttributeError:
                log.info("LC100-close: LC100 already disconnected.")

    @pyqtSlot(bool)
    def connectMotor(self, checked: bool) -> None:
        if checked:
            COM = motors.getPort("VisSpec")
            try:
                self.connectionManager = ConnectionManager(f"--port COM{COM}")
                self.motorCard = TMCM6110(self.connectionManager.connect())
                log.info("Motorcard successfully connected.")
            except Exception as exc:
                log.exception("Connection failed.", exc)
            else:
                self.configureMotor("gratingMotor")
        else:
            try:
                self.connectionManager.disconnect()
                self.readoutTimer.stop()
            except AttributeError:
                pass  # Not existent
            except Exception as exc:
                log.exception(exc)
            try:
                del self.motorCard
            except AttributeError:
                pass

    @pyqtSlot(str, int)
    def configureMotor(self, motorName):
        """Configure the motor `motorname`."""
        # Load the config for the motor and configure the motor.
        settings = QtCore.QSettings()
        self.config = settings.value(motorName, type=dict)
        self.motor = self.config['motorNumber']
        try:
            motors.configureMotor(self.motorCard, self.config)
        except KeyError:
            log.exception("Motor not defined.")
        except Exception as exc:
            log.exception("Error configuring motor.", exc)

    @pyqtSlot()
    def openLC100Settings(self):
        settings = QtCore.QSettings()
        if self.actionStart_scan.isChecked():
            error = LC100.setOperatingMode('idle', self.device)
            log.info(f'LC100-setOperatingMode: {error}')
            self.resetLC100()
            self.actionStart_scan.setChecked(False)
        # Integration Time
        intTime, error = LC100.getIntTime('act', self.device)
        settings.setValue('actIntTime', str(intTime))
        log.info(f'LC100-getIntTime: {error}')
        # Operating Mode
        operatingMode, error = LC100.getOperatingMode(self.device)
        settings.setValue('actOperatingMode', operatingMode)
        log.info(f'LC100-getOperatingMode: {error}')
        # Trigger Delay
        triggerDelay, error = LC100.getTriggerDelay('set', self.device)
        settings.setValue('actTriggerDelay', str(triggerDelay))
        log.info(f'LC100-getTriggerDelay: {error}')
        # Evaluation Box Values
        x, y, error = LC100.getEvalBox(settings.value('evalBoxNumber', type=int), self.device)
        settings.setValue('actX1', str(x[0]))
        settings.setValue('actX2', str(x[1]))
        settings.setValue('actY1', str(y[0]))
        settings.setValue('actY2', str(y[1]))
        log.info(f'LC100-getEvalBox: {error}')
        # Device Information
        info, error = LC100.identificationQuery(self.device)
        settings.setValue('manufacturer', info['manufacturerName'])
        settings.setValue('deviceNameLC100', info['deviceName'])
        settings.setValue('serialNumber', info['serialNumber'])
        log.info(f'LC100-identificationQuery: {error}')
        info, error = LC100.sensorTypeQuery(self.device)
        settings.setValue('numberOfPixels', str(info['numberOfPixels']))
        settings.setValue('description', info['description'])
        log.info(f'LC100-sensorTypeQuery: {error}')
        driverRevision, firmwareRevision, error = LC100.revisionQuery(self.device)
        settings.setValue('driverRevision', driverRevision)
        settings.setValue('firmwareRevision', firmwareRevision)
        log.info(f'LC100-revisionQuery: {error}')
        # 
        self.LC100SettingsDialog = LC100Settings()
        self.LC100SettingsDialog.signals.LC100configured.connect(self.configureLC100)
        self.LC100SettingsDialog.open()

    @pyqtSlot()
    def configureLC100(self):
        """Write values to camera LC100"""
        log.info('Configure LC100.')
        settings = QtCore.QSettings()
        # Integration Time
        error = LC100.setIntTime(settings.value('IntTime', type=float), self.device)
        log.info(f'LC100-setIntTime: {error}')
        # Operating Mode
        error = LC100.setOperatingMode(settings.value('actOperatingMode', type=str), self.device)
        log.info(f'LC100-setOperatingMode: {error}')
        # Trigger Delay
        error = LC100.setTriggerDelay(settings.value('triggerDelay', type=float), self.device)
        log.info(f'LC100-setTriggerDelay: {error}')
        # Evaluation Box Values
        error = LC100.setEvalBox(settings.value('x1', type=int), settings.value('x2', type=int),
                                 settings.value('y1', type=float), settings.value('y2', type=float),
                                 self.device,
                                 settings.value('evalBoxNumber', type=int))
        log.info(f'LC100-setEvalBox: {error}')

    @pyqtSlot()
    def resetLC100(self):
        try:
            error = LC100.reset(self.device)
            log.info(f'LC100-reset: {error}')
        except AttributeError:
            log.error("Could not reset LC100 since the camera is not connected.")

    @pyqtSlot(bool)
    def startScan(self, checked: bool):
        settings = QtCore.QSettings()
        if checked:
            if not self.actionConnect_LC100_2.isChecked():
                self.connectLC100(True)
            try:
                error = LC100.setIntTime(settings.value('intTime', type=float), self.device)
                log.info(f'LC100-setIntTime: {error}')
                error = LC100.setTriggerDelay(settings.value('triggerDelay', type=float), self.device)
                log.info(f'LC100-setTriggerDelay: {error}')
                error = LC100.setEvalBox(settings.value('x1', type=int), settings.value('x2', type=int),
                                         settings.value('y1', type=float), settings.value('y2', type=float),
                                         self.device, settings.value('evalBoxNumber', type=int))
                log.info(f'LC100-setEvalBox: {error}')
                mode = settings.value('operatingMode', type=int)
                if mode == 0:
                    error = LC100.setOperatingMode('idle', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    self.actionStart_scan.setChecked(False)
                elif mode == 1:
                    error = LC100.setOperatingMode('SW-single', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScan(self.device)
                    log.info(f'LC100-startScan: {error}')
                    self.readout()
                    self.actionStart_scan.setChecked(False)
                elif mode == 2:
                    error = LC100.setOperatingMode('SW-loop', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScanCont(self.device)
                    log.info(f'LC100-startScanCont: {error}')
                    self.readoutTimer.start(settings.value('interval'))
                elif mode == 3:
                    error = LC100.setOperatingMode('HW-single', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScanExtTrg(self.device)
                    log.info(f'LC100-startScanExtTrg: {error}')
                    self.readout()
                    self.actionStart_scan.setChecked(False)
                elif mode == 4:
                    error = LC100.setOperatingMode('HW-loop', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScanContExtTrg(self.device)
                    log.info(f'LC100-startScanContExtTrg: {error}')
                    self.readoutTimer.start(settings.value('interval'))
                else:
                    log.info('No valid mode.')
            except Exception as exc:
                log.error(f'Could not start LC100 scan with the following error: {exc}')
                self.actionStart_scan.setChecked(False)
        else:
            try:
                error = LC100.setOperatingMode('idle', self.device)
                log.info(f'LC100-setOperatingMode: {error}')
                self.actionStart_scan.setChecked(False)
                self.readoutTimer.stop()
            except Exception:
                self.actionStart_scan.setChecked(False)
                self.readoutTimer.stop()

    @pyqtSlot()
    def checkForData(self):
        status, error = LC100.getDeviceStatus(self.device)
        #log.info(f"Device status: {status}")
        if status == 'scan is done, waiting for data transfer to PC':
            self.readout()

    def readout(self) -> None:
        '''
        Readout of data (LC100 data / motor position).
        '''
        # readout motor
        motorPosition = self.motorCard.motors[self.motor].actual_position
        self.lbMotorPosition.setText(str(round((self.stepsToWavelength(motorPosition)[1023]
                                                + self.stepsToWavelength(motorPosition)[1024]
                                                ) / 2, 2)))
        # readout LC100
        self.xrange = self.stepsToWavelength(motorPosition)
        self.data, error = LC100.getScanData(self.device)
        signals = self.dataSignals()
        signals.dataReadySignal.emit()
        self.spectrum.setData(self.xrange, self.data)
        self.evaluateData(self.xrange, self.data)
        if self.pbStabilize.isChecked():
            output = self.pid(self.wavelength)
            self.lbPiezoVoltage.setText(f"{round(output, 2)} V")
            try:
                with ni.Task() as task:
                    task.ao_channels.add_ao_voltage_chan("myDAQ1/ao0")
                    task.write(round(output, 2))
            except Exception as exc:
                log.error(f"Stabilization failed with the following error: {exc}")
                self.pbStabilize.setChecked(False)
                self.stabilize(False)

    def stepsToWavelength(self, motorPosition: int):
        microstepresolution = self.motorCard.motors[self.motor].drive_settings.microstep_resolution
        steps = 0.5**microstepresolution * motorPosition
        pixel = (np.arange(2048)-1023.5) * 14e-6
        x0 = 11506.530748739355  # motor position for theta=0° in steps
        theta = np.radians(0.6 / 60)*(steps - x0)
        L = 500e-3  # focal length in m
        delta = np.radians(22.4)
        G = 417.3497295611672  # inverse grating constant in nm
        m = 1  # diffraction order
        wavelength = G / m * (np.sin(theta + delta / 2)
                              + np.sin(theta - (delta / 2) + np.arctan(pixel / L)))
        return wavelength

    def wavelengthToSteps(self, wavelength: float):
        delta = np.radians(22.44)
        m = 1  # diffraction order
        G = 417.3497295611672e-9  # inverse grating constant in nm
        x0 = 11506.530748739355  # motor position for theta=0° in steps
        theta = np.arcsin(m * wavelength * 1e-9 / (2 * G * np.cos(delta / 2)))
        steps = x0 + theta/np.radians(0.6/60)
        microstepresolution = self.motorCard.motors[self.motor].drive_settings.microstep_resolution
        microsteps = steps * 2**microstepresolution
        return microsteps

    def evaluateData(self, x, data: list[int]) -> None:
        """
        Uses the spectrometer calibration to get the x-array from the motorposition with the
        wavelength in nm of each pixel. The data is plotted and a Gaussian fit is used to get the
        wavelength position and the bandwidth. These values get sent out to use in the DataLogger or
        the APEOPO program for stabilization and automatization.
        """
        def Gauss(x, a, b, c, x0):
            return a * np.exp(-(x - x0)**2 / (2 * b**2)) + c
        fitParameter, fitCovariance = curve_fit(
            Gauss, x, data, p0=[max(data) - min(data), 0.5, min(data), x[data.index(max(data))]]
            )
        self.spectrumFit.setData(x, Gauss(x, *fitParameter))
        self.wavelength = fitParameter[3]
        self.lbWavelength.setText(str(round(self.wavelength, 2)))
        self.bandwidth = 2 * np.sqrt(2 * np.log(2)) * fitParameter[1]
        self.lbBandwidth.setText(str(round(self.bandwidth, 2)))
        self.sendData({'VISwavelength': self.wavelength, 'VISbandwidth': self.bandwidth})

    @pyqtSlot()
    def checkMotorStart(self):
        print(float(self.lbMotorPosition.text()))
        print(self.sbMotorPosition.value())
        if self.sbMotorPosition.value() >= float(self.lbMotorPosition.text()):
            position = int(self.wavelengthToSteps(self.sbMotorPosition.value()))
            print(position)
            self.motorCard.move_to(self.motor, position, self.config['positioningSpeed'])
            self.motorTimer.stop()
        else:
            self.motorTimer = QtCore.QTimer()
            self.motorTimer.timeout.connect(self.moveNegative)
            position = int(self.wavelengthToSteps(self.sbMotorPosition.value() - 5))
            self.motorCard.move_to(self.motor, position, self.config['positioningSpeed'])
            self.motorTimer.start(100)

    def moveNegative(self):
        if self.motorCard.motors[self.motor].get_position_reached():
            self.checkMotorStart()

    def stopMotor(self):
        self.motorCard.stop(self.motor)

    def sendData(self, data) -> None:
        """Example how to handle sending data."""
        try:
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)

    @pyqtSlot()
    def saveSpectrum(self):
        path = 'D:\\Measurement Data\\VisSpectrometer\\'
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        data = {'motorPosition': float(self.lbMotorPosition.text()), 'wavelength': self.wavelength, 'bandwidth':self.bandwidth, 'lambdarange':list(self.xrange), 'signal': list(self.data)}
        with open(f'{path}{file_name}.json', 'w') as file:
            json.dump(data, file)
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(file_name)
        log.info('Data saved.')

    @pyqtSlot()
    def setPiezoVoltage(self):
        try:
            with ni.Task() as task:
                task.ao_channels.add_ao_voltage_chan("myDAQ1/ao0")
                task.write(self.sbPiezoVoltage.value())
                self.lbPiezoVoltage.setText(str(self.sbPiezoVoltage.value()))
            log.info(f"New Piezo Voltage: {self.sbPiezoVoltage.value()} V")
        except Exception as exc:
            log.error(f"Set Piezo Voltage failed with the following error: {exc}")

    @pyqtSlot(bool)
    def stabilize(self, checked: bool):
        settings = QtCore.QSettings()
        if checked:
            self.pid = PID(auto_mode=False)
            self.pid.output_limits = (-5, 5)
            self.pid.Kd = settings.value("D", defaultValue=0, type=float)
            self.pid.Kp = settings.value("P", defaultValue=0, type=float)
            self.pid.Ki = settings.value("I", defaultValue=0, type=float)
            self.pid.setpoint = self.sbMotorPosition.value()
            self.pid.set_auto_mode(enabled=True, last_output=self.wavelength)
        else:
            self.pid.set_auto_mode(enabled=False)

    @pyqtSlot()
    def openCalibration(self):
        self.calibration = Calibration(self)
        self.calibration.open()

    class dataSignals(QtCore.QObject):
        dataReadySignal = QtCore.pyqtSignal()


class Calibration(QtWidgets.QDialog):

    lbMotorPosition: QtWidgets.QLabel
    sbMotorPosition: QtWidgets.QSpinBox
    pbMoveTo: QtWidgets.QPushButton
    pbStop: QtWidgets.QPushButton
    pbSave: QtWidgets.QPushButton
    leComment: QtWidgets.QLineEdit

    def __init__(self, parent: VISspec, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent
        self.main_window = parent

        # Load the user interface file and show it.
        uic.loadUi("data/calibration.ui", self)
        self.show()

        settings = QtCore.QSettings()
        self.motorNumber = settings.value('gratingMotor')['motorNumber']
        self.microstepresolution = self.main_window.motorCard.motors[self.motorNumber].drive_settings.microstep_resolution
        print(self.motorNumber)
        self.configPlot()
        #self.signal = self.main_window.dataSignals()
        #self.signal.dataReadySignal.connect(self.readout)
        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readout)
        self.motorTimer = QtCore.QTimer()
        self.motorTimer.timeout.connect(self.waitMotor)

        self.pbMoveTo.clicked.connect(self.moveMotor)
        self.pbStop.clicked.connect(self.stopMotor)
        self.pbSave.clicked.connect(self.saveData)

        self.readoutTimer.start(settings.value('interval'))

    def closeEvent(self):
        self.readoutTimer.stop()
        self.stopMotor()
        self.motorTimer.stop()

    def configPlot(self):
        self.spectrum = self.plot.plot([])
        self.plot.setLabel("bottom", "Motor position in steps")
        self.plot.setLabel("left", "Signal in arb. units")

    @pyqtSlot()
    def readout(self):
        #log.info('Received data.')
        self.position = self.main_window.motorCard.motors[self.motorNumber].actual_position
        self.lbMotorPosition.setText(str(self.position))
        self.data = self.main_window.data
        self.spectrum.setData(self.data)

    @pyqtSlot()
    def moveMotor(self):
        settings = QtCore.QSettings()
        config = settings.value('gratingMotor')
        velocity = config['positioningSpeed']
        if self.sbMotorPosition.value() > self.position:
            self.main_window.motorCard.move_to(self.motorNumber, self.sbMotorPosition.value(),
                                               velocity)
            log.info('Move motor positive')
        else:
            self.main_window.motorCard.move_to(self.motorNumber, self.sbMotorPosition.value() -
                                               int(2**self.microstepresolution * 200), velocity)
            log.info('Move motor negative.')
            self.motorTimer.start(100)

    @pyqtSlot()
    def waitMotor(self):
        log.info('Wait motor.')
        if self.main_window.motorCard.motors[self.motorNumber].get_position_reached():
            self.motorTimer.stop()
            self.moveMotor()

    @pyqtSlot()
    def stopMotor(self):
        self.main_window.motorCard.stop(self.motorNumber)

    @pyqtSlot()
    def saveData(self):
        settings = QtCore.QSettings()
        config = settings.value('gratingMotor')
        path = 'D:\\Measurement Data\\VisSpectrometer\\Calibration\\'
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        header = config
        data = {'motorPosition': self.position, 'signal': self.data}
        comment = self.leComment.text()
        with open(f'{path}{file_name}.json', 'w') as file:
            json.dump((header, data, comment), file)
        log.info('Data saved.')


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(VISspec)

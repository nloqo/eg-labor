
from typing import Any

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from devices import motors


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    buttonBox = QtWidgets.QDialogButtonBox
    leDeviceName = QtWidgets.QLineEdit
    sbInterval = QtWidgets.QSpinBox
    pbGratingMotor = QtWidgets.QPushButton

    def __init__(self, **kwargs) -> None:
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__(**kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/settings.ui", self)
        self.show()

        # Configure settings.
        self.settings = QtCore.QSettings()
        self.signals = self.SettingsSignals()
        # Convenience list for widgets with value(), SetValue() methods.
        self.sets: tuple[
            tuple[QtWidgets.QSpinBox | QtWidgets.QDoubleSpinBox, str, Any, type], ...
        ] = (
            (self.sbInterval, "interval", 50, int),
        )
        self.textSets: tuple[
            tuple[QtWidgets.QLineEdit, str, Any, type], ...
        ] = (
            (self.leDeviceName, 'deviceName', 'USB0::0x1313::0x80A0::M00278991::RAW', str),
        )
        self.readValues()

        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbRestoreDefaults = self.buttonBox.button(restore)  # type: ignore
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)
        self.pbGratingMotor.clicked.connect(self.openGratingMotorSettings)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings."""

        motorConfigured = QtCore.pyqtSignal(str)

    def readValues(self) -> None:
        """Read the stored values and show them on the user interface."""
        for widget, name, value, typ in self.sets:
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        for widget, name, value, typ in self.textSets:
            widget.setText(self.settings.value(name, defaultValue=value, type=typ))

    @pyqtSlot()
    def restoreDefaults(self) -> None:
        """Restore the user interface to default values."""
        for widget, name, value, typ in self.sets:
            widget.setValue(value)
        for widget, name, value, typ in self.textSets:
            widget.setText(value)

    @pyqtSlot()
    def accept(self) -> None:
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for widget, name, value, typ in self.sets:
            self.settings.setValue(name, widget.value())
        for widget, name, value, typ in self.textSets:
            self.settings.setValue(name, widget.text())
        super().accept()  # make the normal accept things

    @pyqtSlot()
    def openGratingMotorSettings(self):
        motorSettings = motors.MotorSettings(key="gratingMotor", motorName="gratingMotor")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("gratingMotor")

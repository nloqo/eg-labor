"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging
import sys
from simple_pid import PID

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot

from devices import motors
from pyleco.utils.data_publisher import DataPublisher
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110

# Local packages.
from data import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


# TODO alle Vorkommnisse von BaseProgram durch neuen "Programm-Name" ersetzen.
class MIRA(QtWidgets.QMainWindow):
    """Define the main window and essential methods of the program."""

    def __init__(self, *args, **kwargs):
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/MIRA.ui", self)
        self.show()

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName("MIRA")
        self.setSettings()
        self.publisher = DataPublisher("MIRA")

        # Get signals
        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readData)

        # Connect actions to slots.
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)
        self.actionConnect.triggered.connect(self.connectMotors)

        # Autostart configuration
        # self.autostartConnect()
        # self.lbMaxVal.setText(str(round(settings.value("Calibration")[1][1], 4)))
        # value_open = 0
        # value_close = 10
        # config = settings.value('Shutter', type=dict)
        # motor = config['motorNumber']
        # try:
        #    if self.motorCard.motors[motor].actual_position == motors.unitsToSteps(value_open,
        # config):
        #        self.pbOpen.setChecked(True)
        #        self.pbOpen.setStyleSheet('background-color: green')
        #    elif self.motorCard.motors[motor].actual_position == motors.unitsToSteps(value_close,
        # config):
        #        self.pbClose.setChecked(True)
        #        self.pbClose.setStyleSheet('background-color: red')
        # except ConnectionError:
        #    pass
        # self.stabilizePID = False

        log.info("MIRA initialized.")

        # Connect buttons to slots
        self.pbOpen.pressed.connect(self.openShutter)
        self.pbClose.pressed.connect(self.closeShutter)
        self.pbSetPower.clicked.connect(self.changePower)
        self.slPolarizer.sliderReleased.connect(self.changePower)
        self.slPolarizer.sliderMoved.connect(self.sliderMove)
        self.pbStabilize.clicked.connect(self.setupPID)
        self.sbStabilize.valueChanged.connect(self.stabilizationValueChanged)

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        # self.closeShutter()
        self.pbStabilize.setChecked(False)
        settings = QtCore.QSettings()
        config = settings.value("HWP", type=dict)
        self.motorCard.motors[config["motorNumber"]].move_to(0)
        self.readoutTimer.stop()
        self.connectionManager.disconnect()

        event.accept()

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        self.settingsDialog = Settings.Settings()
        self.settingsDialog.accepted.connect(self.setSettings)
        self.settingsDialog.signals.motorConfigured.connect(self.configureMotor)
        self.settingsDialog.open()

    def setSettings(self):
        """Apply new settings."""

        settings = QtCore.QSettings()
        if self.pbStabilize.isChecked():
            self.pid.Kd = settings.value("D", defaultValue=0, type=float)
            self.pid.Kp = settings.value("P", defaultValue=0, type=float)
            self.pid.Ki = settings.value("I", defaultValue=0, type=float)

    @pyqtSlot(bool)
    def connectMotors(self, checked):
        """(Dis)connect the motor card."""
        settings = QtCore.QSettings()
        if checked:
            COM = motors.getPort("MIRA")
            try:
                self.connectionManager = ConnectionManager(f"--port COM{COM}")
                self.motorCard = TMCM6110(self.connectionManager.connect())
            except Exception:
                log.exception("Connection failed.")
                print("Connection failed!")
            else:
                print("Connection sucessfull!")
                log.info("Successfully connected.")
                self.configureMotor("Shutter")
                self.configureMotor("HWP")
                self.readoutTimer.start(settings.value("interval"))

        else:
            try:
                self.connectionManager.disconnect()
            except AttributeError:
                pass  # Not existant
            except Exception as exc:
                print(exc)
            try:
                del self.motorCard
            except AttributeError:
                pass
            try:
                del self.motor
            except AttributeError:
                pass

    @pyqtSlot(str)
    def configureMotor(self, motorName):
        """Configure the motor `motorname`."""
        # Load the config for the motor and configure the motor.
        settings = QtCore.QSettings()
        config = settings.value(motorName, type=dict)
        try:
            motors.configureMotor(self.motorCard, config)
        except KeyError:
            return  # no value
        except Exception:
            log.exception("Error configuring motor.")

    @pyqtSlot()
    def openShutter(self):
        """Open the Shutter behind MIRA"""
        settings = QtCore.QSettings()
        value_open = 0
        config = settings.value("Shutter", type=dict)
        steps = motors.unitsToSteps(value_open, config)
        motor = config["motorNumber"]
        try:
            self.motorCard.move_to(motor, steps)
            self.pbOpen.setStyleSheet("background-color: green")
            self.pbClose.setStyleSheet("background-color: grey")
            self.pbClose.setChecked(False)
        except ConnectionError:
            print("Motors not connected!")

    @pyqtSlot()
    def closeShutter(self):
        """Close Shutter behind MIRA"""
        self.pbStabilize.setChecked(False)
        settings = QtCore.QSettings()
        value_close = 10
        config = settings.value("Shutter", type=dict)
        steps = motors.unitsToSteps(value_close, config)
        motor = config["motorNumber"]
        try:
            self.motorCard.move_to(motor, steps)
            self.pbOpen.setStyleSheet("background-color: grey")
            self.pbClose.setStyleSheet("background-color: red")
            self.pbOpen.setChecked(False)
        except ConnectionError:
            print("Motors not connected!")

    @pyqtSlot()
    def changePower(self):
        """Set the HWP angle"""
        settings = QtCore.QSettings()
        config = settings.value("HWP", type=dict)
        steps = motors.unitsToSteps(self.sbPolarizer.value(), config)
        motor = config["motorNumber"]
        self.motorCard.move_to(motor, steps)

    @pyqtSlot(int)
    def sliderMove(self, value):
        """Change the setpoint in the spin box, when the slider is moved."""
        angle = value / 100
        self.sbPolarizer.setValue(angle)

    @pyqtSlot()
    def readData(self):
        settings = QtCore.QSettings()
        try:
            self.dat = {}
            config = settings.value("HWP", type=dict)
            step = self.motorCard.motors[config["motorNumber"]].actual_position
            pos = motors.stepsToUnits(step, config)
            self.lbHWPPos.setText(str(round(pos, 4)))
            self.dat["MIRAraw"] = self.motorCard.get_analog_input(0)
            self.dat["OPOraw"] = self.motorCard.get_analog_input(4)
            self.dat["MIRA"] = round(
                float(settings.value("calMIRAa")) * self.dat["MIRAraw"]
                + float(settings.value("calMIRAb")),
                4,
            )
            self.dat["OPO"] = round(
                float(settings.value("calOPOa")) * self.dat["OPOraw"]
                + float(settings.value("calOPOb")),
                4,
            )
            self.lbPD1Value.setText(str(self.dat["MIRAraw"]) + " / " + str(self.dat["MIRA"]) + "W")
            self.lbPD2Value.setText(str(self.dat["OPOraw"]) + " / " + str(self.dat["OPO"]) + "W")
            self.sendData(self.dat)
            if self.pbStabilize.isChecked():
                output = self.pid(self.dat["OPO"])
                motor = config["motorNumber"]
                self.motorCard.move_to(motor, int(output))

                # TODO: Finish implementation of PID controller.
        except Exception as exc:
            log.exception(f"PD readout failed!, {exc}")

    def sendData(self, values):
        try:
            self.publisher.send_legacy(values)
        except Exception as exc:
            log.exception(f"Sending data failed!, {exc}")

    def autostartConnect(self):
        try:
            self.actionConnect.setChecked(True)
            self.connectMotors(True)
            print("autostart sucess!")
        except Exception:
            pass

    @pyqtSlot()
    def setupPID(self):
        settings = QtCore.QSettings()
        config = settings.value("HWP", type=dict)
        if self.pbStabilize.isChecked():
            self.pid = PID(auto_mode=False)
            self.pid.output_limits = (
                motors.unitsToSteps(settings.value("Calibration")[0][0], config),
                motors.unitsToSteps(settings.value("Calibration")[1][0], config),
            )
            self.pid.Kd = settings.value("D", defaultValue=0, type=float)
            self.pid.Kp = settings.value("P", defaultValue=0, type=float)
            self.pid.Ki = settings.value("I", defaultValue=0, type=float)
            self.pid.setpoint = self.sbStabilize.value()
            self.pid.set_auto_mode(enabled=True, last_output=self.dat["OPO"])
        else:
            self.pid.set_auto_mode(enabled=False)

    def stabilizationValueChanged(self):
        if self.pbStabilize.isChecked():
            self.pid.setpoint = self.sbStabilize.value()


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:  # Verbose log.
        log.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = MIRA()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

"""
Module for the settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from devices import motors


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self):
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/Settings.ui", self)
        self.show()

        # Configure settings.
        self.signals = self.SettingsSignals()
        self.settings = QtCore.QSettings()

        # Convenience list for widgets with value(), SetValue() methods.
        self.sets = (
            # name of widget, key of setting, defaultValue, type of data
            # (self.widget, 'name', 0, int),
            (self.sbInterval, "interval", 0, int),
            (self.sbPublisherPort, "publisherPort", 11100, int),
            (self.sbDeadband, "deadband", 10, int),
            (self.sbMIRAa, "calMIRAa", 0, float),
            (self.sbMIRAb, "calMIRAb", 0, float),
            (self.sbOPOa, "calOPOa", 0, float),
            (self.sbOPOb, "calOPOb", 0, float),
            (self.sbI, "I", 0, float),
            (self.sbP, "P", 0, float),
            (self.sbD, "D", 0, float),
        )
        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbShutter.clicked.connect(self.openShutterSettings)
        self.pbHWP.clicked.connect(self.openHWPSettings)
        self.pbRestoreDefaults = self.buttonBox.button(restore)
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)
        # TODO: example of a fileDialog
        # self.pbSavePath.clicked.connect(self.openFileDialog)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings."""

        motorConfigured = QtCore.pyqtSignal(str)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        # TODO: read settings and write them to the field
        # self.Interval.setValue(self.settings.value("interval", defaultValue=5000, type=int))

    @pyqtSlot()
    def openShutterSettings(self):
        motorSettings = motors.MotorSettings(key="Shutter", motorName="Shutter")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("Shutter")

    @pyqtSlot()
    def openHWPSettings(self):
        motorSettings = motors.MotorSettings(key="HWP", motorName="HWP")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("HWP")

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        # TODO: save the values from the fields into settings
        # self.settings.setValue('savePath', self.leSavePath.text())
        super().accept()  # make the normal accept things

    '''
    TEMPLATE: a possible file dialog
    def openFileDialog(self):
        """Open a file path dialog."""
        self.savePath = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
        self.leSavePath.setText(self.savePath)
    '''

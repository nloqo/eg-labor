# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 13:20:56 2023

@author: ps-admin
"""

# Standard packages.

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import time

# 3rd party packages.
from PyQt6 import QtCore

from devices import motors
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110


settings = QtCore.QSettings("NLOQO", "MIRA")


def fluc(x, a, b, c):
    return a * np.cos(b * (x - c)) ** 2


def changePower(angle):
    config = settings.value("HWP", type=dict)
    steps = motors.unitsToSteps(angle, config)
    motor = config["motorNumber"]
    motorCard.move_to(motor, steps)
    print(motors.stepsToUnits(steps, config))


# %%
COM = motors.getPort("MIRA")
try:
    connectionManager = ConnectionManager(f"--port COM{COM}")
    motorCard = TMCM6110(connectionManager.connect())
except Exception as exc:
    print(f"Connection failed! ({exc})")

config = settings.value("HWP", type=dict)
startVal = motors.stepsToUnits(motorCard.motors[config["motorNumber"]].actual_position, config)

angle = 0
steps = motors.unitsToSteps(angle, config)
changePower(0)

while not motorCard.motors[config["motorNumber"]].get_position_reached():
    time.sleep(0.5)

print("Start Position reached!")

power = []
angle = []
changePower(90)
while not motorCard.motors[config["motorNumber"]].get_position_reached():
    power.append(
        float(settings.value("calOPOa")) * motorCard.get_analog_input(4)
        + float(settings.value("calOPOb"))
    )
    angle.append(
        motors.stepsToUnits(motorCard.motors[config["motorNumber"]].actual_position, config)
    )
    print(angle[-1])
    time.sleep(settings.value("interval") / 1000)


changePower(startVal)

connectionManager.disconnect()
print("Disconnected.")
# %%

plt.plot(angle, power)

x = np.linspace(0, 90, 1000)
fitParameter, fitCovariance = curve_fit(fluc, angle, power, p0=[1, 1 / 25, 50])
plt.plot(x, fluc(x, *fitParameter), color="r")
plt.show()


def g(y):
    return np.arccos(np.sqrt(y / fitParameter[0])) / fitParameter[1] + fitParameter[2]


minimum = g(min(fluc(x, *fitParameter)))
maximum = g(max(fluc(x, *fitParameter)))
print(minimum, maximum)
while minimum > 90:
    minimum = minimum - 90


while maximum > 90:
    maximum = maximum - 90

settings.setValue(
    "Calibration", [(minimum, min(fluc(x, *fitParameter))), (maximum, max(fluc(x, *fitParameter)))]
)

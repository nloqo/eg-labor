"""
Main file of the LeCroy oscilloscope controller.

created on 20.09.2021 by Benedikt Moneke
"""

# Standard packages.
import logging
import pickle
import sys

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
import pyvisa as pv

from pyleco.utils.data_publisher import DataPublisher

# Local packages.
from data import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class LeCroy(QtWidgets.QMainWindow):
    """Define the main window and essential methods of the program."""

    def __init__(self, name="LeCroy", *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("LeCroy.ui", self)
        self.show()
        self.signals = self.LeCroySignals()

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName(name)

        # Intercom
        self.publisher = DataPublisher(full_name=name)

        self.rm = pv.ResourceManager()

        # Connect actions to slots.
        self.actionConnect.triggered.connect(self.connect)
        self.actionStarter.triggered.connect(self.emitStarter)
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)

        self.handler = ConnectionHandler()
        self.thread = QtCore.QThread()
        self.handler.moveToThread(self.thread)
        self.thread.start()
        self.signals.listen.connect(self.handler.listen)
        self.handler.dataReady.connect(self.dataReady)
        self.handler.timeout.connect(self.timeout)

        self.actionConnect.setChecked(True)
        self.connect(True)
        log.info("LeCroy initialized.")

    class LeCroySignals(QtCore.QObject):
        """Signals for LeCroy."""
        listen = QtCore.pyqtSignal()

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        self.handler.stop = True
        self.thread.quit()
        # accept the close event (reject it, if you want to do something else)
        event.accept()

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        settings = Settings.Settings()
        if settings.exec():
            pass

    @pyqtSlot(bool)
    def connect(self, checked):
        """Connect to the LeCroy."""
        if checked:
            settings = QtCore.QSettings()
            COM = settings.value('COM', type=int)
            try:
                self.osci = self.rm.open_resource(f"ASRL{COM}::INSTR",
                                                  baud_rate=115200)
                """
                The same defaults as visa:
                baudrate 9600
                8 data bits
                no parity
                one stop bit
                """
            except Exception as exc:
                log.exception("Connection failed.", exc)
                self.lbValue.setText(str(exc))
                self.actionConnect.setChecked(False)
            else:
                log.info("Connection established.")
                self.osci.clear()
                self.handler.osci = self.osci
                self.signals.listen.emit()
                self.emitStarter()
                self.actionStarter.setEnabled(True)
        else:
            self.handler.stop = True
            try:
                self.osci.close()
            except AttributeError:
                pass
            self.actionStarter.setEnabled(False)

    @pyqtSlot()
    def emitStarter(self):
        """Emit a ready signal to the oscilloscope."""
        try:
            self.osci.write_raw(b'0')
        except pv.VisaIOError as exc:
            if exc.abbreviation == "VI_ERROR_TMO":
                self.timeout()
            else:
                self.lbValue.setText(exc)

    @pyqtSlot(dict)
    def dataReady(self, data):
        """Handle an incoming `data` dictionary."""
        self.lbValue.setText(str(data))
        try:
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)

    @pyqtSlot()
    def timeout(self):
        """Show that an oscilloscope timeout occurred."""
        self.lbValue.setText("Oscilloscope timeout.")


class ConnectionHandler(QtCore.QObject):
    """Object handling the connection to the osci."""
    dataReady = QtCore.pyqtSignal(dict)
    timeout = QtCore.pyqtSignal()

    def __init__(self):
        """Initialize the handler."""
        super().__init__()

    def listen(self):
        """Start listening."""
        log.info("Start listening on Osci.")
        self.stop = False
        while not self.stop:
            try:
                typ, content = self.readMessage(self.osci)
            except pv.VisaIOError as exc:
                if exc.abbreviation == "VI_ERROR_TMO":
                    self.timeout.emit()
                else:
                    print(f"IOError {exc}")
            except Exception as exc:
                print(type(exc).__name__, exc)
            else:
                if typ == "SET":
                    data = pickle.loads(content)
                    self.dataReady.emit(data)
                if typ == "ECO":
                    pass
                self.osci.write_raw(b'0')
        log.info("Stop listening on Osci.")

    def readMessage(self, connection):
        """Receive and decode a message. Return `typ` and `content`."""
        headerLength = 3 + 5  # 3 letters for type, 5 for length

        # Reading header.
        header = connection.read_bytes(headerLength)
        typ = header[0:3].decode('utf-8')
        length = int(header[3:headerLength].decode('utf-8'))

        # Reading message.
        content = connection.read_bytes(length)
        return typ, content


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:
        log.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = LeCroy()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

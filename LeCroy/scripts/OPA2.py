import win32com.client
import pyvisa as pv
import pickle
import time


def get_key(val, dict):
    for key in dict.keys():
        if val == dict.get(key):
            return key


print("Start")
# Connect the serial port.
rm = pv.ResourceManager("@py")
connection = rm.open_resource("ASRLCOM1::INSTR", baud_rate=115200)
connection.clear()
connection.write_raw("RDY00000".encode("utf-8"))

# Connect the oscilloscope program.
scope = win32com.client.Dispatch("LeCroy.ActiveDSOCtrl.1")
scope.MakeConnection("IP:127.0.0.1")
scope.WriteString("VBS app.Measure.ShowMeasure=true", 1)
scope.WriteString("""VBS 'app.Measure.P1.ParamEngine="Area"'""", 1)

# Define variables.
amplitude_down = 0
dictofranges = {
    "2 mV": 0.002,
    "5 mV": 0.005,
    "10 mV": 0.01,
    "20 mV": 0.02,
    "50 mV": 0.05,
    "100 mV": 0.1,
    "200 mV": 0.2,
    "500 mV": 0.5,
    "1 V": 1,
    "2 V": 2,
    "5 V": 5,
    "10 V": 10,
}
listofranges = [
    "2 mV",
    "5 mV",
    "10 mV",
    "20 mV",
    "50 mV",
    "100 mV",
    "200 mV",
    "500 mV",
    "1 V",
    "2 V",
    "5 V",
    "10 V",
]
listofvalues = []

trigger = 0
trigger_0 = 0

time0 = time.perf_counter()
print("Connection done")
# Main loop
flags = True
while flags is True:
    # Read the cycle number.
    scope.WriteString("VBS? 'return=app.Measure.P1.Num.Result.Value'", 1)
    try:
        trigger = int(scope.ReadString(80))
    except ValueError as exc:
        print(exc)
    print(trigger, trigger_0)
    if trigger != trigger_0:
        # A new caputure: evaluate it.

        # Set the Volts/Devision automatically reagrding the signal level
        scope.WriteString("VBS? 'return=app.Measure.P2.Out.Result.Value'", 1)
        try:
            amplitude = float(scope.ReadString(80))
        except ValueError as exc:
            print(exc)
        scope.WriteString("VBS? 'return=app.Acquisition.C2.VerScale", 1)
        try:
            Vrange = float(scope.ReadString(80))
        except ValueError as exc:
            print("Vrange error", exc)
        index = listofranges.index(get_key(Vrange, dictofranges))
        scope.WriteString("VBS? 'return=app.Measure.P2.Out.Result.Status'", 1)

        try:
            status = float(scope.ReadString(80))
        except ValueError as exc:
            print(exc)
        if status == 0.0098 or status == 0.01:
            upperRange = listofranges[index + 1]
            scope.WriteString("C2:VDIV " + upperRange, 1)
            amplitude_up = 0
            print("amplitude up")

        if amplitude / Vrange <= 2:
            amplitude_down += 1
        else:
            amplitude_down = 0

        if amplitude_down == 10:
            if index == 0:
                amplitude_down = 0
            else:
                minorRange = listofranges[index - 1]
                scope.WriteString("C2:VDIV " + minorRange, 1)
                amplitude_down = 0
                print("amplitude down")

        # Get the Signal integral value.
        scope.WriteString("VBS? 'return=app.Measure.P1.Out.Result.Value'", 1)
        try:
            area = float(scope.ReadString(80))
        except ValueError as exc:
            print(exc)
        # Get the Signal width value.
        scope.WriteString("VBS? 'return=app.Measure.P3.Out.Result.Value'", 1)
        try:
            width = float(scope.ReadString(80))
        except ValueError as exc:
            width = 0
            print(exc)
        # Send the value if a byte indicates a listening counterpart.
        buffer = connection.bytes_in_buffer
        if buffer > 0:
            connection.read_bytes(buffer)
            content = pickle.dumps({"area": area, "amplitude": amplitude, "width": width})
            header = ("SET000" + str(len(content))).encode("utf-8")
            connection.write_raw(header + content)
        else:
            print("No response!")

        # Set index and time for the next iteration.
        trigger_0 = trigger
        time0 = time.perf_counter()
    else:
        # Stop after 1.5 s without new measurements, else sleep 5 ms.
        if time.perf_counter() - time0 >= 10:
            flags = False
        else:
            time.sleep(5e-3)

# Close the serial connection and oscilloscope
connection.close()
scope.Disconnect()

# -*- coding: utf-8 -*-
"""
Coding for the LeCroy.

Created on Mon Sep 20 12:38:52 2021

@author: moneke
"""

import pickle
import pyvisa as pv


def readMessage(connection):
    """Receive and decode a message. Return `typ` and `content`."""
    headerLength = 3 + 5  # 3 letters for type, 5 for length

    # Reading header.
    header = connection.read_bytes(headerLength)
    typ = header[0:3].decode('utf-8')
    length = int(header[3:headerLength].decode('utf-8'))

    # Reading message.
    content = connection.read_bytes(length)
    return typ, content


def sendMessage(connection, typ, content=b''):
    """Send a message of `typ` with bytes object `content` via `connection`."""
    header = f"{typ}{len(content):05}".encode('utf-8')
    connection.write_raw(header+content)


def sendObject(connection, content):
    """Send a python `object` via `connection`."""
    sendMessage(connection, typ="SET", content=pickle.dumps(content))


def connect(port, ResourceManager=None):
    """Connect to a device at `port`."""
    if ResourceManager is None:
        ResourceManager = pv.ResourceManager()
    connection = ResourceManager.open_resource(f"ASRL{port}::INSTR",
                                               baud_rate=9600)
    """
        The same defaults as visa:
        baudrate 9600
        8 data bits
        no parity
        one stop bit
    """
    return connection, ResourceManager

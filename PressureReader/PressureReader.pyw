"""
Main file of the pressure reader, which read the pressure sensor and pushes its
values.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging
import sys

import numpy as np
from PyQt6 import QtCore, QtGui, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from PyQt6.QtWidgets import QMessageBox

from pyleco.utils.data_publisher import DataPublisher
from pymeasure.instruments.thyracont.smartline_v2 import VSR
import nidaqmx

from devices.findDevices import find_serial_port

# Local packages.
from data import Calibration, Settings, Configuration


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


# Configuration
hw_info = 0x0403, 0x6001, "AB0P5EA1A"  # broken adapter: "AB0LHRGWA"


class PressureReader(QtWidgets.QMainWindow):
    """Define the main window and essential methods of the program."""

    actionClose: QtGui.QAction
    actionSettings: QtGui.QAction
    actionThyracontConnect: QtGui.QAction
    actionSetHigh: QtGui.QAction
    actionSetLow: QtGui.QAction
    actionThyracontDetails: QtGui.QAction
    actionThyracontConfigure: QtGui.QAction
    actionSiemensConnect: QtGui.QAction

    lbError: QtWidgets.QLabel
    lbThyracont: QtWidgets.QLabel
    lbThyracontDetails: QtWidgets.QLabel
    lbThyracontDetailsDescription: QtWidgets.QLabel
    lbCell: QtWidgets.QLabel
    lbPanel: QtWidgets.QLabel

    def __init__(self, *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/PressureReader.ui", self)
        self.show()

        # Timers etc.
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.readout)

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName("PressureReader")
        settings = QtCore.QSettings()
        self.setSettings(settings)

        self.publisher = DataPublisher("PressureReader")

        # Connect actions to slots.
        #   Application
        self.actionClose.triggered.connect(self.close)  # type: ignore
        self.actionSettings.triggered.connect(self.openSettings)
        #   Thyracont
        self.actionThyracontConnect.triggered.connect(self.connectThyracont)
        self.actionSetHigh.triggered.connect(self.setHigh)
        self.actionSetLow.triggered.connect(self.setLow)
        self.actionThyracontDetails.triggered.connect(self.showThyracontDetails)
        self.actionThyracontConfigure.triggered.connect(self.configureThyracont)
        #   Siemens
        self.actionSiemensConnect.triggered.connect(self.connectSiemens)

        # Try to connect at startup.
        self.showThyracontDetails(False)
        self.actionThyracontConnect.setChecked(True)
        self.connectThyracont(True)
        self.actionSiemensConnect.setChecked(True)
        self.connectSiemens(True)
        self.timer.start()

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        self.connectThyracont(False)
        self.connectSiemens(False)
        # accept the close event (reject it, if you want to do something else)
        event.accept()

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        settingsWindow = Settings.Settings()
        if settingsWindow.exec():
            settings = QtCore.QSettings()
            self.setSettings(settings)

    def setSettings(self, settings):
        """Apply some settings."""
        self.averaging = settings.value("averaging", defaultValue=1, type=int)
        self.timer.setInterval(settings.value("interval", 250, type=int))

    @pyqtSlot(bool)
    def connectThyracont(self, checked):
        """Connect the Thyracont transmitter."""
        if checked:
            settings = QtCore.QSettings()
            com = settings.value("COM", type=int)
            if com == 0:
                com = find_serial_port(*hw_info)
            else:
                com = f"COM{com}"
            try:
                self.transmitter = VSR(com)
                self.unit = self.transmitter.display_unit
            except Exception as exc:
                log.exception("Connection to Thyracont failed.", exc_info=exc)
                self.lbError.setText("Connection failed.")
                self.actionThyracontConnect.setChecked(False)
            else:
                self.lbError.clear()
        else:
            try:
                self.transmitter.close()
                del self.transmitter
            except AttributeError:
                pass

    @pyqtSlot(bool)
    def connectSiemens(self, checked):
        """Connect the Siemens sensors at the NI card."""
        if checked:
            try:
                niTask = nidaqmx.Task()
                niTask.ai_channels.add_ai_voltage_chan("Dev1/ai0", max_val=10)
                niTask.ai_channels.add_ai_voltage_chan("Dev1/ai1", max_val=10)
            except Exception as exc:
                log.exception("Connection to Siemens failed.", exc)
            else:
                self.niTask = niTask
        else:
            try:
                self.niTask.close()
            except AttributeError:
                pass  # No sensors.

    @pyqtSlot(bool)
    def showThyracontDetails(self, checked):
        """Show/hide add Thyracont sensor data."""
        self.lbThyracontDetails.setVisible(checked)
        self.lbThyracontDetailsDescription.setVisible(checked)

    @pyqtSlot()
    def readout(self):
        """Read the pressure and show it. Publish it, if checked."""
        data = {}
        if self.actionThyracontConnect.isChecked():
            try:
                thyracont = self.transmitter.pressure
                if detailed := self.actionThyracontDetails.isChecked():
                    thyracontPiezo = self.transmitter.piezo.pressure  # type: ignore
                    thyracontPirani = self.transmitter.pirani.pressure  # type: ignore
            except AttributeError:
                self.connectThyracont(False)
                self.actionThyracontConnect.setChecked(False)
            except (ConnectionResetError, ConnectionError, AssertionError) as exc:
                self.lbThyracont.setText("Err")
                self.lbError.setText(f"{exc}")
            except Exception as exc:
                log.warning(f"Get pressure error {exc}.")
            else:
                self.lbThyracont.setText(f"{thyracont} {self.unit}")
                data["precisionPressure"] = thyracont
                if detailed:
                    self.lbThyracontDetails.setText(
                        f"{thyracontPiezo} {self.unit}\n{thyracontPirani} "
                        f"{self.unit}"
                    )
                    data["thyracontPiezo"] = thyracontPiezo
                    data["thyracontPirani"] = thyracontPirani
        if self.actionSiemensConnect.isChecked():
            try:
                cell, panel = self.niTask.read(
                    number_of_samples_per_channel=self.averaging
                )
            except (nidaqmx.DaqError, AttributeError):
                pass
            else:
                # TODO when voltage too low, an error happened.
                cell = np.mean(cell) * 124.91118096 - 246.40557883
                panel = np.mean(panel) * 199.85568833 - 396.06299701
                self.lbCell.setText(f"{cell:g} mbar")
                self.lbPanel.setText(f"{panel:g} mbar")
                data["cellPressure"] = cell
                data["panelPressure"] = panel
        if data:
            try:
                self.publisher.send_legacy(data)
            except Exception as exc:
                log.exception("Publisher error.", exc_info=exc)

    @pyqtSlot()
    def setHigh(self):
        """Adjust the high value."""
        dialog = Calibration.Calibration()
        if dialog.exec():
            if dialog.value > 0:
                self.transmitter.set_high(dialog.value)
            else:
                self.transmitter.set_high()

    @pyqtSlot()
    def setLow(self):
        """Adjust the low value."""
        dialog = Calibration.Calibration()
        if dialog.exec():
            if dialog.value > 0:
                self.transmitter.set_low(dialog.value)
            else:
                self.transmitter.set_low()

    @pyqtSlot()
    def configureThyracont(self):
        """Configure the Thyracont sensor."""
        try:
            dialog = Configuration.Configuration(self.transmitter)
        except AttributeError as exc:
            # TODO tell user
            log.exception("AttributeError on configuration", exc_info=exc)
            return
        if dialog.exec():
            keys = dialog.changed.keys()
            data = dialog.changed
            log.debug(data)
            self.transmitter.adapter.connection.timeout = 1000  # type: ignore
            if "low" in keys or "high" in keys or "mode" in keys:
                mode = data["mode"]
                if mode == "direct":
                    self.transmitter.set_direct_sensor_transition(
                        transition_point=data["low"]
                    )
                else:
                    self.transmitter.set_continuous_sensor_transition(
                        low=data["low"], high=data["high"]
                    )
            if "factor" in keys:
                self.transmitter.pirani.gas_factor = data["factor"]  # type: ignore
            if "display" in keys:
                self.transmitter.display_data = data["display"]
            self.transmitter.adapter.connection.timeout = 100  # type: ignore

    def showInformation(
        self, title, text, detailedText=None, icon=QMessageBox.Icon.Information
    ):
        """Show some information to the user."""
        self.info = QtWidgets.QMessageBox()
        info = self.info
        info.setIcon(icon)
        info.setWindowTitle(title)
        info.setText(text)
        if detailedText is not None:
            info.setDetailedText(detailedText)
        info.exec()


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:  # Print log to console.
        log.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = PressureReader()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

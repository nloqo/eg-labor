"""
Module for the settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from qtpy import QtWidgets, uic
from qtpy.QtCore import Slot as pyqtSlot  # type: ignore

from pymeasure.instruments.thyracont.smartline_v2 import VSR


class Configuration(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    bbMode: QtWidgets.QComboBox
    sbLow: QtWidgets.QDoubleSpinBox
    sbHigh: QtWidgets.QDoubleSpinBox
    sbFactor: QtWidgets.QDoubleSpinBox
    bbDisplay: QtWidgets.QComboBox
    buttonBox: QtWidgets.QDialogButtonBox

    transmitter: VSR

    def __init__(self, transmitter):
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/Configuration.ui", self)
        self.show()

        self.pbReset = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.Reset
        )
        self.pbReset.clicked.connect(self.readConfiguration)

        self.transmitter = transmitter
        self.readConfiguration()

        # Connect slots
        self.bbMode.currentTextChanged.connect(self.modeChanged)
        self.sbLow.valueChanged.connect(self.lowChanged)
        self.sbHigh.valueChanged.connect(self.highChanged)
        self.sbFactor.valueChanged.connect(self.factorChanged)
        self.bbDisplay.currentTextChanged.connect(self.displayChanged)

    def readConfiguration(self):
        """Read and show the current configuration."""
        transition = self.transmitter.get_sensor_transition()
        if transition == "direct":
            self.bbMode.setCurrentText("direct")
            self.sbLow.setValue(1)
            self.sbHigh.setValue(0)
            self.sbHigh.setEnabled(False)
        elif transition == "continuous":
            self.bbMode.setCurrentText("continuous")
            self.sbLow.setValue(5)
            self.sbHigh.setValue(15)
            self.sbHigh.setEnabled(True)
        elif transition[0] == "D":
            self.bbMode.setCurrentText("direct")
            self.sbLow.setValue(float(transition[1:]))
            self.sbHigh.setValue(0)
            self.sbHigh.setEnabled(False)
        elif transition[0] == "F":
            values = transition[1:].split("T")
            self.bbMode.setCurrentText("continuous")
            self.sbLow.setValue(float(values[0]))
            self.sbHigh.setValue(float(values[1]))
            self.sbHigh.setEnabled(True)
        factor = self.transmitter.pirani.gas_factor  # type: ignore
        self.sbFactor.setValue(factor)
        source: str = self.transmitter.display_data  # type: ignore
        self.bbDisplay.setCurrentText(source)
        # reset changed dictionary.
        self.changed = {}

    @pyqtSlot(str)
    def modeChanged(self, mode):
        """En/Disable the high field and store the change."""
        self.sbHigh.setEnabled(mode == "continuous")
        self.changed["mode"] = mode
        self.changed["low"] = self.sbLow.value()
        self.changed["high"] = self.sbHigh.value()

    @pyqtSlot(float)
    def lowChanged(self, value):
        """Store the change."""
        self.changed["low"] = value
        self.changed["mode"] = self.bbMode.currentText()

    @pyqtSlot(float)
    def highChanged(self, value):
        """Store the change."""
        self.changed["high"] = value
        self.changed["mode"] = self.bbMode.currentText()

    @pyqtSlot(float)
    def factorChanged(self, value):
        """Store the change."""
        self.changed["factor"] = value

    @pyqtSlot(str)
    def displayChanged(self, text):
        """Store the change."""
        self.changed["display"] = text

    @pyqtSlot()
    def accept(self):
        """Implement the changes."""
        # is executed, if pressed on a button with the accept role
        super().accept()  # make the normal accept things

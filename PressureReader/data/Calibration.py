# -*- coding: utf-8 -*-
"""
Window asking for the calibrating values.

Created on Mon Jul 26 14:23:15 2021

@author: moneke
"""

from PyQt6 import QtWidgets, uic
from PyQt6.QtCore import pyqtSlot


class Calibration(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self, *args, **kwargs):
        """Initialize the dialog with the programName as argument of the settings."""
        # Use initialization of parent class QDialog.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.loadUi("data/Calibration.ui", self)
        self.show()

    @pyqtSlot()
    def accept(self):
        """Store the value and accept."""
        # is executed, if pressed on a button with the accept role
        self.value = self.sbValue.value()
        super().accept()  # make the normal accept things

"""
Main file of the WavemeterReader.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging
import sys

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
import pint

from devices import intercom, high_finesse

# Local packages.
from data import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())

u = pint.UnitRegistry()


class WavemeterReader(QtWidgets.QMainWindow):
    """Define the main window and essential methods of the program."""

    def __init__(self, *args, **kwargs):
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.loadUi("data/WavemeterReader.ui", self)
        self.show()
        self.signals = self.Signals()
        self.signals.wavelength.connect(self.show_wavelength)
        self.signals.frequency.connect(self.show_frequency)
        self.signals.power.connect(self.show_power)

        self.readTimer = QtCore.QTimer()
        self.readTimer.timeout.connect(self.readout)

        self.publisher = intercom.Publisher()

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName("WavemeterReader")
        self.setSettings()

        # Connect actions to slots.
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)
        self.actionConnect.triggered.connect(self.connect)
        self.actionTimer.triggered.connect(self.startTimer)
        self.actionCallback.triggered.connect(self.setupCallback)

        self.restoreSettings()
        self.actionConnect.setChecked(True)
        self.connect(True)
        # Buttons
        log.info("WavemeterReader initialized.")

    class Signals(QtCore.QObject):
        wavelength = QtCore.pyqtSignal(int, int, float)
        frequency = QtCore.pyqtSignal(int, int, float)
        power = QtCore.pyqtSignal(int, int, float)
        data_ready = QtCore.pyqtSignal(int, int, int, float)

    def restoreSettings(self):
        """Restore UI settings."""
        settings = QtCore.QSettings()
        self.sbSerial.setValue(settings.value("serial", type=int))
        self.sbSerial2.setValue(settings.value("serial2", type=int))
        self.leName.setText(settings.value("name"))
        self.leName2.setText(settings.value("name2"))

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        settings = QtCore.QSettings()
        settings.setValue("serial", self.sbSerial.value())
        settings.setValue("serial2", self.sbSerial2.value())
        settings.setValue("name", self.leName.text())
        settings.setValue("name2", self.leName2.text())
        # accept the close event (reject it, if you want to do something else)
        event.accept()

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        settingsDialog = Settings.Settings()
        if settingsDialog.exec():
            self.setSettings()

    def setSettings(self):
        """Apply new settings."""
        # TODO apply changes to variables.
        settings = QtCore.QSettings()
        self.publisher.port = settings.value("publisherPort", type=int)
        self.readTimer.setInterval(settings.value("readoutInterval", 100, type=int))

    @pyqtSlot(bool)
    def connect(self, checked):
        """Connect the wavemeter."""
        if checked:
            try:
                self.wlm = high_finesse.Wavemeter()
            except Exception as exc:
                log.exception("Start reading error.", exc_info=exc)
                self.actionConnect.setChecked(False)

    @pyqtSlot()
    def readout(self):
        """Read the wavemeter."""
        try:
            wl = self.wlm.wavelength
            f = self.wlm.frequency
        except Exception as exc:
            log.exception("Readout error.", exc_info=exc)
            self.actionConnect.setChecked(False)
            self.readTimer.stop()
            return
        self.lbWavelength.setText(f"{wl} nm")
        self.lbFrequency.setText(f"{f} THz")
        try:
            self.publisher.send({"wavelength": wl * u.nm, "frequency": f * u.THz})
        except Exception as exc:
            log.exception("Publisher error.", exc_info=exc)

    @pyqtSlot(bool)
    def startTimer(self, checked):
        """Start / stop reading the wavemeter."""
        if checked and self.actionConnect.isChecked():
            self.readTimer.start()
        else:
            self.readTimer.stop()

    @pyqtSlot(bool)
    def setupCallback(self, checked):
        """Setup the callback function."""
        if not self.actionConnect.isChecked():
            return  # no device present.
        if checked:
            self.wlm.setup_callback(self.data_ready)
        else:
            self.wlm.setup_callback()

    def data_ready(self, version, mode, intVal, dblVal, Res1=None):
        """Callback method."""
        if mode in (
            high_finesse.wlmConst.cmiWavelength1,
            high_finesse.wlmConst.cmiWavelength2,
            high_finesse.wlmConst.cmiWavelength3,
            high_finesse.wlmConst.cmiWavelength4,
            high_finesse.wlmConst.cmiWavelength5,
            high_finesse.wlmConst.cmiWavelength6,
        ):
            self.signals.wavelength.emit(version, intVal, dblVal)
            log.debug(("Wavelength received", version, mode, intVal, dblVal))
        elif mode in (
            high_finesse.wlmConst.cmiFrequency1,
            high_finesse.wlmConst.cmiFrequency2,
        ):
            self.signals.frequency.emit(version, intVal, dblVal)
            log.debug(("Frequency received", version, mode, intVal, dblVal))
        elif mode == high_finesse.wlmConst.cmiPower:
            self.signals.power.emit(version, intVal, dblVal)
            log.debug(("Power received", version, mode, intVal, dblVal))
        else:
            log.debug(("Data received", version, mode, intVal, dblVal))

    @pyqtSlot(int, int, float)
    def show_wavelength(self, version, timestamp, value):
        """Show the received wavelength."""
        if version == self.sbSerial2.value():
            self.lbWavelength2.setText(f"{value} nm")
            if name := self.leName2.text():
                self.publisher.send({f"{name}_wl": value if value > 0 else float("nan")})
        else:
            self.lbWavelength.setText(f"{value} nm")
            if name := self.leName.text():
                self.publisher.send({f"{name}_wl": value if value > 0 else float("nan")})

    @pyqtSlot(int, int, float)
    def show_frequency(self, version, timestamp, value):
        """Show the received frequency."""
        if version == self.sbSerial2.value():
            self.lbFrequency2.setText(f"{value} THz")
            if name := self.leName2.text():
                self.publisher.send({f"{name}_f": value})
        else:
            self.lbFrequency.setText(f"{value} THz")
            if name := self.leName.text():
                self.publisher.send({f"{name}_f": value})

    @pyqtSlot(int, int, float)
    def show_power(self, version, timestamp, value):
        """Show the received power."""
        if version == self.sbSerial2.value():
            self.lbPower2.setText(f"{value} µW")
            if name := self.leName2.text():
                self.publisher.send({f"{name}_P": value})
        else:
            self.lbPower.setText(f"{value} µW")
            if name := self.leName.text():
                self.publisher.send({f"{name}_P": value})


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:  # Print log to console.
        log.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = WavemeterReader()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

"""
Temperature Controller for HCPs TC038 oven class.

created on 23.11.2020 by Benedikt Moneke
"""

import datetime
import logging
import os
import sys

from pymeasure.instruments import hcp
from PyQt6 import uic, QtCore, QtWidgets
from PyQt6.QtCore import pyqtSlot
import pyqtgraph as pg
import pyvisa
from pyvisa.constants import StatusCode as pvs
try:
    from pymeasure.instruments.resources import find_serial_port
except ImportError:
    from devices.findDevices import find_serial_port

from pyleco.utils.data_publisher import DataPublisher

from data import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class TemperatureController(QtWidgets.QMainWindow):
    """For the HCP TC038 oven"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        uic.load_ui.loadUi("data/TemperatureController.ui", self)

        self.show()

        # INITIAL CONFIGURATION
        # get settings
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName("TemperatureController")
        self.settings = QtCore.QSettings()

        # Intercom.
        self.publisher = DataPublisher("Temp")

        # connect front panel and code
        self.btConnect.clicked.connect(self.connect)
        self.btSetSetpoint.clicked.connect(self.setSetpoint)
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)

        # a timer for regular readouts.
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateTemperatures)

        # more configuration
        self.setupPlot()
        log.info("TemperatureController initialized.")

    @pyqtSlot()
    def openSettings(self):
        settings = Settings.Settings()
        if settings.exec():
            # apply changes
            self.timer.setInterval(self.settings.value("interval"))

    def setupPlot(self):
        canvas = self.Temperatures

        # initialize lists
        self.TemperatureList = []
        self.SetpointList = []
        self.TimeList = []
        self.today = datetime.datetime.now(datetime.timezone.utc).date()
        # get line references with initial plots
        pen = pg.mkPen(color=(255, 0, 0))  # setpoint in red
        canvas.addLegend()
        self.lineRefs = [
            self.Temperatures.plot(self.TimeList, self.TemperatureList, name="Temperature"),
            self.Temperatures.plot(self.TimeList, self.SetpointList, name="Setpoint", pen=pen),
        ]
        # set axis description
        canvas.setLabel("left", "temperature (°C)")
        canvas.setLabel("bottom", "UTC seconds today")

    def closeEvent(self, event):
        # close connection to device
        log.info("Closing.")
        if self.btConnect.isChecked():
            self.connect(False)
        # save everything if desired
        self.saveData(0, len(self.TimeList))
        event.accept()  # accept the close event

    @pyqtSlot(bool)
    def connect(self, pressed):
        """Connect to the specified comport if not yet done."""
        if pressed:
            COM_number = self.settings.value("COM", 7, int)
            val = [None] * 3
            if not COM_number:
                data = self.settings.value("device", "", str).replace(" ", "").split(",")
                for i in range(min(len(data), 2)):
                    val[i] = int(data[i]) if data[i] else None
                if len(data) >= 3:
                    val[2] = data[2]
                COM = find_serial_port(val[0], val[1], val[2])
            else:
                COM = f"ASRL{COM_number}"
            if self.settings.value("model") == "TC038":
                self.oven = hcp.TC038(COM)
            else:
                self.oven = hcp.TC038D(COM)
            self.updateTemperatures()
            self.timer.start(self.settings.value("interval", 5000, int))
        else:
            self.timer.stop()
            try:
                # close connection if not pressed
                del self.oven
            except Exception as exc:
                log.exception("Error closing connection.", exc)

    @pyqtSlot()
    def updateTemperatures(self):
        try:
            # read temperatures
            temperature = self.oven.temperature
            setpoint = self.oven.setpoint
        except pyvisa.VisaIOError as exc:
            if exc.error_code == pvs.error_timeout:
                self.btConnect.setChecked(False)
                self.connect(False)
            elif exc.error_code in (
                pvs.error_serial_parity,
                pvs.error_serial_framing,
                pvs.error_serial_overrun,
            ):
                return  # Happen regularly for TC038
            mb = QtWidgets.QMessageBox()
            mb.setIcon(QtWidgets.QMessageBox.Icon.Information)
            mb.setWindowTitle("Connection error")
            mb.setText(f"At update {type(exc).__name__} VISA occurred: {exc}")
            mb.exec()
            return
        except ValueError as exc:
            log.exception("Update temperatures error.", exc)
            return  # Happens after some Serial errors at TC038D
        except Exception as exc:
            self.btConnect.setChecked(False)
            self.connect(False)
            mb = QtWidgets.QMessageBox()
            mb.setIcon(QtWidgets.QMessageBox.Icon.Critical)
            mb.setWindowTitle("Connection error")
            mb.setText(f"At update {type(exc).__name__} occurred: {exc}")
            mb.exec()
            return
        # display temperatures
        self.currentTemperature.setText(f"Temperature: {temperature}°C")
        self.currentSetpoint.setText(f"Current: {setpoint}°C")
        try:
            self.publisher.send_legacy({"ovenTemperature": temperature})
        except Exception as exc:
            log.exception("Publisher error.", exc)
        # add the new values
        now = datetime.datetime.now(datetime.timezone.utc)
        today = datetime.datetime.combine(self.today, datetime.time(), datetime.timezone.utc)
        self.TimeList.append((now - today).total_seconds())
        self.TemperatureList.append(temperature)
        self.SetpointList.append(setpoint)
        # manage data lists
        # define number of datapoints above length
        additionalLength = self.settings.value("saveInterval", 1000, int)
        if (
            len(self.TemperatureList)
            > self.settings.value("listLength", type=int) + additionalLength
        ):
            # if Data has to be saved, save it
            self.saveData(0, additionalLength)
            # cut the lists
            self.TimeList = self.TimeList[additionalLength:]
            self.TemperatureList = self.TemperatureList[additionalLength:]
            self.SetpointList = self.SetpointList[additionalLength:]
        # plot temperatures
        self.lineRefs[0].setData(self.TimeList, self.TemperatureList)
        self.lineRefs[1].setData(self.TimeList, self.SetpointList)

    @pyqtSlot()
    def setSetpoint(self):
        """Set the temperature setpoint."""
        try:
            self.oven.setpoint = self.newSetpoint.value()
        except Exception as exc:
            self.btConnect.setChecked(False)
            self.connect(False)
            log.exception("Error setSetpoint.", exc)
            mb = QtWidgets.QMessageBox()
            mb.setIcon(QtWidgets.QMessageBox.Icon.Critical)
            mb.setText(f"At set setpoint {type(exc).__name__} occurred: {exc}")
            mb.setWindowTitle("Connection error")
            mb.exec()

    def saveData(self, firstIndex=0, count=1):
        """
        Saves the Data from the Lists into a file.

        Parameters
        ----------
        firstIndex : int
            First index of data to be saved. The default is 0.
        count : int
            Count of datapoints to be saved. The default is 1.
        """
        if self.cbSave.isChecked():
            # only if save is desired
            savePath = self.settings.value("savePath")
            if not os.path.isdir(savePath):
                # no save path directory exists: ask for path
                savePath = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
                print(savePath)  # debug
                if savePath == "":
                    # save path was cancelled: abort saving
                    self.cbSave.setChecked(False)
                    return
            name = f"TC{self.today.year}-{self.today.month}-{self.today.day}.txt"
            if not os.path.isfile(savePath + os.sep + name):
                # file does not yet exist: create header
                with open(savePath + os.sep + name, "w") as file:
                    file.write("#Time in utc seconds today\tSetpoint in °C\tTemperature in °C\n")
            with open(savePath + os.sep + name, "a") as file:
                for i in range(firstIndex, firstIndex + count):
                    file.write(
                        f"{self.TimeList[i]}\t{self.SetpointList[i]}\t{self.TemperatureList[i]}\n"
                    )


if __name__ == "__main__":  # if this is the started script file
    if "-v" in sys.argv:
        log.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # start an application
    mainwindow = TemperatureController()  # start the main window
    app.exec()  # start the application with its Event loop

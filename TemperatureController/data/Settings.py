# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 19:02:38 2020

@author: THG-User
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot


class Settings(QtWidgets.QDialog):

    def __init__(self):
        super().__init__()

        uic.loadUi("data/Settings.ui", self)

        self.show()

        self.settings = QtCore.QSettings("NLOQO", "TemperatureController")

        self.readValues()

        # CONNECT BUTTONS
        self.btSavePath.clicked.connect(self.openFileDialog)
        # define RestoreDefaults button and connect it
        self.btRestoreDefaults = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults)
        self.btRestoreDefaults.clicked.connect(self.restoreDefaults)

    def readValues(self):
        self.sbCOM.setValue(self.settings.value("COM", 1, type=int))
        self.leDevice.setText(self.settings.value('device', type=str))
        self.bbModel.setCurrentText(self.settings.value('model', "TC038", str))
        self.sbInterval.setValue(self.settings.value("interval", 5000, int))
        self.sbPort.setValue(self.settings.value('port', 11100, type=int))
        self.sbDatapoints.setValue(self.settings.value("listLength", 7200, int))
        self.sbSaveInterval.setValue(self.settings.value("saveInterval", 1000, int))
        self.savePath = self.settings.value(
            "savePath", type=str,
            defaultValue="D:/Measurement Data/TemperatureController")
        self.leSavePath.setText(self.savePath)

    @pyqtSlot()
    def restoreDefaults(self):
        # restores the fields to standard values
        self.sbCOM.setValue(1)
        self.bbModel.setCurrentText("TC038")
        self.sbInterval.setValue(5000)
        self.sbDatapoints.setValue(7200)
        self.sbSaveInterval.setValue(1000)
        self.savePath = "D:/Measurement Data/TemperatureController"
        self.leSavePath.setText(self.savePath)

    @pyqtSlot()
    def accept(self):
        self.settings.setValue("COM", self.sbCOM.value())
        self.settings.setValue('device', self.leDevice.text())
        self.settings.setValue("model", self.bbModel.currentText())
        self.settings.setValue("interval", self.sbInterval.value())
        self.settings.setValue('port', self.sbPort.value())
        self.settings.setValue("listLength", self.sbDatapoints.value())
        self.settings.setValue("saveInterval", self.sbSaveInterval.value())
        self.settings.setValue("savePath", self.savePath)
        super().accept()

    @pyqtSlot()
    def openFileDialog(self):
        self.savePath = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
        self.leSavePath.setText(self.savePath)

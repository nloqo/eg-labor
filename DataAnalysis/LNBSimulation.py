"""
Simulation von LNB Phasenanpassungswinkel

von Felix Wanitschke.
"""


import numpy as np
import scipy.constants as cons


class Simulation:
    def __init__(self, *args, **kwargs):
        self.A_eo = 2.2454
        self.B_eo = 0.01242
        self.C_eo = 1.3005
        self.D_eo = 0.05313
        self.E_eo = 6.8972
        self.F_eo = 331.33

        self.A_o = 2.4272
        self.B_o = 0.01478
        self.C_o = 1.4617
        self.D_o = 0.05612
        self.E_o = 9.6536
        self.F_o = 371.216

        # zelmonInfraredCorrectedSellmeier1997a

    def n(
        self, lamd, T, oe="o", angle=0
    ):  # Temperaturkorrektur ausgehend von SNLO, da Sellmeier Koeffizienten bei 21 Grad bestimmt.
        # Korrektur lässt n auf 3 nachkommastellen mit SNLO übereinstimmen
        """Bestimmt den Temperaturangepassten Brechungsindex von Lithiumniobat 5% für ordentlichen
        und außerordentlichen Strahl bei gegebener Wellenlänge in Einfallswinkel"""

        angle = 2 * np.pi / 360 * angle
        n_o = np.sqrt(
            1
            + (
                self.A_o * lamd**2 / (lamd**2 - self.B_o)
                + self.C_o * lamd**2 / (lamd**2 - self.D_o)
                + self.E_o * lamd**2 / (lamd**2 - self.F_o)
            )
        )

        n_eo = np.sqrt(
            1
            + (
                self.A_eo * lamd**2 / (lamd**2 - self.B_eo)
                + self.C_eo * lamd**2 / (lamd**2 - self.D_eo)
                + self.E_eo * lamd**2 / (lamd**2 - self.F_eo)
            )
        )
        # print(n_o, n_eo)
        if lamd < 1.1:  # Werte der temperaturanpassung aus SNLO für 1064 nm bei 294.15 K
            if oe == "o":
                return n_o * (1 + (T - 294.15) * 5.3815 * 10 ** (-6))
            else:
                return np.sqrt(
                    1 / (((np.cos(angle) / n_o) ** 2) + ((np.sin(angle) / n_eo) ** 2))
                ) * (1 + (T - 294.15) * 2.2557 * 10 ** (-5))
        else:  # Werte der temperaturanpassung aus SNLO für 3530 nm bei 294.15 K
            if oe == "o":
                return n_o * (1 + (T - 294.15) * 3.197 * 10 ** (-6))
            else:
                return np.sqrt(
                    1 / (((np.cos(angle) / n_o) ** 2) + ((np.sin(angle) / n_eo) ** 2))
                ) * (1 + (T - 294.15) * 1.8938 * 10 ** (-5))

    def omega(self, wavelength):
        return 2 * np.pi * cons.c / wavelength

    def eta(self, lamd, T, oe="o", angle=0):
        """Just to make calculations more compact."""
        return self.n(lamd, T, oe, angle) / lamd

    def rad(self, grad):
        return grad / 360 * 2 * np.pi

    def grad(self, rad):
        return rad / (2 * np.pi) * 360

    def calcPhasematchingAngle(self, lambdaSignal, T):  # nikogosyanNewFormulasCalculation1986
        """Bestimmt den Phasenanpaasungswinkel bei einer bestimmten Welenlänge und einer gegebenen
        Pumpwellenlänge. Typ: ooe, negative uniaxial"""
        lambdaPump = 1.064142
        lambdaIdler = 1 / (1 / lambdaPump - 1 / lambdaSignal)
        A = self.eta(lambdaSignal, T)
        B = self.eta(lambdaIdler, T)
        C = self.eta(lambdaPump, T)
        F = self.eta(lambdaPump, T, "e", 90)
        U = (A + B) ** 2 / C**2
        W = (A + B) ** 2 / F**2
        tan_theta = np.sqrt((1 - U) / (W - 1))
        theta_ = np.arctan(tan_theta)
        theta = theta_ / (2 * np.pi) * 360
        # cutangle_c1 = 44.85  # NC24
        # cutangle_c2 = 44.80  # NC25
        # angle_c1 = theta - cutangle_c1
        # angle_c2 = theta - cutangle_c2
        # angle_c2_mrad = self.rad(angle_c2) * 1000
        # angle_c1_mrad = self.rad(angle_c1) * 1000
        # print(f"Theta = {theta}")
        # print(f"Winkel Kristall 1 = {angle_c1} Grad, {angle_c1_mrad} mrad; ",
        # f"Winkel Kristall 1 = {angle_c2}, {angle_c2_mrad} mrad")
        return theta

    def verstellweg(self, mrad):
        """Bestimmt den Verstellweg der Justageschraube für eine Verkippung der Plattform um mrad"""
        angle_per_rev = 8  # mrad
        travel_per_rev = 318  # µm
        travel_per_angle = travel_per_rev / angle_per_rev  # µm/mrad
        travel = travel_per_angle * mrad
        return travel  # in µm

    def steps(self, travel):
        """Gibt die notwendigen steps bei gegebenen Einstellungen an, um den gewünschten Verstellweg
        zu erreichen (ZST213)"""
        microsteps = 64
        steps_per_rev = 24
        gear = 40.866
        steps_per_mum = (steps_per_rev * gear * microsteps) / 1000  # steps/µm
        return steps_per_mum * travel

    def steps_lamd(self, lamd):
        """Brechnet die notwenigen steps um aus der horizontalen Ausrichtung der Kristalle
        Phasematching zu erreichen, so abändern dass von bestimmter Position relativ aus berechnet
        wird"""
        angle_c1_mrad = self.calcPhasematchingAngle(lamd)[1]
        angle_c2_mrad = self.calcPhasematchingAngle(lamd)[2]
        travel_c1 = self.verstellweg(angle_c1_mrad)
        travel_c2 = self.verstellweg(angle_c2_mrad)
        steps_c1 = self.steps(travel_c1)
        steps_c2 = self.steps(travel_c2)
        # print(f"Bei \lambda = {lamd} µm : Steps für Kristall 1: {steps_c1} "
        # f"({travel_c1} µm Verstellweg), Steps für Kristall 2: {steps_c2} "
        # f"({travel_c2} µm Verstellweg)")
        return steps_c1, steps_c2

    # steps_lamd(3.500)


if __name__ == "__main__":
    hallo = Simulation()
    print(hallo.calcPhasematchingAngle(3.550, 273.15 + 30))
    print(hallo.n(3.550, oe="o", angle=45, T=298.15))

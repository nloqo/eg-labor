# -*- coding: utf-8 -*-
"""
Strahlprofile auswerten

Created on Thu Jul 14 18:50:06 2022 by Benedikt Moneke
"""

# %% Initialisieren
# Initialisieren

# Standard Pakete
from typing import Any, Optional

# 3rd party
import matplotlib.pyplot as plt      # Plotten
import matplotlib.ticker as tic      # Ticks für Plots
import numpy as np        # Mathezeug
import pyqtgraph as pg
import scipy.constants as scnst
from scipy.optimize import curve_fit as fit  # Fitten

# Local packages
import analysis.general as ang
from analysis import math as anm
from analysis.plotting import configurePlot as pl, configureMatplotlib
from analysis import gaussianBeams as gb
from analysis.beam_camera import import_image as import_beam_camera_image
from analysis.beam_profiler import import_image as import_beam_profiler_image
from analysis.fit_gaussian_beam import get_center_index, fit_gaussian_cuts as fitGaussianCuts, \
    fit2dGaussian, max_from_pixels

# Configure Matplotlib
configureMatplotlib(fontSize=11, fontFamily='serif', saveFormat='png', figsize=(18, 6))

# Configure for pyqtgraph
%gui qt
pg.setConfigOption('imageAxisOrder', 'row-major')


# Fitfunktionen

"""Mathematische Funktionen zur Anpassung"""


ellipseRadius = anm.ellipse_radius


gaussFluenz = gb.peak_fluence


"# Daten einlesen"

def importBeamProfiler(name: str, key: str = 'IRPD2', base: str = "M:") -> np.ndarray:
    """Imports beamprofiler data. In "Measurement Data/Beamprofiler/"."""
    global pixelLength
    data, pixelLength = import_beam_profiler_image(name=name)
    return np.array(data[key])


"""# Plotting"""


def plotImage(image: np.ndarray) -> None:
    """Plot the image and the center area."""
    img2 = image.copy()
    img2[img2 < 0.3 * np.max(img2)] = 0

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))
    ax1.imshow(image, cmap=config['plt_cmap'])
    ax2.imshow(image, cmap=config['plt_cmap'])
    plt.show()


def plotAnalysis(image: np.ndarray, energy: float = 1, radius: float = 100,
                 zero_offset: bool = False) -> tuple[
                     dict[int | str, tuple[Any, ...]],
                     dict[int | str, list[list[Any]]],
                     dict[int, np.ndarray],
                     dict[int, tuple[int, int, float]]]:
    """Plot the image analysis.

    Ellipse and `w` is the 1/e squared radius
    """
    par, cov, obl, oblPar = fitGaussianCuts(image=image, radius=radius, zero_offset=zero_offset)
    xcircle, ycircle = ang.ellipse(angle=np.linspace(0, 2 * np.pi, 100),
                                   x0=par[0][1], y0=par[90][1],
                                   r1=2 * par['ellipse'][0],
                                   r2=2 * par['ellipse'][1],
                                   phi=-par['ellipse'][2])
    # das minus wird benötigt, da imshow die Achse anders hat.

    # Plotten
    fig, ((imageA, histogram), (horizontal, vertical), (dia1, dia2)) = plt.subplots(
        3, 2, figsize=(12, 10))

    imageA.plot(xcircle, ycircle, color=config['marking'])
    # imageA.plot(par[0][1], par[90][1], color=config['marking'],
    #             marker=config['marker'])
    imageA.imshow(image, cmap=config['plt_cmap'])
    imageA.xaxis.set_major_formatter(tic.FuncFormatter(
        lambda x, pos: f"{x * pixelLength:1.0f}"))
    imageA.xaxis.set_major_locator(plt.MultipleLocator(1000 / pixelLength))
    imageA.yaxis.set_major_formatter(tic.FuncFormatter(
        lambda y, pos: f"{y * pixelLength:1.0f}"))
    imageA.yaxis.set_major_locator(plt.MultipleLocator(1000 / pixelLength))
    imageA.set_xlabel("horizontal position in µm")
    imageA.set_ylabel("vertical position in µm")

    histogram.hist(image.flatten(), bins=50)

    horizontal.plot(obl[0][:, 0], obl[0][:, 1],
                    label=f"y0={pixelLength*par[0][1]:.1f} µm")
    horizontal.plot(obl[0][:, 0], ang.gaussian(obl[0][:, 0], *par[0]),
                    label=f"w={2*pixelLength*par[0][2]:.2f} µm")
    horizontal.set_xlabel("horizontal position in µm")

    vertical.plot(obl[90][:, 0], obl[90][:, 1],
                  label=f"x0={pixelLength*par[90][1]:.1f} µm")
    vertical.plot(obl[90][:, 0], ang.gaussian(obl[90][:, 0], *par[90]),
                  label=f"w={2*pixelLength*par[90][2]:.2f} µm")
    vertical.set_xlabel("vertical position in µm")

    dia1.plot(obl[45][:, 0], obl[45][:, 1], label="data")
    dia1.plot(obl[45][:, 0], ang.gaussian(obl[45][:, 0], *par[45]),
              label=f"w={2*pixelLength*par[45][2]*np.sqrt(2):.2f} µm")
    dia1.set_xlabel("oblique 45° position in µm")

    dia2.plot(obl[135][:, 0], obl[135][:, 1], label="data")
    dia2.plot(obl[135][:, 0], ang.gaussian(obl[135][:, 0], *par[135]),
              label=f"w={2*pixelLength*par[135][2]*np.sqrt(2):.2f} µm")
    dia2.set_xlabel("oblique 135° position in µm")

    for ax in (horizontal, vertical):
        ax.xaxis.set_major_formatter(tic.FuncFormatter(
            lambda x, pos: f"{x * pixelLength:1.0f}"))
        ax.xaxis.set_major_locator(
            plt.MultipleLocator(1000 / pixelLength))
        ax.legend()

    for ax in (dia1, dia2):
        ax.xaxis.set_major_formatter(tic.FuncFormatter(
            lambda x, pos: f"{x * pixelLength * np.sqrt(2):1.0f}"))
        ax.xaxis.set_major_locator(
            plt.MultipleLocator(1000 / pixelLength / np.sqrt(2)))
        pl(axes=ax)

    plt.show()

    # calculate max fluenz
    gf = gaussFluenz(energy, par[0][2] * pixelLength, par[90][2] * pixelLength)
    print(f"Maximale Fluenz nach Gaußanpassung: {gf:.3} J/cm^2")
    try:
        background = np.mean((par[0][3], par[45][3], par[90][3], par[135][3]))
    except IndexError:
        background = 0
    mean_amplitude = np.mean((par[0][0], par[45][0], par[90][0], par[135][0]))
    mf = gf * (np.max(image) - background) / mean_amplitude
    print(f"Maximale Fluenz der Spitzen gemäß Gaußanpassung: {mf:.3} J/cm^2")
    return par, cov, obl, oblPar


def plotDetails(image, data=None):
    """Plot the image and all cuts detailed."""
    if data is None:
        par, cov, obl, oblPar = fitGaussianCuts(image=image,
                                                radius=500 / pixelLength)
    else:
        par, cov, obl, oblPar = data

    # Ellipse berechnen
    xcircle, ycircle = ang.ellipse(angle=np.linspace(0, 2 * np.pi, 100),
                                   x0=par[0][1], y0=par[90][1],
                                   r1=2 * par['ellipse'][0],
                                   r2=2 * par['ellipse'][1],
                                   phi=-par['ellipse'][2])

    # Plot the image with overlaid ellipse and cross.
    fig = plt.figure(figsize=(10, 6))
    ax = plt.gca()
    ax.plot(xcircle, ycircle, color=config['marking'])
    ax.plot(par[0][1], par[90][1], color=config['marking'],
            marker=config['marker'])
    pos = ax.imshow(image, cmap=config['plt_cmap'])
    ax.xaxis.set_major_formatter(tic.FuncFormatter(
        lambda x, pos: f"{x * pixelLength:1.0f}"))
    ax.xaxis.set_major_locator(plt.MultipleLocator(1000 / pixelLength))
    ax.yaxis.set_major_formatter(tic.FuncFormatter(
        lambda y, pos: f"{y * pixelLength:1.0f}"))
    ax.yaxis.set_major_locator(plt.MultipleLocator(1000 / pixelLength))
    ax.set_xlabel("horizontal position in µm")
    ax.set_ylabel("vertical position in µm")
    fig.colorbar(pos, ax=ax)
    pl(xname="daBeamProfiler/abc", leg=False)

    # Plot all cuts
    fig, axes = plt.subplots(2, 4, figsize=(12, 6))
    i = 0
    j = 0
    for key in oblPar.keys():
        axes[i, j].plot(obl[key][:, 0], obl[key][:, 1], label=f"{key}°")
        axes[i, j].plot(obl[key][:, 0],
                        ang.gaussian(obl[key][:, 0], *par[key]),
                        label=f"w={2*pixelLength*par[key][2]*oblPar[key][2]:.2f} µm")
        axes[i, j].xaxis.set_major_formatter(tic.FuncFormatter(
            lambda x, pos: f"{x * pixelLength:1.0f}"))
        axes[i, j].xaxis.set_major_locator(
            plt.MultipleLocator(1000 / pixelLength))
        pl(axes=axes[i, j])
        j += 1
        if j == 4:
            i = 1
            j = 0
    print("Ellipse: ", par['ellipse'])


def plot2D(image: np.ndarray, zero_offset: bool = False) -> list[Any]:
    # Fit
    X, Y = np.meshgrid(range(len(image[0])), range(len(image[:, 0])))
    try:
        popt, pcov = fit2dGaussian(image, radius=500 / pixelLength, zero_offset=zero_offset)
    except RuntimeError as exc:
        print(f"Fit failed: {exc}")
        angepasst = None
    else:
        angepasst = ang.gaussian2D(X, Y, *popt)
        print("Maximale Fluenz nach Gaußanpassung: "
              f"{gaussFluenz(energy, popt[3]*pixelLength, popt[4]*pixelLength):.3} J/cm^2")

    # Plot
    fig, (orig, anpassung) = plt.subplots(1, 2, figsize=(14, 10),
                                          subplot_kw={'projection': '3d'})
    orig.plot_surface(X, Y, img, cmap=config['plt_cmap'])
    if angepasst is not None:
        anpassung.plot_surface(X, Y, angepasst, cmap=config['plt_cmap'])
        print(f"maximale Differenz {np.max(abs(img-angepasst)):.2f}, "
              f"Zentrum bei {popt[1:3]*pixelLength}µm, "
              f"Strahlradius ist {popt[3:5]*2*pixelLength}µm, "
              f"Winkel {popt[5]}, Maximum der Daten {np.max(image)}, "
              f"Maximum der Anpassung {np.max(angepasst)}")
    plt.show()
    return popt


"""Standard Konfigurationen der Farben."""
config = {
    # 'plt_cmap': "gist_rainbow",
    # 'plt_cmap': "nipy_spectral",
    'plt_cmap': "gist_ncar",
    # 'pg_cmap': "spectrum",
    'pg_cmap': "bipolar",
    'marking': "black",
    'marker': "+",
}
# Alternativ: 'plasma' für beide und C1 für marking


# %% Bild Datei laden
"""Bild Datei laden
==============="""
# %%% Beamcamera Bild laden
"""Bild laden BeamCamera"""
# Parameter
name = "2024_03_01T10_34_26"  # set to "" in order to use day, month etc.

# Import selbst
img, pixelLength = import_beam_camera_image(
    name=name, year=None, month=None, day=None, hour=None,
    minute=None, second=None, base="M:",
)
plotImage(img)


# %%% BeamProfiler Bild laden
"""Bild laden BeamProfiler"""

# Import selbst
img = importBeamProfiler(name="2024_02_28T13_34_47", key="idler-cw")
plotImage(img)


# %% Bilder in extra Fenster anzeigen
"""==============================="""
# %%% Extra Fenster resetten
"""Wenn das Fenster geschlossen wurde, kann man es hier resetten:"""
try:
    del imageView  # noqa
except NameError:
    pass


# %%% Bild in extra Fenster anzeigen
"""Öffnet das Bild in einem extra Fenster zum detaillierten Anschauen"""
try:
    imageView.setImage(img)
except NameError:
    # Fenster ist unbekannt, erzeuge neues.
    imageView = pg.image(img, title="BeamProfiling - single")
    imageView.setPredefinedGradient(config['pg_cmap'])
    imageView.setImage(img)


# %% Analysieren mit Schnitten
"""
Analysiere das Bild mit Hilfe von Schnitten
===========================================
"""


"""Energie des Strahls"""
energy = 10.000e-3  # J
zero_offset = False  # force the offset to zero

# """Maximale Fluenz des Bildes"""
x0i, y0i = get_center_index(img, threshold=0.3 * np.max(img))
print(f"Maximale Fluenz gemäß Pixel {max_from_pixels(img, energy, pixelLength):.4} J/cm^2, Schwerpunkt bei x={x0i}, y={y0i}")


# """Anpassung von Gaußkurven"""
data = plotAnalysis(img, energy, radius=1000 / pixelLength, zero_offset=zero_offset)


# %% Plot Details
"""Plotte Details des Bildes"""

plotDetails(img, data)


# %% 2D Gauss Fit
"""Plotte das Bild mit 2D Gauss Anpassung"""

"""Warning: takes several seconds, up to a minute!"""
plot2D(img, zero_offset=zero_offset)


# %% Mehrere Bilder analysieren - Initialisierung
"""Mehrere Bilder analysieren - Initialisierung
=========================================="""


def plot_fitted(index: int = 0,
                center: Optional[list[tuple[int, int]]] = None,
                show_markings: bool = True, print_width: bool = False) -> None:
    """Plot the image with fitted data."""
    pixelLength = pixel_lengths[index]
    image = images[index]

    plt.figure(figsize=(10, 6))
    ax = plt.gca()
    angle = np.linspace(0, 2 * np.pi, 100)
    if show_markings and center:
        xcircle, ycircle = ang.ellipse(
            angle,
            center[index][0] / pixelLength,
            center[index][1] / pixelLength,
            w[0][index] / pixelLength,
            w[1][index] / pixelLength)
        ax.plot(xcircle, ycircle, color="black")
        ax.plot(center[index][0], center[index][1], color="black", marker="+")
    ax.imshow(image, cmap=config['plt_cmap'])
    # change axis labels
    lx, ly = image.shape
    stepsize_x = (lx // 4 * pixelLength) // 10 * 10 / pixelLength
    stepsize_y = (ly // 4 * pixelLength) // 10 * 10 / pixelLength
    ax.xaxis.set_major_formatter(tic.FuncFormatter(
        lambda x, pos: f"{x*pixelLength:1.0f}"))
    ax.xaxis.set_major_locator(plt.MultipleLocator(stepsize_x))
    ax.yaxis.set_major_formatter(tic.FuncFormatter(
        lambda y, pos: f"{y*pixelLength:1.0f}"))
    ax.yaxis.set_major_locator(plt.MultipleLocator(stepsize_y))
    try:
        ax.set_title(f"{names[index]}, position: {positions[index]:.4}")
    except KeyError:
        ax.set_title(names[index])
    ax.set_xlabel("horizontal position in µm")
    ax.set_ylabel("vertical position in µm")
    pl(leg=False)
    if print_width:
        print(w[0][index], w[1][index])


def extractBeamRadii(names, fit2d: bool = False, **kwargs) -> tuple[np.ndarray, np.ndarray, list]:
    # Fits
    center = []  # Center coordinates
    w = []  # Width of 1D fits.
    w2 = []  # Widths according to 2D fits.
    for i in range(len(images)):
        image = images[i]
        pl = pixel_lengths[i]
        try:
            par, cov, obl, oblPar = fitGaussianCuts(image, **kwargs)
            if fit2d:
                popt, covd = fit2dGaussian(image, **kwargs)
            print(f"Index {i} of {len(images)} done.")
        except Exception:
            print(f"Error at index {i}.")
            raise
        if fit2d:
            # Zuordnung der Standardabweichungen zur Horizontalen und Vertikalen anhand des
            # Fitwinkels von fit2dGaussian
            pi = scnst.pi
            while popt[5] < 0:
                popt[5] += 2 * pi
            while popt[5] > 2 * pi:
                popt[5] -= 2 * pi
            if ((popt[5] > pi / 4 and popt[5] < (3 * pi) / 4)
                    or (popt[5] > (5 * pi) / 4 and popt[5] < (7 * pi) / 4)):
                temp = popt[4]
                popt[4] = popt[3]
                popt[3] = temp
            w2.append((abs(popt[3]) * 2 * pl, abs(popt[4]) * 2 * pl))
        center.append((par[0][1] * pl, par[90][1] * pl))
        w.append((abs(par[0][2]) * 2 * pl, abs(par[90][2]) * 2 * pl))
    w = np.array(w).T
    w2 = np.array(w2).T
    return w, w2, center


def find_start_parameters(positions: list[float], w: list[list[float]], index: int
                          ) -> list[float]:
    """Find the start parameters.

    :param positions: List of positions in cm.
    :param w: list of lists of beam radii (in µm), one horizontal, one vertical.
    :param index: Index to choose between horizontal and vertical list.
    :return: list of Rayleigh length and focus position guess, both in m
    """

    j = np.argmin(w[index])

    z0_guess = positions[j] / 100
    minimum = np.min(w[index])
    pos2 = positions[np.argmax(w[index])] / 100

    # original code
    # z0_guess = positions[np.argmin(w[1, :])] / 100
    # j = np.argmin(w[1, :])
    # while np.sqrt(2) * np.min(w[1, :]) > w[1, j]:
    #     j += 1
    # zR_guess = abs(z0_guess - positions[j] / 100)
    # guessv = [zR_guess, z0_guess]

    # search forward:
    while j < len(w[index]):
        if np.sqrt(2) * minimum < w[index, j]:
            pos2 = positions[j] / 100
            j = -1
            break
        j += 1
    # search backward
    while j > 0:
        j -= 1
        if np.sqrt(2) * minimum < w[index, j]:
            pos2 = positions[j] / 100
            break
    zR_guess = abs(z0_guess - pos2)
    return [zR_guess, z0_guess]


def plotBeamWidthOverPosition(positions: list[float],
                              names: Optional[str] = None,
                              w: Optional[np.ndarray] = None,
                              w2: Optional[np.ndarray] = None,
                              twoD: bool = False,
                              wavelength: float = 1064e-9,
                              fit_M2: bool = True,
                              guessh=None,
                              guessv=None,
                              ) -> tuple[
                                  list[float], list[list[float]], ]:
    """Plot the beam width over the position and return fit parameters.

    :param positions: in cm
    :param names: File names to load and analyse. Alternatively, give the radii in `w`. 
    :param w/w2: radius in µm, alternatively give the file names. 
    :param twoD: Whether to use the 2D fitted radii (in names or w2 list) as well
    :param wavelength: in m.
    :param fit_M2: whether to fit M2 value or not (assuming M2=1).
    :param guessh/guessv: initial guess of parameters (zR, z0, M2) in m, if None estimate from data.
    :return: list of horizontal parameters, list of horizontal covariances
    """
    try:
        if names:
            w, w2, center = extractBeamRadii(names, twoD)
        elif np.array_equal(w, None) or (np.array_equal(w2, None) and twoD == 1):
            raise Exception("`names` or radii `w` is necessary!.")
    except Exception as exc:
        print(exc.args)
        return
    # Anhand der Strahlradien werden Anfangsbedingungen geraten, falls keine übergeben wurden
    if guessh is None:
        guessh = find_start_parameters(positions, w, 0)
        if fit_M2:
            guessh.append(1)
    if guessv is None:
        guessv = find_start_parameters(positions, w, 1)
        if fit_M2:
            guessv.append(1)

    z = np.linspace(min(positions), max(positions), 50)

    fig, axes = plt.subplots(1, 2 if twoD else 1, figsize=(10, 5))
    if isinstance(axes, plt.Axes):
        axes = (axes,)
    parh = [[], []]  # horizontal fit parameters (1D, 2D)
    covh = [[], []]  # covariances
    parv = [[], []]  # vertical fit parameters (1D, 2D)
    covv = [[], []]  # covariances

    def w_z(z, zR, z0, M2=1, w0=None):
        return gb.w_z(z=z, zR=zR, z0=z0, lbd=wavelength, M2=M2, w0=w0)

    for i in range(len(axes)):
        ws = w2 if i else w
        for (index, p, c, g, label, color) in ((0, parh, covh, guessh, "hor", "C0"),
                                               (1, parv, covv, guessv, "ver", "C1")):
            try:
                p[i], c[i] = fit(w_z, positions / 100, ws[index] / 1e6, p0=g)
            except RuntimeError as exc:
                print(f"Fitting failed for {i + 1}D, {index}", exc)
                continue
            axes[i].plot(positions, ws[index], label=label, ls="", marker="+", color=color)
            axes[i].plot(
                z, w_z(z / 100, *p[i]) * 1e6,
                label=(f"z0={p[i][1]*100:.4}cm, "
                       f"zR={p[i][0] * 100:.4}cm, "
                       f"w0={gb.w0(p[i][0], lbd=wavelength) * 1e6:.6}µm, "
                       f"M2={p[i][-1] if fit_M2 else float('nan'):.4}"),
                color=color)
        axes[i].set_xlabel("position in cm")
        axes[i].set_ylabel("beam radius in µm")
        axes[i].set_title("2D gaussian" if i else "central gaussian")
        pl(axes=axes[i], xname="daBeamProfiler/beamRadius")
    return parh[0], covh[0], parv[0], covv[0], parh[1], covh[1], parv[1], covv[1]


# %%% Mehrere Bilder laden zur Sammelanalyse

"""Mehrere Bilder laden"""


# %%%% BeamCamera
# BeamCamera

names = "2023_12_18T18_17_39 2023_12_18T18_20_59 2023_12_18T18_25_50 2023_12_18T18_27_57 2023_12_18T18_29_26".split()  # Fill in with names
positionsL = "54 65 71 85 100".split()  # in cm

kwargs = {}
# Load
positions = np.array([float(positionsL[i]) for i in range(len(positionsL))])
images = []
pixel_lengths = []
for name in names:
    _im, _px = import_beam_camera_image(
        name=name,
        # directory="/home/benedikt/2023-12-12 Jupstage upconversion pumpe/"
    )
    images.append(_im)
    pixel_lengths.append(_px)


# %%%% BeamProfiler
# BeamProfiler

names_string = "2024_03_18T19_29_09 2024_03_18T19_32_26 2024_03_18T19_40_07  2024_03_18T19_45_41  2024_03_18T19_58_38"
positionsString = "8.5                           15                                 20                                       25                                     29"

names = names_string.split()  # Fill in with names
positionsL = positionsString.split()  # in cm

key = "idler2-cw"
kwargs = dict(
    radius=10,  # pixels
    zero_offset=True,
)

# Load
positions = np.array([float(positionsL[i]) for i in range(len(positionsL))])
images: list[np.ndarray] = []
pixel_lengths = []
for name in names:
    images.append(importBeamProfiler(name, key=key))
    pixel_lengths.append(pixelLength)


# %%% Extra Fenster 2 für mehrere Bilder
"""----------------------------------"""

# %%%% Extra Fenster resetten

# Wenn das Fenster geschlossen wurde, kann man es hier resetten:
try:
    del imageView2  # noqa
except NameError:
    pass


# %%%% Bilder in extra Fenster anzeigen

# funktioniert nur bei gleicher Pixelanzahl aller Bilder
try:
    imageView2.setImage(np.array(images))
except NameError:
    imageView2 = pg.image(images[0], title="BeamProfiling - multiple")
    imageView2.setPredefinedGradient(config['pg_cmap'])
    imageView2.setImage(np.array(images))

# %%% Strahlradien mehrerer Bilder gesammelt auswerten
"""Kurven anpassen und Schnitte und Strahlradien extrahieren
----------------------------------------------------------------"""

# %%%%
# Adjust parameters
kwargs.update(dict(
    # radius=5,
))

# %%%%
# Calculate Radii
w, w2, center = extractBeamRadii(names, fit2d=10, **kwargs)


# %%% Ein Bild plotten zum Fit kontrollieren
# Plot a single picture

plot_fitted(index=0, show_markings=True, print_width=True, center=center)


# %%% Alle Fits plotten zur Kontrolle
# Alle Fits plotten zur Kontrolle

for i in range(len(images)):
    plot_fitted(i, show_markings=True, center=center)


# %%% Plot beam width over position
# Plot beam width over position

parh_s, covh_s, parv_s, covv_s, parh_2D, covh_2D, parv_2D, covv_2D = plotBeamWidthOverPosition(
    positions, w=w, w2=w2,
    twoD=0,
    wavelength=532e-9,
    fit_M2=0,
    # guessh=(.65, .40, 1),
    # guessv=(.80, .30),
)



# %% Plot beam widths over position from widths alone (no images)
"""Plot beam widths over position from widths alone (no images)
---------------------------------------------------------------"""

positionsString2 = "110               90                   70                        50                 35"
widthString = "1,629/1,486 1,5/1,442      1,373/1,332         1,322/1,414  1,33/1,536"

positions2 = [float(val) for val in positionsString2.split()]
widths2 = [[], []]
for element in widthString.replace(",", ".").split():
    widths2[0].append(float(element.split("/")[0]))
    widths2[1].append(float(element.split("/")[1]))

plotBeamWidthOverPosition(np.array(positions2), w=np.array(widths2)*1e3, wavelength=3535e-9, fit_M2=10)


# %%

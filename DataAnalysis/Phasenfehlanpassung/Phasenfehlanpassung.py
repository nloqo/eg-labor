import numpy as np
import pylab as pl
from scipy.optimize import curve_fit
import sys

# HCl spezifische Konstanten
u = 1.660539e-27
mH = u
mCl37 = 37 * u
mCl35 = 35 * u
M35 = mCl35 * mH / (mCl35 + mH)
M37 = mCl37 * mH / (mCl37 + mH)
Re = 1.274e-10


# universelle Größen
c = 299792458
h = 6.626e-34
epsilon0 = 8.854e-12
hbar = 6.626e-34 / 2 * np.pi
T = 293.15 + 25
kB = 1.380649e-23
NA = 6.022e23
"""
# Textdateien mit den Hitran-Daten, dabei wurden die Übergänge ausgewählt, die mit einer Verstimmung
von bis zu 100 cm^-1
#von dem Übergang liegen

data0 = np.loadtxt('Transitions_H35Cl_1P.par')
data1 = np.loadtxt('Transitions_H35Cl_1064.par')
data2 = np.loadtxt('Transitions_H35Cl_3P.par')

#Definition der Größen aus den Spalten der Textdateien

A0 = data0[:,3]                     #A in 1/s
A1 = data1[:,3]                     #A in 1/s
A2 = data2[:,3]                     #A in 1/s

E0 = data0[:,6]                     #E0 in cm^-1
E1 = data1[:,6]                     #E0 in cm^-1
E2 = data2[:,6]                     #E0 in cm^-1

E0_J = data0[:,6]*100*h*c           #E0 in J

v0 = data0[:,1]                     #v in cm^-1
v1 = data1[:,1]                     #v in cm^-1
v2 = data2[:,1]                     #v in cm^-1

v0_w = data0[:,1]*100*c*2*np.pi   #v in rad/s
v1_w = data1[:,1]*100*c*2*np.pi   #v in rad/s
v2_w = data2[:,1]*100*c*2*np.pi   #v in rad/s

gamma_self0 = data0[:,5]            #gamma_self in cm^-1*atm^-1
gamma_self1 = data1[:,5]            #gamma_self in cm^-1*atm^-1
gamma_self2 = data2[:,5]            #gamma_self in cm^-1*atm^-1

n_0 = data0[:,7]
n_1 = data1[:,7]
n_2 = data2[:,7]


#Sortieren der Daten, sodass man eine Liste erhält, die alle Energieniveaus nach Energie sortiert
enthält.

E0_ = list(E0_J)
E0_.sort()
E0_ = list(dict.fromkeys(E0_))

#Berechnen der anfänglichen Verteilung rho, wobei nur Niveaus berücksichtigt werden,
#die eine relative Besetzung größer als 10^-5 aufweisen.

Esum = []
for i in range(len(E0_)):
    Esum.append(np.exp(-E0_[i]/(kB*T)))

rho = []
for i in range(len(Esum)):
    if Esum[i]/sum(Esum) > 1e-5:
        rho.append(Esum[i]/sum(Esum))

Estart = []
for i in range(len(rho)):
    Estart.append(round(E0_[i]/(100*h*c),4))

def mu(A,omega):
    return np.sqrt(3*epsilon0*h*c**3/(2*omega**3))

#Liste mit Werten für verschiedene Drücke und MIR-Wellenlängen, die bei der Iteration unten
verwendet werden

p = np.linspace(50,400,81)*1e2
p_atm = p*0.000986923/1e2
p_torr = p/1.33322e2

lamba0 = np.linspace(1/2843e2,1/2798e2,81)

#Offset der Brechungsindizes unter Normalbedingungen

n1 = 442.6e-6+1
n2 = 447.74e-6+1

#Umrechnung der Offset-Werte für den jeweiligen Druck bei gegebener Temperatur

def n(n0,p,T):
    return (n0-1)*p/(760*(1+3.66*T*1e-3))+1

#Festlegung des Strahlparameters und der Zellenlänge

zR = 50e-3
b = 2*zR
L = 11.5e-2

#Erstellen von leeren Matrizen; Hier werden später die Werte für den Plot gespeichert;
# pp ist der Druck, ll die Zellenlänge, bdk der Strahlparameter mal der Phasenfehlanpassung und J3
das Phasenanpassungsintegral

J3 = np.empty((len(p),len(lamba0)))
pp = np.empty((len(p),len(lamba0)))
ll = np.empty((len(p),len(lamba0)))
bdk = np.empty((len(p),len(lamba0)))

#Für die Offset-Werte der MIR-Strahlung werden nur diskrete Werte in dem Paper angegeben, einige
davon sind unten in den Listen n0 und l0 zu sehen. Damit der Plot farblich kontinuierlich ist, wird
die folgende Funktion f angenommen und an die Werte aus dem Paper angefittet. Die Fitparameter
werden später zusammen mit der Funktion verwendet.


def f(x,a,b,c,d):
    return a*x**3+b*x**2+c*x+d

n0 = [454.4,451.1,448.3,444.3,440.5,443.5,455.1]#,453.2,446.6,438.8,430.6
for i in range(len(n0)):
    n0[i]=n0[i]*1e-6+1
l0 = [2780,2800,2820,2840,2860,2880,2900]#,2920,2940,2960,2980
for i in range(len(l0)):
    l0[i]=1/(l0[i]*1e2)

#Plot der Daten und des Fits

pl.plot(l0,n0,'x')
test = np.linspace(3443,3610,100)*1e-9
fitParameter,fitKovarianz = curve_fit(f,l0,n0,p0=[-2.81456e16,2.99367e11,-1.0612e6,2.25414])
fity = f(test,*fitParameter)
pl.plot(test,fity,label='fit')

pl.xlabel('Wellenlänge in nm')
pl.ylabel('Brechungsindex n0')
pl.legend()
pl.show()

#Nun beginnt die Schleife beginnend mit der Iteration über den Druck
for k in range(len(p)):
    print(p[k]*1e-2)
#Teilchenzahldichte N berechnen
    N = p[k]/(kB*T)
#Definition der Linienbreiten nach der Bachelorarbeit von Stewen
    def alpha_D(omega):
        return omega/c*np.sqrt(2*NA*kB*T*np.log(2)/M35)

    def alpha_p(gamma_self,n):
        return (296/T)**n*gamma_self*p_atm[k]*100*c/(2*np.pi)

    def alpha_v(omega,gamma_self,n):
        return 0.5346*alpha_p(gamma_self,n)+np.sqrt(0.2166*alpha_p(gamma_self,n)**2+alpha_D(omega)**2)  # noqa: E501
#Erstellen von leeren Listen, in die die Werte für die verschiedenen Anfangszustände gespeichert
werden, bevor sie aufsummiert werden
    start0 = []
    start1 = []
    start2 = []
#Brechungsindex-Offset für den jewiligen Druck wird berechnet
    n11 = n(n1,p_torr[k],22)
    n22 = n(n2,p_torr[k],22)
#mit der folgenden Schleife werden die verschiedenen Niveaus mit ihrer jeweiligen Verstimmung für
die
#Berechnung des Xi berücksichtigt
    for l in range(len(lamba0)):

        lamba2 = 1/(1/1064e-9+2/lamba0[l])
#Berechnung des MIR-Brechungsindexoffset-Wertes für die jeweilige Wellenlänge und den jeweiligen
Druck
        n0 = f(lamba0[l],fitParameter[0],fitParameter[1],fitParameter[2],fitParameter[3])

        n00 = n(n0,p_torr[k],22)

        for i in range(len(Estart)):
            temp = []
            for j in range(len(E0)):
                if E0[j]==Estart[i]:
                    omega = 2*np.pi*c/lamba0[l]
                    mu0 = mu(A0[j],v0_w[j])
                    gamma = alpha_v(1/(lamba0[l]*1e2), gamma_self0[j], n_0[j])
                    delta1 = v0_w[j]-omega-1j*gamma
                    delta2 = v0_w[j]+omega+1j*gamma
                    temp.append(mu0**2*(1/delta1+1/delta2))
            start0.append(rho[i]*sum(temp))

            temp = []
            for j in range(len(E1)):
                if E1[j]==Estart[i]:
                    omega = 2*np.pi*c/1064e-9
                    mu1 = mu(A1[j],v1_w[j])
                    gamma = alpha_v(1/1064e-7, gamma_self1[j], n_1[j])
                    delta1 = v1_w[j]-omega-1j*gamma
                    delta2 = v1_w[j]+omega+1j*gamma
                    temp.append(mu1**2*(1/delta1+1/delta2))
            start1.append(rho[i]*sum(temp))

            temp = []
            for j in range(len(E2)):
                if E2[j]==Estart[i]:
                    omega = 2*np.pi*c/lamba2
                    mu2 = mu(A2[j],v2_w[j])
                    gamma = alpha_v(1/(lamba2*1e2), gamma_self2[j], n_2[j])
                    delta1 = v2_w[j]-omega-1j*gamma
                    delta2 = v2_w[j]+omega+1j*gamma
                    temp.append(mu0**2*(1/delta1+1/delta2))
            start2.append(rho[i]*sum(temp))
#Berechnung der verschiedenen Xi-Werte
        Xi1 = N/(3*epsilon0*hbar)*sum(start1)
        Xi0 = N/(3*epsilon0*hbar)*sum(start0)
        Xi2 = N/(3*epsilon0*hbar)*sum(start2)
#Berechnung der Brechungsindizes
        n111 = np.sqrt(1+Xi1)+n11
        n000 = np.sqrt(1+Xi0)+n00
        n222 = np.sqrt(1+Xi2)+n22
#Komplexe Phasenfehlanpassung
        dkc = 2*np.pi/lamba2*n22-4*np.pi/lamba0[l]*n00-2*np.pi/1064e-9*n11
#Absortionskoeffizienten
        alpha0 = 2*n000.imag/c
        alpha1 = 2*n111.imag/c
        alpha2 = 2*n222.imag/c
#reelle Phasenfehlanpassungen
        k0 = 2*np.pi*n000.real/lamba0[l]
        k1 = 2*np.pi*n111.real/1064e-9
        k2 = 2*np.pi*n222.real/lamba2
        dk = k2-2*k0-k1

#Definition des J3 nach Lago Paper

        a = b*dkc/2

        def Integrand(z,z0,L):
            return np.exp(-1j*a*z)/(1+1j*(z-L/b*2*z0/L))**2
#Schrittweite der Integralberechnung
        dn = 1e-6
#Integrationsbereich
        x = np.arange(-L/b,L/b,dn)

#folgende Bedingung wurde nur sicherhaltshalber eingeführt, um bei zu kleiner Wahl der Schrittweite
beim Integral eine Überlastung
#des Arbeitsspeichers zu verhindern

        if len(x)>1e8:
            print('!!!MEMORY ERROR!!!')
            sys.exit()

#Abspeichern der Werte in den vorbereiteten Matrizen

        J3[k][l] = abs(sum(Integrand(x,0,L)*dn))**2
        ll[k][l] = lamba0[l]
        pp[k][l] = p[k]*1e-2
        bdk[k][l] = b*dk

#2D plot der Daten

pl.title(f'J3, Länge: {L}')
pl.pcolor(pp, ll*1e9, J3,shading='auto')
pl.colorbar()
pl.xlabel('Druck in mbar')
pl.ylabel('Wellenlänge in nm')
pl.show()

pl.title(f'b*dk, Länge: {L}')
pl.pcolor(pp, ll*1e9, bdk,shading='auto')
pl.colorbar()
pl.xlabel('Druck in mbar')
pl.ylabel('Wellenlänge in nm')
pl.show()




#Fester Druck (100 mbar) und variables z0


p = 100*1e2
p_atm = p*0.000986923/1e2
p_torr = p/1.33322e2

lamba0 = np.linspace(1/2843e2,1/2798e2,81)


n1 = 442.6e-6+1
n2 = 447.74e-6+1

def n(n0,p,T):
    return (n0-1)*p/(760*(1+3.66*T*1e-3))+1

zR = 30e-3
b = 2*zR
L = 6e-2
z0 = np.linspace(-L/2,L/2,81)

J3 = np.empty((len(z0),len(lamba0)))
z00 = np.empty((len(z0),len(lamba0)))
ll = np.empty((len(z0),len(lamba0)))
bdk = np.empty((len(z0),len(lamba0)))

def f(x,a,b,c,d):
    return a*x**3+b*x**2+c*x+d

n0 = [454.4,451.1,448.3,444.3,440.5,443.5,455.1]#,453.2,446.6,438.8,430.6
for i in range(len(n0)):
    n0[i]=n0[i]*1e-6+1
l0 = [2780,2800,2820,2840,2860,2880,2900]#,2920,2940,2960,2980
for i in range(len(l0)):
    l0[i]=1/(l0[i]*1e2)
pl.plot(l0,n0,'x')
test = np.linspace(3443,3610,100)*1e-9
fitParameter,fitKovarianz = curve_fit(f,l0,n0,p0=[-2.81456e16,2.99367e11,-1.0612e6,2.25414])
fity = f(test,*fitParameter)
pl.plot(test,fity,label='fit')

pl.xlabel('Wellenlänge in nm')
pl.ylabel('Brechungsindex n0')
pl.legend()
pl.show()

N = p/(kB*T)

def alpha_D(omega):
    return omega/c*np.sqrt(2*NA*kB*T*np.log(2)/M35)

def alpha_p(gamma_self,n):
    return (296/T)**n*gamma_self*p_atm*100*c/(2*np.pi)

def alpha_v(omega,gamma_self,n):
    return 0.5346*alpha_p(gamma_self,n)+np.sqrt(0.2166*alpha_p(gamma_self,n)**2+alpha_D(omega)**2)

n11 = n(n1,p_torr,22)
n22 = n(n2,p_torr,22)

for k in range(len(z0)):
    print(z0[k]*1e2)

    start0 = []
    start1 = []
    start2 = []

    for l in range(len(lamba0)):

        lamba2 = 1/(1/1064e-9+2/lamba0[l])

        n0 = f(lamba0[l],fitParameter[0],fitParameter[1],fitParameter[2],fitParameter[3])

        n00 = n(n0,p_torr,22)

        for i in range(len(Estart)):
            temp = []
            for j in range(len(E0)):
                if E0[j]==Estart[i]:
                    omega = 2*np.pi*c/lamba0[l]
                    mu0 = mu(A0[j],v0_w[j])
                    gamma = alpha_v(1/(lamba0[l]*1e2), gamma_self0[j], n_0[j])
                    delta1 = v0_w[j]-omega-1j*gamma
                    delta2 = v0_w[j]+omega+1j*gamma
                    temp.append(mu0**2*(1/delta1+1/delta2))
            start0.append(rho[i]*sum(temp))

            temp = []
            for j in range(len(E1)):
                if E1[j]==Estart[i]:
                    omega = 2*np.pi*c/1064e-9
                    mu1 = mu(A1[j],v1_w[j])
                    gamma = alpha_v(1/1064e-7, gamma_self1[j], n_1[j])
                    delta1 = v1_w[j]-omega-1j*gamma
                    delta2 = v1_w[j]+omega+1j*gamma
                    temp.append(mu1**2*(1/delta1+1/delta2))
            start1.append(rho[i]*sum(temp))

            temp = []
            for j in range(len(E2)):
                if E2[j]==Estart[i]:
                    omega = 2*np.pi*c/lamba2
                    mu2 = mu(A2[j],v2_w[j])
                    gamma = alpha_v(1/(lamba2*1e2), gamma_self2[j], n_2[j])
                    delta1 = v2_w[j]-omega-1j*gamma
                    delta2 = v2_w[j]+omega+1j*gamma
                    temp.append(mu0**2*(1/delta1+1/delta2))
            start2.append(rho[i]*sum(temp))

        Xi1 = N/(3*epsilon0*hbar)*sum(start1)
        Xi0 = N/(3*epsilon0*hbar)*sum(start0)
        Xi2 = N/(3*epsilon0*hbar)*sum(start2)

        n111 = np.sqrt(1+Xi1)+n11
        n000 = np.sqrt(1+Xi0)+n00
        n222 = np.sqrt(1+Xi2)+n22



        dkc = 2*np.pi/lamba2*n22-4*np.pi/lamba0[l]*n00-2*np.pi/1064e-9*n11

        alpha0 = 2*n000.imag/c
        alpha1 = 2*n111.imag/c
        alpha2 = 2*n222.imag/c

        k0 = 2*np.pi*n000.real/lamba0[l]
        k1 = 2*np.pi*n111.real/1064e-9
        k2 = 2*np.pi*n222.real/lamba2
        dk = k2-2*k0-k1

        #Definition nach Lago Paper

        a = b*dkc/2

        def Integrand(z,z0,L):
            return np.exp(-1j*a*z)/(1+1j*(z-L/b*2*z0/L))**2
        dn = 1e-6
        x = np.arange(-L/b,L/b,dn)

        if len(x)>1e8:
            print('!!!MEMORY ERROR!!!')
            sys.exit()

        J3[k][l] = abs(sum(Integrand(x,z0[k],L)*dn))**2
        ll[k][l] = lamba0[l]
        z00[k][l] = z0[k]*1e3
        bdk[k][l] = b*dk

pl.title(f'J3, Länge: {L}')
pl.pcolor(z00, ll*1e9, J3)
pl.colorbar()
pl.xlabel('z0 in mm')
pl.ylabel('Wellenlänge in nm')
pl.show()

pl.title(f'b*dk, Länge: {L}')
pl.pcolor(z00, ll*1e9, bdk)
pl.colorbar()
pl.xlabel('Druck in mbar')
pl.ylabel('Wellenlänge in nm')
pl.show()
"""


# Feste Wellenlänge auf 2P-Resonanz bei 2830,36 cm^-1 (hier auch mit beiden Isotopen berücksichtigt)


def mu(A, omega):
    return np.sqrt(3 * epsilon0 * h * c**3 / (2 * omega**3))


# Liste mit Werten für verschiedene Drücke und MIR-Wellenlängen, die bei der Iteration unten
# verwendet werden

p = np.linspace(0, 1000, 101) * 1e2
p_atm = p * 0.000986923 / 1e2
p_torr = p / 1.33322e2

# Offset der Brechungsindizes unter Normalbedingungen

n1 = 442.6e-6 + 1
n2 = 447.8e-6 + 1

# Umrechnung der Offset-Werte für den jeweiligen Druck bei gegebener Temperatur


def n(n0, p, T):
    return (n0 - 1) * p / (760 * (1 + 3.66 * T * 1e-3)) + 1


# Festlegung des Strahlparameters und der Zellenlänge

zR = 30e-3
b = 2 * zR
L = 18e-2

# Erstellen von leeren Matrizen; Hier werden später die Werte für den Plot gespeichert;
# pp ist der Druck, ll die Zellenlänge, bdk der Strahlparameter mal der Phasenfehlanpassung
# und J3 das Phasenanpassungsintegral

J3 = np.empty((len(p), 1))
pp = np.empty((len(p), 1))
bdk = np.empty((len(p), 1))

# Für die Offset-Werte der MIR-Strahlung werden nur diskrete Werte in dem Paper angegeben,
# einige davon sind unten in den Listen n0 und l0 zu sehen. Damit der Plot farblich kontinuierlich
# ist, wird die folgende Funktion f angenommen und an die Werte aus dem Paper angefittet.
# Die Fitparameter werden später zusammen mit der Funktion verwendet.


def f(x, a, b, c, d):
    return a * x**3 + b * x**2 + c * x + d


n0 = [454.4, 451.1, 448.3, 444.3, 440.5, 443.5, 455.1]

for i in range(len(n0)):
    n0[i] = n0[i] * 1e-6 + 1

l0 = [2780, 2800, 2820, 2840, 2860, 2880, 2900]

for i in range(len(l0)):
    l0[i] = 1 / (l0[i] * 1e2)

# Plot der Daten und des Fits

pl.plot(l0, n0, "x")
test = np.linspace(3443, 3610, 100) * 1e-9
fitParameter, fitKovarianz = curve_fit(f, l0, n0, p0=[-2.81456e16, 2.99367e11, -1.0612e6, 2.25414])
fity = f(test, *fitParameter)
pl.plot(test, fity, label="fit")

pl.xlabel("Wellenlänge in nm")
pl.ylabel("Brechungsindex n0")
pl.legend()
pl.show()

# Nun beginnt die Schleife beginnend mit der Iteration über den Druck
for k in range(len(p)):
    print(p[k] * 1e-2)

    # Teilchenzahldichte N berechnen
    N = 0.76 * p[k] / (kB * T)

    # Textdateien mit den Hitran-Daten, dabei wurden die Übergänge ausgewählt,
    # die mit einer Verstimmung von bis zu 100 cm^-1 von dem Übergang liegen

    data0 = np.loadtxt("Transitions_H35Cl_1P.par")
    data1 = np.loadtxt("Transitions_H35Cl_1064.par")
    data2 = np.loadtxt("Transitions_H35Cl_3P.par")

    # Definition der Größen aus den Spalten der Textdateien

    A0 = data0[:, 3]  # A in 1/s
    A1 = data1[:, 3]  # A in 1/s
    A2 = data2[:, 3]  # A in 1/s

    E0 = data0[:, 6]  # E0 in cm^-1
    E1 = data1[:, 6]  # E0 in cm^-1
    E2 = data2[:, 6]  # E0 in cm^-1

    E0_J = data0[:, 6] * 100 * h * c  # E0 in J

    v0 = data0[:, 1]  # v in cm^-1
    v1 = data1[:, 1]  # v in cm^-1
    v2 = data2[:, 1]  # v in cm^-1

    v0_w = data0[:, 1] * 100 * c * 2 * np.pi  # v in rad/s
    v1_w = data1[:, 1] * 100 * c * 2 * np.pi  # v in rad/s
    v2_w = data2[:, 1] * 100 * c * 2 * np.pi  # v in rad/s

    gamma_self0 = data0[:, 5]  # gamma_self in cm^-1*atm^-1
    gamma_self1 = data1[:, 5]  # gamma_self in cm^-1*atm^-1
    gamma_self2 = data2[:, 5]  # gamma_self in cm^-1*atm^-1

    n_0 = data0[:, 7]
    n_1 = data1[:, 7]
    n_2 = data2[:, 7]

    # Sortieren der Daten, sodass man eine Liste erhält, die alle Energieniveaus nach Energie
    # sortiert enthält.

    E0_ = list(E0_J)
    E0_.sort()
    E0_ = list(dict.fromkeys(E0_))

    # Berechnen der anfänglichen Verteilung rho, wobei nur Niveaus berücksichtigt werden,
    # die eine relative Besetzung größer als 10^-5 aufweisen.

    Esum = []
    for i in range(len(E0_)):
        Esum.append(np.exp(-E0_[i] / (kB * T)))

    rho = []
    for i in range(len(Esum)):
        if Esum[i] / sum(Esum) > 1e-5:
            rho.append(Esum[i] / sum(Esum))

    Estart = []
    for i in range(len(rho)):
        Estart.append(round(E0_[i] / (100 * h * c), 4))

    # Definition der Linienbreiten nach der Bachelorarbeit von Stewen
    def alpha_D(omega):
        return omega / c * np.sqrt(2 * NA * kB * T * np.log(2) / M35)

    def alpha_p(gamma_self, n):
        return (296 / T) ** n * gamma_self * p_atm[k] * 100 * c / (2 * np.pi)

    def alpha_v(omega, gamma_self, n):
        return 0.5346 * alpha_p(gamma_self, n) + np.sqrt(
            0.2166 * alpha_p(gamma_self, n) ** 2 + alpha_D(omega) ** 2
        )

    # Erstellen von leeren Listen, in die die Werte für die verschiedenen Anfangszustände
    # gespeichert werden, bevor sie aufsummiert werden

    start0 = []
    start1 = []
    start2 = []

    # Brechungsindex-Offset für den jewiligen Druck wird berechnet

    n11 = n(n1, p_torr[k], 22)
    n22 = n(n2, p_torr[k], 22)
    lamba0 = 1 / 2830.36e2
    lamba2 = 1 / (1 / 1064e-9 + 2 / lamba0)

    # Berechnung des MIR-Brechungsindexoffset-Wertes für die jeweilige Wellenlänge und den
    # jeweiligen Druck

    n0 = f(lamba0, *fitParameter)

    n00 = n(n0, p_torr[k], 22)

    for i in range(len(Estart)):
        temp = []
        for j in range(len(E0)):
            if E0[j] == Estart[i]:
                omega = 2 * np.pi * c / lamba0
                mu0 = mu(A0[j], v0_w[j])
                gamma = alpha_v(1 / (lamba0 * 1e2), gamma_self0[j], n_0[j])
                delta1 = v0_w[j] - omega - 1j * gamma
                delta2 = v0_w[j] + omega + 1j * gamma
                temp.append(mu0**2 * (1 / delta1 + 1 / delta2))
        start0.append(rho[i] * sum(temp))

        temp = []
        for j in range(len(E1)):
            if E1[j] == Estart[i]:
                omega = 2 * np.pi * c / 1064e-9
                mu1 = mu(A1[j], v1_w[j])
                gamma = alpha_v(1 / 1064e-7, gamma_self1[j], n_1[j])
                delta1 = v1_w[j] - omega - 1j * gamma
                delta2 = v1_w[j] + omega + 1j * gamma
                temp.append(mu1**2 * (1 / delta1 + 1 / delta2))
        start1.append(rho[i] * sum(temp))

        temp = []
        for j in range(len(E2)):
            if E2[j] == Estart[i]:
                omega = 2 * np.pi * c / lamba2
                mu2 = mu(A2[j], v2_w[j])
                gamma = alpha_v(1 / (lamba2 * 1e2), gamma_self2[j], n_2[j])
                delta1 = v2_w[j] - omega - 1j * gamma
                delta2 = v2_w[j] + omega + 1j * gamma
                temp.append(mu0**2 * (1 / delta1 + 1 / delta2))
        start2.append(rho[i] * sum(temp))

    # Berechnung der verschiedenen Xi-Werte

    Xi1_35 = N / (3 * epsilon0 * hbar) * sum(start1)
    Xi0_35 = N / (3 * epsilon0 * hbar) * sum(start0)
    Xi2_35 = N / (3 * epsilon0 * hbar) * sum(start2)

    # Gleiche Berechnung für das andere Isotop

    data0 = np.loadtxt("Transitions_H37Cl_1P.par")
    data1 = np.loadtxt("Transitions_H37Cl_1064.par")
    data2 = np.loadtxt("Transitions_H37Cl_3P.par")

    # Definition der Größen aus den Spalten der Textdateien

    A0 = data0[:, 3]  # A in 1/s
    A1 = data1[:, 3]  # A in 1/s
    A2 = data2[:, 3]  # A in 1/s

    E0 = data0[:, 6]  # E0 in cm^-1
    E1 = data1[:, 6]  # E0 in cm^-1
    E2 = data2[:, 6]  # E0 in cm^-1

    E0_J = data0[:, 6] * 100 * h * c  # E0 in J

    v0 = data0[:, 1]  # v in cm^-1
    v1 = data1[:, 1]  # v in cm^-1
    v2 = data2[:, 1]  # v in cm^-1

    v0_w = data0[:, 1] * 100 * c * 2 * np.pi  # v in rad/s
    v1_w = data1[:, 1] * 100 * c * 2 * np.pi  # v in rad/s
    v2_w = data2[:, 1] * 100 * c * 2 * np.pi  # v in rad/s

    gamma_self3 = data0[:, 5]  # gamma_self in cm^-1*atm^-1
    gamma_self4 = data1[:, 5]  # gamma_self in cm^-1*atm^-1
    gamma_self5 = data2[:, 5]  # gamma_self in cm^-1*atm^-1

    n_0 = data0[:, 7]
    n_1 = data1[:, 7]
    n_2 = data2[:, 7]

    # Sortieren der Daten, sodass man eine Liste erhält, die alle Energieniveaus nach Energie
    # sortiert enthält.

    E0_ = list(E0_J)
    E0_.sort()
    E0_ = list(dict.fromkeys(E0_))

    # Berechnen der anfänglichen Verteilung rho, wobei nur Niveaus berücksichtigt werden,
    # die eine relative Besetzung größer als 10^-5 aufweisen.

    Esum = []
    for i in range(len(E0_)):
        Esum.append(np.exp(-E0_[i] / (kB * T)))

    rho = []
    for i in range(len(Esum)):
        if Esum[i] / sum(Esum) > 1e-5:
            rho.append(Esum[i] / sum(Esum))

    Estart = []
    for i in range(len(rho)):
        Estart.append(round(E0_[i] / (100 * h * c), 4))

    # Erstellen von leeren Listen, in die die Werte für die verschiedenen Anfangszustände
    # gespeichert werden, bevor sie aufsummiert werden

    start0 = []
    start1 = []
    start2 = []

    # Brechungsindex-Offset für den jewiligen Druck wird berechnet

    n11 = n(n1, p_torr[k], 22)
    n22 = n(n2, p_torr[k], 22)
    lamba0 = 1 / 2830.36e2
    lamba2 = 1 / (1 / 1064e-9 + 2 / lamba0)
    # Berechnung des MIR-Brechungsindexoffset-Wertes für die jeweilige Wellenlänge und den
    # jeweiligen Druck

    n0 = f(lamba0, *fitParameter)

    n00 = n(n0, p_torr[k], 22)

    N = 0.24 * p[k] / (kB * T)

    for i in range(len(Estart)):
        temp = []
        for j in range(len(E0)):
            if E0[j] == Estart[i]:
                omega = 2 * np.pi * c / lamba0
                mu0 = mu(A0[j], v0_w[j])
                gamma = alpha_v(1 / (lamba0 * 1e2), gamma_self0[j], n_0[j])
                delta1 = v0_w[j] - omega - 1j * gamma
                delta2 = v0_w[j] + omega + 1j * gamma
                temp.append(mu0**2 * (1 / delta1 + 1 / delta2))
        start0.append(rho[i] * sum(temp))

        temp = []
        for j in range(len(E1)):
            if E1[j] == Estart[i]:
                omega = 2 * np.pi * c / 1064e-9
                mu1 = mu(A1[j], v1_w[j])
                gamma = alpha_v(1 / 1064e-7, gamma_self1[j], n_1[j])
                delta1 = v1_w[j] - omega - 1j * gamma
                delta2 = v1_w[j] + omega + 1j * gamma
                temp.append(mu1**2 * (1 / delta1 + 1 / delta2))
        start1.append(rho[i] * sum(temp))

        temp = []
        for j in range(len(E2)):
            if E2[j] == Estart[i]:
                omega = 2 * np.pi * c / lamba2
                mu2 = mu(A2[j], v2_w[j])
                gamma = alpha_v(1 / (lamba2 * 1e2), gamma_self2[j], n_2[j])
                delta1 = v2_w[j] - omega - 1j * gamma
                delta2 = v2_w[j] + omega + 1j * gamma
                temp.append(mu0**2 * (1 / delta1 + 1 / delta2))
        start2.append(rho[i] * sum(temp))

    # Berechnung der verschiedenen Xi-Werte

    Xi1_37 = N / (3 * epsilon0 * hbar) * sum(start1)
    Xi0_37 = N / (3 * epsilon0 * hbar) * sum(start0)
    Xi2_37 = N / (3 * epsilon0 * hbar) * sum(start2)

    Xi1 = Xi1_37 + Xi1_35
    Xi0 = Xi0_37 + Xi0_35
    Xi2 = Xi2_37 + Xi2_35

    # Berechnung der Brechungsindizes

    n111 = np.sqrt(1 + Xi1) + n11
    n000 = np.sqrt(1 + Xi0) + n00
    n222 = np.sqrt(1 + Xi2) + n22

    # Komplexe Phasenfehlanpassung

    dkc = 2 * np.pi / lamba2 * n22 - 4 * np.pi / lamba0 * n00 - 2 * np.pi / 1064e-9 * n11

    # Absortionskoeffizienten

    alpha0 = 2 * n000.imag / c
    alpha1 = 2 * n111.imag / c
    alpha2 = 2 * n222.imag / c

    # reelle Phasenfehlanpassungen

    k0 = 2 * np.pi * n000.real / lamba0
    k1 = 2 * np.pi * n111.real / 1064e-9
    k2 = 2 * np.pi * n222.real / lamba2
    dk = k2 - 2 * k0 - k1

    # Definition des J3 nach Lago Paper

    a = b * dkc / 2

    def Integrand(z, z0, L):
        return np.exp(-1j * a * z) / (1 + 1j * (z - L / b * 2 * z0 / L)) ** 2

    # Schrittweite der Integralberechnung

    dn = 1e-6

    # Integrationsbereich

    x = np.arange(-L / b, L / b, dn)

    # folgende Bedingung wurde nur sicherhaltshalber eingeführt,
    # um bei zu kleiner Wahl der Schrittweite beim Integral eine Überlastung
    # des Arbeitsspeichers zu verhindern

    if len(x) > 1e8:
        print("!!!MEMORY ERROR!!!")
        sys.exit()

    # Abspeichern der Werte in den vorbereiteten Matrizen

    J3[k] = abs(sum(Integrand(x, 5e-2, L) * dn)) ** 2
    pp[k] = p[k] * 1e-2
    bdk[k] = b * dk

# 2D plot der Daten

pl.title(f"J3, Länge: {L}")
pl.plot(pp, J3)
pl.xlabel("Druck in mbar")
pl.ylabel("J3")
pl.show()

pl.title(f"b*dk, Länge: {L}")
pl.plot(pp, bdk / b)
pl.xlabel("Druck in mbar")
pl.ylabel("bdk")
pl.show()

"""

# Feste Wellenlänge auf 2P-Resonanz bei 2830,36 cm^-1 (hier auch mit beiden Isotopen berücksichtigt)
über z0 bei festem Druck von 100 mbar

def mu(A,omega):
    return np.sqrt(3*epsilon0*h*c**3/(2*omega**3))

#Liste mit Werten für verschiedene Drücke und MIR-Wellenlängen, die bei der Iteration unten
verwendet werden

p = 100*1e2
p_atm = p*0.000986923/1e2
p_torr = p/1.33322e2


#Offset der Brechungsindizes unter Normalbedingungen

n1 = 442.6e-6+1
n2 = 447.74e-6+1

#Umrechnung der Offset-Werte für den jeweiligen Druck bei gegebener Temperatur

def n(n0,p,T):
    return (n0-1)*p/(760*(1+3.66*T*1e-3))+1

#Festlegung des Strahlparameters und der Zellenlänge

zR = 30e-3
b = 2*zR
L = 13e-2

z0 = np.linspace(-L/2,L/2,100)
#Erstellen von leeren Matrizen; Hier werden später die Werte für den Plot gespeichert;
# pp ist der Druck, ll die Zellenlänge, bdk der Strahlparameter mal der Phasenfehlanpassung und J3
das Phasenanpassungsintegral

J3 = np.empty((len(z0),1))
z0z = np.empty((len(z0),1))
bdk = np.empty((len(z0),1))

#Für die Offset-Werte der MIR-Strahlung werden nur diskrete Werte in dem Paper angegeben, einige
davon sind unten in den Listen#n0 und l0 zu sehen. Damit der Plot farblich kontinuierlich ist, wird
die folgende Funktion f angenommen und an die Werte aus dem Paper angefittet. Die Fitparameter
werden später zusammen mit der Funktion verwendet.


def f(x,a,b,c,d):
    return a*x**3+b*x**2+c*x+d

n0 = [454.4,451.1,448.3,444.3,440.5,443.5,455.1]#,453.2,446.6,438.8,430.6
for i in range(len(n0)):
    n0[i]=n0[i]*1e-6+1
l0 = [2780,2800,2820,2840,2860,2880,2900]#,2920,2940,2960,2980
for i in range(len(l0)):
    l0[i]=1/(l0[i]*1e2)

#Plot der Daten und des Fits

pl.plot(l0,n0,'x')
test = np.linspace(3443,3610,100)*1e-9
fitParameter,fitKovarianz = curve_fit(f,l0,n0,p0=[-2.81456e16,2.99367e11,-1.0612e6,2.25414])
fity = f(test,*fitParameter)
pl.plot(test,fity,label='fit')

pl.xlabel('Wellenlänge in nm')
pl.ylabel('Brechungsindex n0')
pl.legend()
pl.show()



#Teilchenzahldichte N berechnen
N = 0.76*p/(kB*T)

#Textdateien mit den Hitran-Daten, dabei wurden die Übergänge ausgewählt, die mit einer Verstimmung
von bis zu 100 cm^-1
#von dem Übergang liegen

data0 = np.loadtxt('Transitions_H35Cl_1P.par')
data1 = np.loadtxt('Transitions_H35Cl_1064.par')
data2 = np.loadtxt('Transitions_H35Cl_3P.par')

A0 = data0[:,3]                     #A in 1/s
A1 = data1[:,3]                     #A in 1/s
A2 = data2[:,3]                     #A in 1/s

E0 = data0[:,6]                     #E0 in cm^-1
E1 = data1[:,6]                     #E0 in cm^-1
E2 = data2[:,6]                     #E0 in cm^-1

E0_J = data0[:,6]*100*h*c           #E0 in J

v0 = data0[:,1]                     #v in cm^-1
v1 = data1[:,1]                     #v in cm^-1
v2 = data2[:,1]                     #v in cm^-1

v0_w = data0[:,1]*100*c*2*np.pi   #v in rad/s
v1_w = data1[:,1]*100*c*2*np.pi   #v in rad/s
v2_w = data2[:,1]*100*c*2*np.pi   #v in rad/s

gamma_self0 = data0[:,5]            #gamma_self in cm^-1*atm^-1
gamma_self1 = data1[:,5]            #gamma_self in cm^-1*atm^-1
gamma_self2 = data2[:,5]            #gamma_self in cm^-1*atm^-1

n_0 = data0[:,7]
n_1 = data1[:,7]
n_2 = data2[:,7]


E0_ = list(E0_J)
E0_.sort()
E0_ = list(dict.fromkeys(E0_))

#Berechnen der anfänglichen Verteilung rho, wobei nur Niveaus berücksichtigt werden,
#die eine relative Besetzung größer als 10^-5 aufweisen.

Esum = []
for i in range(len(E0_)):
    Esum.append(np.exp(-E0_[i]/(kB*T)))

rho = []
for i in range(len(Esum)):
    if Esum[i]/sum(Esum) > 1e-5:
        rho.append(Esum[i]/sum(Esum))
Estart = []
for i in range(len(rho)):
    Estart.append(round(E0_[i]/(100*h*c),4))

#Definition der Linienbreiten nach der Bachelorarbeit von Stewen
def alpha_D(omega):
    return omega/c*np.sqrt(2*NA*kB*T*np.log(2)/M35)

def alpha_p(gamma_self,n):
    return (296/T)**n*gamma_self*p_atm*100*c/(2*np.pi)

def alpha_v(omega,gamma_self,n):
    return 0.5346*alpha_p(gamma_self,n)+np.sqrt(0.2166*alpha_p(gamma_self,n)**2+alpha_D(omega)**2)
#Erstellen von leeren Listen, in die die Werte für die verschiedenen Anfangszustände gespeichert
werden, bevor sie aufsummiert werden
start0 = []
start1 = []
start2 = []
#Brechungsindex-Offset für den jewiligen Druck wird berechnet
n11 = n(n1,p_torr,22)
n22 = n(n2,p_torr,22)
lamba0 = 1/2830.36e2
lamba2 = 1/(1/1064e-9+2/lamba0)
#Berechnung des MIR-Brechungsindexoffset-Wertes für die jeweilige Wellenlänge und den jeweiligen
Druck
n0 = f(lamba0,*fitParameter)

n00 = n(n0,p_torr,22)

for i in range(len(Estart)):
    temp = []
    for j in range(len(E0)):
        if E0[j]==Estart[i]:
            omega = 2*np.pi*c/lamba0
            mu0 = mu(A0[j],v0_w[j])
            gamma = alpha_v(1/(lamba0*1e2), gamma_self0[j], n_0[j])
            delta1 = v0_w[j]-omega-1j*gamma
            delta2 = v0_w[j]+omega+1j*gamma
            temp.append(mu0**2*(1/delta1+1/delta2))
    start0.append(rho[i]*sum(temp))

    temp = []
    for j in range(len(E1)):
        if E1[j]==Estart[i]:
            omega = 2*np.pi*c/1064e-9
            mu1 = mu(A1[j],v1_w[j])
            gamma = alpha_v(1/1064e-7, gamma_self1[j], n_1[j])
            delta1 = v1_w[j]-omega-1j*gamma
            delta2 = v1_w[j]+omega+1j*gamma
            temp.append(mu1**2*(1/delta1+1/delta2))
    start1.append(rho[i]*sum(temp))

    temp = []
    for j in range(len(E2)):
        if E2[j]==Estart[i]:
            omega = 2*np.pi*c/lamba2
            mu2 = mu(A2[j],v2_w[j])
            gamma = alpha_v(1/(lamba2*1e2), gamma_self2[j], n_2[j])
            delta1 = v2_w[j]-omega-1j*gamma
            delta2 = v2_w[j]+omega+1j*gamma
            temp.append(mu0**2*(1/delta1+1/delta2))
    start2.append(rho[i]*sum(temp))
#Berechnung der verschiedenen Xi-Werte
Xi1_35 = N/(3*epsilon0*hbar)*sum(start1)
Xi0_35 = N/(3*epsilon0*hbar)*sum(start0)
Xi2_35 = N/(3*epsilon0*hbar)*sum(start2)
#Gleiche Berechnung für das andere Isotop

data0 = np.loadtxt('Transitions_H37Cl_1P.par')
data1 = np.loadtxt('Transitions_H37Cl_1064.par')
data2 = np.loadtxt('Transitions_H37Cl_3P.par')

#Definition der Größen aus den Spalten der Textdateien

A0 = data0[:,3]                     #A in 1/s
A1 = data1[:,3]                     #A in 1/s
A2 = data2[:,3]                     #A in 1/s

E0 = data0[:,6]                     #E0 in cm^-1
E1 = data1[:,6]                     #E0 in cm^-1
E2 = data2[:,6]                     #E0 in cm^-1

E0_J = data0[:,6]*100*h*c           #E0 in J

v0 = data0[:,1]                     #v in cm^-1
v1 = data1[:,1]                     #v in cm^-1
v2 = data2[:,1]                     #v in cm^-1

v0_w = data0[:,1]*100*c*2*np.pi   #v in rad/s
v1_w = data1[:,1]*100*c*2*np.pi   #v in rad/s
v2_w = data2[:,1]*100*c*2*np.pi   #v in rad/s

gamma_self3 = data0[:,5]            #gamma_self in cm^-1*atm^-1
gamma_self4 = data1[:,5]            #gamma_self in cm^-1*atm^-1
gamma_self5 = data2[:,5]            #gamma_self in cm^-1*atm^-1

n_0 = data0[:,7]
n_1 = data1[:,7]
n_2 = data2[:,7]


#Sortieren der Daten, sodass man eine Liste erhält, die alle Energieniveaus nach Energie sortiert
enthält.

E0_ = list(E0_J)
E0_.sort()
E0_ = list(dict.fromkeys(E0_))

#Berechnen der anfänglichen Verteilung rho, wobei nur Niveaus berücksichtigt werden,
#die eine relative Besetzung größer als 10^-5 aufweisen.

Esum = []
for i in range(len(E0_)):
    Esum.append(np.exp(-E0_[i]/(kB*T)))

rho = []
for i in range(len(Esum)):
  if Esum[i]/sum(Esum) > 1e-5:
    rho.append(Esum[i]/sum(Esum))

Estart = []

for i in range(len(rho)):
    Estart.append(round(E0_[i]/(100*h*c),4))

    #Definition der Linienbreiten nach der Bachelorarbeit von Stewen
def alpha_D(omega):
    return omega/c*np.sqrt(2*NA*kB*T*np.log(2)/M37)

def alpha_p(gamma_self,n):
    return (296/T)**n*gamma_self*p_atm*100*c/(2*np.pi)
def alpha_v(omega,gamma_self,n):
    return 0.5346*alpha_p(gamma_self,n)+np.sqrt(0.2166*alpha_p(gamma_self,n)**2+alpha_D(omega)**2)
#Erstellen von leeren Listen, in die die Werte für die verschiedenen Anfangszustände gespeichert
werden, bevor sie aufsummiert werden
start0 = []
start1 = []
start2 = []
#Brechungsindex-Offset für den jewiligen Druck wird berechnet
n11 = n(n1,p_torr,22)
n22 = n(n2,p_torr,22)
lamba0 = 1/2830.36e2
lamba2 = 1/(1/1064e-9+2/lamba0)
#Berechnung des MIR-Brechungsindexoffset-Wertes für die jeweilige Wellenlänge und den jeweiligen
Druck
n0 = f(lamba0,*fitParameter)

n00 = n(n0,p_torr,22)

N = 0.24*p/(kB*T)

for i in range(len(Estart)):
    temp = []
    for j in range(len(E0)):
        if E0[j]==Estart[i]:
            omega = 2*np.pi*c/lamba0
            mu0 = mu(A0[j],v0_w[j])
            gamma = alpha_v(1/(lamba0*1e2), gamma_self0[j], n_0[j])
            delta1 = v0_w[j]-omega-1j*gamma
            delta2 = v0_w[j]+omega+1j*gamma
            temp.append(mu0**2*(1/delta1+1/delta2))
    start0.append(rho[i]*sum(temp))

    temp = []
    for j in range(len(E1)):
        if E1[j]==Estart[i]:
            omega = 2*np.pi*c/1064e-9
            mu1 = mu(A1[j],v1_w[j])
            gamma = alpha_v(1/1064e-7, gamma_self1[j], n_1[j])
            delta1 = v1_w[j]-omega-1j*gamma
            delta2 = v1_w[j]+omega+1j*gamma
            temp.append(mu1**2*(1/delta1+1/delta2))
    start1.append(rho[i]*sum(temp))

    temp = []
    for j in range(len(E2)):
        if E2[j]==Estart[i]:
            omega = 2*np.pi*c/lamba2
            mu2 = mu(A2[j],v2_w[j])
            gamma = alpha_v(1/(lamba2*1e2), gamma_self2[j], n_2[j])
            delta1 = v2_w[j]-omega-1j*gamma
            delta2 = v2_w[j]+omega+1j*gamma
            temp.append(mu0**2*(1/delta1+1/delta2))
    start2.append(rho[i]*sum(temp))
#Berechnung der verschiedenen Xi-Werte
Xi1_37 = N/(3*epsilon0*hbar)*sum(start1)
Xi0_37 = N/(3*epsilon0*hbar)*sum(start0)
Xi2_37 = N/(3*epsilon0*hbar)*sum(start2)


Xi1 = Xi1_37+Xi1_35
Xi0 = Xi0_37+Xi0_35
Xi2 = Xi2_37+Xi2_35

#Berechnung der Brechungsindizes
n111 = np.sqrt(1+Xi1)+n11
n000 = np.sqrt(1+Xi0)+n00
n222 = np.sqrt(1+Xi2)+n22
#Komplexe Phasenfehlanpassung

dkc = 2*np.pi/lamba2*n22-4*np.pi/lamba0*n00-2*np.pi/1064e-9*n11
#Absortionskoeffizienten
alpha0 = 2*n000.imag/c
alpha1 = 2*n111.imag/c
alpha2 = 2*n222.imag/c
#reelle Phasenfehlanpassungen

k0 = 2*np.pi*n000.real/lamba0
k1 = 2*np.pi*n111.real/1064e-9
k2 = 2*np.pi*n222.real/lamba2
dk = k2-2*k0-k1

#Definition des J3 nach Lago Paper

a = b*dkc/2

def Integrand(z,z0,L):
    return np.exp(-1j*a*z)/(1+1j*(z-L/b*2*z0/L))**2
#Schrittweite der Integralberechnung
dn = 1e-5
#Integrationsbereich
x = np.arange(-L/b,L/b,dn)

#folgende Bedingung wurde nur sicherhaltshalber eingeführt, um bei zu kleiner Wahl der Schrittweite
beim Integral eine Überlastung
#des Arbeitsspeichers zu verhindern

if len(x)>1e8:
    print('!!!MEMORY ERROR!!!')
    sys.exit()

#Abspeichern der Werte in den vorbereiteten Matrizen
for i in range(len(z0)):
    J3[i] = abs(sum(Integrand(x,z0[i],L)*dn))**2
    z0z[i] = z0[i]*1e2
    bdk[i] = b*dk


pl.title(f'J3, Länge: {L}')
pl.plot(z0z, J3)
pl.xlabel('z0 in cm')
pl.ylabel('J3')
pl.show()

pl.title(f'b*dk, Länge: {L}')
pl.plot(z0z, bdk)
pl.xlabel('z0 in cm')
pl.ylabel('bdk')
pl.show()




zR = 30e-3
b = 2*zR
L = 5e-2

dk = np.linspace(-100,100,100)

J3 = []
for i in range(len(dk)):
    print(dk[i])
    a = b*dk[i]/2

    def Integrand(z,z0,L):
        return np.exp(-1j*a*z)/(1+1j*(z-L/b*2*z0/L))**2

    dn = 1e-6

    x = np.arange(-L/b,L/b,dn)

    J3.append(abs(sum(Integrand(x,3.65e-2,L)*dn))**2)

pl.plot(dk,J3)
pl.xlabel('dk in m^-1')
pl.ylabel('|J3|^2')
pl.show()
"""

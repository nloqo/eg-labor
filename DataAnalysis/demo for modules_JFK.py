# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 11:16:43 2022

Demo-File for application of modules_JFK

@author: kinder
"""

from importlib import reload  # erlaubt reload von verwendeten Modulen

import numpy as np
import matplotlib.pyplot as plt
import analysis.modules_JFK as m

m = reload(m)  # reloads the module to account for changes

# import pyqtgraph as pg  # Zum interaktiven plotten

# %gui qt
# Nötig in spyder / jupyter, um die Gui zu aktivieren.
# """Mit Magic commands kann man in Ipython das Backend zur Anzeige ändern"""

# interaktive Anzeige im Notebook
# %matplotlib qt
# Anzeige in einem extra Fenster. rcParams darf NICHT pdf sein!

# %% show beamprofiler result
idler_file = "2022_07_06T17_20_26"
m.plot_beamprofiler(idler_file, normalized=False, key="idler")

# %% show Wavemeter measurement corresponding to beamprofiler measurement
idler_file = "2022_04_22T17_04_37"
idler_file = "2022_07_06T17_20_26"
m.plot_beamprofiler(idler_file, normalized=False, key="wavelength")

# %% show camera image with background subtraction
image_file = "2022_05_10T12_21_39"
background_file = "2022_05_10T12_28_02"
m.plot_image(image_file, background_file, normalized=True)

# %% plot absorption spectrum
spectr_file = "2022_06_10T14_15_38"
dict1 = m.import_file(spectr_file)
x, y = m.idler2signal(
    np.array(dict1["wavelengthOSA"]) * 1e9, np.array(dict1["wavemeter"])
), 1e3 * np.array(dict1["Nova1"])
y2 = np.array(dict1["Nova2"]) * 1e3

fig, ax = plt.subplots()

m.plot_options(
    fig,
    ax,
    xlabel="idler wavelength (nm)",
    ylabel="idler power (mW)",
    image_width=16.5,
    aspect_ratio=2,
)
ax.plot(*m.remove_nans(x, y), ".", label="idler")
ax.legend(loc="lower left")
ax2 = ax.twinx()
m.plot_options(fig, ax2, xlabel="idler wavelength (nm)", image_width=16.5, aspect_ratio=2)
ax2.set_ylabel("signal power (mW)", color="red")
ax2.plot(*m.remove_nans(x, y2), "r.", label="signal")

ax2.legend()
plt.tight_layout()
# m.save_plot('absorption_spectr')
plt.show()

# %% OPO: stability measurement
name = "2022_07_08T16_32_46"

dict1 = m.import_file(name)
signal_wavelength_mean_nm = np.nanmean(dict1["meanwavelengthOSA"]) * 1e9

x, y = m.osa_detuning(dict1, "time", power_limit_dbm=-40)
# time data
x1 = (x.copy() - x[0]) / 60

fig, ax[0] = plt.subplots()
m.plot_options(
    fig, ax[0], xlabel="time (min)", ylabel="detuning (GHz)", image_width=16.5, aspect_ratio=2
)

ax[0].plot(
    *m.remove_nans(x1, y),
    ".",
    markersize=1,
    label="$\lambda_{\mathrm{sig}} = $" + f"{signal_wavelength_mean_nm:.3f}nm",  # noqa: W605
)
ax[0].legend()

# time data
x2 = np.array(dict1["time"])
x2 = (x2.copy() - x2[0]) / 60
y2 = np.array(dict1["Nova1"]) * 1e3

ax2 = ax[0].twinx()
m.plot_options(fig, ax[0], xlabel="time (min)", image_width=16.5, aspect_ratio=2)
ax2.set_ylabel("idler power (mW)", color="red")
ax2.plot(x2, y2, "r.", markersize=1)
plt.tight_layout()
# m.save_plot('OPO-stability')
plt.show()

# %% OPO: check temperature stabilisation
name = "2022_07_08T16_32_46"
dict1 = m.import_file(name)

signal_wavelength_mean_nm = np.nanmean(dict1["wavelength"]) * 1e9
y = m.nm2hz(np.array(dict1["wavelength"]) * 1e9) - m.nm2hz(signal_wavelength_mean_nm)
# y2 = np.array(dict1['Nova2'])*1e3
# time data
x = np.array(dict1["time"]) / 60
x = x.copy() - x[0]

fig, ax = plt.subplots(2)
# plot OPO signal detuning and idler power
m.plot_options(fig, ax[0], ylabel="detuning (GHz)", image_width=16.5, aspect_ratio=2)
ax[0].plot(x, y, ".", label=f"mean signal wavelength: {1e-9*signal_wavelength_mean_nm:.3f}nm")
# ax[0].set_ylim([-3, 3])
ax[0].legend(loc="center right")
ax2 = ax[0].twinx()
m.plot_options(
    fig, ax2, xlabel="time (min)", ylabel="idler power (mW)", image_width=16.5, aspect_ratio=2
)
# ax2.plot(x, y2, 'r-')

# plot temperature
temp_ist = np.array(dict1["OPOArduinoIst"])
temp_soll = np.array(dict1["OPOArduinoSoll"])
m.plot_options(
    fig, ax[1], xlabel="time (min)", ylabel="temperature (°C)", image_width=16.5, aspect_ratio=2
)
ax[1].plot(*m.remove_nans(x, temp_ist), "gray", label="T$_{\mathrm{ist}}$")  # noqa: W605
ax[1].plot(
    *m.remove_nans(*m.binning(x, temp_ist, binsize=0.5)[:2]),
    "r",
    label="T$_{\mathrm{ist}}$ (gebinnt)",  # noqa: W605
)
ax[1].plot(*m.remove_nans(x, temp_soll), "b", label="T$_{\mathrm{soll}}$")  # noqa: W605
# ax[1].set_ylim([37.8, 38.3])
ax[1].legend(loc="upper left", ncol=3)

plt.tight_layout()
# m.save_plot('OPO-temp-stability')
plt.show()

# %% OPO: etalon scan
name = "2022_06_29T11_54_42"
dict1 = m.import_file(name)
signal_wavelength_mean_nm = np.nanmean(dict1["meanwavelengthOSA"]) * 1e9

fig, ax = plt.subplots(2)
# OSA detuning for the signal radiation and signal power (OSA peak level)
x, y = m.osa_detuning(dict1, "etalonV", power_limit_dbm=-40)
m.plot_options(fig, ax[0], ylabel="detuning (GHz)", image_width=16.5, aspect_ratio=2)
ax[0].plot(
    *m.remove_nans(x, y),
    ".",
    markersize=1,
    label="$\lambda_{\mathrm{sig}} = $" + f"{signal_wavelength_mean_nm:.3f}nm",  # noqa: W605
)
ax[0].legend(loc="upper left", borderpad=0.1)
ax2 = ax[0].twinx()
m.plot_options(fig, ax2, image_width=16.5, aspect_ratio=2)

x1 = np.array(dict1["etalonV"])
y1 = 1e6 * m.dbm2w(np.array(dict1["meanpeaklevelOSA"]))
ax2.set_ylabel("signal power ($\mathrm{\mu W}$)", color="red")  # noqa: W605
ax2.plot(*m.remove_nans(x1, y1), "r.", markersize=1)

# second plot (idler and signal power)
x2 = np.array(dict1["etalonV"])
y2 = np.array(dict1["Nova1"]) * 1e3
y3 = np.array(dict1["Nova2"]) * 1e3

m.plot_options(
    fig,
    ax[1],
    xlabel="etalon angle (arb. u.)",
    ylabel="idler power (mW)",
    image_width=16.5,
    aspect_ratio=2,
)
ax[1].plot(*m.remove_nans(x2, y2), "b.", markersize=1)
ax2 = ax[1].twinx()
m.plot_options(fig, ax2, image_width=16.5, aspect_ratio=2)
ax2.set_ylabel("signal power (mW)", color="red")
ax2.plot(*m.remove_nans(x2, y3), "r.", markersize=1)

plt.tight_layout()
# m.save_plot('etalon-scan')
plt.show()


# %% humidity dependence plot
name = "2022_06_15T14_14_43"
dict1 = m.import_file(name)
signal_wavelength_mean_nm = np.nanmean(dict1["meanwavelengthOSA"]) * 1e9


y1 = 1e-9 * (
    m.nm2hz(np.array(dict1["meanwavelengthOSA"]) * 1e9) - m.nm2hz(signal_wavelength_mean_nm)
)
y2 = np.array(dict1["Nova1"]) * 1e3
y3 = np.array(dict1["Nova2"]) * 1e3
x = np.array(dict1["hum"])
fig, ax = plt.subplots()
m.plot_options(
    fig, ax, xlabel="humidity (%)", ylabel="idler power (mW)", image_width=16.5, aspect_ratio=2
)
ax.plot(x, y2, ".", label=f"mean signal wavelength: {signal_wavelength_mean_nm:.3f}nm")
# ax.set_ylim([-3, 3])
ax.legend(loc="lower center")
# ax.invert_xaxis()
ax2 = ax.twinx()
m.plot_options(fig, ax2, xlabel="sample", image_width=16.5, aspect_ratio=2)
ax2.set_ylabel("signal power (mW)", color="red")
ax2.plot(x, y3, "r-")
# plt.tight_layout()
# m.save_plot('OPO-stability')
plt.show()

"""
Importing the modules_JFK from analysis, to make it backwards compatible.
"""

from warnings import warn

from analysis.modules_JFK import *  # noqa

warn("Importing `modules_JFK` in this folder is deprecated, use `analysis.modules_JFK`instead.",
     FutureWarning)

# -*- coding: utf-8 -*-
"""
Testing the TMCMotorMaster.

Created on Thu Jan 20 18:29:42 2022 by Benedikt Moneke
"""

import pytest

from devices import testClasses as t

from TMCMotorMaster import TMCMotorMaster


class Empty:
    def function(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


@pytest.fixture()
def empty():
    return Empty()


class QSettings(t.QSettings):
    def __init__(self):
        global settings
        self.settings = settings


class Test_setSettings:
    @pytest.fixture
    def setup(self, monkeypatch, empty):
        global settings
        settings = {"interval": "500", "deadband": 20}
        monkeypatch.setattr("PyQt6.QtCore.QSettings", QSettings)
        empty.timer = Empty()
        empty.timer.setInterval = empty.timer.function
        TMCMotorMaster.setSettings(empty)
        return empty

    def test_timer(self, setup):
        assert setup.timer.args[0] == 500

    def test_port(self, setup):
        assert setup.intercom.address == ["127.0.0.1", 11105, 1]

    def test_deadband(self, setup):
        assert setup.deadband == 20


class Test_getPosition:
    @pytest.fixture(scope="class")
    def master(self):
        empty = Empty()
        empty.motorCard = t.TMC()
        empty.motorNumber = 0
        empty.sbCurrentSteps = t.QWidget()
        empty.sbCurrentPosition = t.QWidget()
        empty.motorCard.positions[0] = 12345
        empty.config = {"stepResolution": 3, "stepCount": 200, "unitSize": 10, "unitOffset": 0}
        return empty

    def test_returnValue(self, master):
        assert TMCMotorMaster.getPosition(master) == (12345, 77.15625)

    @pytest.fixture(scope="class")
    def positive(self, master):
        TMCMotorMaster.getPosition(master)
        return master

    def test_steps(self, positive):
        assert positive.sbCurrentSteps.value() == 12345

    def test_units(self, positive):
        assert positive.sbCurrentPosition.value() == 77.15625

    @pytest.fixture()
    def negative(self, master):
        positions = master.motorCard.positions
        master.motorCard.positions[0] = 4294954951
        TMCMotorMaster.getPosition(master)
        yield master
        master.motorCard.positions = positions

    def test_negativePosition(self, negative):
        assert negative.sbCurrentSteps.value() == -12345


class Test_getDeadband:
    testing = (
        (0, 0),
        (2**14 + 1, 0.44447835286458337),
        (2**13, 0.16666666666666666),
        (2**11, 0),
        (-100, 0),
        (-(2**13), -0.16666666666666666),
        (-(2**12), -0.02777777777777777),
    )

    @pytest.mark.parametrize("raw, filtered", testing)
    def test_getDeadband(self, empty, raw, filtered):
        empty.deadband = 10
        assert TMCMotorMaster.getDeadband(empty, raw) == filtered

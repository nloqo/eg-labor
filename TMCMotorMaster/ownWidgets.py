"""Definition of proper widgets."""
from PyQt6.QtWidgets import QPushButton
from PyQt6.QtCore import pyqtSignal


class Capturing(QPushButton):
    """A pushbutton, that captures the keys and emits events."""

    keyPressed = pyqtSignal(object)
    keyReleased = pyqtSignal(object)

    def keyPressEvent(self, a0):
        self.keyPressed.emit(a0)
        # super().keyPressEvent()

    def keyReleaseEvent(self, e):
        self.keyReleased.emit(e)

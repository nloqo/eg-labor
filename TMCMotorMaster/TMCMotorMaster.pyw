"""
Main file of the base program.

created on 14.12.2021 by Benedikt Burger
"""

# Standard packages.
import logging

from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot

from devices import hid, motors
from pyleco.utils.parser import parser, parse_command_line_parameters
from pyleco.json_utils.errors import ServerError
from pyleco_extras.gui_utils.base_main_window import LECOBaseMainWindowDesigner, Path
from pyleco_extras.directors.tmc_motor_director import TMCMotorDirector

# Local packages.
from data import Settings, referenceSearch


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class TMCMotorMaster(LECOBaseMainWindowDesigner):
    """Define the main window and essential methods of the program."""

    actionPublish: QtGui.QAction
    actionReadConfig: QtGui.QAction
    actionDisableLimits: QtGui.QAction
    actionReferenceSearch: QtGui.QAction
    actionShowMotors: QtGui.QAction
    actionShowInputs: QtGui.QAction
    actionConnectGamepad: QtGui.QAction

    sbCOM: QtWidgets.QSpinBox
    pbConnect: QtWidgets.QPushButton
    pbConfig: QtWidgets.QPushButton
    sbMotor: QtWidgets.QSpinBox
    leConnectName: QtWidgets.QLineEdit
    leProgram: QtWidgets.QLineEdit
    leKey: QtWidgets.QLineEdit
    pbGetConfig: QtWidgets.QPushButton

    wCont: QtWidgets.QWidget  # contains the following:
    pbControl: QtWidgets.QPushButton
    pbGetAddress: QtWidgets.QPushButton
    pbSetAddress: QtWidgets.QPushButton
    sbAddress: QtWidgets.QSpinBox
    pbGetName: QtWidgets.QPushButton
    pbSetName: QtWidgets.QPushButton
    pbGetPosition: QtWidgets.QPushButton
    pbSetPosition: QtWidgets.QPushButton
    pbMoveTo: QtWidgets.QPushButton
    pbMoveByNegative: QtWidgets.QPushButton
    pbMoveByPositive: QtWidgets.QPushButton
    pbRotatePositive: QtWidgets.QPushButton
    pbRotateNegative: QtWidgets.QPushButton
    pbStop: QtWidgets.QPushButton
    cbSteps: QtWidgets.QCheckBox
    sbVoltage: QtWidgets.QDoubleSpinBox
    sbUnits: QtWidgets.QDoubleSpinBox
    sbSteps: QtWidgets.QSpinBox
    sbCurrentPosition: QtWidgets.QDoubleSpinBox
    sbCurrentSteps: QtWidgets.QDoubleSpinBox
    sbVelocity: QtWidgets.QSpinBox
    sbAcceleration: QtWidgets.QSpinBox
    leName: QtWidgets.QLineEdit
    sbController: QtWidgets.QSpinBox
    slController: QtWidgets.QSlider

    def __init__(
        self, name: str = "TMCMotorMaster", host: str = "localhost", **kwargs
    ) -> None:
        """Initialize the main window and its settings."""
        # Use initialization of parent class QMainWindow.
        super().__init__(
            name=name,
            ui_file_name="TMCMotorMaster",
            ui_file_path=Path(__file__).parent / "data",
            host=host,
            logger=log,
            settings_dialog_class=Settings.Settings,
            **kwargs,
        )

        self.rotating = False
        self.director = TMCMotorDirector(actor="", communicator=self.communicator)
        self.remote = False

        # Connect actions to slots.
        self.actionReadConfig.triggered.connect(self.readConfig)
        self.actionDisableLimits.triggered.connect(self.disableLimits)
        self.actionConnectGamepad.triggered.connect(self.connectGamepad)
        self.actionReferenceSearch.triggered.connect(self.referenceSearch)
        self.actionShowMotors.triggered.connect(self.get_motor_names)
        self.actionShowInputs.triggered.connect(self.show_inputs)

        # Connect buttons to slots.
        #   General
        self.pbConnect.clicked.connect(self.connect)
        self.sbMotor.valueChanged.connect(self.setMotorNumber)
        self.pbConfig.clicked.connect(self.openMotorConfig)
        self.pbGetConfig.clicked.connect(self.getConfig)
        self.pbControl.clicked.connect(self.control)
        #   Once connected
        self.pbGetAddress.clicked.connect(self.getAddress)
        self.pbSetAddress.clicked.connect(self.setAddress)
        self.sbAddress.valueChanged.connect(self.addressChanged)
        self.pbGetName.clicked.connect(self.getName)
        self.pbSetName.clicked.connect(self.setName)
        self.pbGetPosition.clicked.connect(self.getPosition)
        self.pbSetPosition.clicked.connect(self.setPosition)
        self.pbMoveTo.clicked.connect(self.moveTo)
        self.pbMoveByNegative.clicked.connect(self.moveByNegative)
        self.pbMoveByPositive.clicked.connect(self.moveByPositive)
        self.pbRotatePositive.clicked.connect(self.rotatePositive)
        self.pbRotateNegative.clicked.connect(self.rotateNegative)
        self.pbStop.clicked.connect(self.stopMotor)
        self.cbSteps.toggled.connect(self.toggleSteps)
        self.toggleSteps()  # connect spinboxes to each other.

        self.actionConnectGamepad.setChecked(True)
        self.gamepad = hid.Gamepad()
        self.gamepad.disconnected.connect(self.gamepadConnectionLost)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.regularReadout)

        self.setSettings()

        # Load last configuration.
        self.restoreConfiguration()

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        self.stop_listen()
        sets = QtCore.QSettings()
        sets.setValue("port", self.sbCOM.value())
        sets.setValue("name", self.leConnectName.text())
        self.connect(False)
        self.gamepad.disconnect()

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    def restoreConfiguration(self) -> None:
        """Restore the last used configuration."""
        sets = QtCore.QSettings()
        self.sbCOM.setValue(sets.value("port", type=int))
        self.leConnectName.setText(sets.value("name"))
        self.motorNumber = 0
        self.config = sets.value("motor", type=dict)
        self.sbMotor.setValue(self.config.get("motorNumber", 0))
        self.sbUnits.setSuffix(self.config.get("unitSymbol", "steps"))
        self.sbCurrentPosition.setSuffix(self.config.get("unitSymbol", "steps"))

    def setSettings(self) -> None:
        """Apply chosen settings."""
        settings = QtCore.QSettings()
        self.timer.setInterval(settings.value("interval", 1000, type=int))
        self.deadband = settings.value("deadband", defaultValue=10, type=int)

    "Configuration / Connection"

    @pyqtSlot()
    def readConfig(self) -> None:
        """Read the current motor configuration and show it."""
        data = {}
        text = "Read motor configuration:"
        try:
            mc = self.motorCard
        except AttributeError:
            return
        ap = mc.motors[0].AP
        aps = {
            "maxCurrent": ap.MaxCurrent,
            "standbyCurrent": ap.StandbyCurrent,
            "positioningSpeed": ap.MaxVelocity,
            "acceleration": ap.MaxAcceleration,
            "stepResolution": ap.MicrostepResolution,
            "stallguardThreshold": ap.SG2Threshold,
            "leftLimitDisabled": ap.LeftLimitSwitchDisable,
            "RightLimitDisabled": ap.RightLimitSwitchDisable,
            "leftLimitReached": ap.LeftEndstop,
            "rightLimitReached": ap.RightEndstop,
            "positionReached": ap.PositionReachedFlag,
        }
        try:
            for key in aps.keys():
                data[key] = mc.get_axis_parameter(aps[key], self.motorNumber)
                text += f"{key}:\t{data[key]}\n"
        except Exception as exc:
            print(exc)
        dialog = QtWidgets.QMessageBox()
        dialog.setWindowTitle("Read motor configuration")
        dialog.setText(text)
        sb = QtWidgets.QMessageBox.StandardButton
        dialog.setStandardButtons(sb.Apply | sb.Close)
        if dialog.exec() == sb.Apply:
            for key in (
                "maxCurrent",
                "standbyCurrent",
                "positioningSpeed",
                "acceleration",
                "stepResolution",
                "stallguardThreshold",
            ):
                self.config[key] = data[key]
            print(self.config)
            settings = QtCore.QSettings()
            settings.setValue("motor", self.config)

    @pyqtSlot(bool)
    def connectGamepad(self, checked: bool) -> None:
        """Establish a connection to the gamepad."""
        if checked:
            self.gamepad.connect()
        else:
            self.gamepad.disconnect()

    @pyqtSlot()
    def gamepadConnectionLost(self) -> None:
        """Uncheck the connected gamepad button."""
        self.actionConnectGamepad.setChecked(False)

    @pyqtSlot(bool)
    def control(self, checked: bool) -> None:
        """Control the motor via keyboard."""
        if checked:
            self.pbControl.grabKeyboard()
            self.pbControl.keyPressed.connect(self.keyPress)  # type: ignore
            self.pbControl.keyReleased.connect(self.keyRelease)  # type: ignore
        else:
            self.pbControl.releaseKeyboard()
            try:
                self.pbControl.keyPressed.disconnect()  # type: ignore
                self.pbControl.keyReleased.disconnect()  # type: ignore
            except TypeError:
                pass  # no connection to any slot

    @pyqtSlot(bool)
    def connect(self, checked: bool) -> None:
        """(Dis)connect the motor card."""
        if checked:
            name = self.leConnectName.text()
            if "." in name:
                # Remote connection
                self.motorCard = self.director
                self.director.actor = name
                self.show_info(f"Remote connection established to {name}.")
                self.remote = True
                try:
                    self.getConfigRemote()
                except (ServerError, TimeoutError):
                    self.pbConnect.setChecked(False)
                    return
            else:
                self.remote = False
                # Local connection
                COM = None
                if name:
                    try:
                        COM = motors.getPort(name)
                    except ValueError as exc:
                        self.show_error(text=str(exc), exc=exc)
                        self.pbConnect.setChecked(False)
                        return
                if not COM:
                    COM = self.sbCOM.value()
                else:
                    self.sbCOM.setValue(COM)
                try:
                    self.connectionManager = ConnectionManager(f"--port COM{COM}")
                    self.motorCard = TMCM6110(self.connectionManager.connect())
                except Exception as exc:
                    log.exception("Connection failed.", exc_info=exc)
                    self.statusBar().showMessage("Connection failed.")  # type: ignore
                    self.pbConnect.setChecked(False)
                    return
                else:
                    self.show_info(f"Successfully connected to {name} at {COM}.")
                    if (address := self.getAddress()) > 1:
                        settings = QtCore.QSettings("NLOQO")
                        settings.setValue(f"TMCport/{address}", COM)
            self.motor = self.motorCard.motors[self.motorNumber]
            self.wCont.setEnabled(True)
            self.pbConfig.setEnabled(True)
            self.gamepad.axisChanged.connect(self.onAxisChanged)
            self.gamepad.buttonPressed.connect(self.onButtonPressed)
            self.timer.start()
        else:
            self.timer.stop()
            self.control(False)
            self.pbControl.setChecked(False)
            self.wCont.setEnabled(False)
            self.pbConfig.setEnabled(False)
            try:
                self.connectionManager.disconnect()
            except AttributeError:
                pass  # Not existent
            except Exception as exc:
                print(exc)
            try:
                del self.motorCard
            except AttributeError:
                pass
            try:
                del self.motor
            except AttributeError:
                pass
            try:
                self.gamepad.axisChanged.disconnect()
                self.gamepad.buttonPressed.disconnect()
            except TypeError:
                pass

    @pyqtSlot()
    def openMotorConfig(self) -> None:
        """Open the motor configuration."""
        settings = QtCore.QSettings()
        self.config["motorNumber"] = self.motorNumber
        settings.setValue("motor", self.config)
        dialog = motors.MotorSettings("motor", "Test motor")
        if dialog.exec():
            settings = QtCore.QSettings()
            self.config = settings.value("motor", type=dict)
            self.sbMotor.setValue(self.config.get("motorNumber"))
            self.configureMotor()
            self.sbUnits.setSuffix(self.config.get("unitSymbol"))
            self.sbCurrentPosition.setSuffix(self.config.get("unitSymbol"))

    @pyqtSlot()
    def getConfig(self) -> None:
        """Load the motor config from another source."""
        source = QtCore.QSettings("NLOQO", self.leProgram.text())
        key = self.leKey.text()
        if config := source.value(key, type=dict):
            self.config = config
            self.sbMotor.setValue(self.config.get("motorNumber"))
            self.configureMotor()
            self.sbUnits.setSuffix(self.config.get("unitSymbol"))
            self.sbCurrentPosition.setSuffix(self.config.get("unitSymbol"))
            settings = QtCore.QSettings()
            settings.setValue("motor", config)

    def getConfigRemote(self) -> None:
        """Get the configuration of the remote motor card."""
        try:
            self.config = self.director.get_configuration(self.motorNumber)
        except ServerError as exc:
            self.show_error("Getting remote configuration failed.", exc=exc)
            raise
        else:
            self.sbUnits.setSuffix(self.config.get("unitSymbol"))
            self.sbCurrentPosition.setSuffix(self.config.get("unitSymbol"))
            if self.cbSteps.isChecked():
                self.updateUnits(self.sbSteps.value())
            else:
                self.updateSteps(self.sbUnits.value())

    @pyqtSlot()
    def get_motor_names(self) -> dict:
        try:
            self.motorCard
        except AttributeError:
            return {}
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle("Motor names")
        if self.remote:
            d = self.director.get_motor_dict()
            msg.setText(
                "\n".join([f"number: {value}\tname: {key}" for key, value in d.items()])
            )
        else:
            msg.setText("Local motors don't have a motor dict")
            d = {}
        msg.exec()
        return d

    @pyqtSlot()
    def show_inputs(self):
        try:
            motorCard = self.motorCard
        except AttributeError:
            return
        inputs: list[str] = []
        for i in range(8):
            inputs.append(str(motorCard.get_digital_input(i)))
        ains: list[str] = []
        for ain in (TMCM6110.IO.AIN0, TMCM6110.IO.AIN4):
            ains.append(str(motorCard.get_analog_input(ain)))
        lines = ["Digital"] + inputs + ["Analog"] + ains
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle("Inputs")
        msg.setText("\n".join(lines))
        msg.exec()

    def configureMotor(self) -> None:
        """Configure a motor."""
        if not hasattr(self, "motor"):
            return
        try:
            motors.configureMotor(self.motorCard, self.config)
        except Exception:
            log.exception("Configure motor failed.")

    @pyqtSlot(int)
    def setMotorNumber(self, number: int) -> None:
        """Store the motor number."""
        self.motorNumber = number
        self.config["motorNumber"] = number
        try:
            self.motor = self.motorCard.motors[number]
        except AttributeError:
            pass
        if self.remote:
            self.getConfigRemote()

    @pyqtSlot()
    def referenceSearch(self) -> None:
        """Open the reference search window."""
        try:
            motorCard = self.motorCard
        except AttributeError:
            return
        dialog = referenceSearch.ReferenceSearch(motorCard, self.motorNumber)
        dialog.exec()

    "Motor itself"

    @pyqtSlot()
    def getAddress(self) -> int:
        """Get the serial address."""
        address = self.motorCard.get_global_parameter(66, 0)
        self.sbAddress.setValue(address)
        return address

    @pyqtSlot()
    def setAddress(self) -> None:
        """Set the serial address."""
        address = self.sbAddress.value()
        self.motorCard.set_global_parameter(66, 0, address)
        if address > 1:
            settings = QtCore.QSettings("NLOQO")
            settings.setValue(f"TMCport/{address}", self.sbCOM.value())

    @pyqtSlot(int)
    def addressChanged(self, address: int) -> None:
        """Update the name for this address."""
        self.getName()

    @pyqtSlot()
    def getName(self) -> str:
        """Get the card name."""
        settings = QtCore.QSettings("NLOQO")
        settings.beginGroup("TMC")
        for key in settings.allKeys():
            if settings.value(key, type=int) == self.sbAddress.value():
                self.leName.setText(key)
                return key
        self.leName.setText("")
        return ""

    @pyqtSlot()
    def setName(self) -> None:
        """Set the card name."""
        address = self.sbAddress.value()
        settings = QtCore.QSettings("NLOQO")
        settings.beginGroup("TMC")
        # remove old keys
        for key in settings.allKeys():
            if settings.value(key, type=int) == address:
                settings.remove(key)
        if name := self.leName.text():
            settings.setValue(name, address)

    @pyqtSlot()
    def get_voltage(self) -> float:
        try:
            voltage = self.motorCard.get_analog_input(8) / 10
            self.sbVoltage.setValue(voltage)
        except Exception as exc:
            log.exception("Reading voltage failed.", exc_info=exc)
            voltage = float("nan")
        return voltage

    @pyqtSlot()
    def getVelocity(self) -> tuple[int, int]:
        """Get velocity and acceleration."""
        motor = self.motor
        velocity = motor.actual_velocity
        acceleration = motor.get_axis_parameter(
            motor.AP.ActualAcceleration, signed=True
        )
        try:
            self.sbVelocity.setValue(velocity)
            self.sbAcceleration.setValue(acceleration)
        except OverflowError:
            log.exception(
                f"Could net set velocity to {velocity} or acceleration to {acceleration}."
            )
        return velocity, acceleration

    @pyqtSlot()
    def getPosition(self) -> tuple[int, float]:
        """Get the current position."""
        position = self.motor.actual_position
        self.sbCurrentSteps.setValue(position)
        try:
            units = motors.stepsToUnits(position, self.config)
        except KeyError:
            units = position
        try:
            self.sbCurrentPosition.setValue(units)
        except OverflowError:
            log.exception(f"Could not set units to {units}.")
        return position, units

    @pyqtSlot()
    def setPosition(self) -> None:
        """Set the current position."""
        self.stopMotor()
        self.motor.actual_position = int(self.sbSteps.value())

    def get_position_reached(self) -> bool:
        flag = self.motor.get_position_reached()
        self.statusBar().showMessage(f"Pos reached: {flag}")  # type: ignore
        return flag

    @pyqtSlot()
    def moveTo(self) -> None:
        """Move to a specific position."""
        velocity = self.config.get("positioningSpeed", 0)
        try:
            self.motor.move_to(int(self.sbSteps.value()), velocity)
        except ServerError as exc:
            self.show_error(
                "Move to failed, is the motor voltage sufficient?", "Move to", exc=exc
            )
        except Exception as exc:
            log.exception("Move to failed.", exc_info=exc)
        else:
            self.rotating = False

    @pyqtSlot()
    def moveByNegative(self) -> None:
        """Move by a certain distance in negative direction."""
        velocity = self.config.get("positioningSpeed", 0)
        try:
            self.motor.move_by(-int(self.sbSteps.value()), velocity)
        except ServerError as exc:
            self.show_error(
                "Move by failed, is the motor voltage sufficient?", "Move by", exc=exc
            )
        except Exception as exc:
            log.exception("Move by failed.", exc_info=exc)
        else:
            self.rotating = False

    @pyqtSlot()
    def moveByPositive(self) -> None:
        """Move by a certain distance in positive direction."""
        velocity = self.config.get("positioningSpeed", 0)
        try:
            self.motor.move_by(int(self.sbSteps.value()), velocity)
        except ServerError as exc:
            self.show_error(
                "Move by failed, is the motor voltage sufficient?", "Move by", exc=exc
            )
        except Exception as exc:
            log.exception("Move by failed.", exc_info=exc)
        else:
            self.rotating = False

    def rotate(self, velocity: int) -> None:
        try:
            self.motor.rotate(velocity=velocity)
        except ServerError as exc:
            self.show_error(
                "Rotate failed, is the motor voltage sufficient?", "Rotate", exc=exc
            )
        except Exception as exc:
            log.exception("Rotate failed.", exc_info=exc)
        else:
            self.rotating = -1 if velocity < 0 else 1

    @pyqtSlot()
    def rotateNegative(self) -> None:
        """Rotate the motor in negative direction."""
        self.rotate(-self.sbController.value())

    @pyqtSlot()
    def rotatePositive(self) -> None:
        """Rotate the motor in negative direction."""
        self.rotate(self.sbController.value())

    @pyqtSlot()
    def stopMotor(self) -> None:
        """Stop the motor."""
        self.motor.stop()
        self.rotating = False

    @pyqtSlot(bool)
    def disableLimits(self, checked: bool) -> None:
        """Disable the limit switches if checked."""
        try:
            m = self.motor
        except AttributeError:
            return
        state = 1 if checked else 0
        m.set_axis_parameter(m.AP.LeftLimitSwitchDisable, state)
        m.set_axis_parameter(m.AP.RightLimitSwitchDisable, state)

    "GUI"

    @pyqtSlot()
    def regularReadout(self) -> None:
        """Read all the values and send them, if desired."""
        try:
            position, units = self.getPosition()
            velocity, acceleration = self.getVelocity()
            self.get_voltage()
        except Exception as exc:
            log.exception("Regular readout failed.", exc_info=exc)
            self.connect(False)
            self.pbConnect.setChecked(False)
            dialog = QtWidgets.QMessageBox()
            dialog.setIcon(QtWidgets.QMessageBox.Icon.Warning)
            dialog.setWindowTitle("Regular readout error")
            dialog.setText(f"Regular readout failed with {type(exc).__name__}: {exc}")
            dialog.exec()
            return
        else:
            data = {
                "position": position,
                "units": units,
                "velocity": velocity,
                "acceleration": acceleration,
            }
        try:
            self.publisher.send_data(data)
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc_info=exc)

    @pyqtSlot()
    def toggleSteps(self) -> None:
        """Toggle which input is the current one."""
        if self.cbSteps.isChecked():
            self.sbSteps.valueChanged.connect(self.updateUnits)
            try:
                self.sbUnits.valueChanged.disconnect()
            except TypeError:
                pass  # not connected.
        else:
            self.sbUnits.valueChanged.connect(self.updateSteps)
            try:
                self.sbSteps.valueChanged.disconnect()
            except TypeError:
                pass  # not connected.

    @pyqtSlot(float)
    def updateSteps(self, units: float) -> None:
        """Set steps to the corresponding `units`."""
        try:
            steps = motors.unitsToSteps(units, self.config)
        except KeyError:
            pass
        else:
            self.sbSteps.setValue(steps)

    @pyqtSlot(float)
    def updateUnits(self, steps: int) -> None:
        """Set units to the corresponding `steps`."""
        try:
            units = motors.stepsToUnits(steps, self.config)
        except KeyError:
            pass
        else:
            self.sbUnits.setValue(units)

    "Keyboard control"

    @pyqtSlot(object)
    def keyPress(self, event) -> None:
        """React to a pressed key."""
        if event.isAutoRepeat():
            return  # Ignore autorepeat while key is held down.
        key = event.key()
        # HACK noqa for match
        match key:  # noqa
            case 16777227 | 53:  # center button, 5
                self.stopMotor()
            case 16777235 | 56 | 16777232 | 55:  # Arrow up, 8, pos1, 7
                self.rotatePositive()
            case 16777237 | 50 | 16777233 | 49:  # Arrow down, 2, end, 1
                self.rotateNegative()
            case 16777234 | 52:  # Arrow left, 4
                self.sbController.setValue(self.sbController.value() // 2)
                if self.rotating:
                    self.rotate(self.rotating * self.sbController.value())
            case 16777236 | 54:  # Arrow right, 6
                self.sbController.setValue(self.sbController.value() * 2)
                if self.rotating:
                    self.rotate(self.rotating * self.sbController.value())
            case 16777238 | 57:  # page up, 9
                self.moveByPositive()
            case 16777239 | 51:  # page down, 3
                self.moveByNegative()
            case 43:  # +
                if self.sbSteps.value() == 0:
                    self.sbSteps.setValue(1)
                else:
                    self.sbSteps.setValue(self.sbSteps.value() * 4)
            case 45:  # -
                self.sbSteps.setValue(self.sbSteps.value() // 4)
            case 16777220 | 16777221:  # main enter, keypad enter
                self.moveTo()

    @pyqtSlot(object)
    def keyRelease(self, event) -> None:
        """React to a pressed key."""
        if event.isAutoRepeat():
            return  # Ignore autorepeat while key is held down.
        key = event.key()
        match key:
            case 16777235 | 56:  # Arrow up, 8
                self.stopMotor()
            case 16777237 | 50:  # Arrow down, 2
                self.stopMotor()

    "Remote control"

    @pyqtSlot(dict)
    def remoteCommand(self, data: dict) -> None:
        """Interpret a remote command and respond if desired."""
        # TODO adjust for new command style.
        raise NotImplementedError("Not implemented according to new LECO style.")
        if not self.pbConnect.isChecked():
            if addr := data.get("query"):
                self.communicator.reply(addr, b"ERR motor not connected.")
            return
        reply = {}
        for key, value in data.items():
            match key:
                case "move_to":
                    self.sbSteps.setValue(value)
                    self.moveTo()
                case "move_by":
                    self.sbSteps.setValue(abs(value))
                    self.moveByNegative() if value < 0 else self.moveByPositive()
                case "move_to_units":
                    self.updateSteps(value)
                    self.moveTo()
                case "move_by_units":
                    self.updateSteps(abs(value))
                    self.moveByNegative() if value < 0 else self.moveByPositive()
                case "rotate":
                    self.sbController.setValue(abs(value))
                    self.rotateNegative() if value < 0 else self.rotatePositive()
                case "stop":
                    if value:
                        self.stopMotor()
                case "actual_position":
                    reply[key] = self.motor.actual_position
                case "actual_velocity":
                    reply[key] = self.motor.actual_velocity
                case "actual_position_units":
                    try:
                        reply[key] = motors.stepsToUnits(
                            self.motor.actual_position, self.config
                        )
                    except KeyError:
                        reply[key] = float("nan")
                case "get_position_reached":
                    reply[key] = self.motor.get_position_reached()
        if addr := data.get("query"):
            self.communicator.reply_json(addr, reply)

    "Gamepad"

    @pyqtSlot(str, float)
    def onAxisChanged(self, axis: str, value: float) -> None:
        """Handle the new `value` of `axis`."""
        if axis == "ABS_Y":
            # Rotate the motor according to value and the max rotation speed.
            velocity = round(self.getDeadband(value) * self.sbController.value())
            self.rotate(velocity)
        elif axis == "ABS_HAT0Y":
            if value < 0:
                if self.sbSteps.value() == 0:
                    self.sbSteps.setValue(1)
                else:
                    self.sbSteps.setValue(self.sbSteps.value() * 4)
            elif value > 0:
                self.sbSteps.setValue(self.sbSteps.value() // 4)
        elif axis == "ABS_HAT0X":
            if value < 0:
                self.sbController.setValue(self.sbController.value() // 2)
            elif value > 0:
                self.sbController.setValue(self.sbController.value() * 2)

    def getDeadband(self, value):
        """Set the central region to zero, adjust the rest accordingly."""
        value = value / 2**15  # Get a proportional value.
        # Calculate a deadband around the center of the axis.
        db = self.deadband / 100
        if value > db:
            value = (value - db) / (1 - db)
        elif value < -db:
            value = (value + db) / (1 - db)
        else:
            value = 0
        return value

    @pyqtSlot(str)
    def onButtonPressed(self, button):
        """Handle what happens if `button` is pressed."""
        if button == "BTN_EAST":
            self.stopMotor()
        elif button == "BTN_SOUTH":
            self.moveTo()
        elif button == "BTN_TL":
            self.moveByNegative()
        elif button == "BTN_TR":
            self.moveByPositive()
        elif button == "BTN_NORTH":
            self.getPosition()
            self.getVelocity()

    def show_error(
        self, text: str, caller: str = "", exc: Exception | None = None
    ) -> None:
        """Log an error and show a message to the user."""
        error = QtWidgets.QMessageBox()
        error.setIcon(QtWidgets.QMessageBox.Icon.Warning)
        error.setWindowTitle("Connection error")
        error.setText(text)
        detailedText = f"Called by {caller}." if caller else ""
        if exc is not None:
            detailedText += f"\n{type(exc).__name__} '{exc}'."
            log.exception(f"{caller} lost connection.", exc_info=exc)
        else:
            log.error(f"{caller} lost connection to the motor card.")
        error.setDetailedText(detailedText)
        error.exec()

    def show_info(self, text: str, timeout: float = 5) -> None:
        """Show info in the statusbar and log it."""
        log.info(text)
        self.statusBar().showMessage(text, int(timeout * 1000))  # type: ignore


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    parser.add_argument(
        "-t", "--motor-test", action="store_true", help="enable motor test mode."
    )
    doc = TMCMotorMaster.__doc__
    kwargs = parse_command_line_parameters(
        logger=log,
        parser=parser,
        parser_description=doc.split(":param", maxsplit=1)[0] if doc else None,
    )
    if kwargs.pop("motor_test"):
        log.info("Enable motor test mode.")
        from devices.pyTrinamicTest import ConnectionManager  # noqa: F811

        tlog = logging.getLogger("devices.PyTrinamicTest")
        tlog.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication([])  # create an application
    mainwindow = TMCMotorMaster(**kwargs)  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

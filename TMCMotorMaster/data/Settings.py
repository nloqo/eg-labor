"""
Module for the settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from qtpy import QtWidgets

from pyleco_extras.gui_utils.base_settings import BaseSettings


class Settings(BaseSettings):

    def setup_form(self, layout: QtWidgets.QFormLayout) -> None:
        interval = QtWidgets.QSpinBox()
        interval.setSuffix(" ms")
        interval.setRange(25, 100000)
        interval.setSingleStep(100)
        interval.setToolTip("Interval between regular readouts.")
        self.add_value_widget("Interval", interval, "interval", 0, int)
        deadband = QtWidgets.QSpinBox()
        deadband.setSuffix("%")
        deadband.setMaximum(100)
        deadband.setToolTip("Area around the center of the joystick, which is interpreted as zero.")
        self.add_value_widget("Deadband", deadband, "deadband", 10, int)

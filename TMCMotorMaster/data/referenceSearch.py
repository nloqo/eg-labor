# -*- coding: utf-8 -*-
"""
Window for reference search.

Created on Fri Jul  1 16:50:13 2022

@author: moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot


class ReferenceSearch(QtWidgets.QDialog):
    """
    Window for the reference search.

    :param motorCard: Connection to the motor card.
    :param motorNumber: Number of the motor to be configured.
    """

    def __init__(self, motorCard, motorNumber):
        """Initialize."""
        super().__init__()
        uic.loadUi("data/ReferenceSearch.ui", self)
        self.show()

        self.bbMode.addItems(["Left switch", "Right switch",
                              "Right and then left switch (0)",
                              "Left and then right switch (0)"])
        self.bbMode.setItemData(0, 1)
        self.bbMode.setItemData(1, 65)
        self.bbMode.setItemData(2, 2)
        self.bbMode.setItemData(3, 66)

        self.motor = motorCard.motors[motorNumber]
        self.motorNumber = motorNumber

        self.pbSet.clicked.connect(self.configure)
        self.pbStart.clicked.connect(self.start)
        self.pbStop.clicked.connect(self.stop)

        self.timer = QtCore.QTimer()
        self.timer.setInterval(250)
        self.timer.timeout.connect(self._timeout)

    @pyqtSlot()
    def configure(self):
        """Configure the motor."""
        m = self.motor
        m.set_axis_parameter(m.AP.ReferenceSearchMode,
                             self.bbMode.currentData())
        m.set_axis_parameter(m.AP.ReferenceSearchSpeed, self.sbSearch.value())
        m.set_axis_parameter(m.AP.ReferenceSwitchSpeed, self.sbSwitch.value())

    def _setStatus(self, value, max):
        self.ppStatus.setValue(value)
        self.ppStatus.setMaximum(max)

    @pyqtSlot()
    def start(self):
        self._setStatus(0, 0)
        self.motor.move_by(0)
        self.motor._parent.connection.reference_search(0, self.motorNumber)
        self.timer.start()

    @pyqtSlot()
    def stop(self):
        self._setStatus(0, 1)
        self.motor._parent.connection.reference_search(1, self.motorNumber)
        self.timer.stop()

    @pyqtSlot()
    def _timeout(self):
        status = self.motor._parent.connection.reference_search(
            2, self.motorNumber)
        if not status:
            self._setStatus(1, 1)
            self.timer.stop()

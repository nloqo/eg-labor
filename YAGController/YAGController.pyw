"""
Main file of the YAGController.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import pyqtSlot

from pyleco.directors.director import Director
try:
    from pyleco.json_utils.errors import JSONRPCError
except ModuleNotFoundError:
    from jsonrpcobjects.errors import JSONRPCError

from devices.gui_utils import start_app
from devices.base_main_window import LECOBaseMainWindow

# Local packages.
from data.settings import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


seedShutter = "pico3.seedShutter"
YAGseed = "pico3.innolas"
YAGshutter = "pico3.yagShutter"

# state of shutters
closed_mark = "✖"  # '\u2716'  # heavy multiplication x
open_mark = "⭘"  # "\u2b58"  # heavy circle
enforced_close_mark = "🔒"  # "\U0001f512"  # lock


class YAGController(LECOBaseMainWindow):
    """Control the YAG (Koheras Quanta-Ray)

    :param name: Name of this program.
    :param host: Host name of the Coordinator.
    """

    gbSeedShutter: QtWidgets.QGroupBox
    lbSeedShutter: QtWidgets.QLabel
    pbSeedShutterClose: QtWidgets.QPushButton
    pbSeedShutterGet: QtWidgets.QPushButton
    pbSeedShutterOpen: QtWidgets.QPushButton

    gbShutter: QtWidgets.QGroupBox
    slFundShutter: QtWidgets.QSlider
    slSHGShutter: QtWidgets.QSlider
    lbFundShutter: QtWidgets.QLabel
    lbSHGShutter: QtWidgets.QLabel

    def __init__(self, name: str = "YAGController", host: str = "localhost", **kwargs) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(name=name, host=host,
                         ui_file_name="YAGController",  # ui file name in the data subfolder
                         settings_dialog_class=Settings,  # class of the settings dialog
                         **kwargs)

        # Apply settings
        # TODO enable save geometry, if desired, see also closeEvent
        # settings = QtCore.QSettings()
        # geometry = settings.value("geometry")
        # if geometry is not None:
        #     self.restoreGeometry(QtCore.QByteArray(geometry))
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.timeout)
        self.setSettings()
        self.timer.start()

        self.director = Director(communicator=self.communicator)

        # Connect actions to slots.
        #   seed
        self.pbSeedShutterClose.clicked.connect(self.closeSeedShutter)
        self.pbSeedShutterOpen.clicked.connect(self.openSeedShutter)
        self.pbSeedShutterGet.clicked.connect(self.getSeedShutter)
        #   shutter
        self.slFundShutter.valueChanged.connect(self.setFundShutter)
        self.slSHGShutter.valueChanged.connect(self.setSHGShutter)

        log.info(f"YAGController initialized with name '{name}'.")

    def __del__(self):
        try:
            self.stop_listen()
        except AttributeError:
            pass

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.stop_listen()

        # TODO: put in stuff you want to do before closing

        # TODO enable if desired
        # # Store the window geometry
        # settings = QtCore.QSettings()
        # settings.setValue("geometry", self.saveGeometry())

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    def setSettings(self) -> None:
        """Apply new settings, called if the settings dialog is accepted."""
        settings = QtCore.QSettings()
        self.timer.setInterval(settings.value("interval", 1000, int))

    @pyqtSlot()
    def timeout(self):
        if self.gbShutter.isChecked():
            self.getYAGShutterState()

    # seed shutter
    @pyqtSlot()
    def openSeedShutter(self):
        self._open_seed_YAG_seed_shutter(True)

    @pyqtSlot()
    def closeSeedShutter(self):
        self._open_seed_YAG_seed_shutter(False)

    def _open_seed_YAG_seed_shutter(self, state: bool) -> None:
        """Open or close the YAG seed shutter."""
        try:
            self.director.set_parameters({'shutter_open': bool(state)}, actor=seedShutter)
        except Exception as exc:
            self.statusBar().showMessage(f"Setting seed shutter failed: {exc}")
        else:
            self.getSeedShutter()

    @pyqtSlot()
    def getSeedShutter(self) -> None:
        """Get the state of the YAG seed shutter."""
        try:
            result = self.director.get_parameters(actor=seedShutter, parameters=["shutter_open"])
        except TimeoutError:
            # self.showInformation("Seed shutter timeout",
            #                      "Timeout while getting Quanta-Ray seed shutter state.")
            self.lbSeedShutter.setText("-")
        except JSONRPCError as exc:
            # self.showInformation("Seed shutter error",
            #                      f"Error while getting Quanta-Ray seed shutter state: {exc}.")
            self.lbSeedShutter.setText("-")
            self.statusBar().showMessage(f"Gettin seed shutter failed: {exc}")
        else:
            try:
                state = result["shutter_open"]
            except Exception as exc:
                log.exception(f"Shutter state data exception: {result}", exc_info=exc)
            else:
                mapping = {True: open_mark, False: closed_mark, "Unknown": "?"}
                color_mapping = {True: "orange", False: "green"}
                self.lbSeedShutter.setStyleSheet(
                    f"color: {color_mapping.get(state, 'red')}; font-weight: bold")
                self.lbSeedShutter.setText(mapping.get(state, "-"))

    # shutter
    @pyqtSlot()
    def getYAGShutterState(self) -> None:
        """Get the state of the YAG beamdump shutters."""
        mapping = {-1: enforced_close_mark, 0: closed_mark, 1: open_mark}
        colors = {enforced_close_mark: "red", closed_mark: "green", open_mark: "orange",
                  "?": "black"}
        try:
            result = self.director.get_parameters(actor=YAGshutter,
                                                  parameters=["shg_open", "fundamental_open"])
        except Exception as exc:
            log.exception("Reading YAG shutter state failed.", exc_info=exc)
            for label in (self.lbFundShutter, self.lbSHGShutter):
                label.setText("-")
                label.setStyleSheet("color: black")
        else:
            shg = mapping.get(result.get("shg_open", -1000), "?")
            fund = mapping.get(result.get("fundamental_open", -1000), "?")
            self.lbFundShutter.setText(fund)
            self.lbSHGShutter.setText(shg)
            for value, label in ((fund, self.lbFundShutter), (shg, self.lbSHGShutter)):
                label.setStyleSheet(f"color: {colors.get(value, 'black')}; font-weight: bold")

    @pyqtSlot(int)
    def setSHGShutter(self, value: int) -> None:
        self.director.set_parameters({"shg_switch": value}, actor=YAGshutter)

    def setFundShutter(self, value: int) -> None:
        """Set the YAG fundamental shutter switch to a state: 1 open, 0 neutral, -1 emergency."""
        self.director.set_parameters({"fundamental_switch": value}, actor=YAGshutter)


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(YAGController)

"""
Module for the Settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from typing import Any

from qtpy import QtCore, QtWidgets, uic
from qtpy.QtCore import Slot as pyqtSlot


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self, **kwargs) -> None:
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__(**kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/settings.ui", self)
        self.show()

        # Configure settings.
        self.settings = QtCore.QSettings()
        # Convenience list for widgets with value(), SetValue() methods.
        self.sets: tuple[tuple[QtWidgets.QWidget, str, Any, type], ...] = (
            # name of widget, key of setting, defaultValue, type of data
            # (self.widget, "name", 0, int),
            (self.sbInterval, "interval", 1000, int),
            # TODO add your widgets.
        )
        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbRestoreDefaults = self.buttonBox.button(restore)
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)
        # TODO: example of a fileDialog
        # self.pbSavePath.clicked.connect(self.openFileDialog)

    def readValues(self) -> None:
        """Read the stored values and show them on the user interface."""
        for widget, name, value, typ in self.sets:
            widget.setValue(self.settings.value(name, defaultValue=value,
                                                type=typ))
        # TODO: read settings and write them to the field
        # self.Interval.setValue(self.settings.value("interval", 5000, int))
        # self.leSavePath.setText(self.settings.value("savePath", type=str))

    @pyqtSlot()
    def restoreDefaults(self) -> None:
        """Restore the user interface to default values."""
        for widget, name, value, typ in self.sets:
            widget.setValue(value)

    @pyqtSlot()
    def accept(self) -> None:
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for widget, name, value, typ in self.sets:
            self.settings.setValue(name, widget.value())
        # TODO: save the values from the fields into settings
        # self.settings.setValue("savePath", self.leSavePath.text())
        super().accept()  # make the normal accept things

    # TEMPLATE: a possible file dialog
    # def openFileDialog(self) -> None:
    #     """Open a file path dialog."""
    #     path = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
    #     self.leSavePath.setText(path)

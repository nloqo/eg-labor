
from pyleco_extras.gui.log_logger.log_logger import LogLogger, start_app


if __name__ == "__main__":  # pragma: no cover
    start_app(LogLogger)

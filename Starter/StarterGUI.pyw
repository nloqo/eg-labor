"""
GUI to control the starter

created on 23.11.2020 by Benedikt Moneke
"""

import logging

from pyleco_extras.gui.starter_gui.StarterGUI import StarterGUI, start_app

gLog = logging.getLogger()


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(main_window_class=StarterGUI, logger=gLog)

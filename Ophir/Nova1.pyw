"""
Main file of the Nova 1 program.

created on 14.12.2020 by Benedikt Burger
"""

# import 3rd party tools
from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import pyqtSlot
import pyvisa as pv

from devices import ophir

# import files from data directory
import OphirBase


log = OphirBase.log


class Nova1(OphirBase.OphirBase):
    """Define the specific Nova 1 functions."""

    rangesE = {"PE-50-V2": ("10J", "2J", "200mJ", "20mJ", "2mJ", "200µJ")}
    rangesP = {"3A": ("3W", "300mW", "30mW", "3mW", "300µW", "AUTO")}

    def __init__(self, name="Nova1", autoconnect=False, *args, **kwargs):
        """Initialize the main window and its settings."""
        # use initialization of parent class
        super().__init__(name=name, *args, **kwargs)

        self.bbPulseLengths.setCurrentText("N/A")
        self.bbThresholds.setCurrentText("N/A")

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.counter = 0

        # connect actions to slots
        self.actionAutorange.triggered.connect(self.setAutorange)

        self.setSettings()

        if autoconnect:
            self.actionConnect.setChecked(True)
            self.connect(True)

        log.info(f"{name} initialized.")

    # DESTRUCTOR

    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        # accept the close event (reject it, if you want to do something else)
        self.connect(False)
        super().closeEvent(event)

    # SLOTS AND THEIR FUNCTIONS
    # Actions
    def setSettings(self):
        """Configure values."""
        super().setSettings()
        # TODO the next to steps are not yet configured in the settings window!
        # self.plotDatapoints = self.settings.value('plot/datapoints', 100, int)
        settings = QtCore.QSettings()
        self.timer.setInterval(settings.value("Interval", 100, type=int))

    @pyqtSlot(bool)
    def connect(self, pressed):
        """Connect/Disconnect the device."""
        if pressed:
            settings = QtCore.QSettings()
            try:
                self.inst = ophir.Nova(f"ASRL{settings.value('COM', type=int)}", timeout=200)
            except (pv.VisaIOError, Exception) as exc:
                self.actionConnect.setChecked(False)
                messagebox = QtWidgets.QMessageBox()
                messagebox.setWindowTitle("Connection failed")
                messagebox.setText(f"Connection failed with {exc}")
                messagebox.exec()
            else:
                log.info("Connection successful.")
                self.inst.power_timeout = 100
                self.inst.energy_timeout = 110
                self.getModes()
                self.getRanges()
                if self.actionStart.isChecked():
                    self.timer.start()
        else:
            self.timer.stop()
            try:
                del self.inst
            except AttributeError:
                pass  # no device existent

    @pyqtSlot()
    def reset(self):
        """Reset the device."""
        try:
            self.inst.reset()
        except AttributeError:
            pass

    def showInformation(self, target):
        """Show the information about the instrument or head."""
        if self.actionConnect.isChecked():
            if target == "instrument":
                text = str(self.inst.id)
            else:
                text = str(self.inst.head_information)
            super().showInformation(target, text)

    # Panel
    def setValue(self, function, value):
        """Set `value` with `function` while pausing the stream."""
        if self.actionStart.isChecked():
            self.timer.stop()
            function(value)
            self.timer.start()
        else:
            function(value)

    def getModes(self):
        """Get measurement modes."""
        if self.actionConnect.isChecked():
            self.bbModes.clear()
            self.bbModes.addItems(("Power", "Energy"))
            self.unit = self.inst.units
            self.bbModes.setCurrentIndex(0 if self.unit == "W" else 1)
            self.mode = self.bbModes.currentText()
            self.plotData.setLabel("left", self.mode, units=self.unit)

    @pyqtSlot(int)
    def setMode(self, index):
        """Set the measurement mode."""
        text = self.bbModes.itemText(index)
        if self.actionConnect.isChecked():
            self.timer.stop()
            self.inst.mode = ophir.LegacyModes[text.upper()]
            if self.actionStart.isChecked():
                self.timer.start()
            self.unit = "W" if text == "Power" else "J"
            self.plotData.setLabel("left", text, units=self.unit)
            self.mode = text
            self.getRanges()

    def getRanges(self):
        """Get all possible ranges and add Auto if autorange does not exist."""
        if not self.actionConnect.isChecked():
            return
        self.bbRanges.clear()
        try:
            head = self.inst.head_information["name"]
            current: int = self.inst.range  # type: ignore
        except ConnectionError as exc:
            messagebox = QtWidgets.QMessageBox()
            messagebox.setWindowTitle("Get range failed")
            messagebox.setText(f"Get range failed with {exc}")
            messagebox.setDetailedText("Happens if no head connected.")
            messagebox.exec()
            return
        if self.unit == "W":
            ranges = self.rangesP.get(head, ("0", "1", "2", "3", "4", "5"))
        else:
            ranges = self.rangesE.get(head, ("0", "1", "2", "3", "4", "5"))
        self.bbRanges.addItems(ranges)
        if current >= 0:
            self.bbRanges.setCurrentIndex(current)

    @pyqtSlot(int)
    def setRange(self, index):
        """Set the range."""
        if self.bbRanges.currentText() == "AUTO":
            index = -1
            self.actionAutorange.setChecked(True)
        else:
            self.actionAutorange.setChecked(False)
        if self.actionConnect.isChecked():
            self.timer.stop()
            try:
                self.inst.range = index
            except ConnectionError:
                # index is not valid
                self.bbRanges.removeItem(index)
            finally:
                if self.actionStart.isChecked():
                    self.timer.start()

    def setAutorange(self, pressed):
        """Set autorange, if possible."""
        if pressed:
            self.timer.stop()
            try:
                self.inst.range = -1
            except ConnectionRefusedError as exc:
                self.actionAutorange.setChecked(False)
                messagebox = QtWidgets.QMessageBox()
                messagebox.setWindowTitle("Autorange failed")
                messagebox.setText(f"Autorange failed with {exc}")
                messagebox.exec()
            else:
                if self.actionStart.isChecked():
                    self.timer.start()

    def getWavelengths(self):
        """Get wavelength options."""
        # TODO
        return
        if self.actionConnect.isChecked():
            self.bbWavelengths.clear()
            current, wavelengths = 5, 6
            self.bbWavelengths.addItems(wavelengths)
            self.bbWavelengths.setCurrentIndex(current)

    @pyqtSlot(int)
    def setWavelength(self, index):
        """Set a wavelength setting from the list."""
        # TODO
        return
        if self.actionConnect.isChecked():
            self.setValue(self.inst.SetWavelength, index)

    @pyqtSlot()
    def update(self):
        """New data is ready, update arrays."""
        try:
            if self.mode == "Power":
                value: float = self.inst.power  # type: ignore
            else:
                value: float = self.inst.energy  # type: ignore
        except pv.VisaIOError as exc:
            if exc.error_code == pv.errors.VI_ERROR_TMO:
                self.lbValue.setText("Timeout")
                return
            raise
        except ConnectionError as exc:
            if str(exc).startswith("TIMEOUT"):
                self.lbValue.setText("Timeout")
                return
            raise
        if value == float("inf"):
            self.lbValue.setText("Over range")
            return
        elif value == []:
            return  # to skip timeouts.
        # update plotData
        self.timeList.append(self.counter)
        self.dataList.append(value)
        self.counter += 1
        try:
            self.lineReference.setData(self.timeList, self.dataList)
        except Exception as exc:
            log.exception(
                f"Setting data with {self.timeList}, {self.dataList} failed.", exc_info=exc
            )
        if self.actionAutocutPlot.isChecked() and (len(self.timeList) > self.plotDatapoints):
            self.timeList = self.timeList[-self.plotDatapoints:]
            self.dataList = self.dataList[-self.plotDatapoints:]
        # Publish the latest value.
        try:
            self.publisher.send_data({"value": value})
            self.publisher.send_legacy({"Nova1": value})
        except Exception as exc:
            log.exception("Publisher error.", exc)
        # Show the current value with SI unit prefix
        exponent = 0
        while abs(value) < 1 and value != 0:
            value *= 1000
            exponent -= 3
        try:
            prefix = self.SI[exponent]
        except KeyError:
            prefix = f"E{exponent}"
        settings = QtCore.QSettings()
        length = min(len(self.dataList), settings.value("plot/averaging", 10, type=int))
        mean = sum(self.dataList[-length:]) / length * 10 ** (-exponent)
        self.lbValue.setText(f"{value:.3f}{prefix}{self.unit}")
        self.lbMean.setText(f"{mean:.3f}{prefix}{self.unit}")
        # TODO do autorange?
        if self.actionAutorange.isChecked():
            pass  # self.autorange(value[-1], status[-1])

    def autorange(self, value, status):
        """Adjust the range, if necessary for `value` or `status`."""
        # TODO cancel?
        return
        ranges = self.ranges
        index = self.bbRanges.currentIndex()
        if value < self.autorangeInterval[0] * ranges[index + 1]:
            self.bbRanges.setCurrentIndex(index + 1)
            self.setRange(index + 1)
        elif index > 0 and (value > self.autorangeInterval[1] * ranges[index] or status):
            if ranges[index - 1] > 0:
                self.bbRanges.setCurrentIndex(index - 1)
                self.setRange(index - 1)

    @pyqtSlot(bool)
    def startStop(self, pressed):
        """Start or stop a data stream and change the action text."""
        if pressed:
            self.timer.start()
        else:
            self.timer.stop()

    @pyqtSlot()
    def resetPlot(self):
        """Reset and clear the plot."""
        super().resetPlot()
        self.counter = 0


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    OphirBase.start_app(Nova1, parser=OphirBase.parser)

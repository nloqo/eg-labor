"""
Main file of the Nova 2 program.

created on 14.12.2020 by Benedikt Burger
"""

# import standard packages
from typing import Callable, Any

# import 3rd party tools
from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import pyqtSlot
import win32com.client

# import files from data directory
import OphirBase


log = OphirBase.log


class Nova2(OphirBase.OphirBase):
    """Define the specific Nova 2 functions."""

    def __init__(self, autoconnect=False, *args, **kwargs):
        """Initialize the main window and its settings."""
        # use initialization of parent class
        super().__init__(name="Nova2", *args, **kwargs)

        # definition of communication
        try:
            self.Ophir = win32com.client.Dispatch("OphirLMMeasurement.CoLMMeasurement")
        except Exception as exc:
            log.exception("Initialization error, driver missing.", exc)
            messagebox = QtWidgets.QMessageBox()
            messagebox.setWindowTitle("Driver missing")
            messagebox.setText(f"Driver missing with {type(exc).__name__}: {exc}")
            messagebox.exec()
        else:
            self.Events = win32com.client.WithEvents(self.Ophir, OphirEvents)
            self.Events.signals.dataReady.connect(self.update)

        # connect panel to slots
        self.bbPulseLengths.activated.connect(self.setPulseLength)
        self.bbThresholds.activated.connect(self.setThreshold)

        self.setSettings()

        if autoconnect:
            self.actionConnect.setChecked(True)
            self.connect(True)

        log.info("Nova2 initialized.")

    # DESTRUCTOR

    def closeEvent(self, event):
        """Clean up, if the window is closed somehow."""
        self.connect(False)
        super().closeEvent(event)

    # SLOTS AND THEIR FUNCTIONS
    # Actions
    @pyqtSlot(bool)
    def connect(self, pressed: bool):
        """Connect/Disconnect the device."""
        if pressed:
            try:
                device, *_ = self.Ophir.ScanUSB()
            except (ValueError, AttributeError) as exc:
                # "not enough values to unpack" == no device found
                # Ophir initialization failed.
                self.actionConnect.setChecked(False)
                log.exception("Connection failed.", exc)
            else:
                # connect to first found USB device and use channel 0
                self.Nova = self.Ophir.OpenUSBDevice(device), 0
                self.getModes()
                self.getRanges()
                self.getPulseLengths()
                self.getWavelengths()
                self.getThresholds()
                self.setWavelengthLimits()
                if self.actionStart.isChecked():
                    self.startStream()
        else:
            try:
                if self.actionStart.isChecked():
                    self.stopStream()
                self.Ophir.StopAllStreams()
                self.Ophir.CloseAll()
            except AttributeError:
                pass  # No Ophir device connected.

    def showInformation(self, target: str) -> None:
        """Show the information about the instrument or head."""
        if self.actionConnect.isChecked():
            if target == "instrument":
                text = str(self.Ophir.GetDeviceInfo(*self.Nova))
            else:
                text = str(self.Ophir.GetSensorInfo(*self.Nova))
            super().showInformation(target, text)

    # Panel
    def setValue(self, function: Callable[[Any, int, Any], None], value: Any):
        """Set `value` with `function` while pausing the stream."""
        if self.actionStart.isChecked():
            self.stopStream()
            function(*self.Nova, value)
            self.startStream()
        else:
            function(*self.Nova, value)

    def getModes(self):
        """Get measurement modes."""
        if self.actionConnect.isChecked():
            self.bbModes.clear()
            current, modes = self.Ophir.GetMeasurementMode(*self.Nova)
            self.bbModes.addItems(modes)
            self.bbModes.setCurrentIndex(current)
            mode = self.bbModes.currentText()
            self.setUnitSymbol(mode)
            self.plotData.setLabel("left", mode, units=self.unit)

    @pyqtSlot(int)
    def setMode(self, mode_index: int) -> None:
        """Set the measurement mode."""
        if self.actionConnect.isChecked():
            self.setValue(self.Ophir.SetMeasurementMode, mode_index)
            mode = self.bbModes.itemText(mode_index)
            self.setUnitSymbol(mode)
            self.plotData.setLabel("left", mode, units=self.unit)
            self.getRanges()

    def setUnitSymbol(self, mode: str) -> None:
        """Set the measurement unit symbol for the measuring `mode`."""
        units = {"Power": "W", "Energy": "J"}
        self.unit = units.get(mode, "?")

    def getRanges(self):
        """Get all possible ranges and add Auto if autorange does not exist."""
        if self.actionConnect.isChecked():
            self.bbRanges.clear()
            current, ranges = self.Ophir.GetRanges(*self.Nova)
            self.bbRanges.addItems(ranges)
            self.ranges = list(map(self.rangeReader, ranges)) + [-1000]
            self.bbRanges.setCurrentIndex(current)

    def rangeReader(self, rangeName: str):
        """Calculate the range limit from the range name."""
        if rangeName == "AUTO":
            return 0
        try:
            number = float(
                rangeName[:-1].replace("m", "E-3").replace("u", "E-6").replace("n", "E-9")
            )
        except Exception as exc:
            log.exception(f"Exception {exc} for {rangeName}.", exc_info=exc)
            number = 0
        return number

    @pyqtSlot(int)
    def setRange(self, range_index: int) -> None:
        """Set the range."""
        if self.actionConnect.isChecked():
            self.setValue(self.Ophir.SetRange, range_index)

    def getPulseLengths(self):
        """Get the pulse lengths for pyroelectric sensors."""
        if self.actionConnect.isChecked():
            self.bbPulseLengths.clear()
            current, values = self.Ophir.GetPulseLengths(*self.Nova)
            self.bbPulseLengths.addItems(values)
            self.bbPulseLengths.setCurrentIndex(current)

    @pyqtSlot(int)
    def setPulseLength(self, pulse_index: int) -> None:
        """Set the pulse length."""
        if self.actionConnect.isChecked():
            self.setValue(self.Ophir.SetPulseLength, pulse_index)

    def getWavelengths(self):
        """Get wavelength options."""
        if self.actionConnect.isChecked():
            self.bbWavelengths.clear()
            current, wavelengths = self.Ophir.GetWavelengths(*self.Nova)
            self.bbWavelengths.addItems(wavelengths)
            self.bbWavelengths.setCurrentIndex(current)

    @pyqtSlot(int)
    def setWavelength(self, wavelength_index: int) -> None:
        """Set a wavelength setting from the list."""
        if self.actionConnect.isChecked():
            self.setValue(self.Ophir.SetWavelength, wavelength_index)

    def getThresholds(self) -> None:
        """Get the pulse lengths for pyroelectric sensors."""
        if self.actionConnect.isChecked():
            self.bbThresholds.clear()
            current, values = self.Ophir.GetThreshold(*self.Nova)
            self.bbThresholds.addItems(values)
            self.bbThresholds.setCurrentIndex(current)

    @pyqtSlot()
    def setChangeWavelength(self) -> None:
        """Change the wavelength of the current wavelength index."""
        index = self.bbWavelengths.currentIndex()
        wavelength = self.sbChangeWl.value()
        self.stopStream()
        self.Ophir.ModifyWavelength(*self.Nova, index, wavelength)
        self.getWavelengths()
        self.startStream()

    @pyqtSlot()
    def saveChangeWavelength(self):
        """Save the changes of the wavelength."""
        self.stopStream()
        self.Ophir.SaveSettings(*self.Nova)
        self.startStream()

    def setWavelengthLimits(self) -> None:
        """Get information about the wavelength limits available for the sensor."""
        modifiable, minWavelength, maxWavelength = self.Ophir.GetWavelengthsExtra(*self.Nova)
        if modifiable:
            self.sbChangeWl.setMaximum(maxWavelength)
            self.sbChangeWl.setMinimum(minWavelength)
            self.pbSaveWlSet.setEnabled(True)
            self.pbSaveWlSet.setEnabled(True)
        else:
            self.pbSaveWlSet.setEnabled(False)
            self.pbSaveWlSet.setEnabled(False)



    @pyqtSlot(int)
    def setThreshold(self, threshold_index: int) -> None:
        """Set the pulse length."""
        if self.actionConnect.isChecked():
            self.setValue(self.Ophir.SetThreshold, threshold_index)

    # Streams
    def startStream(self, mode="standard") -> None:
        """Configure the stream and start it."""
        # TODO configure stream for normal, turbo, immediate
        try:
            self.Ophir.StartStream(*self.Nova)
        except AttributeError:
            self.actionStart.setChecked(False)
        except win32com.client.pythoncom.com_error as exc:  # type: ignore
            self.actionStart.setChecked(False)
            super().showInformation("Stream", f"Com_error at starting a stream: {exc}")
        except Exception as exc:
            log.exception("Starting stream failed.", exc_info=exc)

    @pyqtSlot()
    def update(self):
        """New data is ready, update arrays."""
        # Value(s), timestamp(s), whether overrange
        value, time, status = self.Ophir.GetData(*self.Nova)
        try:
            showvalue = value[-1]
            showstatus = status[-1]
        except IndexError:
            return  # not enough data
        # update plotData
        self.timeList += time
        self.dataList += value
        self.lineReference.setData(self.timeList, self.dataList)
        if self.actionAutocutPlot.isChecked():
            self.timeList = self.timeList[-self.plotDatapoints:]
            self.dataList = self.dataList[-self.plotDatapoints:]
        # Show the current value with SI unit prefix
        if showstatus:
            self.lbValue.setText("Over")
        else:
            settings = QtCore.QSettings()
            exponent = 0
            while abs(showvalue) < 1 and showvalue != 0:
                showvalue *= 1000
                exponent -= 3
            try:
                prefix = self.SI[exponent]
            except KeyError:
                prefix = f"E{exponent}"
            length = min(len(self.dataList), settings.value("plot/averaging", 10, type=int))
            mean = sum(self.dataList[-length:]) / length * 10 ** (-exponent)
            self.lbValue.setText(f"{showvalue:.3f}{prefix}{self.unit}")
            self.lbMean.setText(f"{mean:.3f}{prefix}{self.unit}")
        # Publish the latest value and do automatic range adjustments.
        try:
            self.publisher.send_data({"value": value[-1]})
            self.publisher.send_legacy({"Nova2": value[-1]})
        except Exception as exc:
            log.exception("Publisher error.", exc)
        if self.actionAutorange.isChecked():
            self.autorange(value[-1], status[-1])

    def autorange(self, value: float, is_overrange: bool) -> None:
        """Adjust the range, if necessary for `value` or indicated by `is_overrange`."""
        ranges = self.ranges
        index = self.bbRanges.currentIndex()
        if value < self.autorangeInterval[0] * ranges[index + 1]:
            self.bbRanges.setCurrentIndex(index + 1)
            self.setRange(index + 1)
        elif (value > self.autorangeInterval[1] * ranges[index] or is_overrange) and index > 0:
            if ranges[index - 1] > 0:
                self.bbRanges.setCurrentIndex(index - 1)
                self.setRange(index - 1)

    def stopStream(self):
        """Stop the data stream."""
        self.Ophir.StopStream(*self.Nova)

    @pyqtSlot(bool)
    def startStop(self, checked: bool) -> None:
        """Start or stop a data stream according `pressed`."""
        if checked:
            self.startStream()
        else:
            self.stopStream()


class OphirEvents:
    """Define the event handling for the Ophir devices."""

    def __init__(self):
        """Initialize the signal."""
        super().__init__()
        self.signals = self.Signals()

    class Signals(QtCore.QObject):
        """Define the Qt Signal."""

        dataReady = QtCore.pyqtSignal()

    def OnDataReady(self, *args):
        """DataReady event was fired, emit the QT signal."""
        self.signals.dataReady.emit()


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    OphirBase.start_app(Nova2, parser=OphirBase.parser)

"""
Module for the settings dialog class.
Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from qtpy import QtCore, QtWidgets

from pyleco_extras.gui_utils.base_settings import BaseSettings


class Settings(BaseSettings):

    def setup_form(self, layout: QtWidgets.QFormLayout) -> None:
        sbPlotDataPoints = QtWidgets.QSpinBox()
        sbPlotDataPoints.setToolTip("Number of datapoints to show in the plot")
        sbPlotDataPoints.setMaximum(10000)
        self.add_value_widget(
            labelText="Number data points",
            widget=sbPlotDataPoints,
            key="plot/datapoints",
            defaultValue=400,
            type=int,
        )

        averageDataPoints = QtWidgets.QSpinBox()
        averageDataPoints.setToolTip("Number of datapoints over which to average.")
        averageDataPoints.setMaximum(10000)
        self.add_value_widget(
            labelText="Averaging data points",
            widget=averageDataPoints,
            key="plot/averaging",
            defaultValue=20,
            type=int,
        )

        lower_limit = QtWidgets.QSpinBox()
        lower_limit.setToolTip("Percentage of next lower range at which that range is selected.")
        lower_limit.setSuffix("%")
        self.add_value_widget(
            labelText="Lower limit",
            widget=lower_limit,
            key="autorange/lowerLimit",
            defaultValue=65,
            type=int,
        )

        upper_limit = QtWidgets.QSpinBox()
        upper_limit.setToolTip("Percentage of current range at which that range is selected.")
        upper_limit.setSuffix("%")
        self.add_value_widget(
            labelText="Upper limit",
            widget=upper_limit,
            key="autorange/upperLimit",
            defaultValue=85,
            type=int,
        )

        application = QtCore.QCoreApplication.instance()
        if application.applicationName() == "Nova1":
            self.setup_nova_form()

    def setup_nova_form(self) -> None:
        """Setup the form for the Nova (not NovaII)."""
        com_port = QtWidgets.QSpinBox()
        com_port.setPrefix("COM")
        self.add_value_widget(
            labelText="COM port",
            widget=com_port,
            key="COM",
            defaultValue=0,
            type=int,
        )

        interval = QtWidgets.QSpinBox()
        interval.setSuffix(" ms")
        self.add_value_widget(
            labelText="Readout interval",
            widget=interval,
            key="Interval",
            defaultValue=100,
            type=int,
        )

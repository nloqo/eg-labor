"""
Base file for the Ophir powermeter readout.
Should be subclassed for different communication protocols.

created on 23.11.2020 by Benedikt Burger
"""

import logging

from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot
import pyqtgraph as pg

from pyleco.utils.parser import parser

from pyleco_extras.gui_utils.base_main_window import start_app, LECOBaseMainWindowDesigner, Path

from data.settings import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class OphirBase(LECOBaseMainWindowDesigner):
    """Define the main window and essential methods of the program.

    :param name: Name to publish under.
    """

    SI = {0: "", -3: "m", -6: "µ", -9: "n", -12: "p"}

    actionSettings: QtGui.QAction
    actionClose: QtGui.QAction
    actionConnect: QtGui.QAction
    actionReconnect: QtGui.QAction
    actionReset: QtGui.QAction
    actionInformation: QtGui.QAction
    actionHead: QtGui.QAction
    actionStart: QtGui.QAction
    actionAutorange: QtGui.QAction
    actionResetPlot: QtGui.QAction
    actionAutocutPlot: QtGui.QAction

    bbModes: QtWidgets.QComboBox
    bbPulseLengths: QtWidgets.QComboBox
    bbRanges: QtWidgets.QComboBox
    bbThresholds: QtWidgets.QComboBox
    bbWavelengths: QtWidgets.QComboBox
    lbMean: QtWidgets.QLabel
    lbRange: QtWidgets.QLabel
    lbValue: QtWidgets.QLabel
    plotData: pg.PlotWidget
    sbChangeWl: QtWidgets.QSpinBox
    pbSaveWlSet: QtWidgets.QPushButton
    pbChangeWlSave: QtWidgets.QPushButton

    def __init__(self, name="OphirBase", **kwargs):
        """Initialize the main window and its settings."""
        # use initialization of parent class
        super().__init__(
            name=name,
            ui_file_name="OphirBase",
            ui_file_path=Path(__file__).parent / "data",
            settings_dialog_class=Settings,
            **kwargs,
        )

        self.lbValue.setToolTip(f"Last value, published as {name}.")

        # connect actions to slots
        self.actionConnect.triggered.connect(self.connect)
        self.actionReconnect.triggered.connect(self.reconnect)
        self.actionReset.triggered.connect(self.reset)
        self.actionInformation.triggered.connect(self.actionInformationPressed)
        self.actionHead.triggered.connect(self.actionHeadPressed)
        self.actionResetPlot.triggered.connect(self.resetPlot)

        self.actionStart.triggered.connect(self.startStop)

        # connect panel to slots
        self.bbModes.activated.connect(self.setMode)
        self.bbRanges.activated.connect(self.setRange)
        self.bbWavelengths.activated.connect(self.setWavelength)
        self.pbSaveWlSet.clicked.connect(self.setChangeWavelength)
        self.pbChangeWlSave.clicked.connect(self.saveChangeWavelength)

        # connect panel to slots

        self.setupPlot()

    # SETUP FUNCTIONS

    def setupPlot(self) -> None:
        """Configure the plots."""
        canvas = self.plotData
        canvas.showGrid(x=True, y=True)
        self.timeList = []
        self.dataList = []
        self.lineReference = canvas.plot(self.timeList, self.dataList, name="Power")
        # set axis description
        canvas.setLabel("left", "data", units="?")
        canvas.setLabel("bottom", "time", units="samples")

    # DESTRUCTOR

    def closeEvent(self, event) -> None:
        """Clean up on window closure and accept the close event."""
        log.info("Closing.")
        event.accept()

    # SLOTS AND THEIR FUNCTIONS
    # Actions

    def setSettings(self) -> None:
        """Configure values."""
        settings = QtCore.QSettings()
        settings.beginGroup("autorange")
        self.autorangeInterval = [
            settings.value("lowerLimit", 0, type=int) / 100,
            settings.value("upperLimit", 100, type=int) / 100,
        ]
        settings.endGroup()
        self.plotDatapoints = settings.value("plot/datapoints", defaultValue=100, type=int)

    def connect(self, checked: bool) -> None:
        """Connect/Disconnect the device."""
        raise NotImplementedError

    def startStop(self, checked: bool) -> None:
        raise NotImplementedError

    def setRange(self, range_index: int) -> None:
        raise NotImplementedError

    def setWavelength(self, wavelength_index: int) -> None:
        raise NotImplementedError

    def setChangeWavelength(self) -> None:
        raise NotImplementedError

    def saveChangeWavelength(self) -> None:
        raise NotImplementedError

    def setMode(self, mode_index: int) -> None:
        raise NotImplementedError

    @pyqtSlot()
    def reconnect(self):
        """Reconnect the device, for example after head change."""
        self.connect(False)
        self.connect(True)

    @pyqtSlot()
    def reset(self) -> None:
        """Reset the device."""
        raise NotImplementedError

    def showInformation(self, target: str, text: str = "None.") -> None:
        """Show the information about the instrument or head."""
        confirmation = QtWidgets.QMessageBox()
        confirmation.setWindowTitle(f"Information about {target}.")
        confirmation.setIcon(QtWidgets.QMessageBox.Icon.Information)
        confirmation.setText(text)
        confirmation.setStandardButtons(QtWidgets.QMessageBox.StandardButton.Ok)
        confirmation.exec()

    @pyqtSlot(bool)
    def actionInformationPressed(self) -> None:
        """Show information about the instrument."""
        self.showInformation("instrument")

    @pyqtSlot(bool)
    def actionHeadPressed(self) -> None:
        """Show information about the sensor head."""
        self.showInformation("head")

    @pyqtSlot()
    def resetPlot(self) -> None:
        """Reset the arrays and clear the plot."""
        self.timeList = []
        self.dataList = []
        self.lineReference.setData(self.timeList, self.dataList)


parser.add_argument(
    "-a", "--autoconnect", action="store_true", help="connect automatically to the device."
)


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(OphirBase)

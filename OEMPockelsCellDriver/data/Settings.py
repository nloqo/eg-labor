"""
Module for the Settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self):
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/Settings.ui", self)
        self.show()

        # Configure settings.
        self.settings = QtCore.QSettings()
        # Convenience list for widgets with value(), SetValue() methods.
        self.sets = (
            # name of widget, key of setting, defaultValue, type of data
            # (self.widget, 'name', 0, int),
            (self.sbCOM, 'COM', 1, int),
            (self.sbGateLimit, 'gateLimit', 1000, int),
            (self.sbTTT, 'transistorTemperatureLimit', 80.0, float),
            (self.sbCTT, 'caseTemperatureLimit', 80.0, float),
            (self.sbInterval, 'interval', 50, int)
        )
        self.rbSets = (
            (self.rbFixed, 'pulseWidthMode', True, bool),
            (self.rbSoft, 'triggeringMode', False, bool),
            (self.rbSmart, 'HVProgramMode', False, bool)
        )
        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbRestoreDefaults = self.buttonBox.button(restore)
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for widget, name, value, typ in self.sets:
            widget.setValue(self.settings.value(name, defaultValue=value,
                                                type=typ))
        for widget, name, value, typ in self.rbSets:
            if not self.settings.value(name, defaultValue=value, type=typ):
                if widget == self.rbFixed:
                    self.rbVariable.setChecked(True)
                elif widget == self.rbSoft:
                    self.rbHarsh.setChecked(True)
                elif widget == self.rbSmart:
                    self.rbDefault.setChecked(True)
            else:
                widget.setChecked(True)

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for widget, name, value, typ in self.sets:
            widget.setValue(value)
        for widget, name, value, typ in self.rbSets:
            widget.setChecked(value)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for widget, name, value, typ in self.sets:
            self.settings.setValue(name, widget.value())
        for widget, name, value, typ in self.rbSets:
            print(name, widget.isChecked())
            self.settings.setValue(name, widget.isChecked())

        # TODO: save the values from the fields into settings
        # self.settings.setValue('savePath', self.leSavePath.text())
        super().accept()  # make the normal accept things

    '''
    TEMPLATE: a possible file dialog
    def openFileDialog(self):
        """Open a file path dialog."""
        path = QtWidgets.QFileDialog.getExistingDirectory(self, "Save path")
        self.leSavePath.setText(path)
    '''

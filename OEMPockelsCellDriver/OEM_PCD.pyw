"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging
import sys
import time

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot

from devices import OEMPockelsCellDriver

# Local packages.
from data import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


# TODO alle Vorkommnisse von BaseProgram durch neuen "Programm-Name" ersetzen.
class OEM_PCD(QtWidgets.QMainWindow):
    """Define the main window and essential methods of the program."""

    def __init__(self, *args, **kwargs):
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)

        # Load the user interface file and show it.
        uic.loadUi("data/OEM_PCD.ui", self)
        self.show()

        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readout)

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName("OEM_PCD")

        # Apply settings

        # self.setSettings()

        # Connect actions to slots.
        self.actionClose.triggered.connect(self.close)
        self.actionSettings.triggered.connect(self.openSettings)
        self.actionConnect.triggered.connect(self.connect)

        self.pbHV.clicked.connect(self.HV)
        self.pbSetVoltage.clicked.connect(self.setVoltage)
        log.info("Pockels cell driver initialized.")

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.driver.HVenable(0)
        self.readoutTimer.stop()
        del self.driver
        event.accept()

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        settingsDialog = Settings.Settings()
        if settingsDialog.exec():
            self.setSettings()

    def setSettings(self):
        """Apply new settings."""
        # TODO apply changes to variables.
        settings = QtCore.QSettings()
        self.readoutTimer.stop()
        try:
            HVStatus = self.driver.HVenable()
            if HVStatus == 1:
                self.driver.HVenable(0)
                time.sleep(.5)
            self.driver.gateLimit(settings.value("gateLimit"))
            time.sleep(.1)
            self.driver.pulseWidthMode(int(settings.value("pulseWidthMode", type=bool)))
            time.sleep(.1)
            self.driver.triggeringMode(int(settings.value("triggeringMode", type=bool)))
            time.sleep(.1)
            self.driver.HVprogramMode(int(settings.value("HVProgramMode", type=bool)))
            time.sleep(.1)
            self.driver.transistorsTemperatureThreshold(
                int(settings.value("transistorTemperatureLimit", type=float) * 10))
            time.sleep(.1)
            self.driver.caseTemperatureThreshold(
                int(settings.value("caseTemperatureLimit", type=float) * 10))
        except Exception as exc:
            print(exc)
        self.readoutTimer.start(settings.value("Interval"))

    @pyqtSlot(bool)
    def connect(self, checked):
        settings = QtCore.QSettings()
        if checked:
            settings = QtCore.QSettings()
            com = settings.value('COM', type=int)
            try:
                self.driver = OEMPockelsCellDriver.Driver(f"COM{com}")
                self.readoutTimer.start(settings.value("Interval"))
            except Exception:
                log.exception("Connection to driver failed.")
                self.actionConnect.setChecked(False)
        else:
            self.readoutTimer.stop()
            try:
                del self.driver
            except AttributeError:
                log.exception("Could not close connection.")

    @pyqtSlot()
    def HV(self):
        try:
            if self.pbHV.isChecked():
                self.driver.HVenable(1)
            else:
                self.driver.HVenable(0)
        except Exception:
            self.pbHV.setChecked(False)

    @pyqtSlot()
    def setVoltage(self):
        self.driver.highVoltage(self.sbVoltage.value())

    @pyqtSlot()
    def readout(self):
        # read values of device
        settings = QtCore.QSettings()
        try:
            self.parameters = self.driver.getAllMonitors()
            self.lbTransistorTemperature.setText(f"{self.parameters['transistorTemperature']}")
            if self.parameters["transistorTemperature"] >= float(settings.value("transistorTemperatureLimit")):  # noqa: E501
                self.lbTransistorTemperature.setStyleSheet('color: red')
            else:
                self.lbTransistorTemperature.setStyleSheet('color: black')
            self.lbCaseTemperature.setText(f"{self.parameters['caseTemperature']}")
            if self.parameters["caseTemperature"] >= float(settings.value("caseTemperatureLimit")):
                self.lbcaseTemperature.setStyleSheet('color: red')
            else:
                self.lbTransistorTemperature.setStyleSheet('color: black')
            if self.parameters["overtemperatureError"] == 1:
                self.lbOvertemperatureError.setText("Yes")
                self.lbOvertemperatureError.setStyleSheet("color: red")
            else:
                self.lbOvertemperatureError.setText("No")
                self.lbOvertemperatureError.setStyleSheet("color: black")
            if self.parameters["gateLimitError"] == 1:
                self.lbGateLimitError.setText("Yes")
                self.lbGateLimitError.setStyleSheet("color: red")
            else:
                self.lbGateLimitError.setText("No")
                self.lbGateLimitError.setStyleSheet("color: black")
            if self.parameters["HVfaultError"] == 1:
                self.lbHVFaultError.setText("Yes")
                self.lbHVFaultError.setStyleSheet("color: red")
            else:
                self.lbHVFaultError.setText("No")
                self.lbHVFaultError.setStyleSheet("color: black")
            self.lbVoltage.setText(f"{self.driver.highVoltage()} V")
            pwm = self.driver.pulseWidthMode()

            if pwm == 0:
                self.lbPulseWidthMode.setText("variable")
            elif pwm == 1:
                self.lbPulseWidthMode.setText("fixed")
            else:
                self.lbPulseWidthMode.setText("Not defined!")
            tm = self.driver.triggeringMode()
            if tm == 0:
                self.lbTriggeringMode.setText("harsh")
            elif tm == 1:
                self.lbTriggeringMode.setText("soft")
            else:
                self.lbTriggeringMode.setText("Not defined!")
            hvm = self.driver.HVprogramMode()
            if hvm == 0:
                self.lbHVProgramMode.setText("default")
            elif hvm == 1:
                self.lbHVProgramMode.setText("smart")
            else:
                self.lbHVProgramMode.setText("Not defined!")
            hve = self.driver.HVenable()
            if hve == 1:
                self.lbOn.setStyleSheet("color: green")
                self.lbOff.setStyleSheet("color: black")
            else:
                self.lbOn.setStyleSheet("color: black")
                self.lbOff.setStyleSheet("color: red")
        except Exception as exc:
            log.error(f"OEM Driver readout failed:{exc}\n Try to reconnect.")
            try:
                self.readoutTimer.stop()
                self.connect(False)
                self.connect(True)
            except Exception:
                log.error("Reconnecting failed. Restart manually.")
                self.readoutTimer.stop()
                self.connect(False)


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:  # Verbose log.
        log.setLevel(logging.DEBUG)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = OEM_PCD()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

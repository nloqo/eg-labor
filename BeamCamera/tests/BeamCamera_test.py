# -*- coding: utf-8 -*-
"""
Tests for the Beamcamera


Created on Mon Feb 14 12:38:31 2022 by Benedikt Moneke
"""

import pytest

import numpy as np
from devices import testClasses as t

from BeamCamera import BeamCamera as b


class QSettings(t.QSettings):
    def __init__(self, settings):
        # global settings
        self.settings = settings


class Test_update:
    class Test_orientation:
        @pytest.fixture
        def bc(self):
            bc = t.Empty()
            bc.bbOrientation = t.QWidget()
            bc.bbOrientation.currentIndex = bc.bbOrientation.value
            bc.pgImage = t.QWidget()
            bc.pgImage.setImage = t.empty
            bc.autoFunction = t.empty
            bc.actionEvaluateData = t.QButton()
            return bc

        @pytest.fixture(scope="class")
        def data(self):
            return np.array([[1, 2], [3, 4]])

        @pytest.fixture
        def bc0(self, bc):
            bc.settings = QSettings({'viewDirection': 0})  # against beam
            return bc

        def test_left_against(self, bc0, data):
            bc0.bbOrientation.setValue(0)  # left
            b.update(bc0, data)
            assert np.array_equal(bc0.image, data)

        def test_top(self, bc0, data):
            bc0.bbOrientation.setValue(1)  # top
            b.update(bc0, data)
            assert np.array_equal(bc0.image, [[3, 1], [4, 2]])

        def test_right(self, bc0, data):
            bc0.bbOrientation.setValue(2)  # top
            b.update(bc0, data)
            assert np.array_equal(bc0.image, [[4, 3], [2, 1]])

        def test_bottom(self, bc0, data):
            bc0.bbOrientation.setValue(3)  # top
            b.update(bc0, data)
            assert np.array_equal(bc0.image, [[2, 4], [1, 3]])

        @pytest.fixture
        def bc1(self, bc):
            bc.settings = QSettings({'viewDirection': 1})  # along beam
            return bc

        def test_left_along(self, bc1, data):
            bc1.bbOrientation.setValue(0)  # left
            b.update(bc1, data)
            assert np.array_equal(bc1.image, [[2, 1], [4, 3]])

        def test_top1(self, bc1, data):
            bc1.bbOrientation.setValue(1)  # top
            b.update(bc1, data)
            assert np.array_equal(bc1.image, [[1, 3], [2, 4]])

        def test_right1(self, bc1, data):
            bc1.bbOrientation.setValue(2)  # top
            b.update(bc1, data)
            assert np.array_equal(bc1.image, [[3, 4], [1, 2]])

        def test_bottom1(self, bc1, data):
            bc1.bbOrientation.setValue(3)  # top
            b.update(bc1, data)
            assert np.array_equal(bc1.image, [[4, 2], [3, 1]])

"""
Module for the settings dialog class.

Created on 06.02.2021 by Benedikt Moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self):
        """Initialize the dialog with the programName as argument of the settings."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/Settings.ui", self)
        self.show()

        # Configure settings.
        self.settings = QtCore.QSettings()
        # Convenience list for widgets with value(), SetValue() methods.
        self.sets = (
            # General
            (self.sbFrameRateMax, 'frameRateMax', 10, int),
            # (self.leSavePath, 'savePath', "", str),
            # (sefl.bbViewDirection, 'viewDirection', 0, int),
            (self.sbExposureDefault, 'exposureDefault', 0, int),
            # Automatic adjustment
            (self.sbPixelMaximum, 'autoFunction/pixelMaximum', 4000, int),
            (self.sbPixelMinimum, 'autoFunction/pixelMinimum', 2000, int),
            (self.sbExposureMaximum, 'autoFunction/exposureMaximum', 1000, int),
            (self.sbGainMaximum, 'autoFunction/gainMaximum', 15, float),
            (self.sbSoftwareWaitingTime, 'softwareWaitingTime', 40, int)
        )

        self.readValues()

        # Do automatic limiting
        self.sbPixelMaximum.valueChanged.connect(self.setPixelMinimumLimit)
        self.sbPixelMinimum.valueChanged.connect(self.setPixelMaximumLimit)

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        self.pbRestoreDefaults = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults)
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)
        self.pbSavePath.clicked.connect(self.openFileDialog)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        self.leSavePath.setText(self.settings.value('savePath', defaultValue="", type=str))
        self.bbViewDirection.setCurrentIndex(self.settings.value('viewDirection', type=int))
        self.bbTriggerSource.setCurrentIndex(self.settings.value('triggerSource', type=int))
        self.bbTriggerActivation.setCurrentIndex(self.settings.value('triggerActivation', type=int))
        self.bbExposureMode.setCurrentIndex(self.settings.value('exposureMode', type=int))
        self.leSoftwareTriggerName.setText(self.settings.value('softwareTriggerName', "Picoscope"))

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        self.settings.setValue('savePath', self.leSavePath.text())
        self.settings.setValue('viewDirection', self.bbViewDirection.currentIndex())
        self.settings.setValue('triggerSource', self.bbTriggerSource.currentIndex())
        self.settings.setValue('triggerActivation', self.bbTriggerActivation.currentIndex())
        self.settings.setValue('exposureMode', self.bbExposureMode.currentIndex())
        self.settings.setValue('softwareTriggerName', self.leSoftwareTriggerName.text())
        super().accept()  # make the normal accept things

    def openFileDialog(self):
        """Open a file path dialog."""
        # TODO testing: In another program I had stored self.savePath
        savePath = QtWidgets.QFileDialog.getExistingDirectory(self, "Saves file path")
        self.leSavePath.setText(savePath)

    # limiting function
    @pyqtSlot(int)
    def setPixelMinimumLimit(self, value):
        """Restrict the PixelMinimum value to be below the PixelMaximum."""
        self.sbPixelMinimum.setMaximum(value)

    @pyqtSlot(int)
    def setPixelMaximumLimit(self, value):
        """Restrict the PixelMaximum value to be above the PixelMinimum."""
        self.sbPixelMaximum.setMinimum(value)

"""
Module for the settings dialog class.

Created on 06.02.2021 by Benedikt Moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot


class MSettings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    def __init__(self):
        """Initialize the dialog of the settings."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/MSettings.ui", self)
        self.show()

        # Configure settings.
        self.settings = QtCore.QSettings()
        # Convenience list for widgets with value(), SetValue() methods.
        self.sets = (
            (self.sbCOM, "COM", 0, int),
            (self.sbStart, "startPosition", 0, float),
            (self.sbStop, "stopPosition", 0, float),
            (self.sbSteps, "MSteps", 0, int),
        )

        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        self.pbRestoreDefaults = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        )
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)
        # self.pbSavePath.clicked.connect(self.openFileDialog)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        # self.leSavePath.setText(self.settings.value('savePath', defaultValue="", type=str))

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        # self.settings.setValue('savePath', self.leSavePath.text())
        super().accept()  # make the normal accept things

    def openFileDialog(self):
        """Open a file path dialog."""
        # TODO testing: In another program I had stored self.savePath
        # savePath = QtWidgets.QFileDialog.getExistingDirectory(self, "Saves file path")
        # self.leSavePath.setText(savePath)

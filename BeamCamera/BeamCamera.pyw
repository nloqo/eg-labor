"""
Main file of the beam camera.

Requires Python >=3.10

created on 06.02.2021 by Benedikt Burger
"""

# Standard packages.
import datetime
import logging
import multiprocessing
import time  # for measuring impact
from typing import Any, Callable, Optional

# Third party.
import numpy as np
from pypylon import pylon
from pypylon import genicam
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot
import pyqtgraph as pg
import scipy.ndimage
from lmfit import Model
# from scipy.optimize import curve_fit as fit

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
)
from devices.basler import ImageEventHandler, ConfigurationHandler, pixel_lengths
from analysis.beam_camera import save_image

# Local packages.
from data.Settings import Settings

log = logging.Logger(__name__)
log.addHandler(logging.StreamHandler())


# Parameters
gradient_map = "bipolar"  # "spectrum"


class Curves:
    """A container for all the plot curve items."""

    def __init__(
        self,
        image_plot: pg.PlotItem,
        horizontal_plot: pg.PlotItem,
        vertical_plot: pg.PlotItem,
    ) -> None:
        # create the crosshair
        self.width_marker = image_plot.plot([], [], pen="black")
        self.height_marker = image_plot.plot([], [], pen="black")

        # Get line references with initial plots
        self.horizontal = horizontal_plot.plot([], name="horizontal")
        self.horizontal_fit = horizontal_plot.plot([], name="fit", pen="red")
        self.vertical = vertical_plot.plot([], name="vertical")
        self.vertical_fit = vertical_plot.plot([], name="fit", pen="red")


class BeamCamera(LECOBaseMainWindowDesigner):
    """Read a Basler camera and show its image."""

    actionScanForDevices: QtGui.QAction
    actionShowDeviceInfo: QtGui.QAction
    actionEvaluateData: QtGui.QAction
    actionPause: QtGui.QAction
    actionOff: QtGui.QAction
    actionAutoGain: QtGui.QAction
    actionAutoExposure: QtGui.QAction
    actionSmart: QtGui.QAction
    actionTrigger: QtGui.QAction
    actionHideHistogram: QtGui.QAction
    actionHideData: QtGui.QAction
    actionHideConfiguration: QtGui.QAction
    actionHideSettings: QtGui.QAction
    actionHideImage: QtGui.QAction
    actionHideAOI: QtGui.QAction
    actionPublish: QtGui.QAction

    pgImage: pg.ImageView
    pgHorizontal: pg.PlotItem
    pgVertical: pg.PlotItem

    bbOrientation: QtWidgets.QComboBox
    bbDevice: QtWidgets.QComboBox
    sbExposure: QtWidgets.QSpinBox
    slExposure: QtWidgets.QSlider
    sbGain: QtWidgets.QDoubleSpinBox
    slGain: QtWidgets.QSlider
    pbPause: QtWidgets.QPushButton
    pbSave: QtWidgets.QPushButton
    leSavedName: QtWidgets.QLineEdit
    lbCenter: QtWidgets.QLabel
    lbBeamSize: QtWidgets.QLabel
    lbCalculationTime: QtWidgets.QLabel

    sbLo_v: QtWidgets.QSpinBox
    sbLo_h: QtWidgets.QSpinBox
    sbHi_v: QtWidgets.QSpinBox
    sbHi_h: QtWidgets.QSpinBox
    pbSetAOI: QtWidgets.QPushButton
    pbResetAOI: QtWidgets.QPushButton

    pixel_length: float = 1
    unit: str = "pixel"

    def __init__(self, name: str = "BeamCamera", **kwargs) -> None:
        super().__init__(
            name=name,
            ui_file_name="BeamCamera",
            settings_dialog_class=Settings,
            **kwargs,
        )

        self.signals = self.BeamCameraSignals()
        self.signals.calculationReady.connect(self.showCalculation)
        self.softwareTimer = QtCore.QTimer()
        self.softwareTimer.timeout.connect(self.softwareTrigger)
        self.softwareTimer.setSingleShot(True)

        # Get settings.
        self.settings = QtCore.QSettings()
        self.listener.signals.dataReady.connect(self.trigger_message_arrived)

        # Camera
        self.actionScanForDevices.triggered.connect(self.scanForDevices)
        self.actionShowDeviceInfo.triggered.connect(self.showDeviceInfo)
        self.actionEvaluateData.toggled.connect(self.hideCrosshair)
        self.actionPause.triggered.connect(self.pause)
        self.pbResetAOI.clicked.connect(self.reset_aoi)
        self.pbSetAOI.clicked.connect(self.set_aoi)
        # Auto adjustment submenu
        self.actionOff.triggered.connect(self.setAutoFunctionOff)
        self.actionAutoGain.triggered.connect(self.setAutoFunctionGain)
        self.actionAutoExposure.triggered.connect(self.setAutoFunctionExposure)
        self.actionSmart.triggered.connect(self.setAutoFunctionSmart)
        self.actionTrigger.triggered.connect(self.setTrigger)
        # UI configuration
        self.actionHideHistogram.toggled.connect(self.hide_histogram)

        # Connect buttons to slots.
        #   config
        self.sbExposure.valueChanged.connect(self.setExposure)
        self.bbDevice.activated.connect(self.selectDevice)
        self.sbGain.valueChanged.connect(self.setGain)
        self.slGain.valueChanged.connect(self.slGainValueChanged)
        self.bbOrientation.currentIndexChanged.connect(self.setOrientation)
        self.bbOrientation.activated.connect(self.storeOrientation)
        #   user stuff
        self.pbSave.clicked.connect(self.saveImage)

        # generate Process pool
        self.pool = multiprocessing.Pool(processes=2)

        # Define Variables
        self.autoFunction = self.autoFunctionOff  # or read it from settings?
        self.autoFunctionSettings = {}

        # Additional setup
        self.setupPlots()
        self.setupCamera()
        self.setup_camera_action_list()
        self.scanForDevices()
        self.getAutoFunctionSettings()
        self.restoreConfiguration()
        log.info("BeamCamera initialized.")

    class BeamCameraSignals(QtCore.QObject):
        """Signals for the main window of the BeamCamera"""

        calculationReady = QtCore.pyqtSignal(dict)

    # Setup functions
    def setupPlots(self) -> None:
        """Configure the image and plots."""
        # Initialize dictionary with lists
        self.plot_data: dict[str, Any] = {"dataX": [], "dataY": []}
        # Configure the image
        self.pgImage.getImageItem().setOpts(axisOrder="row-major")  # for array
        self.pgImage.setPredefinedGradient(name=gradient_map)
        # configure plots
        self.pgHorizontal.setLabel("bottom", "hor. position (px)")
        self.pgHorizontal.setLabel("left", "count")
        self.pgVertical.setLabel("bottom", "ver. position (px)")
        self.pgVertical.setLabel("left", "count")

        self.curves = Curves(
            image_plot=self.pgImage.getView(),
            horizontal_plot=self.pgHorizontal,
            vertical_plot=self.pgVertical,
        )

    def setupCamera(self) -> None:
        """Setup the camera instance."""
        log.debug("Setup camera.")
        self.tlFactory = pylon.TlFactory.GetInstance()
        self.camera = pylon.InstantCamera()

        # Configure configuration events.
        self.configurationEventHandler = ConfigurationHandler()
        self.configurationEventHandler.signals.cameraRemoved.connect(self.cameraRemoved)
        self.camera.RegisterConfiguration(
            self.configurationEventHandler,
            pylon.RegistrationMode_ReplaceAll,
            pylon.Cleanup_None,
        )

        # configure camera events
        self.imageEventHandler = ImageEventHandler()
        self.imageEventHandler.signals.imageGrabbed.connect(self.update)
        self.camera.RegisterImageEventHandler(
            self.imageEventHandler, pylon.RegistrationMode_Append, pylon.Cleanup_None
        )

    def setup_camera_action_list(self):
        """List of actions which are stored with each camera"""
        self.options: list[tuple[QtGui.QAction, str, bool]] = [
            (self.actionPublish, "publish", False),
            (self.actionEvaluateData, "evaluate", True),
            (self.actionHideData, "hide_data", False),
            (self.actionHideHistogram, "hide_histogram", False),
            (self.actionHideSettings, "hide_settings", False),
            #(self.actionHideAOI, "hide_AOI", True)
        ]

    def get_auto_function_key(self) -> str:
        auto_functions: dict[Callable[[np.ndarray], None], str] = {
            self.autoFunctionOff: "off",
            self.autoFunctionGain: "gain",
            self.autoFunctionExposure: "exposure",
            self.autoFunctionSmart: "smart",
        }
        return auto_functions[self.autoFunction]

    def set_auto_function(self, key: str) -> None:
        if key == "off":
            self.setAutoFunctionOff()
        elif key == "gain":
            self.setAutoFunctionGain()
        elif key == "exposure":
            self.setAutoFunctionExposure()
        elif key == "smart":
            self.setAutoFunctionSmart()

    def getAutoFunctionSettings(self) -> None:
        """Read the current auto function settings and store them in a dict."""
        self.settings.beginGroup("autoFunction")
        for key in ("pixelMaximum", "pixelMinimum", "exposureMaximum", "gainMaximum"):
            self.autoFunctionSettings[key] = self.settings.value(key, 10, int)
        self.settings.endGroup()

    def restoreConfiguration(self) -> None:
        """Restore the configuration saved at closure of the program."""
        settings = QtCore.QSettings()
        if settings.value("hideData", type=bool):
            self.actionHideData.setChecked(True)
            self.actionHideData.triggered.emit(True)
        self.actionEvaluateData.setChecked(settings.value("evaluateData", type=bool))
        self.set_auto_function(key=settings.value("autoFunction", "off", type=str))

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing")
        self.stop_listen()
        self.selectDevice(0)
        self.pool.close()
        # Store the current configuration
        settings = QtCore.QSettings()
        settings.setValue("hideData", self.actionHideData.isChecked())
        settings.setValue("evaluateData", self.actionEvaluateData.isChecked())
        function = self.get_auto_function_key()
        settings.setValue("autoFunction", function)
        # accept the close event (reject it, if you want to do something else)
        event.accept()

    # Buttons and Actions

    #   Actions
    def setSettings(self) -> None:
        settings = QtCore.QSettings()
        self.getAutoFunctionSettings()
        try:
            frm = settings.value("frameRateMax", 10, type=int)
            self.camera.AcquisitionFrameRate.SetValue(frm)
        except genicam.RuntimeException:
            pass  # probably no camera connected.
        except Exception as exc:
            self.showException(exc, "openSettings")
        self.softwareTimer.setInterval(settings.value("softwareWaitingTime", type=int))

    @pyqtSlot()
    def scanForDevices(self) -> None:
        """Scan for devices and reset combobox."""
        self.bbDevice.clear()
        self.bbDevice.addItem("None")
        for deviceInfo in self.tlFactory.EnumerateDevices():
            name = deviceInfo.GetFriendlyName()
            fullName = deviceInfo.GetFullName()
            if name != "":
                self.bbDevice.addItem(name, fullName)
            else:
                self.bbDevice.addItem(f"S/N {deviceInfo.GetSerialNumber()}", fullName)

    @pyqtSlot()
    def showDeviceInfo(self) -> None:
        """Show information about the connected device."""
        message = QtWidgets.QMessageBox()
        message.setWindowTitle("Device information")
        devInfo = self.camera.GetDeviceInfo()
        text = (
            f"Model name: {devInfo.GetModelName()}\n"
            f"Serial number: {devInfo.GetSerialNumber()}\n"
            f"Friendly name: {devInfo.GetFriendlyName()}.\n"
            f"Full name: {devInfo.GetFullName()}"
        )
        message.setText(text)
        message.exec()

    # Buttons
    @pyqtSlot(int)
    def setExposure(self, value: int) -> None:
        """Set the new exposure time in µs."""
        try:
            self.camera.ExposureTime.SetValue(value)
        except Exception as exc:
            self.showException(exc, "setExposure")

    @pyqtSlot(float)
    def setGain(self, value: float) -> None:
        """Set the gain."""
        try:
            self.camera.Gain.SetValue(value)
        except Exception as exc:
            self.showException(exc, "setGain")
        relativeGain = int(value * 1000 / self.sbGain.maximum())
        self.slGain.setSliderPosition(relativeGain)

    @pyqtSlot(int)
    def slGainValueChanged(self, value: int) -> None:
        """Handle a value change of the gain slider."""
        gain = value / 1000 * self.sbGain.maximum()
        self.sbGain.setValue(gain)

    @pyqtSlot()
    def reset_aoi(self) -> None:
        """Reset the Area Of Interes (AOI) to full detector size."""
        running = not self.pbPause.isChecked()
        if running:
            self.pause(True)
        self.camera.OffsetX.SetValue(0)
        self.camera.OffsetY.SetValue(0)
        self.camera.Width.SetValue(int(self.camera.Width.Max))
        self.camera.Height.SetValue(int(self.camera.Height.Max))
        if running:
            self.pause(False)

    @pyqtSlot()
    def set_aoi(self) -> None:
        """Set the Area Of Interest (AOI) to the predefined values."""
        running = not self.pbPause.isChecked()
        if running:
            self.pause(True)
        # height, width and offset must be multiple of 4
        height = int((self.sbHi_v.value() - self.sbLo_v.value()) / self.pixel_length) // 4 * 4
        width = int((self.sbHi_h.value() - self.sbLo_h.value()) / self.pixel_length) // 4 * 4
        offsetX = int(self.sbLo_h.value() / self.pixel_length) // 4 * 4
        offsetY = int(self.sbLo_v.value() / self.pixel_length) // 4 * 4

        # TODO: Test the orientation of the aoi window.
        if self.bbOrientation.currentText() == "Top":
            height, width = width, height
            offsetX, offsetY = int(self.camera.Height.Max) - offsetY, offsetX
        elif self.bbOrientation.currentText() == "Bottom":
            height, width = width, height
            offsetX, offsetY = offsetY, offsetX
        elif self.bbOrientation == "Left":
            offsetX = int(self.camera.Width.Max) - offsetX
        log.info(f"Offset x: {offsetX}, Width: {width}, Offset y: {offsetY}, Height: {height}")
        self.camera.Height.SetValue(height)
        self.camera.Width.SetValue(width)
        self.camera.OffsetX.SetValue(offsetX)
        self.camera.OffsetY.SetValue(offsetY)
        # TODO: with aoi activated the fit was not possible in the case of too small windows.
        # Possible reasons are rounding errors or that the orientation (including the width and
        # height of data) has to be reset. While disabling pause I try to fix the error by setting
        # the orientation again. CHECK IF FIT ERROR IS SOLVED!
        if running:
            self.pause(False)

    @pyqtSlot(int)
    def setAutoFunctionProfile(self, value: int) -> None:
        """Set the automatic adjustment profile for exposure and gain."""
        try:
            self.camera.AutoFunctionProfile.SetIntValue(value)
        except Exception as exc:
            self.showException(exc, "setAutoFunctionProfile")

    @pyqtSlot()
    def setAutoFunctionOff(self) -> None:
        """Set the autofunction to be off."""
        self.autoFunction = self.autoFunctionOff
        for widget in (self.slExposure, self.sbExposure, self.slGain, self.sbGain):
            widget.setEnabled(True)

    @pyqtSlot()
    def setAutoFunctionExposure(self) -> None:
        """Set the autofunction to be automatic exposure."""
        self.autoFunction = self.autoFunctionExposure
        for widget in (self.slExposure, self.sbExposure):
            widget.setEnabled(False)
        for widget in (self.slGain, self.sbGain):
            widget.setEnabled(True)

    @pyqtSlot()
    def setAutoFunctionGain(self) -> None:
        """Set the autofunction to be automatic gain."""
        self.autoFunction = self.autoFunctionGain
        for widget in (self.slExposure, self.sbExposure):
            widget.setEnabled(True)
        for widget in (self.slGain, self.sbGain):
            widget.setEnabled(False)

    @pyqtSlot()
    def setAutoFunctionSmart(self) -> None:
        """Set the autofunction to be smart."""
        self.autoFunction = self.autoFunctionSmart
        for widget in (self.slExposure, self.sbExposure, self.slGain, self.sbGain):
            widget.setEnabled(False)

    def autoFunctionOff(self, *args, **kwargs):
        """Do nothing, because the autofunction is off."""
        return

    def autoFunctionExposure(self, array: np.ndarray) -> None:
        """Automatically adjust the exposure time."""
        maximum = np.max(array)
        settings = self.autoFunctionSettings
        if maximum > settings["pixelMaximum"]:
            try:
                exposureTime = self.camera.ExposureTime.GetValue()
            except Exception as exc:
                self.showException(exc, "autoFunctionExposure")
            else:
                self.sbExposure.setValue(max(1, round(exposureTime - 250)))
        elif maximum < settings["pixelMinimum"]:
            try:
                exposureTime = self.camera.ExposureTime.GetValue()
            except Exception as exc:
                self.showException(exc, "autoFunctionExposure")
            else:
                if exposureTime < settings["exposureMaximum"] * 1000:
                    self.sbExposure.setValue(round(exposureTime + 250))

    def autoFunctionGain(self, array: np.ndarray) -> None:
        """Automatically adjust the gain."""
        # Gain is supposed to be float, but adjustments are only possible in steps of 0,16345 dB
        maximum = np.max(array)
        settings = self.autoFunctionSettings
        if maximum > settings["pixelMaximum"]:
            try:
                gain = self.camera.Gain.GetValue()
            except Exception as exc:
                self.showException(exc, "autoFunctionGain")
            else:
                if gain > 0:
                    self.sbGain.setValue(max(0, gain - 0.3))
        elif maximum < settings["pixelMinimum"]:
            try:
                gain = self.camera.Gain.GetValue()
            except Exception as exc:
                self.showException(exc, "autoFunctionGain")
            else:
                if gain < settings["gainMaximum"]:
                    self.sbGain.setValue(min(gain + 0.3, settings["gainMaximum"]))

    def autoFunctionSmart(self, array: np.ndarray) -> None:
        """Automatically adjust first exposure time then gain."""
        maximum = np.max(array)
        settings = self.autoFunctionSettings
        if maximum > settings["pixelMaximum"]:
            try:
                gain = self.camera.Gain.GetValue()
                if gain > 0:
                    self.sbGain.setValue(max(0, gain - 0.3))
                else:
                    exposureTime = self.camera.ExposureTime.GetValue()
                    self.sbExposure.setValue(max(1, round(exposureTime - 250)))
            except Exception as exc:
                self.showException(exc, "autoFunctionSmart")
        elif maximum < settings["pixelMinimum"]:
            try:
                exposureTime = self.camera.ExposureTime.GetValue()
                if exposureTime < settings["exposureMaximum"] * 1000:
                    self.sbExposure.setValue(round(exposureTime + 250))
                else:
                    gain = self.camera.Gain.GetValue()
                    self.sbGain.setValue(min(gain + 0.3, settings["gainMaximum"]))
            except Exception as exc:
                self.showException(exc, "autoFunctionSmart")

    @pyqtSlot(int)
    def selectDevice(self, index: int) -> None:
        """Select a device."""
        self.store_camera_config()
        self.camera.Close()
        self.camera.DetachDevice()
        if index != 0:
            fullName = self.bbDevice.currentData()
            device = self.tlFactory.CreateDevice(fullName)
            try:
                self.camera.Attach(device)
                self.camera.Open()
                frm = self.settings.value("frameRateMax", 10, type=int)
                self.camera.AcquisitionFrameRate.SetValue(frm)
                if not self.pbPause.isChecked():
                    self.pause(False)
            except Exception as exc:
                self.showException(exc, "selectDevice")
            else:
                self.getCameraConfig(self.camera)
                short_camera_name = self.bbDevice.currentText().split()[0]
                name = f"BeamCam_{short_camera_name}"
                self.setWindowTitle(name.replace("_", " "))
                self.communicator.name = name
                self.cam_name = short_camera_name
                if self.bbOrientation.currentText() == "Right" or self.bbOrientation.currentText() == "Left":
                    self.sbHi_v.setMaximum(int(self.pixel_length * int(self.camera.Height.Max)))
                    self.sbLo_v.setMaximum(int(self.pixel_length * int(self.camera.Height.Max)))
                    self.sbHi_h.setMaximum(int(self.pixel_length * int(self.camera.Width.Max)))
                    self.sbLo_h.setMaximum(int(self.pixel_length * int(self.camera.Width.Max)))
                else:
                    self.sbHi_h.setMaximum(int(self.pixel_length * int(self.camera.Height.Max)))
                    self.sbLo_h.setMaximum(int(self.pixel_length * int(self.camera.Height.Max)))
                    self.sbHi_v.setMaximum(int(self.pixel_length * int(self.camera.Width.Max)))
                    self.sbLo_v.setMaximum(int(self.pixel_length * int(self.camera.Width.Max)))
                log.info(f"Connected to {self.bbDevice.currentText()}.")
        else:
            self.setWindowTitle("BeamCamera")
            self.communicator.name = "BeamCam"

    @pyqtSlot(bool)
    def pause(self, checked: bool) -> None:
        """Pause image grabbing."""
        if checked:
            try:
                self.camera.StopGrabbing()
            except (AttributeError, genicam.RuntimeException):
                pass  # no camera present
        else:
            try:
                self.setOrientation()
                self.camera.StartGrabbing(
                    pylon.GrabStrategy_LatestImageOnly,
                    pylon.GrabLoop_ProvidedByInstantCamera,
                )
            except (AttributeError, genicam.RuntimeException):
                pass

    @pyqtSlot(dict)
    def trigger_message_arrived(self, values: dict) -> None:
        """Trigger the camera."""
        self.softwareTimer.start()

    def get_image_configuration(self) -> dict[str, Any]:
        settings = QtCore.QSettings()
        config = {}
        config["view_direction_against_beam"] = settings.value(
            "viewDirection", type=int
        )
        try:
            deviceInfo = self.camera.GetDeviceInfo()
            config["model"] = deviceInfo.GetModelName()
            config["fullname"] = deviceInfo.GetFullName()
        except Exception:
            config["model"] = "unknown"
        config["camera"] = self.bbDevice.currentText()
        config["exposure_time"] = f"{self.sbExposure.value() / 1000} ms"
        config["gain"] = f"{self.sbGain.value()} dB"
        config["orientation"] = self.bbOrientation.currentIndex()
        config["precise_time"] = datetime.datetime.now().isoformat()
        config["pixel_length"] = f"{self.pixel_length} {self.unit}"
        if self.actionEvaluateData.isChecked():
            if (hp := self.plot_data.get("horizontalPar")) is not None:
                xR = 2 * self.pixel_length * hp[2]
                config["horizontal_radius"] = f"{xR} {self.unit}"
            if (vp := self.plot_data.get("verticalPar")) is not None:
                yR = 2 * self.pixel_length * vp[2]
                config["vertical_radius"] = f"{yR} {self.unit}"
            config["center"] = self.lbCenter.text()
        return config

    @pyqtSlot()
    def saveImage(self, config: Optional[dict[str, Any]] = None) -> None:
        """Save the image to disk."""
        settings = QtCore.QSettings()
        self.leSavedName.setText("Saving...")

        try:
            name = save_image(
                image=self.image,
                directory=settings.value("savePath", type=str),
                config=self.get_image_configuration(),
            )
        except AttributeError:
            self.leSavedName.setText("No image present.")
            return  # no image present

        self.leSavedName.setText(name)
        log.info(f"Saved image {name}.")
        clipboard = QtWidgets.QApplication.instance().clipboard()  # type: ignore
        clipboard.setText(name)

    @pyqtSlot(int)
    def storeOrientation(self, index: int) -> None:
        """Store the newly chosen orientation of the camera."""
        try:
            fullname = self.camera.GetDeviceInfo().GetFullName()
        except Exception as exc:
            self.showException(exc, "storeOrientation")
        else:
            settings = QtCore.QSettings()
            settings.beginGroup(str(fullname))
            settings.setValue("orientation", index)

    @pyqtSlot(int)
    def setOrientation(self, index: int) -> None:
        """Set the orientation of the image and adjust data."""
        try:
            width = self.camera.Width.GetValue()
            height = self.camera.Height.GetValue()
        except Exception as exc:
            self.showException(exc, "setOrientation")
        else:
            if index % 2:
                width, height = height, width
            self.plot_data["dataX"] = range(width)
            self.plot_data["dataY"] = range(height)

    # Camera and image events
    # @pyqtSlot(?)
    def update(self, array: np.ndarray) -> None:
        """Update the shown image."""
        # direction: 0 against beam, 1 along beam propagation.
        direction = bool(self.settings.value("viewDirection", type=int))
        # orientation of USB port from point of view of camera: 0 left, 1 top,
        # 2 right, 3 bottom.
        orientation = self.bbOrientation.currentIndex()
        # HACK noqa due to spyder not recoqnizing match
        match direction, orientation:  # noqa
            case False, orientation if orientation:
                array = np.rot90(array, 4 - orientation)
            case True, 0:
                array = np.fliplr(array)
            case True, 2:
                array = np.flipud(array)
            case True, orientation:
                #  if orientation in (1, 3)
                array = np.fliplr(array)
                array = np.rot90(array, orientation)
        self.pgImage.setImage(array, autoRange=False)
        self.image = array
        self.autoFunction(array)
        if self.actionPublish.isChecked():
            s = int(np.sum(array))
            data = {"E": s, "P": s / self.sbExposure.value()}
            self.publisher.send_data(data)
            self.publisher.send_legacy(
                {f"{self.cam_name}_{key}": value for key, value in data.items()}
            )
        # Do a mathematical analysis if the button is checked. Else return.
        if not self.actionEvaluateData.isChecked():
            return
        try:
            ready = self.result.ready()
        except AttributeError:
            ready = True  # self.result not yet defined.
        if ready:
            # Send the data to be analysed.
            try:
                self.result = self.pool.apply_async(
                    calcCenter,
                    kwds={"array": array},
                    callback=self.signals.calculationReady.emit,
                )
            except ValueError:
                pass  # Pool already shut down
                log.debug("Value error on sending data to analysis.")
            # Show the last results.
            try:
                x0i, y0i = self.center
            except AttributeError:
                pass  # no data yet defined
            else:
                self.plot_data["dataX"] = array[y0i, :]
                self.plot_data["dataY"] = array[:, x0i]
                # Show detail plots.
                self.curves.horizontal.setData(self.plot_data["dataX"])
                if (hp := self.plot_data.get("horizontalPar")) is not None:
                    data_width = len(self.plot_data["dataX"])
                    x = range(0, data_width, min(10, max(1, int(data_height / 100))))
                    f = gaussian(x, *hp)
                    self.curves.horizontal_fit.setData(x, f)
                    width = 2 * abs(self.plot_data["horizontalPar"][2])
                    f = [max(0, x0i - width), min(x0i + width, data_width)]
                    self.curves.width_marker.setData(f, [y0i, y0i])
                else:
                    self.curves.horizontal_fit.setData([])
                    self.curves.width_marker.setData([])
                self.curves.vertical.setData(self.plot_data["dataY"])
                if (vp := self.plot_data.get("verticalPar")) is not None:
                    data_height = len(self.plot_data["dataY"])
                    y = range(0, data_height, min(10, max(1, int(data_height / 100))))
                    f = gaussian(y, *vp)
                    self.curves.vertical_fit.setData(y, f)
                    height = 2 * abs(self.plot_data["verticalPar"][2])
                    f = [max(0, y0i - height), min(y0i + height, data_height)]
                    self.curves.height_marker.setData([x0i, x0i], f)
                else:
                    self.curves.vertical_fit.setData([])
                    self.curves.height_marker.setData([])

    @pyqtSlot(dict)
    def showCalculation(self, data: dict[str, float]) -> None:
        """Show the results of the calculation."""
        self.center = int(data["x0"]), int(data["y0"])
        x0 = data["x0"] * self.pixel_length
        y0 = data["y0"] * self.pixel_length
        for key in ("horizontalPar", "verticalPar"):
            self.plot_data[key] = data[key]
        self.lbCenter.setText(f"Center: {x0:.1f}{self.unit}, {y0:.1f}{self.unit}")
        if (hp := self.plot_data.get("horizontalPar")) is not None:
            xRadius = 2 * self.pixel_length * abs(hp[2])
        else:
            xRadius = float("nan")
        if (vp := self.plot_data.get("verticalPar")) is not None:
            yRadius = 2 * self.pixel_length * abs(vp[2])
        else:
            yRadius = float("nan")
        self.lbBeamSize.setText(
            (
                f"Radius: {xRadius:.1f}{self.unit} x"
                f" {yRadius:.1f}{self.unit}"
                f"\nDifference: {abs(xRadius - yRadius): >5.1f}"
                f"{self.unit}"
            )
        )
        self.lbCalculationTime.setText(
            f"Pixel: {self.pixel_length} {self.unit}, Time: {data['time'] * 1000:.0f} ms"
        )
        if self.actionPublish.isChecked():
            data = {
                "ch": x0,
                "cv": y0,
                "rh": xRadius,
                "rv": yRadius,
            }
            if (hp := self.plot_data.get("horizontalPar")) is not None:
                data["ah"] = hp[0] / self.sbExposure.value()
            if (vp := self.plot_data.get("verticalPar")) is not None:
                data["av"] = vp[0] / self.sbExposure.value()
            self.publisher.send_data(data)
            self.publisher.send_legacy(
                {f"{self.cam_name}_{key}": value for key, value in data.items()}
            )

    @pyqtSlot(bool)
    def hideCrosshair(self, show: bool) -> None:
        """Hide the crosshair if `show` is False."""
        if not show:
            self.curves.width_marker.setData([])
            self.curves.height_marker.setData([])

    @pyqtSlot(bool)
    def hide_histogram(self, checked: bool) -> None:
        self.pgImage.ui.histogram.setVisible(not checked)
        self.pgImage.ui.roiBtn.setVisible(not checked)
        self.pgImage.ui.menuBtn.setVisible(not checked)

    # @pyqtSlot(?)
    def cameraRemoved(self, camera: pylon.InstantCamera) -> None:
        """Handle a camera removal."""
        log.debug(f"Camera '{camera}' removed.")
        camera.DestroyDevice()
        self.bbDevice.setCurrentIndex(0)

    # Methods
    def showException(self, exception: Exception, caller: Optional[str] = None) -> None:
        """Show an exception to the user."""
        window = QtWidgets.QMessageBox()
        window.setIcon(QtWidgets.QMessageBox.Icon.Warning)
        window.setWindowTitle("Exception notification")
        text = (
            f"Exception type {type(exception).__name__} occurred with "
            f"message {exception}{'' if caller is None else 'in caller '}"
            f"{caller}."
        )
        window.setText(text)
        # optional: window.setDetailedText("some stuff")
        window.exec()

    def getCameraConfig(self, camera: pylon.InstantCamera) -> None:
        """Update the UI to the camera settings."""
        # Shape: Height and Width.
        try:
            fullname = camera.GetDeviceInfo().GetFullName()
            model = camera.GetDeviceInfo().GetModelName()
        except Exception as exc:
            self.showException(exc, "getCameraConfig, Shape")
            return
        # settings object
        settings = QtCore.QSettings()
        settings.beginGroup(str(fullname))
        # orientation
        orientation = settings.value("orientation", 0, int)
        self.bbOrientation.setCurrentIndex(orientation)
        self.setOrientation(orientation)
        # Pixel length.
        self.pixel_length = pixel_lengths.get(model, 1)
        self.unit = "µm" if model in pixel_lengths.keys() else "pixel"
        unit = "px" if self.unit == "pixel" else "m"
        pixel_length = self.pixel_length if unit == "pixel" else self.pixel_length / 1e6
        # adjust axis labels
        image_view = self.pgImage.getView()
        image_view.setLabel("left", "ver. position", units=unit)
        image_view.setLabel("bottom", "hor. position", units=unit)
        image_view.getAxis("left").setScale(pixel_length)
        image_view.getAxis("bottom").setScale(pixel_length)
        self.pgHorizontal.setLabel("bottom", "hor. position", units=unit)
        self.pgHorizontal.getAxis("bottom").setScale(pixel_length)
        self.pgVertical.setLabel("bottom", "ver. position", units=unit)
        self.pgVertical.getAxis("bottom").setScale(pixel_length)
        # Exposure
        defaultExposures = {
            # default exposure times of the cameras, whenever reset due to power loss
            "daA1280-54um": 15000,
            "daA2500-14um": 20000,
        }
        try:
            minimum = int(camera.ExposureTime.GetMin())
            maximum = int(camera.ExposureTime.GetMax())
            value = int(camera.ExposureTime.GetValue())
        except Exception as exc:
            self.showException(exc, "getCameraConfig, Exposure")
            return
        else:
            if value == defaultExposures.get(model, 0) and (
                stored_value := settings.value("exposure_time", 0, int)
            ):
                value = stored_value
            for widget in (self.slExposure, self.sbExposure):
                widget.setMinimum(minimum)
                widget.setMaximum(maximum)
                widget.setValue(value)
        # Gain
        try:
            minimum = int(camera.Gain.GetMin())
            maximum = int(camera.Gain.GetMax())
            value = int(camera.Gain.GetValue())
        except Exception as exc:
            self.showException(exc, "getCameraConfig, Gain")
        else:
            for widget in (self.sbGain,):
                widget.setMinimum(minimum)
                widget.setMaximum(maximum)
                widget.setValue(value)
        # GUI options
        for action, key, default in self.options:
            action.setChecked(settings.value(key, default, bool))
        # Write log entry
        log.info(f"Camera {fullname} configured.")

    def store_camera_config(self) -> None:
        try:
            fullname = self.camera.GetDeviceInfo().GetFullName()
        except Exception as exc:
            self.showException(exc, "storeOrientation")
            return
        if fullname == "N/A":
            # no valid camera
            log.info("Storing data not possible, as it is not a valid camera.")
            return
        settings = QtCore.QSettings()
        settings.beginGroup(str(fullname))
        settings.setValue("orientation", self.bbOrientation.currentIndex())
        settings.setValue("exposure_time", self.sbExposure.value())
        # GUI options
        for action, key, default in self.options:
            settings.setValue(key, action.isChecked())

    @pyqtSlot(bool)
    def setTrigger(self, checked: bool) -> None:
        settings = QtCore.QSettings()
        self.communicator.unsubscribe_all()
        if checked:
            self.camera.TriggerSelector.SetValue("FrameStart")
            if settings.value("triggerSource") == 0:
                self.camera.TriggerSource.SetValue("Line1")
                if settings.value("triggerActivation") == 1:
                    triggerActivation = "FallingEdge"
                else:
                    triggerActivation = "RisingEdge"
                self.camera.TriggerActivation.SetValue(triggerActivation)
                if settings.value("exposureMode") == 1:
                    exposureMode = "TriggerWidth"
                else:
                    exposureMode = "Timed"
                self.camera.ExposureMode.SetValue(exposureMode)
            elif settings.value("triggerSource") == 1:
                self.camera.TriggerSource.SetValue("Line2")
                if settings.value("triggerActivation") == 1:
                    triggerActivation = "FallingEdge"
                else:
                    triggerActivation = "RisingEdge"
                self.camera.TriggerActivation.SetValue(triggerActivation)
                if settings.value("exposureMode") == 1:
                    exposureMode = "TriggerWidth"
                else:
                    exposureMode = "Timed"
                self.camera.ExposureMode.SetValue(exposureMode)
            elif settings.value("triggerSource") == 2:
                self.camera.TriggerSource.SetValue("Software")
                self.communicator.subscribe(
                    settings.value(
                        "softwareTriggerName", defaultValue="picoscope", type=str
                    )
                )
                self.softwareTimer.setInterval(
                    settings.value("softwareWaitingTime", type=int)
                )
            else:
                self.actionTrigger.setChecked(False)
                log.info("No valid trigger source set.")
            self.camera.TriggerMode.SetValue("On")
        else:
            self.camera.TriggerMode.SetValue("Off")

    def softwareTrigger(self):
        self.camera.TriggerSoftware.Execute()


def gaussian(x, amplitude, center, sigma, offset):
    """A gaussian curve."""
    return amplitude * np.exp(-((x - center) ** 2) / 2 / sigma**2) + offset


# Functions for the processes
def calcCenter(array: np.ndarray) -> dict[str, float]:
    """Calculate the center of mass and return it."""
    gaussian_model = Model(gaussian)
    x0: float
    y0: float
    start = time.perf_counter()
    reduced = array.copy()
    reduced[reduced < 0.5 * np.max(reduced)] = 0
    data = {}
    y0, x0 = scipy.ndimage.center_of_mass(reduced)  # type: ignore
    data["y0"] = float(y0)
    data["x0"] = float(x0)
    y0 = int(y0)
    x0 = int(x0)
    ymax, xmax = np.shape(array)
    minimum = np.min(array)
    try:
        x_params = gaussian_model.make_params(amplitude=1000, center=x0, sigma=100, offset=minimum)
        x_result = gaussian_model.fit(array[y0, :], x_params, x=np.arange(xmax))
        data["horizontalPar"] = [x_result.params["amplitude"].value, x_result.params["center"].value,
                                 x_result.params["sigma"].value, x_result.params["offset"].value]
    except RuntimeError as exc:
        data["horizontalPar"] = None
        log.exception("Data fit for horizontal axis failed.", exc_info=exc)
    try:
        y_params = gaussian_model.make_params(amplitude=1000, center=y0, sigma=100, offset=minimum)
        y_result = gaussian_model.fit(array[:, x0], y_params, x=np.arange(ymax))
        data["verticalPar"] = [y_result.params["amplitude"].value, y_result.params["center"].value,
                               y_result.params["sigma"].value, y_result.params["offset"].value]
    except RuntimeError as exc:
        data["verticalPar"] = None
        log.exception("Data fit for vertical axis failed.", exc_info=exc)
    data["time"] = time.perf_counter() - start
    return data


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(BeamCamera)

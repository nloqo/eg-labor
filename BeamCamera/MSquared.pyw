# -*- coding: utf-8 -*-
"""
M^2 measurement based on the BeamCamera.

Created on Thu Jan 13 17:26:47 2022 by Benedikt Moneke
"""

import logging

from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import pyqtSlot
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110

from devices import motors
from devices.gui_utils import parse_command_line_parameters

from BeamCamera import BeamCamera
from data import MSettings


log = logging.Logger(__name__)
log.addHandler(logging.StreamHandler())


class MSquared(BeamCamera):
    """A modified version of the BeamCamera, able to detect M squared."""

    def __init__(self, **kwargs):
        """Initialize the window and adjust it."""
        super().__init__(**kwargs)
        self.setWindowTitle("M Squared")

        self.mTimer = QtCore.QTimer()
        self.mTimer.setInterval(50)
        self.mTimer.timeout.connect(self.makeMPhoto)

        # Add menu entries
        self.menuMSquared = QtWidgets.QMenu("M squared")
        self.menuBar().addMenu(self.menuMSquared)
        self.menuMSquared.addAction("Settings...")
        self.menuMSquared.addAction("Motor...")
        self.menuMSquared.addAction("Connect Motor")
        self.menuMSquared.addAction("Measure")
        self.actionMSettings = self.menuMSquared.actions()[0]
        self.actionMMotor = self.menuMSquared.actions()[1]
        self.actionConnectMotor = self.menuMSquared.actions()[2]
        self.actionConnectMotor.setCheckable(True)
        self.actionMMeasure = self.menuMSquared.actions()[3]
        self.actionMMeasure.setCheckable(True)

        # Connect them to slots
        self.actionMSettings.triggered.connect(self.openMSettings)
        self.actionMMotor.triggered.connect(self.openMMotor)
        self.actionConnectMotor.triggered.connect(self.connectMotor)
        self.actionMMeasure.triggered.connect(self.measureMSquared)

    @pyqtSlot()
    def openMSettings(self):
        """Open the general settings for M squared."""
        dialog = MSettings.MSettings()
        dialog.exec()

    @pyqtSlot()
    def openMMotor(self):
        """Open the configuration for the motor for M squared."""
        dialog = motors.MotorSettings('motor')
        if dialog.exec():
            settings = QtCore.QSettings()
            self.config = settings.value('motor', type=dict)
            if self.actionConnectMotor.isChecked():
                try:
                    motors.configureMotor(self.motorCard, self.config)
                except KeyError:
                    pass  # no value
                except Exception as exc:
                    log.exception("Error configuring motor.", exc_info=exc)

    @pyqtSlot(bool)
    def connectMotor(self, checked):
        """Connect to the motor card."""
        settings = QtCore.QSettings()
        if checked:
            COM = settings.value('COM', type=int)
            try:
                self.connectionManager = ConnectionManager(f"--port COM{COM}")
                self.motorCard = TMCM6110(self.connectionManager.connect())
            except Exception as exc:
                print(exc)
        else:
            try:
                self.connectionManager.disconnect()
            except AttributeError:
                pass  # Not existant
            except Exception as exc:
                print(exc)

    @pyqtSlot(bool)
    def measureMSquared(self, checked):
        """Do a measurement."""
        if checked:
            self.names = []
            config = self.settings.value('motor', type=dict)
            start = motors.unitsToSteps(self.settings.value('startPosition', type=float), config)
            self.motorCard.move_to(config['motorNumber'], start)
            self.mTimer.start()
            self.config = config
            stop = motors.unitsToSteps(self.settings.value('stopPosition', type=float), config)
            self.stepSize = round((stop - start) / self.settings.value('MSteps', type=int))
            self.stepNumber = 0
        else:
            self.mTimer.stop()
            self.motorCard.stop(self.config['motorNumber'])

    @pyqtSlot()
    def makeMPhoto(self):
        """Make a photo if ready."""
        try:
            reached = self.motorCard.motors[self.config['motorNumber']].get_position_reached()
        except Exception as exc:
            print(exc)
            self.mTimer.stop()
            self.actionMMeasure.setChecked(False)
            self.connectMotor(False)
            return
        if reached:
            self.mTimer.stop()  # To give time to save a photo
            self.saveImage()
            self.names.append(self.leSavedName.text())
            if self.stepNumber < self.settings.value('MSteps', type=int):
                self.motorCard.move_by(self.config['motorNumber'], self.stepSize)
                self.stepNumber += 1
                self.mTimer.start()  # Restart timer
            else:
                print(self.names)
                self.leSavedName.setText(str(self.names))
                self.actionMMeasure.setChecked(False)

    @pyqtSlot()
    def saveImage(self):
        """Append the position to the image metadata."""
        microsteps = self.motorCard.motors[self.config['motorNumber']].actual_position
        position = motors.stepsToUnits(microsteps, self.config)
        super().saveImage(config={'position': f"{position} mm"})


if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    kwargs = parse_command_line_parameters(logger=log)
    app = QtWidgets.QApplication([])  # start an application
    mainwindow = MSquared(**kwargs)  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

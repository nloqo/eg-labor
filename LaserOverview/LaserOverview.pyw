"""
Overview over all Lasers in the Lab

created on 23.11.2020 by Benedikt Burger
"""

# Standard packages.
from enum import Enum
import logging
from typing import Any, Optional

# 3rd party packages.

from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot, Qt
from PyQt6.QtGui import QColor

from pyleco.core.message import Message
from pyleco.errors import NODE_UNKNOWN, RECEIVER_UNKNOWN
from pyleco.directors.transparent_director import TransparentDirector

try:
    from pyleco.json_utils.errors import JSONRPCError
except ModuleNotFoundError:
    from jsonrpcobjects.errors import JSONRPCError

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
)

# Local packages.
from data.settings import Settings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


class ActorColors(Enum):
    """Colors for the starters themselves."""

    CONNECTED = QColor("Lime")  # Lime, Green, DarkGreen
    DISCONNECTED = QColor("DarkRed")  # Red, DarkRed


class PropertyColors(Enum):
    EMISSION_ON = QColor("Orange")
    EMISSION_OFF = QColor("White")
    DISCONNECTED = QColor("LightGray")
    DEFAULT = QColor("white")


DESCRIPTION_COLOR = QColor("Gray")  # yellow, gray


class BaseItem(QtGui.QStandardItem):
    def __init__(self, name: str, tooltip: Optional[str] = None, **kwargs) -> None:
        super().__init__(name, **kwargs)
        if tooltip is not None:
            self.setToolTip(tooltip)


class PropertyItem(BaseItem):
    """An item describing a property.

    :param name: Name of the property you want to read.
    :param value_text: Describes how the values should be formatted, for example `"{value} W"`.
        Use `value` as the name of the variable
    :param tooltip: Tooltip to show.
    """

    def __init__(
        self,
        name: str,
        value_text: Optional[str] = None,
        tooltip: Optional[str] = None,
        **kwargs,
    ) -> None:
        self._text = value_text or "{value}"
        super().__init__(name=name, tooltip=tooltip, **kwargs)
        self.setBackground(PropertyColors.DISCONNECTED.value)
        self.name = name
        self.value = "-"

    def setData(self, value: Any, role: int = Qt.ItemDataRole.DisplayRole) -> None:
        if role == Qt.ItemDataRole.DisplayRole:
            color = self.map_value_to_color(value)
            self.setBackground(color)
            self.value = value
        elif role == Qt.ItemDataRole.EditRole:
            self._text = value
            return
        return super().setData(value, role)

    def data(self, role: int = Qt.ItemDataRole.DisplayRole) -> Any:
        if role == Qt.ItemDataRole.DisplayRole:
            try:
                return ": ".join((self.name, self._text.format(value=self.value)))
            except Exception as exc:
                log.error(
                    f"Cannot read '{self.name}' with value text '{self._text}', "
                    "probably the `value_text` parameter is wrong.",
                    exc_info=exc,
                )
                return f"{self.name} - {type(exc).__name__}: {exc}"
        elif role == Qt.ItemDataRole.EditRole:
            return self._text
        else:
            return super().data(role)

    def map_value_to_color(self, value: Any) -> QColor:
        return PropertyColors.DEFAULT.value

    def set_disconnected(self) -> None:
        self.setBackground(PropertyColors.DISCONNECTED.value)


class EmissionPropertyItem(PropertyItem):
    mapping = {
        True: PropertyColors.EMISSION_ON,
        False: PropertyColors.EMISSION_OFF,
    }

    def map_value_to_color(self, value: bool) -> QColor:
        return self.mapping.get(value, PropertyColors.DISCONNECTED).value


class ActorItem(BaseItem):
    def __init__(
        self, name: str, actor: str, tooltip: Optional[str] = None, **kwargs
    ) -> None:
        super().__init__(name, tooltip, **kwargs)
        self.actor = actor

    def data(self, role: int = Qt.ItemDataRole.DisplayRole) -> Any:
        if role == Qt.ItemDataRole.DisplayRole:
            return f"{super().data(role)} ({self.actor})"
        return super().data(role)

    def add_description(self, description: str, position: int = -1) -> None:
        """Add a mere description item at the beginning."""
        item = BaseItem(description)
        item.setBackground(DESCRIPTION_COLOR)
        if position == -1:
            self.appendRow(item)
        else:
            self.insertRow(position, item)

    def add_property(
        self,
        name: str,
        cls: type[PropertyItem] = PropertyItem,
        value_text: Optional[str] = None,
        tooltip: Optional[str] = None,
        **kwargs,
    ) -> PropertyItem:
        prop = cls(name=name, tooltip=tooltip, value_text=value_text, **kwargs)
        self.appendRow(prop)
        return prop

    def get_request_information(self) -> tuple[str, list[str]]:
        properties = []
        for i in range(self.rowCount()):
            child = self.child(i)
            if isinstance(child, PropertyItem):
                properties.append(child.name)
        return self.actor, properties

    def set_values(self, values: dict[str, Any]) -> None:
        self.setBackground(ActorColors.CONNECTED.value)
        for key, value in values.items():
            for i in range(self.rowCount()):
                child = self.child(i)
                if isinstance(child, PropertyItem) and child.name == key:
                    child.setData(value, Qt.ItemDataRole.DisplayRole)
                    break

    def set_disconnected_status(self) -> None:
        self.setBackground(ActorColors.DISCONNECTED.value)
        for i in range(self.rowCount()):
            child = self.child(i)
            if isinstance(child, PropertyItem):
                child.set_disconnected()


class GroupItem(BaseItem):
    def add_actor(
        self,
        actor: str,
        name: Optional[str] = None,
        tooltip: Optional[str] = None,
        **kwargs,
    ) -> ActorItem:
        if name is None:
            name = actor
        act = ActorItem(name=name, actor=actor, tooltip=tooltip, **kwargs)
        self.appendRow(act)
        return act


class LaserOverview(LECOBaseMainWindowDesigner):
    """Overview over all active lasers

    :param name: Name of this program.
    :param host: Host name of the Coordinator.
    """

    treeView: QtWidgets.QTreeView

    def __init__(
        self, name: str = "LaserOverview", host: str = "localhost", **kwargs
    ) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(
            name=name,
            host=host,
            ui_file_name="LaserOverview",  # ui file name in the data subfolder
            settings_dialog_class=Settings,  # class of the settings dialog
            **kwargs,
        )

        self.create_model()

        self.cids: dict[bytes, ActorItem] = {}
        self.director = TransparentDirector(communicator=self.communicator)

        self.readout_timer = QtCore.QTimer()
        self.readout_timer.timeout.connect(self.request_properties)
        self.readout_timer.start(1000)

        # Apply settings
        settings = QtCore.QSettings()
        geometry = settings.value("geometry")
        if geometry is not None:
            self.restoreGeometry(QtCore.QByteArray(geometry))
        self.setSettings()

        # Connect actions to slots.
        log.info(f"LaserOverview initialized with name '{name}'.")

    def __del__(self):
        try:
            self.stop_listen()
        except AttributeError:
            pass

    def create_model(self) -> None:
        """Create the model."""
        # In General
        self.model = QtGui.QStandardItemModel()
        self.model.setHorizontalHeaderLabels(["Lasers"])
        self.model_root = self.model.invisibleRootItem() or QtGui.QStandardItem()
        self.treeView.setModel(self.model)
        self.sm = self.treeView.selectionModel()

        # Items
        self.setup_yag()
        self.setup_thg_lasers()
        self.setup_ps_lasers()

    def setup_yag(self) -> None:
        yag = GroupItem("Quanta-Ray")
        self.model_root.appendRow(yag)

        innolas = yag.add_actor(actor="pico3.innolas", name="Quanta-Ray seed laser")
        innolas.add_description("1064 nm, up to 100 mW, cw")
        innolas.add_property("emission_enabled", EmissionPropertyItem)

        quanta_ray = yag.add_actor(actor="pico3.quanta-ray", name="Quanta-Ray")
        quanta_ray.add_description("1064 nm, up to 2J, 8 ns, 20 Hz")
        quanta_ray.add_description("532 nm, up to 1J, 8 ns, 20 Hz")
        # quanta_ray.add_property("emission_enabled", EmissionPropertyItem)
        qr_status = quanta_ray.add_property(
            "status", value_text="{value}, emission is on if odd"
        )

        def map_qr_value_to_color(value: int) -> QColor:
            if value & 1:
                return PropertyColors.EMISSION_ON.value
            else:
                return PropertyColors.EMISSION_OFF.value

        qr_status.map_value_to_color = map_qr_value_to_color

        shutter = yag.add_actor(actor="pico3.yagShutter", name="Quanta-Ray shutter")

        def map_shutter_value_to_color(value: int) -> QColor:
            if value > 0:
                return PropertyColors.EMISSION_ON.value
            elif value < 0:
                return QColor("red")
            else:
                return PropertyColors.EMISSION_OFF.value

        shutter.add_description("1064 nm, up to 2J, 8 ns, 20 Hz")
        fund = shutter.add_property("fundamental_open")
        fund.map_value_to_color = map_shutter_value_to_color
        shutter.add_description("532 nm, up to 1J, 8 ns, 20 Hz")
        shg = shutter.add_property("shg_open")
        shg.map_value_to_color = map_shutter_value_to_color

        self.treeView.expandRecursively(self.model.indexFromItem(yag))

    def setup_thg_lasers(self) -> None:
        thg = GroupItem("THG")
        self.model_root.appendRow(thg)

        seed = thg.add_actor(actor="MYRES.koherasBasik", name="OPO pump seed laser")
        seed.add_description("1064 nm, up to 50 mW, cw")
        seed.add_property("emission_enabled", EmissionPropertyItem)

        amp = thg.add_actor(actor="MYRES.fiberAmp", name="OPO pump fiber amplifier")
        amp.add_description("1064 nm, up to 15 W, cw")
        amp.add_description("Also OPO: 3000-4000 nm, up to 5 W, cw")
        amp.add_description("Also OPO: 1400-1600 nm, up to 15 W, cw")
        amp.add_property("emission_enabled", EmissionPropertyItem)
        # amp.add_property("power", value_text="{value} W")

        opa = thg.add_actor(actor="MYRES.MIRListener", name="OPO/OPAs")
        opa.add_description("OPO: 3000-4000 nm, up to 5 W, cw")
        opa.add_description("OPO: 1400-1600 nm, up to 15 W, cw")
        opa.add_property("signal_P", EmissionPropertyItem)
        opa.add_description("OPAs: 3000-4000 nm, up to 1 mJ, 8 ns, 20 Hz")
        opa.add_description("OPAs: 1400-1600 nm, up to 3 mJ, 8 ns, 20 Hz")
        opa.add_property("idler2_E", EmissionPropertyItem)

        self.treeView.expandRecursively(self.model.indexFromItem(thg))

    def setup_ps_lasers(self) -> None:
        ps = GroupItem("ps")
        self.model_root.appendRow(ps)

        c15 = ps.add_actor("pico3.C15Controller", name="Coheras C15")
        c15.add_description("532 nm, up to 15 W, cw")
        c15.add_property("emission_enabled", EmissionPropertyItem)
        c15.add_property("power", value_text="{value} W")

        amp = ps.add_actor("PICO2.TiSaAmp", name="Mira and power amp...")
        amp.add_description("790-820 nm, up to 3 W, cw or up to 2 nJ at 2 ps at 76 MHz")
        amp.add_property("mira", EmissionPropertyItem)
        amp.add_property("mira_power", value_text="{value} W")
        amp.add_description(
            "VIS: 505-700 nm, Signal: 1010-1400 nm, Idler: 1800-4400 nm; up to 4 nJ, 1-2 ps, 76 MHz"
        )
        amp.add_property("ApeOPO", EmissionPropertyItem)
        amp.add_description(
            "Regenerative Amplifier: 798-812 nm, up to 7 mJ, 200 ps, 20 Hz"
        )
        amp.add_property("regen", EmissionPropertyItem)
        amp.add_description(
            "Power Amplifier: 798-812 nm, up to 80 mJ at 200 ps or up to 50 mJ at 3 ps, 20 Hz"
        )
        amp.add_property("amp", EmissionPropertyItem)
        amp.add_description(
            "OPAs: 505-700 nm, up to 2 mJ, 2 ps, 20 Hz; 400 nm, up to 15 mJ, 2 ps, 20 Hz"
        )
        amp.add_property("OPA", EmissionPropertyItem)
        amp.add_property("wavelengthRange", tooltip="Frequency conversion")

        self.treeView.expandRecursively(self.model.indexFromItem(ps))

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.stop_listen()

        # Store the window geometry
        settings = QtCore.QSettings()
        settings.setValue("geometry", self.saveGeometry())

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    def setSettings(self) -> None:
        """Apply new settings, called if the settings dialog is accepted."""
        settings = QtCore.QSettings()
        self.readout_timer.setInterval(settings.value("readout_interval", 1000, int))

    def request_properties(self) -> None:
        """Request the status of all lasers."""
        self.request_item_properties(self.model_root)

    def request_item_properties(self, item: QtGui.QStandardItem) -> None:
        if isinstance(item, ActorItem):
            name, properties = item.get_request_information()
            cid = self.director.get_parameters_async(parameters=properties, actor=name)
            self.cids[cid] = item
        elif isinstance(item, GroupItem) or item == self.model_root:
            for i in range(item.rowCount()):
                child = item.child(i)
                if child is not None:
                    self.request_item_properties(child)
        else:
            raise ValueError("Invalid item")

    @pyqtSlot(Message)
    def message_received(self, message: Message) -> None:
        """Handle a message received via the Communicator."""
        actor = self.cids.get(message.conversation_id)
        log.debug(
            f"Message from {message.sender} with {message.conversation_id} and "
            f"{message.data} received."
        )
        if not actor:
            log.warning(f"Not solicited message received: {message}.")
            return
        try:
            values = self.director.generator.get_result_from_response(
                message.payload[0]
            )
        except JSONRPCError as exc:
            if exc.rpc_error.code in (NODE_UNKNOWN.code, RECEIVER_UNKNOWN.code):
                actor.set_disconnected_status()
            else:
                log.exception("Some JSONRPC Error", exc_info=exc)
        except Exception as exc:
            log.exception(
                f"Could not decode response of {message.payload} from {message.sender}.",
                exc_info=exc,
            )
        else:
            actor.set_values(values)


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(LaserOverview)

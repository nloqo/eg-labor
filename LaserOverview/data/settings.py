"""
Module for the Settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Burger
"""

from qtpy import QtWidgets

from pyleco_extras.gui_utils.base_settings import BaseSettings


class Settings(BaseSettings):
    def setup_form(self, layout: QtWidgets.QFormLayout) -> None:
        readout_interval = QtWidgets.QSpinBox()
        readout_interval.setToolTip("Readout interval.")
        readout_interval.setSuffix(" ms")
        readout_interval.setRange(100, 100_000)
        self.add_value_widget(
            "Readout interval", readout_interval, "readout_interval", 1000, int
        )

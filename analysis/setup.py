# -*- coding: utf-8 -*-
"""
Setup devices as a pip package.

Install the package with `python -m pip install -e .` while being in this folder.
That way the package is installed editable, such that the code can be changed
without reinstalling the package.

@author: THG-User
"""

import setuptools

setuptools.setup()

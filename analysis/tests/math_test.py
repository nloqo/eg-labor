# -*- coding: utf-8 -*-
"""
Testing analysis math.

Created on Sat Feb  5 10:17:45 2022

@author: moneke
"""

import pytest

import numpy as np

import analysis.math as m


class Test_invertedSum:
    def test_individual_numbers(self):
        assert m.invertedSum(4, 8, 8) == 2

    def test_array(self):
        assert m.invertedSum(np.array([4, 8, 8])) == 2

    def test_list(self):
        assert m.invertedSum([4, 8, 8]) == 2

    def test_tuple(self):
        assert m.invertedSum((4, 8, 8)) == 2

    def test_other_numbers(self):
        assert m.invertedSum(3, 3) == 1.5

    def test_array_to_number(self):
        result = [2, 8/3]
        assert m.invertedSum([4, 8], 4) == pytest.approx(result)

    def test_array_to_numbers(self):
        result = [2, 8/3]
        assert m.invertedSum([4, 8], 8, 8) == pytest.approx(result)

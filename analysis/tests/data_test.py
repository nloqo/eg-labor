# flake8 E128
# -*- coding: utf-8 -*-
"""
Testing analysis data.


Created on Fri Feb  4 16:37:37 2022

@author: moneke
"""

import pytest

import numpy as np

import analysis.data as d


class Test_binning:
    @pytest.fixture(scope="class")
    def numbers(self):
        "check whether sufficient data points are generated"
        return d.binning(np.linspace(0, 10, 50), np.zeros(50))

    def test_numbers_limits(self, numbers):
        assert np.array_equal(numbers[0], np.linspace(0, 10, 10, endpoint=False))

    def test_numbers_mean(self, numbers):
        assert np.array_equal(numbers[1], np.zeros(10))

    def test_numbers_std(self, numbers):
        assert np.array_equal(numbers[2], np.zeros(10))

    def test_numbers_count(self, numbers):
        assert np.array_equal(numbers[3], np.zeros(10) + 5)

    @pytest.fixture(scope="class")
    def log(self):
        return d.binning(np.linspace(1, 10000, 10000), np.zeros(10000), 4, log=True)

    def test_log_bins(self, log):
        assert np.array_equal(log[0], [1, 10, 100, 1000])

    @pytest.fixture(scope="class")
    def log2(self):
        return d.binning(np.linspace(1, 10000, 10000), np.zeros(10000), binsize=0.5, log=True)

    def test_log2_bins(self, log2):
        result = [
            1.00000000e00,
            3.16227766e00,
            1.00000000e01,
            3.16227766e01,
            1.00000000e02,
            3.16227766e02,
            1.00000000e03,
            3.16227766e03,
        ]
        assert log2[0] == pytest.approx(result)

    @pytest.fixture(scope="class")
    def raw(self):
        nan = np.nan
        return (
            np.linspace(0, 10, 50),
            np.array(
                [
                    nan,
                    1.49850254e-09,
                    1.63449726e-09,
                    1.63449726e-09,
                    nan,
                    1.53018131e-09,
                    2.01464272e-09,
                    nan,
                    1.59865866e-09,
                    1.59865866e-09,
                    1.60345840e-09,
                    nan,
                    nan,
                    1.68665524e-09,
                    1.50554218e-09,
                    1.30011015e-09,
                    nan,
                    1.54522076e-09,
                    nan,
                    1.67481575e-09,
                    1.90392704e-09,
                    nan,
                    1.56058016e-09,
                    nan,
                    1.56122007e-09,
                    nan,
                    1.66009636e-09,
                    1.94904518e-09,
                    nan,
                    1.81305046e-09,
                    8.77726364e-10,
                    nan,
                    1.38522682e-09,
                    nan,
                    1.38522682e-09,
                    1.70713444e-09,
                    1.70713444e-09,
                    nan,
                    1.37690716e-09,
                    1.45210428e-09,
                    1.42650528e-09,
                    nan,
                    nan,
                    -1.40794605e-11,
                    1.34234848e-09,
                    2.11671886e-09,
                    nan,
                    1.57657948e-09,
                    1.57657948e-09,
                    nan,
                ]
            ),
        )

    @pytest.fixture(scope="class")
    def bins(self, raw):
        return d.binning(*raw, bins=10)

    def test_bins_mean(self, bins):
        result = np.array(
            [
                1.58916569e-09,
                1.68553534e-09,
                1.59855194e-09,
                1.50671555e-09,
                1.67524242e-09,
                1.80739733e-09,
                1.21606000e-09,
                1.56082008e-09,
                9.18258100e-10,
                1.75662594e-09,
            ]
        )
        assert bins[1] == pytest.approx(result)

    def test_bins_std(self, bins):
        result = np.array(
            [
                6.41085258e-11,
                1.92055758e-10,
                7.40204482e-11,
                1.55377089e-10,
                1.61704654e-10,
                1.18030571e-10,
                2.39238009e-10,
                1.48710181e-10,
                6.60156843e-10,
                2.54624146e-10,
            ]
        )
        assert bins[2] == pytest.approx(result)

    def test_bins_count(self, bins):
        result = np.zeros(10) + 5
        assert np.array_equal(bins[3], result)

    @pytest.fixture(scope="class")
    def binsize(self, raw):
        return d.binning(*raw, binsize=2.2)

    def test_binsize_bins(self, binsize):
        result = np.array([0.0, 2.2, 4.4, 6.6, 8.8])
        assert binsize[0] == pytest.approx(result)

    def test_binsize_mean(self, binsize):
        result = np.array(
            [1.63913710e-09, 1.60271185e-09, 1.54384934e-09, 1.29156185e-09, 1.65305658e-09]
        )
        assert binsize[1] == pytest.approx(result)

    def test_binsize_std(self, binsize):
        result = np.array(
            [1.48848475e-10, 1.85872275e-10, 3.20630822e-10, 5.49043446e-10, 2.84262083e-10]
        )
        assert binsize[2] == pytest.approx(result)

    def test_binsize_count(self, binsize):
        result = np.array([11, 11, 11, 11, 6])
        assert binsize[3] == pytest.approx(result)

    def test_binsize_count_finite(self, raw):
        result = np.array([8, 6, 7, 7, 4])
        x, mean, std, count = d.binning(*raw, binsize=2.2, count_finite=True)
        assert count == pytest.approx(result)


class Test_runningAverage:
    @pytest.fixture(scope="class")
    def raw(self):
        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    def test_single(self, raw):
        data = d.runningAverage(raw, 1)
        assert np.array_equal(data, np.array(raw), equal_nan=True)

    def test_even_nan(self, raw):
        result = [np.nan, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5]
        data = d.runningAverage(raw, 2, buffer=np.nan)
        assert np.array_equal(data, np.array(result), equal_nan=True)

    def test_odd_0(self, raw):
        result = np.array([0, 2, 3, 4, 5, 6, 7, 8, 9, 0])
        data = d.runningAverage(raw, 3, buffer=0)
        assert np.array_equal(data, result, equal_nan=True)

    def test_large_nan(self, raw):
        result = [np.nan, np.nan, 3, 4, 5, 6, 7, 8, np.nan, np.nan]
        data = d.runningAverage(raw, 5, buffer=np.nan)
        assert np.array_equal(data, np.array(result), equal_nan=True)

    def test_non_linear_nan(self):
        raw = [1, 2, 4, 8, 16, 32, 64, 128]
        data = d.runningAverage(raw, 5, buffer=np.nan)
        result = [np.nan, np.nan, 6.2, 12.4, 24.8, 49.6, np.nan, np.nan]
        assert np.array_equal(data, np.array(result), equal_nan=True)

    def test_non_linear_Number(self):
        raw = [1, 2, 4, 8, 16, 32, 64, 128]
        data = d.runningAverage(raw, 5)
        result = [7 / 3, 3.75, 6.2, 12.4, 24.8, 49.6, 60, 224 / 3]
        assert np.array_equal(data, np.array(result), equal_nan=True)

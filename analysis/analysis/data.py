# -*- coding: utf-8 -*-
"""
Data methods
============
Analyzing data


binning
    Calculate the statistics of binned data.
downsampling
    Reduce the number of samples.
runningAverage
    Calculate a running average.


Created on Wed Jan 26 17:07:25 2022 by Benedikt Burger
"""

import math
from pathlib import Path
from typing import Any, Callable, Iterable, Optional

import numpy as np
from numpy.typing import ArrayLike, NDArray

from pyleco_extras.gui.data_logger.data.load_file import load_datalogger_file


def filter_data(
    x_list: ArrayLike, y_list: ArrayLike
) -> tuple[NDArray, NDArray]:
    """Filter data such that no NaNs are present anymore."""
    raw_x = np.array(x_list)
    raw_y = np.array(y_list)
    index = np.isfinite(raw_x) * np.isfinite(raw_y)
    return raw_x[index], raw_y[index]


def binning(
    x: ArrayLike,
    y: ArrayLike,
    bins: int = 10,
    binsize: Optional[float] = None,
    log: bool = False,
    count_finite: bool = False,
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Calculate the statistics of binned data.

    NaN values are ignored.
    If `binsize` is used, the rightmost bin might contain less data points than
    the rest of the bins.

    Parameters
    ----------
    x : array-like
        The base data which should contain the bins later on.
    y : array-like
        The data to put into the bins.
    bins : int, optional
        Number of bins between minimum and maximum x values.
    binsize : float, optional
        Size of a single bin. For logarithmic bins, the size is in decades.
        If this parameter is set, `bins` is ignored.
    log : bool, optional
        Use an exponentially growing binsize (base 10).
    count_finite : bool, optional
        Count the finite numbers only.

    Returns
    -------
    bins : array
        Lower border of the bins.
    mean : array
        Mean of the values in the bins.
    std : array
        Standard deviation of the values in the bins.
    count : array
        Count of values in the bins.
    """
    if type(x) is not np.array:
        x = np.array(x)
    if type(y) is not np.array:
        y = np.array(y)
    start = np.nanmin(x)
    stop = np.nanmax(x)
    if log:
        assert min(start, stop) > 0, "Logarithm does not work in the negative."
        start, stop = np.log10([start, stop])
        if binsize:
            bins = math.ceil((stop - start) / binsize)
            stop = start + bins * binsize
        limits = np.logspace(start, stop, bins, endpoint=False)
    else:
        if binsize:
            bins = math.ceil((stop - start) / binsize)
            stop = start + bins * binsize
        limits = np.linspace(start, stop, bins, endpoint=False)
    # i = 0
    # indices = (limits[i] <= x) * (x <= limits[i+1])
    # bins = [np.mean((limits[i], limits[i+1]))]
    # mean = [np.nanmean(y[indices])]
    # std = [np.nanstd(y[indices])]
    # count = [len(y[indices])]
    mean = []
    std = []
    count = []
    for i in range(len(limits) - 1):
        indices = (limits[i] <= x) * (x < limits[i + 1])
        mean.append(np.nanmean(y[indices]))
        std.append(np.nanstd(y[indices]))
        if count_finite:
            count.append(np.count_nonzero(np.isfinite(y[indices])))
        else:
            count.append(len(y[indices]))
    indices = limits[-1] <= x
    mean.append(np.nanmean(y[indices]))
    std.append(np.nanstd(y[indices]))
    if count_finite:
        count.append(np.count_nonzero(np.isfinite(y[indices])))
    else:
        count.append(len(y[indices]))
    return limits, np.array(mean), np.array(std), np.array(count)


def downsampling(
    data: ArrayLike,
    size: int = 1,
    method: Callable[[ArrayLike], float] = np.nanmean,
    *args,
    **kwargs,
) -> np.ndarray:
    """
    Reduce the number of samples.

    Parameters
    ----------
    data : array-like
        The data of samples.
    size : int, optional
        Number of samples to unite into one.
    method : function, optional
        The function generating the downsampled data.
    *args, **kwargs
        Arguments handed over to the downsampling method.

    Returns
    -------
    Array of downsampled data.
    """
    new = []
    i = 0
    while i < len(data):
        new.append(method(data[i: i + size], *args, **kwargs))
        i += size
    return np.array(new)


def load_Datalogger_files(
    names: str | Iterable[str],
    folder: str | Path = "M:/Measurement Data/DataLogger",
    printing: bool = False,
) -> tuple[
    dict[str, str],
    dict[str, dict[str, list[float]]],
    dict[str, dict[str, str | float | Any]],
]:
    """Daten mit der Namensliste `names` laden und als dicts headers, datas, metas zurückgeben.

    Die Daten liegen im Ordner DataLogger, einmal als txt und einmal als gepicklte
    Datei mit dem Timestamp als Dateiname: 2021-07-27T14.51.34.txt, 2021-07-27T14.51.34.pkl.
    Neuere Dateinamen sind mit Unterstrich, zur einfacheren Markierung mittels Doppelklick.
    Die gepickelten Daten sind zuerst der Header, dann das Daten Dictionary und anschließend,
    bei neueren Dateien, ein Dictionary mit Meta-Daten.
    """
    extensions = [".json", ".pkl", ".txt"]
    if isinstance(names, str):
        names = (names,)
    headers = {}
    data = {}
    metas = {}
    for name in names:
        h, d, m = load_datalogger_file(Path(folder) / name, printing=printing)
        for ext in extensions:
            if name.endswith(ext):
                name = name.replace(ext, "")
                break
        headers[name] = h
        data[name] = d
        metas[name] = m
    return headers, data, metas


def runningAverage(
    data: ArrayLike,
    size: int,
    *args,
    method: Callable[[ArrayLike], float] = np.nanmean,
    buffer: Optional[float] = None,
    **kwargs,
):
    """
    Calculate a running average.

    Parameters
    ----------
    data : array-like
        The data.
    *args, **kwargs
        Arguments for the method.
    size : int
        Number of samples to take into consideration.
    method : function, optional
        Filtering method how to generate the data point.
        The default is np.nanmean.
    buffer : number
        Value to put at the beginning / end of the array, where not enough
        samples are present. If None, calculate the average with the reduced
        number of samples at the ends.

    Returns
    -------
    Array with the newly calculated data.
    """
    new = np.zeros_like(data, dtype=float)
    front = size // 2
    back = size - front  # includes the center.
    if buffer is not None:
        new[:front] = buffer
        for i in range(front, len(data) - back + 1):
            new[i] = method(data[i - front: i + back], *args, **kwargs)
        new[len(data) - back + 1:] = buffer
    else:
        for i in range(len(data)):
            new[i] = method(
                data[max(0, i - front): min(i + back, len(data))], *args, **kwargs
            )
    return new

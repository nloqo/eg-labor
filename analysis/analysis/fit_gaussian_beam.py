from typing import Optional

import numpy as np
from scipy.ndimage import center_of_mass, sum
from scipy.optimize import curve_fit as fit

from . import math


def get_center_index(image: np.ndarray, threshold: float) -> tuple[int, int]:
    _image = image.copy()
    _image[_image < threshold] = 0  # set values below a threshold to zero
    y0, x0 = center_of_mass(_image)
    y0i = int(y0)  # type: ignore
    x0i = int(x0)  # type: ignore
    return x0i, y0i


def fit_gaussian_cuts(
    image: np.ndarray,
    center: Optional[tuple[float, float]] = None,
    amplitude: Optional[float] = None,
    radius: float = 100,
    offset: float = 0,
    zero_offset: bool = False,
    **kwargs,
) -> tuple[
    dict[int | str, tuple],  # par
    dict[int | str, list[list]],  # cov
    dict[int, np.ndarray],  # obl
    dict[int, tuple[int, int, float]],  # oblPar
]:
    """
    Lege Schnitte durch das Bild und passe Gaußkurven an

    Parameters
    ----------
    image : array-like
        Array of the image data.
    center: tuple of floats or None
        Center of the image (x, y) in pixels. If it is None, the center is calculated.
    amplitude: float or None
        Initial guess of the amplitude. If None, the max pixel value is used.
    radius: float
        Initial guess of the beam radius in pixels.
    offset: float
        Initial offset value.
    zero_offset: bool
        Fix the offset to 0.

    Returns
    -------
    par : dict
        Dictionary of the fit parameters for different angles.
    cov : dict
        Dictionary of the fit covariances.
    obl : dict
        Array with pairs of x-Position and image value.
    oblPar
        Dictionary of the parameters.
    start, stop
        Coordinates where the lines start and stop.
    """
    # Parameter für schiefe Linien: Schritte in x-Richtung und in y-Richtung,
    # Faktor von x-Pixel zu ganzen Pixel
    oblPar = {
        0: [1, 0, 1],
        30: [2, 1, np.sqrt(5) / 2],
        45: [1, 1, np.sqrt(2)],
        60: [1, 2, np.sqrt(5)],
        90: [0, 1, 1],
        120: [1, -2, np.sqrt(5)],
        135: [1, -1, np.sqrt(2)],
        150: [2, -1, np.sqrt(5) / 2],
    }
    obl = {}  # Schiefe Linien
    start = {}
    par = {}  # Parameter
    cov = {}  # Kovarianzen

    xmax = len(image[0])
    ymax = len(image[:, 0])
    image_max = np.nanmax(image)
    image[np.isnan(image)] = image_max
    if center is None:
        x0i, y0i = get_center_index(image, threshold=0.3 * image_max)
    else:
        x0i, y0i = center
    if amplitude is None:
        amplitude = image_max

    # Fit oblique lines.
    for key in oblPar.keys():
        if key == 0 or key == 90:
            # Skip them, as they come below
            continue
        data = []
        xkey = oblPar[key][0]
        ykey = oblPar[key][1]
        steps = int(min(x0i / xkey, (y0i if ykey > 0 else y0i - ymax + 1) / ykey))
        x = int(x0i - steps * xkey)
        y = int(y0i - steps * ykey)
        start = [x, y]
        while x < xmax and 0 <= y and y < ymax:
            data.append([x, image[y, x]])
            x += xkey
            y += ykey
        obl[key] = np.array(data)

        p0_limit = -1 if zero_offset else None

        try:
            par[key], cov[key] = fit(
                math.gaussian,
                obl[key][:, 0],
                obl[key][:, 1],
                p0=(amplitude, x0i, radius, offset)[:p0_limit],
            )
        except Exception:
            print(key, start, [x, y], xkey, ykey, data)
            raise

    # Fit horizontal / vertical lines.
    obl[0] = np.array([range(xmax), image[y0i]]).transpose()  # type: ignore
    obl[90] = np.array([range(ymax), image[:, x0i]]).transpose()  # type: ignore
    par[0], cov[0] = fit(
        math.gaussian,
        range(xmax),
        image[y0i],  # type: ignore
        p0=(amplitude, x0i, radius, offset)[:p0_limit],
    )
    par[90], cov[90] = fit(
        math.gaussian,
        range(ymax),
        image[:, x0i],  # type: ignore
        p0=(amplitude, y0i, radius, offset)[:p0_limit],
    )

    # Ellipse anpassen
    ellipseData = []
    for key in par.keys():
        ellipseData.append([key / 180 * np.pi, abs(par[key][2]) * oblPar[key][2]])
    ellipseData = np.array(ellipseData)
    par["ellipse"], cov["ellipse"] = fit(
        math.ellipse_radius,
        ellipseData[:, 0],
        ellipseData[:, 1],
        p0=(par[0][2], par[90][2], 0),
    )
    return par, cov, obl, oblPar


def fit2dGaussian(
    image: np.ndarray,
    amplitude: Optional[float] = None,
    radius: float = 100,
    offset: Optional[float] = None,
    zero_offset: bool = False,
) -> tuple[list, list]:
    """
    Fit a 2D gaussian to the image and return the fit parameters.

    image : array
        The image data.
    amplitude : float or None, optional
        The initial guess of the amplitude or max value.
    radius : float, optional
        Initial guess of the radius in pixel.

    Returns fit parameters
    -------
    Amplitude
    x0, y0 : central point coordinates.
    sigmaX, sigmaY : standard devaition in the two axes.
    phi : angle between the x-axis and the axis of sigmaX.
    offset
    """

    def gauss2dAdjusted(M, *args):
        """Transform the data to be ready for a 2D fit."""
        x, y = M
        return math.gaussian2D(x, y, *args)

    max_image = np.max(image)
    x0i, y0i = get_center_index(image, threshold=0.5 * max_image)
    X, Y = np.meshgrid(range(len(image[0])), range(len(image[:, 0])))
    xdata = np.vstack((X.ravel(), Y.ravel()))
    if amplitude is None:
        amplitude = max_image
    p0_limit = -1 if zero_offset else None
    if offset is None and not zero_offset:
        offset = np.min(image)
    return fit(
        gauss2dAdjusted,
        xdata,
        image.ravel(),
        p0=(amplitude, x0i, y0i, radius, radius, 0, offset)[:p0_limit],
    )


def max_from_pixels(img: np.ndarray, energy: float, pixel_length: float) -> float:
    """Get the peak fluence based on the pixel values."""
    summe = sum(img - np.min(img))
    maximum = (np.max(img) - np.min(img)) * energy / summe / pixel_length**2 * 1e4**2
    return maximum

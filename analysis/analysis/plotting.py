# -*- coding: utf-8 -*-
"""
Often used plotting functions
=============================

Plotting functions
------------
configureMatplotlib
    Define defaults like saveFormat etc.
configurePlot
    Default plotting options.
initFigure
    Initialize a figure and return the axes object.

Created on Wed Jan 26 17:07:25 2022 by Benedikt Burger
"""

import locale
from typing import Any, cast, Optional

import matplotlib
from matplotlib.axes import Axes
import matplotlib.pyplot as plt
import numpy as np


def configureMatplotlib(
    fontSize: float = 11,
    fontFamily: str = "serif",
    saveFormat: str = "png",
    language: str = "en",
    width: Optional[float] = None,
    aspectRatio: float = 4 / 3,
    figsize: Optional[tuple[float, float]] = None,
    resetFirst: bool = False,
    params: Optional[dict[str, Any]] = None,
) -> None:
    """
    Configure the matplotlib runtime configuration (rc).

    Parameters
    ----------
    fontSize
        Size of font in point.
    fontFamily
        Family of fonts to show: 'serif', 'sans-serif', 'monospace', 'fantasy'
        or 'cursive'.
    saveFormat
        Default file format to save to, if no ending or format is specified in
        savefig.
    language
        Whether the axes formatter uses the german ('de') or default english
        formatting. Relevant for decimal sign.
    width
        The default figure width in cm. overrides figsize
    aspectRatio
        The default figure aspect ratio, requires width to be set.
    figsize : tuple
        The default figsize in inch. It is overridden by width / aspect ratio.
    resetFirst
        Reload the matplotlib defaults before doing any other changes.
    params
        Dictionary with more rcParams and their values.

    See https://matplotlib.org/stable/tutorials/introductory/customizing.html
    for matplotlib.rc(group, **kwargs), matplotlib.rc_context(**kwargs)
    """
    if params is None:
        params = {}
    if resetFirst:
        matplotlib.rcdefaults()
    # Define the figure.
    matplotlib.rcParams["savefig.format"] = saveFormat
    matplotlib.rcParams["savefig.bbox"] = "tight"
    if width is not None and aspectRatio is not None:
        figsize = (width / 2.54, width / 2.54 / aspectRatio)
    if figsize is not None:
        matplotlib.rcParams["figure.figsize"] = figsize
    # Achsenformatierung auf Deutsch
    if language == "de":
        # Festlegung der Systemsprache
        # Windows: "deu_deu", Linux: "de_DE.utf8"
        locale.setlocale(locale.LC_ALL, "deu_deu")
    # Achsenformatierung gemäß Systemsprache
    matplotlib.rcParams["axes.formatter.use_locale"] = bool(language == "de")
    # Define the font.
    matplotlib.rcParams["font.size"] = fontSize
    matplotlib.rcParams["font.family"] = fontFamily
    for key, value in params.items():
        matplotlib.rcParams[key] = value


def configurePlot(
    log: str = "",
    leg: bool = True,
    name: str = "",
    xname: str = "",
    minor: bool = True,
    axes: Optional[Axes] = None,
    formats: tuple[None | str, ...] = (None,),
    savefig_args: Optional[dict[str, Any]] = None,
    tick_args: Optional[dict[str, Any]] = None,
    show_plot: bool = True,
    **kwargs,
) -> None:
    """
    Configure the standard plot with ticks all around, which point inwards.

    Requires matplotlib as plt.

    Parameters
    -----------
    log : str, optional
        Names of axes for which a logarithmic scale should be applied.
    leg : bool, optional
        Whether a legend should be shown. The default is True.
    name : str, optional
        Filename to save the plot to.
    xname : str, optional
        Do nothing. It is a simple method to deactivate saving: change 'name'
        to 'xname'.
    minor : bool, optional
        Show minor ticks. Default is True.
    axes : axes object, optional
        axes object which should be modified. If not supplied, the current
        object is taken.
    formats : iterable of str, optional
        If specified, the file is saved in each format specified and the
        appropriate ending is appended to the filename.
    savefig_args : dict, optional
        Arguments for savefig, except format
    tick_args : dict, optional
        Arguments for the ticks.
    kwargs : dict, optional
        Further values are handed to the legend.
    """
    if axes is None:
        axes = cast(Axes, plt.gca())  # Define the current axes object

    if minor:  # Minorticks
        axes.minorticks_on()

    if tick_args is None:
        tick_args = {}
    for key, value in (
        ("which", "both"),
        ("direction", "in"),
        ("top", True),
        ("right", True),
    ):
        tick_args.setdefault(key, value)
    axes.tick_params(**tick_args)  # axis="both" is default.

    # Logarithmic Axes
    if log != "":
        assert log in ("x", "y", "xy", "yx", "both"), "invalid axis parameter"
        if log == "both":
            log = "xy"
    if "x" in log:
        axes.set_xscale("log")
    if "y" in log:
        axes.set_yscale("log")

    if leg:  # Legend
        axes.legend(**kwargs)

    plt.tight_layout()  # Fit the plot better into the present space.

    if name != "":  # Save file
        if savefig_args is None:
            savefig_args = {}
        savefig_args.setdefault("dpi", "figure")
        for form in formats:
            plt.savefig(
                name if form is None else ".".join((name, form)),
                format=form,
                **savefig_args,
            )
    if show_plot:
        plt.show()


def initFigure(
    figsize: Optional[tuple[float, float]] = None,
    dpi: Optional[float] = None,
    width: Optional[float] = None,
    aspectRatio: float = 4 / 3,
) -> Axes:
    """
    Initialize a figure and return the axes object.

    Width is in cm. figsize is default in inches.
    """
    if width is not None:
        figsize = (width / 2.54, width / 2.54 / aspectRatio)
    plt.figure(figsize=figsize, dpi=dpi)
    return plt.gca()


def plot_single(
    data: dict[str, list[float]],
    xKey: str,
    yKeys: Optional[list[str]] = None,
    y2Keys: Optional[list[str]] = None,
    y3Keys: Optional[list[str]] = None,
    marker: str = "+",
    ls: str = "-",
    show_plot: bool = True,
    units: Optional[dict[str, str]] = None,
    name: str = "",
    title: str = "",
    figsize: tuple[float, float] = (18, 6),
    xlim: dict[str, float | None] = dict(left=None, right=None),
    ylim: dict[str, float | None] = dict(bottom=None, top=None),
    ylim2: dict[str, float | None] = dict(bottom=None, top=None),
    ylim3: dict[str, float | None] = dict(bottom=None, top=None),
) -> Axes:
    """Plot a single plot with several data in several axes."""
    if units is None:
        units = {}
    plt.figure(figsize=figsize)
    ax: Axes = plt.gca()
    if yKeys is None:
        yKeys = []
    for yKey in yKeys:
        if xKey == "time":
            ax.plot(
                np.array(data[xKey]) - data[xKey][0],
                data[yKey],
                label=f"{yKey} ({units.get(yKey, '')})",
                marker=marker,
                ls=ls,
            )
        elif xKey:
            ax.plot(
                data[xKey],
                data[yKey],
                label=f"{yKey} ({units.get(yKey, '')})",
                marker=marker,
                ls=ls,
            )
        else:
            ax.plot(
                data[yKey],
                label=f"{yKey} ({units.get(yKey, '')})",
                marker=marker,
                ls=ls,
            )
        ax.set_ylim(**ylim)  # type: ignore
        # for pl() use the last line of the cell
    number = len(yKeys)
    if y2Keys:
        ax2: Axes = ax.twinx()  # type: ignore
        # legend separator between entries of left and right axis
        ax.plot(np.nan, np.nan, label="axis separator", ls="", marker="")
        for yKey in y2Keys:
            if xKey == "time":
                ax2.plot(
                    np.array(data[xKey]) - data[xKey][0],
                    data[yKey],
                    label=f"{yKey} ({units.get(yKey, '')})",
                    marker=marker,
                    ls=ls,
                    color=f"C{number}",
                )
            elif xKey:
                ax2.plot(
                    data[xKey],
                    data[yKey],
                    label=f"{yKey} ({units.get(yKey, '')})",
                    marker=marker,
                    ls=ls,
                    color=f"C{number}",
                )
            else:
                ax2.plot(
                    data[yKey],
                    label=f"{yKey} ({units.get(yKey, '')})",
                    marker=marker,
                    ls=ls,
                    color=f"C{number}",
                )
            ax.plot(
                np.nan,
                np.nan,
                label=f"{yKey} ({units.get(yKey, '')})",
                marker=marker,
                ls=ls,
                color=f"C{number}",
            )
            number += 1
        ax2.set_ylim(**ylim2)  # type: ignore
        configurePlot(axes=ax2, log="", leg=False)
    if y3Keys:
        ax3: Axes = ax.twinx()  # type: ignore
        ax3.spines.right.set_position(("axes", 1.05))
        # legend separator between entries of left and right axis
        ax.plot(np.nan, np.nan, label="axis separator", ls="", marker="")
        for yKey in y3Keys:
            if xKey == "time":
                ax3.plot(
                    np.array(data[xKey]) - data[xKey][0],
                    data[yKey],
                    label=f"{yKey} ({units.get(yKey, '')})",
                    marker=marker,
                    ls=ls,
                    color=f"C{number}",
                )
            elif xKey:
                ax3.plot(
                    data[xKey],
                    data[yKey],
                    label=f"{yKey} ({units.get(yKey, '')})",
                    marker=marker,
                    ls=ls,
                    color=f"C{number}",
                )
            else:
                ax3.plot(
                    data[yKey],
                    label=f"{yKey} ({units.get(yKey, '')})",
                    marker=marker,
                    ls=ls,
                    color=f"C{number}",
                )
            ax.plot(
                np.nan,
                np.nan,
                label=f"{yKey} ({units.get(yKey, '')})",
                marker=marker,
                ls=ls,
                color=f"C{number}",
            )
            number += 1
        ax3.set_ylim(**ylim3)  # type: ignore
        configurePlot(axes=ax3, log="", leg=False)
    ax.set_title(f"{name}" + f": {title}")
    ax.set_xlabel(f"{xKey} ({units.get(xKey, '')})" if xKey else "index")
    ax.set_xlim(**xlim)  # type: ignore
    ax.set_ylim(**ylim)  # type: ignore
    if show_plot:
        configurePlot(xname=f"daDataLogs/{name}-{xKey}.png", log="", leg=True, axes=ax)
    return ax

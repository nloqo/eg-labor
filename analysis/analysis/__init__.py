# -*- coding: utf-8 -*-
"""
Library for data analysis.

general modules
---------------
data
    Methods analysing data.
general
    Importing the methods from the general modules.
math
    Mathematical functions, also related to physics.
plotting
    Used for plotting the data.


special modules
---------------
gaussianBeams
    Methods calculating gaussian beams.
"""

# -*- coding: utf-8 -*-
"""
Often used mathematical functions
=================================

recommendation:
    `from analysis import math as mf`

Mathematical functions
----------------------
ellipse
    Coordinates of an ellipse.
exponential
    An exponential function, especially for fitting.
gaussian
    A gaussian profile with the maximum as amplitude.
gaussian2D
    A gaussian profile over 2D data with elliptical shape and an angle.
linear
    A linear function.
lorentzian
    A lorentzian curve.
voigt
    A voigt-profile
voigtMax
    Returns the maximum for a voigt profile without offset.

Helpful calculations
--------------------
invertedSum
    The inversion of the sum of inverted numbers.

Physical conversions
--------------------
wavelengthSum
    Return the wavelength of the combination of two wavelengths.

Created on Wed Jan 26 17:07:25 2022 by Benedikt Burger
"""

from functools import wraps
from typing import Callable, Optional

import numpy as np
from numpy.typing import ArrayLike
from scipy.special import voigt_profile


"# Mathematical functions in alphabetic order (except for polynomials)"


def ellipse(
    angle: ArrayLike,
    x0: float = 0,
    y0: float = 0,
    r1: float = 1,
    r2: Optional[float] = None,
    phi: float = 0,
) -> tuple[ArrayLike, ArrayLike]:
    """
    Return coordinates of for `angle` of an ellipse.

    Parameters
    ----------
    angle : array-like
        Angles for which to calculate the coordinates.
    x0, y0
        Center point.
    r1, r2
        Radii. If `r2` is None, it calculates a circle with radius r1.
    phi
        Angle between r1 and x-axis.
    """
    if r2 is None:
        r2 = r1
    x = x0 + r1 * np.cos(phi) * np.cos(angle) - r2 * np.sin(phi) * np.sin(angle)
    y = y0 + r1 * np.sin(phi) * np.cos(angle) + r2 * np.cos(phi) * np.sin(angle)
    return (x, y)


def ellipse_radius(angle: ArrayLike, r1: float, r2: float, angle0: float) -> ArrayLike:
    """Radius of the ellipse with radius r1, r2 and an angle0 from the x axis."""
    return np.sqrt(
        r1**2 * np.cos(angle + angle0) ** 2 + r2**2 * np.sin(angle + angle0) ** 2
    )


def exponential(
    x: ArrayLike, factor: float = 1, exponent: float = 1, offset: float = 0
) -> ArrayLike:
    """An exponential function with offset.

    .. code::

        factor * x**exponent + offset
    """
    return factor * x**exponent + offset


def gaussian(
    x: ArrayLike,
    amplitude: float = 1,
    x0: float = 0,
    sigma: float = 1,
    offset: float = 0,
) -> ArrayLike:
    """
    A gaussian curve with the amplitude as maximum minus offset.

    For ``amplitude = 1/sigma/np.sqrt(2*np.pi)`` it is a density distribution.
    FWHM = ``2 * np.sqrt(2 * np.log(2)) * sigma``

    .. code::

        amplitude * np.exp(-0.5 * ((x - x0) / sigma)**2) + offset
    """
    return amplitude * np.exp(-0.5 * ((x - x0) / sigma) ** 2) + offset


def gaussian2D(
    x: float,
    y: float,
    amplitude: float,
    x0: float,
    y0: float,
    sigmaX: float,
    sigmaY: float,
    phi: float = 0,
    offset: float = 0,
) -> float:
    """
    Calculate a Gaussian with elliptical shape over a two dimensional field.

    Parameters
    ----------
    x, y
        Data of the position.
    amplitude
        Amplitude of the curve.
    x0, y0
        Central point coordinates.
    sigmaX, sigmaY
        standard deviation in the two axes.
    phi
        Angle between the x-axis and the axis of sigmaX.
    offset
        Additional offset.
    """
    a = np.cos(phi) ** 2 / (2 * sigmaX**2) + np.sin(phi) ** 2 / (2 * sigmaY**2)
    b = -np.sin(2 * phi) / (4 * sigmaX**2) + np.sin(2 * phi) / (4 * sigmaY**2)
    c = np.sin(phi) ** 2 / (2 * sigmaX**2) + np.cos(phi) ** 2 / (2 * sigmaY**2)
    exponent = -a * (x - x0) ** 2 + 2 * b * (x - x0) * (y - y0) - c * (y - y0) ** 2
    return amplitude * np.exp(exponent) + offset


def linear(x: ArrayLike, slope: float = 1, offset: float = 0) -> ArrayLike:
    """A linear function.

    .. code::

        slope * x + offset
    """
    return slope * x + offset


def lorentzian(
    x: ArrayLike,
    amplitude: float = 1,
    x0: float = 0,
    gamma: float = 1,
    offset: float = 0,
) -> ArrayLike:
    """
    A lorentzian curve.

    For `amplitude = 1 / np.pi / gamma` it is a probability density.
    gamma is half width at half max. FWHM is 2 * gamma.
    """
    return amplitude * gamma**2 / ((x - x0) ** 2 + gamma**2) + offset


def power(
    x: ArrayLike, factor: float = 1, exponent: float = 1, offset: float = 0
) -> ArrayLike:
    """``factor * x**exponent + offset``"""
    return factor * x**exponent + offset


def voigt(
    x: ArrayLike,
    factor: float = 1,
    mu: float = 0,
    sigma: float = 0,
    gamma: float = 0,
    offset: float = 0,
) -> ArrayLike:
    """
    Voigt profile.

    Parameters
    ----------
    factor
        Amplitude factor of the profile.
    mu
        Center of the profile.
    sigma
        Gaussian standard deviation. FWHM is np.sqrt(8 * np.log(2)) * sigma.
    gamma
        Lorentzian half width of half max. FWHM is 2 * gamma.
    offset
        Offset.
    """
    return factor * voigt_profile(x - mu, sigma, gamma) + offset


def voigtMax(
    factor: float, mu: float = 0, sigma: float = 0, gamma: float = 0, offset: float = 0
) -> float:
    """Amplitude of a Voigt profile."""
    return factor * voigt_profile(0, sigma, gamma)


def voigtWidth(
    factor: float, mu: float = 0, sigma: float = 0, gamma: float = 0, offset: float = 0
) -> float:
    """FWHM of a Voigt profile."""
    # J. Olivero and R. Longbothum. Empirical fits to the Voigt line width:
    # A brief review. J. Quant. Spectrosc. Radiat. Transf. 17, 233–236 (1977)
    # 0.5346*coll_fwhm + np.sqrt(0.2166*coll_fwhm**2 + doppler_fwhm**2)
    return 1.0692 * gamma + np.sqrt(0.8664 * gamma**2 + 8 * np.log(2) * sigma**2)


"## Polynomials in order of their order"


def poly2(x: ArrayLike, b: float = 0, a: float = 0, offset: float = 0) -> ArrayLike:
    """Second order polynomial: ``b * x**2 + a * x + offset``."""
    return b * x**2 + a * x + offset


def poly3(
    x: ArrayLike, c: float = 0, b: float = 0, a: float = 0, offset: float = 0
) -> ArrayLike:
    """Third order polynomial: ``c * x**3 + b * x**2 + a * x + offset``."""
    return c * x**3 + b * x**2 + a * x + offset


def poly4(
    x: ArrayLike,
    d: float = 0,
    c: float = 0,
    b: float = 0,
    a: float = 0,
    offset: float = 0,
) -> ArrayLike:
    """Fourth order polynomial: ``d * x**4 + c * x**3 + b * x**2 + a * x + offset``."""
    return d * x**4 + c * x**3 + b * x**2 + a * x + offset


def poly5(
    x: ArrayLike,
    e: float = 0,
    d: float = 0,
    c: float = 0,
    b: float = 0,
    a: float = 0,
    offset: float = 0,
) -> ArrayLike:
    """Fifth order polynomial: ``e * x**5 + d * x**4 + c * x**3 + b * x**2 + a * x + offset``."""
    return e * x**5 + d * x**4 + c * x**3 + b * x**2 + a * x + offset


def polyN(x: ArrayLike, *factors: float, offset: float = 0) -> ArrayLike:
    """Variable length polynomial.

    .. code::

        polyN(x, 5, 4, 3, 2, 1, offset=0) == 5*x**5 + 4*x**4 + 3*x**3 + 2*x**2 + 1*x + 0


    :param x: Variable of the polynomial.
    :param \\*factors: List of factors in descending order.
    :param offset: Zero order offset.
    """
    value = offset
    order = len(factors)
    for count, factor in enumerate(factors):
        value += factor * x ** (order - count)
    return value


"# Modify functions"


def add_x0(function: Callable) -> Callable:
    """Modify a function to use `x-x0` instead of `x`."""

    @wraps(function)
    def modified_function(x, x0, *args, **kwargs):
        return function(x - x0, *args, **kwargs)

    return modified_function


"# Helpful calculations"


def invertedSum(array: ArrayLike, *numbers) -> ArrayLike:
    """
    The inversion of the sum of inverted numbers.

    Parameters
    ----------
    array : array-like or number
        array or number to which the other numbers should be added. If no
        numbers are given, add the members of the array to each other.
    *numbers : array-like
        The additional numbers to take into consideration.
    """
    if isinstance(array, list) or isinstance(array, tuple):
        array = np.array(array)
    if len(numbers) == 0:
        return 1 / (np.sum(1 / array))
    elif len(numbers) == 1:
        return 1 / (1 / array + 1 / numbers[0])
    else:
        return 1 / (1 / array + np.sum(1 / np.array(numbers)))


"# Physical conversions"


def wavelengthSum(wl1: float, wl2: float) -> float:
    """
    The wavelength of the combination of two wavelengths.

    Use negative wavelengths for subtraction.
    """
    return 1 / (1 / wl1 + 1 / wl2)

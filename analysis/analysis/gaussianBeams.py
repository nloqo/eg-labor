# -*- coding: utf-8 -*-
"""
Methods for calcucalting gaussian beams.

methods
-------
phi
    Spatial phase.
R
    Radius of curvature.
theta
    Half divergence angle.
w0
    Beam waist from Rayleigh length.
w_z
    Beam radius depending on z.
zR
    Rayleigh length depending on the beam waist.
zeta
    Gouy phase.


Created on Fri Jan 28 10:08:26 2022 by Benedikt Burger
"""

from typing import Optional

import numpy as np
from numpy.typing import ArrayLike


# Definitions of the beam itself
def phi(z: ArrayLike, w0: float, lbd: float = 1064e-9) -> ArrayLike:
    """Calculate spatial phase variation."""
    return -np.arctan(lbd * z / np.pi / w0**2)


def R(
    z: ArrayLike,
    zR: Optional[float] = None,
    w0: Optional[float] = None,
    lbd: float = 1064e-9,
):
    """Radius of curvature at position `z`."""
    if zR is None:
        zR = w0**2 * np.pi / lbd
    return z * (1 + zR**2 / z**2)


def theta(lbd: float, w0: float, M2: float = 1) -> float:
    """Half divergence angle."""
    return M2 * lbd / np.pi / w0


def w0(zR: float, lbd: float = 1064e-9) -> float:
    """Beam waist (1/e^2 radius) from Rayleigh length and wavelength."""
    return np.sqrt(lbd * zR / np.pi)


def w_z(
    z: ArrayLike,
    zR: Optional[float] = None,
    z0: float = 0,
    lbd: float = 1064e-9,
    M2: float = 1,
    w0: Optional[float] = None,
) -> ArrayLike:
    """
    Calculate the beam radius (1/e^2).

    Beam radius (1/e^2) depending on place `z`, Rayleigh length `zR`,
    focus position `z0` and wavelength `lbd`.

    All units have to be the same, for example m.
    """
    if zR is None:
        zR = w0**2 * np.pi / lbd
    return np.sqrt(lbd * zR / np.pi) * np.sqrt(1 + (M2 * (z - z0) / zR) ** 2)


def zR(w0: float, lbd: float = 1064e-9) -> float:
    """
    Calculate the Rayleigh length - distance from the focus, where the radius increased to sqrt(2).

    Parameters
    ----------
    w0
        Beam waist = 1/e^2 radius in the focus.
    lbd
        Wavelength
    """
    return w0**2 * np.pi / lbd


def zeta(z: float, zR: float) -> float:
    """Gouy-phase for position `z` and Rayleigh length `zR`."""
    return np.arctan(z / zR)


# Utilities
def peak_fluence(energy: float, xsigma: float, ysigma: Optional[float] = None) -> float:
    """Calculate peak fluence (or power) for a gaussian profile.

    :param energy: Total energy in J or total power in W.
    :param sigma: standard deviation in x/y direction in µm.
    :return: Peak fluence in J/cm^2 or peak power in W/cm^2
    """
    # Peak is twice average
    # sigma is half of radius w
    # transfer from µm to cm
    gf = abs(
        2
        * energy
        / np.pi
        / xsigma
        / (xsigma if ysigma is None else ysigma)
        / 4
        * 1e4**2
    )
    return gf

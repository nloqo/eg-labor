# -*- coding: utf-8 -*-
"""
All my definitions, which are necessary for data analysis
Created on Fri Jan 21 11:05:11 2022

@author: kinder

Pakete
======
"""

import matplotlib  # Mathebibliothek
import matplotlib.pyplot as plt  # Plotten
import matplotlib.ticker as tic  # Ticks für Plots
import matplotlib.lines as lines
import numpy as np  # Mathezeug
import os  # Betriebssystemzeug
import pickle  # Zum "einlegen" von Daten wie Fitparameter
import socket  # enables computer name determination
import re  # Regular expressions for pattern matching
import json
import itertools  # helps flattening of list, etc.

import scipy  # für Konstanten
from scipy import constants as c
from scipy.optimize import curve_fit  # fit
from datetime import datetime  # get current time
from scipy.special import voigt_profile
from scipy import ndimage  # image magic
from collections import defaultdict
from math import log10, floor, ceil  # necessary for display_value

from matplotlib.colors import ListedColormap  # for use of turbo colormap


# colormap for images (accessed 12.02.2022)
# Author: Anton Mikhailov
# Copyright 2019 Google LLC.
# SPDX-License-Identifier: Apache-2.0
# To use it with matplotlib, pass cmap=ListedColormap(turbo_colormap_data) as an arg to imshow()
# (don't forget "from matplotlib.colors import ListedColormap").
turbo_colormap_data = [
    [0.18995, 0.07176, 0.23217],
    [0.19483, 0.08339, 0.26149],
    [0.19956, 0.09498, 0.29024],
    [0.20415, 0.10652, 0.31844],
    [0.20860, 0.11802, 0.34607],
    [0.21291, 0.12947, 0.37314],
    [0.21708, 0.14087, 0.39964],
    [0.22111, 0.15223, 0.42558],
    [0.22500, 0.16354, 0.45096],
    [0.22875, 0.17481, 0.47578],
    [0.23236, 0.18603, 0.50004],
    [0.23582, 0.19720, 0.52373],
    [0.23915, 0.20833, 0.54686],
    [0.24234, 0.21941, 0.56942],
    [0.24539, 0.23044, 0.59142],
    [0.24830, 0.24143, 0.61286],
    [0.25107, 0.25237, 0.63374],
    [0.25369, 0.26327, 0.65406],
    [0.25618, 0.27412, 0.67381],
    [0.25853, 0.28492, 0.69300],
    [0.26074, 0.29568, 0.71162],
    [0.26280, 0.30639, 0.72968],
    [0.26473, 0.31706, 0.74718],
    [0.26652, 0.32768, 0.76412],
    [0.26816, 0.33825, 0.78050],
    [0.26967, 0.34878, 0.79631],
    [0.27103, 0.35926, 0.81156],
    [0.27226, 0.36970, 0.82624],
    [0.27334, 0.38008, 0.84037],
    [0.27429, 0.39043, 0.85393],
    [0.27509, 0.40072, 0.86692],
    [0.27576, 0.41097, 0.87936],
    [0.27628, 0.42118, 0.89123],
    [0.27667, 0.43134, 0.90254],
    [0.27691, 0.44145, 0.91328],
    [0.27701, 0.45152, 0.92347],
    [0.27698, 0.46153, 0.93309],
    [0.27680, 0.47151, 0.94214],
    [0.27648, 0.48144, 0.95064],
    [0.27603, 0.49132, 0.95857],
    [0.27543, 0.50115, 0.96594],
    [0.27469, 0.51094, 0.97275],
    [0.27381, 0.52069, 0.97899],
    [0.27273, 0.53040, 0.98461],
    [0.27106, 0.54015, 0.98930],
    [0.26878, 0.54995, 0.99303],
    [0.26592, 0.55979, 0.99583],
    [0.26252, 0.56967, 0.99773],
    [0.25862, 0.57958, 0.99876],
    [0.25425, 0.58950, 0.99896],
    [0.24946, 0.59943, 0.99835],
    [0.24427, 0.60937, 0.99697],
    [0.23874, 0.61931, 0.99485],
    [0.23288, 0.62923, 0.99202],
    [0.22676, 0.63913, 0.98851],
    [0.22039, 0.64901, 0.98436],
    [0.21382, 0.65886, 0.97959],
    [0.20708, 0.66866, 0.97423],
    [0.20021, 0.67842, 0.96833],
    [0.19326, 0.68812, 0.96190],
    [0.18625, 0.69775, 0.95498],
    [0.17923, 0.70732, 0.94761],
    [0.17223, 0.71680, 0.93981],
    [0.16529, 0.72620, 0.93161],
    [0.15844, 0.73551, 0.92305],
    [0.15173, 0.74472, 0.91416],
    [0.14519, 0.75381, 0.90496],
    [0.13886, 0.76279, 0.89550],
    [0.13278, 0.77165, 0.88580],
    [0.12698, 0.78037, 0.87590],
    [0.12151, 0.78896, 0.86581],
    [0.11639, 0.79740, 0.85559],
    [0.11167, 0.80569, 0.84525],
    [0.10738, 0.81381, 0.83484],
    [0.10357, 0.82177, 0.82437],
    [0.10026, 0.82955, 0.81389],
    [0.09750, 0.83714, 0.80342],
    [0.09532, 0.84455, 0.79299],
    [0.09377, 0.85175, 0.78264],
    [0.09287, 0.85875, 0.77240],
    [0.09267, 0.86554, 0.76230],
    [0.09320, 0.87211, 0.75237],
    [0.09451, 0.87844, 0.74265],
    [0.09662, 0.88454, 0.73316],
    [0.09958, 0.89040, 0.72393],
    [0.10342, 0.89600, 0.71500],
    [0.10815, 0.90142, 0.70599],
    [0.11374, 0.90673, 0.69651],
    [0.12014, 0.91193, 0.68660],
    [0.12733, 0.91701, 0.67627],
    [0.13526, 0.92197, 0.66556],
    [0.14391, 0.92680, 0.65448],
    [0.15323, 0.93151, 0.64308],
    [0.16319, 0.93609, 0.63137],
    [0.17377, 0.94053, 0.61938],
    [0.18491, 0.94484, 0.60713],
    [0.19659, 0.94901, 0.59466],
    [0.20877, 0.95304, 0.58199],
    [0.22142, 0.95692, 0.56914],
    [0.23449, 0.96065, 0.55614],
    [0.24797, 0.96423, 0.54303],
    [0.26180, 0.96765, 0.52981],
    [0.27597, 0.97092, 0.51653],
    [0.29042, 0.97403, 0.50321],
    [0.30513, 0.97697, 0.48987],
    [0.32006, 0.97974, 0.47654],
    [0.33517, 0.98234, 0.46325],
    [0.35043, 0.98477, 0.45002],
    [0.36581, 0.98702, 0.43688],
    [0.38127, 0.98909, 0.42386],
    [0.39678, 0.99098, 0.41098],
    [0.41229, 0.99268, 0.39826],
    [0.42778, 0.99419, 0.38575],
    [0.44321, 0.99551, 0.37345],
    [0.45854, 0.99663, 0.36140],
    [0.47375, 0.99755, 0.34963],
    [0.48879, 0.99828, 0.33816],
    [0.50362, 0.99879, 0.32701],
    [0.51822, 0.99910, 0.31622],
    [0.53255, 0.99919, 0.30581],
    [0.54658, 0.99907, 0.29581],
    [0.56026, 0.99873, 0.28623],
    [0.57357, 0.99817, 0.27712],
    [0.58646, 0.99739, 0.26849],
    [0.59891, 0.99638, 0.26038],
    [0.61088, 0.99514, 0.25280],
    [0.62233, 0.99366, 0.24579],
    [0.63323, 0.99195, 0.23937],
    [0.64362, 0.98999, 0.23356],
    [0.65394, 0.98775, 0.22835],
    [0.66428, 0.98524, 0.22370],
    [0.67462, 0.98246, 0.21960],
    [0.68494, 0.97941, 0.21602],
    [0.69525, 0.97610, 0.21294],
    [0.70553, 0.97255, 0.21032],
    [0.71577, 0.96875, 0.20815],
    [0.72596, 0.96470, 0.20640],
    [0.73610, 0.96043, 0.20504],
    [0.74617, 0.95593, 0.20406],
    [0.75617, 0.95121, 0.20343],
    [0.76608, 0.94627, 0.20311],
    [0.77591, 0.94113, 0.20310],
    [0.78563, 0.93579, 0.20336],
    [0.79524, 0.93025, 0.20386],
    [0.80473, 0.92452, 0.20459],
    [0.81410, 0.91861, 0.20552],
    [0.82333, 0.91253, 0.20663],
    [0.83241, 0.90627, 0.20788],
    [0.84133, 0.89986, 0.20926],
    [0.85010, 0.89328, 0.21074],
    [0.85868, 0.88655, 0.21230],
    [0.86709, 0.87968, 0.21391],
    [0.87530, 0.87267, 0.21555],
    [0.88331, 0.86553, 0.21719],
    [0.89112, 0.85826, 0.21880],
    [0.89870, 0.85087, 0.22038],
    [0.90605, 0.84337, 0.22188],
    [0.91317, 0.83576, 0.22328],
    [0.92004, 0.82806, 0.22456],
    [0.92666, 0.82025, 0.22570],
    [0.93301, 0.81236, 0.22667],
    [0.93909, 0.80439, 0.22744],
    [0.94489, 0.79634, 0.22800],
    [0.95039, 0.78823, 0.22831],
    [0.95560, 0.78005, 0.22836],
    [0.96049, 0.77181, 0.22811],
    [0.96507, 0.76352, 0.22754],
    [0.96931, 0.75519, 0.22663],
    [0.97323, 0.74682, 0.22536],
    [0.97679, 0.73842, 0.22369],
    [0.98000, 0.73000, 0.22161],
    [0.98289, 0.72140, 0.21918],
    [0.98549, 0.71250, 0.21650],
    [0.98781, 0.70330, 0.21358],
    [0.98986, 0.69382, 0.21043],
    [0.99163, 0.68408, 0.20706],
    [0.99314, 0.67408, 0.20348],
    [0.99438, 0.66386, 0.19971],
    [0.99535, 0.65341, 0.19577],
    [0.99607, 0.64277, 0.19165],
    [0.99654, 0.63193, 0.18738],
    [0.99675, 0.62093, 0.18297],
    [0.99672, 0.60977, 0.17842],
    [0.99644, 0.59846, 0.17376],
    [0.99593, 0.58703, 0.16899],
    [0.99517, 0.57549, 0.16412],
    [0.99419, 0.56386, 0.15918],
    [0.99297, 0.55214, 0.15417],
    [0.99153, 0.54036, 0.14910],
    [0.98987, 0.52854, 0.14398],
    [0.98799, 0.51667, 0.13883],
    [0.98590, 0.50479, 0.13367],
    [0.98360, 0.49291, 0.12849],
    [0.98108, 0.48104, 0.12332],
    [0.97837, 0.46920, 0.11817],
    [0.97545, 0.45740, 0.11305],
    [0.97234, 0.44565, 0.10797],
    [0.96904, 0.43399, 0.10294],
    [0.96555, 0.42241, 0.09798],
    [0.96187, 0.41093, 0.09310],
    [0.95801, 0.39958, 0.08831],
    [0.95398, 0.38836, 0.08362],
    [0.94977, 0.37729, 0.07905],
    [0.94538, 0.36638, 0.07461],
    [0.94084, 0.35566, 0.07031],
    [0.93612, 0.34513, 0.06616],
    [0.93125, 0.33482, 0.06218],
    [0.92623, 0.32473, 0.05837],
    [0.92105, 0.31489, 0.05475],
    [0.91572, 0.30530, 0.05134],
    [0.91024, 0.29599, 0.04814],
    [0.90463, 0.28696, 0.04516],
    [0.89888, 0.27824, 0.04243],
    [0.89298, 0.26981, 0.03993],
    [0.88691, 0.26152, 0.03753],
    [0.88066, 0.25334, 0.03521],
    [0.87422, 0.24526, 0.03297],
    [0.86760, 0.23730, 0.03082],
    [0.86079, 0.22945, 0.02875],
    [0.85380, 0.22170, 0.02677],
    [0.84662, 0.21407, 0.02487],
    [0.83926, 0.20654, 0.02305],
    [0.83172, 0.19912, 0.02131],
    [0.82399, 0.19182, 0.01966],
    [0.81608, 0.18462, 0.01809],
    [0.80799, 0.17753, 0.01660],
    [0.79971, 0.17055, 0.01520],
    [0.79125, 0.16368, 0.01387],
    [0.78260, 0.15693, 0.01264],
    [0.77377, 0.15028, 0.01148],
    [0.76476, 0.14374, 0.01041],
    [0.75556, 0.13731, 0.00942],
    [0.74617, 0.13098, 0.00851],
    [0.73661, 0.12477, 0.00769],
    [0.72686, 0.11867, 0.00695],
    [0.71692, 0.11268, 0.00629],
    [0.70680, 0.10680, 0.00571],
    [0.69650, 0.10102, 0.00522],
    [0.68602, 0.09536, 0.00481],
    [0.67535, 0.08980, 0.00449],
    [0.66449, 0.08436, 0.00424],
    [0.65345, 0.07902, 0.00408],
    [0.64223, 0.07380, 0.00401],
    [0.63082, 0.06868, 0.00401],
    [0.61923, 0.06367, 0.00410],
    [0.60746, 0.05878, 0.00427],
    [0.59550, 0.05399, 0.00453],
    [0.58336, 0.04931, 0.00486],
    [0.57103, 0.04474, 0.00529],
    [0.55852, 0.04028, 0.00579],
    [0.54583, 0.03593, 0.00638],
    [0.53295, 0.03169, 0.00705],
    [0.51989, 0.02756, 0.00780],
    [0.50664, 0.02354, 0.00863],
    [0.49321, 0.01963, 0.00955],
    [0.47960, 0.01583, 0.01055],
]


"""define some global filepath variables based upon the computer name:"""


def get_path_prefix():
    pc_name = socket.gethostname()
    if pc_name == "WhiteShard":
        return "M:/"
    elif pc_name == "POPS":
        return "M:/"
    elif pc_name == "MYRES":
        return "D:/"
    else:
        print("data location not specified, use generic path: M:/")
        return "M:/"


path_prefix = get_path_prefix()


def get_save_path(location="OPA-Paper/analysis (python)/Ausgabe/"):
    pc_name = socket.gethostname()
    if pc_name == "WhiteShard":
        return "C:/Users/jfk/HESSENBOX-DA/" + location
    elif pc_name == "POPS":
        return "C:/Users/kinder.NLOQO/HESSENBOX-DA/" + location
    elif pc_name == "FLOYD-PEPPER":
        location = "/"
        return r"C:\Users\Kaul\OneDrive\Uni\Semester 6\Bachelorthesis\Abbildungen" + location
    elif pc_name == "MYRES":
        return "C:/Transfer/JFK/plots/"
    else:
        print("save location not specified, use generic path: M:/")
        return "M:/"


file_path = get_save_path()


"""Funktionen"""


def pl(
    log="", leg=True, name="", xname="", minor=True, dpi=150, savedpi="figure", axes=None, **kwargs
):
    """
    Configure the standard plot with ticks all around, which point inwards.

    Requires matplotlib as plt.

    Parameters
    -----------
    log : str, optional
        Names of axes for which a logarithmic scale should be applied. Default is "".
    leg : bool, optional
        Wether a legend should be shown. The default is True.
    name : str, optional
        Filename to save the plot to.
    xname : str, optional
        Do nothing. It is a simple method to deactivate saving: change 'name' to 'xname'.
    minor : bool, optional
        Show minor ticks. Default is True.
    dpi : int, optional
        Dpi value for display. Default is 150
    savedpi : bool or "figure", optional
        Dpi value for saving the image. If "figure", the same value as `dpi` is taken.
        Default is "figure".
    axes : axes object, optional
        axes object which should be modified. If not supplied, the current object is taken.
    kwargs : dict, optional
        Further values are handed to the legend.
    """

    if axes is None:
        axes = plt.gca()  # Define the current axes object

    if minor:  # Minorticks
        axes.minorticks_on()

    axes.tick_params(axis="both", which="both", direction="in", top="true", right="true")

    # Logarithmic Axes
    if log != "":
        assert log == "x" or log == "y" or log == "both", "invalid axis parameter"
        axes.tick_params(axis=log, which="both", direction="in", top="true", right="true")
    if log == "x":
        axes.set_xscale("log")
    if log == "y":
        axes.set_yscale("log")
    if log == "both":
        axes.set_xscale("log")
        axes.set_yscale("log")

    if leg:  # Legend
        axes.legend(**kwargs)

    if name != "":  # Save file
        plt.savefig(fname=name, dpi=savedpi, bbox_inches="tight")

    plt.tight_layout()  # In order to fit the plot better into the present space.
    plt.show


def import_file(name, path_prefix=path_prefix, printing=False):
    data = {}
    with open(f"M:/Measurement Data/DataLogger/{name}.pkl", "rb") as file:
        result = pickle.load(file)
        if len(result) == 3:
            header, data, units = result
        else:
            header, data = result
    if printing:
        print(name, data.keys())
    return data


def loadFiles(names, path_prefix=path_prefix, printing=False):
    """Daten mit der Namensliste `names` laden und als zwei Listen zurückgeben."""
    if hasattr(names, "__iter__"):
        headers = {}
        datas = {}
        for name in names:
            with open(path_prefix + f"Measurement Data/DataLogger/{name}.pkl", "rb") as file:
                header, data = pickle.load(file)
            headers[name] = header
            datas[name] = data
            if printing:
                print(name, data.keys())
        return headers, datas
    else:
        with open(path_prefix + f"Measurement Data/DataLogger/{names}.pkl", "rb") as file:
            header, data = pickle.load(file)
        if printing:
            print(names, data.keys())
        return {names: header}, {names: data}  # was name...


def plotdata(data, xScale=1, yScale=1):
    """Returns the x and y components of a two dimensional list as individual lists, e.g., for
    plotting. Optional linear scale factors as additional commands"""
    xValues = []
    yValues = []
    for i in range(len(data)):
        xValues.append(xScale * data[i][0])
        yValues.append(yScale * data[i][1])
    return xValues, yValues


def calc_mean(eval_data, print_output=False):
    """
    returns mean, standard deviation and standard error (stdev/sqrt(sample number))
    """
    # removes nan-values from list
    data = []
    for item in eval_data:
        if not np.isnan(item):
            data.append(item)

    if len(data) == 0:
        print("empty list!")
        mean, stdev, serr = 0, 0, 0
    else:
        n_samples = len(eval_data)
        mean = np.nanmean(eval_data)
        stdev = np.nanstd(eval_data)
        serr = stdev / np.sqrt(n_samples)
        if print_output:
            print("mean: {0:1.5f}, SD: {1:1.5f}, SE: {2:1.5f}".format(mean, stdev, serr))
    return mean, stdev, serr


def remove_nan(data):
    """remove entry in a list, if any nan value ocurred"""
    temp = []
    for item in data:
        nan_list = []
        for entry in item:
            nan_list.append(np.isnan(entry))
        if False in nan_list:
            temp.append(item)
    return temp


def remove_nans(x, y):
    """removes nan from list I want to plot"""
    x_out = []
    y_out = []
    for i in range(len(x)):
        if not np.isnan(x[i]) and not np.isnan(y[i]):
            x_out.append(x[i])
            y_out.append(y[i])
        else:
            continue
    return x_out, y_out


def gauss(x, x0, a, sigma):
    """Gaussian function"""
    return a * np.exp(-((x - x0) ** 2) / (2 * sigma**2))


def gaussian_fit(data, print_output=False):
    """performs Gaussian fit (position, amplitude, standard deviation), returns popt, sdev, serr,
    pcov"""
    # arguments of gauss-function: gauss(x, x0, a, sigma)
    x, y = plotdata(data)

    # find peak position
    peak_pos = x[list(y).index(max(y))]

    try:
        popt, pcov = curve_fit(gauss, x, y, p0=[peak_pos, 1, 1])
    except RuntimeError:
        print("fit failed!")
        popt, pcov = [0, 0, 0], np.zeros((3, 3)).tolist()

    # covariance matrix: diagonal elements are the variances, hence, the square root is the
    # standard deviation
    sdev = np.sqrt(abs(pcov.diagonal()).tolist())

    # standard errors are sd/sqrt(n) with the sample size n
    serr = sdev / (np.sqrt(len(data)))

    if print_output:
        print(
            "Gaussian-fit: x0:",
            display_value(popt[0], serr[0]),
            ", a:",
            display_value(popt[1], serr[1]),
            ", sigma:",
            display_value(abs(popt[2]), serr[2]),
        )

    return popt, sdev, serr, pcov


def fit_slice(data, print_output=False):
    """performs Gaussian fit to slice through beamprofile"""
    # Todo: implement gaussian_fit (or replace altogether, maybe more beamprofile features,
    # printoutput, etc.)
    #
    # arguments of gauss-function: gauss(x, x0, a, sigma)
    x, y = plotdata(data)

    # get maximum and its position by image magic
    max_pos, max = data[ndimage.maximum_position(data)[0]]

    try:
        popt, pcov = curve_fit(gauss, x, y, p0=[max_pos, max, 1])
    except RuntimeError:
        print("fit failed!")
        popt, pcov = [0, 0, 0], np.array(np.zeros((3, 3)).tolist())

    # covariance matrix: diagonal elements are the variances, hence, the square root is the
    # standard deviation
    sdev = np.sqrt(abs(pcov.diagonal()).tolist())

    # standard errors are sd/sqrt(n) with the sample size n
    serr = sdev / (np.sqrt(len(data)))

    if print_output:
        print(
            "x0:",
            display_value(popt[0], serr[0]),
            "mm, a:",
            display_value(popt[1], serr[1]),
            ", w0:",
            display_value(2 * abs(popt[2]), 2 * serr[2]),
            "mm",
        )

    return popt, sdev, serr, pcov


def display_value(x, dx):
    """
    Returns a string of the measurement value together with the measurement error
     x: measurement value
     dx: measurment error
    https://stackoverflow.com/questions/57507962/print-measurement-uncertainty-after-number
    (18.01.2022)
    """
    try:
        if dx == 0:
            err = 0
            prec = 1
        else:
            # Power of dx
            power_err = log10(dx)
            # Digits of dx in format a.bcd
            n_err = dx / (10 ** floor(power_err))
            # If the second digit in dx is >=5
            # round the 1st digit in dx up
            if n_err % 1 >= 0.5:
                # If the first digit of dx is 9
                # the precision is one digit less
                if int(n_err) == 9:
                    err = 1
                    # The precision of x is determined by the precision of dx
                    prec = int(-floor(log10(dx))) - 1
                else:
                    err = ceil(n_err)
                    # The precision of x is determined by the precision of dx
                    prec = int(-floor(log10(dx)))
            # Otherwise round down
            else:
                err = floor(n_err)
                # The precision of x is determined by the precision of dx
                prec = int(-floor(log10(dx)))
        # try:
        if prec > 8:  # too many zeros
            return f"{x} ± {dx}"  # f'{x:f}±{err:.f}·'
        else:
            return "{:.{prec}f}({:.0f})".format(x, err, prec=prec)
    except ValueError:
        return "nan"
    # except OverflowError:
    #    return 'nan'


def linear(x, a, b):
    """linear function a*x+b"""
    return a * np.array(x) + b


def linear_fit(fit_data, print_output=False):
    """performs a linear fit a*x+b"""
    x, y = plotdata(fit_data)
    try:
        popt, pcov = curve_fit(linear, x, y, p0=[1, 1])
    except RuntimeError:
        print("fit failed!")
        popt, pcov = [0, 0], np.zeros((2, 2)).tolist()

    # covariance matrix: diagonal elements are the variances, hence, the square root is the
    # standard deviation
    sdev = np.sqrt(abs(pcov.diagonal()).tolist())

    # standard errors are sd/sqrt(n) with the sample size n
    serr = sdev / (np.sqrt(len(fit_data)))

    if print_output:
        print("a:", display_value(popt[0], serr[0]), ", b:", display_value(popt[1], serr[1]))

    return popt, pcov, serr


def create_2dlist(dictionary, key_x, key_y, operation_x=lambda a: a, operation_y=lambda a: a):
    "creates 2D list from entries to the dictionary and removes elements which contain a nan value"
    raw_data = np.array(
        [operation_x(np.array(dictionary[key_x])), operation_y(np.array(dictionary[key_y]))]
    ).T
    temp_data = []
    for item in raw_data:
        if not np.isnan(item[0]) and not np.isnan(item[1]):
            temp_data.append(item)
    return temp_data


def map_list(list, index_x, index_y, operation_x=lambda a: a, operation_y=lambda a: a):
    """
    returns array with x- and y-entries and the possibility to perform arbitrary functions on the
    components
    """
    temp = np.array(list).T
    return np.array([operation_x(temp[index_x]), operation_y(temp[index_y])]).T


def import_spectr(run, event):
    """
    imports mid IR spectrometer data (alignment mode)
    format: index, angle [°], wavelength [nm], power [arb. u.]
    """
    name = f"R{run:04d}E{event:04d}spectrum(alignment).dat"
    path = f"M:/Measurement Data/MidIR_Spectrometer/{name}"
    with open(path, "r") as file:
        raw_data = file.readlines()
    data = []
    for item in raw_data:
        temp = item.split()
        element = []
        for entry in temp:
            element.append(float(entry))
        data.append(element)
    return data


def fit_spectr(run, event, print_output=False):
    """
    imports mid IR spectrometer data (alignment mode) and performs Gaussian fits.
    format: [popt_nm, sd_nm, se_nm], [min_nm, max_nm], peak_value,
    [popt_angles, sd_angles, se_angles], [min_angles, max_angles]
    """
    data = import_spectr(run, event)

    data_angles = map_list(data, 1, 3)
    data_nm = map_list(data, 2, 3)

    peak_value = max(data_angles.T[1])
    angles = data_angles.T[0]
    min_angles, max_angles = min(angles), max(angles)

    wavelengths = data_nm.T[0]
    min_nm, max_nm = min(wavelengths), max(wavelengths)

    popt_angles, sd_angles, se_angles = gaussian_fit(data_angles, print_output)[0:3]
    popt_nm, sd_nm, se_nm = gaussian_fit(data_nm, print_output)[0:3]

    return (
        [popt_nm, sd_nm, se_nm],
        [min_nm, max_nm],
        peak_value,
        [popt_angles, sd_angles, se_angles],
        [min_angles, max_angles],
    )


def import_osa(run, event):
    """
    imports OSA data
    format: wavelength [nm], power [dBm], power [W]
    """
    name = f"R{run:03d}E{event:03d}_OSA.txt"
    path = f"M:/Measurement Data/OSA/{name}"
    with open(path, "r") as file:
        raw_data = file.readlines()
    data = []
    for item in raw_data:
        temp = item.split()
        element = []
        for entry in temp:
            element.append(float(entry.replace(",", ".")))
        # conversion from dBm to W
        element.append(1e-3 * 10 ** (element[-1] / 10))
        data.append(element)
    return data


def fit_osa(run, event, print_output=False):
    """
    fits OSA spectrum
    format: [popt_nm, sd_nm, se_nm], [min_nm, max_nm], peak_value
    """
    fitdata = map_list(np.array(import_osa(run, event)), 0, 2)

    peak_value = max(fitdata.T[1])
    wavelengths = fitdata.T[0]
    min_nm, max_nm = min(wavelengths), max(wavelengths)
    # gauss(x, x0, a, sigma)
    popt_nm, sd_nm, se_nm = gaussian_fit(fitdata, print_output)[0:3]

    return [popt_nm, sd_nm, se_nm], [min_nm, max_nm], peak_value


# conversion relations
def nm2nu(nm):
    """converts wavelength (in nm) to wavenumbers (in cm^-1)"""
    return (100 * nm * 1e-9) ** (-1)


def nm2hz(nm):
    """converts wavelength (in nm) to frequency (in Hz)"""
    return 299792458 * (nm * 1e-9) ** (-1)


def nu2nm(nu):
    """converts wavenumbers (in cm^-1) to wavelength (in nm)"""
    x = np.array(nu).astype(float)
    near_zero = np.isclose(x, 0)
    x[near_zero] = np.inf
    x[~near_zero] = (100 * x[~near_zero]) ** (-1) * 1e9

    # (100*nu)**(-1)*1E9
    return x


def nu2hz(nu):
    """converts wavenumbers (in cm^-1) to frequency (in Hz)"""
    return 299792458 * 100 * nu


def hz2nu(hz):
    """converts frequency (in Hz) to wavenumbers (in cm^-1)"""
    return hz / (299792458 * 100)


def hz2nm(hz):
    """converts frequency (in Hz) to wavelength (in nm)"""
    return 299792458 * hz ** (-1) * 1e9


def idler2signal(idler, pump=1064):
    """converts between idler and signal wavelength (in nm) for a DFG process with fixed pump
    wavelength"""
    return (1 / pump - 1 / idler) ** (-1)


def dbm2w(dbm):
    return 10 ** ((dbm - 30) / 10)


def delta_nm2hz(nm, central_wavelength):
    """converts wavelength bandwidth (in nm) to frequency (in Hz)"""
    return nm2hz(central_wavelength + nm / 2) - nm2hz(central_wavelength - nm / 2)


def save_plot(name, path=file_path, print_output=False):
    """
    saves current plot as png and pdf file, overwrites if necessary, currently uses global
    variable file_path
    """
    now = datetime.now()
    file_name = path + now.strftime("%Y-%m-%d") + "_" + name
    error = False

    try:
        overwritten = os.path.isfile(file_name + ".png")
    except IOError:
        error = True
        print("error: no access to directory!")

    # print(file_name)
    try:
        plt.savefig(file_name + ".png", dpi=600, bbox_inches="tight", transparent=False)
        plt.savefig(file_name + ".pdf", bbox_inches="tight", transparent=False)
    except IOError:
        error = True
        print("error: cannot save files!")

    if print_output and not error:
        if overwritten:
            print("Success! 2 files overwritten at", file_name)
        else:
            print("Success! 2 files saved at", file_name)


# TODO: use list of arguments to optimize multi-pane plots.
def plot_options(
    fig,
    ax,
    xlabel=None,
    ylabel=None,
    font_size=11,
    ticks=["auto", "auto"],
    image_width=10,
    aspect_ratio=scipy.constants.golden,
    dpi=200,
):
    """
    sets plot options just like in Mathematica
    :param fig: figure object from pyplot (necessary)
    :param ax: axes object from pyplot (necessary)
    :param xlabel: x-axis label (string or None)
    :param ylabel: y-axis label (string or None)
    :param font_size: font size (number, standard=11)
    :param ticks: custom or auto ticks (list of x and y: 'auto' or major tick increment)
    :param image_width: width of the image in cm (number, standard=10)
    :param aspect_ratio: aspect ratio of the image (number, standard=golden ratio)

    Parameters
    ----------
    dpi : dpi for displaying in jupyterlab (standard = 200)
    """
    # set size and aspect ratio
    fig.set_size_inches(image_width / 2.54, image_width / 2.54 / aspect_ratio)

    fig = matplotlib.pyplot.gcf()
    plt.rcParams["font.family"] = "Arial"
    fig.set_dpi(dpi)  # only for display in jupyterlab!
    fig.patch.set_facecolor("white")

    # plt.style.use('classic')
    ax.tick_params(
        axis="both", which="major", labelsize=font_size, right=True, top=True, direction="in"
    )
    ax.tick_params(
        axis="both", which="minor", labelsize=font_size - 2, right=True, top=True, direction="in"
    )

    # custom major ticks
    if ticks[0] == "auto":
        ax.xaxis.set_major_locator(tic.AutoLocator())
    else:
        ax.xaxis.set_major_locator(tic.MultipleLocator(ticks[0]))
    if ticks[1] == "auto":
        ax.yaxis.set_major_locator(tic.AutoLocator())
    else:
        ax.yaxis.set_major_locator(tic.MultipleLocator(ticks[1]))

    # activate automatic minor ticks
    ax.xaxis.set_minor_locator(tic.AutoMinorLocator())
    ax.yaxis.set_minor_locator(tic.AutoMinorLocator())

    # changes fontsize of the labels
    ax.set_xlabel(xlabel, color="black", fontsize=font_size)
    ax.set_ylabel(ylabel, color="black", fontsize=font_size)

    # reduce tick lengths
    ax.tick_params(axis="both", which="minor", length=2)
    ax.tick_params(axis="both", which="major", length=3)


def gauss_offset(x, x0, a, sigma, b):
    """Gaussian function with offset"""
    return a * np.exp(-((x - x0) ** 2) / (2 * sigma**2)) + b


def bin_list(x, y, bin_interval):
    """bins list (consisting of two columns with bin_interval"""
    minimum, maximum = min(x), max(x)
    n_bins = int((maximum - minimum) / bin_interval)
    bins = np.linspace(minimum, maximum, n_bins)
    bin_means = np.histogram(x, bins, weights=y)[0] / np.histogram(list(x), bins)[0]

    return bins[0:-1], bin_means


def transmission_overview(
    data,
    bin_interval,
    signal_wavelength,
    startvalues_fit1=["auto", "auto", "auto", "auto"],
    startvalues_fit2=["auto", "auto", "auto", "auto"],
):
    temp = create_2dlist(data, "wavemeter", "IRPD2")
    x_ns, y_ns = np.array(temp).T[0], -np.array(temp).T[1]
    # x_ns, y_ns = data[file_name]['wavemeter'], -np.array(data[file_name]['IRPD2'])
    x_bin_ns, y_bin_ns = bin_list(x_ns, y_ns, bin_interval)

    peak_pos = x_bin_ns[list(y_bin_ns).index(max(y_bin_ns))]

    # fit1 starting values
    p0 = [peak_pos, 1, 1e-3, 1e-1]
    p0_fit1 = []
    for i in range(len(startvalues_fit1)):
        item = startvalues_fit1[i]
        if item == "auto":
            p0_fit1.append(p0[i])
        else:
            p0_fit1.append(item)

    try:
        popt_ns, pcov = curve_fit(gauss_offset, x_bin_ns, y_bin_ns, p0=p0_fit1)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        # sdev_ns = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        # serr_ns = sdev_ns / (np.sqrt(len(x_bin_ns)))
        fit1_failed = False
    except RuntimeError:
        print("fit failed!")
        fit1_failed = True
        popt_ns, pcov = [0, 0, 0, 0], np.zeros((4, 4))

    if not fit1_failed:
        peak_pos_ns = popt_ns[0]
        sigma_ns = abs(popt_ns[2])  # in nm pump radiation2
        sdev_idler_ns = nm2hz(idler2signal(signal_wavelength, peak_pos_ns - sigma_ns / 2)) - nm2hz(
            idler2signal(signal_wavelength, peak_pos_ns + sigma_ns / 2)
        )
        fwhm_ns = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_ns * 1e-6
        print("ns", fwhm_ns, "MHz")

    temp = create_2dlist(data, "wavemeter", "IRPD2-cw")
    x_cw, y_cw = np.array(temp).T[0], -np.array(temp).T[1]
    # x_cw, y_cw = data[file_name]['wavemeter'], -np.array(data[file_name]['IRPD2-cw'])
    x_bin_cw, y_bin_cw = bin_list(x_cw, y_cw, bin_interval)

    peak_pos = x_bin_cw[list(y_bin_cw).index(max(y_bin_cw))]

    # fit1 starting values
    p0 = [peak_pos, 1, 1e-3, 1e-1]
    p0_fit2 = []
    for i in range(len(startvalues_fit2)):
        item = startvalues_fit2[i]
        if item == "auto":
            p0_fit2.append(p0[i])
        else:
            p0_fit2.append(item)

    try:
        popt_cw, pcov = curve_fit(gauss_offset, x_bin_cw, y_bin_cw, p0=p0_fit2)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        # sdev_cw = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        # serr_cw = sdev_cw / (np.sqrt(len(x_bin_cw)))
        fit2_failed = False
    except RuntimeError:
        print("fit failed!")
        fit2_failed = True
        popt_cw = [0, 0, 0, 0]
        # pcov = np.zeros((4, 4))

    if not fit2_failed:
        peak_pos_cw = popt_cw[0]
        sigma_cw = abs(popt_cw[2])  # in nm pump radiation2
        sdev_idler_cw = nm2hz(idler2signal(signal_wavelength, peak_pos_cw - sigma_cw / 2)) - nm2hz(
            idler2signal(signal_wavelength, peak_pos_cw + sigma_cw / 2)
        )
        fwhm_cw = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_cw * 1e-6
        print("cw:", fwhm_cw, "MHz")

    if not fit1_failed and not fit2_failed and fwhm_ns >= fwhm_cw:
        fwhm_laser = np.sqrt(fwhm_ns**2 - fwhm_cw**2)
        print("laser:", fwhm_laser, "MHz")

    # plots
    fig, (ax1, ax2) = plt.subplots(1, 2)
    plot_options(
        fig,
        ax1,
        font_size=10,
        image_width=15,
        ticks=["auto", "auto"],
        xlabel="wavelength [nm]",
        ylabel="IRPD2 ns [arb. u.]",
    )

    ax1.plot(x_ns, -y_ns, "g", label="OPA 1", markersize=5)
    ax1.plot(x_bin_ns, -y_bin_ns, "b.", label="OPA 1", markersize=5)
    if not fit1_failed:
        ax1.plot(x_ns, -gauss_offset(x_ns, *popt_ns), "r", label="OPA 1", markersize=5)
    # x, y = data[file_name]['wavemeter'], np.array(data[file_name]['IRPD2-cw'])
    # plt.plot(x, y, 'r', label='OPA 1', markersize=5)
    # plt.plot(x, np.zeros(len(x)) + back_pd2, 'g')

    # plt.xlim([3230, 3810]) # global!
    # plt.ylim([0, 40])

    plot_options(
        fig,
        ax2,
        font_size=10,
        image_width=15,
        ticks=["auto", "auto"],
        xlabel="wavelength [nm]",
        ylabel="IRPD2 cw [arb. u.]",
    )

    ax2.plot(x_cw, -y_cw, "g", label="OPA 1", markersize=5)
    ax2.plot(x_bin_cw, -y_bin_cw, "b.", label="OPA 1", markersize=5)
    if not fit2_failed:
        ax2.plot(x_cw, -gauss_offset(x_cw, *popt_cw), "r", label="OPA 1", markersize=5)

    plt.tight_layout()
    plt.show()


def transmission_overview2(
    data,
    bin_interval,
    signal_wavelength,
    startvalues_fit1=["auto", "auto", "auto", "auto"],
    startvalues_fit2=["auto", "auto", "auto", "auto"],
    normalized=True,
):
    temp = create_2dlist(data, "wavemeter", "IRPD2")
    x_ns, y_ns = idler2signal(signal_wavelength, np.array(temp).T[0]), -np.array(temp).T[1]

    # x_ns, y_ns = data[file_name]['wavemeter'], -np.array(data[file_name]['IRPD2'])
    x_bin_ns, y_bin_ns = binning(x_ns, y_ns, binsize=bin_interval)[:2]

    peak_pos = x_bin_ns[list(y_bin_ns).index(max(y_bin_ns))]

    # fit1 starting values
    p0 = [peak_pos, 1, 1e-3, 1e-1]
    p0_fit1 = []
    for i in range(len(startvalues_fit1)):
        item = startvalues_fit1[i]
        if item == "auto":
            p0_fit1.append(p0[i])
        else:
            p0_fit1.append(item)

    try:
        popt_ns, pcov = curve_fit(gauss_offset, x_bin_ns, y_bin_ns, p0=p0_fit1)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        # sdev_ns = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        # serr_ns = sdev_ns / (np.sqrt(len(x_bin_ns)))
        fit1_failed = False
    except RuntimeError:
        print("fit failed!")
        fit1_failed = True
        popt_ns, pcov = [0, 0, 0, 0], np.zeros((4, 4))

    if not fit1_failed:
        peak_pos_ns = popt_ns[0]
        sigma_ns = abs(popt_ns[2])  # in nm pump radiation2
        sdev_idler_ns = nm2hz(idler2signal(signal_wavelength, peak_pos_ns - sigma_ns / 2)) - nm2hz(
            idler2signal(signal_wavelength, peak_pos_ns + sigma_ns / 2)
        )
        fwhm_ns = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_ns * 1e-6
        print("ns", fwhm_ns, "MHz")

    temp = create_2dlist(data, "wavemeter", "IRPD2-cw")
    x_cw, y_cw = idler2signal(signal_wavelength, np.array(temp).T[0]), -np.array(temp).T[1]
    # x_cw, y_cw = data[file_name]['wavemeter'], -np.array(data[file_name]['IRPD2-cw'])
    x_bin_cw, y_bin_cw = binning(x_cw, y_cw, binsize=bin_interval)[:2]

    peak_pos = x_bin_cw[list(y_bin_cw).index(max(y_bin_cw))]

    # fit1 starting values
    p0 = [peak_pos, 1, 1e-3, 1e-1]
    p0_fit2 = []
    for i in range(len(startvalues_fit2)):
        item = startvalues_fit2[i]
        if item == "auto":
            p0_fit2.append(p0[i])
        else:
            p0_fit2.append(item)

    try:
        popt_cw, pcov = curve_fit(gauss_offset, x_bin_cw, y_bin_cw, p0=p0_fit2)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        # sdev_cw = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        # serr_cw = sdev_cw / (np.sqrt(len(x_bin_cw)))
        fit2_failed = False
    except RuntimeError:
        print("fit failed!")
        fit2_failed = True
        popt_cw = [0, 0, 0, 0]
        # pcov = np.zeros((4, 4))

    if not fit2_failed:
        peak_pos_cw = popt_cw[0]
        sigma_cw = abs(popt_cw[2])  # in nm pump radiation2
        sdev_idler_cw = nm2hz(idler2signal(signal_wavelength, peak_pos_cw - sigma_cw / 2)) - nm2hz(
            idler2signal(signal_wavelength, peak_pos_cw + sigma_cw / 2)
        )
        fwhm_cw = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_cw * 1e-6
        print("cw:", fwhm_cw, "MHz")

    if not fit1_failed and not fit2_failed and fwhm_ns >= fwhm_cw:
        fwhm_laser = np.sqrt(fwhm_ns**2 - fwhm_cw**2)
        print("laser:", fwhm_laser, "MHz")

    # normalized fit?
    if normalized:
        x_ns_plot = (nm2hz(x_ns) - nm2hz(peak_pos_ns)) * 1e-6
        x_bin_ns_plot = (nm2hz(x_bin_ns) - nm2hz(peak_pos_ns)) * 1e-6
        x_cw_plot = (nm2hz(x_cw) - nm2hz(peak_pos_cw)) * 1e-6
        x_bin_cw_plot = (nm2hz(x_bin_cw) - nm2hz(peak_pos_cw)) * 1e-6
        xlabel = "detuning [MHz]"
        popt_ns_plot = [0, popt_ns[1], 1e-6 * delta_nm2hz(popt_ns[2], popt_ns[0]), popt_ns[3]]
        popt_cw_plot = [0, popt_cw[1], 1e-6 * delta_nm2hz(popt_cw[2], popt_cw[0]), popt_cw[3]]
    else:
        x_ns_plot, x_bin_ns_plot = x_ns, x_bin_ns
        x_cw_plot, x_bin_cw_plot = x_cw, x_bin_cw
        popt_ns_plot, popt_cw_plot = popt_ns, popt_cw
        xlabel = "wavelength [nm]"

    # plots
    fig, (ax1, ax2) = plt.subplots(1, 2)
    # fig.set_dpi(300)
    plot_options(
        fig,
        ax1,
        font_size=10,
        image_width=30,
        ticks=["auto", "auto"],
        xlabel=xlabel,
        ylabel="IRPD2 ns [arb. u.]",
        aspect_ratio=2 * scipy.constants.golden,
    )

    ax1.plot(x_ns_plot, -y_ns, "g", label="OPA 1", markersize=5)
    ax1.plot(x_bin_ns_plot, -y_bin_ns, "b.", label="OPA 1", markersize=5)
    if not fit1_failed:
        ax1.plot(
            x_ns_plot, -gauss_offset(x_ns_plot, *popt_ns_plot), "r", label="OPA 1", markersize=5
        )
    # x, y = data[file_name]['wavemeter'], np.array(data[file_name]['IRPD2-cw'])
    # plt.plot(x, y, 'r', label='OPA 1', markersize=5)
    # plt.plot(x, np.zeros(len(x)) + back_pd2, 'g')

    # plt.xlim([3230, 3810]) # global!
    # plt.ylim([0, 40])

    plot_options(
        fig,
        ax2,
        font_size=10,
        image_width=30,
        ticks=["auto", "auto"],
        xlabel=xlabel,
        ylabel="IRPD2 cw [arb. u.]",
        aspect_ratio=2 * scipy.constants.golden,
    )

    ax2.plot(x_cw_plot, -y_cw, "g", label="OPA 1", markersize=5)
    ax2.plot(x_bin_cw_plot, -y_bin_cw, "b.", label="OPA 1", markersize=5)
    if not fit2_failed:
        ax2.plot(
            x_cw_plot, -gauss_offset(x_cw_plot, *popt_cw_plot), "r", label="OPA 1", markersize=5
        )

    plt.tight_layout()
    plt.show()


def absorption_fit(x, y, startvalues=["auto", "auto", "auto", "auto", "auto"]):
    peak_pos = x[list(y).index(max(y))]

    # good starting values for the fit
    p0 = [peak_pos, 1, 1e-3, min(y), 1e-6]
    p0_fit = []
    for i in range(len(startvalues)):
        item = startvalues[i]
        if item == "auto":
            p0.append(p0[i])
        else:
            p0.append(item)

    def fit_function(x, x0, a, sigma, b, c):
        return a * np.exp(-((x - x0) ** 2) / (2 * sigma**2)) + b + c * (x - x0)

    try:
        popt, pcov = curve_fit(fit_function, x, y, p0=p0_fit)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr = sdev / (np.sqrt(len(x)))
    except RuntimeError:
        print("fit failed!")
        popt_ns, pcov = [0, 0, 0, 0, 0], np.zeros((5, 5))
        sdev = popt_ns
        serr = popt_ns

    return popt, sdev, serr


def transmission_overview3(
    data,
    bin_interval,
    signal_wavelength,
    startvalues_fit1=["auto", "auto", "auto", "auto", "auto"],
    startvalues_fit2=["auto", "auto", "auto", "auto", "auto"],
    normalized=True,
    background_cw=0,
    fit_direction="both",
):
    """
    fit_direction : 'both': Standard, both directions, 'up', 'down'
    """
    temp = create_2dlist(data, "wavemeter", "IRPD2")
    x_ns, y_ns = nm2nu(idler2signal(signal_wavelength, np.array(temp).T[0])), -np.array(temp).T[1]

    # x_ns, y_ns = data[file_name]['wavemeter'], -np.array(data[file_name]['IRPD2'])
    if not bin_interval == "none":
        x_bin_ns, y_bin_ns = binning(x_ns, y_ns, binsize=bin_interval)[:2]
    else:
        x_bin_ns, y_bin_ns = x_ns, y_ns

    peak_pos_ns = x_bin_ns[list(y_bin_ns).index(max(y_bin_ns))]

    def fit_function(x, x0, a, sigma, b, c):
        """Gaussian function with linear offset"""
        return a * np.exp(-((x - x0) ** 2) / (2 * sigma**2)) + b + c * (x - x0)

    # fit1 starting values
    p0 = [peak_pos_ns, 1, 1e-3, min(y_bin_ns), 1e-6]
    p0_fit1 = []
    for i in range(len(startvalues_fit1)):
        item = startvalues_fit1[i]
        if item == "auto":
            p0_fit1.append(p0[i])
        else:
            p0_fit1.append(item)

    try:
        popt_ns, pcov = curve_fit(fit_function, x_ns, y_ns, p0=p0_fit1)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        # sdev_ns = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        # serr_ns = sdev_ns / (np.sqrt(len(x_ns)))
        fit1_failed = False
    except RuntimeError:
        print("fit failed!")
        fit1_failed = True
        popt_ns, pcov = [0, 0, 0, 0, 0], np.zeros((5, 5))

    # Versuche fits auszulagern
    # fit1_failed=False
    # popt_ns, sdev_ns, serr_ns = absorption_fit(x_ns, y_ns, startvalues=startvalues_fit1)

    if not fit1_failed:
        peak_pos_ns = popt_ns[0]
        sigma_ns = abs(popt_ns[2])  # in nm pump radiation2
        sdev_idler_ns = nu2hz(sigma_ns)
        fwhm_ns = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_ns * 1e-6
        print("ns", fwhm_ns, "MHz")

    temp = create_2dlist(data, "wavemeter", "IRPD2-cw")
    x_cw, y_cw = nm2nu(idler2signal(signal_wavelength, np.array(temp).T[0])), -(
        np.array(temp).T[1] - background_cw
    )
    # x_cw, y_cw = data[file_name]['wavemeter'], -np.array(data[file_name]['IRPD2-cw'])
    if not bin_interval == "none":
        x_bin_cw, y_bin_cw = binning(x_cw, y_cw, binsize=bin_interval)[:2]
    else:
        x_bin_cw, y_bin_cw = x_cw, y_cw

    peak_pos = x_bin_cw[list(y_bin_cw).index(max(y_bin_cw))]

    # fit1 starting values
    p0 = [peak_pos, 1, 1e-3, min(y_bin_cw), 1e-6]
    p0_fit2 = []
    for i in range(len(startvalues_fit2)):
        item = startvalues_fit2[i]
        if item == "auto":
            p0_fit2.append(p0[i])
        else:
            p0_fit2.append(item)

    try:
        popt_cw, pcov = curve_fit(fit_function, x_cw, y_cw, p0=p0_fit2)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        # sdev_cw = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        # serr_cw = sdev_cw / (np.sqrt(len(x_cw)))
        fit2_failed = False
    except RuntimeError:
        print("fit failed!")
        fit2_failed = True
        popt_ns = [0, 0, 0, 0, 0]
        # pcov = np.zeros((5, 5))

    if not fit2_failed:
        peak_pos_cw = popt_cw[0]
        sigma_cw = abs(popt_cw[2])  # in nm pump radiation2
        sdev_idler_cw = nu2hz(sigma_cw)
        fwhm_cw = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_cw * 1e-6
        print("cw:", fwhm_cw, "MHz")

    if not fit1_failed and not fit2_failed and fwhm_ns >= fwhm_cw:
        fwhm_laser = np.sqrt(fwhm_ns**2 - fwhm_cw**2)
        print("laser:", fwhm_laser, "MHz")

    # normalized fit?
    if normalized:
        x_ns_plot = (nu2hz(x_ns) - nu2hz(peak_pos_ns)) * 1e-6
        x_ns_fit = x_ns - peak_pos_ns  # trafo for fit
        x_bin_ns_plot = (nu2hz(x_bin_ns) - nu2hz(peak_pos_ns)) * 1e-6
        x_cw_plot = (nu2hz(x_cw) - nu2hz(peak_pos_cw)) * 1e-6
        x_cw_fit = x_cw - peak_pos_cw  # trafo for fit
        x_bin_cw_plot = (nu2hz(x_bin_cw) - nu2hz(peak_pos_cw)) * 1e-6
        xlabel = "detuning [MHz]"
        # normalize
        y_ns_plot = y_ns / popt_ns[3]
        y_bin_ns_plot = y_bin_ns / popt_ns[3]
        y_cw_plot = y_cw / popt_cw[3]
        y_bin_cw_plot = y_bin_cw / popt_cw[3]

        popt_ns_plot = [0, -popt_ns[1] / popt_ns[3], popt_ns[2], -1, popt_ns[4] / popt_ns[3]]
        popt_cw_plot = [0, -popt_cw[1] / popt_cw[3], popt_cw[2], -1, popt_cw[4] / popt_cw[3]]
        ylabel1, ylabel2 = "ns transmission", "cw transmission"
        sigma_ns = 1e-6 * nu2hz(popt_ns[2])
        sigma_cw = 1e-6 * nu2hz(popt_cw[2])

    else:
        x_ns_plot, x_bin_ns_plot = x_ns, x_bin_ns
        x_cw_plot, x_bin_cw_plot = x_cw, x_bin_cw
        x_ns_fit, x_cw_fit = x_ns, x_cw
        y_ns_plot, y_bin_ns_plot = -y_ns, -y_bin_ns
        y_cw_plot, y_bin_cw_plot = -y_cw, -y_bin_cw

        popt_ns_plot, popt_cw_plot = popt_ns, popt_cw
        xlabel = f"wavenumber[cm$^{-1}$]"
        ylabel1, ylabel2 = "IRPD2 ns [arb. u.]", "IRPD2 cw [arb. u.]"

    # plots
    fig, (ax1, ax2) = plt.subplots(1, 2)
    # fig.set_dpi(300)
    plot_options(
        fig,
        ax1,
        font_size=10,
        image_width=30,
        ticks=["auto", "auto"],
        xlabel=xlabel,
        ylabel=ylabel1,
        aspect_ratio=2 * scipy.constants.golden,
    )

    ax1.plot(x_ns_plot, y_ns_plot, "g", label="OPA 1", markersize=5)
    ax1.plot(x_bin_ns_plot, y_bin_ns_plot, "b.", label="OPA 1", markersize=5)

    if not fit1_failed:
        ax1.plot(
            x_ns_plot, -fit_function(x_ns_fit, *popt_ns_plot), "r", label="OPA 1", markersize=5
        )
    # x, y = data[file_name]['wavemeter'], np.array(data[file_name]['IRPD2-cw'])
    # plt.plot(x, y, 'r', label='OPA 1', markersize=5)
    # plt.plot(x, np.zeros(len(x)) + back_pd2, 'g')
    if normalized:
        ax1.set_xlim([-5 * sigma_ns, 5 * sigma_ns])

    # global!
    # plt.ylim([0, 40])

    plot_options(
        fig,
        ax2,
        font_size=10,
        image_width=30,
        ticks=["auto", "auto"],
        xlabel=xlabel,
        ylabel=ylabel2,
        aspect_ratio=2 * scipy.constants.golden,
    )

    ax2.plot(x_cw_plot, y_cw_plot, "g", label="OPA 1", markersize=5)
    ax2.plot(x_bin_cw_plot, y_bin_cw_plot, "b.", label="OPA 1", markersize=5)
    if not fit2_failed:
        ax2.plot(
            x_cw_plot, -fit_function(x_cw_fit, *popt_cw_plot), "r", label="OPA 1", markersize=5
        )

    if normalized:
        ax2.set_xlim([-5 * sigma_cw, 5 * sigma_cw])

    plt.tight_layout()
    plt.show()
    return popt_ns, popt_cw


def transmission_overview4(
    data,
    bin_interval,
    signal_wavelength,
    startvalues=["auto", "auto", "auto", "auto", "auto"],
    normalized=True,
    print_output=True,
    background_cw=0,
    dataset="cw",
    fit_direction="both",
    plot_filename=False,
):
    """
    dataset : 'cw' or 'ns'
    fit_direction : 'both': Standard, both directions, 'up', 'down'
    """
    if dataset == "cw":
        temp = create_2dlist(
            data, "wavemeter", "IRPD2-cw", operation_y=lambda x: x - background_cw
        )
    else:
        temp = create_2dlist(data, "wavemeter", "IRPD2")

    x, y = (
        nm2nu(idler2signal(signal_wavelength, np.array(temp).T[0])),
        -np.array(temp).T[1],
    )
    transmission_plot(
        x,
        y,
        bin_interval,
        startvalues,
        normalized,
        print_output,
        background_cw,
        fit_direction,
        plot_filename,
    )


def transmission_overview5(
    data,
    bin_interval,
    startvalues=["auto", "auto", "auto", "auto", "auto"],
    normalized=True,
    print_output=True,
    background_cw=0,
    dataset="cw",
    fit_direction="both",
    plot_filename=False,
):
    """
    dataset : 'cw' or 'ns'
    fit_direction : 'both': Standard, both directions, 'up', 'down'
    """
    if dataset == "cw":
        temp = create_2dlist(
            data, "idler_wl", "transmission", operation_y=lambda x: x - background_cw
        )
    else:
        temp = create_2dlist(data, "idler_wl", "PD2")

    x, y = nm2nu(np.array(temp).T[0]), -np.array(temp).T[1]
    return transmission_plot(
        x,
        y,
        bin_interval,
        startvalues,
        normalized,
        print_output,
        background_cw,
        dataset,
        fit_direction,
        plot_filename,
    )


def transmission_plot(
    x,
    y,
    bin_interval,
    startvalues=["auto", "auto", "auto", "auto", "auto"],
    normalized=True,
    print_output=True,
    background_cw=0,
    dataset="cw",
    fit_direction="both",
    plot_filename=False,
):
    def fit_function_gauss(x, x0, a, sigma, b, c):
        """Gaussian function with linear offset"""
        return a * np.exp(-((x - x0) ** 2) / (2 * sigma**2)) + b + c * (x - x0)

    def fit_function_voigt(x, x0, a, sigma, b, c, gamma):
        """Voigt function with linear offset"""
        return a * voigt_profile(x - x0, sigma, gamma) + b + c * (x - x0)

    # order for direction of the wavelength measurement: left or right
    x_r = []
    x_l = []
    y_r = []
    y_l = []
    for i in range(len(x) - 1):
        if x[i + 1] - x[i] > 0:
            x_r.append(x[i])
            y_r.append(y[i])
        elif x[i + 1] - x[i] < 0:
            x_l.append(x[i])
            y_l.append(y[i])
    x_r = np.array(x_r)
    y_r = np.array(y_r)
    x_l = np.array(x_l)
    y_l = np.array(y_l)

    if not bin_interval == "none":
        x_bin_r, y_bin_r = binning(x_r, y_r, binsize=bin_interval)[:2]
        x_bin_l, y_bin_l = binning(x_l, y_l, binsize=bin_interval)[:2]
    else:
        x_bin_r, y_bin_r = x_r, y_r
        x_bin_l, y_bin_l = x_l, y_l

    peak_pos = x_bin_r[list(y_bin_r).index(max(y_bin_r))]

    # fit1 starting values
    p0 = [peak_pos, 1, 1e-3, min(y_bin_r), 1e-6]
    p0_gauss = []
    for i in range(len(startvalues)):
        item = startvalues[i]
        if item == "auto":
            p0_gauss.append(p0[i])
        else:
            p0_gauss.append(item)
    #  just add element to start values of the voigt fit
    p0_voigt = [peak_pos, 1, 1e-3, min(y_bin_r), 1e-6, 1e-3]

    # fit1 (right) Gauss
    try:
        popt_r, pcov = curve_fit(fit_function_gauss, x_r, y_r, p0=p0_gauss)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_r = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_r = sdev_r / (np.sqrt(len(x_r)))
        fit1_failed = False
    except RuntimeError:
        print("Gauss fit (right) failed!")
        fit1_failed = True
        popt_r, pcov = [0, 0, 0, 0, 0], np.zeros((5, 5))

    if not fit1_failed:
        peak_pos_r = popt_r[0]
        sigma_r = abs(popt_r[2])
        sdev_idler_r = nu2hz(sigma_r)
        fwhm_r = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_r * 1e-6

    # fit1 (right) Voigt
    try:
        popt_r_voigt, pcov_voigt = curve_fit(fit_function_voigt, x_r, y_r, p0=p0_voigt)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_r_voigt = np.sqrt(abs(pcov_voigt.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_r_voigt = sdev_r_voigt / (np.sqrt(len(x_r)))
        fit1_failed = False
    except RuntimeError:
        print("Voigt fit (right) failed!")
        fit1_failed = True
        popt_r_voigt, pcov_voigt = [0, 0, 0, 0, 0, 0], np.zeros((6, 6))

    if not fit1_failed:
        peak_pos_r = popt_r_voigt[0]
        sigma_r_voigt = abs(popt_r_voigt[2])
        fwhm_r_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_r_voigt) * 1e-6
        fwhm_r_voigt_lorentz = (
            2 * nu2hz(abs(popt_r_voigt[5])) * 1e-6
        )  # gamma in Lorentzian function is HWHM

        if print_output:
            print(
                f"{dataset} (right): Gauss: {fwhm_r:.3f}MHz, Voigt-Fit: Gauss: "
                f"{fwhm_r_voigt_gauss:.3f}MHz, Lorentz: {fwhm_r_voigt_lorentz:.3f}MHz"
            )

    # fit2 (left)
    try:
        popt_l, pcov = curve_fit(fit_function_gauss, x_l, y_l, p0=p0_gauss)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_l = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_l = sdev_l / (np.sqrt(len(x_l)))
        fit2_failed = False
    except RuntimeError:
        print("fit failed!")
        fit2_failed = True
        popt_l, pcov = [0, 0, 0, 0, 0], np.zeros((5, 5))

    if not fit2_failed:
        peak_pos_l = popt_l[0]
        sigma_l = abs(popt_l[2])
        sdev_idler_l = nu2hz(sigma_l)
        fwhm_l = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_l * 1e-6

    # fit2 (left) Voigt
    try:
        popt_l_voigt, pcov_voigt = curve_fit(fit_function_voigt, x_l, y_l, p0=p0_voigt)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_l_voigt = np.sqrt(abs(pcov_voigt.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_l_voigt = sdev_l_voigt / (np.sqrt(len(x_l)))
        fit2_failed = False
    except RuntimeError:
        print("Voigt fit (right) failed!")
        fit2_failed = True
        popt_l_voigt, pcov_voigt = [0, 0, 0, 0, 0, 0], np.zeros((6, 6))

    if not fit2_failed:
        peak_pos_l = popt_l_voigt[0]
        sigma_l_voigt = abs(popt_l_voigt[2])
        fwhm_l_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_l_voigt) * 1e-6
        fwhm_l_voigt_lorentz = (
            2 * nu2hz(abs(popt_l_voigt[5])) * 1e-6
        )  # gamma in Lorentzian function is HWHM

        if print_output:
            print(
                f"{dataset} (left): Gauss: {fwhm_l:.3f}MHz, Voigt-Fit: Gauss: "
                f"{fwhm_l_voigt_gauss:.3f}MHz, Lorentz: {fwhm_l_voigt_lorentz:.3f}MHz"
            )

    # normalized fit?
    if normalized:
        x_r_plot = (nu2hz(x_r) - nu2hz(peak_pos_r)) * 1e-6
        x_r_fit = x_r - peak_pos_r  # trafo for fit
        x_bin_r_plot = (nu2hz(x_bin_r) - nu2hz(peak_pos_r)) * 1e-6
        x_l_plot = (nu2hz(x_l) - nu2hz(peak_pos_l)) * 1e-6
        x_l_fit = x_l - peak_pos_l  # trafo for fit
        x_bin_l_plot = (nu2hz(x_bin_l) - nu2hz(peak_pos_l)) * 1e-6
        xlabel = "detuning [MHz]"
        # normalize
        y_r_plot = y_r / popt_r[3]
        y_bin_r_plot = y_bin_r / popt_r[3]
        y_l_plot = y_l / popt_l[3]
        y_bin_l_plot = y_bin_l / popt_l[3]
        # normalize fit results, invert and orient around 0MHz
        popt_r_plot = [0, -popt_r[1] / popt_r[3], popt_r[2], -1, -popt_r[4] / popt_r[3]]
        popt_l_plot = [0, -popt_l[1] / popt_l[3], popt_l[2], -1, -popt_l[4] / popt_l[3]]
        popt_r_plot_voigt = [
            0,
            -popt_r_voigt[1] / popt_r_voigt[3],
            popt_r_voigt[2],
            -1,
            -popt_r_voigt[4] / popt_r_voigt[3],
            popt_r_voigt[5],
        ]
        popt_l_plot_voigt = [
            0,
            -popt_l_voigt[1] / popt_l_voigt[3],
            popt_l_voigt[2],
            -1,
            -popt_l_voigt[4] / popt_l_voigt[3],
            popt_l_voigt[5],
        ]

        ylabel1, ylabel2 = "transmission (right)", "transmission (left)"
        sigma_r = 1e-6 * nu2hz(popt_r[2])
        sigma_l = 1e-6 * nu2hz(popt_l[2])

        """def fit_function(x, x0, a, sigma, b, c):
        Gaussian function with linear offset
        return a*np.exp(-(x-x0)**2/(2*sigma**2))+b+c*(x-x0)"""

    else:
        x_r_plot, x_bin_r_plot = x_r, x_bin_r
        x_l_plot, x_bin_l_plot = x_l, x_bin_l
        x_r_fit, x_l_fit = x_r, x_l
        y_r_plot, y_bin_r_plot = -y_r, -y_bin_r
        y_l_plot, y_bin_l_plot = -y_l, -y_bin_l

        popt_r_plot, popt_l_plot = popt_r, popt_l
        popt_r_plot_voigt, popt_l_plot_voigt = popt_r_voigt, popt_l_voigt
        xlabel = f"wavenumber[cm$^{-1}$]"
        ylabel1, ylabel2 = "IRPD2 ns [arb. u.]", "IRPD2 cw [arb. u.]"

    # derive the correct OPO pump wavelength from the calculation
    # use the left value from the Gauss fit, this should work every time.
    # center_idler_nu = popt_l[0]
    # center_idler_nm = nu2nm(center_idler_nu)
    # center_pump_nm = (1/center_idler_nm + 1/signal_wavelength)**(-1)
    # signal = (1 / pump - 1 / idler) ** (-1)

    # plots
    font_size = 10
    fig, (ax1, ax2) = plt.subplots(1, 2)

    plot_options(
        fig,
        ax1,
        font_size=font_size,
        image_width=30,
        ticks=["auto", "auto"],
        xlabel=xlabel,
        ylabel=ylabel2,
        aspect_ratio=2 * scipy.constants.golden,
    )
    ax1.set_title(dataset)
    ax1.plot(x_l_plot, y_l_plot, ".", color="lightsteelblue", label="data", markersize=5)
    ax1.plot(x_bin_l_plot, y_bin_l_plot, "o", color="darkslateblue", label="binned", markersize=5)

    if not fit2_failed:
        ax1.plot(
            np.sort(x_l_plot),
            -fit_function_gauss(np.sort(x_l_fit), *popt_l_plot),
            "-",
            color="limegreen",
            label="Gauss",
            markersize=5,
        )
        ax1.plot(
            np.sort(x_l_plot),
            -fit_function_voigt(np.sort(x_l_fit), *popt_l_plot_voigt),
            "r",
            label="Voigt",
            markersize=5,
        )
        ax1.legend(fontsize=font_size, numpoints=1, ncol=1, frameon=True)  # loc="upper right",

    if normalized:
        ax1.set_xlim([-5 * abs(sigma_l), abs(5 * sigma_l)])
    plot_options(
        fig,
        ax2,
        font_size=font_size,
        image_width=30,
        ticks=["auto", "auto"],
        xlabel=xlabel,
        ylabel=ylabel1,
        aspect_ratio=2 * scipy.constants.golden,
    )
    ax2.set_title(dataset)
    ax2.plot(x_r_plot, y_r_plot, ".", color="lightsteelblue", label="data", markersize=5)
    ax2.plot(x_bin_r_plot, y_bin_r_plot, "o", color="darkslateblue", label="binned", markersize=5)

    if not fit1_failed:
        ax2.plot(
            np.sort(x_r_plot),
            -fit_function_gauss(np.sort(x_r_fit), *popt_r_plot),
            "-",
            color="limegreen",
            label="Gauss",
            markersize=5,
        )
        ax2.plot(
            np.sort(x_r_plot),
            -fit_function_voigt(np.sort(x_r_fit), *popt_r_plot_voigt),
            "r",
            label="Voigt",
            markersize=5,
        )
        ax2.legend(fontsize=font_size, numpoints=1, ncol=1, frameon=True)  # loc="upper right",

    if normalized:
        ax2.set_xlim([-5 * abs(sigma_r), 5 * abs(sigma_r)])

    plt.tight_layout()

    if plot_filename is not False:
        save_plot(plot_filename, print_output=True)

    plt.show()
    plot_stuff = (
        x_l_plot,
        y_l_plot,
        x_bin_l_plot,
        y_bin_l_plot,
        np.sort(x_l_plot),
        -fit_function_gauss(np.sort(x_l_fit), *popt_l_plot),
    )
    return (
        [popt_r, sdev_r, serr_r],
        [popt_l, sdev_l, serr_l],
        [popt_r_voigt, sdev_r_voigt, serr_r_voigt],
        [popt_l_voigt, sdev_l_voigt, serr_l_voigt],
        0,
        plot_stuff,
    )  # vorletzer eintrag: center_pump_nm


def binning(x, y, bins=10, binsize=False):
    """
    Calculate the statistics of binned data.

    NaN values are ignored.
    If `binsize` is used, the rightmost bin might contain less data points than
    the rest of the bins.

    Parameters
    ----------
    x : array-like
        The base data which should contain the bins later on.
    y : array-like
        The data to put into the bins.
    bins : int, optional
        Number of bins between minimum and maximum x values.
    binsize : float, optional
        Size of a single bin. If this parameter is set, `bins` is ignored.

    Returns
    -------
    bins : array
        Mean of the borders of the bins.
    mean : array
        Mean of the values in the bins.
    std : array
        Standard deviation of the values in the bins.
    count : array
        Count of values in the bins.
    """
    if type(x) is not np.array:
        x = np.array(x)
    if type(y) is not np.array:
        y = np.array(y)
    start = np.nanmin(x)
    stop = np.nanmax(x)
    if not binsize:
        binsize = (stop - start) / bins
    bins = []
    mean = []
    std = []
    count = []
    position = start
    while position < stop:
        bins.append(position + binsize / 2)
        indices = (position <= x) * (x < (position + binsize))
        mean.append(np.nanmean(y[indices]))
        std.append(np.nanstd(y[indices]))
        count.append(len(y[indices]))
        position += binsize

    return np.array(bins), np.array(mean), np.array(std), np.array(count)


"# Mathematical functions"


def exponential(x, factor=1, exponent=1, offset=0):
    """An exponential function with offset."""
    return factor * x**exponent + offset


# def gauss(x, amplitude=1, x0=0, sigma=1, offset=0):
#     """
#     A gaussian curve with the amplitude as maximum minus offset.

#     For `amplitude = 1/sigma/np.sqrt(2*np.pi)` it is a density distribution.
#     FWHM = 2 * np.sqrt(2 * np.log(2)) * sigma
#     """
#     return amplitude * np.exp(-0.5 * ((x - x0) / sigma)**2) + offset


# def lorentzian(x, amplitude=1, x0=0, gamma=1, offset=0):
#     """
#     A lorentzian curve.

#     For `amplitude = 1 / np.pi / gamma` it is a probability density.
#     gamma is half width at half max. FWHM is 2 * gamma.
#     """
#     return amplitude * gamma**2 / ((x - x0)**2 + gamma**2) + offset


def voigt(x, factor=1, mu=0, sigma=0, gamma=0, offset=0):
    """
    Voigt profile.

    Parameters
    ----------
    :param: factor : Factor of the profile.
    :param: mu : Center of the profile.
    :param: sigma : Standard deviation of the gaussian part.
    :param: gamma : half-width at half-maximumm (HWHM) of the lorentzian part.
    :param: offset : Offset.
    """
    return factor * voigt_profile(x - mu, sigma, gamma) + offset


def spectrum_overview1(
    data,
    bin_interval,
    signal_wavelength,
    startvalues=["auto", "auto", "auto", "auto"],
    normalized=True,
    print_output=True,
    dataset="SFM",
    plot_filename=False,
    fixed_gauss=False,  # if not false: fixes Gaussian component of Voigt-fit to FWHM, cm-1
    show_plot=True,
    fixed_lorentz=False,  # if not false: fixes Gaussian component of Voigt-fit to FWHM, cm-1
    background_sfm=0,
    logscale=False,
    pump_key="wavemeter",
):
    """
    dataset : 'cw' or 'ns'
    fit_direction : 'both': Standard, both directions, 'up', 'down'
    """
    if dataset == "SFM":
        temp = create_2dlist(data, pump_key, "PMT")
    else:
        temp = create_2dlist(data, pump_key, "APD")

    # factor 2 in wavenumber, because the relevant quantity is the two-photon detuning!
    x, y = 2 * nm2nu(idler2signal(signal_wavelength, np.array(temp).T[0])), np.array(temp).T[1]

    # remove zero values for SFM data
    if dataset == "SFM":
        index = y > background_sfm
        x, y = x[index], y[index]

    def fit_function_gauss(x, x0, a, sigma, b):
        """Gaussian function with linear offset, sigma: standard deviation"""
        return a * np.exp(-((x - x0) ** 2) / (2 * sigma**2)) + b

    # order for direction of the wavelength measurement: left or right
    x_r = []
    x_l = []
    y_r = []
    y_l = []
    for i in range(len(x) - 1):
        if x[i + 1] - x[i] > 0:
            x_r.append(x[i])
            y_r.append(y[i])
        elif x[i + 1] - x[i] < 0:
            x_l.append(x[i])
            y_l.append(y[i])
    x_r = np.array(x_r)
    y_r = np.array(y_r)
    x_l = np.array(x_l)
    y_l = np.array(y_l)

    if not bin_interval == "none":
        x_bin_r, y_bin_r = binning(x_r, y_r, binsize=bin_interval)[:2]
        x_bin_l, y_bin_l = binning(x_l, y_l, binsize=bin_interval)[:2]
    else:
        x_bin_r, y_bin_r = x_r, y_r
        x_bin_l, y_bin_l = x_l, y_l

    peak_pos = x_bin_r[list(y_bin_r).index(max(y_bin_r))]

    # fit1 starting values
    p0 = [peak_pos, max(y_bin_r), 1e-3, 1e-6]  # x0, a, sigma, b
    p0_gauss = []
    for i in range(len(startvalues)):
        item = startvalues[i]
        if item == "auto":
            p0_gauss.append(p0[i])
        else:
            p0_gauss.append(item)

    # fix Gaussian or Lorentzian component in the Voigtfit (not both)
    if not fixed_gauss and not fixed_lorentz:
        print("no fit possible!")

    if not fixed_gauss:
        if not fixed_lorentz:
            p0_voigt = [peak_pos, max(y_bin_r), 1e-6, 1e-3, 1e-3]  # x0, a, sigma, b, gamma

            def fit_function_voigt(x, x0, a, sigma, b, gamma):
                """Voigt function with linear offset, sigma: standard deviation of the Gaussian
                component, gamma: half-width at half maximum of the Lorentzian component"""
                return a * voigt_profile(x - x0, sigma, gamma) + b

        else:
            p0_voigt = [peak_pos, max(y_bin_r), 1e-3, 1e-3]  # x0, a, sigma, b

            def fit_function_voigt(x, x0, a, sigma, b):
                """Voigt function with linear offset, sigma: standard deviation of the Gaussian
                component, gamma: half-width at half maximum of the Lorentzian component"""
                return a * voigt_profile(x - x0, sigma, fixed_lorentz / 2) + b

    else:
        p0_voigt = [peak_pos, max(y_bin_r), 1e-3, 1e-3]  # x0, a, b, gamma

        def fit_function_voigt(x, x0, a, b, gamma):
            """Voigt function with linear offset, sigma: standard deviation of the Gaussian
            component, gamma: half-width at half maximum of the Lorentzian component"""
            return a * voigt_profile(x - x0, fixed_gauss / (2 * np.sqrt(2 * np.log(2))), gamma) + b

    # fit1 (right) Gauss
    try:
        popt_r, pcov = curve_fit(fit_function_gauss, x_r, y_r, p0=p0_gauss)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_r = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_r = sdev_r / (np.sqrt(len(x_r)))
        fit1_failed = False
    except RuntimeError:
        print("Gauss fit (right) failed!")
        fit1_failed = True
        popt_r, pcov = [0, 0, 0, 0], np.zeros((4, 4))

    if not fit1_failed:
        peak_pos_r = popt_r[0]
        sigma_r = abs(popt_r[2])
        sdev_idler_r = nu2hz(sigma_r)
        fwhm_r = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_r * 1e-6

    # fit1 (right) Voigt
    try:
        popt_r_voigt, pcov_voigt = curve_fit(fit_function_voigt, x_r, y_r, p0=p0_voigt)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_r_voigt = np.sqrt(abs(pcov_voigt.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_r_voigt = sdev_r_voigt / (np.sqrt(len(x_r)))
        fit1_failed = False
    except RuntimeError:
        print("Voigt fit (right) failed!")
        fit1_failed = True
        if not fixed_gauss or fixed_lorentz:
            popt_r_voigt, pcov_voigt = [0, 0, 0, 0, 0], np.zeros((5, 5))
        else:
            popt_r_voigt, pcov_voigt = [0, 0, 0, 0], np.zeros((4, 4))

    if not fit1_failed:
        peak_pos_r = popt_r_voigt[0]
        if not fixed_gauss:
            if not fixed_lorentz:
                sigma_r_voigt = abs(popt_r_voigt[2])
                fwhm_r_voigt_lorentz = (
                    2 * nu2hz(abs(popt_r_voigt[4])) * 1e-6
                )  # gamma in Lorentzian function is HWHM
                fwhm_r_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_r_voigt) * 1e-6
            else:
                sigma_r_voigt = abs(popt_r_voigt[2])
                fwhm_r_voigt_lorentz = nu2hz(fixed_lorentz) * 1e-6
                fwhm_r_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_r_voigt) * 1e-6
        else:
            fwhm_r_voigt_lorentz = (
                2 * nu2hz(abs(popt_r_voigt[3])) * 1e-6
            )  # gamma in Lorentzian function is HWHM
            fwhm_r_voigt_gauss = nu2hz(fixed_gauss) * 1e-6

        fwhm_r_voigt = voigt_fwhm(fwhm_r_voigt_gauss, fwhm_r_voigt_lorentz)
        if print_output:
            print(
                f"{dataset} (right): Gauss: {fwhm_r:.3f}MHz, Voigt-Fit: Gauss: "
                f"{fwhm_r_voigt_gauss:.3f}MHz, Lorentz: {fwhm_r_voigt_lorentz:.3f}MHz, "
                f"Voigt: {fwhm_r_voigt:.3f}MHz"
            )

    # fit2 (left)
    try:
        popt_l, pcov = curve_fit(fit_function_gauss, x_l, y_l, p0=p0_gauss)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_l = np.sqrt(abs(pcov.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_l = sdev_l / (np.sqrt(len(x_l)))
        fit2_failed = False
    except RuntimeError:
        print("fit failed!")
        fit2_failed = True
        popt_l, pcov = [0, 0, 0, 0], np.zeros((4, 4))

    if not fit2_failed:
        peak_pos_l = popt_l[0]
        sigma_l = abs(popt_l[2])
        sdev_idler_l = nu2hz(sigma_l)
        fwhm_l = 2 * np.sqrt(2 * np.log(2)) * sdev_idler_l * 1e-6

    # fit2 (left) Voigt
    try:
        popt_l_voigt, pcov_voigt = curve_fit(fit_function_voigt, x_l, y_l, p0=p0_voigt)
        # covariance matrix: diagonal elements are the variances, hence, the square root is the
        # standard deviation
        sdev_l_voigt = np.sqrt(abs(pcov_voigt.diagonal()).tolist())
        # standard errors are sd/sqrt(n) with the sample size n
        serr_l_voigt = sdev_l_voigt / (np.sqrt(len(x_l)))
        fit2_failed = False
    except RuntimeError:
        print("Voigt fit (right) failed!")
        fit2_failed = True
        if not fixed_gauss or fixed_lorentz:
            popt_l_voigt, pcov_voigt = [0, 0, 0, 0, 0], np.zeros((5, 5))
        else:
            popt_l_voigt, pcov_voigt = [0, 0, 0, 0], np.zeros((4, 4))
    """
    if not fit2_failed:
        peak_pos_l = popt_l_voigt[0]
        if not fixed_gauss:
            sigma_l_voigt = abs(popt_l_voigt[2])
            fwhm_l_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_l_voigt) * 1e-6
            fwhm_l_voigt_lorentz = 2 * nu2hz(abs(popt_l_voigt[4])) * 1e-6
            # gamma in Lorentzian function is HWHM
        else:
            sigma_l_voigt = fixed_gauss
            fwhm_l_voigt_lorentz = 2 * nu2hz(abs(popt_l_voigt[3])) * 1e-6
            # gamma in Lorentzian function is HWHM
            fwhm_l_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_l_voigt) * 1e-6
        fwhm_l_voigt = voigt_fwhm(fwhm_l_voigt_gauss, fwhm_l_voigt_lorentz)"""

    if not fit2_failed:
        peak_pos_l = popt_l_voigt[0]
        if not fixed_gauss:
            if not fixed_lorentz:
                sigma_l_voigt = abs(popt_l_voigt[2])
                fwhm_l_voigt_lorentz = (
                    2 * nu2hz(abs(popt_l_voigt[4])) * 1e-6
                )  # gamma in Lorentzian function is HWHM
                fwhm_l_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_l_voigt) * 1e-6
            else:
                sigma_l_voigt = abs(popt_l_voigt[2])
                fwhm_l_voigt_lorentz = nu2hz(fixed_lorentz) * 1e-6
                fwhm_l_voigt_gauss = 2 * np.sqrt(2 * np.log(2)) * nu2hz(sigma_l_voigt) * 1e-6
        else:
            fwhm_l_voigt_lorentz = (
                2 * nu2hz(abs(popt_l_voigt[3])) * 1e-6
            )  # gamma in Lorentzian function is HWHM
            fwhm_l_voigt_gauss = nu2hz(fixed_gauss) * 1e-6

        fwhm_l_voigt = voigt_fwhm(fwhm_l_voigt_gauss, fwhm_l_voigt_lorentz)

        if print_output:
            print(
                f"{dataset} (left): Gauss: {fwhm_l:.3f}MHz, Voigt-Fit: Gauss: "
                f"{fwhm_l_voigt_gauss:.3f}MHz, Lorentz: {fwhm_l_voigt_lorentz:.3f}MHz,"
                f" Voigt: {fwhm_l_voigt:.3f}MHz"
            )

    # normalized fit?
    if normalized:
        x_r_plot = (nu2hz(x_r) - nu2hz(peak_pos_r)) * 1e-6
        x_r_fit = x_r - peak_pos_r  # trafo for fit
        x_bin_r_plot = (nu2hz(x_bin_r) - nu2hz(peak_pos_r)) * 1e-6
        x_l_plot = (nu2hz(x_l) - nu2hz(peak_pos_l)) * 1e-6
        x_l_fit = x_l - peak_pos_l  # trafo for fit
        x_bin_l_plot = (nu2hz(x_bin_l) - nu2hz(peak_pos_l)) * 1e-6
        xlabel = "detuning [MHz]"
        # normalize
        y_r_plot = y_r / popt_r[1]
        y_bin_r_plot = y_bin_r / popt_r[1]
        y_l_plot = y_l / popt_l[1]
        y_bin_l_plot = y_bin_l / popt_l[1]

        # normalize fit results and orient around 0MHz
        popt_r_plot = [0, 1, popt_r[2], popt_r[3] / popt_r[1]]
        popt_l_plot = [0, 1, popt_l[2], popt_l[3] / popt_l[1]]
        # careful with Voigt fit! => rescale with Gaussian fit result
        if not fixed_gauss:
            if not fixed_lorentz:
                popt_r_plot_voigt = [
                    0,
                    popt_r_voigt[1] / popt_r[1],
                    popt_r_voigt[2],
                    popt_r_voigt[3] / popt_r[1],
                    popt_r_voigt[4],
                ]
                popt_l_plot_voigt = [
                    0,
                    popt_l_voigt[1] / popt_l[1],
                    popt_l_voigt[2],
                    popt_l_voigt[3] / popt_l[1],
                    popt_l_voigt[4],
                ]
            else:
                popt_r_plot_voigt = [
                    0,
                    popt_r_voigt[1] / popt_r[1],
                    popt_r_voigt[2],
                    popt_r_voigt[3] / popt_r[1],
                ]
                popt_l_plot_voigt = [
                    0,
                    popt_l_voigt[1] / popt_l[1],
                    popt_l_voigt[2],
                    popt_l_voigt[3] / popt_l[1],
                ]
        else:
            popt_r_plot_voigt = [
                0,
                popt_r_voigt[1] / popt_r[1],
                popt_r_voigt[2] / popt_r[1],
                popt_r_voigt[3],
            ]
            popt_l_plot_voigt = [
                0,
                popt_l_voigt[1] / popt_l[1],
                popt_l_voigt[2] / popt_l[1],
                popt_l_voigt[3],
            ]

        # Gauss: x0, a, sigma, b
        # Voigt: x0, a, sigma, b, gamma

        ylabel2, ylabel1 = (
            dataset + " signal pulse energy (left) [arb. u.]",
            dataset + " signal pulse energy (right) [arb. u.]",
        )
        sigma_r = 1e-6 * nu2hz(popt_r[2])
        sigma_l = 1e-6 * nu2hz(popt_l[2])

        """def fit_function(x, x0, a, sigma, b, c):
        Gaussian function with linear offset
        return a*np.exp(-(x-x0)**2/(2*sigma**2))+b+c*(x-x0)"""

    else:
        x_r_plot, x_bin_r_plot = x_r, x_bin_r
        x_l_plot, x_bin_l_plot = x_l, x_bin_l
        x_r_fit, x_l_fit = x_r, x_l
        y_r_plot, y_bin_r_plot = y_r, y_bin_r
        y_l_plot, y_bin_l_plot = y_l, y_bin_l

        popt_r_plot, popt_l_plot = popt_r, popt_l
        popt_r_plot_voigt, popt_l_plot_voigt = popt_r_voigt, popt_l_voigt
        xlabel = f"wavenumber[cm$^{-1}$]"
        ylabel2, ylabel1 = (
            dataset + " signal pulse energy (left) [arb. u.]",
            dataset + " signal pulse energy (right) [arb. u.]",
        )

    # derive the correct OPO pump wavelength from the calculation
    # use the left value from the Gauss fit, this should work every time.
    center_idler_nu = popt_l[0]
    center_idler_nm = nu2nm(center_idler_nu)
    center_pump_nm = (1 / center_idler_nm + 1 / signal_wavelength) ** (-1)
    # signal = (1 / pump - 1 / idler) ** (-1)

    # plots
    if show_plot:
        font_size = 10
        fig, (ax1, ax2) = plt.subplots(1, 2)

        plot_options(
            fig,
            ax1,
            font_size=font_size,
            image_width=30,
            ticks=["auto", "auto"],
            xlabel=xlabel,
            ylabel=ylabel2,
            aspect_ratio=2 * scipy.constants.golden,
        )
        ax1.set_title(dataset)
        ax1.plot(x_l_plot, y_l_plot, ".", color="lightsteelblue", label="data", markersize=5)
        ax1.plot(
            x_bin_l_plot, y_bin_l_plot, "o", color="darkslateblue", label="binned", markersize=5
        )

        if not fit2_failed:
            ax1.plot(
                np.sort(x_l_plot),
                fit_function_gauss(np.sort(x_l_fit), *popt_l_plot),
                "-",
                color="limegreen",
                label="Gauss",
                markersize=5,
            )
            ax1.plot(
                np.sort(x_l_plot),
                fit_function_voigt(np.sort(x_l_fit), *popt_l_plot_voigt),
                "r",
                label="Voigt",
                markersize=5,
            )
            ax1.legend(fontsize=font_size, numpoints=1, ncol=1, frameon=True)  # loc="upper right",

        if normalized:
            ax1.set_xlim([-5 * abs(sigma_l), abs(5 * sigma_l)])
        plot_options(
            fig,
            ax2,
            font_size=font_size,
            image_width=30,
            ticks=["auto", "auto"],
            xlabel=xlabel,
            ylabel=ylabel1,
            aspect_ratio=2 * scipy.constants.golden,
        )
        ax2.set_title(dataset)
        ax2.plot(x_r_plot, y_r_plot, ".", color="lightsteelblue", label="data", markersize=5)
        ax2.plot(
            x_bin_r_plot, y_bin_r_plot, "o", color="darkslateblue", label="binned", markersize=5
        )

        if not fit1_failed:
            ax2.plot(
                np.sort(x_r_plot),
                fit_function_gauss(np.sort(x_r_fit), *popt_r_plot),
                "-",
                color="limegreen",
                label="Gauss",
                markersize=5,
            )
            ax2.plot(
                np.sort(x_r_plot),
                fit_function_voigt(np.sort(x_r_fit), *popt_r_plot_voigt),
                "r",
                label="Voigt",
                markersize=5,
            )
            ax2.legend(fontsize=font_size, numpoints=1, ncol=1, frameon=True)  # loc="upper right",

        if normalized:
            ax2.set_xlim([-5 * abs(sigma_r), 5 * abs(sigma_r)])

        if logscale:
            ax1.set_yscale("log")
            ax2.set_yscale("log")

        plt.tight_layout()

        if plot_filename is not False:
            save_plot(plot_filename, print_output=True)

        plt.show()
    plot_stuff = (
        x_l_plot,
        y_l_plot,
        x_bin_l_plot,
        y_bin_l_plot,
        np.sort(x_l_plot),
        -fit_function_gauss(np.sort(x_l_fit), *popt_l_plot),
    )
    if not fit2_failed and not fit1_failed:
        return (
            [popt_r, sdev_r, serr_r],
            [popt_l, sdev_l, serr_l],
            [popt_r_voigt, sdev_r_voigt, serr_r_voigt],
            [popt_l_voigt, sdev_l_voigt, serr_l_voigt],
            center_pump_nm,
            plot_stuff,
        )
    else:
        return [popt_r, sdev_r, serr_r], [popt_l, sdev_l, serr_l], center_pump_nm, plot_stuff


def raw_moment(data, iord, jord):
    """
    returns statistical moment of order (iord, jord) of a two-dimensional array (image)
    from Joe Kington (accessed: 14.01.2022)
    https://stackoverflow.com/questions/9005659/compute-eigenvectors-of-image-in-python/9007249#9007249
    """

    nrows, ncols = data.shape
    y, x = np.mgrid[:nrows, :ncols]
    data = data * x**iord * y**jord
    return data.sum()


def get_beamdiameters(data, stepsize):
    """
    calculates means and beamdiameters of a Gaussian beamprofile
    after Joe Kington (accessed: 14.01.2022)
    https://stackoverflow.com/questions/9005659/compute-eigenvectors-of-image-in-python/9007249#9007249
    returns: hor. position, ver. position, hor. beam diameter, ver. beam diameter, covariance matrix
    """
    data_sum = data.sum()
    m10 = raw_moment(data, 1, 0)
    m01 = raw_moment(data, 0, 1)
    x_bar = m10 / data_sum
    y_bar = m01 / data_sum
    u11 = (raw_moment(data, 1, 1) - x_bar * m01) / data_sum
    u20 = (raw_moment(data, 2, 0) - x_bar * m10) / data_sum
    u02 = (raw_moment(data, 0, 2) - y_bar * m01) / data_sum
    cov = np.array([[u20, u11], [u11, u02]])

    return [
        x_bar * stepsize,
        y_bar * stepsize,
        4 * np.sqrt(abs(u20)) * stepsize,
        4 * np.sqrt(abs(u02)) * stepsize,
        cov,
    ]


def import_image(file_name):
    """
    imports image file, derives parameters from the auxiliary file and save them as a dictionary:
    keys: file_name, cam_name, cam_id, cam_type, exp_time, gain, acquisition_time,
        step_size, position
    """
    # import auxiliary file for position determination
    aux_file = path_prefix + "Measurement Data/BeamCamera/" + file_name
    try:
        with open(aux_file + ".json", "r", encoding="ASCII") as f:
            image_params = json.loads(f.read())
        # change all keys to lower case
        image_params = {key.lower(): value for key, value in image_params.items()}
        try:
            position = image_params["position"]
        except Exception:
            position = 0
        cam_id = re.findall("\(([0-9]+)\)", image_params["camera"])  # noqa: W605
        cam_name = re.findall("(.+?) \([0-9]+\)", image_params["camera"])  # noqa: W605
        cam_type = image_params["model"]
        acquisition_time = image_params["precise_time"]
        exp_time = image_params["exposure_time"]
        gain = image_params["gain"]

    except FileNotFoundError:
        # legacy for old txt-fileformat
        with open(
            aux_file + ".txt", "r"
        ) as f:  # , encoding='ASCII' => problem if data analysis is toggled on
            aux_data = f.read()

        # derive parameters from the auxiliary file
        file_name, cam_name, cam_id, cam_type = re.findall(
            "# (.+?), camera: '(.+?) \((.+?)\)', model: (.+?)\\n", aux_data  # noqa: W605
        )[0]
        cam_id = int(cam_id)
        exp_time, gain = re.findall("# Exposure time (.+?) ms, gain (.+?) dB.", aux_data)[0]
        exp_time = float(exp_time)
        gain = float(gain)

        # position only in M2 files
        # TODO: check if position is still saved.
        position = re.findall("# Position (.+?) mm", aux_data)
        if position:
            position = float(position[0])
        else:
            position = 0

        # highest time resolution only in newer images
        time = re.findall(file_name + "\.([0-9]+)", aux_data)  # noqa: W605
        if time:
            file_name_ms = file_name + "." + time[0]
        else:
            file_name_ms = file_name
        try:
            acquisition_time = datetime.strptime(file_name_ms, "%Y_%m_%dT%H_%M_%S")
        except Exception:
            acquisition_time = datetime.strptime(file_name_ms, "%Y-%m-%dT%H.%M.%S.%f")
        # todo: add orientation of the camera to the auxiliary file and save the last configuration
        # as standard for this camera

    # get stepsize by checking cam_type:
    if cam_type == "daA2500-14um":
        step_size = 2.2e-3  # in mm
    elif cam_type == "daA1280-54um":
        step_size = 3.75e-3  # in mm
    else:
        print("unknown camera model, assuming a stepsize of 3.75e-3 mm")
        step_size = 3.75e-3  # in mm

    # save the parameters as a dictionary, necessary due to legacy code above...
    values = (
        file_name,
        cam_name,
        cam_id,
        cam_type,
        exp_time,
        gain,
        acquisition_time,
        step_size,
        position,
    )
    keys = (
        "file_name",
        "cam_name",
        "cam_id",
        "cam_type",
        "exp_time",
        "gain",
        "acquisition_time",
        "step_size",
        "position",
    )
    params = {}
    for i, item in enumerate(np.array(values, dtype=object).T):
        params[keys[i]] = item

    # import image
    image_data = np.load(
        path_prefix + "Measurement Data/BeamCamera/" + file_name + ".npy", encoding="ASCII"
    )
    return image_data, params


def subtract_background(image_file, background_file):
    """subtracts the background image from the image pixel wise"""
    image_data, image_params = import_image(image_file)
    background_data, background_params = import_image(background_file)

    # check if both measurements where conducted with the same camera...
    if image_params["cam_id"] != background_params["cam_id"]:
        print(
            "Error, comparing pictures from different cameras! Returned tge unchanged image data."
        )
        return image_data
    else:
        # have to infer the format,otherwise it works with integers ...
        return np.subtract(image_data, background_data, dtype="float32")


def beamprofile_analysis(data, stepsize):
    """calculates means and beamdiameters of a Gaussian beamprofile
    after Joe Kington (accessed: 14.01.2022)
    link:
        https://stackoverflow.com/questions/9005659/compute-eigenvectors-of-image-in-python/9007249#9007249
    returns: hor. position, ver. position, hor. beam diameter, ver. beam diameter, covariance matrix
    """
    data_sum = data.sum()
    m10 = raw_moment(data, 1, 0)
    m01 = raw_moment(data, 0, 1)
    x_bar = m10 / data_sum
    y_bar = m01 / data_sum
    u11 = (raw_moment(data, 1, 1) - x_bar * m01) / data_sum
    u20 = (raw_moment(data, 2, 0) - x_bar * m10) / data_sum
    u02 = (raw_moment(data, 0, 2) - y_bar * m01) / data_sum
    cov = np.array([[u20, u11], [u11, u02]])

    return [
        x_bar * stepsize,
        y_bar * stepsize,
        4 * np.sqrt(abs(u20)) * stepsize,
        4 * np.sqrt(abs(u02)) * stepsize,
        cov,
    ]


def plot_image(
    image_file,
    background_file=False,
    zoom_factor=0.25,
    print_output=True,
    normalized=False,
    dpi=100,
    font_size=9,
    plot_filename=False,
    export_data=False,
    plot_filepath=file_path,
):
    """plots a image (beam profile) ans performs slices, optional data export"""

    # import file and parameters
    image_data, image_params = import_image(image_file)
    # dimensions of the image
    nrows, ncols = image_data.shape

    # pixel size might change from camera to camera
    stepsize = image_params["step_size"]

    # perform optional background subtraction
    if not background_file:
        data = image_data
        # find row and column of the highest value in the image data
        center_row, center_col = np.unravel_index(np.argmax(data), data.shape)
        # careful: need to compensate the fact, that python indexes the rows wrong
        mean_hor, mean_ver = center_col * stepsize, (nrows - center_row) * stepsize
        background_offset_string = "no background offset"
    else:
        data = subtract_background(image_file, background_file)
        # second moment analysis of the beamprofile only works with background subtraction!
        # center of mass derivation does not work!! also second moment derivation is bugged somehow.
        # todo: look into second moment determination, e.g., for m2 files
        # mean_hor, mean_ver = beamprofile_analysis(data, stepsize)[:2]
        # center_col, center_row = int(mean_hor / stepsize), nrows - int(mean_ver / stepsize)

        # use ndimage magic for maximum position determination
        center_row, center_col = ndimage.maximum_position(data)
        # careful: need to compensate the fact, that python indexes the rows wrong
        mean_hor, mean_ver = center_col * stepsize, (nrows - center_row) * stepsize
        background_offset_string = "".join(("background file: ", background_file))

    # cm = ndimage.center_of_mass(data)
    peak_value = ndimage.maximum(data)

    # create 2D lists of the horizontal and vertical slices
    hor_data = []
    for i, x in enumerate(data[center_row]):
        hor_data.append([stepsize * i, x])
    ver_data = []
    for j, y in enumerate(data.T[center_col]):
        ver_data.append([stepsize * j, y])

    if print_output:
        print(
            f"{image_params['file_name']}, {background_offset_string}\n"
            f"pixel size: {1e3 * image_params['step_size']:.2f}um, peak value: {peak_value}, "
            f"mean(hor): {mean_hor:.3f}mm, cm(ver): {mean_ver:.3f}mm"
        )
    # fits
    popt_hor, sdev_hor, serr_hor, pcov_hor = fit_slice(
        hor_data, print_output=print_output
    )  # print_output
    popt_ver, sdev_ver, serr_ver, pcov_ver = fit_slice(
        ver_data, print_output=print_output
    )  # print_output

    # downsample image by zoom_factor, only for display!
    if zoom_factor != 1:
        plot_data = ndimage.zoom(data, zoom=zoom_factor)
    else:
        plot_data = data

    # plot image
    fig, axes = plt.subplots()
    fig.set_size_inches(10, 3)
    fig.patch.set_facecolor("white")

    ax = plt.subplot(1, 3, 1)  # nrows, ncols,index,kwargs
    plot_options(fig, ax, font_size=font_size)
    # ax.imshow(downsampled, origin='lower')  # origin to lower left, standard is upper left somehow

    # fit results for normalization
    x0_hor, a_hor, sigma_hor = popt_hor
    x0_ver, a_ver, sigma_ver = popt_ver
    w0_hor, w0_ver = abs(2 * sigma_hor), abs(2 * sigma_ver)

    # vertical scale factor
    if normalized:
        scale_factor = popt_hor[1]
    else:
        scale_factor = 1

    # plot data for the slices
    x_hor, y_hor = plotdata(hor_data, 1, 1 / scale_factor)  # 1/peak_value
    x_ver, y_ver = plotdata(ver_data, 1, 1 / scale_factor)  # 1/peak_value

    # change plotranges
    if normalized:
        w0_large = max(abs(w0_hor), abs(w0_ver))
        ax.imshow(
            plot_data,
            origin="lower",
            extent=[-x0_hor, ncols * stepsize - x0_hor, -x0_ver, nrows * stepsize - x0_ver],
            cmap=ListedColormap(turbo_colormap_data),
        )
        ax.set_xlim([-1.5 * w0_large, 1.5 * w0_large])
        ax.set_ylim([-1.5 * w0_large, 1.5 * w0_large])
        ax.add_artist(
            lines.Line2D([0, 0], [-1.5 * w0_large, 1.5 * w0_large], color="red", linewidth=0.5)
        )
        ax.add_artist(
            lines.Line2D([-1.5 * w0_large, 1.5 * w0_large], [0, 0], color="red", linewidth=0.5)
        )
    else:
        ax.imshow(
            plot_data,
            origin="lower",
            extent=[0, ncols * stepsize, 0, ncols * stepsize],
            cmap=ListedColormap(turbo_colormap_data),
        )
        ax.add_artist(
            lines.Line2D([x0_hor, x0_hor], [0, ncols * stepsize], color="red", linewidth=0.5)
        )
        ax.add_artist(
            lines.Line2D([0, ncols * stepsize], [x0_ver, x0_ver], color="red", linewidth=0.5)
        )

    ax.set_xlabel("horizontal position (mm)")
    ax.set_ylabel("vertical position (mm)")

    # horizontal plot
    ax2 = plt.subplot(1, 3, 2)  # nrows, ncols,index,kwargs
    plot_options(fig, ax2, font_size=font_size)
    ax2.set_ylabel("intensity (arb. u.)")
    ax2.set_xlabel("horizontal position (mm)")

    if normalized:
        x_hor_plot = np.array(x_hor) - x0_hor
        fit_offset_hor = x0_hor
        ax2.set_xlim(-2 * w0_hor, 2 * w0_hor)
        # ax3.set_ylim(-0.05, 1)
    else:
        x_hor_plot = np.array(x_hor)
        fit_offset_hor = 0

    ax2.plot(x_hor_plot, y_hor, "b+")
    x_hor_fit = np.arange(0.99 * ax2.get_xlim()[0], 0.99 * ax2.get_xlim()[1], 0.001)
    ax2.plot(
        x_hor_fit, gauss(x_hor_fit + fit_offset_hor, *popt_hor) / scale_factor, "r", label="fit"
    )  # /peak_value

    # vertical plot
    ax3 = plt.subplot(1, 3, 3)
    plot_options(fig, ax3, dpi=dpi, font_size=font_size, image_width=16, aspect_ratio=3)
    ax3.set_ylabel("intensity (arb. u.)")
    ax3.set_xlabel("vertical position (mm)")

    if normalized:
        x_ver_plot = np.array(x_ver) - x0_ver
        fit_offset_ver = x0_ver
        ax3.set_xlim(-2 * w0_ver, 2 * w0_ver)
        # ax3.set_ylim(-0.05, 1)
    else:
        x_ver_plot = np.array(x_ver)
        fit_offset_ver = 0

    ax3.plot(x_ver_plot, y_ver, "b+")
    x_ver_fit = np.arange(0.99 * ax3.get_xlim()[0], 0.99 * ax3.get_xlim()[1], 0.001)
    ax3.plot(
        x_ver_fit, gauss(x_ver_fit + fit_offset_ver, *popt_ver) / scale_factor, "r", label="fit"
    )  # /peak_value

    plt.tight_layout()

    if plot_filename is not False:
        save_plot(plot_filename, print_output=True, path=plot_filepath)

    plt.show()

    # image, ,boundaries, fitresults,
    if export_data:
        return (
            data,
            [-x0_hor, ncols * stepsize - x0_hor, -x0_ver, nrows * stepsize - x0_ver],
            x_hor_plot,
            x_ver_plot,
            [popt_hor, sdev_hor, serr_hor, popt_ver, sdev_ver, serr_ver],
        )


def import_beamprofiler(file_name, key="IRPD2"):
    """imports beamprofiler data"""

    # import auxiliary file for position determination
    aux_file = path_prefix + "Measurement Data/Beamprofiler/" + file_name
    try:
        with open(aux_file + ".csv", "r", encoding="ASCII") as f:
            aux_data = f.read()
        # keys from DataLogger
        # dict_keys = re.findall("Variables,(.+?)\\n", aux_data)[0].split()

        # get measurement parameters
        temp = aux_data.split("Variables")[0].split()

        keys = []
        values = []
        for element in temp[1:]:
            temp_element = element.split(",")
            keys.append(temp_element[0])
            values.append(float(temp_element[1]))
        # save as dictionary
        image_params = {}
        for i, item in enumerate(np.array(values, dtype=object).T):
            image_params[keys[i].lower()] = item
    except FileNotFoundError:
        with open(aux_file + ".json", "r", encoding="ASCII") as f:
            image_params = json.loads(f.read())
        # change all keys to lower case
        image_params = {key.lower(): value for key, value in image_params.items()}

    # import image
    with open(path_prefix + f"Measurement Data/Beamprofiler/{file_name}.pkl", "rb") as file:
        data = pickle.load(file)

    # add further parameters to the parameter dict
    image_params["keys"] = list(data.keys())
    image_params["file_name"] = file_name

    image_data = data[key]

    # positions as stated in the pandas dataframe
    cols = image_data.columns
    rows = image_data.T.columns

    # dimensions of the image
    nrows, ncols = image_data.shape

    # pixel size might change from camera to camera
    # stepsize = image_params["stepsize"]
    # start1, start2 = image_params["start1"], image_params["start2"]

    data = np.array(image_data)

    # find row and column of the highest value in the image data
    center_row, center_col = np.unravel_index(np.argmax(data), data.shape)
    # careful: need to compensate the fact, that python indexes the rows wrong
    image_params["mean_hor"] = cols[center_col]
    image_params["mean_ver"] = rows[center_row]

    image_params["peak_value"] = ndimage.maximum(data)

    # create 2D lists of the horizontal and vertical slices
    hor_data = []
    for i, x in enumerate(data[center_row]):
        hor_data.append([cols[i], x])
    ver_data = []
    for j, y in enumerate(data.T[center_col]):
        ver_data.append([rows[j], y])

    return image_data, image_params, hor_data, ver_data


def plot_beamprofiler(
    file_name,
    key="IRPD2",
    print_output=True,
    normalized=False,
    dpi=100,
    font_size=9,
    plot_filename=False,
    background_value=0,
    export_data=False,
):
    # import file and parameters
    image_data, image_params, hor_data_tmp, ver_data_tmp = import_beamprofiler(file_name, key=key)
    # TODO: show lines for hor/v3er cuts
    # background substraction (necessary for cw-measurement!)
    hor_data = []
    for i in hor_data_tmp:
        hor_data.append([i[0], i[1] - background_value])
    ver_data = []
    for i in ver_data_tmp:
        ver_data.append([i[0], i[1] - background_value])
    image_data -= background_value

    if print_output:
        print(
            f"{image_params['file_name']}, stepsize: {image_params['stepsize']:.2f}mm, "
            f"peak value: {image_params['peak_value']:.3f}, mean(hor): "
            f"{image_params['mean_hor']:.3f}mm, mean(ver): {image_params['mean_ver']:.3f}mm"
        )

    # fits
    popt_hor, sdev_hor, serr_hor, pcov_hor = fit_slice(
        hor_data, print_output=print_output
    )  # print_output
    popt_ver, sdev_ver, serr_ver, pcov_ver = fit_slice(
        ver_data, print_output=print_output
    )  # print_output

    # downsample image by zoom_factor, only for display!
    # downsampled = ndimage.zoom(data, zoom=zoom_factor)

    # plot image
    fig, axes = plt.subplots()
    # fig.set_size_inches(10, 3)
    fig.patch.set_facecolor("white")

    ax = plt.subplot(1, 3, 1)  # nrows, ncols,index,kwargs
    plot_options(fig, ax, font_size=font_size)
    # ax.imshow(downsampled, origin='lower')  # origin to lower left, standard is upper left somehow

    # fit results for normalization
    x0_hor, a_hor, sigma_hor = popt_hor
    x0_ver, a_ver, sigma_ver = popt_ver
    w0_hor, w0_ver = abs(2 * sigma_hor), abs(2 * sigma_ver)

    # vertical scale factor
    if normalized:
        scale_factor = popt_hor[1]
    else:
        scale_factor = 1

    # plot data for the slices
    x_hor, y_hor = plotdata(hor_data, 1, 1 / scale_factor)  # 1/peak_value
    x_ver, y_ver = plotdata(ver_data, 1, 1 / scale_factor)  # 1/peak_value

    # positions as stated in the pandas dataframe
    cols = image_data.columns
    rows = image_data.T.columns

    # change plotranges
    if normalized:
        # reduce plotrange, if the profile is small
        w0_plot = (
            min(
                max(abs(2 * w0_hor), abs(2 * w0_ver)),
                abs(cols[-1] - x0_hor) + abs(cols[0] - x0_hor),
                abs(rows[-1] - x0_ver) + abs(rows[0] - x0_ver),
            )
            / 2
        )
        boundaries = cols[0] - x0_hor, cols[-1] - x0_hor, rows[0] - x0_ver, rows[-1] - x0_ver
        ax.imshow(
            image_data, origin="lower", extent=boundaries, cmap=ListedColormap(turbo_colormap_data)
        )
        ax.set_xlim([-w0_plot, w0_plot])
        ax.set_ylim([-w0_plot, w0_plot])
    else:
        boundaries = cols[0], cols[-1], rows[0], rows[-1]
        ax.imshow(
            image_data, origin="lower", extent=boundaries, cmap=ListedColormap(turbo_colormap_data)
        )

    ax.set_xlabel("horizontal position (mm)")
    ax.set_ylabel("vertical position (mm)")

    # horizontal plot
    ax2 = plt.subplot(1, 3, 2)  # nrows, ncols,index,kwargs
    plot_options(fig, ax2, font_size=font_size)
    ax2.set_ylabel("intensity (arb. u.)")
    ax2.set_xlabel("horizontal position (mm)")

    # todo: with extreme offsets (why) the fits don't work with normalized=True!
    if normalized:
        x_hor_plot = np.array(x_hor) - x0_hor
        fit_offset_hor = x0_hor
        xlims_hor = -2 * w0_hor, 2 * w0_hor
        ax2.set_xlim(xlims_hor[0], xlims_hor[1])
        # ax3.set_ylim(-0.05, 1)
    else:
        x_hor_plot = np.array(x_hor)
        fit_offset_hor = 0
        xlims_hor = min(x_hor_plot), max(x_hor_plot)

    ax2.plot(x_hor_plot, y_hor, "b+")
    x_hor_fit = np.arange(xlims_hor[0] + 0.05, xlims_hor[1] - 0.05, 0.001)
    ax2.plot(
        x_hor_fit, gauss(x_hor_fit + fit_offset_hor, *popt_hor) / scale_factor, "r", label="fit"
    )  # /peak_value

    # vertical plot
    ax3 = plt.subplot(1, 3, 3)
    plot_options(fig, ax3, dpi=dpi, image_width=16, aspect_ratio=3, font_size=font_size)
    ax3.set_ylabel("intensity (arb. u.)")
    ax3.set_xlabel("vertical position (mm)")

    if normalized:
        x_ver_plot = np.array(x_ver) - x0_ver
        fit_offset_ver = x0_ver
        xlims_ver = -2 * w0_ver, 2 * w0_ver
        ax3.set_xlim(xlims_ver[0], xlims_ver[1])
        # ax3.set_ylim(-0.05, 1)

    else:
        x_ver_plot = np.array(x_ver)
        fit_offset_ver = 0
        xlims_ver = min(x_ver_plot), max(x_ver_plot)

    ax3.plot(x_ver_plot, y_ver, "b+")
    x_ver_fit = np.arange(xlims_ver[0], xlims_ver[1], 0.001)
    # x_ver_fit = x_ver_plot
    ax3.plot(
        x_ver_fit, gauss(x_ver_fit + fit_offset_ver, *popt_ver) / scale_factor, "r", label="fit"
    )  # /peak_value

    plt.tight_layout()

    if plot_filename:
        save_plot(plot_filename, print_output=True)

    plt.show()

    # image, ,boundaries, fitresults,
    if export_data:
        return (
            image_data,
            boundaries,
            x_hor_plot,
            x_ver_plot,
            [popt_hor, sdev_hor, serr_hor, popt_ver, sdev_ver, serr_ver],
        )


def set_key(dictionary, key, value):
    """
    adds value to key in dictionary. Support the common list structure in dictionaries
    source: Anshul Goyal (accessed: 09.02.2022, https://stackoverflow.com/a/41826126)
    """
    if key not in dictionary:
        dictionary[key] = value
    elif type(value) == list:
        if len(dictionary[key]) == len(value):
            dictionary[key] = [dictionary[key], value]
        else:
            dictionary[key].append(value)
    elif type(dictionary[key]) == list:
        dictionary[key].append(value)
    else:
        dictionary[key] = [dictionary[key], value]


def flatten(input_list):
    return list(itertools.chain.from_iterable(input_list))


def calPressFit(p):
    """calculate the pressure based on fits."""

    def exponentialPlus(x, a, b, c, d):
        return a * x**b + c + d * x

    p = np.array(p)
    l1 = 16.04628
    l2 = 1.2592278
    l3 = 0.153085
    c = np.zeros_like(p)
    i = l1 <= p
    c[i] = p[i]
    i = (l2 <= p) * (p < l1)
    c[i] = exponentialPlus(p[i], 7.70885685, 0.34144037, -3.83997801, 0)
    i = (l3 <= p) * (p < l2)
    c[i] = exponentialPlus(p[i], 1.40091029, 3.87190482, -0.03701845, 0.88537411)
    i = p < l3
    c[i] = 0.65 * p[i]
    i = np.isnan(p)
    c[i] = np.nan
    return c


def coll_broadened_fwhm(
    p_self_mbar, p_n2_mbar=0, gamma_self=0.233, gamma_n2=0.06, n_air=0.73, temp_degc=25
):
    """
    standard values for resonance (A) at a fundamental wavenumber of 2830.36cm−1,
    broadening coefficients in cm-1/bar (given as HWHM)
    """
    temp_k = temp_degc + 273.15
    temp_ref = 295
    p_ref = 1013

    return (
        2
        * (temp_ref / temp_k) ** n_air
        * (gamma_self * p_self_mbar / p_ref + gamma_n2 * p_n2_mbar / p_ref)
    )


def doppler_fwhm(nu, temp_degc=25, isotope="H35Cl"):
    temp_k = temp_degc + 273.15
    c0 = c.speed_of_light
    na = c.N_A
    kb = c.k
    mol_ms = {
        "H35Cl": 35.976678e-3,
        "H37Cl": 37.973729e-3,
        "16O12C16O": 43.989830e-3,
        "16O13C16O": 44.993185e-3,
    }
    try:
        mol_m = mol_ms[isotope]
    except Exception:
        print("wrong input for isotope, continue with values for H35Cl!")
        mol_m = 35.976678e-3
    return nu / c0 * np.sqrt((8 * np.log(2) * na * kb * temp_k) / (mol_m))


def voigt_fwhm(doppler_fwhm, coll_fwhm):
    """J. Olivero and R. Longbothum. Empirical fits to the Voigt line width:
        A brief review. J. Quant. Spectrosc. Radiat. Transf. 17, 233–236 (1977)"""
    return 0.5346 * coll_fwhm + np.sqrt(0.2166 * coll_fwhm**2 + doppler_fwhm**2)


def sdev2fwhm(sdev):
    """
    converts Gaussian standard deviation into FWHM
    """
    return 2 * np.sqrt(2 * np.log(2)) * sdev


def fwhm2sdev(fwhm):
    """
    converts FWHM into Gaussian standard deviation
    """
    return fwhm / (2 * np.sqrt(2 * np.log(2)))


def osa_detuning(
    dict1,
    x_key,
    ref_wavelength_nm="auto",
    key_wavelength="meanwavelengthOSA",
    key_power="meanpeaklevelOSA",
    power_limit_dbm=-100,
):
    """calculates detuning in GHz relative to reference wavelength and checks if the signal power
    was reasonably high"""
    if ref_wavelength_nm == "auto":
        ref = nm2hz(np.nanmean(dict1[key_wavelength]) * 1e9)
    else:
        ref = nm2hz(ref_wavelength_nm)
    detuning = 1e-9 * (nm2hz(np.array(dict1[key_wavelength]) * 1e9) - ref)
    x = np.array(dict1[x_key])
    return x[detuning > power_limit_dbm], detuning[detuning > power_limit_dbm]


def merge_dict(list_of_dicts):
    """merges dictionaries from a list (with the same keys)"""
    full_dict = defaultdict(list)
    for d in list_of_dicts:  # you can list as many input dicts as you want here
        for key, value in d.items():
            full_dict[key].append(value)
    return full_dict


def n_lnb(lambda_nm, temp_degc=25, axis="o"):
    """refractive index of 5% MgO doped congruent LNB (supplied by HC Photonics)
    data and functions from Gayer et al. (Appl. Optics, 2008)"""
    match axis:
        case "e":
            a1, a2, a3, a4, a5, a6 = 5.756, 0.0983, 0.2020, 189.32, 12.52, 1.32e-2
            b1, b2, b3, b4 = 2.86e-6, 4.7e-8, 6.113e-8, 1.516e-4
        case "o":
            a1, a2, a3, a4, a5, a6 = 5.653, 0.1185, 0.2091, 89.61, 10.85, 1.97e-2
            b1, b2, b3, b4 = 7.941e-7, 3.134e-8, -4.641e-9, -2.188e-6
    f = (np.array(temp_degc) - 24.5) * (np.array(temp_degc) + 570.82)
    lambda_um = np.array(lambda_nm) * 1e-3
    # Sellmeier equation for lambda in micrometers!
    return np.sqrt(
        a1
        + b1 * f
        + (a2 + b2 * f) / (lambda_um**2 - (a3 + b3 * f) ** 2)
        + (a4 + b4 * f) / (lambda_um**2 - a5**2)
        - a6 * lambda_um**2
    )


def n(theta, lambda_nm, temp_degc):
    """returns the refractive index under propagation with angle of theta with the crystal c-axis"""
    return 1 / (
        np.sqrt(
            np.cos(np.deg2rad(np.array(theta))) ** 2
            / n_lnb(np.array(lambda_nm), np.array(temp_degc), "o") ** 2
            + np.sin(np.deg2rad(np.array(theta))) ** 2
            / n_lnb(np.array(lambda_nm), np.array(temp_degc), "e") ** 2
        )
    )


def delta_k(lambda_i_nm, lambda_p_nm, period_um, temp_degc):
    """returns the wavevector mismatch in a QPM crystal with defined poling period
    wavelengths in nm, temperature in °C"""
    temp = np.array(temp_degc)
    lambda_i = np.array(lambda_i_nm)
    lambda_p = np.array(lambda_p_nm)
    lambda_s = idler2signal(lambda_i, lambda_p)
    k1 = 2 * np.pi / lambda_p * n_lnb(1e9 * lambda_p, temp_degc=temp, axis="e")
    k2 = 2 * np.pi / lambda_i * n_lnb(1e9 * lambda_i, temp_degc=temp, axis="e")
    k3 = 2 * np.pi / lambda_s * n_lnb(1e9 * lambda_s, temp_degc=temp, axis="e")

    alpha_exp = 15.4e-6, 5.3e-9  # K^-1, Jundt, Optics Letters 1997
    # alpha_exp = 14.8e-6, 4.1e-6  # K^-1, SNLO, OSA Handbook of Optics
    temp_ref = 25  # coefficients are defined at room temperature
    period = np.array(period_um)
    actual_period = period * (
        1 + alpha_exp[0] * (temp - temp_ref) + alpha_exp[1] * (temp - temp_ref) ** 2
    )
    return k1 - k2 - k3 - 2 * np.pi / actual_period


def pm_theta(idler_nm, pump_nm, temp_degc=25):
    """returns the angle, which provides critical phase matching in MgO:LNB"""
    signal_nm = idler2signal(idler_nm, pump_nm)
    n1 = n_lnb(idler_nm, temp_degc, "o")
    n2 = n_lnb(signal_nm, temp_degc, "o")
    n_pump_e = n_lnb(pump_nm, temp_degc, "e")
    n_pump_o = n_lnb(pump_nm, temp_degc, "o")

    n_soll = (n1 / idler_nm + n2 / signal_nm) * pump_nm

    return (
        180
        / np.pi
        * np.arctan(
            n_pump_e
            / n_pump_o
            * np.sqrt((n_soll**2 - n_pump_o**2) / (n_pump_e**2 - n_soll**2))
        )
    )


def calc_qpm(lambda_i_nm, temp_k, period_um, quantity="idler", lambda_p_nm=1064.1584):
    """returns idler wavelength, QPM temperature or poling period depending on quantity:
    quantity:   'idler': idler wavelength (nm)
                'temperature': QPM temperature (°C)
                'period': poling period (um)
    """
    lambda_p = np.array(lambda_p_nm)
    result = []
    match quantity:
        case "idler":
            temp_deg = temp_k - 273.15
            period = 1e-6 * period_um
            for temp in temp_deg:
                fun = lambda x: abs(  # noqa: E731
                    delta_k(
                        lambda_i_nm=1e-9 * x,
                        lambda_p_nm=1e-9 * lambda_p,
                        period_um=period,
                        temp_degc=temp,
                    )
                )
                result.append(scipy.optimize.minimize_scalar(fun, [3000, 4800])["x"])
        case "temperature":
            lambda_i = 1e-9 * np.array(lambda_i_nm)
            period = 1e-6 * np.array(period_um)
            for nm in lambda_i:
                fun = lambda x: abs(  # noqa: E731
                    delta_k(
                        lambda_i_nm=nm, lambda_p_nm=1e-9 * lambda_p, period_um=period, temp_degc=x
                    )
                )
                result.append(scipy.optimize.minimize_scalar(fun, [0, 300])["x"])
        case "period":
            lambda_i = 1e-9 * np.array(lambda_i_nm)
            temp_deg = np.array(temp_k) - 273.15
            for nm in lambda_i:
                fun = lambda x: abs(  # noqa: E731
                    delta_k(
                        lambda_i_nm=nm, lambda_p_nm=1e-9 * lambda_p, period_um=x, temp_degc=temp_deg
                    )
                )
                result.append(scipy.optimize.minimize_scalar(fun, [0, 300])["x"])
    return result


def apply_bounds(x, y, x_bnds="auto", y_bnds="auto"):
    """reduces lists of equal length, depending on stated bounds."""
    if x_bnds == "auto":
        x_bnds = [min(x), max(x)]
    if y_bnds == "auto":
        y_bnds = [min(y), max(y)]
    return (
        x[(x > x_bnds[0]) & (x < x_bnds[1]) & (y > y_bnds[0]) & (y < y_bnds[1])],
        y[(x > x_bnds[0]) & (x < x_bnds[1]) & (y > y_bnds[0]) & (y < y_bnds[1])],
    )

import datetime
import json
import os
import pickle
from typing import Any

import numpy as np
import pandas as pd


def save_image(
    directory: str, configuration: dict[str, Any], data: dict[str, pd.DataFrame]
) -> str:
    """Save the data and the parameters into files."""
    # Get the time for the filename.
    filename = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
    # Get the path.
    if not os.path.isdir(directory):
        # no save path directory exists: ask for path.
        raise FileNotFoundError
    # Save data and parameters
    with open(directory + os.sep + filename + ".pkl", "wb") as file:
        pickle.dump(data, file)
    with open(directory + os.sep + filename + ".json", "w") as file:
        json.dump(configuration, file, indent=4)
    return filename


def save_alignment_data(
    directory: str, configuration: dict[str, Any], alignment_data: dict[str, list]
) -> str:
    # Get the time for the filename.
    filename = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
    # Get the path.
    if not os.path.isdir(directory):
        # no save path directory exists: ask for path.
        raise FileNotFoundError

    meta = {
        "today": datetime.datetime.now(datetime.timezone.utc).date(),
        "name": filename,
        "beamprofiler": configuration,
    }
    data = (", ".join(alignment_data.keys()), alignment_data, meta)
    with open(directory + os.sep + filename + ".pkl", "wb") as file:
        pickle.dump(data, file)
    with open(directory + os.sep + filename + ".json", "w") as file:
        json.dump(configuration, file, indent=4)
    return filename


def import_image(
    name: str, directory: str = "M:/Measurement Data/Beamprofiler"
) -> tuple[dict[str, pd.DataFrame], float]:
    """Import beamprofiler data. In "Measurement Data/Beamprofiler/".

    :return: tuple of dictionary of pandas dataframes and step size in µm.
    """
    # import image
    # with open(directory + f"/{name}.pkl", 'rb') as file:
    #     data = pickle.load(file)
    data = pd.read_pickle(directory + f"/{name}.pkl")
    # Import configuration
    try:
        with open(directory + f"/{name}.json", "r") as file:
            config = json.load(file)
    except FileNotFoundError:
        # Old csv data.
        with open(directory + "/" + name + ".csv", "r", encoding="ASCII") as f:
            aux_data = f.read()
        # get measurement parameters
        keys = []
        values = []
        for element in aux_data.split("Variables")[0].split()[1:]:
            temp_element = element.split(",")
            keys.append(temp_element[0])
            values.append(float(temp_element[1]))

        # save as dictionary
        config = {}
        for i, item in enumerate(np.array(values, dtype=object).T):
            config[keys[i].lower()] = item
        config["name"] = name

    pixel_length = config.get("Stepsize", 1000) * 1000
    return data, pixel_length

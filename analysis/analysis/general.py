# flake8: noqa
# -*- coding: utf-8 -*-
"""
Star-Importing the methods of all general modules from the analysis package.

Recommended to import as ang.
"""

from analysis.data import *
from analysis.math import *
from analysis.plotting import *

"""
Save or load camera image files.
"""

import datetime
import json
import os
from typing import Any, Optional

import numpy as np


# just for reading older files, so no updates needed.
# for new cameras, modify the basler.py file in devices.
pixel_lengths: dict[str, float] = {
    # pixel length in µm
    "daA1280-54um": 3.75,
    "daA2500-14um": 2.2,
    "daA3840-45um": 2,
}


def save_image(
    image: np.ndarray, directory: str = "", config: Optional[dict[str, Any]] = None
) -> str:
    """Save the image to disk."""
    # settings = QtCore.QSettings()
    # self.leSavedName.setText("Saving...")
    name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
    fullpath = "/".join((directory, name))

    # check if there is already a file with the standard name
    if os.path.exists(os.path.normpath(fullpath)):
        number_of_saved_images = 1
        # check if there are other files with integers
        while os.path.exists(
            os.path.normpath("".join((fullpath, "n", str(number_of_saved_images))))
        ):
            number_of_saved_images += 1
        name_saved = "".join((name, "n", str(number_of_saved_images)))
        np.save(os.path.normpath(name_saved), image)
    else:
        name_saved = name
        np.save(os.path.normpath(fullpath), image)

    # Store the metadata.
    if config is None:
        config = {}
    config["name_saved"] = name_saved
    with open(f"{directory}/{name}.json", "w") as file:
        json.dump(config, file, indent=4)
    return name


def import_image(
    name: str = "",
    year=None,
    month=None,
    day=None,
    hour=None,
    minute=None,
    second=None,
    directory: str = "M:/Measurement Data/BeamCamera/",
    base: str | None = None,
) -> tuple[np.ndarray, float]:
    """
    Import an image of the beam camera with `name` or supplied with datetime.

    Normally, the images are retrieved in the "Measurement Data/BeamCamera/" folder on the
    measurement share. The timestamp is the file name.
    A text file of that name contains information regarding the camera and a numpy file contains the
    image.
    """

    if name == "":
        assert (
            hour is not None and minute is not None and second is not None
        ), "Time is not specified"  # noqa
        # If name is not specified, use specified keywords. If date is not
        # fully specified, use values from today.
        now = datetime.datetime.now()
        if year is None:
            year = now.year
        if month is None:
            month = now.month
        if day is None:
            day = now.day
        name = f"{year}-{month:02}-{day:02}T{hour:02}.{minute:02}.{second:02}"
    if base is not None:
        directory = f"{base}/Measurement Data/BeamCamera/"
    if directory is not None:
        path = os.path.normpath(f"{directory}/{name}")
    else:
        path = os.path.normpath(f"daBeamCamera/{name}")

    # Get the pixel configuration, especially the pixel length.
    try:
        with open(path + ".json", "r") as file:
            config = json.load(file)
    except FileNotFoundError:
        with open(path + ".txt", "r") as file:
            model = file.readline().split()[-1]
            pixel_length = pixel_lengths.get(model, 1)
    else:
        model = config["model"]
        pl, unit = config["pixel_length"].split()
        pixel_length = float(pl)
        assert unit == "µm", f"Different pixel length unit '{unit}' than 'µm'!"
    print(model, f"pixel length: {pixel_length} µm")
    return np.load(path + ".npy"), pixel_length

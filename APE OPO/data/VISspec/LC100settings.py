"""
Module for the Settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Burger
"""

from typing import Any

from qtpy import QtCore, QtWidgets, uic
from qtpy.QtCore import Slot as pyqtSlot  # type: ignore


class LC100Signals(QtCore.QObject):
        """Signals for the settings."""
        LC100configured = QtCore.Signal()


# For definition via designer

class LC100Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    buttonBox = QtWidgets.QDialogButtonBox
    sbIntTime: QtWidgets.QDoubleSpinBox
    lbIntTime: QtWidgets.QLabel
    cbIntTime: QtWidgets.QComboBox
    cbOperatingMode: QtWidgets.QComboBox
    lbOperatingMode: QtWidgets.QLabel
    sbTriggerDelay: QtWidgets.QDoubleSpinBox
    lbTriggerDelay: QtWidgets.QLabel
    cbTriggerDelay: QtWidgets.QComboBox
    sbEvalBoxNumber: QtWidgets.QSpinBox
    lbX1: QtWidgets.QLabel
    lbX2: QtWidgets.QLabel
    lbY1: QtWidgets.QLabel
    lbY2: QtWidgets.QLabel
    sbX1: QtWidgets.QSpinBox
    sbX2: QtWidgets.QSpinBox
    sbY1: QtWidgets.QDoubleSpinBox
    sbY2: QtWidgets.QDoubleSpinBox
    lbManufacturerName: QtWidgets.QLabel
    lbDeviceName: QtWidgets.QLabel
    lbSerialNumber: QtWidgets.QLabel
    lbDescription: QtWidgets.QLabel
    lbNumberOfPixels: QtWidgets.QLabel
    lbFirmwareRevision: QtWidgets.QLabel
    lbDriverRevision: QtWidgets.QLabel

    def __init__(self, **kwargs) -> None:
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__(**kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/VISspec/LC100settings.ui", self)
        self.show()

        # Configure settings.
        self.signals = LC100Signals()
        self.settings = QtCore.QSettings()
        # Convenience list for widgets with value(), SetValue() methods.
        self.sets: tuple[
            tuple[QtWidgets.QSpinBox | QtWidgets.QDoubleSpinBox, str, Any, type], ...
        ] = ((self.sbIntTime, 'intTime', 0.1, float),
             (self.sbTriggerDelay, 'triggerDelay', 0.0001, float),
             (self.sbEvalBoxNumber, 'evalBoxNumber', 0, int),
             (self.sbX1, 'x1', 0, int),
             (self.sbX2, 'x2', 2048, int),
             (self.sbY1, 'y1', 0.0, float),
             (self.sbY2, 'y2', 1.0, float)
        )
        self.cbSets: tuple[tuple[QtWidgets.QComboBox, str, Any, type]
        ] = ((self.cbIntTime, 'intTimeUnit', 0, int),
             (self.cbTriggerDelay, 'triggerDelayUnit', 0, int),
             (self.cbOperatingMode, 'operatingMode', 0, int))
        self.lbSets: tuple[tuple[QtWidgets.QLabel, str, Any, type]
        ] = ((self.lbSerialNumber, 'serialNumber', '', str),
             (self.lbDescription, 'description', '', str),
             (self.lbDeviceName, 'deviceNameLC100', '', str),
             (self.lbDriverRevision, 'driverRevision', '', str),
             (self.lbIntTime, 'actIntTime', '', str),
             (self.lbManufacturerName, 'manufacturer', '', str),
             (self.lbFirmwareRevision, 'firmwareRevision', '', str),
             (self.lbNumberOfPixels, 'numberOfPixels', '', str),
             (self.lbOperatingMode, 'actOperatingMode', '', str),
             (self.lbTriggerDelay, 'actTriggerDelay', '', str),
             (self.lbX1, 'actX1', '', str),
             (self.lbX2, 'actX2', '', str),
             (self.lbY1, 'actY1', '', str),
             (self.lbY2, 'actY2', '', str))

        self.changeBoundariesIntTime(self.settings.value('intTimeUnit'))
        self.changeBoundariesTriggerDelay(self.settings.value('triggerDelayUnit'))

        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbRestoreDefaults = self.buttonBox.button(restore)  # type: ignore

        self.cbIntTime.currentIndexChanged.connect(self.changeBoundariesIntTime)
        self.cbTriggerDelay.currentIndexChanged.connect(self.changeBoundariesTriggerDelay)


    @pyqtSlot(int)
    def changeBoundariesIntTime(self, index):
        '''Change the upper / lower boundary for the int time depending on the chosen unit.'''
        if index == 0:
            self.sbIntTime.setMinimum(1)
            self.sbIntTime.setMaximum(50)
        elif index == 1:
            self.sbIntTime.setMinimum(1.054)
            self.sbIntTime.setMaximum(1000)

    @pyqtSlot(int)
    def changeBoundariesTriggerDelay(self, index):
        '''Change the upper / lower boundary for the int time depending on the chosen unit.'''
        if index == 0:
            self.sbTriggerDelay.setMinimum(1)
            self.sbTriggerDelay.setMaximum(50)
        elif index == 1:
            self.sbTriggerDelay.setMinimum(1)
            self.sbTriggerDelay.setMaximum(1000)
        elif index == 2:
            self.sbTriggerDelay.setMinimum(4.525)
            self.sbTriggerDelay.setMaximum(1000)

    def readValues(self) -> None:
        """Read the stored values and show them on the user interface."""
        for widget, name, value, typ in self.sets:
            if widget == self.sbIntTime:
                multiplier = int(10**(3 * self.settings.value('intTimeUnit', type=int)))
            elif widget == self.sbTriggerDelay:
                multiplier = int(10**(3 * self.settings.value('triggerDelayUnit', type=int)))
            else:
                multiplier = 1
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ) * multiplier)
        for widget, name, value, typ in self.cbSets:
            widget.setCurrentIndex(self.settings.value(name, defaultValue=value, type=typ))
        for widget, name, value, typ in self.lbSets:
            widget.setText(self.settings.value(name, defaultValue=value, type=typ))

    @pyqtSlot()
    def restoreDefaults(self) -> None:
        """Restore the user interface to default values."""
        for widget, name, value, typ in self.sets:
            widget.setValue(value)

    @pyqtSlot()
    def accept(self) -> None:
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for widget, name, value, typ in self.sets:
            if widget == self.sbIntTime:
                multiplier = int(10**(3 * self.settings.value('intTimeUnit', type=int)))
            elif widget == self.sbTriggerDelay:
                multiplier = 10**(3 * self.settings.value('triggerDelayUnit', type=int))
            else:
                multiplier = 1
            self.settings.setValue(name, widget.value() / multiplier)
        for widget, name, value, typ in self.cbSets:
            self.settings.setValue(name, widget.currentIndex())
        self.signals.LC100configured.emit()
        super().accept()

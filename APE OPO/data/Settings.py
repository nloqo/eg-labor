"""
Module for the settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
from devices import motors


class Settings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    sbPublisherPort: QtWidgets.QSpinBox
    sbIntervalMotor: QtWidgets.QSpinBox
    sbP: QtWidgets.QSpinBox
    sbI: QtWidgets.QSpinBox
    sbD: QtWidgets.QSpinBox
    pbCavityMotorSettings: QtWidgets.QPushButton
    pbOPOMotorSettings: QtWidgets.QPushButton
    pbGratingMotor: QtWidgets.QPushButton
    lbLastCavPos: QtWidgets.QLabel
    lbLastSHGPos: QtWidgets.QLabel
    leDeviceName: QtWidgets.QLineEdit
    buttonBox: QtWidgets.QDialogButtonBox

    def __init__(self):
        """Initialize the dialog."""
        # Use initialization of parent class QDialog.
        super().__init__()

        # Load the user interface file and show it.
        uic.loadUi("data/Settings.ui", self)
        self.show()

        # Configure settings.
        self.signals = self.SettingsSignals()
        self.settings = QtCore.QSettings()

        # Convenience list for widgets with value(), SetValue() methods.
        self.sets = (
            # name of widget, key of setting, defaultValue, type of data
            # (self.widget, 'name', 0, int),
            (self.sbIntervalMotor, "intervalMotor", 50, int),
            (self.sbPublisherPort, "publisherPort", 11100, int),
            (self.sbP, "P", 2000, int),
            (self.sbI, "I", 100, int),
            (self.sbD, "D", 0, int)
        )
        self.labelSets = (
            (self.lbLastCavPos, "lastCavPos", "0 mm", str),
            (self.lbLastSHGPos, "lastSHGPos", "0 mm", str),
        )
        self.textSets: tuple[
            tuple[QtWidgets.QLineEdit, str, str, type], ...
        ] = (
            (self.leDeviceName, 'deviceName', 'USB0::0x1313::0x80A0::M00278991::RAW', str),
        )
        self.readValues()

        restore = QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        self.pbCavityMotorSettings.clicked.connect(self.openResonatorSettings)
        self.pbOPOMotorSettings.clicked.connect(self.openSHGSettings)
        self.pbGratingMotor.clicked.connect(self.openGratingMotorSettings)
        self.pbRestoreDefaults = self.buttonBox.button(restore)
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings."""
        motorConfigured = QtCore.pyqtSignal(str, int)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        for setting in self.labelSets:
            widget, name, value, typ = setting
            widget.setText(self.settings.value(name, defaultValue=value, type=typ))
        for widget, name, value, typ in self.textSets:
            widget.setText(self.settings.value(name, defaultValue=value, type=typ))

    @pyqtSlot()
    def openResonatorSettings(self):
        motorSettings = motors.MotorSettings(key="resonator", motorName="resonator")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("resonator", 1)

    @pyqtSlot()
    def openSHGSettings(self):
        motorSettings = motors.MotorSettings(key="crystal", motorName="crystal")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("crystal", 1)

    @pyqtSlot()
    def openGratingMotorSettings(self):
        motorSettings = motors.MotorSettings(key="gratingMotor", motorName="gratingMotor")
        if motorSettings.exec():
            self.signals.motorConfigured.emit("gratingMotor")

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)
        for setting in self.labelsets:
            widget, name, value, typ = setting
            widget.setText(value)
        for widget, name, value, typ in self.textSets:
            widget.setText(value)

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        for setting in self.labelSets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.text())
        for widget, name, value, typ in self.textSets:
            self.settings.setValue(name, widget.text())
        super().accept()  # make the normal accept things

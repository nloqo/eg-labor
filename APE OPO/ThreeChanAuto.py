from PyQt6 import QtWidgets, QtCore, uic
import logging
from PyQt6.QtCore import pyqtSlot
import nidaqmx as ni
import numpy as np
import datetime
import json
import pandas as pd
from scipy.optimize import curve_fit

from devices import motors

from data import AutoSettings

log = logging.getLogger("APEOPO")


class ThreeChanAuto(QtWidgets.QDialog):

    pbAdjustment: QtWidgets.QPushButton
    pbStart: QtWidgets.QPushButton
    pbStop: QtWidgets.QPushButton
    pbSave: QtWidgets.QPushButton
    pbSettings: QtWidgets.QPushButton
    rbNormalize: QtWidgets.QRadioButton
    lbPulseWidthMira: QtWidgets.QLabel
    lbInputMira: QtWidgets.QLabel
    lbPulseWidthIR: QtWidgets.QLabel
    lbInputIR: QtWidgets.QLabel
    lbPulseWidthVIS: QtWidgets.QLabel
    lbInputVIS: QtWidgets.QLabel

    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent

        # Load the user interface file and show it.
        uic.loadUi("data/3ChanAuto.ui", self)
        self.show()

        # Data Acquisition
        self.dat = {}
        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readData)
        self.readoutTimer.start()

        # Initialize program
        self.autostartConnect()
        self.signals = self.MotorSignals()
        self.signals.startPos.connect(self.startMeasurement)
        self.signals.endPos.connect(self.reset)
        self.motorTimer = QtCore.QTimer()
        self.motorTimer.timeout.connect(self.waitMotor)
        log.info("Initialized.")

        # Connect actions to slots.
        self.pbSettings.clicked.connect(self.openSettings)

        # Connect buttons to slots
        self.pbAdjustment.clicked.connect(self.adjustment)
        self.pbStart.clicked.connect(self.start)
        self.pbStop.clicked.connect(self.stop)
        self.pbSave.clicked.connect(self.save)

    # %%% General functions

    def autostartConnect(self):
        self.pbStart.setEnabled(True)
        self.configurePlot()

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        try:
            self.stop()
            self.readoutTimer.stop()
        except Exception as exc:
            log.error(exc)

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        self.settingsDialog = AutoSettings.Settings()
        self.settingsDialog.open()

    def configurePlot(self):
        self.figureMira = self.plotMira.plot([])
        self.figureMira_fit = self.plotMira.plot([], pen='r')
        self.figureIR = self.plotIR.plot([])
        self.figureIR_fit = self.plotIR.plot([], pen='r')
        self.figureVIS = self.plotVIS.plot([])
        self.figureVIS_fit = self.plotVIS.plot([], pen='r')
        self.plotMira.setLabel("bottom", "Position in mm")
        self.plotMira.setLabel("left", "Mira SHG signal in V")
        self.plotIR.setLabel("bottom", "Position in mm")
        self.plotIR.setLabel("left", "IR SHG signal in V")
        self.plotVIS.setLabel("bottom", "Position in mm")
        self.plotVIS.setLabel("left", "VIS SHG signal in V")

    @pyqtSlot()
    def readData(self):
        settings = QtCore.QSettings()
        with ni.Task() as task:
            task.ai_channels.add_ai_voltage_chan(
                f"{settings.value('DAQName')}/ai{settings.value('MiraAuto')}",
                name_to_assign_to_channel="MiraAuto")
            task.ai_channels.add_ai_voltage_chan(
                f"{settings.value('DAQName')}/ai{settings.value('IRAuto')}",
                name_to_assign_to_channel="IRAuto")
            task.ai_channels.add_ai_voltage_chan(
                f"{settings.value('DAQName')}/ai{settings.value('VISAuto')}",
                name_to_assign_to_channel="VISAuto")
            task.ai_channels.add_ai_voltage_chan(
                f"{settings.value('DAQName')}/ai{settings.value('MiraPD')}",
                name_to_assign_to_channel="MiraPD")
            task.ai_channels.add_ai_voltage_chan(
                f"{settings.value('DAQName')}/ai{settings.value('IRPD')}",
                name_to_assign_to_channel="IRPD")
            task.ai_channels.add_ai_voltage_chan(
                f"{settings.value('DAQName')}/ai{settings.value('VISPD')}",
                name_to_assign_to_channel="VISPD")
            task.timing.cfg_samp_clk_timing(
                40000,
                source="",
                active_edge=ni.constants.Edge.RISING,
                sample_mode=ni.constants.AcquisitionType.CONTINUOUS,
            )
            DAQ_data = task.read(number_of_samples_per_channel=10)
        self.dat["MiraPD"] = -np.mean(DAQ_data[3])
        self.dat["IRPD"] = -np.mean(DAQ_data[4])
        self.dat["VISPD"] = -np.mean(DAQ_data[5])
        self.dat["MiraAuto"] = -np.mean(DAQ_data[0])
        self.dat["MiraAutoNorm"] = self.dat["MiraAuto"] / self.dat["MiraPD"]
        self.dat["IRAuto"] = -np.mean(DAQ_data[1])
        self.dat["IRAutoNorm"] = self.dat["IRAuto"] / self.dat["IRPD"]
        self.dat["VISAuto"] = -np.mean(DAQ_data[2])
        self.dat["VISAutoNorm"] = self.dat["VISAuto"] / self.dat["VISPD"]
        self.lbInputMira.setText(str(round(self.dat["MiraPD"], 5)))
        self.lbInputIR.setText(str(round(self.dat["IRPD"], 5)))
        self.lbInputVIS.setText(str(round(self.dat["VISPD"], 5)))

    @pyqtSlot()
    def adjustment(self):
        try:
            self.adjustment.open()
        except AttributeError:
            self.adjustment = Adjustment(self)
            self.adjustment.open()

    @pyqtSlot()
    def start(self):
        self.figureIR_fit.setData([])
        self.figureMira_fit.setData([])
        self.figureVIS_fit.setData([])
        self.pbStart.setEnabled(False)
        settings = QtCore.QSettings()
        self.config = settings.value("Stage", type=dict)
        self.motor = self.config["motorNumber"]
        # initialize motor
        self.startPos = settings.value("startPos", type=float)
        self.endPos = self.startPos + settings.value("distance", type=float)
        steps = motors.unitsToSteps(self.startPos, self.config)
        self.position = self.startPos
        self.parent.motorCard2.move_to(self.motor, steps, velocity=100)
        self.motorTimer.start(50)

    @pyqtSlot()
    def waitMotor(self):
        actPosSteps = self.parent.motorCard2.motors[self.config["motorNumber"]].actual_position
        actPosUnits = motors.stepsToUnits(actPosSteps, self.config)
        if self.position == self.startPos:
            log.info(f'Move to start... (actual position: {actPosUnits} mm)')
            if self.parent.motorCard2.motors[self.config["motorNumber"]].get_position_reached():
                log.info("Start position reached.")
                self.motorTimer.stop()
                self.signals.startPos.emit(True)
        elif self.position == self.endPos:
            log.info(f'Measurement in progress... (actual position: {actPosUnits} mm)')
            self.data["Position"].append(actPosUnits)
            if self.rbNormalize.isChecked():
                self.data["PDMira"].append(self.dat["MiraAutoNorm"])
                self.data["PDIR"].append(self.dat["IRAutoNorm"])
                self.data["PDVIS"].append(self.dat["VISAutoNorm"])
            else:
                self.data["PDMira"].append(self.dat["MiraAuto"])
                self.data["PDIR"].append(self.dat["IRAuto"])
                self.data["PDVIS"].append(self.dat["VISAuto"])
            self.figureMira.setData(self.data['Position'], self.data['PDMira'])
            self.figureIR.setData(self.data['Position'], self.data['PDIR'])
            self.figureVIS.setData(self.data['Position'], self.data['PDVIS'])
            if self.parent.motorCard2.motors[self.config["motorNumber"]].get_position_reached():
                log.info('Measurement completed.')
                self.motorTimer.stop()
                self.signals.endPos.emit(True)
        else:
            log.exception('Moving position not defined.')

    @pyqtSlot()
    def stop(self):
        self.parent.motorCard2.stop(self.config['motorNumber'])
        self.motorTimer.stop()
        self.pbStart.setEnabled(True)
        log.info("Measurement stopped.")

    @pyqtSlot()
    def save(self):
        '''Save the data with the position in mm and the signal for Mira, VIS and IR.'''
        settings = QtCore.QSettings()
        path = settings.value('path')
        name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        with open(f'{path}{name}.json', "w") as file:
            json.dump((['Position', 'PDMira', 'PDIR', 'PDVIS'], self.data,
                       ['mm', 'arb. units', 'arb. units', 'arb. units']), file)

    @pyqtSlot(bool)
    def startMeasurement(self):
        log.info('Start Measurement.')
        steps = motors.unitsToSteps(self.endPos, self.config)
        self.position = self.endPos
        self.parent.motorCard2.move_to(self.motor, steps, velocity=self.config['positioningSpeed'])
        self.data = {"Position": [], "PDMira": [], "PDIR": [], "PDVIS": []}
        self.motorTimer.start(50)

    @pyqtSlot(bool)
    def reset(self):
        # Fitfunction for Autocorrelation
        def f_auto(x, a, b, c, d):
            return a + b * (np.cosh((x - d) / c)) ** (-3.7)

        def t_pulse(p):
            return 2 * p * np.arccosh(2**(1 / 3.7)) / 1.54 / 299792458

        steps = motors.unitsToSteps(self.startPos, self.config)
        self.parent.motorCard2.move_to(self.motor, steps, velocity=200)
        dat = pd.DataFrame(
            {
                "Pos": np.array(self.data["Position"]) * 1e-3,
                "PDMira": self.data["PDMira"],
                "PDIR": self.data["PDIR"],
                "PDVIS": self.data["PDVIS"],
            }
        )
        dat = dat.dropna()
        try:
            fitParameter1, fitCovariance1 = curve_fit(
                f_auto,
                dat["Pos"],
                dat["PDMira"],
                p0=[
                    dat["PDMira"][0],
                    max(dat["PDMira"]),
                    1e-3,
                    dat["Pos"][list(dat["PDMira"]).index(max(dat["PDMira"]))],
                ],
            )
            self.pulselengthMira = t_pulse(fitParameter1[2])
            self.lbPulseWidthMira.setText(f'{round(self.pulselengthMira * 1e12 * 2, 2)} ps')
            self.figureMira_fit.setData(dat['Pos'] * 1e3, f_auto(dat['Pos'], *fitParameter1))
        except Exception as exc:
            log.exception(f'Could not fit to Mira data with exception: {exc}')
        try:
            fitParameter2, fitCovariance2 = curve_fit(
                f_auto,
                dat["Pos"],
                dat["PDIR"],
                p0=[
                    dat["PDIR"][0],
                    max(dat["PDIR"]),
                    1e-3,
                    dat["Pos"][list(dat["PDIR"]).index(max(dat["PDIR"]))],
                ],
            )
            self.pulselengthIR = t_pulse(fitParameter2[2])
            self.lbPulseWidthIR.setText(f'{round(self.pulselengthIR * 1e12 * 2, 2)} ps')
            self.figureIR_fit.setData(dat['Pos'] * 1e3, f_auto(dat['Pos'], *fitParameter2))
        except Exception as exc:
            log.exception(f'Could not fit to IR data with exception: {exc}')
        try:
            fitParameter3, fitCovariance3 = curve_fit(
                f_auto,
                dat["Pos"],
                dat["PDVIS"],
                p0=[
                    dat["PDVIS"][0],
                    max(dat["PDVIS"]),
                    1e-3,
                    dat["Pos"][list(dat["PDVIS"]).index(max(dat["PDVIS"]))],
                ],
            )
            self.pulselengthVIS = t_pulse(fitParameter3[2])
            self.lbPulseWidthVIS.setText(f'{round(self.pulselengthVIS * 1e12 * 2, 2)} ps')
            self.figureVIS_fit.setData(dat['Pos'] * 1e3, f_auto(dat['Pos'], *fitParameter3))
        except Exception as exc:
            log.exception(f'Could not fit to IR data with exception: {exc}')
        self.pbStart.setEnabled(True)

    class MotorSignals(QtCore.QObject):
        """Signals for motor."""

        # General signals.
        startPos = QtCore.pyqtSignal(bool)
        endPos = QtCore.pyqtSignal(bool)


class Adjustment(QtWidgets.QDialog):

    sbSampleNumber: QtWidgets.QSpinBox
    sbMoveStage: QtWidgets.QDoubleSpinBox
    sbSweepLower: QtWidgets.QDoubleSpinBox
    sbSweepUpper: QtWidgets.QDoubleSpinBox
    sbIRBBO: QtWidgets.QDoubleSpinBox
    sbVISBBO: QtWidgets.QDoubleSpinBox
    rbShowAll: QtWidgets.QRadioButton
    rbMira: QtWidgets.QRadioButton
    rbIR: QtWidgets.QRadioButton
    rbVIS: QtWidgets.QRadioButton
    pbMoveStage: QtWidgets.QPushButton
    pbSweep: QtWidgets.QPushButton
    pbMoveIR: QtWidgets.QPushButton
    pbMoveVIS: QtWidgets.QPushButton
    lbActStage: QtWidgets.QLabel
    lbActIR: QtWidgets.QLabel
    lbActVIS: QtWidgets.QLabel

    def __init__(self, parent, *args, **kwargs):
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)
        self.parent = parent
        self.grandpa = self.parent.parent

        # Load the user interface file and show it.
        uic.loadUi("data/Adjustment.ui", self)
        self.show()

        # Initialize window
        settings = QtCore.QSettings()
        self.Mira = []
        self.IR = []
        self.VIS = []
        self.rbIR.setStyleSheet("QRadioButton{color: red}")
        self.rbMira.setStyleSheet("QRadioButton{color: green}")
        self.rbVIS.setStyleSheet("QRadioButton{color: blue}")

        self.setupPlot()
        self.motorTimer = QtCore.QTimer()
        self.motorTimer.timeout.connect(self.waitMotor)

        try:
            self.readoutTimer = QtCore.QTimer()
            self.readoutTimer.timeout.connect(self.readout)
            self.readoutTimer.start(settings.value("Interval"))
        except Exception as exc:
            log.error(exc)

        # self.signals = self.MotorSignals()

        # self.signals.sweep.connect(self.sweep)
        # self.signals.endPos.connect(self.reset)

        # Connect buttons to slots
        self.pbSweep.clicked.connect(self.sweep)
        self.pbMoveStage.clicked.connect(self.moveStage)
        self.pbMoveIR.clicked.connect(self.moveIR)
        self.pbMoveVIS.clicked.connect(self.moveVIS)

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        settings = QtCore.QSettings()
        try:
            self.readoutTimer.stop()
            self.grandpa.motorCard2.stop(settings.value('Stage')['motorNumber'])
            self.pbSweep.setChecked(False)
        except Exception as exc:
            log.error(exc)

        # accept the close event (reject it, if you want to do something else)
        event.accept()

    def setupPlot(self):
        self.figureMira = self.plot.plot([], pen="g", label="Mira")
        self.figureIR = self.plot.plot([], pen="r", label="IR")
        self.figureVIS = self.plot.plot([], pen="b", label="VIS")
        self.plot.setLabel("bottom", "Samples")
        self.plot.setLabel("left", "Mira SHG signal in V")

    def readout(self):
        settings = QtCore.QSettings()
        config = settings.value('Stage')
        stageMotor = self.grandpa.motorCard2.motors[config['motorNumber']]
        stagePos = motors.stepsToUnits(stageMotor.actual_position, config)
        self.lbActStage.setText(f'{round(stagePos, 2)} mm')
        config = settings.value('BBOIR')
        IRMotor = self.grandpa.motorCard1.motors[config['motorNumber']]
        IRPos = motors.stepsToUnits(IRMotor.actual_position, config)
        self.lbActIR.setText(f'{round(IRPos, 2)} mm')
        config = settings.value('BBOVIS')
        VISMotor = self.grandpa.motorCard1.motors[config['motorNumber']]
        VISPos = motors.stepsToUnits(VISMotor.actual_position, config)
        self.lbActVIS.setText(f'{round(VISPos, 2)} mm')
        length = self.sbSampleNumber.value()
        if self.rbShowAll.isChecked():
            if len(self.Mira) == length:
                del self.Mira[0]
                self.Mira.append(self.parent.dat['MiraAuto'])
            elif len(self.Mira) > length:
                del self.Mira[0:len(self.Mira) - length + 1]
                self.Mira.append(self.parent.dat['MiraAuto'])
            else:
                self.Mira.append(self.parent.dat['MiraAuto'])
            if len(self.IR) == length:
                del self.IR[0]
                self.IR.append(self.parent.dat["IRAuto"])
            elif len(self.IR) > length:
                del self.IR[0:len(self.IR) - length + 1]
                self.IR.append(self.parent.dat["IRAuto"])
            else:
                self.IR.append(self.parent.dat["IRAuto"])
            if len(self.VIS) == length:
                del self.VIS[0]
                self.VIS.append(self.parent.dat["VISAuto"])
            elif len(self.VIS) > length:
                del self.VIS[0:len(self.VIS) - length + 1]
                self.VIS.append(self.parent.dat["VISAuto"])
            else:
                self.VIS.append(self.parent.dat["VISAuto"])
        elif self.rbMira.isChecked():
            if len(self.Mira) == length:
                del self.Mira[0]
                self.Mira.append(self.parent.dat['MiraAuto'])
            elif len(self.Mira) > length:
                del self.Mira[0:len(self.Mira) - length + 1]
                self.Mira.append(self.parent.dat['MiraAuto'])
            else:
                self.Mira.append(self.parent.dat['MiraAuto'])
            if len(self.IR) > 0:
                self.IR = []
            if len(self.VIS) > 0:
                self.VIS = []
        elif self.rbIR.isChecked():
            if len(self.IR) == length:
                del self.IR[0]
                self.IR.append(self.parent.dat["IRAuto"])
            elif len(self.IR) > length:
                del self.IR[0:len(self.IR) - length + 1]
                self.IR.append(self.parent.dat["IRAuto"])
            else:
                self.IR.append(self.parent.dat["IRAuto"])
            if len(self.Mira) > 0:
                self.Mira = []
            if len(self.VIS) > 0:
                self.VIS = []
        elif self.rbVIS.isChecked():
            if len(self.VIS) == length:
                del self.VIS[0]
                self.VIS.append(self.parent.dat["VISAuto"])
            elif len(self.VIS) > length:
                del self.VIS[0:len(self.VIS) - length + 1]
                self.VIS.append(self.parent.dat["VISAuto"])
            else:
                self.VIS.append(self.parent.dat["VISAuto"])
            if len(self.IR) > 0:
                self.IR = []
            if len(self.Mira) > 0:
                self.Mira = []
        else:
            if len(self.VIS) > 0:
                self.VIS = []
            if len(self.IR) > 0:
                self.IR = []
            if len(self.Mira) > 0:
                self.Mira = []
        self.figureMira.setData(self.Mira)
        self.figureIR.setData(self.IR)
        self.figureVIS.setData(self.VIS)

    # Sweep Stage
    @pyqtSlot(bool)
    def sweep(self, checked):
        settings = QtCore.QSettings()
        self.start = round(self.sbSweepLower.value(), 2)
        self.end = round(self.sbSweepUpper.value(), 2)
        self.config = settings.value("Stage", type=dict)
        self.motor = self.config["motorNumber"]
        actPosSteps = self.grandpa.motorCard2.motors[self.config["motorNumber"]].actual_position
        actPosUnits = motors.stepsToUnits(actPosSteps, self.config)
        if checked:
            self.pbMoveStage.setEnabled(False)
            if (round(actPosUnits, 2) > self.end
               or (round(actPosUnits, 2) < self.end and round(actPosUnits, 2) >= self.start)):
                self.position = self.end
            elif round(actPosUnits, 4) == round(self.end, 2):
                self.position = self.start
            elif round(actPosUnits, 2) < self.start:
                self.position = self.start
            steps = motors.unitsToSteps(self.position, self.config)
            self.grandpa.motorCard2.move_to(self.motor, steps,
                                            velocity=self.config['positioningSpeed'])
            self.motorTimer.start(settings.value("interval"))
        else:
            self.pbMoveStage.setEnabled(True)
            try:
                self.motorTimer.stop()
                self.grandpa.motorCard2.stop(self.motor)
            except Exception as exc:
                log.error(exc)

    def waitMotor(self):
        if not self.pbSweep.isChecked():
            self.motorTimer.stop()
            self.pbMoveStage.setEnabled(True)
            self.grandpa.motorCard2.stop(self.motor)
        if self.grandpa.motorCard2.motors[self.config["motorNumber"]].get_position_reached():
            self.motorTimer.stop()
            self.sweep(self.pbSweep.isChecked())

    @pyqtSlot()
    def moveStage(self):
        settings = QtCore.QSettings()
        config = settings.value('Stage')
        steps = motors.unitsToSteps(self.sbMoveStage.value(), config)
        self.grandpa.motorCard2.move_to(config['motorNumber'], steps,
                                        velocity=config['positioningSpeed'])

    @pyqtSlot()
    def moveIR(self):
        settings = QtCore.QSettings()
        config = settings.value('BBOIR')
        steps = motors.unitsToSteps(self.sbIRBBO.value(), config)
        motorNumber = config['motorNumber']
        self.grandpa.motorCard1.move_to(motorNumber, steps,
                                        velocity=config['positioningSpeed'])

    @pyqtSlot()
    def moveVIS(self):
        settings = QtCore.QSettings()
        config = settings.value('BBOVIS')
        steps = motors.unitsToSteps(self.sbVISBBO.value(), config)
        motorNumber = config['motorNumber']
        self.grandpa.motorCard1.move_to(motorNumber, steps,
                                        velocity=config['positioningSpeed'])

    class MotorSignals(QtCore.QObject):
        sweep = QtCore.pyqtSignal()

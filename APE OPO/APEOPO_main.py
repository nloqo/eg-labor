"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging
import numpy as np

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot

from devices import motors
from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110, TMCM1140
from pyleco.utils import data_publisher
from pyleco.utils.qt_listener import QtListener
from pyleco.directors.transparent_director import TransparentDirector
from pyleco_extras.directors.analyzing_director import AnalyzingDirector
from pyleco_extras.gui_utils.base_main_window import   LECOBaseMainWindowDesigner
import pyleco.json_utils.errors as pylecoError

# Local packages.
from devices.APE_OPO_com import APE_OPO
from data import Settings
from ThreeChanAuto import ThreeChanAuto
from VISspec import VISspec

log = logging.getLogger("APEOPO")


APE_OPO_NAME = "pico3.ape_opo"


class APEOPO(LECOBaseMainWindowDesigner):
    """Define the main window and essential methods of the program."""

    pbStopMotors: QtWidgets.QPushButton
    pbSetCavPos: QtWidgets.QPushButton
    pbPlusCavPos: QtWidgets.QPushButton
    pbMinusCavPos: QtWidgets.QPushButton
    pbSetCrystalPos: QtWidgets.QPushButton
    pbPlusCrystalPos: QtWidgets.QPushButton
    pbMinusCrystalPos: QtWidgets.QPushButton
    pbTempStat: QtWidgets.QPushButton
    pbSetTemperature: QtWidgets.QPushButton
    pbAutocorrelator: QtWidgets.QPushButton
    pbOpenVISspec: QtWidgets.QPushButton
    pbCalWavelength: QtWidgets.QPushButton
    pbCopyCavPos: QtWidgets.QPushButton
    pbCopyCrstalPos: QtWidgets.QPushButton
    lbActualCavPos: QtWidgets.QLabel
    lbActualCrystalPos: QtWidgets.QLabel
    lbSHGTemp: QtWidgets.QLabel
    lbHumidity: QtWidgets.QLabel
    lbIRPower: QtWidgets.QLabel
    lbVISPower: QtWidgets.QLabel
    sbTargetCavPos: QtWidgets.QDoubleSpinBox
    sbStepsizeCavPos: QtWidgets.QDoubleSpinBox
    sbTargetCrystalPos: QtWidgets.QDoubleSpinBox
    sbStepsizeCrystalPos: QtWidgets.QDoubleSpinBox
    sbSHGTemp: QtWidgets.QDoubleSpinBox
    chbTemp: QtWidgets.QCheckBox
    pbMoveOPO: QtWidgets.QPushButton
    sbLambda: QtWidgets.QDoubleSpinBox
    lbCavityCheck: QtWidgets.QLabel
    lbOPOCrystalCheck: QtWidgets.QLabel
    lbTemperatureCheck: QtWidgets.QLabel
    actionConnect: QtWidgets.QWidgetAction
    actionReadoutAPE: QtWidgets.QWidgetAction
    actionClose: QtWidgets.QWidgetAction
    actionSettings: QtWidgets.QWidgetAction
    pbStopMovement: QtWidgets.QPushButton
    pbOptimizeOPO: QtWidgets.QPushButton
    sbScalingPower: QtWidgets.QDoubleSpinBox
    sbScalingDisturbingSignal: QtWidgets.QDoubleSpinBox
    sbScalingBandwidth: QtWidgets.QDoubleSpinBox
    sbOffsetTemperature: QtWidgets.QDoubleSpinBox
    sbOffsetCrystalPos: QtWidgets.QDoubleSpinBox
    cbOptimizeTemperature: QtWidgets.QCheckBox
    cbOptimizeOPOCrystal: QtWidgets.QCheckBox
    cbKeepTempConst: QtWidgets.QCheckBox

    def __init__(self, name="APEOPO", ui_file_name="APEOPO", *args, **kwargs):
        # Use initialization of parent class QMainWindow.
        super().__init__(name=name,
                         ui_file_name=ui_file_name,
                         *args, **kwargs)

        self.director = AnalyzingDirector(actor=APE_OPO_NAME, communicator=self.communicator,
                                          device_class=APE_OPO, )

        # Get settings.
        application = QtCore.QCoreApplication.instance()
        application.setOrganizationName("NLOQO")
        application.setApplicationName(name)
        self.setSettings()
        settings = QtCore.QSettings()

        # Timer
        self.motorTimer = QtCore.QTimer()
        self.motorTimer.timeout.connect(self.readMotors)

        self.autoTimer = QtCore.QTimer()
        self.autoTimer.timeout.connect(self.checkLabels)

        self.optimizeTimer = QtCore.QTimer()
        self.optimizeTimer.timeout.connect(self.checkOptimize)

        self.optimizeTimer2 = QtCore.QTimer()
        self.optimizeTimer2.timeout.connect(self.checkSignal)

        '''
        try:
            self.actionConnect.setChecked(True)
            self.connectMotors(True)
        except Exception as exc:
            log.exception(f"Autostart connection to Motors failed: {exc}")
        '''

        self.listener.signals.dataReady.connect(self.readData)
        self.communicator.subscribe(['OPO_hum', 'OPO_SHGtemp', 'OPO_IR', 'OPO_VIS', 'Nova1'])

        # Connect actions to slots.
        self.actionClose.triggered.connect(self.close)
        self.actionConnect.triggered.connect(self.connectMotors)
        self.actionSettings.triggered.connect(self.openSettings)

        # Connect buttons to slots
        self.pbStopMotors.clicked.connect(self.stopMotors)
        self.pbSetCavPos.clicked.connect(self.setCavPos)
        self.pbPlusCavPos.clicked.connect(self.plusCavPos)
        self.pbMinusCavPos.clicked.connect(self.minusCavPos)
        self.pbSetCrystalPos.clicked.connect(self.setCrystalPos)
        self.pbPlusCrystalPos.clicked.connect(self.plusCrystalPos)
        self.pbMinusCrystalPos.clicked.connect(self.minusCrystalPos)
        self.pbAutocorrelator.clicked.connect(self.openAutocorrelator)
        self.pbOpenVISspec.clicked.connect(self.openVISspec)
        self.pbSetTemperature.clicked.connect(self.setSHGTemp)
        self.pbCopyCavPos.clicked.connect(self.copyCavityPosition)
        self.pbCopyCrstalPos.clicked.connect(self.copyCrystalPosition)
        self.pbMoveOPO.clicked.connect(self.setOPOwavelength)
        self.pbStopMovement.clicked.connect(self.stopMovement)
        self.pbOptimizeOPO.clicked.connect(self.optimizeOPO)

        self.data = {}
        self.data['OPO_setSHGTemp'] = self.sbSHGTemp.value()
        self.data['OPO_setLambda'] = self.sbLambda.value()

        self.actionConnect.trigger()
        try:
            log.info(f"Last SHG temperature: {settings.value('LastTemperature')}°C")
            self.chbTemp.setChecked(True)
            self.sbSHGTemp.setValue(float(settings.value('LastTemperature')))
        except Exception as exc:
            log.exception(f'Could not set temperature right: {exc}')

        self.sbTargetCavPos.setValue(float(settings.value("LastCavPos")))
        self.sbTargetCrystalPos.setValue(float(settings.value("LastSHGPos")))
        self.sbStepsizeCavPos.setValue(0.001)
        self.sbStepsizeCrystalPos.setValue(0.002)
        self.sbLambda.setValue(float(settings.value("LastLambda")))
        self.sbScalingPower.setValue(float(settings.value("LastScalingPower")))
        self.sbScalingBandwidth.setValue(float(settings.value("LastScalingBandwidth")))
        self.sbScalingDisturbingSignal.setValue(float(settings.value("LastScalingDisturbingSignal")))

    @pyqtSlot()
    def closeEvent(self, event):
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        settings = QtCore.QSettings()

        self.motorTimer.stop()
        settings.setValue("LastSHGPos", self.lbActualCrystalPos.text().strip("m"))
        settings.setValue("LastCavPos", self.lbActualCavPos.text().strip("m"))
        settings.setValue("LastTemperature", float(self.lbSHGTemp.text()))
        settings.setValue("LastLambda", self.sbLambda.value())
        settings.setValue("LastScalingPower", self.sbScalingPower.value())
        settings.setValue("LastScalingBandwidth", self.sbScalingBandwidth.value())
        settings.setValue("LastScalingDisturbingSignal", self.sbScalingDisturbingSignal.value())
        self.connectMotors(False)

        event.accept()

    @pyqtSlot()
    def openSettings(self):
        """Open the settings dialogue and apply changed settings."""
        self.settingsDialog = Settings.Settings()
        self.settingsDialog.accepted.connect(self.setSettings)
        self.settingsDialog.signals.motorConfigured.connect(
            self.configureMotor)
        self.settingsDialog.open()

    """@pyqtSlot(bool)
    def setup_controllers(self, checked):
        '''Setup the device controller.'''
        settings = QtCore.QSettings()
        log.info('Connect APE controller...')
        if checked:
            try:
                self.APE = Threading.APEOPOController()
                self.thread = QtCore.QThread()
                self.APE.moveToThread(self.thread)
                self.signals = self.CommanderSignals()
                self.signals.close.connect(self.thread.quit)
                self.thread.started.connect(self.APE.listen)
                self.thread.start()
                # Connect readout return signals.
                self.APE.data.connect(self.readData)
                self.APE.connect('ASRL11')
                self.readoutTimer.start(settings.value('intervalAPE', 300, int))
                log.info("APEOPO initialized.")
            except Exception as exc:
                log.exception(f'Could not connect APE controller: {exc}')
                self.actionConnect_APE.setChecked(False)
        else:
            self.readoutTimer.stop()
            try:
                self.APE.disconnect()
            except (AttributeError, ConnectionError):
                pass
            self.actionConnect_APE.setChecked(False)"""

    def connectMotors(self, checked):
        """(Dis)connect the motor card."""
        settings = QtCore.QSettings()
        if checked:
            log.info('Connect motors...')
            COM1 = motors.getPort("APEOPO")
            COM2 = 6
            try:
                self.connectionManager1 = ConnectionManager(f'--port COM{COM1}')
                self.motorCard1 = TMCM6110(self.connectionManager1.connect())
                self.connectionManager2 = ConnectionManager(f'--port COM{COM2}')
                self.motorCard2 = TMCM1140(self.connectionManager2.connect())
            except Exception as exc:
                log.error(f"Connection failed: {exc}")
                self.actionConnect.setChecked(False)
            else:
                log.info("Successfully connected.")
                try:
                    self.configureMotor('resonator', 1)
                    self.configureMotor('crystal', 1)
                    self.configureMotor("Stage", 2)
                    self.configureMotor("BBOIR", 1)
                    self.configureMotor("BBOVIS", 1)
                    config = settings.value('resonator')
                    m = self.motorCard1.motors[config['motorNumber']]
                    m.set_axis_parameter(m.AP.LeftLimitSwitchDisable, 1)
                    m.set_axis_parameter(m.AP.RightLimitSwitchDisable, 1)
                    config = settings.value('crystal')
                    m = self.motorCard1.motors[config['motorNumber']]
                    m.set_axis_parameter(m.AP.LeftLimitSwitchDisable, 1)
                    m.set_axis_parameter(m.AP.RightLimitSwitchDisable, 1)
                    self.motorTimer.start(settings.value('intervalMotor'))
                except Exception as exc:
                    log.exception(f'Configuring motors after connecting failed: {exc}')

        else:
            try:
                self.connectionManager1.disconnect()
                self.connectionManager2.disconnect()
            except AttributeError:
                pass
            except Exception as exc:
                log.error(exc)
            try:
                del self.motorCard1
                del self.motorCard2
            except AttributeError:
                pass

    @pyqtSlot(str, int)
    def configureMotor(self, motorName, motorCard):
        """Configure the motor `motorname`."""
        # Load the config for the motor and configure the motor.
        settings = QtCore.QSettings()
        config = settings.value(motorName, type=dict)
        try:
            if motorCard == 1:
                try:
                    motors.configureMotor(self.motorCard1, config)
                except KeyError:
                    return
            elif motorCard == 2:
                try:
                    motors.configureMotor(self.motorCard2, config)
                except KeyError:
                    return
        except Exception:
            log.exception("Error configuring motor.")

    @pyqtSlot()
    def stopMotors(self):
        for i in range(5):
            self.motorCard1.stop(i)

    @pyqtSlot()
    def setCavPos(self):
        settings = QtCore.QSettings()
        pos = self.sbTargetCavPos.value()
        config = settings.value('resonator', type=dict)
        steps = motors.unitsToSteps(pos, config)
        number = config['motorNumber']
        motor = self.motorCard1.motors[number]
        try:
            motor.move_to(int(steps))
        except ConnectionError:
            log.error("Motors not connected!")

    @pyqtSlot()
    def copyCavityPosition(self):
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(self.lbActualCavPos.text())

    @pyqtSlot()
    def copyCrystalPosition(self):
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(self.lbActualCrystalPos.text())

    @pyqtSlot()
    def plusCavPos(self):
        settings = QtCore.QSettings()
        dist = self.sbStepsizeCavPos.value()
        config = settings.value('resonator', type=dict)
        steps = motors.unitsToSteps(dist, config)
        motor = self.motorCard1.motors[config['motorNumber']]
        try:
            motor.move_by(steps, config['positioningSpeed'])
        except Exception as exc:
            log.error(exc)

    @pyqtSlot()
    def minusCavPos(self):
        settings = QtCore.QSettings()
        dist = self.sbStepsizeCavPos.value()
        config = settings.value('resonator', type=dict)
        steps = motors.unitsToSteps(dist, config)
        motor = self.motorCard1.motors[config['motorNumber']]
        try:
            motor.move_by(-steps, config['positioningSpeed'])
        except Exception as exc:
            log.error(exc)

    @pyqtSlot()
    def setCrystalPos(self):
        settings = QtCore.QSettings()
        pos = self.sbTargetCrystalPos.value()
        config = settings.value('SHG', type=dict)
        steps = motors.unitsToSteps(pos, config)
        number = config['motorNumber']
        motor = self.motorCard1.motors[number]
        try:
            motor.move_to(int(steps))
        except Exception as exc:
            log.error(exc)

    @pyqtSlot()
    def plusCrystalPos(self):
        settings = QtCore.QSettings()
        dist = self.sbStepsizeCrystalPos.value()
        config = settings.value('SHG', type=dict)
        steps = motors.unitsToSteps(dist, config)
        motor = self.motorCard1.motors[config['motorNumber']]
        actualPosSteps = self.motorCard1.motors[config['motorNumber']].actual_position
        actualPos = motors.stepsToUnits(actualPosSteps, config)
        if actualPos + dist <= 7.6:
            try:
                motor.move_by(steps, config['positioningSpeed'])
            except Exception as exc:
                log.error(exc)
        else:
            log.info("Crystal position at upper limit.")

    @pyqtSlot()
    def minusCrystalPos(self):
        settings = QtCore.QSettings()
        dist = self.sbStepsizeCrystalPos.value()
        config = settings.value('SHG', type=dict)
        steps = motors.unitsToSteps(dist, config)
        motor = self.motorCard1.motors[config['motorNumber']]
        actualPosSteps = self.motorCard1.motors[config['motorNumber']].actual_position
        actualPos = motors.stepsToUnits(actualPosSteps, config)
        if actualPos - dist >= 0.2:
            try:
                motor.move_by(-steps, config['positioningSpeed'])
            except Exception as exc:
                log.error(exc)
        else:
            log.info("Crystal position at lower limit.")

    @pyqtSlot()
    def setSHGTemp(self):
        T = self.sbSHGTemp.value()
        log.info(f'New set temperature for SHG: {T}')
        if self.chbTemp.isChecked() and T > 60:
            self.director.device.setTemperatureSHG(T)
            self.data['OPO_setSHGTemp'] = T
        elif self.chbTemp.isChecked() and T <= 60:
            log.info('Temperatures below 60°C not allowed.')
            self.sbSHGTemp.setValue(float(self.lbSHGTemp.text()))
        elif not self.chbTemp.isChecked() and T <= 70:
            self.director.device.setTemperatureSHG(T)
            self.data['OPO_setSHGTemp'] = T
        elif not self.chbTemp.isChecked() and T > 70:
            log.info('Temperatures above 70°C not allowed.')
            self.sbSHGTemp.setValue(float(self.lbSHGTemp.text()))

    def SHGTemperatureModel(self, wavelength):
        aSHG = -1.53915048e-07
        bSHG = 3.29719783e-05
        cSHG = 9.08273990e-03
        dSHG = -1.55113948e+00
        eSHG = 5.19052410e+01
        x0SHG = 5.82703048e+02
        Lambdaeff = wavelength-x0SHG
        TargetTemperature = (aSHG * Lambdaeff**4 + bSHG * Lambdaeff**3
                                  + cSHG * Lambdaeff**2 + dSHG * Lambdaeff + eSHG)
        return TargetTemperature

    def CavityPosModel(self, wavelength, TargetTemperature):
        aCav = 0.00304834
        bCav = -0.00188716
        cCav = -1.19297948
        TargetCavPos = aCav * wavelength + bCav * TargetTemperature + cCav
        return TargetCavPos

    def OPOCrystalPosModel(self, wavelength):
        aOPO = -2.05081649e-11
        bOPO = 1.24599551e-08
        cOPO = -2.71475015e-06
        dOPO = 2.16032856e-04
        eOPO = 7.87956232e-03
        fOPO = 1.00185178e+00
        x0OPO = 5.60138617e+02
        Lambdaeff = wavelength-x0OPO
        TargetOPOPos = (aOPO * Lambdaeff**5 + bOPO * Lambdaeff**4 + cOPO * Lambdaeff**3
                             + dOPO * Lambdaeff**2 + eOPO * Lambdaeff + fOPO)
        return TargetOPOPos

    def derOPOCrystalModel(self, wavelength):
        aOPO = -2.05081649e-11
        bOPO = 1.24599551e-08
        cOPO = -2.71475015e-06
        dOPO = 2.16032856e-04
        eOPO = 7.87956232e-03
        x0OPO = 5.60138617e+02
        Lambdaeff = wavelength-x0OPO
        derivative = (5 * aOPO * Lambdaeff**4 + 4 * bOPO * Lambdaeff**3 + 3 * cOPO * Lambdaeff**2
                      + 2 * dOPO * Lambdaeff + eOPO)
        derivative = 20 * derivative  #norming
        return derivative

    @pyqtSlot()
    def setOPOwavelength(self):
        # End Stabilisation
        if self.VISspec.PIDcheck:
            self.VISspec.pid.set_auto_mode(enabled=False)
            self.VISspec.PIDcheck = False
            self.VISspec.pbStabilize.setChecked(False)
        # Set Target Wavelength
        Lambda = self.sbLambda.value()
        self.data['OPO_setLambda'] = Lambda
        # Calculate Target Parameters
        TargetTemperature = self.SHGTemperatureModel(Lambda) + self.sbOffsetTemperature.value()
        if not self.cbKeepTempConst.isChecked():
            TargetCavPos = self.CavityPosModel(Lambda, TargetTemperature)
        else:
            TargetCavPos = self.CavityPosModel(Lambda, self.sbSHGTemp.value())
        TargetOPOPos = self.OPOCrystalPosModel(Lambda) + self.sbOffsetCrystalPos.value()
        # Update Spinboxes in order to be able to reuse the Move Functions
        if not self.cbKeepTempConst.isChecked():
            self.sbSHGTemp.setValue(TargetTemperature)
        self.sbTargetCavPos.setValue(TargetCavPos)
        self.sbTargetCrystalPos.setValue(TargetOPOPos)
        # Move Parameters
        if not self.cbKeepTempConst.isChecked():
            self.setSHGTemp()
            if not abs(self.sbSHGTemp.value() - TargetTemperature) < 0.1:
                log.info("Temperature can not be reached with the current settings")
                return None
            self.lbTemperatureCheck.setText("Moving")
        self.setCavPos()
        self.lbCavityCheck.setText("Moving")
        self.setCrystalPos()
        self.lbOPOCrystalCheck.setText("Moving")
        # Move VisSpec
        self.VISspec.sbMotorPosition.setValue(Lambda)
        self.VISspec.pbMoveTo.click()
        # Check if Parameters are Moving
        self.autoTimer.start(5000)

    def checkLabels(self):
        if (self.lbTemperatureCheck.text() == 'Moving'
            or self.lbCavityCheck.text() == 'Moving'
            or self.lbOPOCrystalCheck.text() == 'Moving'
            or self.lbTemperatureCheck.text() == "Checking Temperature one more time"):
            if self.lbTemperatureCheck.text() == "Moving":
                if abs(float(self.lbSHGTemp.text()) - self.sbSHGTemp.value()) < 0.2:
                    self.lbTemperatureCheck.setText("Checking Temperature one more time")
                else:
                    None
            elif self.lbTemperatureCheck.text() == "Checking Temperature one more time":
                if abs(float(self.lbSHGTemp.text()) - self.sbSHGTemp.value()) < 0.2:
                    self.lbTemperatureCheck.setText("Value reached")
                else:
                    self.lbTemperatureCheck.setText("Moving")
            if self.lbCavityCheck.text() == 'Moving':
                if abs(float(self.lbActualCavPos.text()[:-2]) - self.sbTargetCavPos.value()) < 0.001:
                    self.lbCavityCheck.setText("Value reached")
            else:
                None
            if self.lbOPOCrystalCheck.text() == 'Moving':
                if abs(float(self.lbActualCrystalPos.text()[:-2]) - self.sbTargetCrystalPos.value()) < 0.001:
                    self.lbOPOCrystalCheck.setText("Value reached")
            else:
                None
            log.info("Parameters are moving")
        else:
            self.autoTimer.stop()
            log.info("Parameters reached")
            if not self.cbKeepTempConst.isChecked():
                self.VISspec.stabilize(True)

    @pyqtSlot()
    def stopMovement(self):
        settings = QtCore.QSettings()
        self.stopMotors()
        self.sbSHGTemp.setValue(float(self.lbSHGTemp.text()))
        T = self.sbSHGTemp.value()
        self.director.device.setTemperatureSHG(T)
        self.autoTimer.stop()
        self.optimizeTimer.stop()
        settings.setValue("P", settings.value("LastP"))
        settings.setValue("I", settings.value("LastI"))

    @pyqtSlot()
    def optimizeOPO(self):
        settings = QtCore.QSettings()
        '''
        if self.VISspec.frqbandwidth > 1000 or self.VISspec.pdvoltage < 0.05:
            if self.VISspec.frqbandwidth > 1000:
                log.info("Improve bandwidth quality first before optimizing")
            elif self.VISspec.pdvoltage < 0.05:
                log.info("Improve power quality first before optimizing")
        else:
        '''
        self.VISspec.sbMotorPosition.setValue(self.sbLambda.value())
        pvalue = settings.value("P")
        ivalue = settings.value("I")
        settings.setValue("LastP", pvalue)
        settings.setValue("LastI", ivalue)
        settings.setValue("P", 400)
        self.VISspec.pbStabilize.setChecked(True)
        self.VISspec.stabilize(True)
        self.datalist = [[], [], [], []]
        log.info("Empty datalist created.")
        # Check, if OPO Crystal Pos changes the signal in this wl regime
        if abs(0.06 * self.derOPOCrystalModel(self.VISspec.wavelength)) > 0.008 and self.cbOptimizeOPOCrystal.isChecked():
            # Define Stepsize as a function of the derivative of Crystal Model and direction of movement
            self.checkCrystal = True
        else:
            self.checkCrystal = False
        log.info(f"Optimizing Crystal? {self.checkCrystal}")
        if (self.VISspec.wavelength < 622 or self.VISspec.wavelength > 670) and self.cbOptimizeTemperature.isChecked():
            self.checkTemp = True
        else:
            self.checkTemp = False
        log.info(f"Optimizing Temperature? {self.checkTemp}")
        if self.checkCrystal or self.checkTemp:
            self.sbStepsizeCrystalPos.setValue(abs(0.06 * self.derOPOCrystalModel(self.VISspec.wavelength)))
            self.actualQuality = False
            self.CrystalMoving = False
            self.alreadyMovedTemp = False
            self.optimizeTime = 2000
            self.optimizeTimer.start(self.optimizeTime)
            log.info("Optimize Timer started.")
        else:
            log.info("OPO Crystal and Temperature not important in this wl regime, please adjust manually")
            settings.setValue("P", settings.value("LastP"))
            settings.setValue("I", settings.value("LastI"))

    def checkOptimize(self):
        settings = QtCore.QSettings()
        # First Loop
        if not self.actualQuality:
            log.info("First Loop")
            self.actualQuality = np.mean(self.datalist[3])
            self.datalist = [[], [], [], []]
            if self.checkTemp:
                log.info("Starting to move Temp")
                T = self.sbSHGTemp.value()
                self.sbSHGTemp.setValue(T - 0.5)
                self.setSHGTemp()
                self.direction = -2  #Moving Temperature in negative direction
                self.optimizeTimer.start(18000)
            elif not self.checkTemp and self.checkCrystal:
                log.info("Starting to move Crystal")
                self.direction = 1
                self.pbPlusCrystalPos.click()
                self.CrystalMoving = True
                self.optimizeTimer.start(2000)
        else:
            # Check if positions are reached
            Crystalconfig = settings.value('SHG', type=dict)
            if self.CrystalMoving:
                log.info("Crystal Moving")
                if self.motorCard1.motors[Crystalconfig["motorNumber"]].get_position_reached():
                    self.CrystalMoving = False
                else:
                    pass
            elif abs(self.direction) == 2 and not self.alreadyMovedTemp and self.checkTemp:
                log.info("Adjusting Temperature")
                self.formerQuality = self.actualQuality
                self.actualQuality = np.mean(self.datalist[3])
                pdvoltage = np.mean(self.datalist[0])
                bandwidth = np.mean(self.datalist[2])
                log.info(f"Former Quality: {self.formerQuality}, Actual Quality: {self.actualQuality}, PDVoltage: {pdvoltage}, bandwidth: {bandwidth}")
                # Quality Check
                if self.actualQuality > self.formerQuality:
                    if abs(float(self.lbSHGTemp.text()) - self.sbSHGTemp.value()) < 0.2:
                        self.datalist = [[], [], [], []]
                        if self.direction == 2:
                            T = self.sbSHGTemp.value()
                            self.sbSHGTemp.setValue(T + 0.5)
                            self.setSHGTemp()
                            self.optimizeTimer.start(6000)
                        elif self.direction == -2:
                            T = self.sbSHGTemp.value()
                            self.sbSHGTemp.setValue(T - 0.5)
                            self.setSHGTemp()
                            self.optimizeTimer.start(6000)
                    else:
                        self.optimizeTimer.start(5000)
                elif self.actualQuality < self.formerQuality:
                    if self.direction == 2:
                        T = float(self.lbSHGTemp.text())
                        self.sbSHGTemp.setValue(T)
                        self.setSHGTemp()
                        if self.checkCrystal:
                            #Switching to Crystal Improvement
                            log.info("Switching to Crystal adjustment")
                            wavelength_derivation = np.mean(abs(np.array(self.datalist[1])-self.sbLambda.value()))
                            wavelength_deviaton = np.std(self.datalist[1])
                            self.direction = -1
                            self.datalist = [[], [], [], []]
                            self.pbMinusCrystalPos.click()
                            self.CrystalMoving = True
                            self.optimizeTimer.start(2000)
                        else:
                            settings.setValue("P", settings.value("LastP"))
                            settings.setValue("I", settings.value("LastI"))
                            self.VISspec.sbMotorPosition.setValue(np.mean(self.datalist[1]))
                            self.optimizeTimer.stop()
                            del self.datalist
                            self.VISspec.stabilize(False)
                            self.VISspec.stabilize(True)
                            log.info("Optimization successfully terminated")
                    elif self.direction == -2:
                        self.direction = 2
                        self.datalist = [[], [], [], []]
                        T = float(self.lbSHGTemp.text())
                        self.sbSHGTemp.setValue(T)
                        self.setSHGTemp()
                        self.optimizeTimer.start(18000)
            elif not self.CrystalMoving and abs(self.direction) == 1 and self.checkCrystal:
                log.info("Adjusting Crystal Position")
                if self.sbStepsizeCrystalPos.value() > 0.007:
                    # Check if Stabilization reached final value
                    wavelength_derivation = np.mean(abs(np.array(self.datalist[1])-self.sbLambda.value()))
                    wavelength_deviaton = np.std(self.datalist[1])
                    # log.info(f"Wavelength Derivation: {wavelength_derivation}")
                    # log.info(f"Standard Deviation: {wavelength_deviaton}")
                    if wavelength_derivation < 0.1 and wavelength_deviaton < 0.2:
                        log.info("Improving Signal Quality")
                        self.formerQuality = self.actualQuality
                        self.actualQuality = np.mean(self.datalist[3])
                        pdvoltage = np.mean(self.datalist[0])
                        bandwidth = np.mean(self.datalist[2])
                        log.info(f"Former Quality: {self.formerQuality}, Actual Quality: {self.actualQuality}, PDVoltage: {pdvoltage}, bandwidth: {bandwidth}")
                        # Quality Check
                        if self.actualQuality > self.formerQuality:
                            self.datalist = [[], [], [], []]
                            if self.direction == 1:
                                self.pbPlusCrystalPos.click()
                                self.CrystalMoving = True
                            elif self.direction == -1:
                                self.pbMinusCrystalPos.click()
                                self.CrystalMoving = True
                        elif self.actualQuality < self.formerQuality:
                            if self.direction == 1:
                                self.direction = -1
                                self.datalist = [[], [], [], []]
                                self.pbMinusCrystalPos.click()
                                self.CrystalMoving = True
                                self.optimizeTimer.start(self.optimizeTime)
                                log.info(f"Optimize Timer started. Optimize Time {self.optimizeTime}")
                            elif self.direction == -1:
                                self.direction = 1
                                self.sbStepsizeCrystalPos.setValue(self.sbStepsizeCrystalPos.value()*0.6)
                                self.datalist = [[], [], [], []]
                                self.pbPlusCrystalPos.click()
                                self.CrystalMoving = True
                                self.optimizeTime = int(0.8 * self.optimizeTime)
                                if self.optimizeTime < 500:
                                    self.optimizeTime = 500
                                self.optimizeTimer.start(self.optimizeTime)
                                log.info(f"Optimize Timer started. Optimize Time {self.optimizeTime}")
                    elif wavelength_derivation > 0.1 and wavelength_deviaton < 0.2:
                        log.info("Waiting for Stabilization")
                        pass
                    elif wavelength_deviaton > 0.2:
                        log.info("Improving Wavelength Deviation")
                        '''
                        self.optimizeTime = 1000
                        if self.direction == 1:
                            self.direction = -1
                            self.sbStepsizeCrystalPos.setValue(self.sbStepsizeCrystalPos.value()*1.6)
                            self.datalist = [[], [], [], []]
                            self.pbMinusCrystalPos.click()
                            self.CrystalMoving = True
                            self.optimizeTimer.start(self.optimizeTime)
                            log.info(f"Optimize Timer started. Optimize Time {self.optimizeTime}")
                        elif self.direction == -1:
                            self.direction = 1
                            self.sbStepsizeCrystalPos.setValue(self.sbStepsizeCrystalPos.value()*1.6)
                            self.datalist = [[], [], [], []]
                            self.pbPlusCrystalPos.click()
                            self.CrystalMoving = True
                            self.optimizeTimer.start(self.optimizeTime)
                            log.info(f"Optimize Timer started. Optimize Time {self.optimizeTime}")
                            '''
                        self.optimizeTimer.stop()
                        settings.setValue("P", settings.value("LastP"))
                        settings.setValue("I", settings.value("LastI"))
                        self.VISspec.stabilize(False)
                        self.sbStepsizeCrystalPos.setValue(0.01)
                        if self.direction == 1:
                            self.direction = -1
                        elif self.direction == -1:
                            self.direction = 1
                        self.datalist = [[], [], [], []]
                        self.optimizeTimer2.start(1000)
                else:
                    settings.setValue("P", settings.value("LastP"))
                    settings.setValue("I", settings.value("LastI"))
                    self.VISspec.sbMotorPosition.setValue(np.mean(self.datalist[1]))
                    self.optimizeTimer.stop()
                    del self.datalist
                    self.VISspec.stabilize(False)
                    self.VISspec.stabilize(True)
                    log.info("Optimization successfully terminated")

    def checkSignal(self):
        settings = QtCore.QSettings()
        Crystalconfig = settings.value('SHG', type=dict)
        if self.CrystalMoving:
            if self.motorCard1.motors[Crystalconfig["motorNumber"]].get_position_reached():
                self.CrystalMoving = False
            else:
                pass
        elif not self.CrystalMoving:
            wavelength = np.mean(self.datalist[1])
            wavelength_derivation = abs(wavelength - self.sbLambda.value())
            wavelength_deviaton = np.std(self.datalist[1])
            modelposition = self.sbTargetCrystalPos.value()
            actualposition = float(self.lbActualCrystalPos.text()[:-2])
            log.info(f"Actual Crystal Position:{actualposition}, Modelposition: {modelposition}")
            if self.VISspec.PIDcheck:
                log.info("Stabilizing")
                if wavelength_derivation < 0.2:
                    self.VISspec.stabilize(False)
                elif wavelength_derivation > 0.2:
                    if wavelength_deviaton < 0.2:
                        pass
                    else:
                        if abs(modelposition-actualposition) < 0.05:
                            if self.direction == 1:
                                self.datalist = [[], [], [], []]
                                self.pbPlusCrystalPos.click()
                                self.CrystalMoving = True
                                self.optimizeTimer2.start(1000)
                            elif self.direction == -1:
                                self.datalist = [[], [], [], []]
                                self.pbMinusCrystalPos.click()
                                self.CrystalMoving = True
                                self.optimizeTimer2.start(1000)
                        else:
                            if self.direction == -1:
                                self.direction = 1
                                self.datalist = [[], [], [], []]
                                self.pbPlusCrystalPos.click()
                                self.CrystalMoving = True
                                self.optimizeTimer2.start(1000)
                            elif self.direction == 1:
                                self.direction = -1
                                self.datalist = [[], [], [], []]
                                self.pbPlusCrystalPos.click()
                                self.CrystalMoving = True
                                self.optimizeTimer2.start(1000)
            elif not self.VISspec.PIDcheck:
                log.info("Checking Signal")
                pdvoltage = np.mean(self.datalist[0])
                bandwidth = np.mean(self.datalist[2])
                log.info(f"PDVoltage: {pdvoltage}, Bandwidth: {bandwidth}, Wavelength Deviation: {wavelength_deviaton}")
                if wavelength_deviaton < 0.1:
                    if wavelength_derivation < 0.2:
                        if bandwidth < 2000 and pdvoltage > 0.002:
                            self.sbLambda.setValue(wavelength)
                            self.optimizeTimer2.stop()
                            self.alreadyMovedTemp = True
                            self.optimizeTimer.start(1000)
                        else:
                            self.optimizeTimer2.stop()
                            log.info("Please move OPO manually.")
                    elif wavelength_derivation > 0.2 and self.VISspec.signalnoiseratio > 25:
                        self.VISspec.stabilize(True)
                    elif self.VISspec.signalnoiseratio < 25:
                        log.info("Signal Quality too bad for stabilization")
                elif wavelength_deviaton > 0.1:
                    # Move OPO
                    if pdvoltage > 0.002 and abs(modelposition-actualposition)< 0.05:
                        if self.direction == 1:
                            self.datalist = [[], [], [], []]
                            self.pbPlusCrystalPos.click()
                            self.CrystalMoving = True
                            self.optimizeTimer2.start(1000)
                        elif self.direction == -1:
                            self.datalist = [[], [], [], []]
                            self.pbMinusCrystalPos.click()
                            self.CrystalMoving = True
                            self.optimizeTimer2.start(1000)
                    elif abs(modelposition-actualposition)> 0.05:
                        if self.direction == -1:
                            self.direction = 1
                            self.datalist = [[], [], [], []]
                            self.pbPlusCrystalPos.click()
                            self.CrystalMoving = True
                            self.optimizeTimer2.start(1000)
                        elif self.direction == 1:
                            self.direction = -1
                            self.datalist = [[], [], [], []]
                            self.pbPlusCrystalPos.click()
                            self.CrystalMoving = True
                            self.optimizeTimer2.start(1000)
                    elif pdvoltage < 0.002:
                        self.optimizeTimer2.stop()
                        log.info("No signal was found: Please adjust")

    @pyqtSlot(dict)
    def readData(self, data: dict):
        # Read values from OPO Controller unit
        # log.info(f'Data dictionary received: {data}')
        try:
            self.lbHumidity.setText(str(data['OPO_hum']) + "%")
        except KeyError:
            pass
        try:
            self.lbSHGTemp.setText(str(data['OPO_SHGtemp']))
            if abs(self.sbSHGTemp.value() - float(self.lbSHGTemp.text())) <= 0.25:
                self.pbTempStat.setStyleSheet('background-color: green')
            else:
                self.pbTempStat.setStyleSheet('background-color: red')
        except KeyError:
            pass
        try:
            self.lbIRPower.setText(str(data['OPO_IR']))
        except KeyError:
            pass
        try:
            self.lbVISPower.setText(str(data['OPO_VIS']))
        except KeyError:
            pass

    def readMotors(self):
        settings = QtCore.QSettings()
        config = settings.value('resonator', type=dict)
        motor = config['motorNumber']
        steps = self.motorCard1.motors[motor].actual_position
        pos = motors.stepsToUnits(steps, config)
        self.lbActualCavPos.setText(str(round(pos, 5)) + " mm")
        self.data['OPO_cavPosition'] = pos

        config = settings.value('SHG', type=dict)
        motor = config['motorNumber']
        steps = self.motorCard1.motors[motor].actual_position
        pos = motors.stepsToUnits(steps, config)
        self.lbActualCrystalPos.setText(str(round(pos, 5)) + " mm")
        self.data['OPO_crystalPosition'] = pos
        self.publisher.send_legacy(self.data)

    @pyqtSlot()
    def openAutocorrelator(self):
        try:
            self.autocorrelator.open()
        except AttributeError:
            self.autocorrelator = ThreeChanAuto(self)
            self.autocorrelator.open()

    @pyqtSlot()
    def openVISspec(self):
        self.VISspec = VISspec(self)
        self.VISspec.open()

    class CommanderSignals(QtCore.QObject):
        """Signals for the Laser Commander."""
        # General signals.
        start = QtCore.pyqtSignal()
        close = QtCore.pyqtSignal()

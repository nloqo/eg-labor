"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Moneke
"""

# Standard packages.
import logging
import sys

# 3rd party packages.
from PyQt6 import QtWidgets

from APEOPO_main import APEOPO

log = logging.getLogger("APEOPO")
sh = logging.StreamHandler()
log.addHandler(sh)

if __name__ == '__main__':  # if this is the started script file
    """Start the main window if this is the called script file."""
    if "-v" in sys.argv:  # Verbose log.
        # log.setLevel(logging.DEBUG)
        logging.getLogger().setLevel(logging.DEBUG)
        logging.getLogger("data.Threading").addHandler(sh)
    app = QtWidgets.QApplication(sys.argv)  # create an application
    mainwindow = APEOPO(name="APEOPO")  # start the first widget, the main window
    app.exec()  # start the application with its Event loop

"""
Main file of the BaseProgram.

Adjust this file according to your needs.

created on 23.11.2020 by Benedikt Burger
"""

# Standard packages.
import logging

# 3rd party packages.
from PyQt6 import QtCore, QtWidgets, uic
from PyQt6.QtCore import pyqtSlot
import datetime
import json
import numpy as np
from scipy.optimize import curve_fit
from simple_pid import PID
# import nidaqmx

from pytrinamic.connections import ConnectionManager
from pytrinamic.modules import TMCM6110
from pyleco.utils.data_publisher import DataPublisher
import nidaqmx as ni
from devices import Thorlabs_LC100 as LC100
from devices import motors, arduino

# Local packages.
from data.VISspec.LC100settings import LC100Settings

log = logging.getLogger("APEOPO")


class VISspec(QtWidgets.QDialog):
    """
    Control program for the VIS spectrometer with stabilization and automatization of the OPO
    output wavelength.

    :param name: Name of this program.
    :param host: Host name of the Coordinator.
    """

    pbConnectMotor: QtWidgets.QPushButton
    pbConnectLC100: QtWidgets.QPushButton
    pbLC100Settings: QtWidgets.QPushButton
    pbResetLC100: QtWidgets.QPushButton
    pbStartScan: QtWidgets.QPushButton
    pbCalibration: QtWidgets.QPushButton
    pbMoveTo: QtWidgets.QPushButton
    pbStopMotor: QtWidgets.QPushButton
    pbSave: QtWidgets.QPushButton
    pbHome: QtWidgets.QPushButton
    pbStabilize: QtWidgets.QPushButton
    pbSetPiezoVoltage: QtWidgets.QPushButton
    lbBandwidth: QtWidgets.QLabel
    lbWavelength: QtWidgets.QLabel
    lbMotorPosition: QtWidgets.QLabel
    lbPiezoVoltage: QtWidgets.QLabel
    sbMotorPosition: QtWidgets.QDoubleSpinBox
    sbPiezoVoltage: QtWidgets.QDoubleSpinBox
    LC100SettingsDialog: QtWidgets.QDialog
    publisher: DataPublisher

    def __init__(self, parent, *args, **kwargs) -> None:
        # Use initialization of parent class QMainWindow.
        super().__init__(*args, **kwargs)
        self.parent = parent

        # Load the user interface file and show it.
        uic.loadUi("data/VISspec/VISspec.ui", self)
        self.show()

        self.configurePlot()

        self.publisher = self.parent.publisher
        self.parent.communicator.subscribe("Current")
        self.parent.listener.signals.dataReady.connect(self.readData)

        settings = QtCore.QSettings()

        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.checkForData)

        self.piezoTimer = QtCore.QTimer()
        self.piezoTimer.timeout.connect(self.checkForSignal)

        self.pbConnectLC100.clicked.connect(self.connectLC100)
        self.pbLC100Settings.clicked.connect(self.openLC100Settings)
        self.pbResetLC100.clicked.connect(self.resetLC100)
        self.pbStartScan.clicked.connect(self.startScan)
        self.pbConnectMotor.clicked.connect(self.connectMotor)
        self.pbCalibration.clicked.connect(self.openCalibration)
        self.pbMoveTo.clicked.connect(self.checkMotorStart)
        self.pbStopMotor.clicked.connect(self.stopMotor)
        self.pbSave.clicked.connect(self.saveSpectrum)
        self.pbHome.clicked.connect(self.home)
        self.pbStabilize.clicked.connect(self.stabilize)
        self.pbSetPiezoVoltage.clicked.connect(self.setPiezoVoltage)
        self.PIDcheck = False
        self.pbConnectMotor.setChecked(True)
        self.connectMotor(True)

        self.pbConnectLC100.setChecked(True)
        self.connectLC100(True)
        self.arduinoBool = True
        self.sbPiezoVoltage.setValue(settings.value("LastPiezoVoltage", type=float))
        self.pbSetPiezoVoltage.click()
        self.sbMotorPosition.setValue(settings.value("LastSpecPosition", type=float))

        log.info("VISspec initialized.")

    def closeEvent(self, event: QtCore.QEvent) -> None:
        settings = QtCore.QSettings()
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.startScan(False)
        self.parent.communicator.unsubscribe("Current")
        try:
            error = LC100.close(self.device)
            del self.device
            log.info(f"LC100-close: {error}")
        except Exception:
            log.warning('Could not close connection to LC100.')
        event.accept()

        try:
            settings.setValue("LastPiezoVoltage", float(self.lbPiezoVoltage.text()))
            settings.setValue("LastSpecPosition", float(self.sbMotorPosition.value()))
            log.info("Last VisSpec Values set.")
        except Exception:
            log.warning("Could not set last VisSpec Values")
        self.pbStabilize.setChecked(False)

    @pyqtSlot(dict)
    def readData(self, data: dict):
        try:
            self.pdvoltage = data["Current"]
        except KeyError:
            pass

    @pyqtSlot()
    def home(self):
        log.info("Move to home position.")
        Calibration.sbMotorPosition.setValue(0)
        Calibration.moveMotor()

    def configurePlot(self):
        self.spectrum = self.plot.plot([])
        self.spectrumFit = self.plot.plot([], pen="r")
        self.plot.setLabel("bottom", "Wavelength in nm")
        self.plot.setLabel("left", "Signal in arb. units")

    @pyqtSlot(bool)
    def connectLC100(self, checked: bool) -> None:
        settings = QtCore.QSettings()
        if checked:
            deviceName = settings.value("deviceName", type=str)
            log.info(f'Try to connect to {deviceName}')
            self.device, error = LC100.init(deviceName)
            log.info(f"LC100-init: {error}")
            if LC100.getDeviceStatus(self.device) == "unknown status":
                self.pbConnectLC100.setChecked(False)
                log.error("Could not connect to LC100. Check device name and USB connection.")
        else:
            try:
                error = LC100.close(self.device)
                del self.device
            except AttributeError:
                log.info("LC100-close: LC100 already disconnected.")

    @pyqtSlot(bool)
    def connectMotor(self, checked: bool) -> None:
        if checked:
            try:
                self.arduino = arduino.Arduino("COM14")
            except Exception as exc:
                log.exception(f"Could not connect Arduino with error code: {exc}")
            COM = motors.getPort("VisSpec")
            try:
                self.connectionManager = ConnectionManager(f"--port COM{COM}")
                self.motorCard = TMCM6110(self.connectionManager.connect())
                log.info("Motorcard successfully connected.")
            except Exception as exc:
                log.exception("Connection failed.", exc)
            else:
                self.configureMotor("gratingMotor")
        else:
            try:
                self.connectionManager.disconnect()
                self.readoutTimer.stop()
            except AttributeError:
                pass  # Not existent
            except Exception as exc:
                log.exception(exc)
            try:
                del self.motorCard
            except AttributeError:
                pass

    @pyqtSlot(str, int)
    def configureMotor(self, motorName):
        """Configure the motor `motorname`."""
        # Load the config for the motor and configure the motor.
        settings = QtCore.QSettings()
        self.config = settings.value(motorName, type=dict)
        self.motor = self.config['motorNumber']
        try:
            motors.configureMotor(self.motorCard, self.config)
        except KeyError:
            log.exception("Motor not defined.")
        except Exception as exc:
            log.exception("Error configuring motor.", exc)

    @pyqtSlot()
    def openLC100Settings(self):
        settings = QtCore.QSettings()
        if self.pbStartScan.isChecked():
            error = LC100.setOperatingMode('idle', self.device)
            log.info(f'LC100-setOperatingMode: {error}')
            self.resetLC100()
            self.pbStartScan.setChecked(False)
        # Integration Time
        intTime, error = LC100.getIntTime('act', self.device)
        settings.setValue('actIntTime', str(intTime))
        log.info(f'LC100-getIntTime: {error}')
        # Operating Mode
        operatingMode, error = LC100.getOperatingMode(self.device)
        settings.setValue('actOperatingMode', operatingMode)
        log.info(f'LC100-getOperatingMode: {error}')
        # Trigger Delay
        triggerDelay, error = LC100.getTriggerDelay('set', self.device)
        settings.setValue('actTriggerDelay', str(triggerDelay))
        log.info(f'LC100-getTriggerDelay: {error}')
        # Evaluation Box Values
        x, y, error = LC100.getEvalBox(settings.value('evalBoxNumber', type=int), self.device)
        settings.setValue('actX1', str(x[0]))
        settings.setValue('actX2', str(x[1]))
        settings.setValue('actY1', str(y[0]))
        settings.setValue('actY2', str(y[1]))
        log.info(f'LC100-getEvalBox: {error}')
        # Device Information
        info, error = LC100.identificationQuery(self.device)
        settings.setValue('manufacturer', info['manufacturerName'])
        settings.setValue('deviceNameLC100', info['deviceName'])
        settings.setValue('serialNumber', info['serialNumber'])
        log.info(f'LC100-identificationQuery: {error}')
        info, error = LC100.sensorTypeQuery(self.device)
        settings.setValue('numberOfPixels', str(info['numberOfPixels']))
        settings.setValue('description', info['description'])
        log.info(f'LC100-sensorTypeQuery: {error}')
        driverRevision, firmwareRevision, error = LC100.revisionQuery(self.device)
        settings.setValue('driverRevision', driverRevision)
        settings.setValue('firmwareRevision', firmwareRevision)
        log.info(f'LC100-revisionQuery: {error}')
        # 
        self.LC100SettingsDialog = LC100Settings()
        self.LC100SettingsDialog.signals.LC100configured.connect(self.configureLC100)
        self.LC100SettingsDialog.open()

    @pyqtSlot()
    def configureLC100(self):
        """Write values to camera LC100"""
        log.info('Configure LC100.')
        settings = QtCore.QSettings()
        # Integration Time
        error = LC100.setIntTime(settings.value('IntTime', type=float), self.device)
        log.info(f'LC100-setIntTime: {error}')
        # Operating Mode
        error = LC100.setOperatingMode(settings.value('actOperatingMode', type=str), self.device)
        log.info(f'LC100-setOperatingMode: {error}')
        # Trigger Delay
        error = LC100.setTriggerDelay(settings.value('triggerDelay', type=float), self.device)
        log.info(f'LC100-setTriggerDelay: {error}')
        # Evaluation Box Values
        error = LC100.setEvalBox(settings.value('x1', type=int), settings.value('x2', type=int),
                                 settings.value('y1', type=float), settings.value('y2', type=float),
                                 self.device,
                                 settings.value('evalBoxNumber', type=int))
        log.info(f'LC100-setEvalBox: {error}')

    @pyqtSlot()
    def resetLC100(self):
        try:
            error = LC100.reset(self.device)
            log.info(f'LC100-reset: {error}')
        except AttributeError:
            log.error("Could not reset LC100 since the camera is not connected.")

    @pyqtSlot(bool)
    def startScan(self, checked: bool):
        settings = QtCore.QSettings()
        if checked:
            if not self.pbConnectLC100.isChecked():
                self.connectLC100(True)
            try:
                error = LC100.setIntTime(settings.value('intTime', type=float), self.device)
                log.info(f'LC100-setIntTime: {error}')
                error = LC100.setTriggerDelay(settings.value('triggerDelay', type=float), self.device)
                log.info(f'LC100-setTriggerDelay: {error}')
                error = LC100.setEvalBox(settings.value('x1', type=int), settings.value('x2', type=int),
                                         settings.value('y1', type=float), settings.value('y2', type=float),
                                         self.device, settings.value('evalBoxNumber', type=int))
                log.info(f'LC100-setEvalBox: {error}')
                mode = settings.value('operatingMode', type=int)
                if mode == 0:
                    error = LC100.setOperatingMode('idle', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    self.pbStartScan.setChecked(False)
                elif mode == 1:
                    error = LC100.setOperatingMode('SW-single', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScan(self.device)
                    log.info(f'LC100-startScan: {error}')
                    self.readout()
                    self.pbStartScan.setChecked(False)
                elif mode == 2:
                    error = LC100.setOperatingMode('SW-loop', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScanCont(self.device)
                    log.info(f'LC100-startScanCont: {error}')
                    self.readoutTimer.start(int(settings.value('intTime', type=float)))
                elif mode == 3:
                    error = LC100.setOperatingMode('HW-single', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScanExtTrg(self.device)
                    log.info(f'LC100-startScanExtTrg: {error}')
                    self.readout()
                    self.pbStartScan.setChecked(False)
                elif mode == 4:
                    error = LC100.setOperatingMode('HW-loop', self.device)
                    log.info(f'LC100-setOperatingMode: {error}')
                    error = LC100.startScanContExtTrg(self.device)
                    log.info(f'LC100-startScanContExtTrg: {error}')
                    self.readoutTimer.start(int(settings.value('intTime', type=float)))
                else:
                    log.info('No valid mode.')
            except Exception as exc:
                log.error(f'Could not start LC100 scan with the following error: {exc}')
                self.pbStartScan.setChecked(False)
        else:
            try:
                error = LC100.setOperatingMode('idle', self.device)
                log.info(f'LC100-setOperatingMode: {error}')
                self.pbStartScan.setChecked(False)
                self.readoutTimer.stop()
            except Exception:
                self.pbStartScan.setChecked(False)
                self.readoutTimer.stop()

    @pyqtSlot()
    def checkForData(self):
        status = LC100.getDeviceStatus(self.device)
        if (status == 'scan is done, waiting for data transfer to PC'
           or status[0] == 'scan is done, waiting for data transfer to PC'):
            self.readout()

    def readout(self) -> None:
        '''
        Readout of data (LC100 data / motor position).
        '''
        # readout motor
        motorPosition = self.motorCard._MotorTypeA(self.motorCard, self.motor).actual_position
        self.lbMotorPosition.setText(str(round((self.stepsToWavelength(motorPosition)[1023]
                                                + self.stepsToWavelength(motorPosition)[1024]
                                                ) / 2, 2)))
        # Readout PD
        '''
        try:
            
            with ni.Task() as task:
                task.ai_channels.add_ai_voltage_chan("myDAQ1/ai0")
                self.pdvoltage = task.read()
                self.sendData({'PDVoltage': self.pdvoltage})
            
            resolution = 2**10
            if self.arduinoBool:
                self.pdvoltage = float(self.arduino.ask("r").split("\t")[0]) / resolution * 5
                self.sendData({'PDVoltage': self.pdvoltage})
                self.arduinoBool = False
            elif not self.arduinoBool:
                self.biasVoltage = float(self.arduino.ask("r").split("\t")[1]) / resolution * 5
                self.sendData({'BiasVoltage': self.biasVoltage})
                self.arduinoBool = True
        
        except Exception as exc:
            log.exception(f"Readout of MyDAQ / Arduino failed with the following error: {exc}")
        '''
        # readout LC100
        self.xrange = self.stepsToWavelength(motorPosition)
        self.data, error = LC100.getScanData(self.device)
        signals = self.dataSignals()
        signals.dataReadySignal.emit()
        self.spectrum.setData(self.xrange, self.data)
        self.evaluateData(self.xrange, self.data)
        self.frqbandwidth = self.bandwidth * 299792458 / self.wavelength**2
        self.sendData({'Frequency Bandwidth': self.frqbandwidth})
        rescaledpower = self.pdvoltage/0.06 * self.parent.sbScalingPower.value()
        rescaledbandwidth = 400/self.frqbandwidth * self.parent.sbScalingBandwidth.value()
        rescaleddisturbing = 0.05/self.disturbingsignal * self.parent.sbScalingDisturbingSignal.value()
        self.quality = (rescaledbandwidth + rescaleddisturbing + rescaledpower - abs(self.wavelength - self.sbMotorPosition.value()))
        # log.info(f"Scaled Signal: {rescaledpower}, Scaled Frqbandwidth: {rescaledbandwidth}, Scaled DisSignal: {rescaleddisturbing}")
        self.sendData({'Quality': self.quality})

        # log.info(f"Bandwidth in GHz: {self.frqbandwidth}")
        # log.info(f"Quality: {self.quality}")
        try:
            if len(self.parent.datalist[0]) < 50:
                self.parent.datalist[0].append(self.pdvoltage)
                self.parent.datalist[1].append(self.wavelength)
                self.parent.datalist[2].append(self.frqbandwidth)
                self.parent.datalist[3].append(self.quality)
            else:
                self.parent.datalist[0].append(self.pdvoltage)
                self.parent.datalist[0].pop(0)
                self.parent.datalist[1].append(self.wavelength)
                self.parent.datalist[1].pop(0)
                self.parent.datalist[2].append(self.frqbandwidth)
                self.parent.datalist[2].pop(0)
                self.parent.datalist[3].append(self.quality)
                self.parent.datalist[3].pop(0)
        except AttributeError:
            pass
        if self.PIDcheck:
            output = self.pid(self.wavelength)
            p, i, d = self.pid.components
            # log.info(f"P-component: {p}, I-component: {i}, D-component:{d}")
            # log.info(f"New PID-output: {output} V")
            # log.info(f"Error: {self.pid._last_error}")
            self.lbPiezoVoltage.setText(str(round(output, 2)))
            try:
                with ni.Task() as task:
                    task.ao_channels.add_ao_voltage_chan("myDAQ1/ao0")
                    task.write(round(output, 5))
            except Exception as exc:
                log.error(f"Stabilization failed with the following error: {exc}")
                self.pbStabilize.setChecked(False)
                self.stabilize(False)

    def stepsToWavelength(self, motorPosition: int):
        microstepresolution = self.motorCard._MotorTypeA(self.motorCard, self.motor).drive_settings.microstep_resolution
        steps = 0.5**microstepresolution * motorPosition
        pixel = (np.arange(2048)-1023.5) * 14e-6
        x0 = 11506.530748739355  # motor position for theta=0° in steps
        theta = np.radians(0.6 / 60)*(steps - x0)
        L = 500e-3  # focal length in m
        delta = np.radians(22.4)
        G = 417.3497295611672  # inverse grating constant in nm
        m = 1  # diffraction order
        wavelength = G / m * (np.sin(theta + delta / 2)
                              + np.sin(theta - (delta / 2) + np.arctan(pixel / L)))
        return wavelength

    def wavelengthToSteps(self, wavelength: float):
        delta = np.radians(22.44)
        m = 1  # diffraction order
        G = 417.3497295611672e-9  # inverse grating constant in nm
        x0 = 11506.530748739355  # motor position for theta=0° in steps
        theta = np.arcsin(m * wavelength * 1e-9 / (2 * G * np.cos(delta / 2)))
        steps = x0 + theta/np.radians(0.6/60)
        microstepresolution = self.motorCard._MotorTypeA(self.motorCard, self.motor).drive_settings.microstep_resolution
        microsteps = steps * 2**microstepresolution
        return microsteps

    def evaluateData(self, x, data: list[int]) -> None:
        """
        Uses the spectrometer calibration to get the x-array from the motorposition with the
        wavelength in nm of each pixel. The data is plotted and a Gaussian fit is used to get the
        wavelength position and the bandwidth. These values get sent out to use in the DataLogger or
        the APEOPO program for stabilization and automatization.
        """
        def Gauss(x, a, b, c, x0):
            return a * np.exp(-(x - x0)**2 / (2 * b**2)) + c
        fitParameter, fitCovariance = curve_fit(
            Gauss, x, data, p0=[max(data) - min(data), 0.5, min(data), x[data.index(max(data))]]
            )
        self.spectrumFit.setData(x, Gauss(x, *fitParameter))
        self.wavelength = fitParameter[3]
        self.lbWavelength.setText(str(round(self.wavelength, 2)))
        self.bandwidth = 2 * np.sqrt(2 * np.log(2)) * fitParameter[1]
        self.lbBandwidth.setText(str(round(self.bandwidth, 2)))
        a = max(data)-min(data)
        if data.index(max(data)) < 1024:
            noise = np.std(data[1536:2048])
        elif data.index(max(data)) > 1023:
            noise = np.std(data[0:512])
        self.signalnoiseratio = a/noise
        undesiredsignal = abs(data - Gauss(x,fitParameter[0],fitParameter[1],fitParameter[2],fitParameter[3]))
        self.disturbingsignal = np.sum(undesiredsignal)/np.sum(Gauss(x,fitParameter[0],fitParameter[1],fitParameter[2],fitParameter[3]))
        self.sendData({'VISwavelength': self.wavelength,
                       'VISbandwidth': self.bandwidth,
                       'DisturbingSignal': self.disturbingsignal})

    @pyqtSlot()
    def checkMotorStart(self):
        print(float(self.lbMotorPosition.text()))
        print(self.sbMotorPosition.value())
        if self.sbMotorPosition.value() >= float(self.lbMotorPosition.text()):
            position = int(self.wavelengthToSteps(self.sbMotorPosition.value()))
            print(position)
            self.motorCard.move_to(self.motor, position, self.config['positioningSpeed'])
            self.motorTimer.stop()
        else:
            self.motorTimer = QtCore.QTimer()
            self.motorTimer.timeout.connect(self.moveNegative)
            position = int(self.wavelengthToSteps(self.sbMotorPosition.value() - 5))
            self.motorCard.move_to(self.motor, position, self.config['positioningSpeed'])
            self.motorTimer.start(100)

    def moveNegative(self):
        if self.motorCard._MotorTypeA(self.motorCard, self.motor).get_position_reached():
            self.checkMotorStart()

    def stopMotor(self):
        self.motorCard.stop(self.motor)

    def sendData(self, data) -> None:
        """Example how to handle sending data."""
        try:
            self.publisher.send_legacy(data)
        except Exception as exc:
            log.exception("Publisher error.", exc)

    @pyqtSlot()
    def saveSpectrum(self):
        path = 'D:\\Measurement Data\\VisSpectrometer\\'
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        data = {'motorPosition': float(self.lbMotorPosition.text()), 'wavelength': self.wavelength,
                'bandwidth': self.bandwidth, 'lambdarange': list(self.xrange),
                'signal': list(self.data)}
        with open(f'{path}{file_name}.json', 'w') as file:
            json.dump(data, file)
        clipboard = QtWidgets.QApplication.instance().clipboard()
        clipboard.setText(file_name)
        log.info('Data saved.')

    @pyqtSlot()
    def setPiezoVoltage(self):
        try:
            with ni.Task() as task:
                task.ao_channels.add_ao_voltage_chan("myDAQ1/ao0")
                task.write(self.sbPiezoVoltage.value())
                self.lbPiezoVoltage.setText(str(round(self.sbPiezoVoltage.value(),2)))
            log.info(f"New Piezo Voltage: {self.sbPiezoVoltage.value()} V")
        except Exception as exc:
            log.error(f"Set Piezo Voltage failed with the following error: {exc}")

    def PID_rescale(self):
        scaling_factor = (0.00304834 - 0.00188716 *
                          (-4 * 1.54e-07 * (self.wavelength-582.7)**3
                           + 3 * 3.29e-05 * (self.wavelength-582.7)**2
                           + 2 * 0.00908 * (self.wavelength-582.7) - 1.55)
                          ) / 0.0072
        return scaling_factor

    @pyqtSlot(bool)
    def stabilize(self, checked: bool):
        settings = QtCore.QSettings()
        if checked:
            if self.signalnoiseratio > 10:
                self.pid = PID(auto_mode=False)
                self.pid.output_limits = (0, 5)
                self.pid.Kd = settings.value("D", defaultValue=0, type=float) / 5e3 * self.PID_rescale()
                self.pid.Kp = settings.value("P", defaultValue=0, type=float) / 5e3 * self.PID_rescale()
                self.pid.Ki = settings.value("I", defaultValue=0, type=float) / 5e3 * self.PID_rescale()
                log.info(f"P: {self.pid.Kp}, I: {self.pid.Ki}, D: {self.pid.Kd}")
                self.pid.setpoint = self.sbMotorPosition.value()
                log.info(f"Setpoint: {self.sbMotorPosition.value()} nm")
                log.info(f"Components: {self.pid.components}")
                log.info(f"Last Output: {self.lbPiezoVoltage.text()}")
                self.pid.set_auto_mode(enabled=True, last_output=float(self.lbPiezoVoltage.text()))
                self.pid.sample_time = int(settings.value('intTime', type=float))
                log.info(f"Components: {self.pid.components}")
                self.PIDcheck = True
            else:
                # Scan Piezo regime in order to find a peak
                self.sbPiezoVoltage.setValue(5)
                self.piezoTimer.start(1000)
                self.pbStabilize.setChecked(False)
                self.pid.set_auto_mode(enabled=False, last_output=float(self.lbPiezoVoltage.text()[:-2]))
                self.PIDcheck = False
        else:
            self.pid.set_auto_mode(enabled=False)
            self.PIDcheck = False

    def checkForSignal(self):
        if self.signalnoiseratio > 20:
            self.piezoTimer.stop()
            self.pbStabilize.click()
        elif self.signalnoiseratio < 20 and self.sbPiezoVoltage.value() > 0.1:
            piezoVoltage = self.sbPiezoVoltage.value()
            self.sbPiezoVoltage.setValue(piezoVoltage-0.25)
            self.pbSetPiezoVoltage.click()
        elif self.sbPiezoVoltage.value() < 0.1:
            self.piezoTimer.stop()
            log.info("No signal was found")

    @pyqtSlot()
    def openCalibration(self):
        self.calibration = Calibration(self)
        self.calibration.open()

    class dataSignals(QtCore.QObject):
        dataReadySignal = QtCore.pyqtSignal()


class Calibration(QtWidgets.QDialog):

    lbMotorPosition: QtWidgets.QLabel
    sbMotorPosition: QtWidgets.QSpinBox
    pbMoveTo: QtWidgets.QPushButton
    pbStop: QtWidgets.QPushButton
    pbSave: QtWidgets.QPushButton
    leComment: QtWidgets.QLineEdit

    def __init__(self, parent: VISspec, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent
        self.main_window = parent

        # Load the user interface file and show it.
        uic.loadUi("data/VISspec/calibration.ui", self)
        self.show()

        settings = QtCore.QSettings()
        self.motorNumber = settings.value('gratingMotor')['motorNumber']
        self.motorCard = self.main_window.motorCard
        self.microstepresolution = self.motorCard._MotorTypeA(self.motorCard, self.motorNumber).drive_settings.microstep_resolution
        self.configPlot()
        self.readoutTimer = QtCore.QTimer()
        self.readoutTimer.timeout.connect(self.readout)
        self.motorTimer = QtCore.QTimer()
        self.motorTimer.timeout.connect(self.waitMotor)

        self.pbMoveTo.clicked.connect(self.moveMotor)
        self.pbStop.clicked.connect(self.stopMotor)
        self.pbSave.clicked.connect(self.saveData)

        self.readoutTimer.start(settings.value('interval'))

    def closeEvent(self):
        self.readoutTimer.stop()
        self.stopMotor()
        self.motorTimer.stop()

    def configPlot(self):
        self.spectrum = self.plot.plot([])
        self.plot.setLabel("bottom", "Motor position in steps")
        self.plot.setLabel("left", "Signal in arb. units")

    @pyqtSlot()
    def readout(self):
        self.position = self.motorCard._MotorTypeA(self.motorCard, self.motorNumber).actual_position
        self.lbMotorPosition.setText(str(self.position))
        self.data = self.main_window.data
        self.spectrum.setData(self.data)

    @pyqtSlot()
    def moveMotor(self):
        settings = QtCore.QSettings()
        config = settings.value('gratingMotor')
        velocity = config['positioningSpeed']
        if self.sbMotorPosition.value() > self.position:
            self.main_window.motorCard.move_to(self.motorNumber, self.sbMotorPosition.value(),
                                               velocity)
            log.info('Move motor positive')
        else:
            self.main_window.motorCard.move_to(self.motorNumber, self.sbMotorPosition.value() -
                                               int(2**self.microstepresolution * 200), velocity)
            log.info('Move motor negative.')
            self.motorTimer.start(100)

    @pyqtSlot()
    def waitMotor(self):
        log.info('Wait motor.')
        if self.motorCard._MotorTypeA(self.motorCard, self.motorNumber).get_position_reached():
            self.motorTimer.stop()
            self.moveMotor()

    @pyqtSlot()
    def stopMotor(self):
        self.main_window.motorCard.stop(self.motorNumber)

    @pyqtSlot()
    def saveData(self):
        settings = QtCore.QSettings()
        config = settings.value('gratingMotor')
        path = 'D:\\Measurement Data\\VisSpectrometer\\Calibration\\'
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        header = config
        data = {'motorPosition': self.position, 'signal': self.data}
        comment = self.leComment.text()
        with open(f'{path}{file_name}.json', 'w') as file:
            json.dump((header, data, comment), file)
        log.info('Data saved.')

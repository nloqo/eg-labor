
import math

import pyqtgraph as pg
from qtpy import QtCore, QtGui, QtWidgets, uic
import datetime
import json
from qtpy.QtCore import Slot as pyqtSlot, Signal as pyqtSignal  # type: ignore


class ChannelSettings(QtWidgets.QDialog):
    """Window for the settings of a single channel, including a plot."""

    plot: pg.PlotItem

    bbRange: QtWidgets.QComboBox
    bbCoupling: QtWidgets.QComboBox
    cbInverted: QtWidgets.QCheckBox
    sbOffset: QtWidgets.QDoubleSpinBox

    gbZeroing: QtWidgets.QGroupBox
    sbZeroStart: QtWidgets.QSpinBox
    sbZeroStop: QtWidgets.QSpinBox
    leVariableNameZero: QtWidgets.QLineEdit
    bbFunctionZero: QtWidgets.QComboBox

    sbSignalStart: QtWidgets.QSpinBox
    sbSignalStop: QtWidgets.QSpinBox
    pbScale: QtWidgets.QPushButton
    pbSaveArray: QtWidgets.QPushButton
    bbFunction: QtWidgets.QComboBox
    leVariableName: QtWidgets.QLineEdit

    lbOverrange: QtWidgets.QLabel

    def __init__(self, channel: str, main, **kwargs):
        """Initialize the window for `channel`, with the `parent`."""
        super().__init__(**kwargs)
        self.channel = channel
        self.main = main
        self.signals = self.SettingsSignals()

        # UI
        uic.load_ui.loadUi("data/ChannelSettings.ui", self)
        self.setWindowTitle(f"Channel {channel} settings")
        # Red circle as overrange indicator.
        size = min(self.lbOverrange.height(), self.lbOverrange.width())
        self.lbOverrange.setFixedHeight(20)
        self.lbOverrange.setFixedWidth(self.lbOverrange.height())
        picture = QtGui.QPixmap("data/RedCircle.png")
        picture2 = picture.scaled(size, size,
                                  QtCore.Qt.AspectRatioMode.KeepAspectRatio,
                                  QtCore.Qt.TransformationMode.SmoothTransformation)
        self.lbOverrange.setPixmap(picture2)
        # self.lbOverrange.setGeometry(QtCore.QRect(312, 454, 40, 40))
        # The settings object.
        self.settings = QtCore.QSettings()
        self.samples = self.settings.value('samples', type=int)
        self.settings.beginGroup(f"ch{self.channel}")

        self.bbRange.addItems(['20 mV', '50 mV', '100 mV', '200 mV', '500 mV',
                               '1 V', '2 V', '5 V', '10 V', '20 V'])
        self.bbCoupling.addItems(["DC50", "DC", "AC"])
        self.bbFunction.addItems(["average", "integrate", "sum",
                                  "first", "maximum", "minimum"])
        self.bbFunctionZero.addItems(["average", "integrate", "sum",
                                  "first", "maximum", "minimum"])

        self.bbSets = (
            (self.bbRange, 'range', "200 mV", str),
            (self.bbCoupling, 'coupling', "DC", str),
            (self.bbFunction, 'function', "average", str),
            (self.bbFunctionZero, 'functionZero', "average", str)
        )
        self.sets = (
            # (self.leVariableName, "variableName", "", str),
            # (self.cbInverted, "inverted", False, bool),
            (self.sbOffset, "offset", 0.0, float),
            (self.sbSignalStart, 'signalStart', 1000, int),
            (self.sbSignalStop, 'SignalStop', 2000, int),
            # (self.gbZeroing, "zeroing", False, bool),
            (self.sbZeroStart, 'zeroStart', 0, int),
            (self.sbZeroStop, 'ZeroStop', 1000, int),
            # (self.leVariableNameZero, "variableNameZero", "", str),
        )

        # Initialize
        self.setupPlot()
        self.setLimits(self.samples)
        self.readValues()
        if self.gbZeroing.isChecked():
            self.createZeroLines()

        # Connect to slots
        self.leVariableName.editingFinished.connect(self.setVariableName)
        self.bbCoupling.currentTextChanged.connect(self.setCoupling)
        self.bbRange.currentTextChanged.connect(self.setVRange)
        self.sbOffset.valueChanged.connect(self.setOffset)
        self.cbInverted.stateChanged.connect(self.setInverted)
        #   Evaluation
        self.bbFunction.currentTextChanged.connect(self.setFunction)
        self.bbFunctionZero.currentTextChanged.connect(self.setFunctionZero)
        self.sbSignalStart.valueChanged.connect(self.setSignalStart)
        self.sbSignalStop.valueChanged.connect(self.setSignalStop)
        self.pbScale.clicked.connect(self.scale)
        self.pbSaveArray.clicked.connect(self.saveArray)
        #   Zeroing.
        self.gbZeroing.clicked.connect(self.setZeroing)
        self.sbZeroStart.valueChanged.connect(self.setZeroStart)
        self.sbZeroStop.valueChanged.connect(self.setZeroStop)
        self.leVariableNameZero.editingFinished.connect(self.setVariableNameZero)

    class SettingsSignals(QtCore.QObject):
        """Signals for the settings"""
        channelChanged = pyqtSignal(str)

    def setLimits(self, samples):
        """Set the limits of the spinboxes to the number of `samples`."""
        for widget in (self.sbSignalStart, self.sbSignalStop,
                       self.sbZeroStart, self.sbZeroStop):
            widget.setMaximum(samples)
        try:
            for line in (self.signalStart, self.signalStop,
                         self.zeroStart, self.zeroStop):
                line.setBounds((0, samples))
        except AttributeError:
            pass

    def setupPlot(self):
        """Configure the plot."""
        self.figure = self.plot.plot([])
        self.plot.setLabel('bottom', "sample")
        self.plot.setLabel('left', "voltage")
        # Lines for the signal.
        start = self.settings.value('signalStart', type=int)
        stop = self.settings.value('SignalStop', type=int)
        self.signalStart = self.plot.addLine(x=start, pen='y', movable=True)
        self.signalStop = self.plot.addLine(x=stop, pen='y', movable=True)
        self.signalStart.sigDragged.connect(self.signalStartDragged)
        self.signalStop.sigDragged.connect(self.signalStopDragged)

    def createZeroLines(self):
        """Create lines for zeroing."""
        start = self.settings.value('zeroStart', type=int)
        stop = self.settings.value('ZeroStop', type=int)
        self.zeroStart = self.plot.addLine(x=start, pen='b', movable=True,
                                           bounds=(0, self.samples))
        self.zeroStop = self.plot.addLine(x=stop, pen='b', movable=True,
                                          bounds=(0, self.samples))
        self.zeroStart.sigDragged.connect(self.zeroStartDragged)
        self.zeroStop.sigDragged.connect(self.zeroStopDragged)

    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        for setting in self.bbSets:
            widget, name, value, typ = setting
            widget.setCurrentText(self.settings.value(name, defaultValue=value, type=typ))
        self.leVariableName.setText(self.settings.value("variableName", "name", str))
        self.cbInverted.setChecked(self.settings.value('inverted', type=bool))
        self.gbZeroing.setChecked(self.settings.value('zeroing', type=bool))
        self.leVariableNameZero.setText(self.settings.value("variableNameZero", type=str))

    # Move the lines.
    @pyqtSlot()
    def signalStartDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbSignalStart.setValue(math.ceil(self.signalStart.value()))

    @pyqtSlot()
    def signalStopDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbSignalStop.setValue(math.floor(self.signalStop.value()))

    @pyqtSlot()
    def zeroStartDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbZeroStart.setValue(math.ceil(self.zeroStart.value()))

    @pyqtSlot()
    def zeroStopDragged(self):
        """Transfer the line value to the spinbox."""
        self.sbZeroStop.setValue(math.floor(self.zeroStop.value()))

    # Store the changed values.
    #   General
    @pyqtSlot()
    def setVariableName(self):
        """Set the variable name."""
        self.settings.setValue('variableName', self.leVariableName.text())

    @pyqtSlot(str)
    def setCoupling(self, text):
        self.settings.setValue('coupling', text)
        self.signals.channelChanged.emit(self.channel)

    @pyqtSlot(str)
    def setVRange(self, text):
        """Set the measurement range in V."""
        self.settings.setValue('range', text)
        self.signals.channelChanged.emit(self.channel)

    @pyqtSlot(float)
    def setOffset(self, value):
        self.settings.setValue('offset', value)
        self.signals.channelChanged.emit(self.channel)

    @pyqtSlot(int)
    def setInverted(self, checked):
        """Invert the measured voltages."""
        self.settings.setValue('inverted', bool(checked))
        self.signals.channelChanged.emit(self.channel)

    #   Evaluation
    @pyqtSlot(str)
    def setFunction(self, text):
        """Sets the function that was chosen."""
        self.settings.setValue('function', text)

    @pyqtSlot(str)
    def setFunctionZero(self, text):
        """Sets the function that was chosen."""
        self.settings.setValue('functionZero', text)

    @pyqtSlot(int)
    def setSignalStart(self, value):
        self.settings.setValue("signalStart", value)
        self.signalStart.setValue(value)

    @pyqtSlot(int)
    def setSignalStop(self, value):
        self.settings.setValue("signalStop", value)
        self.signalStop.setValue(value)

    @pyqtSlot(bool)
    def scale(self, checked):
        """Zooms into the function interval of the signal."""
        if checked:
            self.plot.setXRange(self.sbSignalStart.value() - 10,
                                self.sbSignalStop.value() + 10)
        else:
            self.plot.enableAutoRange(x=True)

    #   Zeroing
    @pyqtSlot(bool)
    def setZeroing(self, checked):
        self.settings.setValue('zeroing', checked)
        if checked:
            self.createZeroLines()
        else:
            try:
                self.plot.removeItem(self.zeroStart)
                self.plot.removeItem(self.zeroStop)
            except Exception as exc:
                print(f"Error removing items {exc}")

    @pyqtSlot(int)
    def setZeroStart(self, value):
        self.settings.setValue('zeroStart', value)
        self.zeroStart.setValue(value)

    @pyqtSlot(int)
    def setZeroStop(self, value):
        self.settings.setValue("zeroStop", value)
        self.zeroStop.setValue(value)

    @pyqtSlot()
    def setVariableNameZero(self):
        """Set the variable name."""
        self.settings.setValue('variableNameZero', self.leVariableNameZero.text())

    @pyqtSlot()
    def saveArray(self):
        data = self.main.data[self.channel]
        data = [float(x) for x in data]
        path = "D:\\Measurement Data\\Picoscope\\"
        file_name = datetime.datetime.now().strftime("%Y_%m_%dT%H_%M_%S")
        with open(f"{path}{file_name}.json", "w") as file:
            json.dump(data, file)

    # Show data.
    @pyqtSlot(bool)
    def update(self, overrange):
        """Update the plot with the new data."""
        data = self.main.data[self.channel]
        self.figure.setData(data)
        self.lbOverrange.setVisible(overrange)

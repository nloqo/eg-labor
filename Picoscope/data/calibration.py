# -*- coding: utf-8 -*-
"""
Created on Fri Sep 30 14:44:13 2022

@author: THG-User
"""


def linear(x, a, b):
    return a * x + b


def quadratic(x, a, b, c):
    return a * x**2 + b * x + c


def Calibration(device, data):
    if device == "407A":
        parameters = [-650.596, 0]
        return linear(data, *parameters)
    if device == "CwPartMira":
        parameters = [1/4.12, 0.08/4.12]
        return linear(data, *parameters)
    if device == "PD532Regen":
        parameters = [5.50378683e6, -4.15203985e-01]
        return linear(data, *parameters)
    if device == "PD532Amp":
        parameters = [2.12210382e7, 1.46101429]
        return linear(data, *parameters)
    if device == "PowerAmp":
        parameters = [9.28469370e+06, 9.88231135e-01]
        return linear(data, *parameters)
    if device == "Regen":
        parameters = [1.38080870e+07, -1.05998921e-01]
        return linear(data, *parameters)

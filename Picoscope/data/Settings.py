"""
Module for the settings dialog class.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from typing import Any, Iterable

from picoscope import picobase
from qtpy import QtCore, QtWidgets, uic
from qtpy.QtCore import Slot as pyqtSlot  # type: ignore


class GeneralSettings(QtWidgets.QDialog):
    """Define the settings dialog and its methods."""

    buttonBox = QtWidgets.QDialogButtonBox

    bbSeries: QtWidgets.QComboBox
    leSerialNumber: QtWidgets.QLineEdit

    def __init__(self, picoscopes: Iterable[str], **kwargs):
        """
        Initialize the dialog.

        Parameters
        ----------
        picoscopes
            Names of available picoscope drivers
        """
        # Use initialization of parent class QDialog.
        super().__init__(**kwargs)

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/Settings.ui", self)
        self.show()

        self.bbSeries.addItems(picoscopes)
        # Configure settings.
        self.settings = QtCore.QSettings()
        # Convenience list for widgets with value(), SetValue() methods.
        self.bb_sets: tuple[tuple[QtWidgets.QComboBox, str, Any, Any], ...] = (
            (self.bbSeries, "series", "ps4000a", str),
        )

        self.sets: tuple[tuple[QtWidgets.QSpinBox, str, Any, Any], ...] = ()
        self.readValues()

        # CONNECT BUTTONS.
        # Define RestoreDefaults button and connect it.
        self.pbRestoreDefaults = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        )  # type: ignore
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            widget.setCurrentText(self.settings.value(name, defaultValue=value, type=typ))
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))
        self.leSerialNumber.setText(self.settings.value("serialNumber", "", type=str))

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            widget.setCurrentText(value)
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)
        self.leSerialNumber.setText("")

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.currentText())
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        self.settings.setValue("serialNumber", self.leSerialNumber.displayText())

        super().accept()  # make the normal accept things


class TriggerSettings(QtWidgets.QDialog):
    """Dialog window for the trigger settings."""

    buttonBox = QtWidgets.QDialogButtonBox

    bbResolution: QtWidgets.QComboBox
    bbTriggerChannel: QtWidgets.QComboBox
    bbTriggerRange: QtWidgets.QComboBox
    bbTriggerPoint: QtWidgets.QComboBox

    sbTimebase: QtWidgets.QDoubleSpinBox
    sbSamples: QtWidgets.QSpinBox
    sbPreTriggerSamples: QtWidgets.QSpinBox
    sbTriggerThreshold: QtWidgets.QDoubleSpinBox
    sbTimeout: QtWidgets.QSpinBox

    pbTimebase: QtWidgets.QPushButton

    ps: picobase._PicoscopeBase

    def __init__(self, main, **kwargs):
        """Initialize the dialog with the `parent` object."""
        # Use initialization of parent class QDialog.
        super().__init__(**kwargs)

        self.ps = main.ps

        # Load the user interface file and show it.
        uic.load_ui.loadUi("data/Trigger.ui", self)
        self.show()

        channels = list(self.ps.CHANNELS.keys())
        try:
            channelCount = main.channelCount
        except AttributeError:
            pass
        else:
            if channelCount < (maximum := self.ps.CHANNELS["MaxChannels"]):
                for i in range(channelCount, maximum):
                    channels.remove(chr(65 + i))
        channels.remove("MaxChannels")
        try:
            channels.remove("External")
        except ValueError:
            pass

        self.bbResolution.addItems(self.ps.ADC_RESOLUTIONS.keys())
        # TODO ADC doesn't do anything yet
        self.bbTriggerChannel.addItems(channels)
        self.bbTriggerRange.addItems(
            ("20 mV", "50 mV", "100 mV", "200 mV", "500 mV", "1 V", "2 V", "5 V", "10 V", "20 V")
        )
        self.bbTriggerPoint.addItems(self.ps.THRESHOLD_TYPE.keys())
        # Configure settings.
        self.settings = QtCore.QSettings()
        # Convenience list for widgets with value(), SetValue() methods.
        self.bb_sets = (
            (self.bbResolution, "resolution", "12", str),
            (self.bbTriggerChannel, "triggerChannel", "C", str),
            (self.bbTriggerRange, "triggerRange", "5 V", str),
            (self.bbTriggerPoint, "triggerPoint", "Rising", str),
        )

        self.sets = (
            (self.sbTimebase, "timebase", 12.5, float),
            (self.sbSamples, "samples", 20000, int),
            (self.sbPreTriggerSamples, "preTriggerSamples", 1000, int),
            (self.sbTriggerThreshold, "triggerThreshold", 0.0, float),
            (self.sbTimeout, "timeout", 100, int),
        )

        self.sbSamples.valueChanged.connect(self.setPreTriggerSamplesMax)
        self.readValues()
        self.verifyTimebase()

        # CONNECT BUTTONS.
        self.pbTimebase.clicked.connect(self.verifyTimebase)
        # Define RestoreDefaults button and connect it.
        self.pbRestoreDefaults = self.buttonBox.button(
            QtWidgets.QDialogButtonBox.StandardButton.RestoreDefaults
        )  # type: ignore
        self.pbRestoreDefaults.clicked.connect(self.restoreDefaults)

    @pyqtSlot()
    def verifyTimebase(self):
        """Verify the timebase and set the maximum number of samples."""
        try:
            timebaseN = self.ps.getTimeBaseNum(self.sbTimebase.value() / 1e9)
            timebase, maxSamples = self.ps._lowLevelGetTimebase(timebaseN, 1, 0, 0)
        except IOError as exc:
            if str(exc) == (
                "Error calling _lowLevelGetTimebase: PICO_INVALID_TIMEBASE "
                "(The timebase is not supported or is invalid.)"
            ):
                message = QtWidgets.QMessageBox()
                message.setWindowTitle("Invalid Timebase")
                message.setText(
                    "The timebase chosen is invalid for the current combination "
                    "of timebase and channels active. Choose a higher timebase "
                    "or deactivate channels. You have to start a measurement in "
                    "order to deactivate channels."
                )
                message.setIcon(QtWidgets.QMessageBox.Icon.Information)
                message.exec()
            else:
                print(f"IOError {exc}")
        except Exception as exc:
            print(f"Verify timebase failed with {type(exc).__name__}: {exc}")
        else:
            self.sbTimebase.setValue(timebase * 1e9)
            maxSamples = min(maxSamples, 2147483647)
            self.sbSamples.setMaximum(maxSamples)

    @pyqtSlot()
    def readValues(self):
        """Read the stored values and show them on the user interface."""
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            widget.setCurrentText(self.settings.value(name, defaultValue=value, type=typ))
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(self.settings.value(name, defaultValue=value, type=typ))

    @pyqtSlot()
    def restoreDefaults(self):
        """Restore the user interface to default values."""
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            widget.setCurrentText(value)
        for setting in self.sets:
            widget, name, value, typ = setting
            widget.setValue(value)  # type: ignore

    @pyqtSlot()
    def accept(self):
        """Save the values from the user interface in the settings."""
        # is executed, if pressed on a button with the accept role
        for setting in self.bb_sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.currentText())
        for setting in self.sets:
            widget, name, value, typ = setting
            self.settings.setValue(name, widget.value())
        super().accept()  # make the normal accept things

    @pyqtSlot(int)
    def setPreTriggerSamplesMax(self, samples):
        """Set the maximum pre trigger samples to `samples`."""
        self.sbPreTriggerSamples.setMaximum(samples)

"""
Main file of the Picoscope program.

created on 23.11.2020 by Benedikt Burger
"""

# Standard packages.
import logging
import time
from typing import Any, Optional

# 3rd party
import numpy as np
from picoscope import picobase, ps4000a, ps5000a, ps6000a
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot
from PyQt6.QtWidgets import QMessageBox

from pyleco_extras.gui_utils.base_main_window import (
    LECOBaseMainWindowDesigner,
    start_app,
    Path,
)

# Local packages.
from data import Settings, calibration
from data.channel_settings import ChannelSettings


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())


picoscopes = {
    "ps4000a": ps4000a.PS4000a,
    "ps5000a": ps5000a.PS5000a,
    "ps6000a": ps6000a.PS6000a,
}


class Picoscope(LECOBaseMainWindowDesigner):
    """Control a picoscope oscilloscope and read its data."""

    actionChA: QtGui.QAction
    actionChB: QtGui.QAction
    actionChC: QtGui.QAction
    actionChD: QtGui.QAction
    actionChE: QtGui.QAction
    actionChF: QtGui.QAction
    actionChG: QtGui.QAction
    actionChH: QtGui.QAction
    actionChannel_A: QtGui.QAction
    actionChannel_B: QtGui.QAction
    actionChannel_C: QtGui.QAction
    actionChannel_D: QtGui.QAction
    actionChannel_E: QtGui.QAction
    actionChannel_F: QtGui.QAction
    actionChannel_G: QtGui.QAction
    actionChannel_H: QtGui.QAction

    actionConnect: QtGui.QAction
    actionReconnect: QtGui.QAction
    actionMeasure: QtGui.QAction
    actionInfo: QtGui.QAction
    actionTrigger: QtGui.QAction

    lbDisplay: QtWidgets.QLabel
    lbVariable: QtWidgets.QLabel
    lbOverrange: QtWidgets.QLabel

    ps: picobase._PicoscopeBase

    def __init__(self, name="Picoscope", **kwargs):
        super().__init__(
            name=name,
            ui_file_name="Picoscope",
            ui_file_path=Path(__file__).parent / "data",
            settings_dialog_class=Settings.GeneralSettings,
            **kwargs,
        )

        self.signals = self.PicoscopeSignals()
        self.channelSettings: dict[str, ChannelSettings] = {}

        self.name = name

        # Get settings.
        settings = QtCore.QSettings()

        # Set settings.
        self.setSettings()

        # Connect actions to slots.
        # Connection
        self.actionConnect.triggered.connect(self.connectPico)
        self.actionReconnect.triggered.connect(self.reconnect)
        self.actionInfo.triggered.connect(self.showInfo)
        self.actionTrigger.triggered.connect(self.openTriggerSetup)
        self.actionMeasure.triggered.connect(self.startMeasurement)
        self.actionChA.triggered.connect(self.readoutA)
        self.actionChB.triggered.connect(self.readoutB)
        self.actionChC.triggered.connect(self.readoutC)
        self.actionChD.triggered.connect(self.readoutD)
        self.actionChE.triggered.connect(self.readoutE)
        self.actionChF.triggered.connect(self.readoutF)
        self.actionChG.triggered.connect(self.readoutG)
        self.actionChH.triggered.connect(self.readoutH)
        # ChannelSettings
        self.actionChannel_A.triggered.connect(self.openChannelSettingsA)
        self.actionChannel_B.triggered.connect(self.openChannelSettingsB)
        self.actionChannel_C.triggered.connect(self.openChannelSettingsC)
        self.actionChannel_D.triggered.connect(self.openChannelSettingsD)
        self.actionChannel_E.triggered.connect(self.openChannelSettingsE)
        self.actionChannel_F.triggered.connect(self.openChannelSettingsF)
        self.actionChannel_G.triggered.connect(self.openChannelSettingsG)
        self.actionChannel_H.triggered.connect(self.openChannelSettingsH)
        # Signals
        self.signals.blockReady.connect(self.transferData)
        self.signals.runBlock.connect(self.runBlock)

        # Select the last active channels at startup.
        self.channelsToConfigure = {}
        self.listOfChannelActions = [
            self.actionChA,
            self.actionChB,
            self.actionChC,
            self.actionChD,
            self.actionChE,
            self.actionChF,
            self.actionChG,
            self.actionChH,
        ]
        self.activeChannels: dict[str, bool] = {}
        self.data: dict[str, list | np.ndarray] = {}
        for i in range(8):
            self.data[chr(i + 65)] = []
            if settings.value(f"ch{chr(i+65)}/enabled", type=bool):
                self.listOfChannelActions[i].setChecked(True)
                self.activeChannels[chr(i + 65)] = True

        # For counting the measurement interval
        self.last = time.perf_counter()
        log.info(f"{name} initialized.")

    class PicoscopeSignals(QtCore.QObject):
        """Signals for the Picoscope class."""

        close = QtCore.pyqtSignal()
        blockReady = QtCore.pyqtSignal(int)
        runBlock = QtCore.pyqtSignal()
        channelUpdatedA = QtCore.pyqtSignal(bool)
        channelUpdatedB = QtCore.pyqtSignal(bool)
        channelUpdatedC = QtCore.pyqtSignal(bool)
        channelUpdatedD = QtCore.pyqtSignal(bool)
        channelUpdatedE = QtCore.pyqtSignal(bool)
        channelUpdatedF = QtCore.pyqtSignal(bool)
        channelUpdatedG = QtCore.pyqtSignal(bool)
        channelUpdatedH = QtCore.pyqtSignal(bool)
        samplesChanged = QtCore.pyqtSignal(int)

        def __init__(self):
            """Aggregate signals into a dictionary."""
            super().__init__()
            self.channelUpdated = {
                "A": self.channelUpdatedA,
                "B": self.channelUpdatedB,
                "C": self.channelUpdatedC,
                "D": self.channelUpdatedD,
                "E": self.channelUpdatedE,
                "F": self.channelUpdatedF,
                "G": self.channelUpdatedG,
                "H": self.channelUpdatedH,
            }

    @pyqtSlot()
    def closeEvent(self, event) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing.")
        self.connectPico(False)
        self.stop_listen()
        # accept the close event (reject it, if you want to do something else)
        event.accept()

    @pyqtSlot()
    def openSettings(self) -> None:
        """Open the settings dialogue and apply changed settings."""
        settingsWindow = Settings.GeneralSettings(picoscopes.keys())
        if settingsWindow.exec():
            self.setSettings()

    def setSettings(self) -> None:
        """Set the general settings."""
        settings = QtCore.QSettings()
        serialNumber = settings.value("serialNumber", type=str)
        self.setWindowTitle(f"{self.name} {serialNumber}")
        series = settings.value("series", "ps4000a", type=str)
        if not hasattr(self, "ps") or not isinstance(self.ps, picoscopes[series]):
            try:
                self.ps = picoscopes[series](connect=False)
            except Exception as exc:
                log.exception("Driver loading failed.")
                self.lbDisplay.setText(
                    f"Driver loading failed with {type(exc).__name__}: {exc}."
                )

    @pyqtSlot()
    def openTriggerSetup(self) -> None:
        """Open the trigger setup."""
        triggerWindow = Settings.TriggerSettings(main=self)
        if triggerWindow.exec():
            self.setupTrigger()

    def setupTrigger(self) -> None:
        """Setup the trigger."""
        settings = QtCore.QSettings()
        self.picoscopeToConfigure = True
        self.signals.samplesChanged.emit(settings.value("samples", type=int))

    def configureEverything(self) -> None:
        """Set the flags in order to configure the scope and all channels."""
        self.channelsToConfigure = {}
        for i in range(self.channelCount):
            self.channelsToConfigure[chr(65 + i)] = True
            self.listOfChannelActions[i].setVisible(True)
        for i in range(self.channelCount, 8):
            self.listOfChannelActions[i].setVisible(False)
        self.picoscopeToConfigure = True

    def configurePicoscope(self) -> None:
        """Configure the general settings of the picoscope."""
        settings = QtCore.QSettings()
        timebase = settings.value("timebase", type=float) * 1e-9  # ns to s
        samples = settings.value("samples", type=int)
        resolution = settings.value("resolution", defaultValue="12", type=str)
        channel = settings.value("triggerChannel", defaultValue="A", type=str)
        threshold = settings.value("triggerThreshold", type=float)
        point = settings.value("triggerPoint", type=str)
        timeout_ms = settings.value("timeout", 100, type=int)
        text = (
            f"\ntimebase {timebase} \nsamples {samples} \nresolution {resolution} \n"
            f"channel {channel} \nthreshold {threshold} \n"
            f"point {point} \ntimeout_ms {timeout_ms}"
        )
        try:
            self.ps.setSimpleTrigger(
                channel,
                threshold,
                point,
                timeout_ms=timeout_ms,
            )
            self.timebase, noSamples, maxSamples = self.ps.setSamplingInterval(
                timebase, timebase * samples
            )
        except IOError as exc:
            self.showMessage(
                "Configure Picoscope Error",
                f"{type(exc).__name__} configuring the picoscope: {exc}" + text,
                exc=exc,
            )
            raise
        else:
            self.picoscopeToConfigure = False
            log.debug(
                f"Trigger set: Samples in duration {noSamples}, "
                f"max possible samples {maxSamples}"
            )
        try:
            self.ps.setResolution(resolution)
        except IOError as exc:
            notSupported = (
                "Error calling _lowLevelSetDeviceResolution: "
                "PICO_NOT_SUPPORTED_BY_THIS_DEVICE (A function "
                "has been called that is not supported by the "
                "current device variant.)"
            )
            if str(exc) == notSupported:
                pass
            else:
                self.showMessage(
                    "Configure Picoscope Error",
                    "Resolution is not set.",
                    f"{type(exc).__name__} configuring the picoscope: {exc}",
                    exc=exc,
                )
        log.info("Picoscope configured.")

    def setChannel(self, channel: str) -> None:
        """Configure a channel."""
        settings = QtCore.QSettings()
        settings.beginGroup(f"ch{channel}")
        enabled = settings.value("enabled", type=bool)
        data = {
            "coupling": settings.value("coupling", "DC", type=str),
            "VRange": self.RangeStrToVolt(settings.value("range", "200 mV", type=str)),
            "VOffset": settings.value("offset", type=float),
            "enabled": enabled,
        }
        try:
            self.ps.setChannel(channel, **data)
        except IOError as exc:
            self.showMessage(
                "Set Channel Error", f"Error setting channel {channel}: {exc}", exc=exc
            )
            raise
        else:
            self.activeChannels[channel] = enabled

    def _configure_picoscope(self) -> None:
        if self.channelsToConfigure:
            for channel in self.channelsToConfigure:
                self.setChannel(channel)
            self.channelsToConfigure = {}
            log.info("Channels configured.")
        if self.picoscopeToConfigure:
            self.configurePicoscope()

    def _runBlock(self) -> None:
        settings = QtCore.QSettings()
        max = 5
        i = 0
        while True:
            try:
                self.ps.runBlock(
                    settings.value("preTriggerSamples", type=int)
                    / settings.value("samples", type=int),
                    segmentIndex=0,
                    callback=self.blockReady,
                )
            except IOError as exc:
                log.exception(f"RunBlock failed on try {i}.", exc_info=exc)
                if i == max:
                    self.showMessage(
                        "Run Block Failed",
                        f"Running a block failed on try {i}: {exc}",
                        exc=exc,
                    )
                    self.actionMeasure.setChecked(False)
                else:
                    i += 1
                    time.sleep(0.005)
            else:
                break

    def runBlock(self) -> None:
        """Configure the picoscope, if necessary, and start a measurement."""
        # Configure the picoscope
        try:
            self._configure_picoscope()
        except IOError as exc:
            log.exception("RunBlock setup failed with IOError.", exc_info=exc)
            self.actionMeasure.setChecked(False)
            self.configureEverything()
            return  # Already handled beforehand.
        except Exception as exc:
            log.exception("RunBlock setup failed.", exc_info=exc)
            self.actionMeasure.setChecked(False)
            self.showMessage(
                "Run Block Failed",
                f"Run Block setup failed with {type(exc).__name__}: {exc}",
            )
            return
        # call "runBlock"
        self._runBlock()
        log.debug("RunBlock called.")

    @pyqtSlot(bool)
    def connectPico(self, checked: bool) -> None:
        """Connect to the Picoscope and start a measurement."""
        if checked:
            serialNumber = QtCore.QSettings().value("serialNumber", type=str)
            if serialNumber == "":
                serialNumber = None
            try:
                self.ps.open(serialNumber=serialNumber)
                self.channelCount = int(self.ps.getUnitInfo("VariantInfo")[1])
                m = self.ps.ping()
            except (AttributeError, IOError) as exc:
                self.actionConnect.setChecked(False)
                self.showMessage(
                    "Connection failed",
                    f"Connection to the scope failed with {type(exc).__name__}:{exc}",
                    exc=exc,
                )
                return
            if m == 0:
                # Configure all channels and the picoscope
                self.configureEverything()
                if self.actionMeasure.isChecked():
                    self.runBlock()
                if serialNumber is None:
                    serialNumber = self.ps.getUnitInfo("BatchAndSerial")
                self.setWindowTitle(f"Picoscope {serialNumber}")
            else:
                self.showMessage(
                    "Error",
                    f"{self.ps.ERROR_CODES[m][1]}: \n {self.ps.ERROR_CODES[m][2]}",
                    icon=QMessageBox.Icon.Warning,
                )
                log.error(
                    f"Connect to scope failed with {self.ps.ERROR_CODES[m][1]}: \n"
                    f"{self.ps.ERROR_CODES[m][2]}"
                )
                self.connectPico(False)
        else:
            if self.ps.handle is not None:
                self.ps.close()

    @pyqtSlot()
    def reconnect(self) -> None:
        self.connectPico(False)
        self.actionMeasure.setChecked(True)
        self.connectPico(True)

    @pyqtSlot(bool)
    def startMeasurement(self, checked: bool) -> None:
        """Start/stop measureing."""
        if self.actionConnect.isChecked():
            if checked:
                self.runBlock()
            else:
                if self.ps.handle is not None:
                    try:
                        self.ps.stop()
                    except IOError as exc:
                        log.exception(
                            "Stopping measurement failed with IOError.", exc_info=exc
                        )
                    except Exception as exc:
                        self.showMessage(
                            "Stopping error", f"Error stopping: {exc}", exc=exc
                        )

    def RangeStrToVolt(self, string: str) -> float:
        """Transform the Voltage-Ranges string with unit to a float in Volt."""
        if string in ["20 mV", "50 mV", "100 mV", "200 mV", "500 mV"]:
            return float(string.replace(" mV", "")) / 1000
        else:
            return float(string.replace(" V", ""))

    @pyqtSlot(str)
    def channelConfigured(self, channel: str) -> None:
        """Set `channel` to be configured."""
        self.channelsToConfigure[channel] = True

    @pyqtSlot(bool)
    def readoutA(self, checked: bool) -> None:
        """Configure Channel A."""
        self.readout(checked, "A")

    @pyqtSlot(bool)
    def readoutB(self, checked: bool) -> None:
        """Configure Channel B."""
        self.readout(checked, "B")

    @pyqtSlot(bool)
    def readoutC(self, checked: bool) -> None:
        """Configure Channel C."""
        self.readout(checked, "C")

    @pyqtSlot(bool)
    def readoutD(self, checked: bool) -> None:
        """Configure Channel D."""
        self.readout(checked, "D")

    @pyqtSlot(bool)
    def readoutE(self, checked: bool) -> None:
        """Configure Channel E."""
        self.readout(checked, "E")

    @pyqtSlot(bool)
    def readoutF(self, checked: bool) -> None:
        """Configure Channel F."""
        self.readout(checked, "F")

    @pyqtSlot(bool)
    def readoutG(self, checked: bool) -> None:
        """Configure Channel G."""
        self.readout(checked, "G")

    @pyqtSlot(bool)
    def readoutH(self, checked: bool) -> None:
        """Configure Channel H."""
        self.readout(checked, "H")

    def readout(self, checked: bool, channel: str) -> None:
        """(De)Activate `channel` if `checked`."""
        settings = QtCore.QSettings()
        if not checked and channel == settings.value("triggerChannel", type=str):
            text = (
                f"You cannot deactivate channel {channel}, "
                "because it is the trigger channel."
            )
            self.showMessage("Trigger necessary", text)
        else:
            settings.setValue(f"ch{channel}/enabled", checked)
            self.channelsToConfigure[channel] = True

    @pyqtSlot(bool)
    def openChannelSettingsA(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("A")

    @pyqtSlot(bool)
    def openChannelSettingsB(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("B")

    @pyqtSlot(bool)
    def openChannelSettingsC(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("C")

    @pyqtSlot(bool)
    def openChannelSettingsD(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("D")

    @pyqtSlot(bool)
    def openChannelSettingsE(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("E")

    @pyqtSlot(bool)
    def openChannelSettingsF(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("F")

    @pyqtSlot(bool)
    def openChannelSettingsG(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("G")

    @pyqtSlot(bool)
    def openChannelSettingsH(self, checked: bool) -> None:
        """Open the Channel Settings for the specified channel."""
        self.openChannelSettings("H")

    def openChannelSettings(self, channel: str) -> None:
        """Open the Channel Settings."""
        if channel not in self.channelSettings.keys():
            channelSettings = ChannelSettings(channel, main=self)
            channelSettings.signals.channelChanged.connect(self.channelConfigured)
            self.signals.channelUpdated[channel].connect(channelSettings.update)
            self.signals.samplesChanged.connect(channelSettings.setLimits)
            self.channelSettings[channel] = channelSettings
        self.channelSettings[channel].open()

    def blockReady(self, handle, status: int, pointer=None) -> None:
        """Emit a data ready signal."""
        self.signals.blockReady.emit(status)

    def _transfer_data(self) -> None:
        names_list: list[str] = []
        values_list: list[str] = []
        overflowing_list: list[str] = []
        data_dict: dict[str, Any] = {}
        for channelNumber in range(self.channelCount):
            channel = chr(channelNumber + 65)  # Start at A
            if self.activeChannels.get(channel):
                self.read_channel(
                    channel,
                    names_list=names_list,
                    values_list=values_list,
                    overflowing_list=overflowing_list,
                    data_dict=data_dict,
                )
                # TODO
        names_list.append("Interval")
        values_list.append(f"{1000 * (time.perf_counter() - self.last):.4}ms")
        self.last = time.perf_counter()
        self.lbVariable.setText("\n".join(names_list))
        self.lbDisplay.setText("\n".join(values_list))
        self.lbOverrange.setText("\n".join(overflowing_list))
        try:
            self.publisher.send_legacy(data_dict)
            self.publisher.send_data(data=data_dict)
        except Exception as exc:
            log.exception("Publisher error.", exc_info=exc)

    @pyqtSlot(int)
    def transferData(self, status: int) -> None:
        """Transfer Data from Scope to computer."""
        if status != 0:
            self.showMessage(
                str(self.ps.errorNumToName(status)),
                ("Data transfer failed!" f"\n{self.ps.errorNumToDesc(status)}"),
                icon=QMessageBox.Icon.Warning,
            )
            log.error(f"Data transfer failed: {self.ps.errorNumToName(status)}")
            return
        log.debug("Transferring data.")
        self.publisher.send_legacy({self.name: True})  # As a trigger.
        if self.actionConnect.isChecked():
            self._transfer_data()
            # Start the next block with configured channels.
            if self.actionMeasure.isChecked():
                self.runBlock()
                # self.signals.runBlock.emit()

    def read_channel(
        self,
        channel: str,
        names_list: list[str],
        values_list: list[str],
        overflowing_list: list[str],
        data_dict: dict[str, Any],
    ) -> None:
        try:
            data, overflow = self.ps.getDataV(channel, returnOverflow=True)
        except (IOError, AttributeError) as exc:
            if "PICO_HARDWARE_CAPTURING_CALL_STOP" in exc.args[0]:
                log.exception("Error reading a channel during capture.", exc_info=exc)
            else:
                log.exception("Error transferring data.", exc_info=exc)
                self.showMessage(
                    "Data Transfer Error",
                    f"{type(exc).__name__} transferring data: {exc}",
                )
                # TODO add option to stop measureing.
                self.actionMeasure.setChecked(False)
        else:
            settings = QtCore.QSettings()
            inverted = settings.value(f"ch{channel}/inverted", type=bool)
            self.data[channel] = -data if inverted else data
            self.signals.channelUpdated[channel].emit(overflow)
            value, zero = self.dataCalculation(channel, overflow)
            # Show value
            values_list.append(str(value))
            varName = settings.value(f"ch{channel}/variableName", type=str)
            names_list.append(f"Channel {channel} ({varName}):")
            # TODO add proper calibration method
            if cal := calibration.Calibration(varName, value):
                data_dict[varName + "_cal"] = cal
            overflowing_list.append("OVERRANGE!" if overflow else "")
            data_dict[varName] = value
            # TODO handle overrange for intercom
            # Show zero value
            if zeroName := settings.value(f"ch{channel}/variableNameZero", type=str):
                values_list.append(str(zero))
                names_list.append(f"Channel {channel} ({zeroName}):")
                overflowing_list.append("")
                data_dict[zeroName] = zero

    def dataCalculation(self, channel: str, overflow: bool) -> tuple[float, Any]:
        """Calculate the value and zeroing for `channel` unless `overflow`."""
        if overflow:
            return np.nan, np.nan

        data = self.data[channel]
        settings = QtCore.QSettings()
        settings.beginGroup(f"ch{channel}")

        if settings.value("zeroing", type=bool):
            zeroStart = settings.value("zeroStart", type=int)
            zeroStop = settings.value("zeroStop", type=int)
            zeroFunction = settings.value("functionZero", defaultValue="average", type=str)
            zeroDirection = 1 if zeroStop > zeroStart else -1
            zeroData = data[zeroStart: zeroStop + zeroDirection: zeroDirection]
            if zeroFunction == "average":
                zero = np.mean(zeroData)
            elif zeroFunction == "integrate":
                zero = self.timebase * sum(zeroData)
            elif zeroFunction == "sum":
                zero = sum(zeroData)
            else:
                raise NotImplementedError
        else:
            zero = 0

        start = settings.value("signalStart", 0, type=int)
        stop = settings.value("signalStop", 99, type=int)
        function = settings.value("function", defaultValue="average", type=str)

        direction = 1 if stop > start else -1
        dataView = data[start: stop + direction: direction]
        if function == "average":
            return np.mean(dataView) - zero, zero  # type: ignore
        elif function == "integrate":
            if settings.value("functionZero", defaultValue="average", type=str) == "integrate":
                return self.timebase * (
                    sum(dataView) - zero), zero
            else:
                return self.timebase * (
                    sum(dataView) - zero * (abs(stop - start) + 1)
                ), zero
        elif function == "sum":
            return sum(dataView) - zero * (abs(stop - start) + 1), zero
        elif function == "first":  # The first value.
            return dataView[0] - zero, zero
        elif function == "maximum":
            return np.nanmax(dataView) - zero, zero
        elif function == "minimum":
            return np.nanmin(dataView) - zero, zero
        else:
            raise NotImplementedError

    @pyqtSlot()
    def showInfo(self) -> None:
        """Show all the information about the picoscope."""
        try:
            text = self.ps.getAllUnitInfo()
        except Exception as exc:
            log.exception("Reading all unit info failed.", exc_info=exc)
        else:
            self.showMessage("Picoscope information", text)

    def showMessage(
        self,
        title: str,
        text: str,
        detailedText: Optional[str] = None,
        icon=QMessageBox.Icon.Information,
        exc: Optional[Exception] = None,
    ) -> None:
        """Open a messagebox that shows for example the error that occurred."""
        if exc is not None:
            log.exception(f"An error occurred: {text}", exc_info=exc)
        self.message = QtWidgets.QMessageBox()
        message = self.message
        message.setText(text)
        message.setWindowTitle(title)
        if detailedText is not None:
            message.setDetailedText(detailedText)
        message.setIcon(icon)
        message.exec()


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    start_app(Picoscope)

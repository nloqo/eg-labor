"""
Main file of the Lasercommander.

created on 16.12.2020 by Benedikt Burger
"""

# Standard packages
import datetime
import logging
from time import sleep
from typing import Any, Callable, NamedTuple, Optional

# import third party tools and libraries
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import pyqtSlot
from PyQt6.QtWidgets import QMessageBox
from pymeasure.instruments.ipgphotonics.yar import YAR
from pyleco.directors.director import Director
from pyleco.directors.transparent_director import TransparentDirector
from pyleco.directors.starter_director import StarterDirector
from pyleco.directors.coordinator_director import CoordinatorDirector
from pyleco.core.message import Message
from pyleco.core.serialization import split_name_str
from pyleco.json_utils.errors import (
    JSONRPCError,
    ServerError,
    RECEIVER_UNKNOWN,
    NODE_UNKNOWN,
)

from pyleco_extras.gui_utils.base_main_window import (
    start_app,
    LECOBaseMainWindowDesigner,
    Path,
)
from pyleco_extras.directors.analyzing_director import AnalyzingDirector
from pyleco_extras.directors.tmc_motor_director import TMCMotorDirector

# import files from data directory
from data.Settings import GeneralSettings


logging.getLogger("PyQt6").setLevel(logging.INFO)
log = logging.getLogger(__name__)
handler = logging.StreamHandler()
gLog = logging.getLogger()
gLog.addHandler(handler)


# Parameters
YAG_seed_target_values = 25, 50.87, 49.66  # TEC, Heater1, Heater2, in °C
polarizer_start_values = {
    "polarizerOPO": 5,
    "polarizerOPA2": 5,
    # "polarizerProbe": 0,
    # "polarizerMIR": 45,
}
#   Component names
SEED = "MYRES.koherasBasik"
AMPLIFIER = "MYRES.fiberAmp"
ACULIGHT = "MYRES.aculight"
OPO2OVEN = "MYRES.OPOoven"
OPO_BLOCK = "MYRES.opoControl"
OPA1OVEN = "MYRES.OPA1oven"
POLARIZER = "MYRES.polarizer"
MATCHING_MOTORS = "MYRES.matchingMotors"
WAVEMETER = "MYRES.wavemeterReader"
NICARD = "MYRES.nicard"
YAG_SEED_SHUTTER = "pico3.seedShutter"
YAG_SEED = "pico3.innolas"
YAG = "pico3.quanta-ray"
YAG_SHUTTER = "pico3.yagShutter"
PICO3_NICARD = "pico3.nicard"
LAB_TEMPERATURE_CONTROLLER = "pico3.TemperatureController"

nan = float("nan")
# whether something is correct or not
check_mark = "\u2714"  # heavy check mark
x_mark = "\u2716"  # heavy multiplication x

# state of shutters
closed_mark = "\u2716"  # heavy multiplication x
open_mark = "\u2b58"  # heavy circle
enforced_close_mark = "\U0001f512"  # lock

"\u2a3b"  # multiplication sign in triangle
"\U0001f53a"  # up-pointing red triangle
"\u25ec"  # triangle dot
"\u27c1"  # triangle triangle
"\u25b2"  # black triangle
"\u25b3"  # white triangle
"\u26a0"  # warning sign
"\u274c"  # red cross mark
"\u2b24"  # black large circle
"\u2b58"  # heavy circle
"\u26bf"  # squared key
"\U0001f511"  # key
"\U0001f512"  # lock


class Lasercommander(LECOBaseMainWindowDesigner):
    """Control the different elements of the MIR laser system for nonlinear spectroscopy.

    :param name: Name of this program.
    :param host: Host of the Coordinator.
    """

    "Define GUI elements"
    # Actions
    actionConnectAll: QtGui.QAction
    actionDisconnectAll: QtGui.QAction
    actionEnable_Seed_Emission: QtGui.QAction
    actionDisable_Seed_Emission: QtGui.QAction
    actionMatchingControl: QtGui.QAction
    actionDirectPolarizerControl: QtGui.QAction
    actionBlockSHGShutter: QtGui.QAction
    actionStateReport: QtGui.QAction
    actionStartLaser: QtGui.QAction
    actionStopLaser: QtGui.QAction
    actionStartOPO: QtGui.QAction
    actionStopOPO: QtGui.QAction
    actionInitializePolarizerMotors: QtGui.QAction
    actionRestPolarizerMotors: QtGui.QAction
    actionStartYAG: QtGui.QAction
    actionStopYAG: QtGui.QAction

    # Seed
    gbWavelength: QtWidgets.QGroupBox
    sbWavelength: QtWidgets.QDoubleSpinBox
    pbWavelength: QtWidgets.QPushButton
    pbWavelengthMin: QtWidgets.QPushButton
    pbWavelengthMax: QtWidgets.QPushButton
    pbWavelengthStep: QtWidgets.QPushButton
    pbWavelengthScan: QtWidgets.QPushButton
    lbWavelength: QtWidgets.QLabel
    lbWavelengthSetpoint: QtWidgets.QLabel
    gbModulation: QtWidgets.QGroupBox
    sbModulationLevel: QtWidgets.QSpinBox
    sbModulationOffset: QtWidgets.QSpinBox
    sbModulationFrequency: QtWidgets.QDoubleSpinBox
    pbModulationGet: QtWidgets.QPushButton
    pbModulationSet: QtWidgets.QPushButton
    pbModulationScan: QtWidgets.QPushButton

    # Amplifier
    gbPower: QtWidgets.QGroupBox
    pbPower: QtWidgets.QPushButton
    sbPower: QtWidgets.QSpinBox
    lbPower: QtWidgets.QLabel
    lbPowerSetpoint: QtWidgets.QLabel
    lbPowerCurrent: QtWidgets.QLabel
    lbPowerTemperature: QtWidgets.QLabel

    # Aculight OPO
    gbAculight: QtWidgets.QGroupBox
    sbAculightEtalon: QtWidgets.QDoubleSpinBox
    sbAculightCrystalTemp: QtWidgets.QDoubleSpinBox
    pbAculightCrystalTemp: QtWidgets.QPushButton
    lbAculight: QtWidgets.QLabel

    # OPO Block
    gbOPOBlock: QtWidgets.QGroupBox
    lbOPOBlockTemperature: QtWidgets.QLabel

    # Matching motors
    gbMatchingMotors: QtWidgets.QGroupBox
    pbOPOPosition: QtWidgets.QPushButton
    pbOPO2Position: QtWidgets.QPushButton
    pbOPA2aPosition: QtWidgets.QPushButton
    pbOPA2bPosition: QtWidgets.QPushButton
    sbOPOPosition: QtWidgets.QDoubleSpinBox
    sbOPO2Position: QtWidgets.QDoubleSpinBox
    sbOPA2aPosition: QtWidgets.QDoubleSpinBox
    sbOPA2bPosition: QtWidgets.QDoubleSpinBox
    lbOPO2Position: QtWidgets.QLabel
    lbOPA2a: QtWidgets.QLabel
    lbOPA2b: QtWidgets.QLabel

    pbControl: QtWidgets.QPushButton
    sbControl: QtWidgets.QSpinBox

    # OPO
    gbOPO: QtWidgets.QGroupBox
    lbOPO: QtWidgets.QLabel

    # Polarizer motors
    gbPolarizer: QtWidgets.QGroupBox
    pbPolarizer: QtWidgets.QPushButton
    slPolarizer: QtWidgets.QSlider
    sbPolarizer: QtWidgets.QDoubleSpinBox
    bbPolarizer: QtWidgets.QComboBox
    lbPolarizerOPA: QtWidgets.QLabel
    lbPolarizerExp: QtWidgets.QLabel

    # OPA1 oven
    gbOPA1: QtWidgets.QGroupBox
    sbOPA1Temperature: QtWidgets.QDoubleSpinBox
    pbOPA1: QtWidgets.QToolButton
    lbOPA1Setpoint: QtWidgets.QLabel
    lbOPA1Temperature: QtWidgets.QLabel

    # YAG
    gbYAG: QtWidgets.QGroupBox
    lbYAG_shots: QtWidgets.QLabel
    lbYAG_BUT: QtWidgets.QLabel

    # YAG Shutter
    pbYAGShutter: QtWidgets.QPushButton
    pbYAGShutterPoll: QtWidgets.QPushButton
    rbYAGfundO: QtWidgets.QRadioButton
    rbYAGfundN: QtWidgets.QRadioButton
    rbYAGfundL: QtWidgets.QRadioButton
    lbYAGShutterFund: QtWidgets.QLabel
    lbYAGShutterSHG: QtWidgets.QLabel

    # YAG seed
    pbSeedTemperature: QtWidgets.QPushButton
    pbSeedTemperaturePoll: QtWidgets.QPushButton
    lbSeedTemperature1: QtWidgets.QLabel
    lbSeedTemperature2: QtWidgets.QLabel

    # YAG seed shutter
    pbSeedShutter: QtWidgets.QPushButton
    pbSeedShutterOpen: QtWidgets.QPushButton
    pbSeedShutterClose: QtWidgets.QPushButton
    lbSeedShutter: QtWidgets.QLabel

    # Lab Temperature
    gbLabTemperature: QtWidgets.QGroupBox
    lbLabTemperature: QtWidgets.QLabel

    "# INITIALIZATION"

    def __init__(self, name: str = "Lasercommander", **kwargs):
        # Use initialization of parent class QMainWindow.
        super().__init__(
            name=name,
            ui_file_name="Lasercommander",
            ui_file_path=Path(__file__).parent / "data",
            settings_dialog_class=GeneralSettings,
            **kwargs,
        )

        self.configSets = (
            # UI configuration widget, key, defaultValue, type
            (self.sbOPOPosition, "sbOPOPosition", 0, int),
            (self.sbOPO2Position, "sbOPO2Position", 0, int),
            (self.sbOPA2aPosition, "sbOPA2aPosition", 0, int),
            (self.sbOPA2bPosition, "sbOPA2bPosition", 0, int),
        )

        # Define objects: Signals, Timers, Locks...
        self.signals = self.CommanderSignals()
        self.readoutTimer = QtCore.QTimer()

        # Device groups with an individual name
        self.seed_group = SeedGroup(commander=self)
        self.amplifier_group = AmplifierGroup(commander=self)
        # self.matching_group = MatchingGroup(commander=self)
        # self.opo_group = OPOGroup(commander=self)
        self.polarizer_group = PolarizerGroup(commander=self)
        self.yag_shutter_group = YAGShutterGroup(commander=self)
        self.yag_seed_group = YAGSeedGroup(commander=self)
        self.yag_seed_shutter_group = YAGSeedShutterGroup(commander=self)

        self.groups: list[InstrumentGroup] = [
            self.seed_group,
            self.amplifier_group,
            # self.matching_group,
            # self.opo_group,
            self.polarizer_group,
            self.yag_shutter_group,
            self.yag_seed_group,
            self.yag_seed_shutter_group,
        ]

        # Device groups without an individual variable name
        group_classes: list[type[InstrumentGroup]] = [
            AculightGroup,
            OPOGroup,
            YagGroup,
            MatchingGroup,
            LabTemperatureGroup,
        ]
        for group_class in group_classes:
            self.groups.append(group_class(commander=self))

        # Directors
        self.director = Director(actor="", communicator=self.communicator)
        self.seed = self.seed_group.seed
        self.amplifier = self.amplifier_group.amplifier

        # Get settings.
        QtCore.QCoreApplication.instance().setApplicationName("Lasercommander")  # type: ignore
        self.settings = QtCore.QSettings()
        self.setSettings()  # requires timers to be present
        # Just at startup:
        self.readoutTimer.start()
        self.restoreConfiguration()

        # Connect actions to slots.
        #    connection
        self.actionConnectAll.triggered.connect(self.connectAllPressed)
        self.actionDisconnectAll.triggered.connect(self.disconnectAllPressed)
        #   devices
        #       Laser
        self.actionEnable_Seed_Emission.triggered.connect(
            self.seed_group.enableSeedEmission
        )
        self.actionDisable_Seed_Emission.triggered.connect(
            self.seed_group.disableSeedEmission
        )
        #       Polarizer
        self.actionDirectPolarizerControl.triggered.connect(
            self.polarizer_group.setDirectPolarizerControl
        )
        #       YAG shutter
        self.actionBlockSHGShutter.toggled.connect(
            self.yag_shutter_group.blockYAGSHGShutter
        )
        #       state report
        self.actionStateReport.triggered.connect(self.generate_state_report)
        #   start / stop
        #       Full laser system
        self.actionStartLaser.triggered.connect(self.startLaser)
        self.actionStopLaser.triggered.connect(self.stopLaser)
        #       OPO laser
        self.actionStartOPO.triggered.connect(self.startOPO)
        self.actionStopOPO.triggered.connect(self.stopOPO)
        #       Polarizer
        self.actionInitializePolarizerMotors.triggered.connect(
            self.polarizer_group.initializePolarizerMotors
        )
        self.actionRestPolarizerMotors.triggered.connect(
            self.polarizer_group.restPolarizerMotors
        )
        #       YAG
        self.actionStartYAG.triggered.connect(self.startYAG)
        self.actionStopYAG.triggered.connect(self.stopYAG)

        # Connect buttons to slots.
        #    OPO 2 Block
        self.gbOPOBlock.toggled.connect(self.connectOPOBlock)
        #    OPA 1
        self.gbOPA1.toggled.connect(self.connectOPA1Oven)
        self.pbOPA1.clicked.connect(self.setOPA1Temperature)

        log.info("Lasercommander initialized.")

    # Setup methods and signal class
    class CommanderSignals(QtCore.QObject):
        """Signals for the Laser Commander."""

        # General signals.
        start = QtCore.pyqtSignal()
        close = QtCore.pyqtSignal()

    def readout_connection(self, slot: Callable[[], None], checked: bool) -> None:
        """(Dis)Connect the readout timer to/from a controller slot."""
        if checked:
            self.readoutTimer.timeout.connect(slot)
        else:
            try:
                self.readoutTimer.timeout.disconnect(slot)
            except TypeError:
                pass
                log.debug("Readout timer disconnection failed.")

    def restoreConfiguration(self) -> None:
        """Restore the UI configuration from the stored settings."""
        for widget, key, default, typ in self.configSets:
            widget.setValue(self.settings.value(key, default, typ))

    # On closure
    def closeEvent(self, event: QtCore.QEvent) -> None:
        """Clean up if the window is closed somehow."""
        log.info("Closing")
        self.stop_listen()
        self.storeConfiguration()
        self.signals.close.emit()
        self.disconnectAllPressed()
        event.accept()

    def storeConfiguration(self) -> None:
        """Store the current UI configuration."""
        for widget, key, default, typ in self.configSets:
            self.settings.setValue(key, widget.value())

    "# SLOTS AND THEIR FUNCTIONS"

    "## ACTIONS"

    # Application
    @pyqtSlot()
    def openSettings(self) -> None:
        """Open the settings dialogue and apply changed settings."""
        # open the settings dialogue
        self.settingsDialog = self.settings_dialog_class()
        self.settingsDialog.accepted.connect(self.setSettings)
        self.settingsDialog.open()  # to enable concurrent motor settings

    def setSettings(self) -> None:
        """Set variables to values from stored settings."""
        settings = QtCore.QSettings()
        self.readoutTimer.setInterval(settings.value("plotInterval", 1000, int))
        self.seed_group.set_settings(settings)

    # Devices/Connect
    def start_task(self, fullname: str, install: bool = False) -> None:
        """Request the task with `fullname` to be started."""
        node, name = split_name_str(fullname, "MYRES")
        starter = f"{node}.starter"
        try:
            with StarterDirector(
                actor=starter, communicator=self.communicator
            ) as director:
                director.start_tasks(name)
                if install:
                    director.install_tasks(name)
        except Exception as exc:
            log.exception(f"Starting task {name} failed.", exc_info=exc)

    @pyqtSlot(bool)
    def connectOPOBlock(self, checked: bool) -> None:
        """Connect to the OPO block."""
        self.gbOPOBlock.setChecked(checked)
        if checked:
            self.start_task(fullname=OPO_BLOCK)
            try:
                result = self.director.get_parameters("temperatures", actor=OPO_BLOCK)  # noqa
            except (ConnectionError, TimeoutError, JSONRPCError) as exc:
                self.connectionLost(OPO_BLOCK, "Connect", exc=exc)
                return
            else:
                self.show_OPOBlock_data(result)
        self.readout_connection(self.readOPOBlock, checked)

    @pyqtSlot(bool)
    def connectOPA1Oven(self, checked: bool) -> None:
        """Connect to the OPA1 oven."""
        self.gbOPA1.setChecked(checked)
        if checked:
            self.start_task(OPA1OVEN)
            try:
                # Test the connection
                self.director.get_parameters("temperature", actor=OPA1OVEN)
            except (ConnectionError, TimeoutError, JSONRPCError) as exc:
                self.connectionLost(OPA1OVEN, "Connect", exc=exc)
                return
        self.readout_connection(self.readOPA1Oven, checked)

    def connectAll(self, connect: bool) -> None:
        """(Dis)Connect all devices possible."""
        for group in self.groups:
            if group.groupbox is not None:
                group.groupbox.setChecked(connect)
        for switch in (
            self.gbOPOBlock,
            self.gbOPA1,
        ):
            switch.setChecked(connect)

    @pyqtSlot()
    def connectAllPressed(self) -> None:
        """React to actionConnectAll pressed."""
        self.connectAll(True)

    @pyqtSlot()
    def disconnectAllPressed(self) -> None:
        """React to actionDisconnectAll pressed."""
        self.connectAll(False)

    @pyqtSlot()
    def startLaser(self) -> None:
        """Start the full laser system."""
        self.connectAll(connect=True)
        self.startOPO()
        self.polarizer_group.initializePolarizerMotors()
        self.startYAG()

    @pyqtSlot()
    def stopLaser(self) -> None:
        """Stop the full laser system."""
        self.polarizer_group.restPolarizerMotors()
        self.stopYAG()
        self.stopOPO()

    @pyqtSlot()
    def startOPO(self) -> None:
        """Start the OPO system."""
        confirmation = QMessageBox()
        confirmation.setIcon(QMessageBox.Icon.Question)
        confirmation.setWindowTitle("Start laser?")
        confirmation.setStandardButtons(
            QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.Cancel
        )
        target = self.settings.value("amplifierPower", type=float)
        confirmation.setText(
            f"Do you want to start the laser with an output power of {target} W?"
        )
        if confirmation.exec() != QMessageBox.StandardButton.Yes:
            return
        self.start_task(self.seed_group.actor)
        self.start_task(self.amplifier_group.actor)
        self.start_task(self.yag_seed_group.actor)
        sleep(0.5)
        self.seed_group.connect_device(True)
        self.amplifier_group.connect_device(True)

        self.sbPower.setValue(target)
        self.pbPower.click()

    @pyqtSlot()
    def stopOPO(self) -> None:
        """Stop the OPO system."""
        confirmation = QMessageBox()
        confirmation.setIcon(QMessageBox.Icon.Question)
        confirmation.setWindowTitle("Stop laser?")
        confirmation.setStandardButtons(
            QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.Cancel
        )
        if self.gbPower.isChecked():
            if self.amplifier.device.emission_enabled:
                confirmation.setText(
                    "Do you really want to shut down the amplifier and seed laser?"
                )
                if confirmation.exec() == QMessageBox.StandardButton.Yes:
                    self.sbPower.setValue(-2)
                    self.pbPower.click()
            elif self.gbWavelength.isEnabled():
                confirmation.setText("Do you really want to disable seed emission?")
                if confirmation.exec() == QMessageBox.StandardButton.Yes:
                    self.seed.device.emission_enabled = False  # type: ignore
        else:
            m = (
                "No communication to the amplifier. Is amplifier emission "
                "off and do you want to disable seed emission?"
            )
            confirmation.setText(m)
            if confirmation.exec() == QMessageBox.StandardButton.Yes:
                self.seed.device.emission_enabled = False  # type: ignore

    @pyqtSlot()
    def generate_state_report(self) -> None:
        """Generate a state report and copy it to the clipboard."""
        self.yag_seed_group.getYAGSeedTemperature()
        text_elements = [
            "MIR laser system report \t" + datetime.datetime.now().isoformat(),
            f"OPO pump\t{self.lbWavelength.text()}\t{self.lbPower.text()}",
            f"OPO\t {self.lbOPOBlockTemperature.text()}",
            f"Aculight: {self.lbAculight.text()}",
            f"OPO Output: {self.lbOPO.text()}",
            f"OPA1\t{self.lbOPA1Temperature.text()}",
            f"OPA2\t{self.lbOPA2a.text()}\t{self.lbOPA2b.text()}",
            f"Polarizer\t{self.lbPolarizerOPA.text()}\t{self.lbPolarizerExp.text()}",
            f"YAG seed\t{self.lbSeedShutter.text()}\tPCB {self.lbSeedTemperature1.text()}  TC {self.lbSeedTemperature2.text()}",  # noqa: E501
            f"YAG\t{self.lbYAG_BUT.text()}\t{self.lbYAG_shots.text()}",
            "",
        ]
        clipboard = QtWidgets.QApplication.instance().clipboard()  # type: ignore
        clipboard.setText("\n".join(text_elements))

    "## BUTTONS"

    "### OPA 1"

    @pyqtSlot()
    def setOPA1Temperature(self) -> None:
        """Set the temperature setpoint of OPA 1."""
        try:
            self.director.set_parameters(
                actor=OPA1OVEN, parameters={"setpoint": self.sbOPA1Temperature.value()}
            )
        except TimeoutError:
            pass

    "### YAG"

    def control_YAG_polling(self, checked: bool) -> None:
        self.yag_seed_group.poll(checked=checked)
        self.pbSeedTemperaturePoll.setChecked(checked)
        self.yag_shutter_group.poll(checked=checked)
        self.pbYAGShutterPoll.setChecked(checked)

    @pyqtSlot()
    def startYAG(self) -> None:
        """Start the YAG utilities."""
        self.control_YAG_polling(True)
        self.yag_seed_shutter_group.openYAGSeedShutter()
        self.actionBlockSHGShutter.setChecked(True)
        self.rbYAGfundN.setChecked(True)
        self.yag_shutter_group.setYAGFundamentalShutterNeutral()

    @pyqtSlot()
    def stopYAG(self) -> None:
        """Stop the YAG."""
        self.control_YAG_polling(False)
        self.yag_seed_shutter_group.closeYAGSeedShutter()
        self.actionBlockSHGShutter.setChecked(False)
        self.rbYAGfundN.setChecked(True)
        self.yag_shutter_group.setYAGFundamentalShutterNeutral()

    "## READOUT"

    #   Read: Request values (and mapybe show it directly)
    @pyqtSlot()
    def readOPOBlock(self) -> None:
        self.director.get_parameters_async("temperatures", actor=OPO_BLOCK)

    def show_OPOBlock_data(self, data: dict[str, Any]) -> None:
        try:
            temps = data["temperatures"]
        except KeyError as exc:
            log.warning("Reading temperatures of OPO block failed.", exc_info=exc)
        else:
            self.lbOPOBlockTemperature.setText(
                f"block {temps[1]:.2f}°C, table {temps[2]:.2f}°C"
            )

    @pyqtSlot()
    def readOPA1Oven(self) -> None:
        """Read the OPA 1 oven."""
        try:
            result = self.director.get_parameters(
                actor=OPA1OVEN, parameters=["temperature", "setpoint"]
            )
        except (TimeoutError, ConnectionError, JSONRPCError) as exc:
            self.connectionLost(OPA1OVEN, "readOPA1Oven", exc)
        else:
            self.show_OPA1Oven_data(data=result)

    #   show_xyz_data: show the read data
    def show_OPA1Oven_data(self, data: dict) -> None:
        """Use the `data` read from the OPA1 oven."""
        self.lbOPA1Setpoint.setText(f"set: {data.get('setpoint', nan)}°C")
        self.lbOPA1Temperature.setText(f"{data.get('temperature', nan)}°C")

    "# METHODS"

    "## Connection: Change the front panel according to the connection"

    def connectionLost(
        self, device: str, caller: str, exc: Optional[Exception] = None
    ) -> None:
        """
        Deactivate the connection to 'device', give an error message with
        'caller' and optionally the exception 'exc' as hint.

        :param device: Name of the device or full name of the component.
        :param caller: Method which tried to call that device.
        :param exc: Exception which has been called
        """
        connection = {
            OPA1OVEN: self.connectOPA1Oven,
            OPO_BLOCK: self.connectOPOBlock,
            "?": log.warning,
        }
        text = f"Connection to {device} failed."
        try:
            self.get_group_from_sender(sender_name=device).connect_device(False)
        except KeyError:
            try:
                connection[device](False)
            except KeyError:
                text = f"Connection to an unknown device '{device}' failed!"
        # show an Error message Box
        error = QMessageBox()
        error.setIcon(QMessageBox.Icon.Warning)
        error.setWindowTitle("Connection error")
        error.setText(text)
        detailedText = f"Called by {caller}."
        if exc is not None:
            detailedText += f"\n{type(exc).__name__} '{exc}'."
            log.error(f"{caller} lost connection.", exc_info=exc)
        else:
            log.error(f"{caller} lost connection to {device}.")
        error.setDetailedText(detailedText)
        error.exec()

    @pyqtSlot(Exception)
    def connectionErrorAmplifier(self, exc: Exception) -> None:
        """Handle a thread's amplifier connection error."""
        if isinstance(exc, AssertionError):
            self.assertion(exc, caller="Amplifier thread")
        else:
            self.connectionLost(AMPLIFIER, "thread", exc)

    "## Show and store information"

    def assertion(
        self, assertion: str | Exception, caller: Optional[str] = None
    ) -> None:
        """Give a hint about bad inputs and similar."""
        warning = QMessageBox()
        warning.setIcon(QMessageBox.Icon.Warning)
        warning.setWindowTitle("Wrong inputs")
        warning.setText(f"Assertion failed with '{assertion}'.")
        warning.setDetailedText(f"Called by {caller}")
        warning.exec()

    def showInformation(
        self,
        title: str,
        text: str,
        detailedText: Optional[str] = None,
        icon=QMessageBox.Icon.Information,
    ) -> None:
        """Show some information to the user."""
        long_text = title + ": " + text
        log.warning(long_text)
        self.show_status_bar_message(long_text, 10000)
        self.info = QtWidgets.QMessageBox()
        info = self.info
        info.setIcon(icon)
        info.setWindowTitle(title)
        info.setText(text)
        if detailedText is not None:
            info.setDetailedText(detailedText)
        info.exec()

    @pyqtSlot(Message)
    def message_received(self, message: Message) -> None:
        """Handle a message received via the Communicator."""
        log.debug(
            f"Message from {message.sender} with {message.conversation_id} "
            f"and data '{message.data}' received."
        )
        if b'"jsonrpc":' not in message.payload[0]:
            log.error(f"Non json message received: {message}.")
            return
        if not isinstance((data := message.data), dict):
            log.error(f"Non dictionary message received: {data}.")
            return
        if "result" in data.keys():
            self.handle_device_response(message.sender.decode(), data.get("result"))  # type: ignore
        elif error := data.get("error"):
            if (
                message.sender_elements.name == b"COORDINATOR"
                and error.get("code") == RECEIVER_UNKNOWN.code
            ):
                full_name = error.get("data", "")
                self.handle_receiver_unknown(full_name)
            elif (
                error.get("code") == NODE_UNKNOWN.code and error.get("data") == "pico3"
            ):
                log.warning(
                    "Pico3 is not known, I tell the Coordinator to connect to pico3."
                )
                with CoordinatorDirector(
                    actor="MYRES.COORDINATOR", communicator=self.communicator
                ) as director:
                    director.add_nodes({"pico3": "pico3.iap.physik.tu-darmstadt.de"})
            else:
                text = (
                    f"Error from {message.sender}: {error}, {message.conversation_id}."
                )
                log.error(text)
                # self.showInformation("Error message received", text)
                self.show_status_bar_message(text, 10000)

    def handle_receiver_unknown(self, full_name: str) -> None:
        name = split_name_str(full_name)[1]
        log.error(f"Connection lost to {name} at {full_name}.")
        self.connectionLost(full_name, "message_received")
        self.show_status_bar_message(
            f"Connection lost to {name} at {full_name}.", 10000
        )

    def get_group_from_sender(self, sender_name: str) -> "InstrumentGroup":
        for group in self.groups:
            if sender_name == group.actor:
                return group
        raise KeyError

    def handle_device_response(self, sender_name: str, data: dict[str, Any]):
        """Handle a response from a device."""
        try:
            self.get_group_from_sender(sender_name=sender_name).show_data(data)
        except KeyError:
            pass
        else:
            return
        if sender_name == OPA1OVEN:
            self.show_OPA1Oven_data(data)
        elif sender_name == OPO_BLOCK:
            self.show_OPOBlock_data(data)
        # TODO add more

    def test(self):
        """Print a message if something happens."""
        log.info("Test successful")


class InstrumentGroup(QtCore.QObject):
    """Group of methods for a single instrument."""

    actor: str  # Full name of the actor
    groupbox: Optional[QtWidgets.QGroupBox] = None  # for connect_all

    def __init__(self, commander: Lasercommander, **kwargs) -> None:
        super().__init__(parent=commander, **kwargs)
        self.commander = commander
        self.make_connections()

    def make_connections(self) -> None:
        """Make the connections between Signals and Slots."""
        pass

    def connect_device(self, checked: bool) -> None:
        "(Dis)Connect the device."
        raise NotImplementedError
        # for example:
        self.groupbox.setChecked(checked)
        if checked:
            self.commander.start_task(self.actor)
        self.poll(checked=checked)

    def connection_lost_action(self) -> None:
        pass

    @pyqtSlot()
    def request_default_data(self) -> None:
        """Request data for regular readout."""
        raise NotImplementedError

    def show_data(self, data: dict[str, Any]) -> None:
        """Show any data returned by the device."""
        raise NotImplementedError

    def poll(self, checked: bool) -> None:
        """Enable/disable regular polling"""
        self.commander.readout_connection(self.request_default_data, checked=checked)


class SeedGroup(InstrumentGroup):
    actor = SEED

    def __init__(self, commander: Lasercommander, **kwargs):
        super().__init__(commander=commander, **kwargs)
        self.seed = TransparentDirector(
            actor=self.actor, communicator=self.commander.communicator
        )
        self.scanTimer = QtCore.QTimer()
        self.groupbox = self.commander.gbWavelength

        self.scanTimer.timeout.connect(self.setWavelengthStep)

    def make_connections(self):
        self.commander.gbWavelength.toggled.connect(self.connect_device)
        self.commander.pbWavelength.clicked.connect(self.setWavelength)
        self.commander.pbWavelengthMin.clicked.connect(self.setWavelengthMin)
        self.commander.pbWavelengthMax.clicked.connect(self.setWavelengthMax)
        self.commander.pbWavelengthStep.clicked.connect(self.setWavelengthStep)
        self.commander.pbWavelengthScan.clicked.connect(self.setWavelengthScan)
        self.commander.gbModulation.toggled.connect(self.enableModulation)
        self.commander.sbModulationLevel.valueChanged.connect(
            self.modulationLevelChanged
        )
        self.commander.sbModulationOffset.valueChanged.connect(
            self.modulationOffsetChanged
        )
        self.commander.pbModulationGet.clicked.connect(self.getModulation)
        self.commander.pbModulationSet.clicked.connect(self.setModulationPressed)
        self.commander.pbModulationScan.clicked.connect(self.setModulationScan)

    def set_settings(self, settings: QtCore.QSettings):
        self.scanTimer.setInterval(50000 * settings.value("seedScanStepSize", 4, int))

    @property
    def enabled(self) -> bool:
        return self.commander.gbWavelength.isEnabled()

    @property
    def checked(self) -> bool:
        return self.commander.gbWavelength.isChecked()

    @pyqtSlot(bool)
    def connect_device(self, checked: bool) -> None:
        """(Dis)Connect the seed laser."""
        self.commander.gbWavelength.setChecked(checked)
        if checked:
            self.commander.start_task(self.actor)
            # TODO read current modulation state and set the groupbox accordingly
            self.enableModulation(self.commander.gbModulation.isChecked())
        self.poll(checked=checked)

    @pyqtSlot()
    def enableSeedEmission(self) -> None:
        """Start the Laser system."""
        if self.enabled:
            self.seed.device.emission_enabled = True  # type: ignore

    @pyqtSlot()
    def disableSeedEmission(self) -> None:
        confirmation = QMessageBox()
        confirmation.setIcon(QMessageBox.Icon.Question)
        confirmation.setWindowTitle("Stop laser?")
        confirmation.setStandardButtons(
            QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.Cancel
        )
        if self.commander.gbPower.isChecked():
            if self.commander.amplifier.device.emission_enabled:
                confirmation.setText(
                    "The amplifier is still enabled, you cannot switch off the seed laser!"
                )
                confirmation.exec()
                return
            elif self.enabled:
                confirmation.setText("Do you really want to disable seed emission?")
                if confirmation.exec() == QMessageBox.StandardButton.Yes:
                    self._disableSeedEmission()
        else:
            confirmation.setText(
                "No communication to the amplifier. Is amplifier emission "
                "off and do you want to disable seed emission?"
            )
            if confirmation.exec() == QMessageBox.StandardButton.Yes:
                self._disableSeedEmission()

    def _disableSeedEmission(self) -> None:
        if self.enabled:
            self.seed.device.emission_enabled = False  # type: ignore

    @property
    def wlRange(self) -> tuple[float, float]:
        return self.seed.device.wlRange  # type: ignore

    @pyqtSlot()
    def setWavelength(self) -> None:
        """Set the seed wavelength."""
        setpoint = self.commander.sbWavelength.value()
        wlRange = self.wlRange
        testL = wlRange[0] / 1e3 <= setpoint - 1064
        testH = setpoint - 1064 <= wlRange[1] / 1e3
        try:
            assert testL and testH, "Invalid range for seed wavelength."
        except AssertionError:
            if setpoint < 1064:
                setpoint = 1064 + wlRange[0] / 1e3
            else:
                setpoint = 1064 + wlRange[1] / 1e3
            self.commander.sbWavelength.setValue(setpoint)
        self.seed.device.wavelength_setpoint = setpoint  # type: ignore

    @pyqtSlot()
    def setWavelengthMin(self) -> None:
        """Set the value to the minimum."""
        self.commander.sbWavelength.setValue(1064 + self.wlRange[0] / 1000)

    @pyqtSlot()
    def setWavelengthMax(self) -> None:
        """Set the value to the minimum."""
        self.commander.sbWavelength.setValue(1064 + self.wlRange[1] / 1000)

    # @pyqtSlot()
    def setWavelengthStep(self) -> None:
        """Do a single wavelength step."""
        # 10 GHz corresponds to 37 pm
        self.commander.sbWavelength.setValue(
            self.commander.sbWavelength.value() + 0.035
        )
        self.setWavelength()
        if (
            self.commander.pbWavelengthScan.isChecked()
            and self.commander.sbWavelength.value()
            == self.commander.sbWavelength.maximum()
        ):
            self.commander.pbWavelengthScan.setChecked(False)
            self.setWavelengthScan(False)

    @pyqtSlot(bool)
    def setWavelengthScan(self, checked: bool) -> None:
        """(Dis)able a wavelength scan."""
        settings = QtCore.QSettings()
        if checked:
            self.scanTimer.start(
                int(5e4 * settings.value("seedScanStepSize", 4, type=int))
            )
        else:
            self.scanTimer.stop()

    @pyqtSlot(int)
    def modulationLevelChanged(self, value: int) -> None:
        """Limit the modulation offset lest the sum exceeds 1000%o."""
        limit = 1000 - value
        self.commander.sbModulationOffset.setMinimum(-limit)
        self.commander.sbModulationOffset.setMaximum(limit)

    @pyqtSlot(int)
    def modulationOffsetChanged(self, value: int) -> None:
        """Limit the modulation level lest the sum exceeds 1000%o."""
        limit = 1000 - abs(value)
        self.commander.sbModulationLevel.setMaximum(limit)

    @pyqtSlot()
    def setModulationScan(self) -> None:
        """Set the modulation to default scan settings."""
        self.commander.sbModulationFrequency.setValue(0.01)
        self.commander.sbModulationOffset.setValue(0)
        self.commander.sbModulationLevel.setValue(1000)

    @pyqtSlot()
    def getModulation(self) -> None:
        """Get the current seed modulation."""
        if self.enabled:
            try:
                data = self.seed.get_parameters(
                    (
                        "wavelength_modulation_frequency1",
                        "wavelength_modulation_level",
                        "wavelength_modulation_offset",
                    )
                )
            except (TimeoutError, ConnectionError, JSONRPCError) as exc:
                self.commander.connectionLost(SEED, "getModulation", exc)
            else:
                self.show_seed_modulation_data(data)

    @pyqtSlot()
    def setModulationPressed(self) -> None:
        """Set the modulation config."""
        data = {}
        newFrequency = self.commander.sbModulationFrequency.value()
        if self.wavelengthModulationFrequency != newFrequency:
            data["wavelength_modulation_frequency1"] = newFrequency
            self.wavelengthModulationFrequency = newFrequency
        data["wavelength_modulation_level"] = self.commander.sbModulationLevel.value()
        data["wavelength_modulation_offset"] = self.commander.sbModulationOffset.value()
        self.seed.set_parameters(data)

    @pyqtSlot(bool)
    def enableModulation(self, checked: bool) -> None:
        """Enable wavelength modulation for the seed laser."""
        if self.checked:
            # Use the second modulation frequency for "no modulation".
            try:
                if checked:
                    self.seed.call_action("update_modulation_setup", unset=0x10)
                else:
                    self.seed.call_action("update_modulation_setup", set=0x10)
            except (TimeoutError, ConnectionError, ValueError, Exception) as exc:
                self.commander.connectionLost(SEED, "enableModulation", exc)
            else:
                self.getModulation()

    @pyqtSlot()
    def request_default_data(self) -> None:
        """Read the seed."""
        self.seed.get_parameters_async(("wavelength", "wavelength_setpoint"))

    def show_data(self, data: dict[str, float]) -> None:
        """Use the `data` read from the seed."""
        wl = data.get("wavelength", nan)
        self.commander.lbWavelengthSetpoint.setText(
            f"set: {data.get('wavelength_setpoint', nan):.4f} nm"
        )
        self.commander.lbWavelength.setText(f"{wl:.4f} nm")

    def show_seed_modulation_data(self, data: dict[str, Any]) -> None:
        """Use the `data` read from the seed regarding modulation."""
        freq = data.get("wavelength_modulation_frequency1")
        if freq is not None:
            self.commander.sbModulationFrequency.setValue(freq)
            self.wavelengthModulationFrequency = freq
        try:
            self.commander.sbModulationLevel.setValue(
                data["wavelength_modulation_level"]
            )
            self.commander.sbModulationOffset.setValue(
                data["wavelength_modulation_offset"]
            )
        except KeyError:
            pass


class AmplifierGroup(InstrumentGroup):
    actor = AMPLIFIER

    def __init__(self, commander: Lasercommander, **kwargs):
        super().__init__(commander=commander, **kwargs)
        self.amplifier = AnalyzingDirector(
            actor=self.actor, communicator=self.commander.communicator, device_class=YAR
        )
        self.groupbox = self.commander.gbPower

    def make_connections(self):
        self.commander.gbPower.toggled.connect(self.connect_device)
        self.commander.pbPower.clicked.connect(self.setPowerPressed)

    @pyqtSlot(bool)
    def connect_device(self, checked: bool) -> None:
        """(Dis)connect the amplifier."""
        self.commander.gbPower.setChecked(checked)
        if checked:
            self.commander.start_task(self.actor)
        self.poll(checked)

    def setPower(self, target: float) -> None:
        """Set the target power for the amplifier to `target` and start it."""
        if target < -1:
            target = -2
        elif target < 0:
            target = -1
        self.amplifier.device.power_setpoint = target

    @pyqtSlot()
    def setPowerPressed(self) -> None:
        """Set the target power for the amplifier or stop a change."""
        power = self.commander.sbPower.value()
        self.setPower(target=power)

    @pyqtSlot()
    def request_default_data(self) -> None:
        """Read the amplifier."""
        self.amplifier.get_parameters_async(
            ("power", "power_setpoint", "temperature", "current")
        )

    def show_data(self, data: dict[str, float]) -> None:
        """Use the `data` read from the amplifier."""
        self.commander.lbPowerSetpoint.setText(
            f"set: {data.get('power_setpoint', nan):.3f} W"
        )
        self.commander.lbPower.setText(f"{data.get('power', nan):.3f} W")
        if "current" in data.keys():
            self.commander.lbPowerCurrent.setText(f"{data['current']:.2f} A")
        if "temperature" in data.keys():
            self.commander.lbPowerTemperature.setText(f"{data['temperature']:.1f}°C")


class AculightGroup(InstrumentGroup):
    actor = ACULIGHT
    _to_init = False

    class State(NamedTuple):
        crystal_temperature_setpoint: float
        crystal_temperature: float
        etalon_angle: float
        seed_voltage: float

    def make_connections(self) -> None:
        super().make_connections()
        self.groupbox = self.commander.gbAculight
        self.commander.gbAculight.toggled.connect(self.connect_device)
        self.commander.sbAculightEtalon.valueChanged.connect(self.set_etalon_angle)
        self.commander.pbAculightCrystalTemp.clicked.connect(
            self.set_crystal_temperature_pressed
        )

    def connect_device(self, checked: bool) -> None:
        self.commander.gbAculight.setChecked(checked)
        if checked:
            self.commander.start_task(self.actor)
        self.poll(checked=checked)
        self._to_init = checked

    def set_crystal_temperature_pressed(self) -> None:
        self.set_crystal_temperature(self.commander.sbAculightCrystalTemp.value())

    def set_crystal_temperature(self, value: float) -> None:
        self.commander.director.set_parameters(
            {"temperature_setpoint": value}, actor=self.actor
        )

    def set_etalon_angle(self, value: float) -> None:
        try:
            self.commander.director.set_parameters({"etalon": value}, actor=self.actor)
        except JSONRPCError as exc:
            self.commander.show_status_bar_message(f"Setting etalon angle failed {exc}")

    def request_default_data(self):
        self.commander.director.get_parameters_async(["state"], actor=self.actor)

    def show_data(self, data: dict[str, Any]) -> None:
        state = data.get("state")
        if state:
            (
                crystal_temperature_setpoint,
                crystal_temperature,
                etalon_angle,
                seed_voltage,
            ) = state
            self.commander.lbAculight.setText(
                f"crystal {crystal_temperature}°C, etalon {etalon_angle}°"
            )
            if self._to_init:
                self.commander.sbAculightCrystalTemp.setValue(
                    crystal_temperature_setpoint
                )
                self.commander.sbAculightEtalon.setValue(etalon_angle)
                self._to_init = False


class MatchingGroup(InstrumentGroup):
    actor = MATCHING_MOTORS

    def __init__(self, commander: Lasercommander, **kwargs) -> None:
        super().__init__(commander, **kwargs)
        self.setup_control()

    def make_connections(self):
        self.groupbox = self.commander.gbMatchingMotors
        #    Matching motors
        self.commander.gbMatchingMotors.toggled.connect(self.connect_device)
        self.commander.pbOPOPosition.clicked.connect(self.setOPOPosition)
        self.commander.pbOPO2Position.clicked.connect(self.setOPO2Position)
        self.commander.pbOPA2aPosition.clicked.connect(self.setOPA2aPosition)
        self.commander.pbOPA2bPosition.clicked.connect(self.setOPA2bPosition)
        #    Matching motors keyboard control
        self.commander.actionMatchingControl.toggled.connect(self.control)

    @pyqtSlot(bool)
    def connect_device(self, checked: bool) -> None:
        """Connect the stepper motor for the polarizer attenuator."""
        self.commander.gbMatchingMotors.setChecked(checked)
        if checked:
            self.commander.start_task(self.actor)
            try:
                self.motors = TMCMotorDirector(
                    self.actor, communicator=self.commander.communicator
                )
            except Exception as exc:
                self.commander.connectionLost(self.actor, "connectMatchingMotors", exc)
        else:
            try:
                del self.motors
            except Exception:
                pass
        self.commander.readout_connection(self.readMatchingMotors, checked)

    "### Matching motors"

    @pyqtSlot()
    def setOPOPosition(self) -> None:
        self.setMatchingPosition("???", self.commander.sbOPOPosition.value())

    @pyqtSlot()
    def setOPO2Position(self) -> None:
        self.setMatchingPosition("OPO", self.commander.sbOPO2Position.value())

    @pyqtSlot()
    def setOPA2aPosition(self) -> None:
        self.setMatchingPosition("OPA2a", self.commander.sbOPA2aPosition.value())

    @pyqtSlot()
    def setOPA2bPosition(self) -> None:
        self.setMatchingPosition("OPA2b", self.commander.sbOPA2bPosition.value())

    def setMatchingPosition(self, motor: int | str, value: float) -> None:
        """Set the motor to the position `value` in µm."""
        try:
            self.motors.move_to_units(motor=motor, position=value / 1000)
        except ServerError as exc:
            self.commander.showInformation(
                "Motor error",
                text="Setting matching position failed.",
                detailedText=f"motor {motor}, value {value}\n{exc}",
            )

    def rotate_OPA2(self, motor: str, direction: int) -> None:
        """Rotate a OPA2 motor (0 or 1) in direction (-1 or +1)."""
        try:
            self.motors.rotate(
                motor=motor, velocity=self.commander.sbControl.value() * direction
            )
        except ServerError as exc:
            self.commander.showInformation(
                "Motor error",
                text="Rotate OPA2 failed..",
                detailedText=f"motor {motor}, value {direction}\n{exc}",
            )
        else:
            self._rotating[motor] = direction

    def stop_OPA2(self, motor: str) -> None:
        self.motors.stop(motor)
        self._rotating[motor] = 0

    @pyqtSlot()
    def readMatchingMotors(self) -> None:
        """Read the matching motor card."""
        data = {}
        for motor in ("OPO", "OPA2a", "OPA2b"):
            try:
                data[motor] = self.motors.get_actual_units(motor)
            except Exception as exc:
                self.commander.connectionLost(self.actor, "Readout", exc)
                return
        self.show_data(data)

    def show_data(self, data: dict) -> None:
        """Use the `data` read from the Phase Matching Motors."""
        for motor, label, spinbox in (
            # ('???', self.lbOPOPosition, self.sbOPOPosition),
            ("OPO", self.commander.lbOPO2Position, self.commander.sbOPO2Position),
            ("OPA2a", self.commander.lbOPA2a, self.commander.sbOPA2aPosition),
            ("OPA2b", self.commander.lbOPA2b, self.commander.sbOPA2bPosition),
        ):
            label.setText(f"{data.get(motor, nan):.4f} mm")

    "## Keyboard control"

    def setup_control(self) -> None:
        """Setup keyboard control."""
        self._rotating: dict[str, int] = {"OPA2a": 0, "OPA2b": 0}
        self.commander.pbControl.keyPressEvent = self.control_keyPressEvent  # type: ignore
        self.commander.pbControl.keyReleaseEvent = self.control_keyReleaseEvent  # type: ignore

    @pyqtSlot(bool)
    def control(self, checked: bool) -> None:
        """Control a motor via keyboard."""
        self.commander.pbControl.setChecked(checked)
        if checked:
            self.commander.pbControl.grabKeyboard()
        else:
            self.commander.pbControl.releaseKeyboard()

    def control_keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        """React to a pressed key."""
        if event.isAutoRepeat():
            return  # Ignore autorepeat while key is held down.
        key = event.key()
        match key:  # noqa
            case 16777227 | 53:  # center button, 5
                pass
            case 16777235 | 56 | 16777232 | 55:  # Arrow up, 8, pos1, 7
                self.rotate_OPA2("OPA2a", +1)
            case 16777237 | 50 | 16777233 | 49:  # Arrow down, 2, end, 1
                self.rotate_OPA2("OPA2a", -1)
            case 16777234 | 52:  # Arrow left, 4
                self.commander.sbControl.setValue(self.commander.sbControl.value() // 2)
                for motor in ("OPA2a", "OPA2b"):
                    if d := self._rotating[motor]:
                        self.rotate_OPA2(motor, d)
            case 16777236 | 54:  # Arrow right, 6
                self.commander.sbControl.setValue(self.commander.sbControl.value() * 2)
                for motor in ("OPA2a", "OPA2b"):
                    if d := self._rotating[motor]:
                        self.rotate_OPA2(motor, d)
            case 16777238 | 57:  # page up, 9
                self.rotate_OPA2("OPA2b", +1)
            case 16777239 | 51:  # page down, 3
                self.rotate_OPA2("OPA2b", -1)
            case 43:  # +
                pass
            case 45:  # -
                pass
            case 16777220 | 16777221:  # main enter, keypad enter
                pass

    def control_keyReleaseEvent(self, event: QtGui.QKeyEvent) -> None:
        """React to a pressed key."""
        if event.isAutoRepeat():
            return  # Ignore autorepeat while key is held down.
        key = event.key()
        match key:
            case 16777235 | 56 | 16777232 | 55 | 16777237 | 50 | 16777233 | 49:  # 7, 8, 1, 2
                self.stop_OPA2("OPA2a")
            case 16777238 | 57 | 16777239 | 51:  # 9, 3
                self.stop_OPA2("OPA2b")
            case 16777220 | 16777221:  # main enter, keypad enter
                self.commander.actionMatchingControl.setChecked(False)


class OPOGroup(InstrumentGroup):
    actor = WAVEMETER

    def make_connections(self) -> None:
        self.groupbox = self.commander.gbOPO
        self.commander.gbOPO.toggled.connect(self.poll)

    def request_default_data(self) -> None:
        self.commander.director.get_parameters_async(
            ["wavelength", "power"], actor=self.actor
        )

    def show_data(self, data: dict[str, Any]) -> None:
        wl = data.get("wavelength")
        pw = data.get("power")
        wlt = f"{wl:.3f}" if wl is not None else "-"
        pwt = f"{pw:g}" if pw is not None else "-"
        self.commander.lbOPO.setText(f"Signal {wlt} nm, {pwt} µW")

    def connect_device(self, checked: bool) -> None:
        self.commander.gbOPO.setChecked(checked)


class PolarizerGroup(InstrumentGroup):
    actor = POLARIZER

    def make_connections(self):
        self.groupbox = self.commander.gbPolarizer
        self.commander.gbPolarizer.toggled.connect(self.connect_device)
        self.commander.pbPolarizer.clicked.connect(self.setPolarizerPressed)
        # Connect slPolarizer to setCurrentPolarizer
        self.setDirectPolarizerControl(True)
        self.commander.slPolarizer.valueChanged.connect(self.polarizerSliderMove)
        self.commander.slPolarizer.sliderMoved.connect(self.polarizerSliderMove)
        self.commander.bbPolarizer.activated.connect(self.polarizerComboActivated)

    @pyqtSlot(bool)
    def connect_device(self, checked: bool) -> None:
        """Connect the stepper motor for the polarizer attenuator."""
        self.commander.gbPolarizer.setChecked(checked)
        if checked:
            self.commander.start_task(self.actor)
            try:
                self.polarizer = TMCMotorDirector(
                    self.actor, communicator=self.commander.communicator
                )
            except Exception as exc:
                self.commander.connectionLost(self.actor, "connectPolarizer", exc)
        else:
            try:
                del self.polarizer
            except Exception:
                pass
        self.commander.readout_connection(self.readPolarizer, checked)

    @pyqtSlot(int)
    def setCurrentPolarizer(self, angle: float) -> None:
        """Set the currently chosen polarizer to an `angle`."""
        self.setPolarizer(self.commander.bbPolarizer.currentText(), angle)

    def setPolarizer(self, polarizer: str, angle: float) -> None:
        """Set the `polarizer` to an `angle`."""
        try:
            self.polarizer.move_to_units(polarizer.replace("polarizer", ""), angle)
        except ServerError as exc:
            self.commander.showInformation(
                "Polarizer error",
                text="Setting polarizer position failed.",
                detailedText=f"motor '{polarizer}', value {angle}\n{exc}",
            )

    @pyqtSlot()
    def setPolarizerPressed(self) -> None:
        """Set polarizer according to spinbox."""
        self.setCurrentPolarizer(self.commander.sbPolarizer.value())

    @pyqtSlot(int)
    def polarizerSliderMove(self, angle: int) -> None:
        """Set the spinbox according to the slider."""
        self.commander.sbPolarizer.setValue(angle)

    @pyqtSlot(int)
    def polarizerComboActivated(self, index: int) -> None:
        """Show the current position of the chosen motor in the spinbox."""
        name = self.commander.bbPolarizer.currentText().replace("Probe", "probe")
        value = self.polarizerPosition[name]
        self.commander.sbPolarizer.setValue(value)
        # self.commander.slPolarizer.setValue(round(value))

    @pyqtSlot(bool)
    def setDirectPolarizerControl(self, checked: bool) -> None:
        """(De)Activate direct polarizer control."""
        if checked:
            self.commander.slPolarizer.valueChanged.connect(self.setCurrentPolarizer)
        else:
            try:
                self.commander.slPolarizer.valueChanged.disconnect()
            except Exception:
                pass

    @pyqtSlot()
    def initializePolarizerMotors(self) -> None:
        """Send the polarizer motors to the start position for measurement."""
        if self.commander.gbPolarizer.isChecked():
            for motor, start in polarizer_start_values.items():
                self.setPolarizer(motor, start)

    @pyqtSlot()
    def restPolarizerMotors(self) -> None:
        """Send the polarizer motors to their rest position."""
        if self.commander.gbPolarizer.isChecked():
            for motor in ("OPO", "OPA2", "MIR"):
                try:
                    self.polarizer.move_to(motor, 0)
                except ServerError as exc:
                    self.commander.showInformation(
                        title="Polarizer error",
                        text="Setting polarizer position failed.",
                        detailedText=f"motor '{motor}', value {0}\n{exc}",
                    )
                    return
                except Exception as exc:
                    self.commander.connectionLost(
                        self.actor, "restPolarizerMotors", exc
                    )

    @pyqtSlot()
    def readPolarizer(self) -> None:
        """Read the polarizer motor card."""
        data = {}
        for motor in ("OPO", "OPA2", "MIR"):
            try:
                data[motor] = self.polarizer.get_actual_units(motor)
            except Exception as exc:
                self.commander.connectionLost(self.actor, "Readout", exc)
                return
        self.show_data(data)

    def show_data(self, data: dict[str, float]) -> None:
        """Use the `data` read from the Polarizer Attenuator."""
        text = (
            f"OPO: {data.get('OPO', nan):6.2f}°\t"
            f"OPA2: {data.get('OPA2', nan):6.2f}°"
        )
        self.commander.lbPolarizerOPA.setText(text)
        text = (
            f"MIR: {data.get('MIR', nan):6.2f}°"
        )
        self.commander.lbPolarizerExp.setText(text)
        self.polarizerPosition = data


class YAGShutterGroup(InstrumentGroup):
    actor = YAG_SHUTTER
    parameters = ["shg_open", "fundamental_open"]

    def __init__(self, commander: Lasercommander, **kwargs):
        super().__init__(commander=commander, **kwargs)
        self.director = Director(actor=YAG_SHUTTER, communicator=commander.communicator)

    def make_connections(self):
        self.commander.pbYAGShutter.clicked.connect(self.getYAGShutterState)
        self.commander.pbYAGShutterPoll.clicked.connect(self.poll)
        self.commander.rbYAGfundO.clicked.connect(self.setYAGFundamentalShutterOpen)
        self.commander.rbYAGfundN.clicked.connect(self.setYAGFundamentalShutterNeutral)
        self.commander.rbYAGfundL.clicked.connect(self.setYAGFundamentalShutterLock)

    @pyqtSlot()
    def getYAGShutterState(self) -> None:
        """Get the state of the YAG beamdump shutters."""
        try:
            result = self.director.get_parameters(parameters=self.parameters)
        except Exception as exc:
            log.exception("Reading YAG shutter state failed.", exc_info=exc)
            for label in (
                self.commander.lbYAGShutterFund,
                self.commander.lbYAGShutterSHG,
            ):
                label.setText("-")
                label.setStyleSheet("color: black")
        else:
            self.show_data(result)

    @pyqtSlot()
    def request_default_data(self) -> None:
        self.director.get_parameters_async(parameters=self.parameters)

    def show_data(self, data: dict[str, int]) -> None:
        mapping = {-1: enforced_close_mark, 0: closed_mark, 1: open_mark, None: "?"}
        colors = {
            enforced_close_mark: "red",
            closed_mark: "green",
            open_mark: "orange",
            "?": "black",
        }
        shg = mapping.get(data.get("shg_open"), "?")
        fund = mapping.get(data.get("fundamental_open"), "?")
        self.commander.lbYAGShutterFund.setText(f"F {fund}")
        self.commander.lbYAGShutterSHG.setText(f"S {shg}")
        for value, label in (
            (fund, self.commander.lbYAGShutterFund),
            (shg, self.commander.lbYAGShutterSHG),
        ):
            label.setStyleSheet(
                f"color: {colors.get(value, 'black')}; font-weight: bold"
            )

    @pyqtSlot(bool)
    def blockYAGSHGShutter(self, checked: bool) -> None:
        """Block the YAG shutter for the SHG beam."""
        try:
            self.director.set_parameters(
                {"shg_switch": -1 * int(checked)}, actor=YAG_SHUTTER
            )
        except TimeoutError:
            self.commander.show_status_bar_message(
                "Timeout during 'set shutter state'."
            )

    @pyqtSlot(bool)
    def setYAGFundamentalShutterOpen(self, checked: bool = True) -> None:
        if checked:
            self.set_YAG_fundamental_shutter_switch(1)

    @pyqtSlot(bool)
    def setYAGFundamentalShutterNeutral(self, checked: bool = True) -> None:
        if checked:
            self.set_YAG_fundamental_shutter_switch(0)

    @pyqtSlot(bool)
    def setYAGFundamentalShutterLock(self, checked: bool = True) -> None:
        if checked:
            self.set_YAG_fundamental_shutter_switch(-1)

    def set_YAG_fundamental_shutter_switch(self, state: int) -> None:
        """Set the YAG fundamental shutter switch to a state: 1 open, 0 neutral, -1 lock."""
        try:
            self.director.set_parameters(
                {"fundamental_switch": state}, actor=YAG_SHUTTER
            )
        except JSONRPCError as exc:
            self.commander.show_status_bar_message(f"Set shutter state error {exc}")
        except TimeoutError:
            self.commander.show_status_bar_message(
                "Timeout during 'set shutter state'."
            )


class YAGSeedGroup(InstrumentGroup):
    actor = YAG_SEED
    parameters = ["temperature_0", "temperature_2", "temperature_3", "temperature_4"]

    def __init__(self, commander: Lasercommander, **kwargs):
        super().__init__(commander=commander, **kwargs)
        self.director = Director(actor=self.actor, communicator=commander.communicator)

    def make_connections(self):
        self.commander.pbSeedTemperature.clicked.connect(self.getYAGSeedTemperature)
        self.commander.pbSeedTemperaturePoll.clicked.connect(self.poll)

    @pyqtSlot()
    def getYAGSeedTemperature(self) -> None:
        """Get the YAG seed temperatures."""
        try:
            result = self.director.get_parameters(parameters=self.parameters)
        except TimeoutError:
            self.commander.showInformation(
                title="YAG seed timeout",
                text="Timeout while getting Quanta-Ray seed temperatures.",
            )
            self.commander.lbSeedTemperature1.setText("-")
            self.commander.lbSeedTemperature2.setText("-")
        except JSONRPCError as exc:
            self.commander.showInformation(
                title="YAG seed error",
                text=f"Error while getting Quanta-Ray seed temperatures: {exc}.",
            )
        else:
            self.show_data(result)

    @pyqtSlot()
    def request_default_data(self) -> None:
        self.director.get_parameters_async(parameters=self.parameters)

    def show_data(self, data: dict[str, float]) -> None:
        t0 = data.get("temperature_0", -1)  # TEC1
        t2 = data.get("temperature_2", -1)  # Heater1
        t3 = data.get("temperature_3", -1)  # Heater2
        t4 = data.get("temperature_4", -1)  # PCB
        t0s, t2s, t3s = YAG_seed_target_values
        well = round(t0, 2) == t0s and round(t2, 2) == t2s and round(t3s, 2) == t3s
        self.commander.lbSeedTemperature1.setText(f"{t2:.3f}°C  {t3:.3f}°C")
        self.commander.lbSeedTemperature2.setText(
            f"{t0:.3f}°C  {t4:.3f}°C  {check_mark if well else x_mark}"
        )
        color = "green" if well else "red"
        self.commander.lbSeedTemperature1.setStyleSheet(f"color: {color}")
        self.commander.lbSeedTemperature2.setStyleSheet(f"color: {color}")


class YAGSeedShutterGroup(InstrumentGroup):
    actor = YAG_SEED_SHUTTER

    def __init__(self, commander: Lasercommander, **kwargs):
        super().__init__(commander, **kwargs)
        self.director = Director(actor=self.actor, communicator=commander.communicator)

    def make_connections(self):
        self.commander.pbSeedShutter.clicked.connect(self.getYAGSeedShutter)
        self.commander.pbSeedShutterOpen.clicked.connect(self.openYAGSeedShutter)
        self.commander.pbSeedShutterClose.clicked.connect(self.closeYAGSeedShutter)

    @pyqtSlot()
    def getYAGSeedShutter(self) -> None:
        """Get the state of the YAG seed shutter."""
        try:
            result = self.director.get_parameters(parameters=["shutter_open"])
        except TimeoutError:
            self.commander.showInformation(
                "Seed shutter timeout",
                "Timeout while getting Quanta-Ray seed shutter state.",
            )
            self.commander.lbSeedShutter.setText("-")
        except JSONRPCError as exc:
            self.commander.showInformation(
                "Seed shutter error",
                f"Error while getting Quanta-Ray seed shutter state: {exc}.",
            )
            self.commander.lbSeedShutter.setText("-")
        else:
            self.show_data(result)

    def show_data(self, data: dict[str, bool]) -> None:
        try:
            state = data["shutter_open"]
        except Exception as exc:
            log.exception(f"Shutter state data exception: {data}", exc_info=exc)
        else:
            mapping = {True: open_mark, False: closed_mark, "Unknown": "?"}
            color_mapping = {True: "green", False: "red"}
            self.commander.lbSeedShutter.setStyleSheet(
                f"color: {color_mapping.get(state, 'orange')}; font-weight: bold"
            )
            self.commander.lbSeedShutter.setText(mapping.get(state, "-"))

    @pyqtSlot()
    def openYAGSeedShutter(self) -> None:
        """Open the YAG seed shutter."""
        self._open_seed_YAG_seed_shutter(state=True)

    @pyqtSlot()
    def closeYAGSeedShutter(self) -> None:
        """Close the YAG seed shutter."""
        self._open_seed_YAG_seed_shutter(state=False)

    def _open_seed_YAG_seed_shutter(self, state: bool) -> None:
        """Open or close the YAG seed shutter."""
        try:
            self.director.set_parameters({"shutter_open": bool(state)})
        except TimeoutError:
            self.commander.showInformation(
                title="Seed shutter timeout",
                text="Timeout while setting Quanta-Ray seed shutter state.",
            )
        except JSONRPCError as exc:
            self.commander.showInformation(
                title="Seed shutter error",
                text=f"Error while setting Quanta-Ray seed shutter state: {exc}.",
            )
        else:
            self.getYAGSeedShutter()


class YagGroup(InstrumentGroup):
    actor = YAG
    but_actor = PICO3_NICARD

    def __init__(self, commander: Lasercommander, **kwargs) -> None:
        super().__init__(commander=commander, **kwargs)
        self.but_director = TransparentDirector(
            actor=self.but_actor, communicator=self.commander.communicator
        )
        self.yag_director = TransparentDirector(
            actor=self.actor, communicator=self.commander.communicator
        )

    def make_connections(self) -> None:
        """Make the connections between Signals and Slots."""
        self.groupbox = self.commander.gbYAG
        self.commander.gbYAG.toggled.connect(self.connect_device)

    @pyqtSlot(bool)
    def connect_device(self, checked: bool) -> None:
        """Connect the stepper motor for the polarizer attenuator."""
        self.commander.gbYAG.setChecked(checked)
        if checked:
            self.commander.start_task(self.but_actor)
            self.commander.start_task(self.actor)
        self.poll(checked=checked)

    @pyqtSlot()
    def request_default_data(self) -> None:
        """Request data for regular readout."""
        self.ask_and_show_but()
        self.yag_director.get_parameters_async(parameters=["shot_count"])
        # self.ask_and_show_shots()

    def show_data(self, data: dict[str, Any]) -> None:
        """Show any data returned by the device."""
        for key, value in data.items():
            if key == "shot_count":
                self.commander.lbYAG_shots.setText(f"{value:,} shots")

    def ask_and_show_but(self):
        try:
            but = self.but_director.device.yag_but  # type: ignore
        except Exception as exc:
            log.exception("Failed to get YAG_BUT value.", exc_info=exc)
            but = None
        self.commander.lbYAG_BUT.setText("-" if but is None else f"BUT: {but:.3} V")

    def ask_and_show_shots(self):
        try:
            shots = self.yag_director.device.shot_count  # type: ignore
        except Exception as exc:
            log.exception("Failed to get YAG shots.", exc_info=exc)
            shots = None
        self.commander.lbYAG_shots.setText("-" if shots is None else f"{shots:,} shots")


class LabTemperatureGroup(InstrumentGroup):
    actor = LAB_TEMPERATURE_CONTROLLER
    counter: int = 0
    INTERVAL = 5  # readout loops
    # for temperature
    SETPOINT = 24.2  # °C
    TOLERANCE = 0.5  # K
    MAX_OUTPUT = 4  # V

    def __init__(self, commander: Lasercommander, **kwargs) -> None:
        super().__init__(commander, **kwargs)
        self.groupbox = self.commander.gbLabTemperature

    def make_connections(self) -> None:
        super().make_connections()
        self.commander.gbLabTemperature.toggled.connect(self.connect_device)

    def connect_device(self, checked: bool) -> None:
        "(Dis)Connect the device."
        if checked:
            # directly start
            self.counter = 1000
        self.commander.gbLabTemperature.setChecked(checked)
        self.poll(checked=checked)

    def connection_lost_action(self) -> None:
        pass

    @pyqtSlot()
    def request_default_data(self) -> None:
        """Request data for regular readout."""
        if self.counter < self.INTERVAL:
            self.counter += 1
        else:
            self.commander.director.ask_rpc_async("get_current_data", actor=self.actor)
            self.counter = 1

    def show_data(self, data: dict[str, Any]) -> None:
        """Show any data returned by the device."""
        mira = data.get("mira", nan)
        output = data.get("pidOutput0", nan)
        self.commander.lbLabTemperature.setText(f'mira {mira:.2f}°C, out {output:.2f} V')
        mira_error = abs(mira - self.SETPOINT) > self.TOLERANCE
        output_error = not (0.5 < output < self.MAX_OUTPUT)
        color = "red" if mira_error or output_error else "green"
        self.commander.lbLabTemperature.setStyleSheet(f"color: {color}")


if __name__ == "__main__":
    """Start the main window if this is the called script file."""
    start_app(
        main_window_class=Lasercommander,
        logger=gLog,
    )

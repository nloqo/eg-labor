# -*- coding: utf-8 -*-
"""
Testing the Lasercommander.

Created on Wed Feb  2 12:26:03 2022 by Benedikt Moneke
"""

import pytest

from Lasercommander import Lasercommander


class Empty:
    pass


class Mock_Widget:
    def __init__(self, value=None):
        self.content = value

    def value(self):
        return self.content

    def setValue(self, value):
        self.content = value


@pytest.fixture(autouse=True)
def empty():
    return Empty()


@pytest.fixture
def nonef():
    """Just do nothing."""
    def n(*args, **kwargs):
        pass
    return n


@pytest.fixture
def widget():
    return Mock_Widget()


@pytest.fixture
def seed():
    seed = Empty()
    seed.wlRange = (-380, 370)
    return seed


class Test_setWavelength:
    @pytest.fixture(autouse=True)
    def lc(self, empty, widget, seed, nonef):
        empty.connectionLost = nonef
        empty.threadpool = empty
        empty.threadpool.start = nonef
        empty.seed = seed
        empty.seed.setWavelengthSetpoint = nonef
        empty.sbWavelength = widget
        empty.locks = {'seed': None}
        return empty

    def test_tooLow(self, lc):
        lc.sbWavelength.content = 1000
        Lasercommander.setWavelength(lc)
        assert lc.sbWavelength.value() == 1063.62


class Test_setMin_MaxWavelength:
    @pytest.fixture
    def lc(self, empty, widget, seed):
        empty.seed = seed
        empty.sbWavelength = widget
        return empty

    def test_min(self, lc):
        Lasercommander.setWavelengthMin(lc)
        assert lc.sbWavelength.value() == 1063.62

    def test_max(self, lc):
        Lasercommander.setWavelengthMax(lc)
        assert lc.sbWavelength.value() == 1064.37

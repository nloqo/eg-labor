# -*- coding: utf-8 -*-
"""
Testing Readout from Threading.py

Created on Thu Feb  3 12:17:12 2022 by Benedikt Moneke

@author: moneke
"""

import pytest

from PyQt6 import QtCore

from devices import testClasses as t

from data.Threading import Readout


class QSettings(t.QSettings):
    def __init__(self):
        global settings
        self.settings = settings


@pytest.fixture()
def settings(monkeypatch):
    global settings
    settings = {}
    monkeypatch.setattr(QtCore, "QSettings", QSettings)


class Test_readMatchingMotors:
    @pytest.fixture
    def r(self, settings):
        r = t.Empty()
        r.data = t.pyqtSignal()
        r.devices = {}
        r.devices['MatchingMotors'] = t.TMC([0, 1, 2])
        r.locks = {}
        r.locks['MatchingMotors'] = QtCore.QMutex()
        return r

    def test_noConfig(self, r):
        Readout.readMatchingMotors(r)
        assert r.data.emitted == ('MatchingMotors', {})

    def test_OPOMotor(self, r):
        global settings
        settings['OPOMotor'] = {'motorNumber': 0}
        settings['OPA2aMotor'] = {'motorNumber': 1}
        settings['OPA2bMotor'] = {'motorNumber': 2}
        Readout.readMatchingMotors(r)
        assert r.data.emitted == ('MatchingMotors',
                                  {'OPOMotor': {'actualPosition': 0},
                                   'OPA2aMotor': {'actualPosition': 1},
                                   'OPA2bMotor': {'actualPosition': 2}})

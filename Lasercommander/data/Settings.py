"""
Settings dialog for the Lasercommander.

classes
-------
GeneralSettings
    The dialog for all the settings.

Created on Thu Nov 26 19:02:38 2020 by Benedikt Moneke
"""

from qtpy import QtWidgets

from pyleco_extras.gui_utils.base_settings import BaseSettings


class GeneralSettings(BaseSettings):
    """Define the settings dialog and its methods."""

    def setup_form(self, layout):
        interval = QtWidgets.QSpinBox()
        interval.setToolTip("Interval between readouts from connected devices.")
        interval.setSuffix(" ms")
        interval.setRange(10, 10000)
        self.add_value_widget("Readout interval", interval, "plotInterval", 1000, int)

        seedScanStepSize = QtWidgets.QSpinBox()
        seedScanStepSize.setToolTip("Number of half periods for a single step.")
        seedScanStepSize.setSuffix(" half periods")
        seedScanStepSize.setMinimum(1)
        self.add_value_widget("Seed scan step", seedScanStepSize, "seedScanStepSize", 4, int)

        ampPower = QtWidgets.QDoubleSpinBox()
        ampPower.setToolTip("Default power at startup.")
        ampPower.setSuffix(" W")
        ampPower.setRange(-1, 15)
        self.add_value_widget("Amplifier power", ampPower, "amplifierPower", 0, float)


if __name__ == "__main__":  # if this is the started script file
    """Start the main window if this is the called script file."""
    app = QtWidgets.QApplication([])  # start an application
    app.setOrganizationName("NLOQO")
    app.setApplicationName("Lasercommander")
    mainwindow = GeneralSettings()  # start the first widget, the main window
    app.exec()  # start the application with its Event loop
